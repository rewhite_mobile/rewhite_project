package me.rewhite.delivery.tablet;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.google.android.gms.common.util.ProcessUtils;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import fdk.FDK_Module;
import me.rewhite.delivery.R;
import me.rewhite.delivery.TabletFragment.TabletPickupDetailFragmentV2Local;
import me.rewhite.delivery.TabletFragment.TabletPickupItemBlankFragmentV2;
import me.rewhite.delivery.TabletFragment.TabletPickupItemFragmentV2;
import me.rewhite.delivery.TabletFragment.TabletPickupItemSubCategoryFragmentV2;
import me.rewhite.delivery.activity.PickupItemAdditionalPicker;
import me.rewhite.delivery.activity.PickupItemColorPicker;
import me.rewhite.delivery.activity.PickupItemCopyCount;
import me.rewhite.delivery.activity.PickupItemPartsPicker;
import me.rewhite.delivery.activity.PickupItemPriceMod;
import me.rewhite.delivery.activity.PickupItemRepairPicker;
import me.rewhite.delivery.activity.PickupItemTagMod;
import me.rewhite.delivery.adapter.TableLocalPickupElementListViewAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.common.receiver.VanReceiver;
import me.rewhite.delivery.common.util.Logger;
import me.rewhite.delivery.common.util.ProgressDialogHelper;
import me.rewhite.delivery.data.PickupType;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.task.TabletImageUploadTask2;
import me.rewhite.delivery.util.BarcodeStringUtil;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.TimeUtil;
import me.rewhite.delivery.util.ToastUtility;
import me.rewhite.delivery.util.UnicodeFormatter;
import me.rewhite.delivery.van.CatManager;
import me.rewhite.delivery.van.VanType;

public class LocalOrderInputActivity extends TabletBaseActivity {

    private final static String TAG = "LocalOrderInputActivity";
    private JSONObject selectedUserData;
    private AQuery aq;

    public static final String FRAGMENT_CATEGORY = "pickup_category";
    public static final String FRAGMENT_SUB_CATEGORY = "pickup_subcategory";
    public static final String FRAGMENT_DETAIL = "pickup_detail";
    public static final String FRAGMENT_BLANK = "pickup_blank";

    private static final int PICK_COLOR = 11;
    private static final int PICK_REPAIR = 12;
    private static final int PICK_PARTS = 13;
    private static final int PICK_ADDITIONAL = 14;
    private static final int PICK_PRICE = 15;

    private static final int ADD_ELEMENT = 16;
    private static final int MOD_TAG = 17;
    private static final int COPY_ELEMENT = 18;
    private static final int MOD_ELEMENT = 19;

    private static final int SEARCH_USER_RESULT = 21;
    private static final int ADD_USER_RESULT = 22;
    private static final int MOD_USER_RESULT = 23;

    private int completedOrderId = -1;
    private int currentOrderId = -1;
    private int selectedOrderId = -1;
    private JSONObject currentJsonData;
    private JSONArray pickupItemsV2;

    int prevTagNo = -1;

    private ListView bucketListView;
    private TableLocalPickupElementListViewAdapter elementAdapter;
    int pickCount = -1;

    public String Step1Name = "";
    public int Step1Index = 0;
    public String Step2Name = "";
    public int Step2Index = 0;

    public String colorPickValue = "";
    public JSONArray pickedRepairArray;
    public JSONArray pickedAdditionalArray;
    public JSONArray pickedPartsArray;
    public int pickedPrice = 0;

    public JSONArray repairArray;
    public JSONArray additionalArray;
    public JSONArray partsArray;
    public JSONArray colorsArray;
    public JSONArray priceArray;

    public int tagNo;
    public int tagLength;
    public int bucketIndex = 0;
    public int pickupMode = -1;

    public JSONArray pickupItems;
    //public int pickupItemsPosition = 0;

    public String pickedRelaundry = "N";
    public String pickedService = "N";
    public String laundry = "";

    boolean isInitialized = false;

    private ProgressDialog mProgressDialog;

    private Messenger mService = null;    // 서비스와 통신하는데 사용되는 메신저
    private boolean mBound = false;    // 서비스 연결 여부
    private VanReceiver vanReceiver;

    @Override
    protected void onStart() {
        Logger.d(TAG, "onStart");
        currentVanType = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.VAN_TYPE);
        if (currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)) {

        } else if (currentVanType.equals(VanType.FIRSTDATA)) {

        } else if (currentVanType.equals(VanType.SMARTRO)) {
            //Register BroadcastReceiver
            //to receive event from our service
            vanReceiver = new VanReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(CatManager.INTENT_ACTION);
            registerReceiver(vanReceiver, intentFilter);
        }

        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logger.d(TAG, "onCrteate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablet_activity_local_order_input);

        currentVanType = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.VAN_TYPE);
        if (currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)) {

        } else if (currentVanType.equals(VanType.FIRSTDATA)) {
            try {
                FDK_Module.Deliver_App_Resources_to_FDK("FTDI", this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (currentVanType.equals(VanType.SMARTRO)) {
            String successDeviceIP = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.CAT_DEVICE_IP);
            if (successDeviceIP != null) {
                Intent catIntent = new Intent(this, CatManager.class);
                catIntent.putExtra("ServerIP", successDeviceIP);
                bindService(catIntent, mConnection, Context.BIND_AUTO_CREATE);
            }
        }

        ProgressDialogHelper.showDialog(this, "잠시만 기다려주세요");

        aq = new AQuery(this);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("userData") != null) {
                try {
                    selectedUserData = new JSONObject(intent.getStringExtra("userData"));

                    ImageOptions op = new ImageOptions();
                    op.round = 60;
                    op.memCache = true;
                    op.ratio = 1.f;

                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.nickname_text).getTextView());
                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.address_text).getTextView());
                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.phone_text).getTextView());
                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ordercount_text).getTextView());
                    CommonUtility.setTypefaceNormalSetup(aq.id(R.id.recv_title).getTextView());
                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.recv_price).getTextView());
                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_submit).getTextView());

                    CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_top).getTextView());
                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.current_tag_no).getTextView());


                    aq.id(R.id.profile_image).image(selectedUserData.getString("imageThumbPath"), op);
                    aq.id(R.id.nickname_text).text(selectedUserData.getString("userName"));
                    aq.id(R.id.address_text).text(selectedUserData.getString("address1") + " " + selectedUserData.getString("address2"));
                    aq.id(R.id.phone_text).text(MZValidator.validTelNumber(selectedUserData.getString("phone")));
                    aq.id(R.id.ordercount_text).text(selectedUserData.getString("orderCount"));
                    aq.id(R.id.recv_price).text(MZValidator.toNumFormat(selectedUserData.getInt("receivablePrice")) + " 원");
                    aq.id(R.id.btn_add).clicked(this, "addAction");
                    aq.id(R.id.btn_user_mod).clicked(this, "modUserAction");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        aq.id(R.id.btn_back).clicked(this, "backStep");
        bootstrap();
    }


    @Override
    public void onStop() {
        Logger.d(TAG, "onStop");
        super.onStop();
        if (currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)) {

        } else if (currentVanType.equals(VanType.FIRSTDATA)) {

        } else if (currentVanType.equals(VanType.SMARTRO)) {
            if (mBound) {
                unbindService(mConnection);
                mBound = false;
            }
            unregisterReceiver(vanReceiver);
        }

    }

    // ServiceConnection 인터페이스를 구현하는 객체를 생성한다.
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = new Messenger(service);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
            mBound = false;
        }
    };

    public void modUserAction(View button) {
        Logger.d(TAG, "modUserAction");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), UserModActivity.class);
                intent.putExtra("userData", selectedUserData.toString());
                startActivityForResult(intent, MOD_USER_RESULT);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }, 0);
    }

    private void bootstrap() {
        Logger.d(TAG, "bootstrap");
        if (pickupItems == null) {
            pickupItems = new JSONArray();
        }
        bucketListView = (ListView) findViewById(R.id.listView);
        try {
            elementAdapter = new TableLocalPickupElementListViewAdapter(getApplicationContext(), R.layout.pickitem_list_item, arrangeElement(pickupItems));
            elementAdapter.notifyDataSetChanged();
            bucketListView.setAdapter(elementAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        checkEmpty();
        initialize();

        //showFragment(FRAGMENT_BLANK);

        aq.id(R.id.btn_submit).clicked(this, "submitItems");
        aq.id(R.id.btn_back).clicked(this, "backStepAction");
        aq.id(R.id.btn_mod_tag).clicked(this, "modTagAction");

    }

    public void modTagAction(View button) {
        Logger.d(TAG, "modTagAction");
        Intent pickIntent = new Intent(getApplicationContext(), PickupItemTagMod.class);
        pickIntent.putExtra("initTagNo", tagNo);
        pickIntent.putExtra("initTagLength", tagLength);
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, MOD_TAG);
    }

    public void backStepAction(View button) {
        Logger.d(TAG, "backStepAction");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
        alertDialogBuilder.setCancelable(true).setMessage("세탁물 등록이 완료되지 않았습니다.\n중단하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    boolean isFirstExist = false;

    private JSONArray arrangeElement(JSONArray prevData) throws JSONException {
        Logger.d(TAG, "arrangeElement");
        JSONArray jsonData = prevData;
        Log.e("arrangeElement", jsonData.toString());

        JSONArray tempData = new JSONArray();

        for (int i = 0; i < jsonData.length(); i++) {
            int res = (tagNo + i) % (int) Math.pow(10, tagLength);
            if (res == 0) {
                isFirstExist = true;
            }
        }
        for (int i = 0; i < jsonData.length(); i++) {
            JSONObject json = new JSONObject();
            json.put("step1index", jsonData.getJSONObject(i).getInt("step1index"));
            json.put("step2index", jsonData.getJSONObject(i).getInt("step2index"));
            json.put("itemId", jsonData.getJSONObject(i).getInt("itemId"));
            json.put("rCount", jsonData.getJSONObject(i).getInt("rCount"));

            int res = (tagNo + i) % (int) Math.pow(10, tagLength);
            if (res < 100 && isFirstExist == true) {
                json.put("tagId", res + 1);
            } else {
                json.put("tagId", res);
            }

            json.put("itemTitle", jsonData.getJSONObject(i).getString("itemTitle"));
            json.put("sp", jsonData.getJSONObject(i).getInt("sp"));
            json.put("laundryPrice", jsonData.getJSONObject(i).getInt("laundryPrice"));
            json.put("repairPrice", jsonData.getJSONObject(i).getInt("repairPrice"));
            json.put("additionalPrice", jsonData.getJSONObject(i).getInt("additionalPrice"));
            json.put("isService", jsonData.getJSONObject(i).getString("isService"));
            json.put("isReLaundry", jsonData.getJSONObject(i).getString("isReLaundry"));
            json.put("storeMessage", jsonData.getJSONObject(i).getString("storeMessage"));

            json.put("confirmPrice", jsonData.getJSONObject(i).getInt("confirmPrice"));
            json.put("qty", jsonData.getJSONObject(i).getInt("qty"));
            json.put("itemColor", jsonData.getJSONObject(i).getString("itemColor"));
            json.put("laundry", jsonData.getJSONObject(i).getString("laundry"));
            json.put("repair", jsonData.getJSONObject(i).getJSONArray("repair"));
            json.put("additional", jsonData.getJSONObject(i).getJSONArray("additional"));
            json.put("part", jsonData.getJSONObject(i).getJSONArray("part"));
            json.put("photo", jsonData.getJSONObject(i).getJSONArray("photo"));

            tempData.put(json);
        }

        return tempData;
    }

    private void goOutputScreen() {
        Logger.d(TAG, "goOutputScreen");
        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), LocalOrderOutputActivity.class);
        intent.putExtra("orderId", selectedOrderId + "");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("userData", selectedUserData.toString());
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    public void submitItems(View button) {
        Logger.d(TAG, "submitItems");
        /*
        TODO
        현장 세탁물 등록 API 연동
         */
        if (pickupItems.length() == 0 || pickupItems == null) {
            DUtil.alertShow(this, "세탁물이 등록되지않아서 저장할수 없습니다.");
            return;
        }

        try {

            RequestParams params = new RequestParams();
            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            params.put("userId", selectedUserData.getString("userId"));
            params.put("addressSeq", selectedUserData.getString("addressSeq"));
            params.put("priceType", "101");
            params.put("items", arrangeElement(pickupItems).toString());
            params.put("k", 1);
            NetworkClient.post(Constants.ADD_ORDER_DIRECT, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                    // TODO Auto-generated method stub
                    DUtil.Log(Constants.ADD_ORDER_DIRECT, error.getMessage());
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                    String result;
                    try {
                        result = new String(data, "UTF-8");
                        DUtil.Log(Constants.ADD_ORDER_DIRECT, result);
                        //dismissDialog();

                        JSONObject jsondata = new JSONObject(result);

                        if ("S0000".equals(jsondata.getString("resultCode"))) {
                            ToastUtility.show(getApplicationContext(), "물품이 정상적으로 등록되었습니다", Toast.LENGTH_LONG);

                            if ("TRUE".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.IS_CONNECTED))) {
                                //completedOrderId = jsondata.getInt("data");
                                getOrderInfoForPrint(jsondata.getInt("data"));
                            } else {
                                goOutputScreen();
                            }


                        } else {
                            ToastUtility.show(getApplicationContext(), jsondata.getString("message"), Toast.LENGTH_LONG);
                        }
                    } catch (UnsupportedEncodingException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getOrderInfoForPrint(int _orderId) {
        Logger.d(TAG, "getOrderInfoForPrint");
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        selectedOrderId = _orderId;
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_DETAIL, result);

                    JSONObject resultJson = new JSONObject(result);

                    if ("S0000".equals(resultJson.getString("resultCode"))) {
                        if (resultJson.isNull("data") || resultJson.getJSONObject("data") == null) {
                            //
                            //aq.id(R.id.result_area).visible();
                            //aq.id(R.id.empty).visible();
                        } else {

                            JSONObject jsondata = resultJson.getJSONObject("data");
                            currentJsonData = jsondata;

                            currentOrderId = jsondata.getInt("orderId");

                            aq.id(R.id.pick_count_text).text("수거수량 " + jsondata.getInt("pickupQuantity") + "개");
                            aq.id(R.id.totalprice_text).text(MZValidator.toNumFormat(jsondata.getInt("deliveryPrice")) + " 원");

                            pickupItemsV2 = jsondata.getJSONArray("pickupItemsV2");

                            printReceipt();
                        }

                    } else {
                        aq.id(R.id.empty).visible();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    String currentVanType;

    private void printReceipt() {
        Logger.d(TAG, "printReceipt");
        currentVanType = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.VAN_TYPE);
        if (currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)) {

        } else if (currentVanType.equals(VanType.FIRSTDATA)) {
            try {
                String printString = "";

                printString += "0E0B12" + UnicodeFormatter.stringToHex("세탁물 내역서", "euc-kr") + "0A";
                printString += "0A";
                printString += "0A";
                printString += "0A";
                printString += "0F14" + UnicodeFormatter.stringToHex("고객명 : " + selectedUserData.getString("userName") + " (주문번호 : " + selectedOrderId + ")", "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("연락처 : " + MZValidator.validTelNumber(selectedUserData.getString("phone")), "euc-kr") + "0A";

                printString += UnicodeFormatter.stringToHex("주문일시 : " + TimeUtil.convertTimestampToString(currentJsonData.getLong("registerDateApp")), "euc-kr") + "0A";
                //printString += UnicodeFormatter.stringToHex("배송예정일 : 2016-07-06 오후 5:19:33", "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("주소 : " + selectedUserData.getString("address1") + " " + selectedUserData.getString("address2"), "euc-kr") + "0A";
                printString += "0A";

                printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                printString += UnicodeFormatter.stringJustifyToHex("TAG No.  품목", "금액", 44) + "0A";
                printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";

                for (int i = 0; i < pickupItemsV2.length(); i++) {
                    JSONObject item = pickupItemsV2.getJSONObject(i);

                    String isRepair = (item.getJSONArray("repair").length() > 0) ? "수선" : null;
                    String isAdditional = (item.getJSONArray("additional").length() > 0) ? "추가기술" : null;
                    String isPart = (item.getJSONArray("part").length() > 0) ? "부속품" : null;
                    String isService = item.getString("isService");
                    String isReLaundry = item.getString("isReLaundry");

                    int sp = 0;
                    if (item.isNull("sp")) {
                        sp = item.getInt("itemSP");
                    } else {
                        sp = item.getInt("sp");
                    }

                    String itemTitle = item.getString("itemTitle");
                    itemTitle = itemTitle.replace("\n", "");

                    switch (sp) {
                        case 1:
                            itemTitle += "(일반)";
                            break;
                        case 2:
                            itemTitle += "(명품)";
                            break;
                        case 3:
                            itemTitle += "(아동)";
                            break;
                    }

                    if (isService != null) {
                        if ("Y".equals(isService)) {
                            itemTitle += " -무료서비스";
                        }
                    }
                    if (isReLaundry != null) {
                        if ("Y".equals(isReLaundry)) {
                            itemTitle += " -재세탁";
                        }
                    }

                    String itemName = MZValidator.getTagFormmater(pickupItemsV2.getJSONObject(i).getInt("tagId"), 4) + "    " + itemTitle;
                    String itemPrice = MZValidator.toNumFormat(item.getInt("confirmPrice"));
                    printString += UnicodeFormatter.stringJustifyToHex(itemName, itemPrice, 44) + "0A";
                    printString += "0A";

                    if (isRepair != null) {
                        for (int j = 0; j < item.getJSONArray("repair").length(); j++) {
                            String repairPrice = MZValidator.toNumFormat(item.getJSONArray("repair").getJSONObject(j).getInt("additionalPrice"));
                            printString += UnicodeFormatter.stringJustifyToHex("         " + "+ 수선 :" + item.getJSONArray("repair").getJSONObject(j).getString("additionalItemTitle"), "(" + repairPrice + ")", 44) + "0A";
                        }
                    }
                    if (isAdditional != null) {
                        for (int j = 0; j < item.getJSONArray("additional").length(); j++) {
                            String addPrice = MZValidator.toNumFormat(item.getJSONArray("additional").getJSONObject(j).getInt("additionalPrice"));
                            printString += UnicodeFormatter.stringJustifyToHex("         " + "+ 기술료 :" + item.getJSONArray("additional").getJSONObject(j).getString("additionalItemTitle"), "(" + addPrice + ")", 44) + "0A";
                        }
                    }
                    if (isPart != null) {
                        String parts = "";
                        for (int j = 0; j < item.getJSONArray("part").length(); j++) {
                            if (j == 0) {
                                parts += item.getJSONArray("part").getJSONObject(j).getString("partTitle");
                            } else {
                                parts += ", " + item.getJSONArray("part").getJSONObject(j).getString("partTitle");
                            }
                        }
                        printString += UnicodeFormatter.stringToHex("         " + "+ 부속품 :" + parts, "euc-kr") + "0A";
                    }

                    if (item.getJSONArray("repair").length() + item.getJSONArray("additional").length() + item.getJSONArray("part").length() > 0) {
                        printString += "0A";
                    }

                }

        /*
            현장접수는 배송비 추가가 없음
         */
//                if(currentJsonData.getInt("deliveryPrice") < 20000){
//                    printString += "0A";
//                    printString += UnicodeFormatter.stringJustifyToHex("         배송비 (20,000원 이하)","2,000", 47) + "0A";
//                }

                printString += "14" + UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                //printString += "13"+UnicodeFormatter.stringJustifyToHex("총 수량 :",pickupItemsV2.length()+"개", 44) ;
                printString += "14" + UnicodeFormatter.stringJustifyToHex("총 수량 :", pickupItemsV2.length() + "개", 44);
                printString += "0A";
                Log.e("총 수량 :", pickupItemsV2.length() + "개");
                printString += "14" + UnicodeFormatter.stringJustifyToHex("총 금액 :", MZValidator.toNumFormat(currentJsonData.getInt("deliveryPrice")) + "원", 44);
                printString += "0A";
                Log.e("총 금액 :", MZValidator.toNumFormat(currentJsonData.getInt("deliveryPrice")) + "원");
                printString += "14" + UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr") + "0A";
                printString += "0A";

                Log.e("storeName", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_NAME));
                Log.e("STORE_PHONE", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_PHONE));

                printString += "14" + UnicodeFormatter.stringToHex(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_NAME), "euc-kr") + "0A";
                printString += "14" + UnicodeFormatter.stringToHex(MZValidator.validTelNumber(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_PHONE)), "euc-kr") + "0A";
                printString += "0A";
                printString += "14" + UnicodeFormatter.stringToHex("저희 세탁소를 찾아주셔서 감사합니다.", "euc-kr") + "0A";
                printString += "14" + UnicodeFormatter.stringToHex("고객님의 소중한 의류를 성심껏 관리하겠습니다.", "euc-kr") + "0A";
                printString += "0A";
                printString += "0A";

                String printStringAgain = printString;
                printString += "11";
                printString += printStringAgain;
                printString += "11";

                printStringExcute(printString);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (currentVanType.equals(VanType.SMARTRO)) {
            if (mBound) {
                Message msg = Message.obtain(null, CatManager.PRINT_REQUEST, 0, 0);

                String contentString = "1c";
                String printString = "";

                try {
                    contentString += UnicodeFormatter.stringToHex("A1");
                    Log.w("A1", contentString);
                    contentString += "1c";
                    //////////////////
                    printString += "17" + UnicodeFormatter.stringToHex("      세탁물 내역서", "euc-kr") + "18" + "0A";
                    printString += "0A";
                    printString += "0A";
                    printString += "0A";
                    printString += UnicodeFormatter.stringToHex("고객명 : " + selectedUserData.getString("userName") + " (주문번호 : " + selectedOrderId + ")", "euc-kr") + "0A";
                    printString += UnicodeFormatter.stringToHex("연락처 : " + MZValidator.validTelNumber(selectedUserData.getString("phone")), "euc-kr") + "0A";

                    printString += UnicodeFormatter.stringToHex("주문일시 : " + TimeUtil.convertTimestampToString(currentJsonData.getLong("registerDateApp")), "euc-kr") + "0A";
                    //printString += UnicodeFormatter.stringToHex("배송예정일 : 2016-07-06 오후 5:19:33", "euc-kr") + "0A";
                    printString += UnicodeFormatter.stringToHex("주소 : " + selectedUserData.getString("address1") + " " + selectedUserData.getString("address2"), "euc-kr") + "0A";
                    printString += "0A";

                    printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr") + "0A";
                    printString += UnicodeFormatter.stringJustifyToHex("TAG No.  품목", "금액", 47) + "0A";
                    printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr") + "0A";
                    for (int i = 0; i < pickupItemsV2.length(); i++) {
                        JSONObject item = pickupItemsV2.getJSONObject(i);

                        String isRepair = (item.getJSONArray("repair").length() > 0) ? "수선" : null;
                        String isAdditional = (item.getJSONArray("additional").length() > 0) ? "추가기술" : null;
                        String isPart = (item.getJSONArray("part").length() > 0) ? "부속품" : null;
                        String isService = item.getString("isService");
                        String isReLaundry = item.getString("isReLaundry");

                        int sp = 0;
                        if (item.isNull("sp")) {
                            sp = item.getInt("itemSP");
                        } else {
                            sp = item.getInt("sp");
                        }

                        String itemTitle = item.getString("itemTitle");
                        itemTitle = itemTitle.replace("\n", "");

                        switch (sp) {
                            case 1:
                                itemTitle += "(일반)";
                                break;
                            case 2:
                                itemTitle += "(명품)";
                                break;
                            case 3:
                                itemTitle += "(아동)";
                                break;
                        }

                        if (isService != null) {
                            if ("Y".equals(isService)) {
                                itemTitle += " -무료서비스";
                            }
                        }
                        if (isReLaundry != null) {
                            if ("Y".equals(isReLaundry)) {
                                itemTitle += " -재세탁";
                            }
                        }

                        String itemName = MZValidator.getTagFormmater(pickupItemsV2.getJSONObject(i).getInt("tagId"), 4) + "    " + itemTitle;
                        String itemPrice = MZValidator.toNumFormat(item.getInt("confirmPrice"));
                        printString += UnicodeFormatter.stringJustifyToHex(itemName, itemPrice, 47) + "0A";
                        printString += "0A";

                        if (isRepair != null) {
                            for (int j = 0; j < item.getJSONArray("repair").length(); j++) {
                                String repairPrice = MZValidator.toNumFormat(item.getJSONArray("repair").getJSONObject(j).getInt("additionalPrice"));
                                printString += UnicodeFormatter.stringJustifyToHex("         " + "+ 수선 :" + item.getJSONArray("repair").getJSONObject(j).getString("additionalItemTitle"), "(" + repairPrice + ")", 47) + "0A";
                            }
                        }
                        if (isAdditional != null) {
                            for (int j = 0; j < item.getJSONArray("additional").length(); j++) {
                                String addPrice = MZValidator.toNumFormat(item.getJSONArray("additional").getJSONObject(j).getInt("additionalPrice"));
                                printString += UnicodeFormatter.stringJustifyToHex("         " + "+ 기술료 :" + item.getJSONArray("additional").getJSONObject(j).getString("additionalItemTitle"), "(" + addPrice + ")", 47) + "0A";
                            }
                        }
                        if (isPart != null) {
                            String parts = "";
                            for (int j = 0; j < item.getJSONArray("part").length(); j++) {
                                if (j == 0) {
                                    parts += item.getJSONArray("part").getJSONObject(j).getString("partTitle");
                                } else {
                                    parts += ", " + item.getJSONArray("part").getJSONObject(j).getString("partTitle");
                                }
                            }
                            printString += UnicodeFormatter.stringToHex("         " + "+ 부속품 :" + parts, "euc-kr") + "0A";
                        }

                        if (item.getJSONArray("repair").length() + item.getJSONArray("additional").length() + item.getJSONArray("part").length() > 0) {
                            printString += "0A";
                        }

                    }

                /*
                    현장접수는 배송비 추가가 없음
                 */
//                if(currentJsonData.getInt("deliveryPrice") < 20000){
//                    printString += "0A";
//                    printString += UnicodeFormatter.stringJustifyToHex("         배송비 (20,000원 이하)","2,000", 47) + "0A";
//                }

                    printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr") + "0A";
                    printString += "13" + UnicodeFormatter.stringJustifyToHex("총 수량 :", pickupItemsV2.length() + "개", 47) + "140A";
                    printString += "13" + UnicodeFormatter.stringJustifyToHex("총 금액 :", MZValidator.toNumFormat(currentJsonData.getInt("deliveryPrice")) + "원", 47) + "140A";
                    printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr") + "0A";
                    printString += "0A";

                    String page1PrintString = printString;

                    printString += "0A";
                    printString += UnicodeFormatter.stringToHex("(상점 보관용)", "euc-kr") + "0A";

                    // 같은내용 2장 출력
                    printString += "1E1E1E" + page1PrintString;

                    printString += "13" + UnicodeFormatter.stringToHex(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_VIRTUAL_NAME), "euc-kr") + "140A";
                    printString += UnicodeFormatter.stringToHex(MZValidator.validTelNumber(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_VIRTUAL_PHONE)), "euc-kr") + "0A";
                    printString += "0A";
                    printString += UnicodeFormatter.stringToHex("저희 세탁소를 찾아주셔서 감사합니다.", "euc-kr") + "0A";
                    printString += UnicodeFormatter.stringToHex("고객님의 소중한 의류를 성심껏 관리하겠습니다.", "euc-kr") + "0A";
//                printString += "0A";
//                printString += UnicodeFormatter.stringToHex("우리동네 모바일 세탁소 리화이트", "euc-kr") + "0A";
//                printString += UnicodeFormatter.stringToHex("www.rewhite.me", "euc-kr") + "0A";
                    printString += "0A";

                    int storeId = Integer.parseInt(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_ID));
                    printString += "11" + UnicodeFormatter.stringToHex(BarcodeStringUtil.getLocalOrderBarcodeString(storeId, currentOrderId), "euc-kr") + "120A";
                    printString += UnicodeFormatter.stringToHex("(고객 전달용)", "euc-kr") + "0A";
                    printString += "0A";
                    printString += "0A";

                    printString += "1D1D1D";
                    //////////////////
                    String hexSize = UnicodeFormatter.stringToHex(String.format(Locale.KOREA, "%04d", (printString.length() / 2) + 4));
                    contentString += hexSize + printString;
                    contentString += "1c";
                    contentString += "1c";
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
                byte[] content = new byte[contentString.length() / 2];
                content = UnicodeFormatter.hexToByteArray(contentString);

                try {
                    Bundle b = new Bundle();
                    b.putByteArray("content", content); // for example
                    msg.setData(b);
                    mService.send(msg);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            } else {
                goOutputScreen();
                //DUtil.alertShow(this, "PRINT가 연결되지 않았습니다. 연결상태를 확인해주세요.");
            }
        }
    }

    private void printStringExcute(String _hex) throws Exception {
        Logger.d(TAG, "printStringExcute");
        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        iProcID = FDK_Module.Creat();

        //FDK_Module.Input(iProcID, "DATA", "123123");
        FDK_Module.Input(iProcID, "DATA", _hex);

        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/Print/FreeStyle_F3");
        if (0 == iRet) {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        } else if (-1000 == iRet) {
            strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        } else {
            strMsg = "에러[" + iRet + "]\r\n";
            strData = "";
            strData = FDK_Module.Output(iProcID, "ErrorInfo");
            strMsg = strMsg + strData;
        }
        //ShowMsg(strMsg);

        //Output Sample
        if (0 == iRet || -1000 == iRet) {
            strMsg = "Output List :\r\n";
            strMsg += "Ascii : " + FDK_Module.Output(iProcID, "Ascii") + "\r\n";
            strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
            strMsg += "AsciiLen : " + FDK_Module.Output(iProcID, "AsciiLen") + "\r\n";
            strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
            //ShowMsg(strMsg);
            if (0 == iRet) {
                goOutputScreen();
            } else {
                // 프린터 출력실패 : 재인쇄
            }
        }

        iRet = FDK_Module.Destroy(iProcID);
    }

    private void checkEmpty() {
        Logger.d(TAG, "checkEmpty");
        if (pickupItems.length() > 0) {
            //aq.id(R.id.empty_view).gone();
            aq.id(R.id.listView).visible();
            //aq.id(R.id.guide_bottom).visible();
            //aq.id(R.id.top_tag_info).gone();
            aq.id(R.id.empty_screen).gone();
        } else {
            //aq.id(R.id.empty_view).visible();
            aq.id(R.id.listView).gone();
            //aq.id(R.id.guide_bottom).gone();
            //aq.id(R.id.top_tag_info).visible();
            aq.id(R.id.empty_screen).visible();
        }
    }

    public void refreshList() {
        Logger.d(TAG, "refreshList");
        checkEmpty();
        try {
            elementAdapter = new TableLocalPickupElementListViewAdapter(getApplicationContext(), R.layout.tablet_pickitem_list_item, arrangeElement(pickupItems));
            Log.i("refresh added", pickupItems.toString());
            elementAdapter.notifyDataSetChanged();
            bucketListView.setAdapter(elementAdapter);
            //bucketListView.invalidate();

            aq.id(R.id.item_count).text(pickupItems.length() + "");
            aq.id(R.id.price_label).text(getTotalPrice());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getTotalPrice() {
        Logger.d(TAG, "getTotalPrice");
        int price = 0;
        for (int i = 0; i < pickupItems.length(); i++) {
            try {
                price += pickupItems.getJSONObject(i).getInt("confirmPrice");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return MZValidator.toNumFormat(price);
    }

    public void deleteSelected(final int position) {
        Logger.d(TAG, "deleteSelected");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());
        alertDialogBuilder.setCancelable(true).setMessage("등록하신 품목을 삭제하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        JSONArray temp = new JSONArray();
                        for (int i = 0; i < pickupItems.length(); i++) {
                            if (position != i) {
                                try {
                                    temp.put(pickupItems.getJSONObject(i));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        pickupItems = temp;
                        refreshList();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void copySelected(int position) {
        Logger.d(TAG, "copySelected");
        //pickupItems.getJSONObject(position)
        Intent pickIntent = new Intent(getApplicationContext(), PickupItemCopyCount.class);
        try {
            pickIntent.putExtra("pickedItem", pickupItems.getJSONObject(position).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, COPY_ELEMENT);
    }

    public void itemSelected(int position) {
        Logger.d(TAG, "itemSelected");
        try {
            pickedAdditionalArray = pickupItems.getJSONObject(position).getJSONArray("additional");
            pickedPartsArray = pickupItems.getJSONObject(position).getJSONArray("part");
            pickedRepairArray = pickupItems.getJSONObject(position).getJSONArray("repair");
            pickedRelaundry = pickupItems.getJSONObject(position).getString("isReLaundry");
            pickedService = pickupItems.getJSONObject(position).getString("isService");
            imagePickValue = pickupItems.getJSONObject(position).getJSONArray("photo");
            Step1Index = pickupItems.getJSONObject(position).getInt("step1index");
            Step2Index = pickupItems.getJSONObject(position).getInt("step2index");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(FRAGMENT_DETAIL);

        fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragment = new TabletPickupDetailFragmentV2Local();

        Bundle args = new Bundle();
        args.putString("pickupItems", pickupItems.toString());
        args.putInt("pickupItemsPosition", position);

        fragment.setArguments(args);

        //ft.setCustomAnimations(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        ft.replace(R.id.content_frame, fragment, FRAGMENT_DETAIL);
        //ft.addToBackStack(fragmentName);
        ft.commitAllowingStateLoss();
        //ft.commit();

    }

    public void addActionStatement() {
        Logger.d(TAG, "addActionStatement");
        Step1Name = "";
        Step1Index = 0;
        Step2Name = "";
        Step2Index = 0;
        pickedRepairArray = new JSONArray();
        pickedAdditionalArray = new JSONArray();
        pickedPartsArray = new JSONArray();

        colorPickValue = "";
        imagePickValue = new JSONArray();

        aq.id(R.id.empty_screen).gone();
        showFragment(FRAGMENT_CATEGORY);
    }

    public void addAction(View button) {
        Logger.d(TAG, "addAction");
        if (!isInitialized) {
            return;
        }

        addActionStatement();

        /*
        Intent pickIntent = new Intent(mCtx, PickupItemActivity.class);
        pickIntent.putExtra("pickupMode", PickupType.PICKUPTYPE_CLEAN);
        pickIntent.putExtra("repairData", repairArray.toString());
        pickIntent.putExtra("additionalData", additionalArray.toString());
        pickIntent.putExtra("partsData", partsArray.toString());
        pickIntent.putExtra("colorsData", colorsArray.toString());
        pickIntent.putExtra("priceData", priceArray.toString());
        pickIntent.putExtra("isFirstExist", isFirstExist);

        pickIntent.putExtra("initTagNo", tagNo);
        pickIntent.putExtra("initTagLength", tagLength);
        pickIntent.putExtra("bucketIndex", pickupItems.length());
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, ADD_ELEMENT);
*/
        //bucketIndex++;
    }

    private void initialize() {
        Logger.d(TAG, "initialize");
        getPickupInitializeData();
    }

    private void getPickupInitializeData() {
        Logger.d(TAG, "getPickupInitializeData");
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.PICKUP_INITIALIZE_DATA, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.PICKUP_INITIALIZE_DATA, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.PICKUP_INITIALIZE_DATA, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        JSONObject retData = jsondata.getJSONObject("data");
                        repairArray = retData.getJSONArray("repair");
                        additionalArray = retData.getJSONArray("additional");
                        partsArray = retData.getJSONArray("part");
                        colorsArray = retData.getJSONArray("color");

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getLastTag();
                            }
                        }, 200);

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private void getLastTag() {
        Logger.d(TAG, "getLastTag");
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.GET_LASTEST_TAG_NO, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GET_LASTEST_TAG_NO, error.getMessage());
                ProgressDialogHelper.dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_LASTEST_TAG_NO, result);
                    ProgressDialogHelper.dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        JSONArray retData = jsondata.getJSONArray("data");
                        if (prevTagNo == -1) {
                            tagNo = retData.getInt(0);
                        } else {
                            tagNo = prevTagNo;
                        }

                        tagLength = retData.getInt(1);

                        aq.id(R.id.current_tag_no).text(MZValidator.getTagFormmater(tagNo, tagLength));

                        if (pickupItems.length() > 0) {
                            refreshList();
                        }

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getPriceTable();
                            }
                        }, 200);

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private void getPriceTable() {
        Logger.d(TAG, "getPriceTable");
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));

        if (pickupMode == PickupType.PICKUPTYPE_CLEAN) {
            params.put("mode", "G");
        } else if (pickupMode == PickupType.PICKUPTYPE_TODAY) {
            params.put("mode", "T");
        } else if (pickupMode == PickupType.PICKUPTYPE_TOMORROW) {
            params.put("mode", "N");
        } else if (pickupMode == PickupType.PICKUPTYPE_B2B_HANDYS) {
            params.put("mode", "B");
        }

        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_PRICE_TABLE_V2, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_PRICE_TABLE_V2, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_PRICE_TABLE_V2, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        JSONArray obj = jsondata.getJSONArray("data");
                        priceArray = new JSONArray();
                        for (int i = 0; i < obj.length(); i++) {
                            if (obj.getJSONObject(i).getJSONArray("items").length() > 0) {
                                priceArray.put(obj.getJSONObject(i));
                            }
                        }
                        //priceArray = jsondata.getJSONArray("data");
                        isInitialized = true;


                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void submitElement(JSONObject element, int tagNumber, int position) {
        Logger.d(TAG, "submitElement");
        try {
            JSONArray tempItems = new JSONArray();
            if (pickupItems != null) {
                for (int i = 0; i < pickupItems.length(); i++) {
                    if (bucketIndex == i) {
                        tempItems.put(element);
                    } else {
                        tempItems.put(pickupItems.getJSONObject(i));
                    }
                }
            } else {
                tempItems.put(element);
            }

            try {
                Log.e("Add Element", element.toString());
                JSONObject getObj = element;
                if (pickupItems == null) {
                    pickupItems = new JSONArray();
                    pickupItems.put(getObj);
                } else {

                    int pos = position;

                    if (pickupItems.length() > pos) {
                        int tag = pickupItems.getJSONObject(pos).getInt("tagId");
                        int itemId = pickupItems.getJSONObject(pos).getInt("itemId");
                        JSONArray temp = new JSONArray();
                        if (tag == tagNumber && itemId == getObj.getInt("itemId")) {
                            for (int i = 0; i < pickupItems.length(); i++) {
                                if (pos == i) {
                                    temp.put(getObj);
                                } else {
                                    temp.put(pickupItems.getJSONObject(i));
                                }
                            }
                            pickupItems = temp;
                        } else {
                            pickupItems.put(getObj);
                        }
                    } else {
                        pickupItems.put(getObj);
                    }
                }

                bucketIndex = pickupItems.length();
                //arrangeElement(pickupItems);

                refreshList();

                showFragment(FRAGMENT_BLANK);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//
//            Intent resultData = new Intent();
//            resultData.putExtra("entire", tempItems.toString());
//            resultData.putExtra("element", element.toString());
//            resultData.putExtra("tagNumber", tagNumber);
//            resultData.putExtra("position", position);
//            setResult(Activity.RESULT_OK, resultData);
//            //finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void showFragment(String fragmentName) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);
        showFragment(fragmentName, null, null);
    }

    String transitionName = "";

    public void endScreenJump() {
        showFragment(FRAGMENT_DETAIL);
    }

    public void showFragment(String fragmentName, View sharedElement, Bundle args) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(fragmentName);

        if (FRAGMENT_CATEGORY.equals(fragmentName)) {
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletPickupItemFragmentV2();
        } else if (FRAGMENT_SUB_CATEGORY.equals(fragmentName)) {
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletPickupItemSubCategoryFragmentV2();
        } else if (FRAGMENT_DETAIL.equals(fragmentName)) {
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletPickupDetailFragmentV2Local();
        } else if (FRAGMENT_BLANK.equals(fragmentName)) {
            if (pickupItems.length() > 0) {
                aq.id(R.id.listView).visible();
                aq.id(R.id.empty_screen).gone();
            } else {
                aq.id(R.id.listView).gone();
                aq.id(R.id.empty_screen).visible();
            }
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletPickupItemBlankFragmentV2();
        }

        Log.e(TAG, "[TabletPickupItemFragmentV2] Create : " + fragmentName);

        if (null != args) {
            fragment.setArguments(args);
        }

        //ft.setCustomAnimations(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        ft.replace(R.id.content_frame, fragment, fragmentName);
        //ft.addToBackStack(fragmentName);
        ft.commitAllowingStateLoss();
        //ft.commit();

    }


    public void selectPricePicker() {
        Logger.d(TAG, "selectPricePicker");
        Intent pickIntent = new Intent(this, PickupItemPriceMod.class);
        pickIntent.putExtra("pickedPrice", pickedPrice);
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, PICK_PRICE);
    }

    public void selectColorPicker() {
        Logger.d(TAG, "selectColorPicker");
        Intent pickIntent = new Intent(this, PickupItemColorPicker.class);
        pickIntent.putExtra("pickedColor", colorPickValue);
        pickIntent.putExtra("data", colorsArray.toString());
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, PICK_COLOR);

    }

    public void selectRepairPicker() {
        Logger.d(TAG, "selectRepairPicker");
        Intent pickIntent = new Intent(this, PickupItemRepairPicker.class);
        pickIntent.putExtra("pickedRepair", pickedRepairArray.toString());
        pickIntent.putExtra("data", repairArray.toString());
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, PICK_REPAIR);

    }

    public void selectAdditionalPicker() {
        Logger.d(TAG, "selectAdditionalPicker");
        Intent pickIntent = new Intent(this, PickupItemAdditionalPicker.class);
        pickIntent.putExtra("pickedAdditional", pickedAdditionalArray.toString());
        pickIntent.putExtra("data", additionalArray.toString());
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, PICK_ADDITIONAL);

    }

    public void selectPartsPicker() {
        Logger.d(TAG, "selectPartsPicker");
        Intent pickIntent = new Intent(this, PickupItemPartsPicker.class);
        pickIntent.putExtra("pickedParts", pickedPartsArray.toString());
        pickIntent.putExtra("data", partsArray.toString());
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, PICK_PARTS);

    }

    SharedPreferences preferences;
    public static final String[] INITIAL_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    public static final String[] STORAGE_PERMS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    public static final int INITIAL_REQUEST = 1337;

    public static final int STORAGE_REQUEST = INITIAL_REQUEST + 6;

    private static final int REQ_CODE_PICK_IMAGE = 0;
    public String TEMP_PHOTO_FILE = "upload.jpg"; // 임시 저장파일
    private final int CAMERA_CAPTURE = 1;
    private final int CROP_PIC = 2;
    private Uri picUri;
    public JSONArray imagePickValue;

    public void setPhotoFileName(String filename) {
        Logger.d(TAG, "setPhotoFileName");
        TEMP_PHOTO_FILE = filename;
    }

    public String getPhotoFileName() {
        Logger.d(TAG, "getPhotoFileName");
        return TEMP_PHOTO_FILE;
    }

    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Logger.d(TAG, "onRequestPermissionsResult");
        switch (requestCode) {

            case STORAGE_REQUEST:
                if (canAccessStorage()) {
                    addIntentCall();
                } else {
                    Toast.makeText(this, "사진첩에 접근할수 있는 권한이 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public boolean canAccessStorage() {
        Logger.d(TAG, "canAccessStorage");
        return (hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        Logger.d(TAG, "hasPermission");
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    public static boolean isIntentAvailable(Context context, String action) {
        Logger.d(TAG, "isIntentAvailable");
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent(action);
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    public void addIntentCall() {
        Logger.d(TAG, "addIntentCall");
        Display display = getWindowManager().getDefaultDisplay();
        int mOutputX = display.getWidth();

        setPhotoFileName("upload_" + String.valueOf
                (System.currentTimeMillis()) + ".jpg");

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //File file = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getTempFile()));
        //intent.putExtra(MediaStore.EXTRA_OUTPUT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString());
        // requestCode지정해서 인텐트 실행
        startActivityForResult(intent, CAMERA_CAPTURE);
    }

    /**
     * this function does the crop operation.
     */
    private void performCrop() {
        Logger.d(TAG, "performCrop");
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File f = new File(path, TEMP_PHOTO_FILE);
            cropIntent.setDataAndType(Uri.fromFile(f), "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            //cropIntent.putExtra("outputX", 200);
            //cropIntent.putExtra("outputY", 200);
            // retrieve data on return
            cropIntent.putExtra("scale", true);
            //cropIntent.putExtra("return-data", true);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, CROP_PIC);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            Toast toast = Toast
                    .makeText(this, "This device doesn't support the crop action!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.d(TAG, "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICK_COLOR:
                    int colorIndex = data.getIntExtra("index", 0);
                    String colorValue = data.getStringExtra("value");
                    String colorName = data.getStringExtra("name");
                    Log.e("color", colorIndex + " / " + colorValue + " / " + colorName);
                    //colorPickValue = colorValue;

                    TabletPickupDetailFragmentV2Local fragment1 = (TabletPickupDetailFragmentV2Local) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if (fragment1 != null) {
                        fragment1.setColorPickValue(colorValue);
                    }

                    break;
                case PICK_REPAIR:
                    try {
                        JSONArray ret = new JSONArray(data.getStringExtra("value"));
                        Log.e("REPAIR_PARTS", ret.toString());

                        TabletPickupDetailFragmentV2Local fragment2 = (TabletPickupDetailFragmentV2Local) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                        if (fragment2 != null) {
                            fragment2.setRepairPickValue(ret);
                        }
                        /*
                        FragmentManager fm2 = getSupportFragmentManager();
                        String tag2 = fm2.getBackStackEntryAt(fm2.getBackStackEntryCount() - 1).getName();
                        if(LocalOrderInputActivity.FRAGMENT_DETAIL.equals(tag2)){
                            TabletPickupDetailFragmentV2Local fragment = (TabletPickupDetailFragmentV2Local)getSupportFragmentManager().findFragmentById(R.id.content_frame);
                            if(fragment != null){
                                fragment.setRepairPickValue(ret);
                            }

                        }*/

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case PICK_PRICE:
                    int retp = data.getIntExtra("value", 0);
                    Log.e("PICK_PRICE", retp + "");

                    TabletPickupDetailFragmentV2Local fragment3 = (TabletPickupDetailFragmentV2Local) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if (fragment3 != null) {
                        fragment3.setPricePickValue(retp);
                    }
                    break;
                case PICK_PARTS:
                    try {
                        JSONArray ret = new JSONArray(data.getStringExtra("value"));
                        Log.e("PICK_PARTS", ret.toString());

                        TabletPickupDetailFragmentV2Local fragment4 = (TabletPickupDetailFragmentV2Local) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                        if (fragment4 != null) {
                            fragment4.setPartsPickValue(ret);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    break;
                case PICK_ADDITIONAL:
                    try {
                        JSONArray ret = new JSONArray(data.getStringExtra("value"));
                        Log.e("ADDITIONAL_PARTS", ret.toString());

                        TabletPickupDetailFragmentV2Local fragment5 = (TabletPickupDetailFragmentV2Local) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                        if (fragment5 != null) {
                            fragment5.setAdditionalPickValue(ret);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case CAMERA_CAPTURE:
                    try {
                        /*the user's device may not support cropping*/
                        Log.e("CAMERA_CAPTURE", "CAMERA_CAPTURE");
                        performCrop();
                        //cropCapturedImage(Uri.fromFile(file));
                    } catch (ActivityNotFoundException aNFE) {
                        //display an error message if user device doesn't support
                        String errorMessage = "Sorry - your device doesn't support the crop action!";
                        Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                    String filePath = null;

                    File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                    File f = new File(path, TEMP_PHOTO_FILE);
                    filePath = f.getAbsolutePath();
                    Log.i("imageDATA PATH :::::", "Chosen path = " + filePath);
                    Log.i("File :::::", "getExternalStoragePublicDirectory = " + TEMP_PHOTO_FILE);
                    Log.i("imageDATA PATH :::::", "Chosen path = " + filePath);

                    BitmapFactory.Options bfo = new BitmapFactory.Options();
                    //bfo.inInputShareable = true;
                    //bfo.inDither=false;
                    //bfo.inTempStorage=new byte[16 * 1024];
                    //bfo.inPurgeable = true;
                    //bfo.inJustDecodeBounds = true;
                    // Get dimensions of image first (takes very little time)
                    //bfo.inJustDecodeBounds = true;
                    //bfo.inDither = false;
                    //bfo.inPreferredConfig = Bitmap.Config.RGB_565;
                    int size = 6;
                    Bitmap bmpAvator;

                    //OutOfMemoryError 날경우 이미지 사이즈 축소
                    while (true) {
                        try {
                            bfo.inSampleSize = size;
                            bmpAvator = BitmapFactory.decodeFile(filePath, bfo);

                            int degrees = GetExifOrientation(filePath);
                            // 회전한 이미지 취득
                            bmpAvator = GetRotatedBitmap(bmpAvator, degrees);
                            File fileCacheItem = new File(filePath);
                            OutputStream out = null;
                            try {
                                fileCacheItem.createNewFile();
                                out = new FileOutputStream(fileCacheItem);
                                bmpAvator.compress(Bitmap.CompressFormat.JPEG, 80, out);
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                try {
                                    out.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            HashMap<String, Object> reqParams = new HashMap<String, Object>();
                            //reqParams.put("filename", TEMP_PHOTO_FILE);
                            new TabletImageUploadTask2(this, preferences).execute(reqParams);


                            break;
                        } catch (OutOfMemoryError e) {
                            e.printStackTrace();
                            Log.i("inSampleSize :::::", "resize = " + size);
                            size++;
                        }
                    }
                    break;
                case CROP_PIC:
                    //Bundle extras = data.getExtras();
                    // get the cropped bitmap
                    //Bitmap thePic = extras.getParcelable("data");
                    Log.e("CROP_PIC", "CROP_PIC");


                    break;
                case MOD_TAG:

                    tagNo = data.getIntExtra("value", 0);
                    aq.id(R.id.current_tag_no).text(MZValidator.getTagFormmater(tagNo, tagLength));

                    break;
                case COPY_ELEMENT:
                    try {
                        Log.e("COPY_ELEMENT", data.getStringExtra("item"));
                        JSONObject getObj = new JSONObject(data.getStringExtra("item"));

                        if (pickupItems == null) {
                            pickupItems = new JSONArray();
                            pickupItems.put(getObj);
                        } else {
                            int count = data.getIntExtra("value", 0);
                            for (int i = 0; i < count; i++) {
                                JSONObject json = new JSONObject();
                                json.put("step1index", getObj.getInt("step1index"));
                                json.put("step2index", getObj.getInt("step2index"));
                                json.put("itemId", getObj.getInt("itemId"));
                                json.put("rCount", getObj.getInt("rCount"));
                                int res = (tagNo + (pickupItems.length() + 1)) % (int) Math.pow(10, tagLength);
                                json.put("tagId", res);
                                json.put("itemTitle", getObj.getString("itemTitle"));
                                json.put("sp", getObj.getInt("sp"));
                                json.put("laundryPrice", getObj.getInt("laundryPrice"));
                                json.put("repairPrice", getObj.getInt("repairPrice"));
                                json.put("additionalPrice", 0);
                                json.put("isService", "N");
                                json.put("isReLaundry", "N");
                                json.put("storeMessage", getObj.getString("storeMessage"));

                                json.put("confirmPrice", getObj.getInt("confirmPrice"));
                                json.put("qty", getObj.getInt("qty"));
                                json.put("itemColor", "");
                                json.put("laundry", getObj.getString("laundry"));
                                json.put("repair", getObj.getJSONArray("repair"));
                                json.put("additional", new JSONArray());
                                json.put("part", new JSONArray());
                                json.put("photo", new JSONArray());

                                pickupItems.put(json);
                            }
                        }

                        bucketIndex = pickupItems.length();

                        //pickupItems = arrangeElement(pickupItems);


                        refreshList();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case MOD_USER_RESULT:

                    try {
                        selectedUserData = new JSONObject(data.getStringExtra("userData"));

                        aq.id(R.id.nickname_text).text(selectedUserData.getString("userName"));
                        aq.id(R.id.address_text).text(selectedUserData.getString("address1") + " " + selectedUserData.getString("address2"));
                        aq.id(R.id.phone_text).text(MZValidator.validTelNumber(selectedUserData.getString("phone")));
                        aq.id(R.id.ordercount_text).text(selectedUserData.getString("orderCount"));
                        aq.id(R.id.recv_price).text(MZValidator.toNumFormat(selectedUserData.getInt("receivablePrice")) + " 원");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
            }
        }
    }

    private int GetExifOrientation(String filepath) {
        Logger.d(TAG, "GetExifOrientation");
        int degree = 0;
        ExifInterface exif = null;

        try {
            exif = new ExifInterface(filepath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (exif != null) {
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            if (orientation != -1) {
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        degree = 90;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        degree = 180;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        degree = 270;
                        break;
                }
            }
        }

        return degree;
    }

    private Bitmap GetRotatedBitmap(Bitmap bitmap, int degrees) {
        Logger.d(TAG, "GetRotatedBitmap");
        if (degrees != 0 && bitmap != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

            try {
                Bitmap b2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);

                if (bitmap != b2) {
                    bitmap.recycle();
                    bitmap = b2;
                }
            } catch (OutOfMemoryError e) {
                // 메모리 부족에러시, 원본을 반환
                e.printStackTrace();
            }
        }

        return bitmap;
    }

    public void uploadCompleted(String response) {
        Logger.d(TAG, "uploadCompleted");
        try {
            JSONObject json = new JSONObject(response);
            Log.i("uploadCompleted", response.toString());

            if ("S0000".equals(json.getString("resultCode"))) {
                DUtil.alertShow(this, "이미지 업로드가 완료되었습니다");
                String imgurl = json.getString("data");

                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                File f = new File(path, TEMP_PHOTO_FILE);
                if (f.exists()) {
                    f.delete();
                }
                //sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory()))); // 갤러리를 갱신하기 위해..

                TabletPickupDetailFragmentV2Local fragment = (TabletPickupDetailFragmentV2Local) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                if (fragment != null) {
                    fragment.setImagePicsValue(imgurl);
                }
                //
            } else {
                DUtil.alertShow(this, "이미지 업로드가 실패했습니다.");
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    void createExternalStoragePublicPicture() {
        Logger.d(TAG, "createExternalStoragePublicPicture");
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        //File file = new File(getCacheDir(), TEMP_PHOTO_FILE);

        try {
            // Make sure the Pictures directory exists.
            if (!path.exists()) {
                path.mkdirs();
            }

            InputStream is = getAssets().open("kakao_default_profile_image.png");// getResources().openRawResource(R.mipmap.kakao_default_profile_image);
            OutputStream os = new FileOutputStream(file);
            byte[] data = new byte[is.available()];
            is.read(data);
            os.write(data);
            is.close();
            os.close();

            MediaScannerConnection.scanFile(this, new String[]{file.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });
        } catch (IOException e) {
            // Unable to create file, likely because external storage is
            // not currently mounted.
            e.printStackTrace();
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }

    void deleteExternalStoragePublicPicture() {
        Logger.d(TAG, "deleteExternalStoragePublicPicture");
        //File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        //File file = new File(path, TEMP_PHOTO_FILE);
        //Environment.getDownloadCacheDirectory();
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        if (file.exists()) {
            file.delete();
        }
    }

    boolean hasExternalStoragePublicPicture() {
        Logger.d(TAG, "hasExternalStoragePublicPicture");
        //File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        //File file = new File(path, TEMP_PHOTO_FILE);
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        return file.exists();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("fileName", fileName);
    }

    String fileName;

    /**
     * 임시 저장 파일의 경로를 반환
     */
    private Uri getTempUri() {
        Logger.d(TAG, "getTempUri");
        //Uri mImageCaptureUri = Uri.fromFile(new File(this.getExternalCacheDir(), TEMP_PHOTO_FILE));
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        fileName = file.getAbsolutePath();
        return Uri.fromFile(file);
    }

    /**
     * 외장메모리에 임시 이미지 파일을 생성하여 그 파일의 경로를 반환
     */
    private File getTempFile() {
        Logger.d(TAG, "getTempFile");
        if (hasExternalStoragePublicPicture()) {
            Log.i("hasExternal", "HAVED");
            deleteExternalStoragePublicPicture();
            Log.i("deleteExternal", "DELETED");
        }
        Log.i("createExternal", "getTempFile()");
        createExternalStoragePublicPicture();
/*
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File f = new File(path, TEMP_PHOTO_FILE);
*/
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File f = new File(path, TEMP_PHOTO_FILE);

        try {
            f.createNewFile(); // 외장메모리에 temp.jpg 파일 생성
        } catch (IOException e) {
            Log.e("cklee", "fileCreation fail");
            e.printStackTrace();
        }

        return f;
    }

}
