package me.rewhite.delivery.tablet;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.Bootstrap;
import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.PickCountScreen;
import me.rewhite.delivery.adapter.TableItemElementListViewAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.data.PickupType;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.StringUtil;
import me.rewhite.delivery.util.TimeUtil;
import me.rewhite.delivery.util.ToastUtility;

public class TableOrderDetailActivity extends TabletBaseActivity {

    AQuery aq;
    Context mCtx = this;

    JSONObject selectedUserData;

    JSONObject parentData;
    String userName;
    String profileImage;
    long pickupRequestTime;
    long deliveryRequestTime;
    long pickupCompleteTime;
    long deliveryCompleteTime;
    String addressName;
    String addressDetailName;
    String orderStatus;
    String orderPrice;
    int orderId;
    double longitude;
    double latitude;
    private static final int PICKUP_COUNT = 7;
    private static final int PICKUP_ITEM = 8;
    private static final int PICKUP_REPAIR_ITEM = 9;
    private static final int SHOW_ITEM = 10;
    private static final int MOD_DELIVERY_OK = 2;

    String orderRequest;
    JSONArray pickupItemsV2 = new JSONArray();
    String pickupItems;
    String pickupRepairItems;
    String isPayment;
    String phoneNo;
    String isDeliveryMan;
    int pickQuantity = 0;
    String orderSubType;
    String orderType;

    boolean isConfirmed = true;
    boolean isPaidConfirmed = true;

    ProgressDialog mProgressDialog;
    ImageOptions options;

    String isReceivable;
    JSONArray payments;
    String paymentType;
    int prevTagNo = -1;

    private static final int SEARCH_USER_RESULT = 11;

    @Override
    protected void onDestroy() {
        Log.d("onDestroy", "called onDestroy");
        if (mProgressDialog != null){
            mProgressDialog.dismiss();
        }
        super.onDestroy();
    }

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(TableOrderDetailActivity.this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    public void closeClicked(View button) {
        finish();
    }

    private JSONArray getPickupItem(JSONArray originData) {
        JSONArray result = new JSONArray();
        try {
            for (int i = 0; i < originData.length(); i++) {
                if ("G".equals(originData.getJSONObject(i).getString("pickupType")) || "T".equals(originData.getJSONObject(i).getString("pickupType")) || "N".equals(originData.getJSONObject(i).getString("pickupType"))) {
                    result.put(originData.getJSONObject(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private JSONArray getRepairItem(JSONArray originData) {
        JSONArray result = new JSONArray();
        try {
            for (int i = 0; i < originData.length(); i++) {
                if ("R".equals(originData.getJSONObject(i).getString("pickupType"))) {
                    result.put(originData.getJSONObject(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(CommonUtility.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        Log.e("LocalOrderOutput", "click search");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablet_activity_order_detail);

        aq = new AQuery(this);
        aq.hardwareAccelerated11();

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.profile_name).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.phone_text).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.address_text).getTextView());

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.title_deco).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_order_id).getTextView());
        CommonUtility.setTypefaceLightSetup(aq.id(R.id.text_order_date).getTextView());
        CommonUtility.setTypefaceLightSetup(aq.id(R.id.time_title).getTextView());
        CommonUtility.setTypefaceLightSetup(aq.id(R.id.time_content).getTextView());
        CommonUtility.setTypefaceLightSetup(aq.id(R.id.btn_change).getTextView());
        CommonUtility.setTypefaceLightSetup(aq.id(R.id.req_message_text).getTextView());
        CommonUtility.setTypefaceLightSetup(aq.id(R.id.req_title).getTextView());

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_subtype).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.title_deco2).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.status_text).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.date_deco_title_01).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.date_deco_title_02).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.pickup_date).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.pickup_time).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.delivery_date).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.delivery_time).getTextView());

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.totalprice_text).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.total_price_title).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.pick_count_text).getTextView());

        aq.id(R.id.picks_layout).gone();
        aq.id(R.id.total_layout).gone();
        aq.id(R.id.delivery_action_layout).gone();

        aq.id(R.id.btn_back).clicked(this, "closeClicked");
        aq.id(R.id.btn_call).clicked(this, "callClicked");
        aq.id(R.id.btn_show).clicked(this, "showItemsAction");
        aq.id(R.id.btn_change).clicked(this, "deliveryChange");


        options = new ImageOptions();
        options.round = 60;
        options.ratio = 1.0f;
        options.memCache = true;
        options.targetWidth = 60;
        options.animation = AQuery.FADE_IN_NETWORK;
        options.preset = aq.getCachedImage(R.mipmap.kakao_default_profile_image);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                if (intent.getStringExtra("data") != null) {

                }
                parentData = new JSONObject(intent.getStringExtra("data"));

                orderId = parentData.getInt("orderId");

                aq.id(R.id.text_order_id).text(orderId + "");
                aq.id(R.id.text_order_date).text("주문일시 " + TimeUtil.convertTimestampToString2(parentData.getLong("registerDateApp")) );

                aq.id(R.id.order_count).text(parentData.getInt("addressOrderCount") + "");

                //aq.id(R.id.address_text).text(selectedUserData.getString("address1") + " " +selectedUserData.getString("address2"));
                //aq.id(R.id.phone_text).text(selectedUserData.getString("phone"));

                orderStatus = parentData.getString("orderStatus");
                orderSubType = parentData.getString("orderSubType");
                orderType = parentData.getString("orderType");

                longitude = parentData.getDouble("longitude");
                latitude = parentData.getDouble("latitude");

                userName = parentData.getString("userName");

                profileImage = parentData.getString("imageThumbPath");
                if (!StringUtil.isNullOrEmpty(profileImage)) {
                    aq.id(R.id.profile_image).image(profileImage, options);
                    // TODO BigImage required
                    //aq.id(R.id.image_profile_bg).image(profileImage);
                }

                if ("3".equals(orderType)) {
                    if ("301".equals(orderSubType)) {
                        phoneNo = parentData.getString("deliveryRequestMessage");
                        userName = parentData.getString("label");
                        aq.id(R.id.profile_name).text(userName);
                    }
                } else {
                    phoneNo = parentData.getString("phone");
                    aq.id(R.id.profile_name).text(userName);
                }

                aq.id(R.id.btn_alert).clicked(this, "preAlertAction");
                aq.id(R.id.btn_delivery_alert).clicked(this, "preAlertAction");

                addressName = parentData.getString("address1");
                addressDetailName = parentData.getString("address2");
                String newAddressName = addressName + " " + addressDetailName;
                if (!StringUtil.isNullOrEmpty(newAddressName)) {
                    aq.id(R.id.address_text).text(newAddressName);
                }

                aq.id(R.id.phone_text).text(MZValidator.validTelNumber(parentData.getString("phone")));


                mProgressDialog = new ProgressDialog(TableOrderDetailActivity.this);
                mProgressDialog.setMessage("잠시만 기다려주세요");
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
                getRemoteData(orderId);

                aq.id(R.id.btn_submit).clicked(this, "addCompleteAction");
                aq.id(R.id.btn_delivery).clicked(this, "orderCompleteAction");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if ("101".equals(orderSubType)) {
            aq.id(R.id.btn_add).clicked(this, "addItemsAction").tag(1);
            aq.id(R.id.btn_mod).clicked(this, "addItemsAction").tag(1);
            aq.id(R.id.top_layout).backgroundColor(0xff5eaeff);
            aq.id(R.id.text_subtype).text("일반세탁");


            aq.id(R.id.btn_find_location).backgroundColor(0xff007fff);

        } else if ("201".equals(orderSubType)) {

        } else if ("105".equals(orderSubType)) {
            aq.id(R.id.btn_add).clicked(this, "addItemsAction").tag(3);
            aq.id(R.id.btn_mod).clicked(this, "addItemsAction").tag(3);
            aq.id(R.id.top_layout).backgroundColor(0xffff7272);
            aq.id(R.id.text_subtype).text("빠른세탁 당일");

            aq.id(R.id.btn_find_location).backgroundColor(0xfffd7070);

        } else if ("104".equals(orderSubType)) {
            aq.id(R.id.btn_add).clicked(this, "addItemsAction").tag(4);
            aq.id(R.id.btn_mod).clicked(this, "addItemsAction").tag(4);
            aq.id(R.id.top_layout).backgroundColor(0xffff845e);
            aq.id(R.id.text_subtype).text("빠른세탁 익일");

            aq.id(R.id.btn_find_location).backgroundColor(0xffff845e);
        } else if ("301".equals(orderSubType)) {
            aq.id(R.id.btn_add).clicked(this, "addItemsAction").tag(5);
            aq.id(R.id.btn_mod).clicked(this, "addItemsAction").tag(5);
            aq.id(R.id.top_layout).backgroundColor(0xffa957e8);
            aq.id(R.id.text_subtype).text("침구 제휴세탁");

            aq.id(R.id.btn_find_location).backgroundColor(0xffa957e8);
        }


        aq.id(R.id.btn_count_add).clicked(this, "addItemsAction").tag(0);
        aq.id(R.id.btn_count_mod).clicked(this, "addItemsAction").tag(0);

    }

    private void getRemoteData(int _orderId) {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_DETAIL, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        parentData = jsondata.getJSONObject("data");

                        aq.id(R.id.text_order_id).text("" + parentData.getInt("orderId"));
                        aq.id(R.id.text_order_date).text("주문일시 : " + TimeUtil.convertTimestampToString(parentData.getLong("registerDateApp")) );

                        if(parentData.isNull("pickupRequestMessage")){
                            aq.id(R.id.req_layout).gone();
                        }else{
                            aq.id(R.id.req_message_text).text(parentData.getString("pickupRequestMessage"));
                        }

                        aq.id(R.id.pickup_date).text(TimeUtil.convertTimestampToStringDate2(parentData.getLong("pickupRequestTimeApp")) );
                        aq.id(R.id.pickup_time).text(TimeUtil.convertTimestampToStringTime2(parentData.getLong("pickupRequestTimeApp")) );

                        aq.id(R.id.delivery_date).text(TimeUtil.convertTimestampToStringDate2(parentData.getLong("deliveryRequestTimeApp")) );
                        aq.id(R.id.delivery_time).text(TimeUtil.convertTimestampToStringTime2(parentData.getLong("deliveryRequestTimeApp")) );

                        aq.id(R.id.totalprice_text).text(MZValidator.toNumFormat(parentData.getInt("deliveryPrice"))+" 원");


                        int addressOrderCount = parentData.getInt("addressOrderCount");
                        int addressOrderQuantity = parentData.getInt("addressOrderQuantity");

                        /*
                        TODO
                        Handys 기능
                         */
                        if ("301".equals(orderSubType)) {
                            //aq.id(R.id.b2b_layout).visible();
                            if (addressOrderCount == 0) {
                                aq.id(R.id.text_address_order_count).text("배송해야할 침구세트가 없습니다.");
                            } else {
                                aq.id(R.id.text_address_order_count).text(addressOrderQuantity + " 개");
                            }
                        }

                        isReceivable = jsondata.getJSONObject("data").getString("isReceivable");
                        isDeliveryMan = jsondata.getJSONObject("data").getString("isDeliveryMan");

                        isPayment = jsondata.getJSONObject("data").getString("isPayment");
//
//                        int totalPickCount = jsondata.getJSONObject("data").getInt("pickupQuantity");
//                        aq.id(R.id.item_count_text).text("총 " + totalPickCount + "개");

                        pickupItems = getPickupItem(jsondata.getJSONObject("data").getJSONArray("pickupItems")).toString();
                        pickupRepairItems = getRepairItem(jsondata.getJSONObject("data").getJSONArray("pickupItems")).toString();
                        int pr = jsondata.getJSONObject("data").getInt("deliveryPrice");

                        if (pr < 20000) {
                            pr = pr + 2000;
                            aq.id(R.id.delivery_display_text).visible();
                        }
                        orderPrice = MZValidator.toNumFormat(pr);
                        aq.id(R.id.text_total_price).text(orderPrice);
                        JSONArray history = jsondata.getJSONObject("data").getJSONArray("history");
                        for (int i = 0; i < history.length(); i++) {
                            if ("03".equals(history.getJSONObject(i).getString("orderStatus")) || "11".equals(history.getJSONObject(i).getString("orderStatus"))) {
                                pickupCompleteTime = history.getJSONObject(i).getLong("statusDateApp");
                            }
                            if ("23".equals(history.getJSONObject(i).getString("orderStatus"))) {
                                deliveryCompleteTime = history.getJSONObject(i).getLong("statusDateApp");
                            }
                        }
                        orderStatus = jsondata.getJSONObject("data").getString("orderStatus");


                        // 세탁목록
                        JSONArray arr = getPickupItem(jsondata.getJSONObject("data").getJSONArray("pickupItems"));
                        String picksText = "";
                        int count = 0;
                        for (int i = 0; i < arr.length(); i++) {
                            if ("N".equals(arr.getJSONObject(i).getString("isOption"))) {
                                picksText += arr.getJSONObject(i).getString("itemTitle") + " x" + arr.getJSONObject(i).getInt("itemQuantity");
                                count += arr.getJSONObject(i).getInt("itemQuantity");
                                if (i == arr.length() - 1) {
                                    picksText += "\n총 " + count + "벌";
                                } else {
                                    picksText += ", ";
                                }
                            }
                        }
                        //수선목록
                        JSONArray arr2 = getRepairItem(jsondata.getJSONObject("data").getJSONArray("pickupItems"));
                        String picksRepairText = "";
                        int count2 = 0;
                        for (int i = 0; i < arr2.length(); i++) {
                            if ("N".equals(arr2.getJSONObject(i).getString("isOption"))) {
                                picksRepairText += arr2.getJSONObject(i).getString("itemTitle") + " x" + arr2.getJSONObject(i).getInt("itemQuantity");
                                count2 += arr2.getJSONObject(i).getInt("itemQuantity");
                                if (i == arr2.length() - 1) {
                                    picksRepairText += "\n총 " + count2 + "건";
                                } else {
                                    picksRepairText += ", ";
                                }
                            }
                        }

                        picksText += picksRepairText;

                        Log.e("clean value", arr.length() + " / " + picksText);

                        pickupItemsV2 = jsondata.getJSONObject("data").getJSONArray("pickupItemsV2");
                        if(arr.length() == 0 && pickupItemsV2.length() > 0){
                            count = pickupItemsV2.length();
                            if(count == 1){
                                picksText = pickupItemsV2.getJSONObject(0).getString("itemTitle") + " " +count + "개";
                            }else{
                                picksText = pickupItemsV2.getJSONObject(0).getString("itemTitle") + " 등 " +count + "개";
                            }

                        }

                        Log.e("clean value v2", pickupItemsV2.length() + " / " + picksText);

                        if ("Y".equals(isPayment)) {
                            payments = jsondata.getJSONObject("data").getJSONArray("payments");
                            if (payments.length() > 0) {
                                paymentType = payments.getJSONObject(0).getString("paymentType");
                                if ("P91".equals(paymentType)) {
                                    aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_card);
                                } else if ("P92".equals(paymentType)) {
                                    aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_cash);
                                } else {
                                    aq.id(R.id.paid_image).image(R.mipmap.paidinfo_already);
                                }
                            } else {
                                aq.id(R.id.paid_info_layout).gone();
                            }
                        } else {
                            payments = null;
                            aq.id(R.id.paid_info_layout).gone();
                        }

                        ListView pickItemsListView = (ListView)findViewById(R.id.listView);

                        if(pickupItemsV2.length() > 0){
                            aq.id(R.id.empty).gone();
                            aq.id(R.id.pick_count_text).text("수거수량 : " + parentData.getInt("pickupQuantity") + "개\n" + "입력수량 : " + pickupItemsV2.length() + "개");
                        }

                        orderAdapter = new TableItemElementListViewAdapter(mCtx, R.layout.tablet_order_pickup_item, pickupItemsV2);
                        orderAdapter.notifyDataSetChanged();

                        pickItemsListView.setAdapter(orderAdapter);

                        //aq.id(R.id.text_items).text(picksText);
                        //aq.id(R.id.text_repair_items).text(picksRepairText);

                        if (arr.length() > 0 || arr2.length() > 0) {
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.btn_show).gone();
                        }else if(pickupItemsV2.length() > 0){
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.btn_show).visible();
                        } else {
                            aq.id(R.id.picks_layout).gone();
                        }
                        if (count2 > 0) {
                            //aq.id(R.id.repair_layout).visible();
                        } else {
                            //aq.id(R.id.repair_layout).gone();
                        }
                    } else {

                    }

                    setStatusScreen(orderStatus);
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    boolean isFirstExist = false;


    private TableItemElementListViewAdapter orderAdapter;



    public void itemSelected(int position, JSONObject jsondata) {
        Log.i("itemSel", jsondata.toString());

        try {
            getDataByOrderId(jsondata.getString("orderId"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getDataByOrderId(String _orderId) {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_DETAIL, result);

                    JSONObject resultJson = new JSONObject(result);

                    if ("S0000".equals(resultJson.getString("resultCode"))) {
                        if (resultJson.isNull("data")) {
                            //
                            aq.id(R.id.empty).visible();
                        } else {

                            JSONObject jsondata = resultJson.getJSONObject("data");

                            aq.id(R.id.text_order_id).text("" + jsondata.getInt("orderId"));
                            aq.id(R.id.text_order_date).text("주문일시 : " + TimeUtil.convertTimestampToString(jsondata.getLong("registerDateApp")) );

                            if(jsondata.isNull("pickupRequestMessage")){
                                aq.id(R.id.req_layout).gone();
                            }else{
                                aq.id(R.id.req_message_text).text(jsondata.getString("pickupRequestMessage"));
                            }

                            aq.id(R.id.pickup_date).text(TimeUtil.convertTimestampToStringDate2(jsondata.getLong("pickupRequestTimeApp")) );
                            aq.id(R.id.pickup_time).text(TimeUtil.convertTimestampToStringTime2(jsondata.getLong("pickupRequestTimeApp")) );

                            aq.id(R.id.delivery_date).text(TimeUtil.convertTimestampToStringDate2(jsondata.getLong("deliveryRequestTimeApp")) );
                            aq.id(R.id.delivery_time).text(TimeUtil.convertTimestampToStringTime2(jsondata.getLong("deliveryRequestTimeApp")) );

                            aq.id(R.id.pick_count_text).text("(수거수량 : " + jsondata.getInt("pickupQuantity") + "개)");
                            aq.id(R.id.totalprice_text).text(MZValidator.toNumFormat(jsondata.getInt("deliveryPrice"))+" 원");



                            aq.id(R.id.empty).gone();
                        }

                    } else {
                        aq.id(R.id.empty).visible();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case SEARCH_USER_RESULT:
                    try {
                        selectedUserData = new JSONObject(data.getStringExtra("element"));
                        Log.i("selectedUsers", selectedUserData.toString());

                        ImageOptions op = new ImageOptions();
                        op.round = 60;
                        op.memCache = true;
                        op.ratio = 1.f;

                        aq.id(R.id.profile_image).image(selectedUserData.getString("imageThumbPath"), op);
                        aq.id(R.id.profile_name).text(selectedUserData.getString("userName"));
                        aq.id(R.id.address_text).text(selectedUserData.getString("address1") + " " +selectedUserData.getString("address2"));
                        aq.id(R.id.phone_text).text(selectedUserData.getString("phone"));

                        //findOrderByUserId(selectedUserData.getString("userId"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
                case PICKUP_ITEM:
                    try {
                        pickupItems = data.getStringExtra("data");
                        pickupItemsV2 = new JSONArray(pickupItems);

                        prevTagNo = data.getIntExtra("prevTagNo", -1);

                        JSONArray arr = new JSONArray(pickupItems);
                        Snackbar.make(aq.getView(), "상품을 추가했습니다", Snackbar.LENGTH_SHORT).show();
                        Log.i("pickupItems list", pickupItemsV2.toString());
                        aq.id(R.id.total_layout).visible();
                        //int price = data.getIntExtra("price", 0);
                        String priceString = MZValidator.toNumFormat(getTotalPrice());

                        aq.id(R.id.text_total_price).text(priceString);

                        String picksText = "";
                        int count = 0;
                        if(arr.length() > 0){
                            picksText = arr.getJSONObject(0).getString("itemTitle");
                            aq.id(R.id.text_items).text(picksText + " 등 "+ arr.length() +"개");
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.add_area).gone();

                            submitPickup();
                        }else {
                            aq.id(R.id.picks_layout).gone();
                            aq.id(R.id.add_area).visible();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
                case PICKUP_REPAIR_ITEM:
                    try {
                        pickupRepairItems = data.getStringExtra("data");
                        JSONArray arr = new JSONArray(pickupRepairItems);
                        Snackbar.make(aq.getView(), "상품을 추가했습니다", Snackbar.LENGTH_SHORT).show();
                        Log.i("pickupRepairItems list", pickupRepairItems);
                        aq.id(R.id.total_layout).visible();
                        //int price = data.getIntExtra("price", 0);
                        String priceString = MZValidator.toNumFormat(getTotalPrice());

                        aq.id(R.id.text_total_price).text(priceString);

                        String picksText = "";
                        int count = 0;
                        for (int i = 0; i < arr.length(); i++) {
                            if ("N".equals(arr.getJSONObject(i).getString("isOption"))) {
                                picksText += arr.getJSONObject(i).getString("itemTitle") + " x" + arr.getJSONObject(i).getInt("itemQuantity");
                                count += arr.getJSONObject(i).getInt("itemQuantity");
                                if (i == arr.length() - 1) {
                                    picksText += "\n총 " + count + "건";
                                } else {
                                    picksText += ", ";
                                }
                            }
                        }

                        //aq.id(R.id.text_repair_items).text(picksText);

                        JSONArray count1 = new JSONArray(pickupItems);
                        if (count1.length() > 0) {
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.btn_show).gone();
                        }else if(pickupItemsV2.length() > 0){
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.btn_show).visible();
                        } else {
                            aq.id(R.id.picks_layout).gone();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case PICKUP_COUNT:
                    pickQuantity = data.getIntExtra("result", 0);
                    if (pickQuantity > 0) {
                        //aq.id(R.id.picks_layout).visible();
                        aq.id(R.id.item_count_text).text("총 " + pickQuantity + "벌");

                        //aq.id(R.id.item_count_layout).visible();
                        //aq.id(R.id.btn_submit).visible();
                        aq.id(R.id.btn_count_add).gone();

                        submitPickup();
                    } else {
                        //aq.id(R.id.picks_layout).gone();

                        //aq.id(R.id.item_count_layout).gone();
                        aq.id(R.id.btn_submit).gone();
                        aq.id(R.id.btn_count_add).visible();
                    }

                    break;
                case MOD_DELIVERY_OK:
                    Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("USER")
                            .setAction("배송요청일 변경")
                            .setLabel("Order")
                            .build());

                    Toast.makeText(this, "배송요청정보가 변경되었습니다.", Toast.LENGTH_LONG).show();
                    getRemoteData(orderId);
                    /*
                    try {
                        orderStatus = "21";
                        setStatusScreen(orderStatus);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                    break;
                case SHOW_ITEM:
                    getRemoteData(orderId);
                    break;
            }
        }
    }

    private String displayTimeString(long time) {
        //Date dt = new Date();
        //dt.setTime(time);
        if (SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP) == null) {
            SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.DELIVERY_GAP, "1");
        }
        return TimeUtil.getDateByMDSH(time, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP));
    }

    private void setStatusScreen(String _statusCode) throws JSONException {
        //showDialog();
        //aq.id(R.id.delivery_action_layout).gone();
        //aq.id(R.id.pick_action_layout).gone();
        aq.id(R.id.alert_layout).gone();
        aq.id(R.id.btn_submit).gone();
        aq.id(R.id.btn_delivery).gone();
        aq.id(R.id.paid_info_layout).gone();
        aq.id(R.id.below_layout).visible();
        aq.id(R.id.btn_change).gone();
        aq.id(R.id.btn_count_mod).gone();
        aq.id(R.id.req_message_text).text(parentData.getString("pickupRequestMessage"));
        aq.id(R.id.b2b_layout).gone();
        aq.id(R.id.btn_show).gone();
        aq.id(R.id.status_image).gone();

        if ("01".equals(_statusCode)) {
            aq.id(R.id.status_text).text("수거접수");
            aq.id(R.id.alert_layout).visible();
            aq.id(R.id.pre_alert_area).visible();
            aq.id(R.id.prealarm_txtimage).image(R.mipmap.screen_pre_alarm);
            aq.id(R.id.below_layout).gone();
            aq.id(R.id.time_title).text("수거희망시간");
            String dps = displayTimeString(parentData.getLong("pickupRequestTimeApp"));
            aq.id(R.id.time_content).text(dps);
            aq.id(R.id.screen_pre_alarm).visible();
            aq.id(R.id.item_count_layout).gone();
            aq.id(R.id.price_info).gone();

            if ("301".equals(orderSubType)) {
                aq.id(R.id.b2b_layout).visible();
            } else {
                aq.id(R.id.b2b_layout).gone();
            }
        } else if ("02".equals(_statusCode) || "03".equals(_statusCode)) {
            if ("02".equals(_statusCode)) {
                aq.id(R.id.status_text).text("수거하러 이동중");
                aq.id(R.id.time_title).text("수거희망시간");
                String dps = displayTimeString(parentData.getLong("pickupRequestTimeApp"));
                aq.id(R.id.time_content).text(dps);
                if ("Y".equals(isDeliveryMan)) {
                    aq.id(R.id.add_area).gone();
                    aq.id(R.id.count_add_area).visible();
                } else {
                    aq.id(R.id.add_area).visible();
                    aq.id(R.id.count_add_area).gone();
                }
                //aq.id(R.id.pick_action_layout).gone();

                aq.id(R.id.item_count_layout).gone();
                aq.id(R.id.btn_count_mod).visible();

                if ("301".equals(orderSubType)) {
                    aq.id(R.id.b2b_layout).visible();
                } else {
                    aq.id(R.id.b2b_layout).gone();
                }
            } else if ("03".equals(_statusCode)) {
                aq.id(R.id.status_text).text("수거항목 등록");
                aq.id(R.id.time_title).text("수거완료시간");
                String dps = displayTimeString(pickupCompleteTime);
                aq.id(R.id.time_content).text(dps);
                aq.id(R.id.add_area).visible();
                aq.id(R.id.count_add_area).gone();
                aq.id(R.id.btn_count_mod).gone();
                //aq.id(R.id.inner_code_layout).visible();
                //aq.id(R.id.btn_submit).visible();

                aq.id(R.id.status_image).visible().image(R.mipmap.icon_tablet_main_input_item);

            }

            aq.id(R.id.price_info).gone();
            aq.id(R.id.alert_layout).gone();

        } else if ("11".equals(_statusCode) || "12".equals(_statusCode) || "13".equals(_statusCode)) {
            aq.id(R.id.pre_alert_area).gone();
            aq.id(R.id.add_area).gone();

            aq.id(R.id.count_add_area).gone();
            aq.id(R.id.add_area).gone();
            aq.id(R.id.btn_show).visible();
            aq.id(R.id.btn_mod).gone();
            aq.id(R.id.price_info).visible();

            if ("11".equals(_statusCode)) {
                aq.id(R.id.status_text).text("세탁 준비중");

            } else if ("12".equals(_statusCode)) {
                aq.id(R.id.status_text).text("세탁 중");
            } else if ("13".equals(_statusCode)) {
                aq.id(R.id.status_text).text("세탁완료");
                aq.id(R.id.pre_alert_area).visible();
                //  배송준비중 동일하게
                aq.id(R.id.alert_layout).visible();
                aq.id(R.id.screen_pre_alarm).visible();
                aq.id(R.id.below_layout).gone();
            }
/*
            aq.id(R.id.text_pickup_time_title).text("수거완료시간").typeface(CommonUtility.getNanumBarunTypeface());
            String dps = displayTimeString(pickupCompleteTime);
            aq.id(R.id.time_content).text(dps).typeface(CommonUtility.getNanumBarunLightTypeface());*/
            //
            aq.id(R.id.time_title).text("배송희망시간");
            String dds = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
            aq.id(R.id.time_content).text(dds);
            //

            aq.id(R.id.pick_action_layout).gone();

            JSONArray count1 = new JSONArray(pickupItems);
            JSONArray count2 = new JSONArray(pickupRepairItems);
            JSONArray countV2 = pickupItemsV2;

            if (count1.length() > 0 || count2.length() > 0) {
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).gone();
            }else if(pickupItemsV2.length() > 0){
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).visible();
            } else {
                aq.id(R.id.picks_layout).gone();
            }
            aq.id(R.id.total_layout).visible();
            //aq.id(R.id.text_delivery_area).visible();
        } else if ("21".equals(_statusCode) || "22".equals(_statusCode)) {
            aq.id(R.id.count_add_area).gone();
            aq.id(R.id.add_area).gone();
            aq.id(R.id.pre_alert_area).visible();
            //aq.id(R.id.prealarm_txtimage).image(R.mipmap.screen_pre_alarm_delivery);
            aq.id(R.id.btn_show).visible();
            aq.id(R.id.btn_mod).gone();
            aq.id(R.id.price_info).visible();

            if("Y".equals(parentData.getString("isPayment"))){
                JSONArray payments = parentData.getJSONArray("payments");
                if (payments.length() > 0) {
                    String paymentType = payments.getJSONObject(0).getString("paymentType");
                    if ("P91".equals(paymentType)) {
                        //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_card);
                        if("Y".equals(parentData.getString("isReceivable"))){
                            //aq.id(R.id.text_payment).text("완료지연(미수금)");
                            aq.id(R.id.status_image).visible().image(R.mipmap.icon_tablet_main_pay_receivable);
                        }else{
                            //aq.id(R.id.text_payment).text("카드단말기 요청");
                            aq.id(R.id.status_image).visible().image(R.mipmap.icon_tablet_main_pay_card_req);
                        }
                    } else if ("P92".equals(paymentType)) {
                        if("Y".equals(parentData.getString("isReceivable"))){
                            //aq.id(R.id.text_payment).text("완료지연(미수금)");
                            aq.id(R.id.status_image).visible().image(R.mipmap.icon_tablet_main_pay_receivable);
                        }else{
                            //aq.id(R.id.text_payment).text("현금결제 선택");
                            aq.id(R.id.status_image).visible().image(R.mipmap.icon_tablet_main_pay_cash_req);
                        }
                        //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_cash);
                    } else {
                        //aq.id(R.id.text_payment).text("결제완료");
                        aq.id(R.id.status_image).visible().image(R.mipmap.icon_tablet_main_pay_completed);
                        //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_already);
                    }
                } else {
                    //aq.id(R.id.text_payment).text("결제완료");
                    aq.id(R.id.status_image).visible().image(R.mipmap.icon_tablet_main_pay_completed);
                }
            }else if ("N".equals(parentData.getString("isPayment"))) {
                if("Y".equals(parentData.getString("isReceivable"))){
                    //aq.id(R.id.text_payment).text("완료지연(미수금)");
                    aq.id(R.id.status_image).visible().image(R.mipmap.icon_tablet_main_pay_receivable);
                }else{
                    //aq.id(R.id.text_payment).text("결제방식 선택 전");
                    aq.id(R.id.status_image).visible().image(R.mipmap.icon_tablet_main_pay_not_selected);
                }
            } else{
                aq.id(R.id.status_image).gone();
            }

            if ("21".equals(_statusCode)) {
                aq.id(R.id.status_text).text("배송 준비중");
                aq.id(R.id.alert_layout).gone();
                aq.id(R.id.screen_pre_alarm).gone();
                aq.id(R.id.delivery_alert_layout).visible();

                aq.id(R.id.below_layout).visible();
                aq.id(R.id.paid_info_layout).gone();
                aq.id(R.id.btn_change).visible();
                aq.id(R.id.loc_layout).gone();
                aq.id(R.id.item_count_layout).gone();
                aq.id(R.id.prod_layout).gone();
                //aq.id(R.id.inner_code_layout).gone();
                //aq.id(R.id.pick_action_layout).gone();
                aq.id(R.id.delivery_action_layout).gone();

            } else if ("22".equals(_statusCode)) {
                if("Y".equals(parentData.getString("isReceivable"))){
                    aq.id(R.id.status_text).text("배송완료 지연(미수금)");
                    //aq.id(R.id.status_image).visible().image(R.mipmap.icon_tablet_main_pay_receivable);
                    aq.id(R.id.delivery_action_layout).gone();
                    aq.id(R.id.btn_change).gone();
                }else{
                    aq.id(R.id.status_text).text("배송 중");
                    aq.id(R.id.delivery_action_layout).visible();
                    aq.id(R.id.btn_delivery).visible();
                    aq.id(R.id.btn_change).visible();
                }

                aq.id(R.id.alert_layout).gone();
                aq.id(R.id.delivery_alert_layout).gone();
                aq.id(R.id.below_layout).visible();

                aq.id(R.id.loc_layout).visible();
                aq.id(R.id.item_count_layout).visible();
                aq.id(R.id.prod_layout).visible();
                //aq.id(R.id.inner_code_layout).gone();
                //aq.id(R.id.pick_action_layout).gone();
                //aq.id(R.id.delivery_action_layout).gone();

                aq.id(R.id.paid_info_layout).visible();


            }
            //aq.id(R.id.pre_alert_area).gone();
            //
            /*
            aq.id(R.id.text_pickup_time_title).text("수거완료시간").typeface(CommonUtility.getNanumBarunTypeface());
            String dps = displayTimeString(pickupCompleteTime);
            aq.id(R.id.text_pickup_time).text(dps).typeface(CommonUtility.getNanumBarunLightTypeface());*/
            aq.id(R.id.time_title).text("배송희망시간");
            String dds = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
            aq.id(R.id.time_content).text(dds);

            //
            aq.id(R.id.add_area).gone();
            aq.id(R.id.pick_action_layout).gone();


            JSONArray count1 = new JSONArray(pickupItems);
            JSONArray count2 = new JSONArray(pickupRepairItems);
            JSONArray countV2 = pickupItemsV2;

            if (count1.length() > 0 || count2.length() > 0) {
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).gone();
            }else if(pickupItemsV2.length() > 0){
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).visible();
            } else {
                aq.id(R.id.picks_layout).gone();
            }
            //aq.id(R.id.picks_layout).visible();
            aq.id(R.id.total_layout).visible();

            //aq.id(R.id.text_delivery_area).visible();
        } else if ("23".equals(_statusCode) || "24".equals(_statusCode)) {
            aq.id(R.id.count_add_area).gone();
            aq.id(R.id.add_area).gone();
            aq.id(R.id.btn_show).visible();
            aq.id(R.id.btn_mod).gone();

            if ("23".equals(_statusCode)) {
                aq.id(R.id.status_text).text("배송완료");
            } else if ("24".equals(_statusCode)) {
                aq.id(R.id.status_text).text("방문 수령 완료");
            }
            aq.id(R.id.paid_info_layout).visible();
            aq.id(R.id.pre_alert_area).gone();
            /*
            aq.id(R.id.text_pickup_time_title).text("수거완료시간").typeface(CommonUtility.getNanumBarunTypeface());
            String dps = displayTimeString(pickupCompleteTime);
            aq.id(R.id.text_pickup_time).text(dps).typeface(CommonUtility.getNanumBarunLightTypeface());*/
            aq.id(R.id.time_title).text("배송완료시간");
            String dds = displayTimeString(deliveryCompleteTime);
            aq.id(R.id.time_content).text(dds);
            //
            aq.id(R.id.add_area).gone();
            JSONArray count1 = new JSONArray(pickupItems);
            JSONArray count2 = new JSONArray(pickupRepairItems);
            JSONArray countV2 = pickupItemsV2;

            if (count1.length() > 0 || count2.length() > 0) {
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).gone();
            }else if(pickupItemsV2.length() > 0){
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).visible();
            } else {
                aq.id(R.id.picks_layout).gone();
            }

            aq.id(R.id.pick_action_layout).gone();
            aq.id(R.id.total_layout).visible();
            //aq.id(R.id.text_delivery_area).visible();
            //aq.id(R.id.delivery_action_layout).visible();
        } else {
            aq.id(R.id.count_add_area).gone();
            aq.id(R.id.add_area).gone();
            if ("91".equals(_statusCode)) {
                aq.id(R.id.text_status).text("주문취소");
            } else if ("92".equals(_statusCode)) {
                aq.id(R.id.text_status).text("재세탁 신청");
            } else if ("93".equals(_statusCode)) {
                aq.id(R.id.text_status).text("환불");
            } else if ("94".equals(_statusCode)) {
                aq.id(R.id.text_status).text("보상요청");
            } else if ("95".equals(_statusCode)) {
                aq.id(R.id.text_status).text("고객사유로 취소");
            } else if ("96".equals(_statusCode)) {
                aq.id(R.id.text_status).text("주문 거부");
            }
            aq.id(R.id.pre_alert_area).gone();
            aq.id(R.id.add_area).gone();
            //aq.id(R.id.text_delivery_area).visible();
            aq.id(R.id.pick_action_layout).gone();
        }

        dismissDialog();
    }

    public void addItemsAction(View button) {
        final int tag = (int) button.getTag();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent pickIntent = new Intent(mCtx, TabletOrderInputActivity.class);
                //Intent pickIntent = new Intent(ctx, PriceActivity.class);
                Intent pickCountIntent = new Intent(mCtx, PickCountScreen.class);
                if (pickQuantity > 0) {
                    pickCountIntent.putExtra("pickCount", pickQuantity);
                    pickIntent.putExtra("pickCount", pickQuantity);
                }

                int pickupMode;
                switch (tag) {
                    case 0:

                        pickCountIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickCountIntent, PICKUP_COUNT);
                        break;
                    case 1:
                        pickupMode = PickupType.PICKUPTYPE_CLEAN;
                        if (!StringUtil.isNullOrEmpty(pickupItems)) {
                            pickIntent.putExtra("pickupItems", pickupItems);
                            pickIntent.putExtra("pickupMode", pickupMode);
                            pickIntent.putExtra("prevTagNo", prevTagNo);
                        }
                        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickIntent, PICKUP_ITEM);
                        break;
                    case 2:
                        /*
                        pickupMode = PickupType.PICKUPTYPE_REPAIR;
                        if (!StringUtil.isNullOrEmpty(pickupRepairItems)) {
                            pickIntent.putExtra("pickupItems", pickupRepairItems);
                            pickIntent.putExtra("pickupMode", pickupMode);
                        }
                        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickIntent, PICKUP_REPAIR_ITEM);*/
                        break;
                    case 3:
                        pickupMode = PickupType.PICKUPTYPE_TODAY;
                        if (!StringUtil.isNullOrEmpty(pickupItems)) {
                            pickIntent.putExtra("pickupItems", pickupItems);
                            pickIntent.putExtra("pickupMode", pickupMode);
                            pickIntent.putExtra("prevTagNo", prevTagNo);
                        }
                        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickIntent, PICKUP_ITEM);
                        break;
                    case 4:
                        pickupMode = PickupType.PICKUPTYPE_TOMORROW;
                        if (!StringUtil.isNullOrEmpty(pickupItems)) {
                            pickIntent.putExtra("pickupItems", pickupItems);
                            pickIntent.putExtra("pickupMode", pickupMode);
                            pickIntent.putExtra("prevTagNo", prevTagNo);
                        }
                        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickIntent, PICKUP_ITEM);
                        break;
                    case 5:
                        pickupMode = PickupType.PICKUPTYPE_B2B_HANDYS;
                        if (!StringUtil.isNullOrEmpty(pickupItems)) {
                            pickIntent.putExtra("pickupItems", pickupItems);
                            pickIntent.putExtra("pickupMode", pickupMode);
                            pickIntent.putExtra("prevTagNo", prevTagNo);
                        }
                        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickIntent, PICKUP_ITEM);
                        break;
                    default:
                        //pickupMode = PickupType.PICKUPTYPE_CLEAN;
                        break;
                }
            }
        }, 200);
    }

    public void preAlertAction(View button) {
        String orderReqStatus = "";
        if (!"01".equals(orderStatus) && !"21".equals(orderStatus) && !"13".equals(orderStatus)) {
            return;
        } else {
            if ("01".equals(orderStatus)) {
                orderReqStatus = "02";
            } else if ("21".equals(orderStatus)) {
                orderReqStatus = "22";
            } else if ("13".equals(orderStatus)) {
                orderReqStatus = "22";
            }
        }
        modStatus(orderReqStatus);
    }

    private void modStatus(final String _orderStatus) {
        showDialog();
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("orderStatus", _orderStatus);
        params.put("k", 1);
        NetworkClient.post(Constants.MOD_ORDER_STATUS, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.MOD_ORDER_STATUS, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.MOD_ORDER_STATUS, result);

                    JSONObject jsondata = new JSONObject(result);
                    dismissDialog();
                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        aq.id(R.id.pre_alert_area).gone();
                        if ("02".equals(_orderStatus) || "22".equals(_orderStatus)) {
                            orderStatus = _orderStatus;
                            aq.id(R.id.below_layout).visible();
                            aq.id(R.id.alert_layout).gone();
                            setStatusScreen(_orderStatus);
                        } else if ("23".equals(_orderStatus)) {
                            ToastUtility.show(mCtx, "배송완료 처리되었습니다", Toast.LENGTH_SHORT);
                            orderStatus = _orderStatus;
                            finish();
                        }

                        if (jsondata.isNull("data")) {
                            //

                        } else {
                            //JSONArray locationInfo = jsondata.getJSONArray("data");

                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }


    /*
        수거완료 처리
     */
    private void submitPickup() {
        showDialog();
//
//        if (getTotalPrice() < 0) {
//            DUtil.alertShow(this, "총금액이 만원이하 입니다.");
//            dismissDialog();
//            return;
//        }

        String api_url = "";
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);

        // 2015-11-06
        // 세탁소로 이동중 -> 세탁준비중 으로 단계축소처리
        //params.put("orderStatus", "03");
        if ("Y".equals(isDeliveryMan) && ("02".equals(orderStatus) || "01".equals(orderStatus))) {
            params.put("orderStatus", "03");
            params.put("pickupQuantity", pickQuantity);
            api_url = Constants.ORDER_PICKUP_COMPLETE;
            if (pickQuantity == 0) {
                DUtil.alertShow(this, "수량을 추가해주세요.");
                dismissDialog();
                return;
            }
        } else {
            // 수량입력후 품목 등록
            //params.put("orderStatus", "11");
            params.put("items", pickupItemsV2.toString());
            // V2 API로 교체
            api_url = Constants.ORDER_PICKUP_COMPLETE_V2;

            if (pickupItemsV2.length() == 0) {
                DUtil.alertShow(this, "세탁물을 추가해주세요.");
                dismissDialog();
                return;
            }
        }

        params.put("k", 1);
        Log.i("SUBMIT PARAM", params.toString());

        NetworkClient.post(api_url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_PICKUP_COMPLETE, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_PICKUP_COMPLETE, result);

                    dismissDialog();
                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //
                            //Toast.makeText(getApplicationContext(), "접수를 완료했습니다.", Toast.LENGTH_LONG).show();
                        } else {
                            //JSONArray locationInfo = jsondata.getJSONArray("data");
                            Toast.makeText(getApplicationContext(), "접수를 완료했습니다.", Toast.LENGTH_LONG).show();

                            getRemoteData(orderId);
                            //finish();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), jsondata.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void addCompleteAction(View button) {

        try {
            JSONArray arr = new JSONArray(pickupItems);
            JSONArray arr2 = new JSONArray(pickupRepairItems);
            if (arr.length() > 0 || arr2.length() > 0) {
                submitPickup();
            } else {
                if ("Y".equals(isDeliveryMan)) {
                    submitPickup();
                } else {
                    DUtil.alertShow(this, "수거항목을 추가해주세요.");
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private int getTotalPrice() {
        int totalPrice = 0;
        try {
            //JSONArray arr1 = new JSONArray(pickupItems);

            for (int i = 0; i < pickupItemsV2.length(); i++) {
                int price = pickupItemsV2.getJSONObject(i).getInt("confirmPrice");
                totalPrice += price;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        //
        if (totalPrice < 20000 && totalPrice != 0) {
            totalPrice += 2000;
        }
        return totalPrice;
    }

    public void orderCompleteAction(View button) {
        //modStatus("23");

        if ("Y".equals(isPayment)) {

            if (payments.length() > 0) {
                if ("P91".equals(paymentType)) {
                    // 미수금 경고창
                    //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_card);
                    orderNotPaymentPopup();
                } else if ("P92".equals(paymentType)) {
                    // 미수금 경고창
                    //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_cash);
                    orderNotPaymentPopup();
                } else {
                    //
                    modStatus("23");
                    //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_already);
                }
            } else {
                //aq.id(R.id.paid_info_layout).gone();
                // 미수금 경고창
                orderNotPaymentPopup();
            }

        } else {
            // 미수금 경고창
            orderNotPaymentPopup();
        }
    }

    public void orderNotPaymentPopup(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(true).setMessage("결제가 완료되지 않은 주문 건입니다. 현금(혹은 현장카드결제)을 받으셨나요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        modStatus("23");
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                noPaymentDeliveryCompleteAction();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void noPaymentDeliveryCompleteAction(){
        showDialog();
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.NO_PAYMENT_STAY, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.NO_PAYMENT_STAY, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.NO_PAYMENT_STAY, result);

                    JSONObject jsondata = new JSONObject(result);
                    dismissDialog();
                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        aq.id(R.id.pre_alert_area).gone();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                        alertDialogBuilder.setCancelable(true).setMessage("배송은 완료되었지만, 미수금이 있어서 \"완료지연(미수금)\" 으로 표기됩니다.\n" +
                                "고객님 결제 완료 시 자동으로 \"배송완료\" 처리됩니다. ")
                                .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }
}
