package me.rewhite.delivery.tablet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;

public class BizNoModActivity extends TabletBaseActivity {

    TextView input_search_text;
    public final Context mCtx = this;
    AQuery aq;

    ProgressDialog mProgressDialog;

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(BizNoModActivity.this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(CommonUtility.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablet_activity_biz_no);

        aq = new AQuery(this);
        aq.hardwareAccelerated11();

        aq.id(R.id.btn_close).clicked(this, "closeClicked");
        aq.id(R.id.btn_submit).clicked(this, "submitAction");
    }

    public void submitAction(View button){
        submitBusinessNo();
    }

    public void submitBusinessNo(){
        String bizno = aq.id(R.id.text_bizno).getText().toString();
        String serial = aq.id(R.id.text_hw_serial).getText().toString();

        if(bizno.length() == 10 && serial.length() > 8){
            Intent resultData = new Intent();
            resultData.putExtra("bizNo", bizno);
            resultData.putExtra("hwSerial", serial);
            setResult(Activity.RESULT_OK, resultData);
            finish();
        }else{
            DUtil.alertShow(this, "사업자번호는 10자리입니다. 입력하신 내용을 다시 확인해주세요.");
        }

    }

    public void closeClicked(View button){
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

}
