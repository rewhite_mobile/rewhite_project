package me.rewhite.delivery.tablet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class TabletAddItemActivity extends TabletBaseActivity implements AdapterView.OnItemClickListener{

    private static final String TAG = "TabletAddItemActivity";
    public final Context mCtx = this;
    AQuery aq;
    String currentType = "";
    GridView gv;
    GridView gvSub;
    int categoryId;
    int itemClassId;

    ProgressDialog mProgressDialog;

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(TabletAddItemActivity.this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(CommonUtility.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablet_activity_add_items);

        Intent intent = getIntent();
        if (intent != null) {
            currentType = intent.getStringExtra("type");
        }

        Log.w(TAG, "onCreate");

        aq = new AQuery(this);
        aq.hardwareAccelerated11();

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.product_name).getTextView());

        aq.id(R.id.btn_close).clicked(this, "closeClicked");
        aq.id(R.id.btn_submit).clicked(this, "submitClicked");

        gv = (GridView)findViewById(R.id.gridView);
        gvSub = (GridView)findViewById(R.id.subGridView);
        if (gv != null) {
            gv.setOnItemClickListener(this);
            gvSub.setOnItemClickListener(this);
        }

        initialize();
    }

    JSONArray filteredMainArray;
    JSONArray filteredSubArray;

    private JSONArray getCategoryData(int categoryId) throws JSONException {
        JSONArray temp = new JSONArray();
        for(int i = 0; i < filteredSubArray.length(); i++){
            if(filteredSubArray.getJSONObject(i).getInt("categoryId") == categoryId){
                temp.put(filteredSubArray.getJSONObject(i));
            }
        }
        return temp;
    }


    private void initialize(){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("categoryType", currentType);
        params.put("k", 1);
        NetworkClient.post(Constants.GET_ITEM_CATEGORY, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                Log.e(Constants.GET_ITEM_CATEGORY, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_ITEM_CATEGORY, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {

                        } else {
                            filteredMainArray = new JSONArray();
                            filteredSubArray = new JSONArray();

                            for(int i = 0; i < jsondata.getJSONArray("data").length(); i++){
                                if(jsondata.getJSONArray("data").getJSONObject(i).getInt("itemClassId") == 0){
                                    filteredMainArray.put(jsondata.getJSONArray("data").getJSONObject(i));
                                }else{
                                    filteredSubArray.put(jsondata.getJSONArray("data").getJSONObject(i));
                                }
                            }

                            // 커스텀 아답타 생성
                            mainAdapter = new ColorPickAdapter (mCtx,
                                    R.layout.tablet_pui_parts_layout_row,       // GridView 항목의 레이아웃 row.xml
                                    filteredMainArray);    // 데이터
                            gv.setAdapter(mainAdapter);  // 커스텀 아답타를 GridView 에 적용
                        }
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });


    }


    public void submitClicked(View button){

        if(aq.id(R.id.item_title_input).getText().toString().length() < 2){
            DUtil.alertShow(this, "품목명을 2자이상 입력해주세요.");
            return;
        }

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("categoryId", categoryId);
        params.put("itemClassId", itemClassId);
        params.put("itemType", currentType);
        params.put("itemTitle", aq.id(R.id.item_title_input).getText().toString());
        params.put("k", 1);
        NetworkClient.post(Constants.ADD_PRICE_ITEM, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ADD_PRICE_ITEM, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ADD_PRICE_ITEM, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {

                        } else {
                            Intent resultData = new Intent();
                            setResult(Activity.RESULT_OK, resultData);
                            finish();

                        }
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }


    public void closeClicked(View button){
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    String mainSelectedString = "";
    String subSelectedString = "";

    ColorPickAdapter mainAdapter;
    ColorPickAdapter subAdapter;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if(parent.equals(gv)){
            //ColorPickAdapter tempAdapter = (ColorPickAdapter)parent.getAdapter();
            mainAdapter.setSelectedItemPosition(position);
            mainAdapter.notifyDataSetChanged();

            Log.e("gridView clicked", position + "");
            JSONObject sItem = mainAdapter.getItem(position);
            try {
                aq.id(R.id.text_subtitle).text(sItem.getString("itemClassTitle"));
                mainSelectedString = sItem.getString("itemClassTitle");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                int cateId = filteredMainArray.getJSONObject(position).getInt("categoryId");
                JSONArray specificCategoryData = getCategoryData(cateId);

                // 커스텀 아답타 생성
                subAdapter = new ColorPickAdapter (mCtx,
                        R.layout.tablet_pui_parts_layout_row,       // GridView 항목의 레이아웃 row.xml
                        specificCategoryData);    // 데이터
                gvSub.setAdapter(subAdapter);  // 커스텀 아답타를 GridView 에 적용
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            Log.e("gridSubView clicked", position + " / " + parent.getAdapter().getItem(position));
            //ColorPickAdapter tempAdapter = (ColorPickAdapter)parent.getAdapter();
            subAdapter.setSelectedItemPosition(position);
            subAdapter.notifyDataSetChanged();

            try {
                JSONObject subItem = subAdapter.getItem(position);
                subSelectedString = subItem.getString("itemClassTitle");
                categoryId = subItem.getInt("categoryId");
                itemClassId = subItem.getInt("itemClassId");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            aq.id(R.id.input_area).visible();
            aq.id(R.id.select_area).gone();

            aq.id(R.id.text_selected).text(mainSelectedString + " > " + subSelectedString);
        }

    }

    class ColorPickAdapter extends BaseAdapter {
        Context context;
        int layout;
        JSONArray cates;
        LayoutInflater inf;
        private int mSelectedPosition = -1;

        public ColorPickAdapter(Context context, int layout, JSONArray cates) {
            this.context = context;
            this.layout = layout;
            this.cates = cates;
            inf = (LayoutInflater) context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return cates.length();
        }

        @Override
        public JSONObject getItem(int position) {
            JSONObject obj = new JSONObject();
            try {
                obj = cates.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return obj;
        }

        public void setSelectedItemPosition(int position) {
            mSelectedPosition = position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView==null)
                convertView = inf.inflate(layout, null);


            try {
                if(mSelectedPosition == position) {
                    convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.tablet_pui_button_drawable_pressed);
                } else {
                    convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.tablet_pui_button_drawable_normal);
                }

//                if(convertView.isSelected()){
//                    convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.tablet_pui_button_drawable_pressed);
//                    //((TextView)convertView.findViewById(R.id.parts_title)).setTextColor(0xFFFFFFFF);
//                }else{
//                    convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.tablet_pui_button_drawable_normal);
//                    //((TextView)convertView.findViewById(R.id.parts_title)).setTextColor(0xFFFFFFFF);
//                }
                TextView iv = (TextView)convertView.findViewById(R.id.parts_title);
                CommonUtility.setTypefaceBoldSetup(iv);
                iv.setText(cates.getJSONObject(position).getString("itemClassTitle"));
                //@drawable/pui_button_drawable_normal

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return convertView;
        }
    }

}
