package me.rewhite.delivery.tablet;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.androidquery.AQuery;

import org.apache.http.util.EncodingUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.util.CommonUtility;

@SuppressLint({ "NewApi", "JavascriptInterface", "SetJavaScriptEnabled" })
public class TabletMonthlyReportActivity extends TabletBaseActivity {

    private AQuery aq;
    WebView webView;
    ProgressDialog dialog;
    public Context ctx = this;

    String currentTerms;
    String currentName;
    String pKey;
    String name;
    String type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablet_html);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("pKey") != null) {
            }
            pKey = intent.getStringExtra("pKey");
            name = intent.getStringExtra("name");
            type = intent.getStringExtra("type");

            Log.e("pKey", pKey + "");
        }

        aq = new AQuery(this);

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.title_text).getTextView());

        aq.id(R.id.title_text).text(name);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        if("R".equals(type)){
            aq.id(R.id.icon_image).image(R.mipmap.icon_top_finance_rewhite);
        }else{
            aq.id(R.id.icon_image).image(R.mipmap.icon_top_finance_local);
        }

        webView = (WebView)findViewById(R.id.webview_area);
        webView.setWebViewClient(new MyWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.clearCache(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);


        String postData = null;
        try {
            postData = "p=" + URLEncoder.encode(pKey, "UTF-8");
            Log.e("key", postData);
            webView.postUrl(Constants.SHOW_MONTHLY_FINANCE, EncodingUtils.getBytes(postData, "BASE64"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }




        //aq.id(R.id.top).gone();
		/*
		aq.id(R.id.title_name).text("FAQ");
		aq.id(R.id.btn_setting).clicked(this, "settingStart");
		aq.id(R.id.btn_info).clicked(this, "infoStart");*/

        // Progress 처리 진행상황을 보기위해
        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.web_loading_comment));
    }
    boolean loadingFinished = true;
    boolean redirect = false;

    public void closeClicked(View button){
        finish();
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
            if (!loadingFinished) {
                redirect = true;
            }

            loadingFinished = false;
            view.loadUrl(urlNewString);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap facIcon) {
            loadingFinished = false;
            // SHOW LOADING IF IT ISNT ALREADY VISIBLE
            dialog.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (!redirect) {
                loadingFinished = true;
            }

            if (loadingFinished && !redirect) {
                // HIDE LOADING IT HAS FINISHED
                dialog.dismiss();
            } else {
                redirect = false;
            }

        }
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }
    }
}
