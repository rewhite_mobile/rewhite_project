package me.rewhite.delivery.tablet;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.TabletFragment.TabletUserAllFragment;
import me.rewhite.delivery.TabletFragment.TabletUserCouponFragment;
import me.rewhite.delivery.TabletFragment.TabletUserLongtimeFragment;
import me.rewhite.delivery.TabletFragment.TabletUserReceivableFragment;
import me.rewhite.delivery.TabletFragment.TabletUserSearchFragment;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.util.CommonUtility;

public class TabletUserManageActivity extends TabletBaseActivity {

    private final static String TAG = TabletUserManageActivity.class.getSimpleName();
    public static final String FRAGMENT_ALL = "all";
    public static final String FRAGMENT_VIP = "vip";
    public static final String FRAGMENT_LONGTIME = "longtime";
    public static final String FRAGMENT_RECEIVABLE = "receivable";
    public static final String FRAGMENT_COUPON = "coupon";
    public static final String FRAGMENT_SEARCH = "search";
    AQuery aq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablet_user_manage);

        aq = new AQuery(this);
        aq.id(R.id.btn_back).clicked(this, "closeClicked");

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.screen_title).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_1).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_2).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_3).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_4).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_5).getTextView());

        aq.id(R.id.btn_1).clicked(this, "showData").tag(0).background(R.mipmap.tablet_subnav_selected);//.typeface(CommonUtility.getNanumBarunBoldTypeface());
        aq.id(R.id.btn_2).clicked(this, "showData").tag(1).background(R.mipmap.tablet_subnav_normal);//.typeface(CommonUtility.getNanumBarunBoldTypeface());
        aq.id(R.id.btn_3).clicked(this, "showData").tag(2).background(R.mipmap.tablet_subnav_normal);//.typeface(CommonUtility.getNanumBarunBoldTypeface());
        aq.id(R.id.btn_4).clicked(this, "showData").tag(3).background(R.mipmap.tablet_subnav_normal);//.typeface(CommonUtility.getNanumBarunBoldTypeface());
        aq.id(R.id.btn_5).clicked(this, "showData").tag(4).background(R.mipmap.tablet_subnav_normal);//.typeface(CommonUtility.getNanumBarunBoldTypeface());

        aq.id(R.id.rep_icon).image(R.mipmap.icon_top_customer_all);

        showFragment(FRAGMENT_ALL);
    }

    public void closeClicked(View button){
        finish();
    }

    public void showData(View button){
        int tag = (int)((Button)button).getTag();
        switch (tag){
            case 0:
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("전체 고객 목록");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_customer_all);
                showFragment(FRAGMENT_ALL);
                break;
            case 1:
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("장기보관 고객");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_customer_delay);

                showFragment(FRAGMENT_LONGTIME);
                break;
            case 2:
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("미수금 고객");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_customer_receivable);
                showFragment(FRAGMENT_RECEIVABLE);
                break;
            case 3:
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("쿠폰발행");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_customer_coupon);
                showFragment(FRAGMENT_COUPON);
                break;
            case 4:
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("회원 조회");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_customer_search);
                showFragment(FRAGMENT_SEARCH);
                break;

        }

    }

    public void showFragment(String fragmentName) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);
        showFragment(fragmentName, null, null);
    }

    String transitionName = "";

    public void showFragment(String fragmentName, View sharedElement, Bundle args) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(fragmentName);

        if (FRAGMENT_ALL.equals(fragmentName)) {
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletUserAllFragment();
        }else if(FRAGMENT_LONGTIME.equals(fragmentName)){
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletUserLongtimeFragment();
        }else if(FRAGMENT_RECEIVABLE.equals(fragmentName)){
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletUserReceivableFragment();
        }else if(FRAGMENT_COUPON.equals(fragmentName)){
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletUserCouponFragment();
        }else if(FRAGMENT_SEARCH.equals(fragmentName)){
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletUserSearchFragment();
        }

        Log.e(TAG, "[TabletStoreInfoFragment] Create : " + fragmentName);

        if (null != args) {
            fragment.setArguments(args);
        }

        //ft.setCustomAnimations(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        ft.replace(R.id.content_frame, fragment, fragmentName);
        ft.addToBackStack(fragmentName);
        //ft.commitAllowingStateLoss();
        ft.commit();

    }


    public void itemSelected(int position, String _url, String _title) {

    }

}
