package me.rewhite.delivery.tablet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.TabletFragment.TabletAdditionalPriceTableFragment;
import me.rewhite.delivery.TabletFragment.TabletDeliveryPriceTableFragment;
import me.rewhite.delivery.TabletFragment.TabletPartsPriceTableFragment;
import me.rewhite.delivery.TabletFragment.TabletPriceTableFragment;
import me.rewhite.delivery.TabletFragment.TabletRepairPriceTableFragment;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class TabletPriceSettingActivity extends TabletBaseActivity {

    private final static String TAG = TabletPriceSettingActivity.class.getSimpleName();
    public static final String FRAGMENT_PRICE_TABLE = "price_table";
    public static final String FRAGMENT_REPAIR_PRICE_TABLE = "repair_price_table";
    public static final String FRAGMENT_DELIVERY_PRICE_TABLE = "delivery_price_table";
    public static final String FRAGMENT_ADDITIONAL_PRICE_TABLE = "additional_price_table";
    public static final String FRAGMENT_PARTS_PRICE_TABLE = "parts_price_table";
    AQuery aq;
    private JSONObject storeInfo;

    ProgressDialog mProgressDialog;
    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablet_price_settings);

        showDialog();

        aq = new AQuery(this);
        aq.id(R.id.btn_back).clicked(this, "closeClicked");

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.screen_title).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_1).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_2).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_3).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_4).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_5).getTextView());

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_new_item).getTextView());

        aq.id(R.id.screen_title).text("세탁 요금표");

        aq.id(R.id.btn_1).clicked(this, "showData").tag(0).background(R.mipmap.tablet_subnav_selected);
        aq.id(R.id.btn_2).clicked(this, "showData").tag(1).background(R.mipmap.tablet_subnav_normal);
        aq.id(R.id.btn_3).clicked(this, "showData").tag(2).background(R.mipmap.tablet_subnav_normal);
        aq.id(R.id.btn_4).clicked(this, "showData").tag(3).background(R.mipmap.tablet_subnav_normal);
        aq.id(R.id.btn_5).clicked(this, "showData").tag(4).background(R.mipmap.tablet_subnav_normal);

        aq.id(R.id.rep_icon).image(R.mipmap.icon_top_price_clean);

        aq.id(R.id.btn_new_item).clicked(this, "addNewItem");

        getStoreInfo();
    }

    public void addNewItem(View button){

        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setClass(this, TabletAddItemActivity.class);

        if(currentIndex == 0){
            intent.putExtra("type", "1");
            startActivityForResult(intent, ADD_ITEM_G_MOD_COMPLETED);
        }else if(currentIndex == 1){
            intent.putExtra("type", "2");
            startActivityForResult(intent, ADD_ITEM_R_MOD_COMPLETED);
        }else if(currentIndex == 2){
            intent.putExtra("type", "3");
            startActivityForResult(intent, ADD_ITEM_D_MOD_COMPLETED);
        }else if(currentIndex == 3){
            intent.putExtra("type", "5");
            startActivityForResult(intent, ADD_ITEM_Z_MOD_COMPLETED);
        }
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

    }

    int currentIndex = 0;
    public void showData(View button){
        int tag = (int)((Button)button).getTag();
        currentIndex=tag;
        switch (tag){
            case 0:
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("세탁 요금표");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_price_clean);
                showFragment(FRAGMENT_PRICE_TABLE);
                break;
            case 1:
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_price_repair);
                aq.id(R.id.screen_title).text("수선 요금표");
                showFragment(FRAGMENT_REPAIR_PRICE_TABLE);
                break;
            case 2:
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_price_delivery);
                aq.id(R.id.screen_title).text("택배 요금표");
                showFragment(FRAGMENT_DELIVERY_PRICE_TABLE);
                break;
            case 3:
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_price_additional);
                aq.id(R.id.screen_title).text("추가기술");
                showFragment(FRAGMENT_ADDITIONAL_PRICE_TABLE);
                break;
            case 4:
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_price_parts);
                aq.id(R.id.screen_title).text("부속품");
                showFragment(FRAGMENT_PARTS_PRICE_TABLE);
                break;
        }

    }

    public void closeClicked(View button){
        finish();
    }

    public void showFragment(String fragmentName) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);
        showFragment(fragmentName, null, null);
    }

    String transitionName = "";

    public void showFragment(String fragmentName, View sharedElement, Bundle args) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(fragmentName);

        if(FRAGMENT_PRICE_TABLE.equals(fragmentName)){
            fragment = new TabletPriceTableFragment();
        }else if(FRAGMENT_REPAIR_PRICE_TABLE.equals(fragmentName)){
            fragment = new TabletRepairPriceTableFragment();
        }else if(FRAGMENT_DELIVERY_PRICE_TABLE.equals(fragmentName)){
            fragment = new TabletDeliveryPriceTableFragment();
        }else if(FRAGMENT_ADDITIONAL_PRICE_TABLE.equals(fragmentName)){
            fragment = new TabletAdditionalPriceTableFragment();
        }else if(FRAGMENT_PARTS_PRICE_TABLE.equals(fragmentName)){
            fragment = new TabletPartsPriceTableFragment();
        }

        Log.e(TAG, "[TabletStoreInfoFragment] Create : " + fragmentName);


        //ft.setCustomAnimations(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        ft.replace(R.id.content_frame, fragment, fragmentName);
        ft.addToBackStack(null);
        ft.commitAllowingStateLoss();
        //ft.commit();

    }

    private void getStoreInfo(){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.GET_STORE_INFORMATION, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GET_STORE_INFORMATION, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_STORE_INFORMATION, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        storeInfo = jsondata.getJSONObject("data");
                        String storePriceRate1 = storeInfo.getString("storePriceRate1");
                        String storePriceRate2 = storeInfo.getString("storePriceRate2");

                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.STORE_RATE_1, storePriceRate1);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.STORE_RATE_2, storePriceRate2);

                        showFragment(FRAGMENT_PRICE_TABLE);
                    }
                    dismissDialog();
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private static final int PRICE_G_MOD_COMPLETED = 101;
    private static final int PRICE_R_MOD_COMPLETED = 201;
    private static final int PRICE_D_MOD_COMPLETED = 301;
    private static final int PRICE_N_MOD_COMPLETED = 102;
    private static final int PRICE_T_MOD_COMPLETED = 103;
    private static final int PRICE_Z_MOD_COMPLETED = 501;

    private static final int ADD_ITEM_G_MOD_COMPLETED = 1101;
    private static final int ADD_ITEM_R_MOD_COMPLETED = 1201;
    private static final int ADD_ITEM_D_MOD_COMPLETED = 1301;
    private static final int ADD_ITEM_Z_MOD_COMPLETED = 1501;


    public void priceModProcess(Bundle args){
        Log.e("priceMod", args.toString());

        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setClass(this, TabletPriceModActivity.class);
        intent.putExtra("groupPosition", args.getInt("groupPosition"));
        intent.putExtra("childPosition", args.getInt("childPosition"));

        if(currentIndex == 0){
            intent.putExtra("type", "101");
            intent.putExtra("data", args.getString("data"));
            startActivityForResult(intent, PRICE_G_MOD_COMPLETED);
        }else if(currentIndex == 1){
            intent.putExtra("type", "201");
            intent.putExtra("data", args.getString("data"));
            startActivityForResult(intent, PRICE_R_MOD_COMPLETED);
        }else if(currentIndex == 2){
            intent.putExtra("type", "301");
            intent.putExtra("data", args.getString("data"));
            startActivityForResult(intent, PRICE_D_MOD_COMPLETED);
        }else if(currentIndex == 3){
            intent.putExtra("type", "501");
            intent.putExtra("data", args.getString("data"));
            startActivityForResult(intent, PRICE_Z_MOD_COMPLETED);
        }

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    // 가격표 사용여부 수정
    public void useModProcess(Bundle args){
        Log.e("useMod", args.toString());

        int groupPosition = args.getInt("groupPosition");
        int childPosition = args.getInt("childPosition");
        try {
            JSONObject data = new JSONObject(args.getString("data"));
            String isUse = "";
            if("Y".equals(data.getString("isUse"))){
                isUse = "N";
            }else{
                isUse = "Y";
            }

            modUseStatus(data.getInt("itemId"), isUse, data, groupPosition, childPosition);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // 부속품 사용여부 수정
    public void usePartModProcess(Bundle args){
        Log.e("usePartMod", args.toString());

        int position = args.getInt("position");
        try {
            JSONObject data = new JSONObject(args.getString("data"));
            String isUse = "";
            if("Y".equals(data.getString("isUse"))){
                isUse = "N";
            }else{
                isUse = "Y";
            }

            modPartUseStatus(data.getInt("partId"), isUse, data, position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void modUseStatus(int itemId, final String isUse, final JSONObject jdata, final int groupPosition, final int childPosition){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("itemId", itemId);
        params.put("isUse", isUse);
        if(currentIndex==0){
            params.put("priceType", "101");
        }else if(currentIndex==1){
            params.put("priceType", "201");
        }else if(currentIndex==2){
            params.put("priceType", "301");
        }else if(currentIndex==3){
            params.put("priceType", "501");
        }

        try {
            params.put("storePrice1", jdata.getInt("storePrice1"));
            params.put("storePrice2", jdata.getInt("storePrice2"));
            params.put("storePrice3", jdata.getInt("storePrice3"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        params.put("k", 1);

        Log.e("modUseStatus req", params.toString());

        NetworkClient.post(Constants.SET_PRICE_ITEM, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SET_PRICE_ITEM, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.SET_PRICE_ITEM, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {

                        } else {
                            if(currentIndex==0){
                                TabletPriceTableFragment rf =  (TabletPriceTableFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                                if(rf!=null){
                                    rf.refreshListView(groupPosition, childPosition, jdata.getInt("storePrice1"), jdata.getInt("storePrice2"), jdata.getInt("storePrice3"), jdata.getInt("storePrice4"), isUse);
                                }
                            }else if(currentIndex==1){
                                TabletRepairPriceTableFragment rf =  (TabletRepairPriceTableFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                                if(rf!=null){
                                    rf.refreshListView(groupPosition, childPosition, jdata.getInt("storePrice1"), jdata.getInt("storePrice2"), jdata.getInt("storePrice3"), jdata.getInt("storePrice4"), isUse);
                                }
                            }else if(currentIndex==2){
                                TabletDeliveryPriceTableFragment rf =  (TabletDeliveryPriceTableFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                                if(rf!=null){
                                    rf.refreshListView(groupPosition, childPosition, jdata.getInt("storePrice1"), jdata.getInt("storePrice2"), jdata.getInt("storePrice3"), jdata.getInt("storePrice4"), isUse);
                                }
                            }else if(currentIndex==3){
                                TabletAdditionalPriceTableFragment rf =  (TabletAdditionalPriceTableFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                                if(rf!=null){
                                    rf.refreshListView(groupPosition, childPosition, jdata.getInt("storePrice1"), jdata.getInt("storePrice2"), jdata.getInt("storePrice3"), jdata.getInt("storePrice4"), isUse);
                                }
                            }

                        }
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private void modPartUseStatus(int partId, final String isUse, final JSONObject jdata, final int position){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("partId", partId);
        params.put("isUse", isUse);
        try {
            params.put("partTitle", jdata.getString("partTitle"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        params.put("k", 1);

        Log.e("modPartUseStatus req", params.toString());

        NetworkClient.post(Constants.SET_PARTS_ITEM, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SET_PARTS_ITEM, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.SET_PARTS_ITEM, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {

                        } else {
                            TabletPartsPriceTableFragment rf =  (TabletPartsPriceTableFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                            if(rf!=null){
                                rf.refreshListView(position, isUse);
                            }

                        }
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            int groupPosition = data.getIntExtra("groupPosition", 0);
            int childPosition = data.getIntExtra("childPosition", 0);
            int storePrice1 = data.getIntExtra("storePrice1", 0);
            int storePrice2 = data.getIntExtra("storePrice2", 0);
            int storePrice3 = data.getIntExtra("storePrice3", 0);
            int storePrice4 = data.getIntExtra("storePrice4", 0);
            String isUse = data.getStringExtra("isUse");

            Log.e("onActivityResult", "OK");

            switch (requestCode) {
                case PRICE_G_MOD_COMPLETED:
                    TabletPriceTableFragment rf1 =  (TabletPriceTableFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if(rf1!=null){
                        rf1.refreshListView(groupPosition, childPosition, storePrice1, storePrice2, storePrice3, storePrice4, isUse);
                    }
                    break;
                case PRICE_R_MOD_COMPLETED:
                    TabletRepairPriceTableFragment rf2 =  (TabletRepairPriceTableFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if(rf2!=null){
                        rf2.refreshListView(groupPosition, childPosition, storePrice1, storePrice2, storePrice3, storePrice4, isUse);
                    }
                    break;
                case PRICE_D_MOD_COMPLETED:
                    TabletDeliveryPriceTableFragment rf3 =  (TabletDeliveryPriceTableFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if(rf3!=null){
                        rf3.refreshListView(groupPosition, childPosition, storePrice1, storePrice2, storePrice3, storePrice4, isUse);
                    }
                    break;
                case PRICE_Z_MOD_COMPLETED:
                    TabletAdditionalPriceTableFragment rf4 =  (TabletAdditionalPriceTableFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if(rf4!=null){
                        rf4.refreshListView(groupPosition, childPosition, storePrice1, storePrice2, storePrice3, storePrice4, isUse);
                    }
                    break;
                case ADD_ITEM_G_MOD_COMPLETED:
                    TabletPriceTableFragment rf5 =  (TabletPriceTableFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if(rf5!=null){
                        rf5.initialize();
                    }
                    break;
                case ADD_ITEM_R_MOD_COMPLETED:
                    TabletRepairPriceTableFragment rf6 =  (TabletRepairPriceTableFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if(rf6!=null){
                        rf6.initialize();
                    }
                    break;
                case ADD_ITEM_D_MOD_COMPLETED:
                    TabletDeliveryPriceTableFragment rf7 =  (TabletDeliveryPriceTableFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if(rf7!=null){
                        rf7.initialize();
                    }
                    break;
                case ADD_ITEM_Z_MOD_COMPLETED:
                    TabletAdditionalPriceTableFragment rf8 =  (TabletAdditionalPriceTableFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if(rf8!=null){
                        rf8.initialize();
                    }
                    break;
            }
        }else{

        }
    }
}
