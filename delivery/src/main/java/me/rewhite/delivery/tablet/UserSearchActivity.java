package me.rewhite.delivery.tablet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.TabletUserListItem;
import me.rewhite.delivery.adapter.TabletUserListViewAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class UserSearchActivity extends TabletBaseActivity {

    private static final String TAG = "UserSearchActivity";
    TextView input_search_text;
    public final Context mCtx = this;
    AQuery aq;
    int currentType = 0;

    private static final int ADD_USER_RESULT = 9;

    ProgressDialog mProgressDialog;
    String searchType = "INPUT";

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(UserSearchActivity.this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(CommonUtility.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablet_activity_user_search);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("type") != null) {

            }
            searchType = intent.getStringExtra("type");
        }

        Log.w(TAG, "onCreate");

        aq = new AQuery(this);
        aq.hardwareAccelerated11();

        if(!"INPUT".equals(searchType)){
            aq.id(R.id.btn_add_user).gone();
        }

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.input_search_phone).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.input_search).getTextView());

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.btn_search).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.btn_search2).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.btn_num_del).getTextView());

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_empty_desc).getTextView());

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_search_phone).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_search_name).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_search_orderno).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_search_tagno).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.btn_add_user).getTextView());

        aq.id(R.id.btn_close).clicked(this, "closeClicked");

        aq.id(R.id.btn_num_0).clicked(this, "numPadClicked").tag(0);
        aq.id(R.id.btn_num_1).clicked(this, "numPadClicked").tag(1);
        aq.id(R.id.btn_num_2).clicked(this, "numPadClicked").tag(2);
        aq.id(R.id.btn_num_3).clicked(this, "numPadClicked").tag(3);
        aq.id(R.id.btn_num_4).clicked(this, "numPadClicked").tag(4);
        aq.id(R.id.btn_num_5).clicked(this, "numPadClicked").tag(5);
        aq.id(R.id.btn_num_6).clicked(this, "numPadClicked").tag(6);
        aq.id(R.id.btn_num_7).clicked(this, "numPadClicked").tag(7);
        aq.id(R.id.btn_num_8).clicked(this, "numPadClicked").tag(8);
        aq.id(R.id.btn_num_9).clicked(this, "numPadClicked").tag(9);

        aq.id(R.id.btn_num_del).clicked(this, "numDelClicked");

        aq.id(R.id.btn_search_phone).clicked(this, "typeClicked").tag(0);
        aq.id(R.id.btn_search_name).clicked(this, "typeClicked").tag(1);
        aq.id(R.id.btn_search_orderno).clicked(this, "typeClicked").tag(2);
        aq.id(R.id.btn_search_tagno).clicked(this, "typeClicked").tag(3);

        aq.id(R.id.btn_search).clicked(this, "searchClicked").tag(0);
        aq.id(R.id.btn_search2).clicked(this, "searchClicked").tag(1);

        aq.id(R.id.btn_add_user).clicked(this, "addUserClicked");


        aq.id(R.id.bottom_area).visible();
        setEmptyResult(true,false);


    }

    public void addUserClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setClass(mCtx, UserAddActivity.class);
                startActivityForResult(intent, ADD_USER_RESULT);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }, 0);
    }

    private void search(int _type, String _keyword){
        if(_type == 0){
            searchUser(_keyword, null);
        }else if(_type == 1){
            searchUser(_keyword, null);
        }else if(_type == 2){
            searchUserByOrderId(_keyword);
        }else if(_type == 3){
            searchUser(_keyword, null);
        }
    }

    private TabletUserListViewAdapter orderAdapter;
    private ArrayList<TabletUserListItem> userData;

    public void searchUser(String _keyword, @Nullable String searchType){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        if(searchType == null){
            if(currentType == 0){
                params.put("search", "phone");
            }else if(currentType == 1){
                params.put("search", "userName");
            }else if(currentType == 2){
                params.put("search", "userId");
            }else if(currentType == 3){
                params.put("search", "tagId");
            }
        }else{
            if("USERID".equals(searchType)){
                params.put("search", "userId");
            }else{
                return;
            }
        }

        params.put("keyword", _keyword);

        params.put("page", 1);
        params.put("block", 50);
        params.put("k", 1);
        NetworkClient.post(Constants.USER_SEARCH, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.USER_SEARCH, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.USER_SEARCH, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //
                            //aq.id(R.id.empty).gone();
                            setEmptyResult(true,true);
                        } else {
                            ListView orderListView = (ListView)findViewById(R.id.listView);
                            userData = new ArrayList<TabletUserListItem>();
                            JSONArray orderInfo = jsondata.getJSONArray("data");

                            for (int i = 0; i < orderInfo.length(); i++) {

                                String address1 = orderInfo.getJSONObject(i).getString("address1");
                                String address2 = orderInfo.getJSONObject(i).getString("address2");
                                String userName = orderInfo.getJSONObject(i).getString("userName");
                                String phone = orderInfo.getJSONObject(i).getString("phone");
                                String imageThumbPath = orderInfo.getJSONObject(i).getString("imageThumbPath");
                                String jsonData = orderInfo.getJSONObject(i).toString();

                                TabletUserListItem aItem = new TabletUserListItem(userName, address1, address2, phone, imageThumbPath, jsonData);
                                userData.add(aItem);

                                DUtil.Log("TabletUserListItem", "count : " + i);
                            }

                            if(userData.size() > 0){
                                aq.id(R.id.listView).visible();
                                setEmptyResult(false,true);
                            }else{
                                aq.id(R.id.listView).gone();
                                setEmptyResult(true,true);
                            }

                            orderAdapter = new TabletUserListViewAdapter(mCtx, R.layout.tablet_user_item, userData);
                            orderAdapter.notifyDataSetChanged();

                            orderListView.setAdapter(orderAdapter);
                        }

                    } else {
                        setEmptyResult(true,true);
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void searchUserByOrderId(String _search) {
        //ORDER_SUMMARY_COUNT
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("search", "orderId");
        params.put("mode", "Z");
        params.put("keyword", _search);
        params.put("page", 1);
        params.put("block", 50);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_LIST, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_LIST, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //
                            //aq.id(R.id.empty).gone();
                            setEmptyResult(true,true);
                        } else {
                            ListView orderListView = (ListView)findViewById(R.id.listView);
                            //orderData = new ArrayList<>();
                            JSONArray orderInfo = jsondata.getJSONArray("data");

                            if(orderInfo.length() > 0){
                                String userId = orderInfo.getJSONObject(0).getString("userId");
                                searchUser(userId, "USERID");
                                //aq.id(R.id.empty).gone();
                            }else{
                                //aq.id(R.id.empty).visible();
                                setEmptyResult(true,true);
                                aq.id(R.id.listView).gone();
                            }

                        }

                    } else {
                        setEmptyResult(true,true);
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void searchClicked(View button){
        switch (currentType){
            case 0:
                String inputPhoneStr = aq.id(R.id.input_search_phone).getText().toString();
                if(inputPhoneStr.length() >= 4){
                    search(0, inputPhoneStr);
                }else{
                    DUtil.alertShow(this, "전화번호 뒤 4자리를 입력해주세요.");
                }
                break;
            case 1:
                String inputNameStr = aq.id(R.id.input_search).getText().toString();
                if(inputNameStr.length() >= 1){
                    search(1, inputNameStr);
                }else{
                    DUtil.alertShow(this, "이름을 입력해주세요.");
                }
                break;
            case 2:
                String inputOrderNoStr = aq.id(R.id.input_search_phone).getText().toString();
                if(inputOrderNoStr.length() >= 5){
                    search(2, inputOrderNoStr);
                }else{
                    DUtil.alertShow(this, "주문번호는 5자리 이상입니다.");
                }

                break;
            case 5:
                String inputTagNoStr = aq.id(R.id.input_search_phone).getText().toString();
                if(inputTagNoStr.length() >= 1){
                    search(3, inputTagNoStr);
                }else{
                    DUtil.alertShow(this, "Tag번호를 입력해주세요");
                }

                break;
        }
    }

    public void typeClicked(View button){
        int tag = (int)button.getTag();
        currentType = tag;
        setEmptyResult(false,false);

        switch (tag){
            case 0:
                aq.id(R.id.btn_search_phone).backgroundColor(Color.parseColor("#dd2238"));
                aq.id(R.id.btn_search_name).backgroundColor(Color.parseColor("#091624"));
                aq.id(R.id.btn_search_orderno).backgroundColor(Color.parseColor("#091624"));
                aq.id(R.id.btn_search_tagno).backgroundColor(Color.parseColor("#091624"));
                aq.id(R.id.search_form_phone).visible();
                aq.id(R.id.search_form_name).gone();
                aq.id(R.id.num_pad).visible();

                aq.id(R.id.input_search_phone).text("").getTextView().setHint("전화번호 뒤 4자리");
                break;
            case 1:
                aq.id(R.id.btn_search_phone).backgroundColor(Color.parseColor("#091624"));
                aq.id(R.id.btn_search_name).backgroundColor(Color.parseColor("#dd2238"));
                aq.id(R.id.btn_search_orderno).backgroundColor(Color.parseColor("#091624"));
                aq.id(R.id.btn_search_tagno).backgroundColor(Color.parseColor("#091624"));
                aq.id(R.id.search_form_phone).gone();
                aq.id(R.id.search_form_name).visible();
                aq.id(R.id.num_pad).gone();


                break;
            case 2:
                aq.id(R.id.btn_search_phone).backgroundColor(Color.parseColor("#091624"));
                aq.id(R.id.btn_search_name).backgroundColor(Color.parseColor("#091624"));
                aq.id(R.id.btn_search_orderno).backgroundColor(Color.parseColor("#dd2238"));
                aq.id(R.id.btn_search_tagno).backgroundColor(Color.parseColor("#091624"));
                aq.id(R.id.search_form_phone).visible();
                aq.id(R.id.search_form_name).gone();
                aq.id(R.id.num_pad).visible();

                aq.id(R.id.input_search_phone).text("").getTextView().setHint("주문번호(5자리) 입력");
                break;
            case 3:
                aq.id(R.id.btn_search_phone).backgroundColor(Color.parseColor("#091624"));
                aq.id(R.id.btn_search_name).backgroundColor(Color.parseColor("#091624"));
                aq.id(R.id.btn_search_orderno).backgroundColor(Color.parseColor("#091624"));
                aq.id(R.id.btn_search_tagno).backgroundColor(Color.parseColor("#dd2238"));
                aq.id(R.id.search_form_phone).visible();
                aq.id(R.id.search_form_name).gone();
                aq.id(R.id.num_pad).visible();

                aq.id(R.id.input_search_phone).text("").getTextView().setHint("택번호 입력");
                break;
        }
    }

    public void setEmptyResult(boolean isEmpty, boolean needAddUser){
        if(isEmpty){
            aq.id(R.id.add_user_area).visible();
            if(needAddUser){
                aq.id(R.id.btn_add_user).visible();
            }else{
                if("INPUT".equals(searchType)){
                    aq.id(R.id.btn_add_user).visible();
                }else{
                    aq.id(R.id.btn_add_user).gone();
                }

            }
        }else{
            aq.id(R.id.add_user_area).gone();
        }

    }

    public void numPadClicked(View button){
        int tag = (int)button.getTag();
        String inputPhoneStr = aq.id(R.id.input_search_phone).getText().toString();

        switch (currentType){
            case 0:
                if(inputPhoneStr.length() < 4){
                    aq.id(R.id.input_search_phone).text(inputPhoneStr+tag);
                }
                break;
            case 2:
                if(inputPhoneStr.length() < 7){
                    aq.id(R.id.input_search_phone).text(inputPhoneStr+tag);
                }
                break;
        }


    }

    public void numDelClicked(View button){
        aq.id(R.id.input_search_phone).text("");
    }

    public void itemSelected(int position, final JSONObject jsondata) {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent();
                if("INPUT".equals(searchType)){
                    intent.setClass(mCtx, LocalOrderInputActivity.class);
                }else if("OUTPUT".equals(searchType)){
                    intent.setClass(mCtx, LocalOrderOutputActivity.class);
                    if(currentType == 2){
                        intent.putExtra("orderId", aq.id(R.id.input_search_phone).getText().toString());
                    }

//                    try {
//                        if(jsondata.getInt("orderCount") == 0){
//                            DUtil.alertShow(UserSearchActivity.this, "주문내역이 없는 회원입니다.");
//                            return;
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("userData",jsondata.toString());
                //intent.putExtra("position", position);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();
            }
        }, 0);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
    }

    public void closeClicked(View button){
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ADD_USER_RESULT:
                    String newUserId = data.getStringExtra("userId");
                    Log.i("selectedUsers", newUserId);
                    aq.id(R.id.input_search_phone).text("");
                    searchUser(newUserId, "USERID");

                    break;
            }
        }else{

        }
    }
}
