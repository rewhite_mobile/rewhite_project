package me.rewhite.delivery.tablet;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.OrderListItem;
import me.rewhite.delivery.adapter.TabletRewhiteOrderListViewAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class TabletRewhiteActivity extends TabletBaseActivity {

    ListView orderListView;
    AQuery aq;

    private Context mCtx = this;
    private TabletRewhiteOrderListViewAdapter orderAdapter;
    private ArrayList<OrderListItem> orderData;
    int currentPage = 1;
    int blocksize = 50;
    String currentSearchType = "Z";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablet_rewhite);

        aq = new AQuery(this);

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.screen_title).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_a).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_b).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_c).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_d).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_z).getTextView());

        orderListView = (ListView)findViewById(R.id.listView);

        aq.id(R.id.btn_z).clicked(this, "showData").tag(0).background(R.mipmap.tablet_subnav_selected);
        aq.id(R.id.btn_a).clicked(this, "showData").tag(1).background(R.mipmap.tablet_subnav_normal);
        aq.id(R.id.btn_b).clicked(this, "showData").tag(4).background(R.mipmap.tablet_subnav_normal);
        aq.id(R.id.btn_c).clicked(this, "showData").tag(2).background(R.mipmap.tablet_subnav_normal);
        aq.id(R.id.btn_d).clicked(this, "showData").tag(3).background(R.mipmap.tablet_subnav_normal);

        aq.id(R.id.btn_back).clicked(this, "closeClicked");

        aq.id(R.id.rep_icon).image(R.mipmap.icon_top_order_all);

        orderData = new ArrayList<>();
        initialize();
    }

    public void closeClicked(View button){
        finish();
    }

    public void showData(View button){
        int tag = (int)((Button)button).getTag();
        switch (tag){
            case 0:
                currentSearchType = "Z";
                aq.id(R.id.btn_z).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_a).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_c).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_d).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_b).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("전체주문내역");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_order_all);

                break;
            case 1:
                currentSearchType = "A";
                aq.id(R.id.btn_a).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_z).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_c).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_d).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_b).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("수거접수");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_order_req);
                break;
            case 2:
                currentSearchType = "C";
                aq.id(R.id.btn_c).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_a).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_b).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_d).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_z).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("수거완료");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_order_picked);
                break;
            case 3:
                currentSearchType = "D";
                aq.id(R.id.btn_d).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_a).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_c).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_b).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_z).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("세탁진행");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_order_washing);
                break;
            case 4:
                currentSearchType = "B";
                aq.id(R.id.btn_b).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_a).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_c).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_d).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_z).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("배송예정");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_order_delivery);
                break;
        }
        orderData = new ArrayList<>();
        currentPage = 1;
        //aq.id(R.id.empty).visible();
        initialize();
    }

    ProgressDialog mProgressDialog;
    View footerView;

    public void showDialog(){
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(mCtx);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog(){
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    private int currentLoadPage = 0;
    private void initialize(){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("mode", currentSearchType);
        params.put("page", currentPage);
        params.put("block", blocksize);
        params.put("k", 1);

        NetworkClient.post(Constants.ORDER_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_LIST, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_LIST, result);

                    JSONObject jsondata = new JSONObject(result);
                    //orderListView = (ListView) getActivity().findViewById(R.id.listView);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data") || jsondata.getString("data") == null) {
                            //
                            if (orderData == null) {
                                orderData = new ArrayList<>();
                            } else {
                                if (orderData.size() == 0 && currentPage == 1) {
                                    orderAdapter = new TabletRewhiteOrderListViewAdapter(mCtx, R.layout.tablet_rewhite_order_history_item, orderData);
                                    orderAdapter.notifyDataSetChanged();
                                    //orderListView.setAdapter(orderAdapter);
                                    aq.id(orderListView).gone();
                                    aq.id(R.id.empty).visible();
                                } else {
                                    Toast.makeText(mCtx, "더이상 내용이 없습니다", Toast.LENGTH_SHORT).show();
//                                    if (footerView != null) {
//                                        aq.id(footerView).gone();
//                                    }
                                }
                            }

                        } else {

                            orderData = new ArrayList<>();

                            JSONArray orderInfo = jsondata.getJSONArray("data");

                            for (int i = 0; i < orderInfo.length(); i++) {

                                String orderId = orderInfo.getJSONObject(i).getString("orderId");
                                String orderStatus = orderInfo.getJSONObject(i).getString("orderStatus");
                                String addressSeq = orderInfo.getJSONObject(i).getString("addressSeq");
                                String storeId = orderInfo.getJSONObject(i).getString("storeId");
                                String deliveryPrice = orderInfo.getJSONObject(i).getString("deliveryPrice");
                                String pickupRequestTimeApp = orderInfo.getJSONObject(i).getString("pickupRequestTimeApp");
                                String deliveryRequestTimeApp = orderInfo.getJSONObject(i).getString("deliveryRequestTimeApp");
                                String registerDateApp = orderInfo.getJSONObject(i).getString("registerDateApp");
                                String orderRequest = orderInfo.getJSONObject(i).getString("orderRequest");
                                String orderPickupItemMessage = orderInfo.getJSONObject(i).getString("orderPickupItemMessage");
                                if(orderPickupItemMessage.length() > 20){
                                    orderPickupItemMessage = orderPickupItemMessage.substring(0,18) + " ...";
                                }
                                String statusTimeApp = orderInfo.getJSONObject(i).getString("statusTimeApp");
                                String userName = orderInfo.getJSONObject(i).getString("userName");
                                int quantity = orderInfo.getJSONObject(i).getInt("pickupQuantity");
                                String isPayment = orderInfo.getJSONObject(i).getString("isPayment");
                                String payments = orderInfo.getJSONObject(i).getString("payments");
                                String isReceivable = orderInfo.getJSONObject(i).getString("isReceivable");
                                String isAdditionalPayment = orderInfo.getJSONObject(i).getString("isAdditionalPayment");

                                OrderListItem aItem = new OrderListItem(orderId, orderStatus, addressSeq, storeId, userName, deliveryPrice, pickupRequestTimeApp,
                                        deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage, orderInfo.getJSONObject(i).toString(), quantity, isPayment, payments, isReceivable, isAdditionalPayment);
                                orderData.add(aItem);
                            }

                            orderListView = (ListView)findViewById(R.id.listView);
                            aq.id(orderListView).visible();

                            orderAdapter = new TabletRewhiteOrderListViewAdapter(mCtx, R.layout.tablet_rewhite_order_history_item, orderData);
                            orderAdapter.notifyDataSetChanged();
                            orderListView.setAdapter(orderAdapter);
                            if (currentPage != 1) {
                                int currentPosition = orderListView.getFirstVisiblePosition();
                                orderListView.setSelectionFromTop(currentPosition + 1, 0);
                            }

                            //orderListView.setSelectionFromTop(currentPage*30 + 1, 0);

                            if (orderData.size() > 0) {
                                aq.id(R.id.empty).gone();
                                DUtil.Log("ORDER DATA SIZE", currentPage * blocksize + " ::: " + orderData.size());

                                if (currentPage * blocksize == orderData.size()) {
                                    //aq.id(footerView).visible();
                                }else{
//                                    if (footerView != null) {
//                                        aq.id(footerView).gone();
//
//                                        //orderListView.removeFooterView(footerView);
//                                    }
                                }

                            } else {
                                aq.id(R.id.empty).visible();
                            }
                        }

                    } else {
                        DUtil.Log(Constants.ORDER_LIST, "initialize");
                    }
                    dismissDialog();
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    dismissDialog();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    dismissDialog();
                }
            }
        });
    }

    private static final int ORDER_LIST_REFRESH = 13;

    public void itemSelected(int position, JSONObject json) {
        Log.i("itemSel", json.toString());

        Intent intent = new Intent();

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setClass(this, TableOrderDetailActivity.class);
        intent.putExtra("data", json.toString());
        startActivityForResult(intent, ORDER_LIST_REFRESH);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        //overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case ORDER_LIST_REFRESH:
                if (resultCode == RESULT_OK) {

                }
                initialize();
                break;
        }


    }
}
