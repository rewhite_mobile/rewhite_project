package me.rewhite.delivery.tablet;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.balysv.materialripple.MaterialRippleLayout;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import fdk.FDK_Module;
import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.TableItemElementListViewAdapter;
import me.rewhite.delivery.adapter.TabletOrderListItem;
import me.rewhite.delivery.adapter.TabletOrderListViewAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.tablet2.TabletOrderElementModActivity;
import me.rewhite.delivery.util.BarcodeStringUtil;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZCrypto;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.TimeUtil;
import me.rewhite.delivery.util.ToastUtility;
import me.rewhite.delivery.util.UnicodeFormatter;
import me.rewhite.delivery.van.CatManager;
import me.rewhite.delivery.van.VanType;

public class LocalOrderOutputActivity extends TabletBaseActivity {

    AQuery aq;
    Context mCtx;

    JSONObject selectedUserData;
    String selectedOrderId;

    private static final int SEARCH_USER_RESULT = 11;
    private static final int PAYMENT_VAN_CARD = 12;
    private static final int PAYMENT_VAN_CASH = 13;
    private static final int PAYMENT_PRE_VAN_CARD = 14;
    private static final int PAYMENT_PRE_VAN_CASH = 15;

    private static final int MOD_USER_RESULT = 23;
    private static final int MOD_ORDER_ELEMENT = 24;

    private Handler mHandler;
    ProgressDialog mProgressDialog;
    private Messenger mService = null;    // 서비스와 통신하는데 사용되는 메신저
    private boolean mBound = false;    // 서비스 연결 여부
    VanReceiver vanReceiver;

    private class VanReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            // TODO Auto-generated method stub

            String RETCODE = arg1.getStringExtra("RETCODE");
            String MESSAGE = arg1.getStringExtra("MESSAGE");
            boolean RESULT = arg1.getBooleanExtra("RESULT", false);

            Log.w("vanReceiver : ", RETCODE + " , " + MESSAGE);

            if("2102".equals(RETCODE)){
                if(RESULT){
                    // 취소성공
                    JSONObject resultObj = null;
                    try {
                        resultObj = new JSONObject(MESSAGE);
                        paymentLocalPaymentProcess(false, resultObj.getString("encPayAuth"), Integer.parseInt(resultObj.getString("payPrice")), resultObj.getString("payMethod"), true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    DUtil.alertShow(LocalOrderOutputActivity.this, MESSAGE);
                }
            }else if("9998".equals(RETCODE)){

            }else if("8888".equals(RETCODE)){
                if(RESULT){
                    showDialog(MESSAGE);
                }else{
                    dismissDialog();
                }
            }else{
                dismissDialog();
            }


        }
    }

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(LocalOrderOutputActivity.this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void showDialog(final String _message) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(LocalOrderOutputActivity.this);
        mProgressDialog.setMessage(_message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    @Override
    protected void onStart() {

        currentVanType = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.VAN_TYPE);
        if(currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)){

        }else if(currentVanType.equals(VanType.FIRSTDATA)){

        }else if(currentVanType.equals(VanType.SMARTRO)){
            //Register BroadcastReceiver
            //to receive event from our service
            vanReceiver = new VanReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(CatManager.INTENT_ACTION);
            registerReceiver(vanReceiver, intentFilter);
        }

        super.onStart();
    }

    @Override
    public void onStop(){
        super.onStop();
        if(currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)){

        }else if(currentVanType.equals(VanType.FIRSTDATA)){

        }else if(currentVanType.equals(VanType.SMARTRO)){
            if(mBound){
                unbindService(mConnection);
                mBound = false;
            }
            unregisterReceiver(vanReceiver);
        }

    }

    // ServiceConnection 인터페이스를 구현하는 객체를 생성한다.
    private ServiceConnection mConnection = new ServiceConnection(){

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = new Messenger(service);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0){
            mService = null;
            mBound = false;
        }
    };

    String currentVanType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.e("LocalOrderOutput", "click search");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablet_activity_local_order_output);

        currentVanType = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.VAN_TYPE);
        if(currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)){

        }else if(currentVanType.equals(VanType.FIRSTDATA)){
            try {
                FDK_Module.Deliver_App_Resources_to_FDK("FTDI", this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(currentVanType.equals(VanType.SMARTRO)){
            String successDeviceIP = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.CAT_DEVICE_IP);
            if(successDeviceIP != null){
                Intent catIntent = new Intent(this, CatManager.class);
                catIntent.putExtra("ServerIP", successDeviceIP);
                bindService(catIntent, mConnection, Context.BIND_AUTO_CREATE);
            }
        }


        mCtx = this;
        mHandler = new Handler();
        aq = new AQuery(this);


//        setMaterialRippleLayout((View)findViewById(R.id.btn_mod));
//        setMaterialRippleLayout((View)findViewById(R.id.btn_cancel));
//        setMaterialRippleLayout((View)findViewById(R.id.btn_back));
//        setMaterialRippleLayout((View)findViewById(R.id.btn_print));
//        setMaterialRippleLayout((View)findViewById(R.id.btn_user_mod));
//        setMaterialRippleLayout((View)findViewById(R.id.btn_output));

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("userData") != null) {
                try {
                    selectedUserData = new JSONObject(intent.getStringExtra("userData"));
                    selectedOrderId = intent.getStringExtra("orderId");

                    ImageOptions op = new ImageOptions();
                    op.round = 60;
                    op.memCache = true;
                    op.ratio = 1.f;

                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.profile_name).getTextView());
                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.phone_text).getTextView());
                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.address_text).getTextView());
                    CommonUtility.setTypefaceLightSetup(aq.id(R.id.text_unpay_title).getTextView());

                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_unpay_price).getTextView());
                    CommonUtility.setTypefaceLightSetup(aq.id(R.id.text_top_desc).getTextView());
                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_order_id).getTextView());
                    CommonUtility.setTypefaceLightSetup(aq.id(R.id.text_order_date).getTextView());

                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.total_price_title).getTextView());
                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.totalprice_text).getTextView());
                    CommonUtility.setTypefaceBoldSetup(aq.id(R.id.pick_count_text).getTextView());

                    CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_print).getTextView());
                    CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_unpay).getTextView());
                    CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_cash_pay).getTextView());
                    CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_card_pay).getTextView());

                    aq.id(R.id.profile_image).image(selectedUserData.getString("imageThumbPath"), op);
                    aq.id(R.id.profile_name).text(selectedUserData.getString("userName"));
                    aq.id(R.id.address_text).text(selectedUserData.getString("address1") + " " +selectedUserData.getString("address2"));
                    aq.id(R.id.phone_text).text(MZValidator.validTelNumber(selectedUserData.getString("phone")));
                    aq.id(R.id.ordercount_text).text(selectedUserData.getString("orderCount"));
                    aq.id(R.id.text_unpay_price).text(MZValidator.toNumFormat(selectedUserData.getInt("receivablePrice"))+" 원");

                    aq.id(R.id.btn_user_mod).clicked(this, "modUserAction");

                    findOrderByUserId(selectedUserData.getString("userId"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        aq.id(R.id.btn_back).clicked(this, "backStep");
        aq.id(R.id.btn_print).clicked(this, "printAction");

        aq.id(R.id.btn_unpay).clicked(this, "unpayOutAction");
        aq.id(R.id.btn_cash_pay).clicked(this, "payCashOutAction").tag(0); // 후불출고처리
        aq.id(R.id.btn_card_pay).clicked(this, "payCardOutAction").tag(0); // 후불출고처리
        aq.id(R.id.btn_card_pay_cancel).clicked(this, "payCardOutCancelAction");
        aq.id(R.id.btn_cash_pay_cancel).clicked(this, "payCashReceiptCancelAction");

        // 선불
        aq.id(R.id.btn_prepay_card).clicked(this, "payCardOutAction").tag(1); // 선불 미출고처리
        aq.id(R.id.btn_prepay_cash).clicked(this, "payCashOutAction").tag(1); // 선불 미출고처리
        aq.id(R.id.btn_popup_close).clicked(this, "prePayPopupClose");
        aq.id(R.id.btn_pre_pay).clicked(this, "prePayPopupOpen");
        aq.id(R.id.btn_output).clicked(this, "prePaidOutAction");

        aq.id(R.id.btn_mod).clicked(this, "orderModAction");
        aq.id(R.id.btn_cancel).clicked(this, "orderCancelAction");



    }

    private void setMaterialRippleLayout(View _target){
        MaterialRippleLayout.on(_target)
                .rippleColor(Color.parseColor("#FFffff"))
                .rippleAlpha(0.2f)
                .rippleHover(true)
                .rippleOverlay(true)
                .create();
    }

    private void orderCancelAction(final int orderId){
        // 주문취소 API 호출
        // v2/Shop/OrderDirectCancelForFranchisee
        // CANCEL_LOCAL_ORDER
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("orderStatus", "91"); // 91 : 취소, 93 : 환불, 94 : 보상요청
        params.put("k", 1);

        DUtil.Log("RequestParams", params.toString());

        NetworkClient.post(Constants.CANCEL_LOCAL_ORDER, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.CANCEL_LOCAL_ORDER, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.CANCEL_LOCAL_ORDER, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if(orderId == jsondata.getInt("data")){
                            findOrderByUserId(selectedUserData.getString("userId"));
                        }

                    } else {
                        ToastUtility.show(mCtx, jsondata.getString("message"), Toast.LENGTH_LONG);
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void orderCancelAction(View button){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
        alertDialogBuilder.setCancelable(true).setMessage("해당 주문건을 취소하시겠습니까?")
                .setPositiveButton("취소합니다", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        orderCancelAction(currentOrderId);
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
    public void orderModAction(View button){
        // 수정창 띄움
        Intent intent = new Intent();
        intent.setClass(mCtx, TabletOrderElementModActivity.class);
        intent.putExtra("orderData", currentJsonData.toString());
        intent.putExtra("orderId", currentOrderId);
        startActivityForResult(intent, MOD_ORDER_ELEMENT);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void modUserAction(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setClass(mCtx, UserModActivity.class);
                intent.putExtra("userData", selectedUserData.toString());
                startActivityForResult(intent, MOD_USER_RESULT);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }, 0);
    }

    public void prePaidOutAction(View button){
        paymentLocalProcess(true);
    }

    // 선불결제 팝업
    public void prePayPopupOpen(View button){
        aq.id(R.id.order_completed_layout).visible();
    }
    public void prePayPopupClose(View button){
        aq.id(R.id.order_completed_layout).gone();
    }

    public void unpayOutAction(View button){
        // 미수금 처리
        paymentLocalProcess(false);
    }


    public void payCashOutAction(View button){
        //paymentLocalProcess(true);
        final int tag = (int)button.getTag();
        // 현금영수증
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setClass(mCtx, TabletVanCashPaymentActivity.class);
                intent.putExtra("items", pickupItemsV2.toString());
                intent.putExtra("order", currentJsonData.toString());
                if(tag == 0){
                    startActivityForResult(intent, PAYMENT_VAN_CASH);
                }else if(tag == 1){
                    startActivityForResult(intent, PAYMENT_PRE_VAN_CASH);
                }
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }, 0);
    }
    public void payCardOutAction(View button){
        if("TRUE".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.IS_CONNECTED))){
            final int tag = (int)button.getTag();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent intent = new Intent();
                    intent.setClass(mCtx, TabletVanCardPaymentActivity.class);
                    intent.putExtra("items", pickupItemsV2.toString());
                    intent.putExtra("order", currentJsonData.toString());
                    if(tag == 0){
                        startActivityForResult(intent, PAYMENT_VAN_CARD);
                    }else if(tag == 1){
                        startActivityForResult(intent, PAYMENT_PRE_VAN_CARD);
                    }
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }
            }, 0);
        }else{
            DUtil.alertShow(LocalOrderOutputActivity.this, "결제시도가 실패했습니다. CAT단말기가 연결되지 않았습니다.");
        }
        //DUtil.alertShow(LocalOrderOutputActivity.this, "결제시도가 실패했습니다. CAT단말기가 연결되지 않았습니다.");
    }

    public void payCashReceiptCancelAction(View button){
        if(currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)){

        }else if(currentVanType.equals(VanType.FIRSTDATA)){
            try {
                payCashReceiptCancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(currentVanType.equals(VanType.SMARTRO)){
            payCashReceiptCancelSmartro();
        }

    }

    public void payCashReceiptCancelSmartro(){
        if(mBound){
            Message msg = Message.obtain(null, CatManager.CARDPAY_CANCEL_REQUEST, 0, 0);

            String contentString = "1c";
            String printString = "";

            try {
                contentString += UnicodeFormatter.stringToHex("02"); // 현금승인
                contentString += "1c";
                contentString += UnicodeFormatter.stringToHex(currentJsonData.getInt("deliveryPrice")+""); // 금액
                contentString += "1c";
                // 세금
                contentString += "1c";
                // 봉사료
                contentString += "1c";

                String encJson = currentJsonData.getJSONArray("payments").getJSONObject(0).getString("returnValue");
                String decJson = MZCrypto.getBase64decode(encJson);
                JSONObject payInfo = new JSONObject(decJson);
                String authCode = payInfo.getString("authCode").substring(2, payInfo.getString("authCode").length());
                String authType = payInfo.getString("authCode").substring(0,2);

                Log.e("card cancel", payInfo.getString("authCode") + " / " + payInfo.toString() );

                // 할부개월/사용구분
                contentString += UnicodeFormatter.stringToHex(authType) + "1c";
                // 승인번호
                contentString += UnicodeFormatter.stringToHex(authCode) + "1c";
                // 원거래일자
                contentString += UnicodeFormatter.stringToHex(payInfo.getString("authDate").substring(0,8)) + "1c";
                // 비밀번호  : 단말기설정
                contentString += "1c";
                // 품목명
                contentString += "1c";
                // 서명여부
                contentString += "1c";
                // 취소사유 : 현금
                contentString += UnicodeFormatter.stringToHex("1") + "1c";
                // FILLER1
                contentString += "1c";
                // FILLER2
                contentString += "1c";


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            byte[] content = new byte[contentString.length()/2];
            content = UnicodeFormatter.hexToByteArray(contentString);

            try{
                Bundle b = new Bundle();
                b.putByteArray("payCancel", content); // for example
                msg.setData(b);
                mService.send(msg);
            }catch(RemoteException e){
                e.printStackTrace();
            }
        }else{
            DUtil.alertShow(this, "PRINT가 연결되지 않았습니다. 연결상태를 확인해주세요.");
        }
    }

    public void payCashReceiptCancel() throws Exception{
        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        String transactionAmount = currentJsonData.getString("deliveryPrice");
        String bizNo =  SharedPreferencesUtility.get(SharedPreferencesUtility.Van.BIZ_NO);

        String encJson = currentJsonData.getJSONArray("payments").getJSONObject(0).getString("returnValue");
        String decJson = MZCrypto.getBase64decode(encJson);
        JSONObject payInfo = new JSONObject(decJson);

        Log.e("cash cancel", transactionAmount + " / " + payInfo.toString() );

        iProcID = FDK_Module.Creat();

        FDK_Module.Input(iProcID, "Business Number", bizNo);
        FDK_Module.Input(iProcID, "Transaction Amount", transactionAmount);
        FDK_Module.Input(iProcID, "Transaction Type", payInfo.getString("authType"));
        FDK_Module.Input(iProcID, "Original Authorization Number", payInfo.getString("authCode"));
        FDK_Module.Input(iProcID, "Original Authorization Date", payInfo.getString("authDate").substring(0,8));
        FDK_Module.Input(iProcID, "Cancel Reason", "1");
        FDK_Module.Input(iProcID, "Receipt Print Flag", "2");

        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/SecuritySafe_Tell/CashReceiptCancel_K7");

        if(0 == iRet)
        {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;

            showMessage("현금영수증 발급취소가 완료되었습니다.");
        }
        else if(-1000 == iRet)
        {
            strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;

            showMessage("현금영수증 발급취소가 실패했습니다.\n다시 시도해주세요.");
        }
        else
        {
            strMsg = "에러[" + iRet + "]\r\n";
            strData = "";
            strData = FDK_Module.Output(iProcID, "ErrorInfo");
            strMsg = strMsg + strData;

            showMessage("현금영수증 발급취소가 실패했습니다.\n다시 시도해주세요." + strMsg);
        }
        //ShowMsg(strMsg);

        //Output Sample
        if(0 == iRet || -1000 == iRet)
        {
            strMsg = "Output List :\r\n";
            strMsg += "STX : " + FDK_Module.Output(iProcID, "STX") + "\r\n";
            strMsg += "Work Type : " + FDK_Module.Output(iProcID, "Work Type") + "\r\n";
            strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
            strMsg += "Business Number : " + FDK_Module.Output(iProcID, "Business Number") + "\r\n";
            strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
            strMsg += "Response Code : " + FDK_Module.Output(iProcID, "Response Code") + "\r\n";
            strMsg += "FS3 : " + FDK_Module.Output(iProcID, "FS3") + "\r\n";
            strMsg += "Card Data : " + FDK_Module.Output(iProcID, "Card Data") + "\r\n";
            strMsg += "FS4 : " + FDK_Module.Output(iProcID, "FS4") + "\r\n";
            strMsg += "Installment Period : " + FDK_Module.Output(iProcID, "Installment Period") + "\r\n";
            strMsg += "FS5 : " + FDK_Module.Output(iProcID, "FS5") + "\r\n";
            strMsg += "Authorization Date : " + FDK_Module.Output(iProcID, "Authorization Date") + "\r\n";
            strMsg += "FS6 : " + FDK_Module.Output(iProcID, "FS6") + "\r\n";
            strMsg += "Original Authorization Date : " + FDK_Module.Output(iProcID, "Original Authorization Date") + "\r\n";
            strMsg += "FS7 : " + FDK_Module.Output(iProcID, "FS7") + "\r\n";
            strMsg += "Transaction Amount : " + FDK_Module.Output(iProcID, "Transaction Amount") + "\r\n";
            strMsg += "FS8 : " + FDK_Module.Output(iProcID, "FS8") + "\r\n";
            strMsg += "Authorization Number : " + FDK_Module.Output(iProcID, "Authorization Number") + "\r\n";
            strMsg += "FS9 : " + FDK_Module.Output(iProcID, "FS9") + "\r\n";
            strMsg += "Issuer Code : " + FDK_Module.Output(iProcID, "Issuer Code") + "\r\n";
            strMsg += "FS10 : " + FDK_Module.Output(iProcID, "FS10") + "\r\n";
            strMsg += "Merchant Number : " + FDK_Module.Output(iProcID, "Merchant Number") + "\r\n";
            strMsg += "FS11 : " + FDK_Module.Output(iProcID, "FS11") + "\r\n";
            strMsg += "DDC Flag : " + FDK_Module.Output(iProcID, "DDC Flag") + "\r\n";
            strMsg += "FS12 : " + FDK_Module.Output(iProcID, "FS12") + "\r\n";
            strMsg += "Notice : " + FDK_Module.Output(iProcID, "Notice") + "\r\n";
            strMsg += "FS13 : " + FDK_Module.Output(iProcID, "FS13") + "\r\n";
            strMsg += "Acquirer Code : " + FDK_Module.Output(iProcID, "Acquirer Code") + "\r\n";
            strMsg += "FS14 : " + FDK_Module.Output(iProcID, "FS14") + "\r\n";
            strMsg += "Display : " + FDK_Module.Output(iProcID, "Display") + "\r\n";
            strMsg += "FS15 : " + FDK_Module.Output(iProcID, "FS15") + "\r\n";
            strMsg += "Point Customer Name : " + FDK_Module.Output(iProcID, "Point Customer Name") + "\r\n";
            strMsg += "FS16 : " + FDK_Module.Output(iProcID, "FS16") + "\r\n";
            strMsg += "Point Title Type : " + FDK_Module.Output(iProcID, "Point Title Type") + "\r\n";
            strMsg += "FS17 : " + FDK_Module.Output(iProcID, "FS17") + "\r\n";
            strMsg += "Point Add : " + FDK_Module.Output(iProcID, "Point Add") + "\r\n";
            strMsg += "FS18 : " + FDK_Module.Output(iProcID, "FS18") + "\r\n";
            strMsg += "Point Usable : " + FDK_Module.Output(iProcID, "Point Usable") + "\r\n";
            strMsg += "FS19 : " + FDK_Module.Output(iProcID, "FS19") + "\r\n";
            strMsg += "Point Save : " + FDK_Module.Output(iProcID, "Point Save") + "\r\n";
            strMsg += "FS20 : " + FDK_Module.Output(iProcID, "FS20") + "\r\n";
            strMsg += "Point Merchant : " + FDK_Module.Output(iProcID, "Point Merchant") + "\r\n";
            strMsg += "FS21 : " + FDK_Module.Output(iProcID, "FS21") + "\r\n";
            strMsg += "Notification : " + FDK_Module.Output(iProcID, "Notification") + "\r\n";
            strMsg += "FS22 : " + FDK_Module.Output(iProcID, "FS22") + "\r\n";
            strMsg += "Cash Receipt Authorization Number : " + FDK_Module.Output(iProcID, "Cash Receipt Authorization Number") + "\r\n";
            strMsg += "FS23 : " + FDK_Module.Output(iProcID, "FS23") + "\r\n";
            strMsg += "Cash Receipt Notice : " + FDK_Module.Output(iProcID, "Cash Receipt Notice") + "\r\n";
            strMsg += "FS24 : " + FDK_Module.Output(iProcID, "FS24") + "\r\n";
            strMsg += "CAT ID : " + FDK_Module.Output(iProcID, "CAT ID") + "\r\n";
            strMsg += "FS25 : " + FDK_Module.Output(iProcID, "FS25") + "\r\n";
            strMsg += "Issuer Name : " + FDK_Module.Output(iProcID, "Issuer Name") + "\r\n";
            strMsg += "FS26 : " + FDK_Module.Output(iProcID, "FS26") + "\r\n";
            strMsg += "Acquirer Name : " + FDK_Module.Output(iProcID, "Acquirer Name") + "\r\n";
            strMsg += "FS27 : " + FDK_Module.Output(iProcID, "FS27") + "\r\n";
            strMsg += "Sign Compress Method : " + FDK_Module.Output(iProcID, "Sign Compress Method") + "\r\n";
            strMsg += "FS28 : " + FDK_Module.Output(iProcID, "FS28") + "\r\n";
            strMsg += "Sign MAC Value : " + FDK_Module.Output(iProcID, "Sign MAC Value") + "\r\n";
            strMsg += "FS29 : " + FDK_Module.Output(iProcID, "FS29") + "\r\n";
            strMsg += "Sign Data : " + FDK_Module.Output(iProcID, "Sign Data") + "\r\n";
            strMsg += "FS30 : " + FDK_Module.Output(iProcID, "FS30") + "\r\n";
            strMsg += "Sign Image Create Key : " + FDK_Module.Output(iProcID, "Sign Image Create Key") + "\r\n";
            strMsg += "FS31 : " + FDK_Module.Output(iProcID, "FS31") + "\r\n";
            strMsg += "ETX : " + FDK_Module.Output(iProcID, "ETX") + "\r\n";
            strMsg += "LRC : " + FDK_Module.Output(iProcID, "LRC") + "\r\n";
            //ShowMsg(strMsg);

            String encPayAuthJSONString = null;
            try {
                JSONObject payAuthData = new JSONObject();
                payAuthData.put("authCode", FDK_Module.Output(iProcID, "Authorization Number"));
                payAuthData.put("authDate", FDK_Module.Output(iProcID, "Authorization Date"));
                payAuthData.put("authType", FDK_Module.Output(iProcID, "Authorization Date"));
                String payAuthJSONString = payAuthData.toString();
                encPayAuthJSONString = MZCrypto.getBase64encode(payAuthJSONString);

                Log.e("== paid enc", encPayAuthJSONString);
                Log.e("== paid enc-dec", MZCrypto.getBase64decode(encPayAuthJSONString));
            } catch (Exception e) {
                e.printStackTrace();
            }

            paymentLocalPaymentProcess(false, encPayAuthJSONString, Integer.parseInt(FDK_Module.Output(iProcID, "Transaction Amount")), "P92", true);
        }

        iRet = FDK_Module.Destroy(iProcID);
    }

    public void payCardOutCancelAction(View button){

        if(currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)){

        }else if(currentVanType.equals(VanType.FIRSTDATA)){
            try {
                payCardCancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(currentVanType.equals(VanType.SMARTRO)){
            payCardCancelSmartro();
        }

    }

    public void payCardCancelSmartro(){
        if(mBound){
            Message msg = Message.obtain(null, CatManager.CARDPAY_CANCEL_REQUEST, 0, 0);

            String contentString = "1c";
            String printString = "";

            try {
                contentString += UnicodeFormatter.stringToHex("01"); // 신용승인
                contentString += "1c";
                contentString += UnicodeFormatter.stringToHex(currentJsonData.getInt("deliveryPrice")+""); // 금액
                contentString += "1c";
                // 세금
                contentString += "1c";
                // 봉사료
                contentString += "1c";
                // 할부개월
                contentString += "1c";

                String encJson = currentJsonData.getJSONArray("payments").getJSONObject(0).getString("returnValue");
                String decJson = MZCrypto.getBase64decode(encJson);
                JSONObject payInfo = new JSONObject(decJson);

                Log.e("card cancel", payInfo.getString("authCode") + " / " + payInfo.toString() );

                // 승인번호
                contentString += UnicodeFormatter.stringToHex(payInfo.getString("authCode")) + "1c";
                // 원거래일자
                contentString += UnicodeFormatter.stringToHex(payInfo.getString("authDate").substring(0,8)) + "1c";
                // 비밀번호  : 단말기설정
                contentString += "1c";
                // 품목명
                contentString += "1c";
                // 서명여부
                contentString += "1c";
                // 취소사유 : 현금
                contentString += "1c";
                // FILLER1
                contentString += "1c";
                // FILLER2
                contentString += "1c";


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            byte[] content = new byte[contentString.length()/2];
            content = UnicodeFormatter.hexToByteArray(contentString);

            try{
                Bundle b = new Bundle();
                b.putByteArray("payCancel", content); // for example
                msg.setData(b);
                mService.send(msg);
            }catch(RemoteException e){
                e.printStackTrace();
            }
        }else{
            DUtil.alertShow(this, "PRINT가 연결되지 않았습니다. 연결상태를 확인해주세요.");
        }
    }

    public void payCardCancel() throws Exception{
        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        String transactionAmount = currentJsonData.getString("deliveryPrice");
        String bizNo =  SharedPreferencesUtility.get(SharedPreferencesUtility.Van.BIZ_NO);

        String encJson = currentJsonData.getJSONArray("payments").getJSONObject(0).getString("returnValue");
        String decJson = MZCrypto.getBase64decode(encJson);
        JSONObject payInfo = new JSONObject(decJson);

        Log.e("card cancel", transactionAmount + " / " + payInfo.toString() );

        iProcID = FDK_Module.Creat();

        FDK_Module.Input(iProcID, "Business Number", bizNo);
        FDK_Module.Input(iProcID, "Transaction Amount", transactionAmount);
        FDK_Module.Input(iProcID, "Original Authorization Number", payInfo.getString("authCode"));
        FDK_Module.Input(iProcID, "Original Authorization Date", payInfo.getString("authDate").substring(0,8));
        FDK_Module.Input(iProcID, "Receipt Print Flag", "2");

        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/SecuritySafe_Tell/CreditCancel_D7");

        if(0 == iRet)
        {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;

            showMessage("카드결제 취소가 완료되었습니다.");
        }
        else if(-1000 == iRet)
        {
            strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;

            showMessage("카드결제 취소가 실패했습니다.\n다시 시도해주세요.");
        }
        else
        {
            strMsg = "에러[" + iRet + "]\r\n";
            strData = "";
            strData = FDK_Module.Output(iProcID, "ErrorInfo");
            strMsg = strMsg + strData;

            showMessage("오류가 발생했습니다.\n다시 시도해주세요.\n"+strMsg);
        }
        //ShowMsg(strMsg);

        //Output Sample
        if(0 == iRet || -1000 == iRet)
        {
            strMsg = "Output List :\r\n";
            strMsg += "STX : " + FDK_Module.Output(iProcID, "STX") + "\r\n";
            strMsg += "Work Type : " + FDK_Module.Output(iProcID, "Work Type") + "\r\n";
            strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
            strMsg += "Business Number : " + FDK_Module.Output(iProcID, "Business Number") + "\r\n";
            strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
            strMsg += "Response Code : " + FDK_Module.Output(iProcID, "Response Code") + "\r\n";
            strMsg += "FS3 : " + FDK_Module.Output(iProcID, "FS3") + "\r\n";
            strMsg += "Card Data : " + FDK_Module.Output(iProcID, "Card Data") + "\r\n";
            strMsg += "FS4 : " + FDK_Module.Output(iProcID, "FS4") + "\r\n";
            strMsg += "Installment Period : " + FDK_Module.Output(iProcID, "Installment Period") + "\r\n";
            strMsg += "FS5 : " + FDK_Module.Output(iProcID, "FS5") + "\r\n";
            strMsg += "Authorization Date : " + FDK_Module.Output(iProcID, "Authorization Date") + "\r\n";
            strMsg += "FS6 : " + FDK_Module.Output(iProcID, "FS6") + "\r\n";
            strMsg += "Original Authorization Date : " + FDK_Module.Output(iProcID, "Original Authorization Date") + "\r\n";
            strMsg += "FS7 : " + FDK_Module.Output(iProcID, "FS7") + "\r\n";
            strMsg += "Transaction Amount : " + FDK_Module.Output(iProcID, "Transaction Amount") + "\r\n";
            strMsg += "FS8 : " + FDK_Module.Output(iProcID, "FS8") + "\r\n";
            strMsg += "Authorization Number : " + FDK_Module.Output(iProcID, "Authorization Number") + "\r\n";
            strMsg += "FS9 : " + FDK_Module.Output(iProcID, "FS9") + "\r\n";
            strMsg += "Issuer Code : " + FDK_Module.Output(iProcID, "Issuer Code") + "\r\n";
            strMsg += "FS10 : " + FDK_Module.Output(iProcID, "FS10") + "\r\n";
            strMsg += "Merchant Number : " + FDK_Module.Output(iProcID, "Merchant Number") + "\r\n";
            strMsg += "FS11 : " + FDK_Module.Output(iProcID, "FS11") + "\r\n";
            strMsg += "DDC Flag : " + FDK_Module.Output(iProcID, "DDC Flag") + "\r\n";
            strMsg += "FS12 : " + FDK_Module.Output(iProcID, "FS12") + "\r\n";
            strMsg += "Notice : " + FDK_Module.Output(iProcID, "Notice") + "\r\n";
            strMsg += "FS13 : " + FDK_Module.Output(iProcID, "FS13") + "\r\n";
            strMsg += "Acquirer Code : " + FDK_Module.Output(iProcID, "Acquirer Code") + "\r\n";
            strMsg += "FS14 : " + FDK_Module.Output(iProcID, "FS14") + "\r\n";
            strMsg += "Display : " + FDK_Module.Output(iProcID, "Display") + "\r\n";
            strMsg += "FS15 : " + FDK_Module.Output(iProcID, "FS15") + "\r\n";
            strMsg += "Point Customer Name : " + FDK_Module.Output(iProcID, "Point Customer Name") + "\r\n";
            strMsg += "FS16 : " + FDK_Module.Output(iProcID, "FS16") + "\r\n";
            strMsg += "Point Title Type : " + FDK_Module.Output(iProcID, "Point Title Type") + "\r\n";
            strMsg += "FS17 : " + FDK_Module.Output(iProcID, "FS17") + "\r\n";
            strMsg += "Point Add : " + FDK_Module.Output(iProcID, "Point Add") + "\r\n";
            strMsg += "FS18 : " + FDK_Module.Output(iProcID, "FS18") + "\r\n";
            strMsg += "Point Usable : " + FDK_Module.Output(iProcID, "Point Usable") + "\r\n";
            strMsg += "FS19 : " + FDK_Module.Output(iProcID, "FS19") + "\r\n";
            strMsg += "Point Save : " + FDK_Module.Output(iProcID, "Point Save") + "\r\n";
            strMsg += "FS20 : " + FDK_Module.Output(iProcID, "FS20") + "\r\n";
            strMsg += "Point Merchant : " + FDK_Module.Output(iProcID, "Point Merchant") + "\r\n";
            strMsg += "FS21 : " + FDK_Module.Output(iProcID, "FS21") + "\r\n";
            strMsg += "Notification : " + FDK_Module.Output(iProcID, "Notification") + "\r\n";
            strMsg += "FS22 : " + FDK_Module.Output(iProcID, "FS22") + "\r\n";
            strMsg += "Cash Receipt Authorization Number : " + FDK_Module.Output(iProcID, "Cash Receipt Authorization Number") + "\r\n";
            strMsg += "FS23 : " + FDK_Module.Output(iProcID, "FS23") + "\r\n";
            strMsg += "Cash Receipt Notice : " + FDK_Module.Output(iProcID, "Cash Receipt Notice") + "\r\n";
            strMsg += "FS24 : " + FDK_Module.Output(iProcID, "FS24") + "\r\n";
            strMsg += "CAT ID : " + FDK_Module.Output(iProcID, "CAT ID") + "\r\n";
            strMsg += "FS25 : " + FDK_Module.Output(iProcID, "FS25") + "\r\n";
            strMsg += "Issuer Name : " + FDK_Module.Output(iProcID, "Issuer Name") + "\r\n";
            strMsg += "FS26 : " + FDK_Module.Output(iProcID, "FS26") + "\r\n";
            strMsg += "Acquirer Name : " + FDK_Module.Output(iProcID, "Acquirer Name") + "\r\n";
            strMsg += "FS27 : " + FDK_Module.Output(iProcID, "FS27") + "\r\n";
            strMsg += "Sign Compress Method : " + FDK_Module.Output(iProcID, "Sign Compress Method") + "\r\n";
            strMsg += "FS28 : " + FDK_Module.Output(iProcID, "FS28") + "\r\n";
            strMsg += "Sign MAC Value : " + FDK_Module.Output(iProcID, "Sign MAC Value") + "\r\n";
            strMsg += "FS29 : " + FDK_Module.Output(iProcID, "FS29") + "\r\n";
            strMsg += "Sign Data : " + FDK_Module.Output(iProcID, "Sign Data") + "\r\n";
            strMsg += "FS30 : " + FDK_Module.Output(iProcID, "FS30") + "\r\n";
            strMsg += "Sign Image Create Key : " + FDK_Module.Output(iProcID, "Sign Image Create Key") + "\r\n";
            strMsg += "FS31 : " + FDK_Module.Output(iProcID, "FS31") + "\r\n";
            strMsg += "ETX : " + FDK_Module.Output(iProcID, "ETX") + "\r\n";
            strMsg += "LRC : " + FDK_Module.Output(iProcID, "LRC") + "\r\n";
            //ShowMsg(strMsg);

            String encPayAuthJSONString = null;
            try {
                JSONObject payAuthData = new JSONObject();
                payAuthData.put("authCode", FDK_Module.Output(iProcID, "Authorization Number"));
                payAuthData.put("authDate", FDK_Module.Output(iProcID, "Authorization Date"));
                String payAuthJSONString = payAuthData.toString();
                encPayAuthJSONString = MZCrypto.getBase64encode(payAuthJSONString);

                Log.e("== paid enc", encPayAuthJSONString);
                Log.e("== paid enc-dec", MZCrypto.getBase64decode(encPayAuthJSONString));
            } catch (Exception e) {
                e.printStackTrace();
            }

            // 카드이름
            String cardName = FDK_Module.Output(iProcID, "Issuer Name");
            // 거래일시
            String paymentDate = FDK_Module.Output(iProcID, "Authorization Date");
            // 카드번호
            String cardNo = FDK_Module.Output(iProcID, "Card Data");
            String maskingCardNo = cardNo.substring(0,4) + "-" + cardNo.substring(4,5) + "**-****-****";
            // 유효기간(년/월):**/**
            String validPeriod = FDK_Module.Output(iProcID, "Installment Period"); // 값이 없을경우 일시불
            if(validPeriod.length() == 0){
                validPeriod = "일시불";
            }else{
                validPeriod = "할부" + validPeriod + "개월";
            }
            // 가맹점번호
            String merchantNo = FDK_Module.Output(iProcID, "Merchant Number");
            // 승인번호
            String authorizationNo = FDK_Module.Output(iProcID, "Authorization Number");
            // 매입사
            String acquirerName = FDK_Module.Output(iProcID, "Acquirer Name");
            // 무서명거래
            String notice = FDK_Module.Output(iProcID, "Notice");
            // 판매금액
            int transAmount = Integer.parseInt(FDK_Module.Output(iProcID, "Transaction Amount"));
            // 부가가치세

            // 합계

            // CAT ID
            String catID = FDK_Module.Output(iProcID, "CAT ID");
            // 사업자번호
            String bizNumber = FDK_Module.Output(iProcID, "Business Number");
            //

            String printString = "";

            printString += "0E0B12"+UnicodeFormatter.stringToHex("매출전표(고객용)", "euc-kr") + "0A";
            printString += "0A";
            printString += "0A";
            printString += "0F14";
            printString += "14" + UnicodeFormatter.stringJustifyToHex(cardName,"신용IC취소", 44) + "0A";

            printString += "14" + UnicodeFormatter.stringJustifyToHex("거래일시",TimeUtil.convertFormatedDateStringFDK(paymentDate), 44) + "0A"; // yyyyMMddHHmmss

            printString += "14" + UnicodeFormatter.stringJustifyToHex("카드번호",maskingCardNo, 44) + "0A";

            printString += "14" + UnicodeFormatter.stringJustifyToHex("유효기간(년/월):**/**",validPeriod, 44) + "0A";
            printString += "14" + UnicodeFormatter.stringJustifyToHex("가맹점번호",merchantNo, 44) + "0A";
            printString += "14" + UnicodeFormatter.stringJustifyToHex("승인번호",authorizationNo, 44) + "0A";
            printString += "14" + UnicodeFormatter.stringJustifyToHex("매입사:",acquirerName, 44) + "0A";

            printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr")+ "0A";
            printString += UnicodeFormatter.stringJustifyToHex("판매금액", "-" + MZValidator.toNumFormat(transAmount)+" 원", 44) + "0A";
            printString += UnicodeFormatter.stringJustifyToHex("부가가치세", "0 원", 44) + "0A";
            printString += UnicodeFormatter.stringJustifyToHex("합계", "-" + MZValidator.toNumFormat(transAmount) + " 원", 44) + "0A";
            printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr")+"0A";

            printString += "14"+UnicodeFormatter.stringJustifyToHex("가맹점명",SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_NAME), 44) + "0A";
            printString += "14"+UnicodeFormatter.stringJustifyToHex("사업자번호",bizNumber, 44) + "0A";
            printString += "14"+UnicodeFormatter.stringJustifyToHex("대표자명:"+SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_OWNER),"TEL:"+MZValidator.validTelNumber(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_PHONE)), 44) + "0A";

            String storeAddress = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_ADDRESS);
            if(storeAddress.length() > 20){
                storeAddress = storeAddress.substring(0,20);
            }
            printString += "14"+UnicodeFormatter.stringJustifyToHex("주소",storeAddress, 44) + "0A";
            printString += "14"+UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr")+"0A";
            printString += "14"+UnicodeFormatter.stringJustifyToHex("CATID:"+catID,notice, 44) + "0A";
            printString += "0A";
            printString += "14"+UnicodeFormatter.stringToHex("저희 세탁소를 찾아주셔서 감사합니다.", "euc-kr") + "0A";
            printString += "14"+UnicodeFormatter.stringToHex("고객님의 소중한 의류를 성심껏 관리하겠습니다.", "euc-kr") + "0A";
            printString += "11";

            int paymentPrice = Integer.parseInt(FDK_Module.Output(iProcID, "Transaction Amount"));

            cardCancelPrint(printString, false, encPayAuthJSONString, paymentPrice, "P91");
        }

        iRet = FDK_Module.Destroy(iProcID);
    }

    private void cardCancelPrint(String printString, final Boolean isPaymentTry, String _payAuthEncrypted, int paymentPrice, String payMethod) throws Exception{
        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        iProcID = FDK_Module.Creat();

        //FDK_Module.Input(iProcID, "DATA", "123123");
        FDK_Module.Input(iProcID, "DATA", printString);

        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/Print/FreeStyle_F3");
        if(0 == iRet)
        {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        }
        else if(-1000 == iRet)
        {
            strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        }
        else
        {
            strMsg = "에러[" + iRet + "]\r\n";
            strData = "";
            strData = FDK_Module.Output(iProcID, "ErrorInfo");
            strMsg = strMsg + strData;
        }
        //ShowMsg(strMsg);

        //Output Sample
        if(0 == iRet || -1000 == iRet)
        {
            strMsg = "Output List :\r\n";
            strMsg += "Ascii : " + FDK_Module.Output(iProcID, "Ascii") + "\r\n";
            strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
            strMsg += "AsciiLen : " + FDK_Module.Output(iProcID, "AsciiLen") + "\r\n";
            strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
            //ShowMsg(strMsg);
            paymentLocalPaymentProcess(false, _payAuthEncrypted, paymentPrice, payMethod, true);
        }

        iRet = FDK_Module.Destroy(iProcID);

    }

    private void paymentLocalPaymentProcess(final Boolean isPaymentTry, String _payAuthEncrypted, int paymentPrice, String payMethod, final Boolean isOutEnd){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", currentOrderId);
        params.put("paymentType", payMethod); // P91 : 현장결제-카드, P92 : 현장결제-현금
        params.put("paymentPrice", paymentPrice);
        if(isPaymentTry){
            params.put("isPayment", "Y");
            params.put("returnValue", _payAuthEncrypted);
        }else{
            params.put("isPayment", "N");
        }
        params.put("k", 1);

        Log.w("paymentLocalPaymentProcess", params.toString());

        NetworkClient.post(Constants.OUT_PAY_ORDER_DIRECT, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.OUT_PAY_ORDER_DIRECT, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.OUT_PAY_ORDER_DIRECT, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //
                            //aq.id(R.id.empty).gone();
                        } else {
                            if(isPaymentTry){
                                if(isOutEnd){
                                    paymentLocalProcess(false);
                                }else{
                                    findOrderByUserId(selectedUserData.getString("userId"));
                                }

                            }else{
                                findOrderByUserId(selectedUserData.getString("userId"));
                            }

                        }

                    } else {
                        ToastUtility.show(mCtx, jsondata.getString("message"), Toast.LENGTH_LONG);
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private void paymentLocalProcess(final boolean isPayment){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", currentOrderId);
        String isReceivablePay = "";
        if(isPayment){
            isReceivablePay = "N";
        }else{
            isReceivablePay = "Y";
        }
        params.put("isReceivable", isReceivablePay);
        params.put("k", 1);
        NetworkClient.post(Constants.OUT_ORDER_DIRECT, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.OUT_ORDER_DIRECT, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.OUT_ORDER_DIRECT, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //
                            //aq.id(R.id.empty).gone();
                        } else {
                            ToastUtility.show(mCtx, "출고처리 되었습니다.", Toast.LENGTH_LONG);
                            findOrderByUserId(selectedUserData.getString("userId"));

                            if(!isPayment){
                                searchUserId(selectedUserData.getString("userId"));
                            }
                            //getDataByOrderId(currentOrderId + "");
                            /*
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                            alertDialogBuilder.setCancelable(true).setMessage("결제가 성공적으로 완료되었습니다.")
                                    .setPositiveButton("메인화면으로 이동", new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }
                                    }).setNegativeButton("유지", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDataByOrderId(currentOrderId + "");
                                }
                            });
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();*/

                        }

                    } else {
                        ToastUtility.show(mCtx, jsondata.getString("message"), Toast.LENGTH_LONG);
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void ShowMsg(String strMsg)
    {
        android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(this);
        alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();     //닫기
            }
        });
        alert.setMessage(strMsg);
        alert.show();
    }

    private void printStringExcute(String _hex) throws Exception{

        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        iProcID = FDK_Module.Creat();

        //FDK_Module.Input(iProcID, "DATA", "123123");
        FDK_Module.Input(iProcID, "DATA", _hex);

        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/Print/FreeStyle_F3");
        if(0 == iRet)
        {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        }
        else if(-1000 == iRet)
        {
            strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        }
        else
        {
            strMsg = "에러[" + iRet + "]\r\n";
            strData = "";
            strData = FDK_Module.Output(iProcID, "ErrorInfo");
            strMsg = strMsg + strData;
        }
        //ShowMsg(strMsg);

        //Output Sample
        if(0 == iRet || -1000 == iRet)
        {
            strMsg = "Output List :\r\n";
            strMsg += "Ascii : " + FDK_Module.Output(iProcID, "Ascii") + "\r\n";
            strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
            strMsg += "AsciiLen : " + FDK_Module.Output(iProcID, "AsciiLen") + "\r\n";
            strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
            //ShowMsg(strMsg);
        }

        iRet = FDK_Module.Destroy(iProcID);
    }

    private void printFDK(){
        if("TRUE".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.IS_CONNECTED))){

            try{
                String printString = "";

                printString += "0E0B12"+UnicodeFormatter.stringToHex("세탁물 내역서", "euc-kr") + "0A";
                printString += "0A";
                printString += "0A";
                printString += "0A";
                printString += "0F14"+UnicodeFormatter.stringToHex("고객명 : "+selectedUserData.getString("userName")+" (주문번호 : "+selectedOrderId+")", "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("연락처 : "+MZValidator.validTelNumber(selectedUserData.getString("phone")), "euc-kr") + "0A";

                printString += UnicodeFormatter.stringToHex("주문일시 : " + TimeUtil.convertTimestampToString(currentJsonData.getLong("registerDateApp")), "euc-kr") + "0A";
                //printString += UnicodeFormatter.stringToHex("배송예정일 : 2016-07-06 오후 5:19:33", "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("주소 : "+selectedUserData.getString("address1") + " " +selectedUserData.getString("address2"), "euc-kr") + "0A";
                printString += "0A";

                printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr")+ "0A";
                printString += UnicodeFormatter.stringJustifyToHex("TAG No.  품목", "금액", 44) + "0A";
                printString += UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr")+"0A";

                for(int i = 0 ; i < pickupItemsV2.length(); i++){
                    JSONObject item = pickupItemsV2.getJSONObject(i);

                    String isRepair = (item.getJSONArray("repair").length() > 0)?"수선":null;
                    String isAdditional = (item.getJSONArray("additional").length() > 0)?"추가기술":null;
                    String isPart = (item.getJSONArray("part").length() > 0)?"부속품":null;
                    String isService = item.getString("isService");
                    String isReLaundry = item.getString("isReLaundry");

                    int sp = 0;
                    if(item.isNull("sp")){
                        sp = item.getInt("itemSP");
                    }else{
                        sp = item.getInt("sp");
                    }

                    String itemTitle = item.getString("itemTitle");
                    itemTitle = itemTitle.replace("\n","");

                    switch(sp){
                        case 1:
                            itemTitle += "(일반)";
                            break;
                        case 2:
                            itemTitle += "(명품)";
                            break;
                        case 3:
                            itemTitle += "(아동)";
                            break;
                    }

                    if(isService != null){
                        if("Y".equals(isService)){
                            itemTitle += " -무료서비스";
                        }
                    }
                    if(isReLaundry != null){
                        if("Y".equals(isReLaundry)){
                            itemTitle += " -재세탁";
                        }
                    }

                    String itemName = MZValidator.getTagFormmater(pickupItemsV2.getJSONObject(i).getInt("tagId"),4) + "    " + itemTitle;
                    String itemPrice = MZValidator.toNumFormat(item.getInt("confirmPrice"));
                    printString += UnicodeFormatter.stringJustifyToHex(itemName, itemPrice, 44) + "0A";
                    printString += "0A";

                    if(isRepair != null){
                        for(int j = 0 ; j < item.getJSONArray("repair").length(); j++){
                            String repairPrice = MZValidator.toNumFormat(item.getJSONArray("repair").getJSONObject(j).getInt("additionalPrice"));
                            printString += UnicodeFormatter.stringJustifyToHex("         " + "+ 수선 :" + item.getJSONArray("repair").getJSONObject(j).getString("additionalItemTitle"), "("+repairPrice+")", 44) + "0A";
                        }
                    }
                    if(isAdditional != null){
                        for(int j = 0 ; j < item.getJSONArray("additional").length(); j++){
                            String addPrice = MZValidator.toNumFormat(item.getJSONArray("additional").getJSONObject(j).getInt("additionalPrice"));
                            printString += UnicodeFormatter.stringJustifyToHex("         " + "+ 기술료 :" + item.getJSONArray("additional").getJSONObject(j).getString("additionalItemTitle"), "("+addPrice+")", 44) + "0A";
                        }
                    }
                    if(isPart != null){
                        String parts = "";
                        for(int j = 0 ; j < item.getJSONArray("part").length(); j++){
                            if(j == 0){
                                parts += item.getJSONArray("part").getJSONObject(j).getString("partTitle");
                            }else{
                                parts += ", " + item.getJSONArray("part").getJSONObject(j).getString("partTitle");
                            }
                        }
                        printString += UnicodeFormatter.stringToHex("         " + "+ 부속품 :" + parts, "euc-kr") + "0A";
                    }

                    if(item.getJSONArray("repair").length() + item.getJSONArray("additional").length() + item.getJSONArray("part").length() > 0){
                        printString += "0A";
                    }

                }

        /*
            현장접수는 배송비 추가가 없음
         */
//                if(currentJsonData.getInt("deliveryPrice") < 20000){
//                    printString += "0A";
//                    printString += UnicodeFormatter.stringJustifyToHex("         배송비 (20,000원 이하)","2,000", 47) + "0A";
//                }

                printString += "14"+UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr")+"0A";
                //printString += "13"+UnicodeFormatter.stringJustifyToHex("총 수량 :",pickupItemsV2.length()+"개", 44) ;
                printString += "14"+ UnicodeFormatter.stringJustifyToHex("총 수량 :",pickupItemsV2.length()+"개", 44) ;
                printString += "0A";
                Log.e("총 수량 :", pickupItemsV2.length()+"개");
                printString += "14"+UnicodeFormatter.stringJustifyToHex("총 금액 :",MZValidator.toNumFormat(currentJsonData.getInt("deliveryPrice")) + "원", 44) ;
                printString += "0A";
                Log.e("총 금액 :", MZValidator.toNumFormat(currentJsonData.getInt("deliveryPrice")) + "원");
                printString += "14"+UnicodeFormatter.stringToHex("--------------------------------------------", "euc-kr")+"0A";
                printString += "0A";

                printString += "14"+UnicodeFormatter.stringToHex(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_NAME), "euc-kr") + "0A";
                printString += "14"+UnicodeFormatter.stringToHex(MZValidator.validTelNumber(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_PHONE)), "euc-kr") + "0A";
                printString += "0A";
                printString += "14"+UnicodeFormatter.stringToHex("저희 세탁소를 찾아주셔서 감사합니다.", "euc-kr") + "0A";
                printString += "14"+UnicodeFormatter.stringToHex("고객님의 소중한 의류를 성심껏 관리하겠습니다.", "euc-kr") + "0A";
                printString += "0A";
                printString += "0A";

                String barcodeDataString = "101000" + selectedOrderId;
                String barcodeData = UnicodeFormatter.stringToHex(barcodeDataString, "euc-kr");

            /*
                TODO
                Barcode 출력
             */
                //printString += "16204087" +  barcodeData;
                //printString += "194087" +  barcodeData;
                String printStringAgain = printString;
                printString += "11";
                printString += printStringAgain;
                printString += "11";

                printStringExcute(printString);

            }catch (Exception e){
                e.printStackTrace();
            }
        }else{
            DUtil.alertShow(LocalOrderOutputActivity.this, "접수증 인쇄가 실패했습니다. CAT단말기가 연결되지 않았습니다.");
        }
    }
    private void printSmartro(){
        if(mBound){
            Message msg = Message.obtain(null, CatManager.PRINT_REQUEST, 0, 0);

            String contentString = "1c";
            String printString = "";

            try {
                contentString += UnicodeFormatter.stringToHex("A1");
                Log.w("A1", contentString);
                contentString += "1c";
                //////////////////
                printString += "17"+UnicodeFormatter.stringToHex("      세탁물 내역서", "euc-kr") + "18"+"0A";
                printString += "0A";
                printString += "0A";
                printString += "0A";
                printString += UnicodeFormatter.stringToHex("고객명 : "+selectedUserData.getString("userName")+" (주문번호 : "+selectedOrderId+")", "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("연락처 : "+MZValidator.validTelNumber(selectedUserData.getString("phone")), "euc-kr") + "0A";

                printString += UnicodeFormatter.stringToHex("주문일시 : " + TimeUtil.convertTimestampToString(currentJsonData.getLong("registerDateApp")), "euc-kr") + "0A";
                //printString += UnicodeFormatter.stringToHex("배송예정일 : 2016-07-06 오후 5:19:33", "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("주소 : "+selectedUserData.getString("address1") + " " +selectedUserData.getString("address2"), "euc-kr") + "0A";
                printString += "0A";

                printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr")+ "0A";
                printString += UnicodeFormatter.stringJustifyToHex("TAG No.  품목", "금액", 47) + "0A";
                printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr")+"0A";
                for(int i = 0 ; i < pickupItemsV2.length(); i++){
                    JSONObject item = pickupItemsV2.getJSONObject(i);

                    String isRepair = (item.getJSONArray("repair").length() > 0)?"수선":null;
                    String isAdditional = (item.getJSONArray("additional").length() > 0)?"추가기술":null;
                    String isPart = (item.getJSONArray("part").length() > 0)?"부속품":null;
                    String isService = item.getString("isService");
                    String isReLaundry = item.getString("isReLaundry");

                    int sp = 0;
                    if(item.isNull("sp")){
                        sp = item.getInt("itemSP");
                    }else{
                        sp = item.getInt("sp");
                    }

                    String itemTitle = item.getString("itemTitle");
                    itemTitle = itemTitle.replace("\n","");

                    switch(sp){
                        case 1:
                            itemTitle += "(일반)";
                            break;
                        case 2:
                            itemTitle += "(명품)";
                            break;
                        case 3:
                            itemTitle += "(아동)";
                            break;
                    }

                    if(isService != null){
                        if("Y".equals(isService)){
                            itemTitle += " -무료서비스";
                        }
                    }
                    if(isReLaundry != null){
                        if("Y".equals(isReLaundry)){
                            itemTitle += " -재세탁";
                        }
                    }

                    String itemName = MZValidator.getTagFormmater(pickupItemsV2.getJSONObject(i).getInt("tagId"),4) + "    " + itemTitle;
                    String itemPrice = MZValidator.toNumFormat(item.getInt("confirmPrice"));
                    printString += UnicodeFormatter.stringJustifyToHex(itemName, itemPrice, 47) + "0A";
                    printString += "0A";

                    if(isRepair != null){
                        for(int j = 0 ; j < item.getJSONArray("repair").length(); j++){
                            String repairPrice = MZValidator.toNumFormat(item.getJSONArray("repair").getJSONObject(j).getInt("additionalPrice"));
                            printString += UnicodeFormatter.stringJustifyToHex("         " + "+ 수선 :" + item.getJSONArray("repair").getJSONObject(j).getString("additionalItemTitle"), "("+repairPrice+")", 47) + "0A";
                        }
                    }
                    if(isAdditional != null){
                        for(int j = 0 ; j < item.getJSONArray("additional").length(); j++){
                            String addPrice = MZValidator.toNumFormat(item.getJSONArray("additional").getJSONObject(j).getInt("additionalPrice"));
                            printString += UnicodeFormatter.stringJustifyToHex("         " + "+ 기술료 :" + item.getJSONArray("additional").getJSONObject(j).getString("additionalItemTitle"), "("+addPrice+")", 47) + "0A";
                        }
                    }
                    if(isPart != null){
                        String parts = "";
                        for(int j = 0 ; j < item.getJSONArray("part").length(); j++){
                            if(j == 0){
                                parts += item.getJSONArray("part").getJSONObject(j).getString("partTitle");
                            }else{
                                parts += ", " + item.getJSONArray("part").getJSONObject(j).getString("partTitle");
                            }
                        }
                        printString += UnicodeFormatter.stringToHex("         " + "+ 부속품 :" + parts, "euc-kr") + "0A";
                    }

                    if(item.getJSONArray("repair").length() + item.getJSONArray("additional").length() + item.getJSONArray("part").length() > 0){
                        printString += "0A";
                    }

                }

                /*
                    현장접수는 배송비 추가가 없음
                 */
//                if(currentJsonData.getInt("deliveryPrice") < 20000){
//                    printString += "0A";
//                    printString += UnicodeFormatter.stringJustifyToHex("         배송비 (20,000원 이하)","2,000", 47) + "0A";
//                }

                printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr")+"0A";
                printString += "13"+UnicodeFormatter.stringJustifyToHex("총 수량 :",pickupItemsV2.length()+"개", 47) +  "140A";
                printString += "13"+UnicodeFormatter.stringJustifyToHex("총 금액 :",MZValidator.toNumFormat(currentJsonData.getInt("deliveryPrice")) + "원", 47) +  "140A";
                printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr")+"0A";
                printString += "0A";

                String page1PrintString = printString;

                printString += "0A";
                printString += UnicodeFormatter.stringToHex("(상점 보관용)", "euc-kr") + "0A";

                // 같은내용 2장 출력
                printString += "1E1E1E" + page1PrintString;

                printString += "13"+UnicodeFormatter.stringToHex(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_VIRTUAL_NAME), "euc-kr") + "140A";
                printString += UnicodeFormatter.stringToHex(MZValidator.validTelNumber(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_VIRTUAL_PHONE)), "euc-kr") + "0A";
                printString += "0A";
                printString += UnicodeFormatter.stringToHex("저희 세탁소를 찾아주셔서 감사합니다.", "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("고객님의 소중한 의류를 성심껏 관리하겠습니다.", "euc-kr") + "0A";
//                printString += "0A";
//                printString += UnicodeFormatter.stringToHex("우리동네 모바일 세탁소 리화이트", "euc-kr") + "0A";
//                printString += UnicodeFormatter.stringToHex("www.rewhite.me", "euc-kr") + "0A";
                printString += "0A";

                int storeId = Integer.parseInt(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_ID));
                printString += "11"+UnicodeFormatter.stringToHex(BarcodeStringUtil.getLocalOrderBarcodeString(storeId ,currentOrderId), "euc-kr") + "120A";
                printString += UnicodeFormatter.stringToHex("(고객 전달용)", "euc-kr") + "0A";
                printString += "0A";
                printString += "0A";

                printString += "1D1D1D";
                //////////////////
                String hexSize = UnicodeFormatter.stringToHex(String.format(Locale.KOREA, "%04d", (printString.length()/2)+4));
                contentString += hexSize + printString;
                contentString += "1c";
                contentString += "1c";
            } catch (UnsupportedEncodingException | JSONException e) {
                e.printStackTrace();
            }
            byte[] content = new byte[contentString.length()/2];
            content = UnicodeFormatter.hexToByteArray(contentString);

            try{
                Bundle b = new Bundle();
                b.putByteArray("content", content); // for example
                msg.setData(b);
                mService.send(msg);
            }catch(RemoteException e){
                e.printStackTrace();
            }
        }else{
            DUtil.alertShow(this, "PRINT가 연결되지 않았습니다. 연결상태를 확인해주세요.");
        }
    }

    public void printAction(View button){
        if(currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)){

        }else if(currentVanType.equals(VanType.FIRSTDATA)){
            printFDK();
        }else if(currentVanType.equals(VanType.SMARTRO)){
            printSmartro();
        }
    }

    public void backStep(View button){
        finish();
    }
//    private void popupSearch(){
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                Intent intent = new Intent();
//                intent.setClass(mCtx, UserSearchActivity.class);
//                intent.putExtra("type","OUTPUT");
//                startActivityForResult(intent, SEARCH_USER_RESULT);
//                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
//            }
//        }, 0);
//    }

    private TabletOrderListViewAdapter orderAdapter;
    private ArrayList<TabletOrderListItem> orderData;

    public void findOrderByUserId(String userId){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("userId", userId);
        params.put("page", 1);
        params.put("block", 50);
        params.put("k", 1);
        NetworkClient.post(Constants.GET_ORDER_DATA_BY_USERID, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GET_ORDER_DATA_BY_USERID, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_ORDER_DATA_BY_USERID, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //
                            //aq.id(R.id.empty).gone();
                            aq.id(R.id.result_area).gone();
                            aq.id(R.id.empty).visible();

                            aq.id(R.id.order_count).text("0");

                        } else {
                            ListView orderListView = (ListView)findViewById(R.id.listView);
                            orderData = new ArrayList<>();
                            JSONArray orderInfo = jsondata.getJSONArray("data");

                            if(orderInfo.length() > 0){

                                aq.id(R.id.order_count).text(orderInfo.length() + "");
                                //String userId = orderInfo.getJSONObject(0).getString("userId");

                                int orderIdPosition = 0;

                                for (int i = 0; i < orderInfo.length(); i++) {

                                    String orderId = orderInfo.getJSONObject(i).getString("orderId");
                                    if(selectedOrderId != null){
                                        if(selectedOrderId.equals(orderId)){
                                            orderIdPosition = i;
                                        }
                                    }

                                    String registerDateApp = orderInfo.getJSONObject(i).getString("registerDateApp");
                                    String orderPickupItemMessage = orderInfo.getJSONObject(i).getString("orderPickupItemMessage");
                                    String isPayment = orderInfo.getJSONObject(i).getString("isPayment");
                                    String isReceivable = orderInfo.getJSONObject(i).getString("isReceivable");
                                    int deliveryPrice = orderInfo.getJSONObject(i).getInt("deliveryPrice");
                                    String jsonData = orderInfo.getJSONObject(i).toString();

                                    TabletOrderListItem aItem = new TabletOrderListItem(orderId, registerDateApp, orderPickupItemMessage, isPayment, isReceivable, deliveryPrice, jsonData);
                                    orderData.add(aItem);

                                }

                                orderAdapter = new TabletOrderListViewAdapter(mCtx, R.layout.tablet_order_history_item, orderData);
                                orderAdapter.notifyDataSetChanged();

                                if (orderListView != null) {
                                    orderListView.setAdapter(orderAdapter);
                                    orderListView.invalidate();
                                    orderListView.invalidateViews();
                                    orderAdapter.notifyDataSetChanged();
                                }
                                orderListView.invalidate();

                                getDataByOrderId(orderInfo.getJSONObject(orderIdPosition).getString("orderId"));

                                //aq.id(R.id.empty).gone();
                            }else{
                                //aq.id(R.id.empty).visible();
                            }

                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void itemSelected(int position, JSONObject jsondata) {
        Log.i("itemSel", jsondata.toString());

        try {
            getDataByOrderId(jsondata.getString("orderId"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    int currentOrderId;
    JSONObject currentJsonData;

    public void getDataByOrderId(String _orderId) {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        selectedOrderId = _orderId;
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_DETAIL, result);

                    JSONObject resultJson = new JSONObject(result);

                    if ("S0000".equals(resultJson.getString("resultCode"))) {
                        if (resultJson.isNull("data") || resultJson.getJSONObject("data") == null) {
                            //
                            //aq.id(R.id.result_area).visible();
                            //aq.id(R.id.empty).visible();
                        } else {
                            //
                            aq.id(R.id.empty).gone();
                            aq.id(R.id.result_area).visible();

                            JSONObject jsondata = resultJson.getJSONObject("data");
                            currentJsonData = jsondata;

                            aq.id(R.id.output_layout).gone();
                            aq.id(R.id.cancel_layout).gone();
                            aq.id(R.id.btn_cash_pay_cancel).gone();
                            aq.id(R.id.btn_card_pay_cancel).gone();
                            aq.id(R.id.status_mark).visible();

                            aq.id(R.id.btn_mod).visible();
                            aq.id(R.id.btn_cancel).visible();

                            if(jsondata.getInt("orderStatus") == 24){
                                // 완료
                                aq.id(R.id.btn_unpay).gone();
                                aq.id(R.id.paymethod_layout).gone();
                                aq.id(R.id.btn_pre_pay).gone();

                                aq.id(R.id.btn_mod).gone();
                                aq.id(R.id.btn_cancel).gone();

                                aq.id(R.id.status_mark).image(R.mipmap.mark_output_completed);
                            }else{
                                if(jsondata.getInt("orderStatus") == 91){
                                    // 취소
                                    aq.id(R.id.btn_mod).gone();
                                    aq.id(R.id.btn_cancel).gone();

                                    aq.id(R.id.btn_unpay).gone();
                                    aq.id(R.id.paymethod_layout).gone();
                                    aq.id(R.id.btn_pre_pay).gone();

                                    aq.id(R.id.status_mark).image(R.mipmap.mark_output_cancelled);
                                }else{
                                    if("Y".equals(jsondata.getString("isPayment"))){
                                        aq.id(R.id.paymethod_layout).gone();
                                        aq.id(R.id.btn_pre_pay).gone();
                                        aq.id(R.id.btn_unpay).gone();
                                        aq.id(R.id.output_layout).visible();
                                        aq.id(R.id.status_mark).image(R.mipmap.mark_paid);
                                    }else{
                                        // 미결제시
                                        aq.id(R.id.paymethod_layout).visible();
                                        aq.id(R.id.btn_pre_pay).visible();
                                        aq.id(R.id.btn_unpay).visible();
                                        aq.id(R.id.status_mark).image(R.mipmap.mark_unpay);
                                        //aq.id(R.id.btn_cash_pay).visible();
                                        //aq.id(R.id.btn_card_pay).visible();
                                    }
                                }

                            }

                            currentOrderId = jsondata.getInt("orderId");

                            CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_order_id).getTextView());
                            aq.id(R.id.text_order_id).text("" + jsondata.getInt("orderId"));
                            aq.id(R.id.text_order_date).text("주문일시 " + TimeUtil.convertTimestampToString(jsondata.getLong("registerDateApp")) );


                            if(!jsondata.isNull("payments")){
                                if(jsondata.getJSONArray("payments").length() > 0){

                                    String paymentType = jsondata.getJSONArray("payments").getJSONObject(0).getString("paymentType");
                                    if("P91".equals(paymentType)){
                                        // 카드결제 한경우
                                        aq.id(R.id.cancel_layout).visible();
                                        aq.id(R.id.btn_card_pay_cancel).visible();
                                        aq.id(R.id.btn_cash_pay_cancel).gone();

                                        String encJson = jsondata.getJSONArray("payments").getJSONObject(0).getString("returnValue");
                                        Log.e("enc", encJson + "");
                                        String decJson = MZCrypto.getBase64decode(encJson);
                                        Log.e("dec", decJson + "");
                                        JSONObject payInfo = new JSONObject(decJson);

                                        Date posAuthDate = TimeUtil.getDateTimeSimpleDatePOS(payInfo.getString("authDate"));
                                        if(payInfo.isNull("authDate")){
                                            posAuthDate = new Date();
                                        }else{
                                            posAuthDate = TimeUtil.getDateTimeSimpleDatePOS(payInfo.getString("authDate"));
                                        }

                                        aq.id(R.id.text_order_date).text("주문일시 "
                                                + TimeUtil.convertTimestampToString(jsondata.getLong("registerDateApp"))
                                                + "  |  결제승인번호 : " + payInfo.getString("authCode")
                                                + "  |  결제완료 : " + TimeUtil.convertTimestampToString(posAuthDate.getTime())
                                        );

                                    }else if("P92".equals(paymentType)){
                                        // 현금영수증 발행 한경우
                                        aq.id(R.id.cancel_layout).visible();
                                        aq.id(R.id.btn_card_pay_cancel).gone();

                                        if(jsondata.getJSONArray("payments").getJSONObject(0).isNull("returnValue") || "".equals(jsondata.getJSONArray("payments").getJSONObject(0).getString("returnValue"))){
                                            aq.id(R.id.text_order_date).text("주문일시 "
                                                    + TimeUtil.convertTimestampToString(jsondata.getLong("registerDateApp")));
                                            aq.id(R.id.btn_cash_pay_cancel).gone();
                                        }else{

                                            String encJson = jsondata.getJSONArray("payments").getJSONObject(0).getString("returnValue");
                                            Log.e("enc", encJson + "");
                                            String decJson = MZCrypto.getBase64decode(encJson);
                                            Log.e("dec", decJson + "");
                                            JSONObject payInfo = new JSONObject(decJson);
                                            Date posAuthDate;
                                            if("".equals(payInfo.getString("authCode"))){
                                                posAuthDate = new Date();
                                                aq.id(R.id.text_order_date).text("주문일시 "
                                                        + TimeUtil.convertTimestampToString(jsondata.getLong("registerDateApp"))
                                                        + "  |  결제완료 : " + TimeUtil.convertTimestampToString(jsondata.getLong("paymentDateApp"))
                                                );
                                                aq.id(R.id.btn_cash_pay_cancel).gone();
                                            }else{
                                                aq.id(R.id.btn_cash_pay_cancel).visible();
                                                posAuthDate = TimeUtil.getDateTimeSimpleDatePOS(payInfo.getString("authDate"));
                                                aq.id(R.id.text_order_date).text("주문일시 "
                                                        + TimeUtil.convertTimestampToString(jsondata.getLong("registerDateApp"))
                                                        + "  |  현금영수증 승인번호 : " + payInfo.getString("authCode")
                                                        + "  |  발행일시 : " + TimeUtil.convertTimestampToString(posAuthDate.getTime())
                                                );
                                                aq.id(R.id.btn_cash_pay_cancel).visible();
                                            }
                                        }
                                    }
                                }
                            }


                            aq.id(R.id.totalprice_text).text(MZValidator.toNumFormat(jsondata.getInt("deliveryPrice"))+" 원");

                            pickupItemsV2 = jsondata.getJSONArray("pickupItemsV2");
                            ListView pickItemsListView = (ListView)findViewById(R.id.itemListView);
                            itemsAdapter = new TableItemElementListViewAdapter(mCtx, R.layout.tablet_order_pickup_item, pickupItemsV2);
                            itemsAdapter.notifyDataSetChanged();
                            pickItemsListView.setAdapter(itemsAdapter);

                            aq.id(R.id.pick_count_text).text("접수수량 " + pickupItemsV2.length() + "개");

                        }

                    } else {
                        aq.id(R.id.empty).visible();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void showMessage(final String _message){
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {

                mProgressDialog = ProgressDialog.show(LocalOrderOutputActivity.this,"",
                        _message,true);
                mHandler.postDelayed( new Runnable()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            if (mProgressDialog!=null&&mProgressDialog.isShowing()){
                                mProgressDialog.dismiss();
                            }
                        }
                        catch ( Exception e )
                        {
                            e.printStackTrace();
                        }
                    }
                }, 2000);
            }
        } );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String encPayAuthJSONString = null;
        String encReceiptAuthJSONString = null;

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PAYMENT_VAN_CARD:
                    encPayAuthJSONString = null;
                    try {
                        JSONObject payAuthData = new JSONObject();
                        payAuthData.put("authCode", data.getStringExtra("AUTH_CODE"));
                        payAuthData.put("authDate", data.getStringExtra("AUTH_DATE"));
                        String payAuthJSONString = payAuthData.toString();
                        encPayAuthJSONString = MZCrypto.getBase64encode(payAuthJSONString);

                        Log.e("== paid enc", encPayAuthJSONString);
                        Log.e("== paid enc-dec", MZCrypto.getBase64decode(encPayAuthJSONString));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    paymentLocalPaymentProcess(true, encPayAuthJSONString, data.getIntExtra("PAY_PRICE", 0), "P91", true);
                    showMessage("성공적으로 카드결제가 완료되었습니다.");
                    break;
                case PAYMENT_VAN_CASH:
                    encReceiptAuthJSONString = null;
                    try {
                        JSONObject payAuthData = new JSONObject();
                        payAuthData.put("authCode", data.getStringExtra("AUTH_CODE"));
                        payAuthData.put("authDate", data.getStringExtra("AUTH_DATE"));
                        payAuthData.put("authType", data.getStringExtra("AUTH_TYPE"));
                        String payAuthJSONString = payAuthData.toString();
                        encReceiptAuthJSONString = MZCrypto.getBase64encode(payAuthJSONString);

                        Log.e("== paid enc", encReceiptAuthJSONString);
                        Log.e("== paid enc-dec", MZCrypto.getBase64decode(encReceiptAuthJSONString));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    paymentLocalPaymentProcess(true, encReceiptAuthJSONString, data.getIntExtra("PAY_PRICE", 0), "P92", true);
                    showMessage("성공적으로 결제처리되었습니다.");
                    break;
                case PAYMENT_PRE_VAN_CARD:
                    encPayAuthJSONString = null;
                    try {
                        JSONObject payAuthData = new JSONObject();
                        payAuthData.put("authCode", data.getStringExtra("AUTH_CODE"));
                        payAuthData.put("authDate", data.getStringExtra("AUTH_DATE"));
                        String payAuthJSONString = payAuthData.toString();
                        encPayAuthJSONString = MZCrypto.getBase64encode(payAuthJSONString);

                        Log.e("== paid enc", encPayAuthJSONString);
                        Log.e("== paid enc-dec", MZCrypto.getBase64decode(encPayAuthJSONString));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    paymentLocalPaymentProcess(true, encPayAuthJSONString, data.getIntExtra("PAY_PRICE", 0), "P91", false);
                    showMessage("성공적으로 카드결제가 완료되었습니다.");
                    aq.id(R.id.order_completed_layout).gone();
                    break;
                case PAYMENT_PRE_VAN_CASH:
                    encReceiptAuthJSONString = null;
                    try {
                        JSONObject payAuthData = new JSONObject();
                        payAuthData.put("authCode", data.getStringExtra("AUTH_CODE"));
                        payAuthData.put("authDate", data.getStringExtra("AUTH_DATE"));
                        payAuthData.put("authType", data.getStringExtra("AUTH_TYPE"));
                        String payAuthJSONString = payAuthData.toString();
                        encReceiptAuthJSONString = MZCrypto.getBase64encode(payAuthJSONString);

                        Log.e("== paid enc", encReceiptAuthJSONString);
                        Log.e("== paid enc-dec", MZCrypto.getBase64decode(encReceiptAuthJSONString));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    paymentLocalPaymentProcess(true, encReceiptAuthJSONString, data.getIntExtra("PAY_PRICE", 0), "P92", false);
                    showMessage("성공적으로 결제처리되었습니다.");
                    aq.id(R.id.order_completed_layout).gone();
                    break;
                case MOD_USER_RESULT:

                    try {
                        selectedUserData = new JSONObject(data.getStringExtra("userData"));

                        aq.id(R.id.nickname_text).text(selectedUserData.getString("userName"));
                        aq.id(R.id.address_text).text(selectedUserData.getString("address1") + " " +selectedUserData.getString("address2"));
                        aq.id(R.id.phone_text).text(MZValidator.validTelNumber(selectedUserData.getString("phone")));
                        aq.id(R.id.ordercount_text).text(selectedUserData.getString("orderCount"));
                        aq.id(R.id.recv_price).text(MZValidator.toNumFormat(selectedUserData.getInt("receivablePrice")) + " 원");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;

                case MOD_ORDER_ELEMENT:
                    String recvOrderId = data.getIntExtra("orderId", -1) + "";
                    getDataByOrderId(recvOrderId);
                    break;

                default:
                    break;
            }
        }
    }

    JSONArray pickupItemsV2;
    private TableItemElementListViewAdapter itemsAdapter;

    public void searchUserId(String _userId){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("search", "userId");

        params.put("keyword", _userId);

        params.put("page", 1);
        params.put("block", 50);
        params.put("k", 1);
        NetworkClient.post(Constants.USER_SEARCH, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.USER_SEARCH, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.USER_SEARCH, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {

                        } else {
                            JSONArray orderInfo = jsondata.getJSONArray("data");

                            for (int i = 0; i < orderInfo.length(); i++) {
                                JSONObject jsonData = orderInfo.getJSONObject(i);
                                aq.id(R.id.text_unpay_price).text(MZValidator.toNumFormat(jsonData.getInt("receivablePrice"))+" 원");
                            }

                        }

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }
}
