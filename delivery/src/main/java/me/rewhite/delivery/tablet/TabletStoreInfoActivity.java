package me.rewhite.delivery.tablet;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.delivery.R;
import me.rewhite.delivery.TabletFragment.TabletSalesInfoFragment;
import me.rewhite.delivery.TabletFragment.TabletStoreInfoFragment;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class TabletStoreInfoActivity extends TabletBaseActivity {

    private final static String TAG = TabletStoreInfoActivity.class.getSimpleName();
    public static final String FRAGMENT_STORE_INFO = "store_info";
    public static final String FRAGMENT_SALES_INFO = "sales_info";
    AQuery aq;
    private JSONObject storeInfo;

    ProgressDialog mProgressDialog;
    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablet_store_info);

        showDialog();

        aq = new AQuery(this);
        aq.id(R.id.btn_back).clicked(this, "closeClicked");

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.screen_title).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_1).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_2).getTextView());

        aq.id(R.id.btn_1).clicked(this, "showData").tag(0).background(R.mipmap.tablet_subnav_selected);
        aq.id(R.id.btn_2).clicked(this, "showData").tag(1).background(R.mipmap.tablet_subnav_normal);
        aq.id(R.id.rep_icon).image(R.mipmap.icon_top_storeinfo);

        initialize();
    }

    public void showData(View button){
        int tag = (int)((Button)button).getTag();
        switch (tag){
            case 0:
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("내 상점정보");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_storeinfo);
                showFragment(FRAGMENT_STORE_INFO);
                break;
            case 1:
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_salesinfo);
                aq.id(R.id.screen_title).text("영업정보");
                showFragment(FRAGMENT_SALES_INFO);
                break;

        }

    }

    public void closeClicked(View button){
        finish();
    }

    private void initialize(){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.GET_STORE_INFORMATION, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GET_STORE_INFORMATION, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_STORE_INFORMATION, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        storeInfo = jsondata.getJSONObject("data");

                        showFragment(FRAGMENT_STORE_INFO);
                    }
                    dismissDialog();
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    String transitionName = "";

    public void showFragment(String fragmentName) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);

        FragmentManager fm = getSupportFragmentManager();
//        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(fragmentName);

        if (FRAGMENT_STORE_INFO.equals(fragmentName)) {
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletStoreInfoFragment();
            Bundle storeInfoBundle = new Bundle();
            storeInfoBundle.putString("storeInfo", storeInfo.toString());
            fragment.setArguments(storeInfoBundle);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, fragment).commitAllowingStateLoss();
        }else if(FRAGMENT_SALES_INFO.equals(fragmentName)){
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletSalesInfoFragment();
            Bundle storeInfoBundle = new Bundle();
            storeInfoBundle.putString("storeInfo", storeInfo.toString());
            fragment.setArguments(storeInfoBundle);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, fragment).commitAllowingStateLoss();
        }

        Log.e(TAG, "[TabletStoreInfoFragment] Create : " + fragmentName);
    }
}
