package me.rewhite.delivery.tablet;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.StringUtil;
import me.rewhite.delivery.util.TimeUtil;
import me.rewhite.delivery.util.ValidateUtil;

public class UserModActivity extends TabletBaseActivity {

    private static final String TAG = "UserModActivity";
    TextView input_search_text;
    public final Context mCtx = this;
    AQuery aq;
    int currentType = 0;
    JSONObject selectedUserData;
    String userId;

    ProgressDialog mProgressDialog;
    String searchType = "INPUT";

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(UserModActivity.this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(CommonUtility.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablet_activity_user_mod);

        Log.w(TAG, "onCreate");

        aq = new AQuery(this);
        aq.hardwareAccelerated11();

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("userData") != null) {
                try {
                    Log.e("userData", intent.getStringExtra("userData"));

                    selectedUserData = new JSONObject(intent.getStringExtra("userData"));

                    userId = selectedUserData.getString("userId");
                    aq.id(R.id.text_name).text(selectedUserData.getString("userName"));
                    aq.id(R.id.text_address).text(selectedUserData.getString("address1") + " " +selectedUserData.getString("address2"));
                    aq.id(R.id.text_phone).text(MZValidator.validTelNumber(selectedUserData.getString("phone")));
                    aq.id(R.id.text_memo).text(selectedUserData.getString("memo"));
                    aq.id(R.id.text_email).text(selectedUserData.getString("email"));

                    if(!"".equals(selectedUserData.getString("birth"))){
                        String[] birthData = selectedUserData.getString("birth").split("\\.");
                        Date birth = TimeUtil.getDateTimeSimpleDateFormat(selectedUserData.getString("birth"));

                        selectedBirth = TimeUtil.getDisplayDateFormat(birth);
                        aq.id(R.id.text_birth).text(TimeUtil.getSimpleDisplayDateFormat(birth));
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }

        aq.id(R.id.btn_close).clicked(this, "closeClicked");

        aq.id(R.id.btn_submit).clicked(this, "userModSubmitAction");
        aq.id(R.id.btn_remove).clicked(this, "userRemoveSubmitAction");

        aq.id(R.id.text_birth).clicked(this, "pickBirthData");
    }

    private int year;
    private int month;
    private int day;
    String selectedBirth = null;

    public void pickBirthData(View button) {
        final Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        showDialog(DATE_DIALOG_ID);
    }

    public void userRemoveSubmitAction(View button){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
        alertDialogBuilder.setCancelable(true).setMessage("회원정보를 삭제하시겠습니까? 삭제하면 복구불가합니다.")
                .setPositiveButton("삭제", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        removeUserProcess(userId);
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    static final int DATE_DIALOG_ID = 100;

    @Override
    protected Dialog onCreateDialog(int id) {

        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener, 1987, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            Date birth = calendar.getTime();

            selectedBirth = TimeUtil.getDisplayDateFormat(birth);
            aq.id(R.id.text_birth).text(TimeUtil.getSimpleDisplayDateFormat(birth));

            // set selected date into Date Picker

        }
    };

    public void userModSubmitAction(View button){
        final String name = aq.id(R.id.text_name).getText().toString();
        final String phone = aq.id(R.id.text_phone).getText().toString();
        final String address = aq.id(R.id.text_address).getText().toString();
        final String email = aq.id(R.id.text_email).getText().toString();
        final String birth = selectedBirth;//aq.id(R.id.text_birth).getText().toString();
        final String memo = aq.id(R.id.text_memo).getText().toString();

        if(StringUtil.isNullOrEmpty(name) || StringUtil.isNullOrEmpty(phone) || StringUtil.isNullOrEmpty(address)){
            DUtil.alertShow(this, "이름, 전화번호, 주소는 필수입력 항목입니다.");
        }else{
            if(!"".equals(email) && !ValidateUtil.checkEmail(email)){
                DUtil.alertShow(this, "이메일 형식이 잘못되었습니다.");
            }else{
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                alertDialogBuilder.setCancelable(true).setMessage("회원정보를 수정하시겠습니까?")
                        .setPositiveButton("정보수정", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                modUserProcess(userId, name, phone, address, email, birth, memo);
                            }
                        }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }

            modUserProcess(userId, name, phone, address, email, birth, memo);
        }
    }

    private void removeUserProcess(String _userId){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("userId", _userId);
        params.put("mode", "D");
        params.put("k", 1);

        NetworkClient.post(Constants.USER_MOD, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.USER_MOD, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.USER_MOD, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {

                        } else {

                            Intent resultData = new Intent();
                            resultData.putExtra("userData", jsondata.getString("data"));
                            setResult(Activity.RESULT_OK, resultData);
                            finish();
                        }

                    } else {
                        DUtil.alertShow(UserModActivity.this, jsondata.getString("message"));
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private void modUserProcess(String _userId, String _userName, String _phone, String _address, String _email, String _birth, String _memo) {

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("userId", _userId);
        params.put("userName", _userName);
        params.put("userPhone", _phone);
        params.put("userAddress", _address);
        params.put("userEmail", _email);
        params.put("birth", _birth);
        params.put("memo", _memo);
        params.put("mode", "U");
        params.put("k", 1);

        NetworkClient.post(Constants.USER_MOD, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.USER_MOD, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.USER_MOD, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {

                        } else {

                            Intent resultData = new Intent();
                            resultData.putExtra("userData", jsondata.getString("data"));
                            setResult(Activity.RESULT_OK, resultData);
                            finish();
                        }

                    } else {
                        DUtil.alertShow(UserModActivity.this, jsondata.getString("message"));
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
    }

    public void closeClicked(View button){
        setResult(Activity.RESULT_CANCELED);
        finish();
    }
}
