package me.rewhite.delivery.tablet;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.TabletFragment.TabletRewhiteFinanceFragment;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class TabletFinanceActivity extends TabletBaseActivity {

    private final static String TAG = TabletFinanceActivity.class.getSimpleName();
    public static final String FRAGMENT_REWHITE_FINANCE = "rewhite_finance";
    public static final String FRAGMENT_LOCALHOST_FINANCE = "localhost_finance";
    AQuery aq;
    public JSONArray localFinanceInfo;
    public JSONArray rewhiteFinanceInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablet_finance);

        aq = new AQuery(this);
        aq.id(R.id.btn_back).clicked(this, "closeClicked");

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.screen_title).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_1).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_2).getTextView());

        aq.id(R.id.btn_1).clicked(this, "showData").tag(0).background(R.mipmap.tablet_subnav_selected);
        aq.id(R.id.btn_2).clicked(this, "showData").tag(1).background(R.mipmap.tablet_subnav_normal);

        aq.id(R.id.rep_icon).image(R.mipmap.icon_top_finance_local);

        initialize(FRAGMENT_LOCALHOST_FINANCE);
    }

    public void showData(View button){
        int tag = (int)((Button)button).getTag();
        switch (tag){
            case 0:
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("점포 매출정보");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_finance_local);
                initialize(FRAGMENT_LOCALHOST_FINANCE);
                break;
            case 1:
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("리화이트 매출정보");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_finance_rewhite);
                initialize(FRAGMENT_REWHITE_FINANCE);
                break;
        }

    }

    public void closeClicked(View button){
        finish();
    }

    private void initialize(final String _loadType){
        String loadUrl = Constants.GET_REWHITE_FINANCE;
        if(FRAGMENT_LOCALHOST_FINANCE.equals(_loadType)){
            loadUrl = Constants.GET_LOCAL_FINANCE;
        }else{
            loadUrl = Constants.GET_REWHITE_FINANCE;
        }
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(loadUrl, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(_loadType, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(_loadType, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if(FRAGMENT_LOCALHOST_FINANCE.equals(_loadType)){
                            localFinanceInfo = jsondata.getJSONArray("data");
                            showFragment(FRAGMENT_LOCALHOST_FINANCE);
                        }else{
                            rewhiteFinanceInfo = jsondata.getJSONArray("data");
                            showFragment(FRAGMENT_REWHITE_FINANCE);
                        }

                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void showFragment(String fragmentName) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);
        showFragment(fragmentName, null, null);
    }

    String transitionName = "";

    public void showFragment(String fragmentName, View sharedElement, Bundle args) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(fragmentName);
        Bundle pArgs = new Bundle();
        if (FRAGMENT_REWHITE_FINANCE.equals(fragmentName)) {
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            pArgs.putString("financeType",FRAGMENT_REWHITE_FINANCE);
            fragment = new TabletRewhiteFinanceFragment();
        }else if(FRAGMENT_LOCALHOST_FINANCE.equals(fragmentName)){
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            pArgs.putString("financeType",FRAGMENT_LOCALHOST_FINANCE);
            fragment = new TabletRewhiteFinanceFragment();
        }

        Log.e(TAG, "[TabletStoreInfoFragment] Create : " + fragmentName);

        if (null != pArgs) {
            fragment.setArguments(pArgs);
        }

        //ft.setCustomAnimations(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        ft.replace(R.id.content_frame, fragment, fragmentName);
        ft.addToBackStack(fragmentName);
        //ft.commitAllowingStateLoss();
        ft.commit();

    }

}
