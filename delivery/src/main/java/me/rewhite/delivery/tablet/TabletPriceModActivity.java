package me.rewhite.delivery.tablet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class TabletPriceModActivity extends TabletBaseActivity {

    private static final String TAG = "TabletPriceModActivity";
    public final Context mCtx = this;
    AQuery aq;
    String currentType = "";

    private static final int ADD_USER_RESULT = 9;

    ProgressDialog mProgressDialog;
    JSONObject itemData;
    int groupPosition;
    int childPosition;
    EditText inputDefaultPrice = null;

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(TabletPriceModActivity.this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(CommonUtility.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablet_activity_mod_price);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("data") != null) {
                try {
                    itemData = new JSONObject(intent.getStringExtra("data"));
                    currentType = intent.getStringExtra("type");
                    groupPosition = intent.getIntExtra("groupPosition",0);
                    childPosition = intent.getIntExtra("childPosition",0);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        Log.w(TAG, "onCreate");

        aq = new AQuery(this);
        aq.hardwareAccelerated11();

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.input_price_01).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.input_price_02).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.input_price_03).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.input_price_04).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.title_01).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.title_02).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.title_03).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.title_04).getTextView());

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.btn_submit).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.product_name).getTextView());

        try {
            aq.id(R.id.product_name).text(itemData.getString("itemTitle"));
            aq.id(R.id.input_price_02).text(itemData.getString("storePrice2"));
            aq.id(R.id.input_price_03).text(itemData.getString("storePrice3"));


            LinearLayout ll = (LinearLayout)findViewById(R.id.desc_area);
            LinearLayout ll2 = (LinearLayout)findViewById(R.id.input_area);
            if("Y".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.IS_VISIT_PRICE))){
                ll.setWeightSum(5f);
                ll2.setWeightSum(5f);
                aq.id(R.id.input_price_01).text(itemData.getString("storePrice4"));
                aq.id(R.id.input_price_04).text(itemData.getString("storePrice1"));
            }else{
                aq.id(R.id.input_price_04).gone();
                aq.id(R.id.title_04).gone();
                ll.setWeightSum(4f);
                ll2.setWeightSum(4f);
                aq.id(R.id.input_price_01).text(itemData.getString("storePrice1"));
                aq.id(R.id.input_price_04).text(itemData.getString("storePrice4"));
            }

            double rate1 = Double.parseDouble(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_RATE_1));
            double rate2 = Double.parseDouble(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_RATE_2));
            aq.id(R.id.title_01).text("일반 (100%)");
            aq.id(R.id.title_02).text("명품 (" + (int)(rate1*100) + "%)");
            aq.id(R.id.title_03).text("아동 (" + (int)(rate2*100) + "%)");
        } catch (JSONException e) {
            e.printStackTrace();
        }



        inputDefaultPrice = (EditText)findViewById(R.id.input_price_01);
        TextWatcher watcher = new TextWatcher()
        {
            @Override
            public void afterTextChanged(Editable s) {
                //텍스트 변경 후 발생할 이벤트를 작성.
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                //텍스트의 길이가 변경되었을 경우 발생할 이벤트를 작성.
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                //텍스트가 변경될때마다 발생할 이벤트를 작성.
                if (inputDefaultPrice.isFocusable())
                {
                    if(!"".equals(s.toString())){
                        int originValue = Integer.parseInt(s.toString());
                        double rate1 = Double.parseDouble(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_RATE_1));
                        double rate2 = Double.parseDouble(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_RATE_2));
                        aq.id(R.id.input_price_02).text((int)(originValue*rate1) + "");
                        aq.id(R.id.input_price_03).text((int)(originValue*rate2) + "");
                        aq.id(R.id.input_price_04).text(originValue + "");
                    }
                }
            }
        };

        //호출
        inputDefaultPrice.addTextChangedListener(watcher);

        aq.id(R.id.btn_close).clicked(this, "closeClicked");
        aq.id(R.id.btn_submit).clicked(this, "submitClicked");

    }
    int storePrice01;
    int storePrice02;
    int storePrice03;
    int storePrice04;

    public void submitClicked(View button){

        storePrice02 = Integer.parseInt(aq.id(R.id.input_price_02).getText().toString());
        storePrice03 = Integer.parseInt(aq.id(R.id.input_price_03).getText().toString());

        // 방문 일반요금 / 수거배달 일반요금 4<->1
        if("Y".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.IS_VISIT_PRICE))){
            storePrice01 = Integer.parseInt(aq.id(R.id.input_price_04).getText().toString());
            storePrice04 = Integer.parseInt(aq.id(R.id.input_price_01).getText().toString());
        }else{
            storePrice01 = Integer.parseInt(aq.id(R.id.input_price_01).getText().toString());
            storePrice04 = storePrice01;
        }

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        try {
            params.put("itemId", itemData.getString("itemId"));
            params.put("isUse", itemData.getString("isUse"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        params.put("priceType", currentType);

        params.put("storePrice1", storePrice01);
        params.put("storePrice2", storePrice02);
        params.put("storePrice3", storePrice03);
        params.put("storePrice4", storePrice04);
        params.put("k", 1);
        NetworkClient.post(Constants.SET_PRICE_ITEM, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SET_PRICE_ITEM, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.SET_PRICE_ITEM, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {

                        } else {
                            Intent resultData = new Intent();
                            resultData.putExtra("groupPosition", groupPosition);
                            resultData.putExtra("childPosition", childPosition);
                            // 방문 일반요금 / 수거배달 일반요금 4<->1
                            // 해당화면에서 Swap해서 표시
                            resultData.putExtra("storePrice1", storePrice01);
                            resultData.putExtra("storePrice2", storePrice02);
                            resultData.putExtra("storePrice3", storePrice03);
                            resultData.putExtra("storePrice4", storePrice04);
                            resultData.putExtra("isUse", itemData.getString("isUse"));
                            setResult(Activity.RESULT_OK, resultData);
                            finish();

                        }
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }


    public void closeClicked(View button){
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

}
