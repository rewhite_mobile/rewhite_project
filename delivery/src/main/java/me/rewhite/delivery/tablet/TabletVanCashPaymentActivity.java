package me.rewhite.delivery.tablet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;

import com.androidquery.AQuery;
import com.balysv.materialripple.MaterialRippleLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import fdk.FDK_Module;
import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.TimeUtil;
import me.rewhite.delivery.util.UnicodeFormatter;
import me.rewhite.delivery.van.CatManager;
import me.rewhite.delivery.van.VanType;

public class TabletVanCashPaymentActivity extends TabletBaseActivity {

    private static final String TAG = "TabletVanCashPaymentActivity";
    public final Context mCtx = this;
    AQuery aq;

    ProgressDialog mProgressDialog;
    JSONObject orderInfo;
    JSONArray pickupItemsV2;

    // 스마트로
    private Messenger mService = null;    // 서비스와 통신하는데 사용되는 메신저
    private boolean mBound = false;    // 서비스 연결 여부
    VanReceiver vanReceiver;

    String currentVanType;

    @Override
    protected void onStart() {

        currentVanType = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.VAN_TYPE);
        if(currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)){

        }else if(currentVanType.equals(VanType.FIRSTDATA)){

        }else if(currentVanType.equals(VanType.SMARTRO)){
            //Register BroadcastReceiver
            //to receive event from our service
            vanReceiver = new VanReceiver();
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(CatManager.INTENT_ACTION);
            registerReceiver(vanReceiver, intentFilter);
        }

        super.onStart();
    }

    @Override
    public void onStop(){
        super.onStop();
        if(currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)){

        }else if(currentVanType.equals(VanType.FIRSTDATA)){

        }else if(currentVanType.equals(VanType.SMARTRO)){
            if(mBound){
                unbindService(mConnection);
                mBound = false;
            }
            unregisterReceiver(vanReceiver);
        }

    }

    // ServiceConnection 인터페이스를 구현하는 객체를 생성한다.
    private ServiceConnection mConnection = new ServiceConnection(){

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = new Messenger(service);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0){
            mService = null;
            mBound = false;
        }
    };

    private class VanReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            // TODO Auto-generated method stub

            String RETCODE = arg1.getStringExtra("RETCODE");
            String MESSAGE = arg1.getStringExtra("MESSAGE");
            boolean RESULT = arg1.getBooleanExtra("RESULT", false);

            Log.w("vanReceiver : ", RETCODE + " , " + MESSAGE);

            if("0102".equals(RETCODE)){
                if(RESULT){
                    // 승인번호 추출필요
                    try {
                        JSONObject msg = new JSONObject(MESSAGE);
                        payCashCompleted(msg.getString("authType")+msg.getString("authCode"), msg.getString("authDate"), "P92", Integer.parseInt(msg.getString("payPrice")) );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    DUtil.alertShow(TabletVanCashPaymentActivity.this, MESSAGE);
                }
            }else if("9998".equals(RETCODE)){

            }else if("8888".equals(RETCODE)){
                if(RESULT){
                    showDialog(MESSAGE);
                }else{
                    dismissDialog();
                }
            }


        }
    }

    public void payCashCompleted(String _code, String _date, String _transactionType, int _price){

        Intent resultData = new Intent();
        resultData.putExtra("PAY_PRICE", _price);
        resultData.putExtra("AUTH_DATE", _date);
        resultData.putExtra("AUTH_CODE", _code);
        resultData.putExtra("AUTH_TYPE", _transactionType);
        setResult(Activity.RESULT_OK, resultData);
        finish();
    }

    private void setMaterialRippleLayout(View _target){
        MaterialRippleLayout.on(_target)
                .rippleColor(Color.parseColor("#FFffff"))
                .rippleAlpha(0.2f)
                .rippleHover(true)
                .rippleOverlay(true)
                .create();
    }

    public void showDialog(String _message) {
        if (mProgressDialog == null){
            mProgressDialog = new ProgressDialog(TabletVanCashPaymentActivity.this);
        }
        mProgressDialog.setMessage(_message);
        mProgressDialog.setCancelable(false);
        if(!mProgressDialog.isShowing()){
            mProgressDialog.show();
        }
    }

    public void showDialog() {
        if (mProgressDialog == null){
            mProgressDialog = new ProgressDialog(TabletVanCashPaymentActivity.this);
        }
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        if(!mProgressDialog.isShowing()){
            mProgressDialog.show();
        }
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(CommonUtility.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablet_activity_van_cash_payment);

        currentVanType = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.VAN_TYPE);
        if(currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)){

        }else if(currentVanType.equals(VanType.FIRSTDATA)){
            try {
                FDK_Module.Deliver_App_Resources_to_FDK("FTDI", this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(currentVanType.equals(VanType.SMARTRO)){
            String successDeviceIP = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.CAT_DEVICE_IP);
            if(successDeviceIP != null){
                Intent catIntent = new Intent(this, CatManager.class);
                catIntent.putExtra("ServerIP", successDeviceIP);
                bindService(catIntent, mConnection, Context.BIND_AUTO_CREATE);
            }
        }

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("order") != null) {
                try {
                    orderInfo = new JSONObject(intent.getStringExtra("order"));
                    pickupItemsV2 = new JSONArray(intent.getStringExtra("items"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        aq = new AQuery(this);
        aq.hardwareAccelerated11();

        try {
            aq.id(R.id.text_price).text(MZValidator.toNumFormat(orderInfo.getInt("deliveryPrice")) + " 원");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        aq.id(R.id.btn_close).clicked(this, "closeClicked");

        setMaterialRippleLayout((View)findViewById(R.id.btn_submit));
        setMaterialRippleLayout((View)findViewById(R.id.req_receipt));
        aq.id(R.id.btn_submit).clicked(this, "submitClicked");

        aq.id(R.id.btn_type_01).clicked(this, "typeSelectAction").tag(0);
        aq.id(R.id.btn_type_02).clicked(this, "typeSelectAction").tag(1);
        aq.id(R.id.btn_type_03).clicked(this, "typeSelectAction").tag(2);
        aq.id(R.id.req_receipt).clicked(this, "receiptRequest");
    }

    public void submitClicked(View button){
        int transactionAmount = 0;
        try {
            transactionAmount = orderInfo.getInt("deliveryPrice");

            Intent resultData = new Intent();
            resultData.putExtra("PAY_PRICE", transactionAmount);
            Date now = new Date();
            SimpleDateFormat mDateformatDateTimePOS = new SimpleDateFormat(TimeUtil.FORMAT_DATE_TIME_POS);
            now.setTime(orderInfo.getLong("paymentDateApp"));
            resultData.putExtra("AUTH_DATE", mDateformatDateTimePOS.format(now));
            resultData.putExtra("AUTH_CODE", "");
            resultData.putExtra("AUTH_TYPE", "");
            setResult(Activity.RESULT_OK, resultData);
            finish();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    String currentType = "00";
    public void typeSelectAction(View button){
        int tag = (int)button.getTag();
        switch (tag){
            case 0:
                aq.id(R.id.icon_01).image(R.mipmap.pui_order_element_color_cf0000);
                aq.id(R.id.icon_02).image(R.mipmap.pui_order_element_color_ffffff);
                aq.id(R.id.icon_03).image(R.mipmap.pui_order_element_color_ffffff);
                currentType = "00";
                break;
            case 1:
                aq.id(R.id.icon_01).image(R.mipmap.pui_order_element_color_ffffff);
                aq.id(R.id.icon_02).image(R.mipmap.pui_order_element_color_cf0000);
                aq.id(R.id.icon_03).image(R.mipmap.pui_order_element_color_ffffff);
                currentType = "01";
                break;
            case 2:
                aq.id(R.id.icon_01).image(R.mipmap.pui_order_element_color_ffffff);
                aq.id(R.id.icon_02).image(R.mipmap.pui_order_element_color_ffffff);
                aq.id(R.id.icon_03).image(R.mipmap.pui_order_element_color_cf0000);
                currentType = "99";
                break;
        }
    }

    public void ShowMsg(String strMsg)
    {
        android.support.v7.app.AlertDialog.Builder alert = new android.support.v7.app.AlertDialog.Builder(this);
        alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();     //닫기
            }
        });
        alert.setMessage(strMsg);
        alert.show();
    }

    private void cashReceiptAuth() throws Exception{
        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        String bizNo =  SharedPreferencesUtility.get(SharedPreferencesUtility.Van.BIZ_NO);
        int transactionAmount = orderInfo.getInt("deliveryPrice");
        int taxAmount = transactionAmount/11;
        int taxableAmount = transactionAmount - taxAmount;

        iProcID = FDK_Module.Creat();

        FDK_Module.Input(iProcID, "Business Number", bizNo);
        FDK_Module.Input(iProcID, "Transaction Type", currentType);
        FDK_Module.Input(iProcID, "Transaction Amount", String.valueOf(transactionAmount));
        FDK_Module.Input(iProcID, "Tax Amount", String.valueOf(taxAmount));
        FDK_Module.Input(iProcID, "Taxable Amount", String.valueOf(taxableAmount));
        FDK_Module.Input(iProcID, "Non-Taxable Amount", "0");

        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/SecuritySafe_Tell/CashReceiptAuth_K6");
        if(0 == iRet)
        {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        }
        else if(-1000 == iRet)
        {
            strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        }
        else
        {
            strMsg = "에러[" + iRet + "]\r\n";
            strData = "";
            strData = FDK_Module.Output(iProcID, "ErrorInfo");
            strMsg = strMsg + strData;
        }
        ShowMsg(strMsg);

        //Output Sample
        if(0 == iRet || -1000 == iRet)
        {
            strMsg = "Output List :\r\n";
            strMsg += "STX : " + FDK_Module.Output(iProcID, "STX") + "\r\n";
            strMsg += "Work Type : " + FDK_Module.Output(iProcID, "Work Type") + "\r\n";
            strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
            strMsg += "Business Number : " + FDK_Module.Output(iProcID, "Business Number") + "\r\n";
            strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
            strMsg += "Response Code : " + FDK_Module.Output(iProcID, "Response Code") + "\r\n";
            strMsg += "FS3 : " + FDK_Module.Output(iProcID, "FS3") + "\r\n";
            strMsg += "Card Data : " + FDK_Module.Output(iProcID, "Card Data") + "\r\n";
            strMsg += "FS4 : " + FDK_Module.Output(iProcID, "FS4") + "\r\n";
            strMsg += "Transaction Type : " + FDK_Module.Output(iProcID, "Transaction Type") + "\r\n";
            strMsg += "FS5 : " + FDK_Module.Output(iProcID, "FS5") + "\r\n";
            strMsg += "Authorization Date : " + FDK_Module.Output(iProcID, "Authorization Date") + "\r\n";
            strMsg += "FS6 : " + FDK_Module.Output(iProcID, "FS6") + "\r\n";
            strMsg += "Original Authorization Date : " + FDK_Module.Output(iProcID, "Original Authorization Date") + "\r\n";
            strMsg += "FS7 : " + FDK_Module.Output(iProcID, "FS7") + "\r\n";
            strMsg += "Transaction Amount : " + FDK_Module.Output(iProcID, "Transaction Amount") + "\r\n";
            strMsg += "FS8 : " + FDK_Module.Output(iProcID, "FS8") + "\r\n";
            strMsg += "Authorization Number : " + FDK_Module.Output(iProcID, "Authorization Number") + "\r\n";
            strMsg += "FS9 : " + FDK_Module.Output(iProcID, "FS9") + "\r\n";
            strMsg += "Issuer Code : " + FDK_Module.Output(iProcID, "Issuer Code") + "\r\n";
            strMsg += "FS10 : " + FDK_Module.Output(iProcID, "FS10") + "\r\n";
            strMsg += "Merchant Number : " + FDK_Module.Output(iProcID, "Merchant Number") + "\r\n";
            strMsg += "FS11 : " + FDK_Module.Output(iProcID, "FS11") + "\r\n";
            strMsg += "DDC Flag : " + FDK_Module.Output(iProcID, "DDC Flag") + "\r\n";
            strMsg += "FS12 : " + FDK_Module.Output(iProcID, "FS12") + "\r\n";
            strMsg += "Notice : " + FDK_Module.Output(iProcID, "Notice") + "\r\n";
            strMsg += "FS13 : " + FDK_Module.Output(iProcID, "FS13") + "\r\n";
            strMsg += "Acquirer Code : " + FDK_Module.Output(iProcID, "Acquirer Code") + "\r\n";
            strMsg += "FS14 : " + FDK_Module.Output(iProcID, "FS14") + "\r\n";
            strMsg += "Display : " + FDK_Module.Output(iProcID, "Display") + "\r\n";
            strMsg += "FS15 : " + FDK_Module.Output(iProcID, "FS15") + "\r\n";
            strMsg += "Point Customer Name : " + FDK_Module.Output(iProcID, "Point Customer Name") + "\r\n";
            strMsg += "FS16 : " + FDK_Module.Output(iProcID, "FS16") + "\r\n";
            strMsg += "Point Title Type : " + FDK_Module.Output(iProcID, "Point Title Type") + "\r\n";
            strMsg += "FS17 : " + FDK_Module.Output(iProcID, "FS17") + "\r\n";
            strMsg += "Point Add : " + FDK_Module.Output(iProcID, "Point Add") + "\r\n";
            strMsg += "FS18 : " + FDK_Module.Output(iProcID, "FS18") + "\r\n";
            strMsg += "Point Usable : " + FDK_Module.Output(iProcID, "Point Usable") + "\r\n";
            strMsg += "FS19 : " + FDK_Module.Output(iProcID, "FS19") + "\r\n";
            strMsg += "Point Save : " + FDK_Module.Output(iProcID, "Point Save") + "\r\n";
            strMsg += "FS20 : " + FDK_Module.Output(iProcID, "FS20") + "\r\n";
            strMsg += "Point Merchant : " + FDK_Module.Output(iProcID, "Point Merchant") + "\r\n";
            strMsg += "FS21 : " + FDK_Module.Output(iProcID, "FS21") + "\r\n";
            strMsg += "Notification : " + FDK_Module.Output(iProcID, "Notification") + "\r\n";
            strMsg += "FS22 : " + FDK_Module.Output(iProcID, "FS22") + "\r\n";
            strMsg += "Cash Receipt Authorization Number : " + FDK_Module.Output(iProcID, "Cash Receipt Authorization Number") + "\r\n";
            strMsg += "FS23 : " + FDK_Module.Output(iProcID, "FS23") + "\r\n";
            strMsg += "Cash Receipt Notice : " + FDK_Module.Output(iProcID, "Cash Receipt Notice") + "\r\n";
            strMsg += "FS24 : " + FDK_Module.Output(iProcID, "FS24") + "\r\n";
            strMsg += "CAT ID : " + FDK_Module.Output(iProcID, "CAT ID") + "\r\n";
            strMsg += "FS25 : " + FDK_Module.Output(iProcID, "FS25") + "\r\n";
            strMsg += "Issuer Name : " + FDK_Module.Output(iProcID, "Issuer Name") + "\r\n";
            strMsg += "FS26 : " + FDK_Module.Output(iProcID, "FS26") + "\r\n";
            strMsg += "Acquirer Name : " + FDK_Module.Output(iProcID, "Acquirer Name") + "\r\n";
            strMsg += "FS27 : " + FDK_Module.Output(iProcID, "FS27") + "\r\n";
            strMsg += "ETX : " + FDK_Module.Output(iProcID, "ETX") + "\r\n";
            strMsg += "LRC : " + FDK_Module.Output(iProcID, "LRC") + "\r\n";

            Log.e("cashAuth", strMsg);
            if(iRet == 0){
                /*
                TODO 매출전표 출력기능 필요

                 */
                payCashCompleted(FDK_Module.Output(iProcID, "Authorization Number"),FDK_Module.Output(iProcID, "Authorization Date"), FDK_Module.Output(iProcID, "Transaction Type"), Integer.parseInt(FDK_Module.Output(iProcID, "Transaction Amount")));
            }
        }

        iRet = FDK_Module.Destroy(iProcID);
    }

    public void receiptRequest(View button){
        if(currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)){

        }else if(currentVanType.equals(VanType.FIRSTDATA)){
            if("TRUE".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.IS_CONNECTED))){
                try {
                    cashReceiptAuth();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                DUtil.alertShow(TabletVanCashPaymentActivity.this, "CAT단말기가 연결되지 않았습니다.");
            }
        }else if(currentVanType.equals(VanType.SMARTRO)){
            if(mBound){
                Message msg = Message.obtain(null, CatManager.CASHPAY_RECEIPT_REQUEST, 0, 0);

                String contentString = "1c";
                String printString = "";

                try {
                    contentString += UnicodeFormatter.stringToHex("02");
                    contentString += "1c";
                    contentString += UnicodeFormatter.stringToHex(orderInfo.getInt("deliveryPrice")+"") + "1c1c1c"; // 금액
                    String smartroSelectType = "";
                    if("00".equals(currentType)){
                        smartroSelectType = "03";
                    }else if("01".equals(currentType)){
                        smartroSelectType = "12";
                    }else if("99".equals(currentType)){
                        smartroSelectType = "01";
                    }
                    contentString += UnicodeFormatter.stringToHex(smartroSelectType) + "1c"; // 현금영수증 소비자:0,,사업자:1,자진발급:2 / 신용카드번호:0,주민번호:1,사업자번호:2,핸드폰번호:3,보너스카드번호:4
                    contentString += "1c"; // 암호화 설정 : 단말기설정


                    contentString += getPrintString(pickupItemsV2);// 품목명
                    contentString += "1c";

                    contentString += UnicodeFormatter.stringToHex("2"); // 서명여부
                    contentString += "1c";

                    contentString += "1c";

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                byte[] content = new byte[contentString.length()/2];
                content = UnicodeFormatter.hexToByteArray(contentString);

                try{
                    Bundle b = new Bundle();
                    b.putByteArray("authReceipt", content); // for example
                    msg.setData(b);
                    mService.send(msg);
                }catch(RemoteException e){
                    e.printStackTrace();
                }
            }else{
                DUtil.alertShow(this, "PRINT가 연결되지 않았습니다. 연결상태를 확인해주세요.");
            }
        }

    }

    private String getPrintString(JSONArray items) throws JSONException, UnsupportedEncodingException {
        String printString = "";
        for(int i = 0 ; i < items.length(); i++){
            JSONObject item = items.getJSONObject(i);

            String isRepair = (item.getJSONArray("repair").length() > 0)?"수선":null;
            String isAdditional = (item.getJSONArray("additional").length() > 0)?"추가기술":null;
            String isPart = (item.getJSONArray("part").length() > 0)?"부속품":null;
            String isService = item.getString("isService");
            String isReLaundry = item.getString("isReLaundry");

            int sp = 0;
            if(item.isNull("sp")){
                sp = item.getInt("itemSP");
            }else{
                sp = item.getInt("sp");
            }

            String itemTitle = item.getString("itemTitle");
            itemTitle = itemTitle.replace("\n","");

            switch(sp){
                case 1:
                    itemTitle += "(일반)";
                    break;
                case 2:
                    itemTitle += "(명품)";
                    break;
                case 3:
                    itemTitle += "(아동)";
                    break;
            }

            if(isService != null){
                if("Y".equals(isService)){
                    itemTitle += " -무료서비스";
                }
            }
            if(isReLaundry != null){
                if("Y".equals(isReLaundry)){
                    itemTitle += " -재세탁";
                }
            }

            String itemName = MZValidator.getTagFormmater(items.getJSONObject(i).getInt("tagId"),4) + "    " + itemTitle;
            String itemPrice = MZValidator.toNumFormat(item.getInt("confirmPrice"));
            printString += UnicodeFormatter.stringJustifyToHex(itemName, itemPrice, 47) + "0A";
            printString += "0A";

            if(isRepair != null){
                for(int j = 0 ; j < item.getJSONArray("repair").length(); j++){
                    String repairPrice = MZValidator.toNumFormat(item.getJSONArray("repair").getJSONObject(j).getInt("additionalPrice"));
                    printString += UnicodeFormatter.stringJustifyToHex("         " + "+ 수선 :" + item.getJSONArray("repair").getJSONObject(j).getString("additionalItemTitle"), "("+repairPrice+")", 47) + "0A";
                }
            }
            if(isAdditional != null){
                for(int j = 0 ; j < item.getJSONArray("additional").length(); j++){
                    String addPrice = MZValidator.toNumFormat(item.getJSONArray("additional").getJSONObject(j).getInt("additionalPrice"));
                    printString += UnicodeFormatter.stringJustifyToHex("         " + "+ 기술료 :" + item.getJSONArray("additional").getJSONObject(j).getString("additionalItemTitle"), "("+addPrice+")", 47) + "0A";
                }
            }
            if(isPart != null){
                String parts = "";
                for(int j = 0 ; j < item.getJSONArray("part").length(); j++){
                    if(j == 0){
                        parts += item.getJSONArray("part").getJSONObject(j).getString("partTitle");
                    }else{
                        parts += ", " + item.getJSONArray("part").getJSONObject(j).getString("partTitle");
                    }
                }
                printString += UnicodeFormatter.stringToHex("         " + "+ 부속품 :" + parts, "euc-kr") + "0A";
            }

            if(item.getJSONArray("repair").length() + item.getJSONArray("additional").length() + item.getJSONArray("part").length() > 0){
                printString += "0A";
            }

        }

        printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr")+"0A";
        printString += "13"+UnicodeFormatter.stringJustifyToHex("총 수량 :",items.length()+"개", 47) +  "140A";
        printString += "13"+UnicodeFormatter.stringJustifyToHex("총 금액 :",MZValidator.toNumFormat(orderInfo.getInt("deliveryPrice")) + "원", 47) +  "140A";
        printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr")+"0A";

        return printString;
    }

    public void closeClicked(View button){
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

}
