package me.rewhite.delivery.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.common.logic.Constants;


public class AboutActivity extends BaseActivity {

    AQuery aq;
    Context ctx = this;

    String FacebookPageId = "293162404141109";
    String wwwAddress = "http://www.rewhite.me";
    String BlogAddress = "http://blog.rewhite.me";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
        aq.id(R.id.btn_www).clicked(this, "buttonClicked").tag(1);
        aq.id(R.id.btn_blog).clicked(this, "buttonClicked").tag(2);
        aq.id(R.id.btn_facebook).clicked(this, "buttonClicked").tag(3);

        aq.id(R.id.text_term).clicked(this, "termsClicked");
        aq.id(R.id.text_opensource).clicked(this, "opensourceClicked");

    }

    public void closeClicked(View button) {
        finish();
    }

    public Intent getOpenFacebookIntent(Context context) {

        try {
            context.getPackageManager().getPackageInfo("com.facebook.katana", 0);
            return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/" + FacebookPageId));
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/rewhite.page"));
        }
    }

    public void buttonClicked(View button) {
        int tag = (int) button.getTag();
        switch (tag) {
            case 1:
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(wwwAddress));
                startActivity(i);
                break;
            case 2:
                Intent j = new Intent(Intent.ACTION_VIEW, Uri.parse(BlogAddress));
                startActivity(j);
                break;
            case 3:
                startActivity(getOpenFacebookIntent(this));
                break;
            default:
                break;
        }
    }

    public void termsClicked(View button) {
        Intent termsIntent = new Intent().setClass(AboutActivity.this, TermsActivity.class);
        termsIntent.putExtra("section", 0);
        termsIntent.putExtra("TITLE_NAME", "약관보기");
        startActivity(termsIntent);
    }

    public void opensourceClicked(View button) {
        Intent intent = new Intent().setClass(AboutActivity.this, TermsDetailView.class);
        intent.putExtra("TERMS_TITLE", "오픈소스 라이센스");
        intent.putExtra("TERMS_URI", Constants.TERMS_OSS);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
