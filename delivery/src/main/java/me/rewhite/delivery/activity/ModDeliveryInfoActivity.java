package me.rewhite.delivery.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.fragment.DeliveryModFragment;
import me.rewhite.delivery.fragment.IFragment;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.TimeUtil;

public class ModDeliveryInfoActivity extends BaseActivity {

    private final static String TAG = ModDeliveryInfoActivity.class.getSimpleName();
    public static final String FRAGMENT_DELIVERY = "order_delivery-mod";

    public final Context mCtx = this;
    AQuery aq;
    public JSONObject storeInfo;
    public long deliveryReqTime = 0;
    public long pickupReqTime = 0;
    public String storeClosingDay;
    public String storeDayoff;
    public String deliveryRequestMessage;
    public int washingDays = 3;// 배달일기준 : 수거일기준 3일후
    public int synchronizedLimitTime = 30;


    private Fragment mContent;

    private boolean mIsTouchDisable; // 연속 터치로 생기는 이슈 막기 위함
    public JSONObject orderInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_delivery_info);

        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        aq.id(R.id.title_text).typeface(CommonUtility.getNanumBarunTypeface());

        Intent intent = getIntent();
        if (intent != null) {
            try {
                String info = intent.getStringExtra("INFO");
                orderInfo = new JSONObject(info);
                deliveryReqTime = orderInfo.getLong("deliveryRequestTimeApp");
                pickupReqTime = orderInfo.getLong("pickupRequestTimeApp");
                //washingDays = orderInfo.getInt("washingDays");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        initilize();
    }

    public void setTitle(String _title) {
        aq.id(R.id.title_text).text(_title);
    }

    public void closeClicked(View button) {
        onBackPressed();
    }

    private void initilize() {

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);

        NetworkClient.post(Constants.SHOW_STORE_INFO, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SHOW_STORE_INFO, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.SHOW_STORE_INFO, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            storeInfo = jsondata.getJSONObject("data");
                            storeClosingDay = storeInfo.getString("storeClosingDay");
                            storeDayoff = storeInfo.getString("storeDayoff");
                            washingDays = storeInfo.getInt("washingDays");
                            showFragment(FRAGMENT_DELIVERY);
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    public void showFragment(String fragmentName) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);
        showFragment(fragmentName, null);
    }

    public void showFragment(String fragmentName, Bundle args) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(fragmentName);

        fragment = new DeliveryModFragment();
        aq.id(R.id.title_text).text("배송일 변경");

        Log.e(TAG, "[DeliveryModFragment] Create : " + fragmentName);

        if (null != args) {
            fragment.setArguments(args);
        }
        ft.setCustomAnimations(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        ft.replace(R.id.content_frame, fragment, fragmentName);
        ft.addToBackStack(fragmentName);
        ft.commitAllowingStateLoss();
        //ft.commit();
    }

    public boolean getTouchDisable() {
        Log.d(TAG, "[getTouchDisable]");
        return mIsTouchDisable;
    }

    public void setTouchDisable(boolean isTouchDisable) {
        Log.d(TAG, "[setTouchDisable]" + isTouchDisable);
        mIsTouchDisable = isTouchDisable;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        DUtil.Log(TAG, "[onBackPressed] Back Stack Entry Count : " + fm.getBackStackEntryCount());

        if (fm.getBackStackEntryCount() >= 1) {

            String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
            IFragment fragment = (IFragment) fm.findFragmentByTag(tag);
            if (false == fragment.onBackPressed()) {

                if (fm.getBackStackEntryCount() == 1) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                    alertDialogBuilder.setCancelable(true).setMessage("배송일 변경신청이 완료되지 않았어요.\n중단하시겠어요?")
                            .setPositiveButton("네", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {
                    setTouchDisable(true);
                    //clearCaptureView();

                    fm.popBackStack();

                }
            }
        } else {
            super.onBackPressed();
        }
        //finish();
        //super.onBackPressed();
    }

    public void modInfoExcute(RequestParams params){
        NetworkClient.post(Constants.ORDER_MOD, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_MOD, error.getMessage());


            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_MOD, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            //JSONArray orderInfo = jsondata.getJSONArray("data");

                            Intent resultData = new Intent();
                            //resultData.putExtra("content", orderInfo.toString());
                            setResult(Activity.RESULT_OK, resultData);
                            finish();
                        }

                    } else {
                        Toast.makeText(mCtx, jsondata.getString("message"), Toast.LENGTH_SHORT);
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    public RequestParams modParams;
    public void modInfo() {
        modParams = new RequestParams();
        try {
            modParams.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            modParams.put("orderId", orderInfo.getString("orderId"));
            modParams.put("deliveryRequestTime", deliveryReqTime);
            Log.i("modInfo time", TimeUtil.convertTimestampToString(deliveryReqTime) + "");
            modParams.put("deliveryRequestMessage", deliveryRequestMessage);
            modParams.put("k", 1);
            Log.i("modInfo", modParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(deliveryRequestMessage == null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
            alertDialogBuilder.setCancelable(true).setMessage("변경사유를 입력해주세요.")
                    .setPositiveButton("네", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else{
            if("3".equals(deliveryRequestMessage)){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                alertDialogBuilder.setCancelable(true).setMessage("고객과 통화하셔서 배송일시를 다시 조정하셨나요?")
                        .setPositiveButton("네", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                modInfoExcute(modParams);
                            }
                        }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }else{
                modInfoExcute(modParams);
            }
        }



    }
}
