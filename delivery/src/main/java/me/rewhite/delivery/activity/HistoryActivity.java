package me.rewhite.delivery.activity;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import me.rewhite.delivery.R;
import me.rewhite.delivery.fragment.IFragment;
import me.rewhite.delivery.fragment.OrderDetailFragment;

public class HistoryActivity extends AppCompatActivity implements IFragment.OnFragmentInteractionListener {

    private String orderId;
    private String orderStatus;
    private String userId;

    private Fragment mContent;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }

        if (getIntent() != null) {
            orderId = getIntent().getStringExtra("orderId");
            orderStatus = getIntent().getStringExtra("orderStatus");
            userId = getIntent().getStringExtra("userId");
        }

        Bundle bundle = new Bundle();
        bundle.putString("orderId", orderId);
        bundle.putString("orderStatus", orderStatus);
        bundle.putString("userId", userId);

        mContent = new OrderDetailFragment().newInstance(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, mContent).commit();
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
