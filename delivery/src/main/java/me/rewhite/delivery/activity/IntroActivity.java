package me.rewhite.delivery.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.androidquery.AQuery;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.splunk.mint.Mint;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;
import io.fabric.sdk.android.Fabric;
import me.rewhite.delivery.Bootstrap;
import me.rewhite.delivery.Bootstrap.TrackerName;
import me.rewhite.delivery.QuickstartPreferences;
import me.rewhite.delivery.R;
import me.rewhite.delivery.RegistrationIntentService;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.logic.RewhiteCMD;
import me.rewhite.delivery.common.util.Logger;
import me.rewhite.delivery.fragment.CustomDialogFragment;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.session.AccessToken;
import me.rewhite.delivery.session.RewhiteException;
import me.rewhite.delivery.session.Session;
import me.rewhite.delivery.session.SessionCallback;
import me.rewhite.delivery.util.CapacityUtil;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZCrypto;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.RewhiteServerInterface;
import me.rewhite.delivery.util.RewhiteServerInterface.RewhiteServiceInterfaceListener;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.SharedPreferencesUtility.Command;
import me.rewhite.delivery.util.StringUtil;
import me.rewhite.delivery.util.SystemUtil;

import static me.rewhite.delivery.util.CommonUtility.SENDER_ID;

//import com.android.vending.billing.IInAppBillingService;


public class IntroActivity extends FragmentActivity implements CustomDialogFragment.CustomDialogListener, RewhiteServiceInterfaceListener {

    private static final String TAG = "IntroActivity";

    private final static String EXTRA_KEY = "me.rewhite.delivery";
    private final static String EXTRA_SHOTCUT = "Rewhite Shortcut";

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private AQuery aq;
    private Context mContext;
    View decorView;

    private RewhiteServerInterface mServerInterface;
    // Push Message Receiver 정의
    //private BroadcastReceiver mHandleMessageReceiver;
    GoogleCloudMessaging mGcm;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;

    private final SessionCallback mySessionCallback = new RewhiteShopPlusSessionStatusCallback();
    private Session session;
//	IInAppBillingService mService;
//
//	ServiceConnection mServiceConn = new ServiceConnection() {
//
//		@Override
//		public void onServiceDisconnected(ComponentName name) {
//			mService = null;
//		}
//
//		@Override
//		public void onServiceConnected(ComponentName name, IBinder service) {
//			mService = IInAppBillingService.Stub.asInterface(service);
//		}
//	};


    @Override
    protected void onDestroy() {

        session.removeCallback(mySessionCallback);
//		if (mProgressDialog != null){
//			mProgressDialog.dismiss();
//		}

//        if (mService != null) {
//            unbindService(mServiceConn);
//        }

        super.onDestroy();


    }

//	// [START on_start_add_listener]
//	@Override
//	public void onStart() {
//		super.onStart();
//		if (mAuthListener != null) {
//			mAuth.addAuthStateListener(mAuthListener);
//		}
//	}
//	// [END on_start_add_listener]
//
//	// [START on_stop_remove_listener]
//	@Override
//	public void onStop() {
//		super.onStop();
//		if (mAuthListener != null) {
//			mAuth.removeAuthStateListener(mAuthListener);
//		}
//	}
//	// [END on_stop_remove_listener]

    protected void onResume() {
        super.onResume();
        DUtil.Log(TAG, "onResume");

        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = manager.getActiveNetworkInfo();


        if (networkInfo != null && networkInfo.isConnected()) {
            if (checkPlayServices()) {
                if (Session.getCurrentSession() != null) {
                    if (Session.getCurrentSession().isOpened()) {
                        IntroActivity.this.onSessionOpened();
                    } else {
                        initialize();
                    }
                } else {
                    initialize();
                }
            }
        } else {
            if (Session.getCurrentSession() != null) {
                if (Session.getCurrentSession().isOpened()) {
                    IntroActivity.this.onSessionOpened();
                } else {
                    initialize();
                }
            } else {
                initialize();
            }
        }

    }
//
//	// [START declare_auth]
//	private FirebaseAuth mAuth;
//	// [END declare_auth]
//
//	// [START declare_auth_listener]
//	private FirebaseAuth.AuthStateListener mAuthListener;

    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //로그 활성화
        Logger.setAllLogEnable(getApplicationContext(), getResources().getBoolean(R.bool.log_enable));

        Fabric.with(this, new Crashlytics());

        if (CommonUtility.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.tablet_activity_intro);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_intro);
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR);//.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.navigationBarColor));
        }

        decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_LAYOUT_FLAGS);

        aq = new AQuery(this);

//		// [START initialize_auth]
//		mAuth = FirebaseAuth.getInstance();
//		// [END initialize_auth]
//
//		// [START auth_state_listener]
//		mAuthListener = new FirebaseAuth.AuthStateListener() {
//			@Override
//			public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//				FirebaseUser user = firebaseAuth.getCurrentUser();
//				if (user != null) {
//					// User is signed in
//					Log.w(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
//				} else {
//					// User is signed out
//					Log.w(TAG, "onAuthStateChanged:signed_out");
//				}
//				// [START_EXCLUDE]
//				// [END_EXCLUDE]
//			}
//		};
//		// [END auth_state_listener]

        Animation anim = AnimationUtils.loadAnimation(this, R.anim.riseup);
        aq.id(R.id.logo_image).animate(anim);

        mServerInterface = new RewhiteServerInterface(this);
        mServerInterface.setInterface(this);

//		Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
//		serviceIntent.setPackage("com.android.vending");
//		bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);

        DUtil.Log(TAG, "onCreate");

        mContext = this;
        session = Session.getInstance(mContext);
        session.addCallback(mySessionCallback);

        // DEBUG
        //SharedPreferencesUtility.set(Command.IS_TUTORIAL, "true");

        CommonUtility.loadTypeface(this);

//        GCMRegistrar.checkDevice(mContext);
//        GCMRegistrar.checkManifest(mContext);
//
//        mHandleMessageReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
//                Log.i("GCM onReceive", newMessage);
//            }
//        };
//        registerReceiver(mHandleMessageReceiver, new IntentFilter(
//                DISPLAY_MESSAGE_ACTION));
        // Bugsense(Mint Express) Initialize

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    //mInformationTextView.setText(getString(R.string.gcm_send_message));
                } else {
                    // mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };
        // Registering BroadcastReceiver
        registerReceiver();

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);

            mGcm = GoogleCloudMessaging.getInstance(this);
            getGCMID();
        }

        uplusInitialize();


        Tracker t = ((Bootstrap) getApplication()).getTracker(TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        //t.enableAdvertisingIdCollection(true);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        //SharedPreferencesUtility.set(Command.REG_NUMBER, userNumber);

        /*
         * 앱실행시 Local Storage(SQLite) 점검 (강제삭제의 경우 대비)
         */
        // 계정정보DB 생성여부 체크
        boolean memo2Result = isCheckDB(this, Constants.DB_NAME);

        DUtil.Log("SafePayment DB Check", "DB2 Check=" + memo2Result);
        if (!memo2Result) { // DB가 없으면 복사
            DUtil.Log("MEMO DB Check", "MEMO DB NO EXIST");
            copyDB(this, Constants.DB_NAME);
        }


    }

    private void uplusInitialize() {
        //if(!"".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.PARTNER.UPLUS_ID))){

        RequestParams params = new RequestParams();
        params.put("id", "07051219815");
        params.put("pass", MZCrypto.makeSha512Key("rewhite12!"));

        String callbackUrl = Constants.UPLUS_070_CALLBACK + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_ID);
        params.put("callbackurl", callbackUrl);
        params.put("callbackhost", "52.79.49.158");
        params.put("callbackport", 80);

        Log.e("uplusInitialize", params.toString());

        NetworkClient.postExt(Constants.SET_UPLUS_070_CALLBACK, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SET_UPLUS_070_CALLBACK, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.SET_UPLUS_070_CALLBACK, result);

                    //JSONObject jsondata = new JSONObject(result);

                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
        //}
    }

    private long splashDelay = 1000;
    boolean isStopNeed = false;

    private void checkExcuted() {

        if (!"true".equals(SharedPreferencesUtility.get(Command.IS_GETSTARTED))) {
            TimerTask task = new TimerTask() {

                @Override
                public void run() {
                    if (!isStopNeed) {
                        DUtil.Log("GetStartedActivity start", "GetStartedActivity MAKE");

                        if (CommonUtility.isTablet(mContext)) {
                            Intent tutorialIntent = new Intent(IntroActivity.this, GetStartedTabletActivity.class);
                            tutorialIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                            // tutorialIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            // // Task안남기도록 실행
                            startActivity(tutorialIntent);
                            finish();
                        } else {
                            Intent tutorialIntent = new Intent(IntroActivity.this, GetStartedActivity.class);
                            tutorialIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
                            // tutorialIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            // // Task안남기도록 실행
                            startActivity(tutorialIntent);
                            finish();
                        }

                    }
                }
            };

            Timer timer = new Timer();
            timer.schedule(task, splashDelay);
        } else {
//            if (session == null) {
//                session = Session.getInstance(mContext);
//            }
            // 바탕화면 바로가기 생성
            String accessToken = AccessToken.createFromCache().getAccessTokenString();
            DUtil.Log(TAG, "ACCESS_TOKEN : " + accessToken);

            if (StringUtil.isNullOrEmpty(accessToken)) {
                IntroActivity.this.onSessionClosed();
                //session.addCallback(mySessionCallback);
            } else {
                IntroActivity.this.onSessionOpened();
            }

        }
    }

    private void appVersionCheck() {
        RequestParams params = new RequestParams();
        params.add("k", "1");

        NetworkClient.post(Constants.VERSION_CHECK, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.VERSION_CHECK, error.getMessage());

                String accessToken = AccessToken.createFromCache().getAccessTokenString();
                DUtil.Log(TAG, "ACCESS_TOKEN : " + accessToken);

                if (StringUtil.isNullOrEmpty(accessToken)) {
                    IntroActivity.this.onSessionClosed();
                } else {
                    IntroActivity.this.onSessionOpened();
                }
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    JSONObject json = new JSONObject(result);

                    if ("S0000".equals(json.getString("resultCode"))) {
                        String version = json.getJSONObject("data").getString("ad");

                        int myVerNum = 0;
                        PackageManager m = mContext.getPackageManager();

                        // 설치된 패키지의 버전이름 추출 : 1.0.0 스타일
                        String app_ver = m.getPackageInfo(mContext.getPackageName(), 0).versionName;
                        Log.i("appver", version + " / " + app_ver);

                        // 구분자 '.'를 기반으로 5자리 Integer값으로 변환.
                        // 각 Middle, Minor 버전값은 99를 넘지 못한다.
                        myVerNum = MZValidator.convertVersionStringToInteger(app_ver);

                        // 추출된 버전값을 Preferences값에 저장한다.
                        //SharedPreferencesUtility.set(cmd, value);

                        // 서버에서 최신앱 버전정보를 조회하여 5자리 값으로 변환.
                        int currentVerNum = MZValidator.convertVersionStringToInteger(version);
                        Log.i("AppVersionNum", currentVerNum + "/" + myVerNum);

                        boolean isStrict = false;

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(IntroActivity.this);
                        alertDialogBuilder.setCancelable(false).setMessage(getResources().getString(R.string.appupdate_minor))
                                .setPositiveButton("업데이트", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setData(Uri.parse("market://details?id=" + DUtil.getAppPackageName(mContext)));
                                        startActivity(intent);
                                        finish();
                                    }
                                });

                        if (currentVerNum - myVerNum <= 0) {
                            checkExcuted();
                        } else {
                            if (currentVerNum - myVerNum >= 1000) {
                                // 강제업데이트
                                alertDialogBuilder.setCancelable(false);
                            } else {
                                alertDialogBuilder.setCancelable(true).setNegativeButton("확인", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // 설치된 버전값이 서버값보다 작을 경우 update알림 :
                                        // Minor업데이트의 경우 필수업데이트가 아님.
                                        checkExcuted();
                                    }
                                });
                            }

                            if (!IntroActivity.this.isFinishing()) {
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }

                        }


                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (NameNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Mint.logException(e);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });

    }


    private void getGCMID() {
        String gcmId = "";

//        final String regId = GCMRegistrar.getRegistrationId(this);
//        if (StringUtil.isNullOrEmpty(regId)) {
//            // Automatically registers application on startup.
//            GCMRegistrar.register(this, SENDER_ID);
//        } else {
//            // Device is already registered on GCM, check server.
//            if (GCMRegistrar.isRegisteredOnServer(this)) {
//                // Skips registration.
//                Log.i("GCM", "GCM already_registered" + "\n");
//                Log.e("GCM ID", regId);
//            } else {
//                Log.e("GCM ID", regId);
//            }
//            SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.GCM_ID, regId);
//        }

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg;
                try {
                    if (mGcm == null) {
                        mGcm = GoogleCloudMessaging.getInstance(getApplicationContext());

                    }
                    msg = mGcm.register(SENDER_ID);
                } catch (IOException e) {
                    msg = "";
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                if (!msg.equals("")) {
                    Log.e("GCM ID onPostExecute", msg);
                    SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.GCM_ID, msg);
                }
            }
        }.execute(null, null, null);

    }

    private void initialize() {

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("deviceToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));
        params.put("deviceInformation", CapacityUtil.SHOW_MEMORYSIZE() + "\n\n" + SystemUtil.getSystemInfoString(this));
        params.put("k", 1);
        Log.e("ReauthrizeToken", params.toString());
        NetworkClient.post(Constants.AUTH_ACCESS_TOKEN, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.AUTH_ACCESS_TOKEN + "(failed)", error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.AUTH_ACCESS_TOKEN, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        //getGCMID();
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                appVersionCheck();
                            }
                        }, 1000);
                    } else {
                        SharedPreferencesUtility.set(Command.IS_AUTO_LOGIN, "false");
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN, "");
                        //getGCMID();
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                appVersionCheck();
                            }
                        }, 1000);
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        try {
            if (Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
                return;
            }
        } catch (IllegalStateException e) {
            Session.initialize(this);
            return;
        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    private class RewhiteShopPlusSessionStatusCallback implements SessionCallback {

        /**
         * 세션이 오픈되었으면 가입페이지로 이동 한다.
         */
        @Override
        public void onSessionOpened() {
            // 뺑글이 종료

            // 프로그레스바를 보이고 있었다면 중지하고 세션 오픈후 보일 페이지로 이동
            IntroActivity.this.onSessionOpened();
            DUtil.Log(TAG, "onSessionOpened");
        }

        @Override
        public void onSessionOpening() {
            // 뺑글이 시작
        }

        /**
         * 세션이 삭제되었으니 로그인 화면이 보여야 한다.
         *
         * @param exception 에러가 발생하여 close가 된 경우 해당 exception
         */
        @Override
        public void onSessionClosed(RewhiteException exception) {
            // TODO Auto-generated method stub
            IntroActivity.this.onSessionClosed();
            DUtil.Log(TAG, "onSessionClosed = " + exception.getLocalizedMessage());
        }
    }

    protected void onSessionClosed() {

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mContext, SignActivity.class);
                mContext.startActivity(intent);
                ((Activity) mContext).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                ((Activity) mContext).finish();
            }
        }, 1000);

    }

    protected void onSessionOpened() {

        if (!"true".equals(SharedPreferencesUtility.get(Command.IS_AUTO_LOGIN))) {
            //AccessToken.clearAccessTokenFromCache();
            session.close();
            //onSessionClosed();
        } else {

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    Log.e("refreshUserInfo", "==================== onSessionOpened ==================== ");
                    Session.getCurrentSession().refreshUserInfo();

                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    if (CommonUtility.isTablet(mContext)) {
                        // 이전 태블릿버전으로 빌드
                        intent.setClass(mContext, TabletMainActivity.class);

                        // ㄱㅐ발중
                        //intent.setClass(mContext, TabletMainV2Activity.class);
                    } else {
                        intent.setClass(mContext, MainActivity.class);
                    }
                    mContext.startActivity(intent);
                    ((Activity) mContext).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    ((Activity) mContext).finish();
                }
            }, 500);
        }


    }

    private void registerReceiver() {
        if (!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }

    // google play service가 사용가능한가
    private boolean checkPlayServices() {

        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    // DB가 있나 체크하기
    public boolean isCheckDB(Context mContext, String _dbname) {

        String filePath = this.getApplicationContext().getFilesDir() + Constants.PACKAGE_NAME + "/databases/" + _dbname;
        File file = new File(filePath);

        return file.exists();

    }

    // DB를 복사하기
    // assets의 /db/xxxx.db 파일을 설치된 프로그램의 내부 DB공간으로 복사하기
    public void copyDB(Context mContext, String _dbname) {
        DUtil.Log("MiniApp", "copyDB");
        AssetManager manager = this.getApplicationContext().getAssets();
        String folderPath = this.getApplicationContext().getFilesDir() + Constants.PACKAGE_NAME + "/databases";
        String filePath = this.getApplicationContext().getFilesDir() + Constants.PACKAGE_NAME + "/databases/" + _dbname;
        File folder = new File(folderPath);
        File file = new File(filePath);

        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            InputStream is = manager.open(_dbname);
            BufferedInputStream bis = new BufferedInputStream(is);

            if (folder.exists()) {
            } else {
                folder.mkdirs();
            }

            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }

            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            int read = -1;
            byte[] buffer = new byte[1024];
            while ((read = bis.read(buffer, 0, 1024)) != -1) {
                bos.write(buffer, 0, read);
            }

            bos.flush();

            bos.close();
            fos.close();
            bis.close();
            is.close();

        } catch (IOException e) {
            DUtil.Log("ErrorMessage : ", e.getMessage());
        }

    }

    @Override
    public void onResult(String command, String result) {
        Log.d(TAG, "[onResult] : command : " + command + " , result : " + result);

        if (command.equals(RewhiteCMD.VERSION)) {
            if (result == null) {
                // TODO : 서버 정기 정검 팝업
                // 네트워크 상태 체크 해서 네트워크가 끊어져 있는 상태가 아니라면 팝업 띄움.

            } else {
                JSONObject o = null;
                try {
                    o = new JSONObject(result);
                    if (o.getBoolean(RewhiteCMD.JSON_RESULT)) {
                        String version = o.getString(RewhiteCMD.JSON_VERSION);
                        Log.d(TAG, "get version : " + version + ", app ver : " + CommonUtility.getAppVersion(this));

                        String[] versionArr = version.split(".");
                        String[] aVersionArr = CommonUtility.getAppVersion(this).split(".");

                        boolean isUpdate = false;
                        for (int i = 0; i < aVersionArr.length; i++) {
                            int sVersion = Integer.valueOf(versionArr[i]);
                            int aVersion = Integer.valueOf(aVersionArr[i]);

                            if (aVersion < sVersion) {
                                isUpdate = true;
                                break;
                            }
                        }

                        if (isUpdate) {
                            CustomDialogFragment dialog = CustomDialogFragment.newInstance(CustomDialogFragment.TYPE_UPDATE_APP, null);
                            dialog.show(getSupportFragmentManager(), String.valueOf(CustomDialogFragment.TYPE_UPDATE_APP));
                        } else {
                            // 다음 단계로
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, int mode, String param) {
        if (mode == CustomDialogFragment.TYPE_UPDATE_APP) {
            // 앱 종료
            finish();
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog, int mode, String param) {
        if (mode == CustomDialogFragment.TYPE_UPDATE_APP) {
            // 업데이트
            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            marketLaunch.setData(Uri.parse("market://details?id=me.rewhite.delivery"));
            startActivity(marketLaunch);
            finish();
        }
    }
}
