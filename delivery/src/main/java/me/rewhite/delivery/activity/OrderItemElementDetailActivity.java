package me.rewhite.delivery.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.task.ImageTagUploadTask;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class OrderItemElementDetailActivity extends BaseActivity {

    AQuery aq;
    private final static String TAG = OrderItemElementDetailActivity.class.getSimpleName();

    public final Context mCtx = this;
    private Fragment mContent;

    private boolean mIsTouchDisable; // 연속 터치로 생기는 이슈 막기 위함
    private boolean mFlagFinish;
    private Handler mHandler;
    private Runnable mTask;
    private String currentFragname;

    public String colorPickValue = "";
    public JSONArray pickedRepairArray;
    public JSONArray pickedAdditionalArray;
    public JSONArray pickedPartsArray;
    public int pickedPrice = 0;

    public JSONArray repairArray;
    public JSONArray additionalArray;
    public JSONArray partsArray;
    public JSONArray colorsArray;
    public JSONArray priceArray;

    public int tagNo;
    public int tagLength;
    public int bucketIndex = 0;
    public String orderStatus = "";

    public JSONArray pickupItems;
    //public int pickupItemsPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_item_element_detail_v2);

        Log.e(TAG, "orderItemElementDetailActivity");
        aq = new AQuery(this);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                if (intent.getStringExtra("data") != null) {

                }

                tagNo = intent.getIntExtra("initTagNo", 0);
                tagLength = intent.getIntExtra("initTagLength", 0);
                bucketIndex = intent.getIntExtra("bucketIndex", 0);
                orderStatus = intent.getStringExtra("orderStatus");

                if(intent.getStringExtra("pickupItems") != null){
                    pickupItems = new JSONArray(intent.getStringExtra("pickupItems"));
                    bucketIndex = intent.getIntExtra("pickupItemsPosition", 0);

                    aq.id(R.id.tag_label).text(getTagFormmater(pickupItems.getJSONObject(bucketIndex).getInt("tagId"),tagLength));
                    aq.id(R.id.title_text).text(pickupItems.getJSONObject(bucketIndex).getString("itemTitle"));

                    pickedRepairArray = pickupItems.getJSONObject(bucketIndex).getJSONArray("repair");
                    pickedPartsArray= pickupItems.getJSONObject(bucketIndex).getJSONArray("part");
                    pickedAdditionalArray = pickupItems.getJSONObject(bucketIndex).getJSONArray("additional");
                    colorPickValue = pickupItems.getJSONObject(bucketIndex).getString("itemColor");
                    imagePickValue = pickupItems.getJSONObject(bucketIndex).getJSONArray("photo");

                    /*
                    Step1Index = pickupItems.getJSONObject(bucketIndex).getInt("step1index");
                    Step1Name = priceArray.getJSONObject(Step1Index).getString("itemTitle");
                    Step2Index = pickupItems.getJSONObject(bucketIndex).getInt("step2index");
                    Step2Name = priceArray.getJSONObject(Step1Index).getJSONArray("items").getJSONObject(Step2Index).getString("itemTitle");*/
                }else{
                    pickedRepairArray = new JSONArray();
                    pickedAdditionalArray = new JSONArray();
                    pickedPartsArray = new JSONArray();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        mHandler = new Handler();
        mTask = new Runnable() {

            @Override
            public void run() {
                mFlagFinish = false;
            }
        };

        try {
            initilize();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        aq.id(R.id.pui_detail_btn_add_pics).clicked(this, "picsAddAction");
        aq.id(R.id.btn_back).clicked(this, "closeClicked");
        aq.id(R.id.btn_ok).clicked(this, "closeClicked");
    }

    public void picsAddAction(View button){
        if(imagePickValue != null){
            if(imagePickValue.length() < 3){
                try{
                    if ( Build.VERSION.SDK_INT >= 23){
                        if (!canAccessStorage()) {
                            requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                        }else{
                            addIntentCall();
                        }
                    }else{
                        addIntentCall();
                    }
                }
                catch(NullPointerException ex){
                    addIntentCall();
                }
            }else{
                DUtil.alertShow(this, "사진 3장만 첨부 가능합니다.\n" +
                        "등록하신 사진 삭제 후에 \n" +
                        "다시 추가해주세요.");
            }
        }else{
            try{
                if ( Build.VERSION.SDK_INT >= 23){
                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    }else{
                        addIntentCall();
                    }
                }else{
                    addIntentCall();
                }
            }
            catch(NullPointerException ex){
                addIntentCall();
            }
        }

    }

    private String getTagFormmater(int tagno, int taglength){
        String pre = "";
        String after = "";
        if(taglength == 4){
            pre = tagno/1000 + "";
            if(tagno%1000 >= 100){
                after = pre + "-"+ tagno%1000 + "";
            }else if(tagno%1000 >= 10){
                after = pre + "-"+ "0" + tagno%1000;
            }else{
                after = pre + "-"+ "00" + tagno%1000;
            }

        }else{
            if(tagno >= 1000){
                after = tagno + "";
            }else if(tagno >= 100){
                after = "0" + tagno;
            }else if(tagno >= 10){
                after = "00" + tagno;
            }else{
                after = "000" + tagno;
            }
        }

        return after;
    }

    public void closeClicked(View button) {
        if(isModfied){
            Intent resultData = new Intent();
            setResult(Activity.RESULT_OK, resultData);
            finish();
        }else{
            super.onBackPressed();
        }
    }

    int sp = 0;
    int totalPrice = 0;

    public void initilize() throws JSONException {
        int step1selectindex = 0;
        JSONObject pickedCurrentItems = pickupItems.getJSONObject(bucketIndex);
        sp = pickedCurrentItems.getInt("itemSP");
        totalPrice = pickedCurrentItems.getInt("confirmPrice");
        //aq.id(R.id.icon_step1).image(R.drawable.pui_circle);
        aq.id(R.id.total_price_label).text(MZValidator.toNumFormat(totalPrice));

        // Step1
        if(pickedCurrentItems.getJSONArray("repair").length() > 0){
            if(sp != 4){
                aq.id(R.id.step1_label).text("세탁+수선");
                // 세탁수선
                aq.id(R.id.clean_type_area).visible();
                aq.id(R.id.element_must_repair).visible();
                aq.id(R.id.price_type_area).visible();
                step1selectindex = 3;
            }else{
                aq.id(R.id.step1_label).text("수선");
                // 수선
                aq.id(R.id.element_must_repair).visible();
                aq.id(R.id.clean_type_area).gone();
                aq.id(R.id.price_type_area).visible();
                step1selectindex = 2;
            }
        }else{
            aq.id(R.id.step1_label).text("세탁");
            // 세탁
            aq.id(R.id.clean_type_area).visible();
            aq.id(R.id.element_must_repair).gone();
            aq.id(R.id.price_type_area).visible();
            step1selectindex = 1;
        }

        if(step1selectindex == 1 || step1selectindex == 3){
            aq.id(R.id.step2_label).text(pickedCurrentItems.getString("laundry"));
            // Step2
        }

        // Step3
        switch(sp){
            case 1:
                //calculatedPrice = itemData.getInt("storePrice1");
                aq.id(R.id.step3_label).text("일반");
                break;
            case 2:
                //calculatedPrice = itemData.getInt("storePrice2");
                aq.id(R.id.step3_label).text("명품");
                break;
            case 3:
                aq.id(R.id.step3_label).text("아동");
                //calculatedPrice = itemData.getInt("storePrice3");
                break;
            case 4:
                // 수선만
                if(pickedCurrentItems.getJSONArray("repair").length() > 0){
                    int repairsp = pickedCurrentItems.getJSONArray("repair").getJSONObject(0).getInt("additionalItemSP");
                    switch(repairsp){
                        case 1:
                            //calculatedPrice = itemData.getInt("storePrice1");
                            aq.id(R.id.step3_label).text("일반");
                            break;
                        case 2:
                            //calculatedPrice = itemData.getInt("storePrice2");
                            aq.id(R.id.step3_label).text("명품");
                            break;
                        case 3:
                            aq.id(R.id.step3_label).text("아동");
                            //calculatedPrice = itemData.getInt("storePrice3");
                            break;
                        default:
                            break;
                    }
                }

                break;
            default:
                break;
        }

        // 수선내역
        Log.e("repairPickIndex", step1selectindex + "");
        if(step1selectindex == 2 || step1selectindex == 3){
            aq.id(R.id.repair_element_label_area).visible();
            JSONArray repairValue = pickedCurrentItems.getJSONArray("repair");
            String addAppendString = "";
            int repairPrices = 0;
            try {
                for(int i =0 ; i < repairValue.length(); i++){
                    if(i == 0){
                        addAppendString = repairValue.getJSONObject(i).getString("additionalItemTitle");
                    }else{
                        addAppendString = addAppendString + ", " + repairValue.getJSONObject(i).getString("additionalItemTitle");
                    }
                    repairPrices += repairValue.getJSONObject(i).getInt("additionalPrice");

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("repairPickValue", addAppendString + " / " + repairPrices);

            aq.id(R.id.pui_detail_repair_element_label).text(addAppendString);
            aq.id(R.id.pui_repair_price).text(MZValidator.toNumFormat(repairPrices));

            //aq.id(R.id.pui_detail_btn_step4_01).getButton().setText("추가 / 변경하기");
        }
        // Step3

        aq.id(R.id.sep_line).visible();
        aq.id(R.id.option_area).visible();
        aq.id(R.id.total_price_area).visible();
        aq.id(R.id.blank_area).visible();

        if("N".equals(pickedCurrentItems.getString("isService")) && "N".equals(pickedCurrentItems.getString("isReLaundry"))){
            aq.id(R.id.etc_area).gone();
        }else{
            aq.id(R.id.etc_area).visible();
            if("Y".equals(pickedCurrentItems.getString("isService"))){
                aq.id(R.id.label_etc).text("무료서비스");
            }else if("Y".equals(pickedCurrentItems.getString("isReLaundry"))){
                aq.id(R.id.label_etc).text("재세탁");
            }
        }

        // Additional
        if(pickedAdditionalArray.length() > 0){
            aq.id(R.id.additional_area).visible();
            aq.id(R.id.additional_element_label_area).visible();
            String addAppendString = "";
            int addPrices = 0;
            try {
                for(int i =0 ; i < pickedAdditionalArray.length(); i++){
                    if(i == 0){
                        addAppendString = pickedAdditionalArray.getJSONObject(i).getString("additionalItemTitle");
                    }else{
                        addAppendString = addAppendString + ", " + pickedAdditionalArray.getJSONObject(i).getString("additionalItemTitle");
                    }
                    addPrices += pickedAdditionalArray.getJSONObject(i).getInt("additionalPrice");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            aq.id(R.id.pui_additional_element).text(addAppendString);
            aq.id(R.id.pui_additional_price).text(MZValidator.toNumFormat(addPrices));

        }else{
            aq.id(R.id.additional_area).gone();
        }

        // part
        if(pickedPartsArray.length() > 0){
            aq.id(R.id.parts_area).visible();
            aq.id(R.id.options_element_label_area).visible();
            String partsAppendString = "";
            try {
                for(int i =0 ; i < pickedPartsArray.length(); i++){
                    if(i == 0){
                        partsAppendString = pickedPartsArray.getJSONObject(i).getString("partTitle");
                    }else{
                        partsAppendString = partsAppendString + ", " + pickedPartsArray.getJSONObject(i).getString("partTitle");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("partsPickValue", partsAppendString);
            aq.id(R.id.pui_element_options).text(partsAppendString);
        }else{
            aq.id(R.id.parts_area).gone();
        }

        // Colorpick
        if(!"".equals(pickedCurrentItems.getString("itemColor"))){
            aq.id(R.id.itemcolor_area).visible();
            aq.id(R.id.before_color_area).gone();
            aq.id(R.id.after_color_area).visible();

            //aq.id(R.id.label_color).text(pickedCurrentItems.getString("itemColor"));
            int resId = getResources().getIdentifier("pui_order_element_color_" + pickedCurrentItems.getString("itemColor"), "mipmap", "me.rewhite.delivery");
            aq.id(R.id.pui_btn_color_picked).image(resId);
        }else{
            aq.id(R.id.itemcolor_area).gone();
        }

        // Photo
        int orderStts = Integer.parseInt(orderStatus);
        if(imagePickValue.length() > 0){
            aq.id(R.id.pui_pics_options_area).visible();

            for(int i = 0; i < 3; i++){
                int removeId = getResources().getIdentifier("btn_remove_0"+(i+1), "id", "me.rewhite.delivery");
                ImageButton btn = (ImageButton)findViewById(removeId);
                aq.id(btn).gone();

                int resId = getResources().getIdentifier("pics_0"+(i+1), "id", "me.rewhite.delivery");
                ImageView iv = aq.id(resId).getImageView();
                aq.id(iv).image(R.mipmap.pui_image_background);
            }

            for(int i =0; i < imagePickValue.length(); i++){
                String url = "";
                if(imagePickValue.getJSONObject(i).isNull("photoUrl")){
                    url = imagePickValue.getJSONObject(i).getString("fileUrl");
                }else{
                    url = imagePickValue.getJSONObject(i).getString("photoUrl");
                }
                int resId = getResources().getIdentifier("pics_0"+(i+1), "id", "me.rewhite.delivery");
                ImageView iv = aq.id(resId).getImageView();
                aq.id(iv).image(url);

                int resRemoveId = getResources().getIdentifier("btn_remove_0"+(i+1), "id", "me.rewhite.delivery");
                ImageButton btn = (ImageButton)findViewById(resRemoveId);
                aq.id(btn).image(R.mipmap.pui_btn_image_delete_normal);
                if(url != "" && url != null){
                    aq.id(btn).visible();
                }
            }
            aq.id(R.id.btn_remove_01).clicked(this, "removeImage").tag(1);
            aq.id(R.id.btn_remove_02).clicked(this, "removeImage").tag(2);
            aq.id(R.id.btn_remove_03).clicked(this, "removeImage").tag(3);

        }else{
            //aq.id(R.id.photo_area).gone();
            if(orderStts < 20){

            }else{
                //aq.id(R.id.pui_pics_options_area).gone();
                aq.id(R.id.photo_area).gone();
            }
        }

        // 주문상태별 사진추가기능 비활성화 처리
        if(orderStts == 11){

        }else{
            aq.id(R.id.btn_remove_01).gone();
            aq.id(R.id.btn_remove_02).gone();
            aq.id(R.id.btn_remove_03).gone();
        }
        if(orderStts < 20){

        }else{
            aq.id(R.id.pui_detail_btn_add_pics).gone();
        }

    }


    public static final String[] INITIAL_PERMS={
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    public static final String[] STORAGE_PERMS={
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    public static final int INITIAL_REQUEST=1337;

    public static final int STORAGE_REQUEST=INITIAL_REQUEST+6;

    private static final int REQ_CODE_PICK_IMAGE = 0;
    private static final int TAKE_CAMERA = 1;					// 카메라 리턴 코드값 설정
    private static final int TAKE_GALLERY = 2;				// 앨범선택에 대한 리턴 코드값 설정
    private static final String TEMP_PHOTO_FILE = "upload.jpg"; // 임시 저장파일
    public JSONArray imagePickValue;

    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch(requestCode) {

            case STORAGE_REQUEST:
                if (canAccessStorage()) {
                    addIntentCall();
                }
                else {
                    Toast.makeText(this, "사진첩에 접근할수 있는 권한이 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public boolean canAccessStorage() {
        return(hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    public static boolean isIntentAvailable( Context context, String action){
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent( action);
        List<ResolveInfo> list = packageManager.queryIntentActivities( intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }



    public void addIntentCall(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra( MediaStore.EXTRA_OUTPUT, getTempUri() );
        // requestCode지정해서 인텐트 실행
        startActivityForResult(intent, REQ_CODE_PICK_IMAGE);

        /*

        final CharSequence[] items = {"카메라로 직접 촬영", "사진첩에서 불러오기"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);     // 여기서 this는 Activity의 this

        // 여기서 부터는 알림창의 속성 설정
        builder.setTitle("색상을 선택하세요")        // 제목 설정
                .setItems(items, new DialogInterface.OnClickListener(){    // 목록 클릭시 설정
                    public void onClick(DialogInterface dialog, int index){
                        if(index == 0){
                            // Camera
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            intent.putExtra( MediaStore.EXTRA_OUTPUT, getTempUri() );
                            // requestCode지정해서 인텐트 실행
                            startActivityForResult(intent, REQ_CODE_PICK_IMAGE);
                        }else if(index == 1){
                            // Album
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.setType("image/*");
                            startActivityForResult(intent, TAKE_GALLERY);
                        }
                        Toast.makeText(getApplicationContext(), items[index], Toast.LENGTH_SHORT).show();
                    }
                });

        AlertDialog dialog = builder.create();    // 알림창 객체 생성
        dialog.show();    // 알림창 띄우기
*/
    }

    private static final int PICK_COLOR = 11;
    private static final int PICK_REPAIR = 12;
    private static final int PICK_PARTS = 13;
    private static final int PICK_ADDITIONAL = 14;
    private static final int PICK_PRICE = 15;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case REQ_CODE_PICK_IMAGE:

                        File f = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE);
                        String filePath = f.getAbsolutePath();

                        // Get dimensions of image first (takes very little time)
                        BitmapFactory.Options bfo = new BitmapFactory.Options();
                        bfo.inJustDecodeBounds = true;
                        bfo.inDither = false;
                        bfo.inPreferredConfig = Bitmap.Config.RGB_565;
                        BitmapFactory.decodeFile(filePath, bfo);

                        // Calculate sample size to keep image under maxFileSize
                        int maxFileSize = 2572864; // in bytes
                        int sampleSize = 1;
                        long fileSize = 2 * (bfo.outWidth / sampleSize) * (bfo.outHeight / sampleSize);
                        while (fileSize > maxFileSize) {
                            sampleSize++;
                            fileSize = 2 * (bfo.outWidth / sampleSize) * (bfo.outHeight / sampleSize);
                        }
                        // Decode image using calculated sample size
                        bfo.inSampleSize = sampleSize;
                        bfo.inJustDecodeBounds = false;
                        Bitmap bmpAvator = BitmapFactory.decodeFile(filePath, bfo);
                        /* Rotation */
                        int degrees = GetExifOrientation(filePath);
                        // 회전한 이미지 취득
                        bmpAvator = GetRotatedBitmap(bmpAvator, degrees);
                        File fileCacheItem = new File(filePath);
                        OutputStream out = null;
                        try {
                            fileCacheItem.createNewFile();
                            out = new FileOutputStream(fileCacheItem);
                            bmpAvator.compress(Bitmap.CompressFormat.JPEG, 90, out);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                out.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        HashMap<String, Object> reqParams = new HashMap<String, Object>();
                        new ImageTagUploadTask(this, preferences).execute(reqParams);


                    break;
                case TAKE_GALLERY:
                    if( data != null )
                    {
                        Log.e("Test", "result = " + data);

                        Uri thumbnail = data.getData();
                        if( thumbnail != null )
                        {
                            //ImageView Imageview = (ImageView) findViewById(R.id.imgview);
                            //Imageview.setImageURI(thumbnail);


                        }
                    }
                    break;
            }
        }
    }

    SharedPreferences preferences;


    public boolean getTouchDisable() {
        Log.d(TAG, "[getTouchDisable]");
        return mIsTouchDisable;
    }

    public void setTouchDisable(boolean isTouchDisable) {
        Log.d(TAG, "[setTouchDisable]" + isTouchDisable);
        mIsTouchDisable = isTouchDisable;
    }

    boolean isModfied = false;
    @Override
    public void onBackPressed() {
        if(isModfied){
            Intent resultData = new Intent();
            setResult(Activity.RESULT_OK, resultData);
            finish();
        }else{
            super.onBackPressed();
        }
    }

    private int GetExifOrientation(String filepath) {
        int degree = 0;
        ExifInterface exif = null;

        try {
            exif = new ExifInterface(filepath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (exif != null) {
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            if (orientation != -1) {
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        degree = 90;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        degree = 180;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        degree = 270;
                        break;
                }
            }
        }

        return degree;
    }

    private Bitmap GetRotatedBitmap(Bitmap bitmap, int degrees) {
        if (degrees != 0 && bitmap != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

            try {
                Bitmap b2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);

                if (bitmap != b2) {
                    bitmap.recycle();
                    bitmap = b2;
                }
            } catch (OutOfMemoryError e) {
                // 메모리 부족에러시, 원본을 반환
            }
        }

        return bitmap;
    }

    public void uploadCompleted(String response) {

        try {
            JSONObject json = new JSONObject(response);
            Log.i("uploadCompleted", response.toString());

            if ("S0000".equals(json.getString("resultCode"))) {
                DUtil.alertShow(this, "이미지 업로드가 완료되었습니다");
                String imgurl = json.getString("data");
                isModfied = true;
                setImagePicsValue(imgurl);
                //
            } else {
                DUtil.alertShow(this, "이미지 업로드가 실패했습니다.");
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void submitImage() throws JSONException {
        JSONObject pickedCurrentItems = pickupItems.getJSONObject(bucketIndex);
        int orderId = pickedCurrentItems.getInt("orderId");
        int itemId = pickedCurrentItems.getInt("itemId");
        int tagId = pickedCurrentItems.getInt("tagId");

        JSONObject submitObj = new JSONObject();

        submitObj.put("itemId", itemId);
        submitObj.put("tagId", tagId);
        submitObj.put("photo", imagePickValue);

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("items", submitObj.toString());
        params.put("k", 1);

        DUtil.Log("imagePickValue param", imagePickValue.toString());

        NetworkClient.post(Constants.TAG_ADD_PICTURES, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.TAG_ADD_PICTURES, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.TAG_ADD_PICTURES, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //
                            //Toast.makeText(getApplicationContext(), "접수를 완료했습니다.", Toast.LENGTH_LONG).show();
                        } else {
                            //JSONArray locationInfo = jsondata.getJSONArray("data");
                            //Toast.makeText(getApplicationContext(), "접수를 완료했습니다.", Toast.LENGTH_LONG).show();
                            //finish();
                        }
                        isModfied = true;
                        reloadImageContainer();
                    } else {
                        Toast.makeText(getApplicationContext(), jsondata.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private JSONArray removePosition(int position, JSONArray data) throws JSONException {
        JSONArray temp = new JSONArray();
        for(int i = 0; i < data.length(); i++){
            if(position != i){
                temp.put(data.getJSONObject(i));
            }
        }
        return temp;
    }

    public void removeImage(View button){
        final int tag = (int)button.getTag();

        // TAG_REMOVE_PICTURES
        JSONObject pickedCurrentItems = null;
        RequestParams params = new RequestParams();

        try {
            pickedCurrentItems = pickupItems.getJSONObject(bucketIndex);
            int orderId = pickedCurrentItems.getInt("orderId");
            int itemId = pickedCurrentItems.getInt("itemId");
            int tagId = pickedCurrentItems.getInt("tagId");
            String url = "";
            if(imagePickValue.getJSONObject((tag-1)).isNull("photoUrl")){
                url = imagePickValue.getJSONObject((tag-1)).getString("fileUrl");
            }else{
                url = imagePickValue.getJSONObject((tag-1)).getString("photoUrl");
            }

            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            params.put("orderId", orderId);
            params.put("itemId", itemId);
            params.put("tagId", tagId);
            params.put("fileUrl", url);
            params.put("k", 1);

            NetworkClient.post(Constants.TAG_REMOVE_PICTURES, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                    // TODO Auto-generated method stub
                    DUtil.Log(Constants.TAG_REMOVE_PICTURES, error.getMessage());

                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                    String result;
                    try {
                        result = new String(data, "UTF-8");
                        DUtil.Log(Constants.TAG_REMOVE_PICTURES, result);

                        JSONObject jsondata = new JSONObject(result);

                        if ("S0000".equals(jsondata.getString("resultCode"))) {

                            switch (tag){
                                case 1:
                                    if(imagePickValue.length() > 0){
                                        imagePickValue = removePosition(0, imagePickValue);
                                    }
                                    break;
                                case 2:
                                    if(imagePickValue.length() > 1){
                                        imagePickValue = removePosition(1, imagePickValue);
                                    }
                                    break;
                                case 3:
                                    if(imagePickValue.length() > 2){
                                        imagePickValue = removePosition(2, imagePickValue);
                                    }
                                    break;
                            }

                            Log.e("removePics imagePickValue", imagePickValue.toString() + "");

                            if(imagePickValue.length() == 0){
                                aq.id(R.id.pui_pics_options_area).gone();
                            }else{
                                aq.id(R.id.pui_pics_options_area).visible();
                                aq.id(R.id.pics_01).image(R.mipmap.pui_image_background);
                                aq.id(R.id.pics_02).image(R.mipmap.pui_image_background);
                                aq.id(R.id.pics_03).image(R.mipmap.pui_image_background);

                                aq.id(R.id.btn_remove_01).image(R.mipmap.pui_btn_image_delete_disabled);
                                aq.id(R.id.btn_remove_02).image(R.mipmap.pui_btn_image_delete_disabled);
                                aq.id(R.id.btn_remove_03).image(R.mipmap.pui_btn_image_delete_disabled);

                                for(int i =0; i < imagePickValue.length(); i++){
                                    String url = "";
                                    if(imagePickValue.getJSONObject(i).isNull("photoUrl")){
                                        url = imagePickValue.getJSONObject(i).getString("fileUrl");
                                    }else{
                                        url = imagePickValue.getJSONObject(i).getString("photoUrl");
                                    }
                                    int resId = getResources().getIdentifier("pics_0"+(i+1), "id", "me.rewhite.delivery");
                                    ImageView iv = aq.id(resId).getImageView();
                                    aq.id(iv).image(url);

                                    int resRemoveId = getResources().getIdentifier("btn_remove_0"+(i+1), "id", "me.rewhite.delivery");
                                    ImageButton btn = (ImageButton)findViewById(resRemoveId);
                                    aq.id(btn).image(R.mipmap.pui_btn_image_delete_normal);
                                }
                            }

                            submitImage();
                            //reloadImageContainer();
                        } else {
                            Toast.makeText(getApplicationContext(), jsondata.getString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (UnsupportedEncodingException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void setImagePicsValue(String _value){

        try {
            JSONObject obj = new JSONObject();
            obj.put("photoUrl", _value);

            if(imagePickValue == null){
                imagePickValue = new JSONArray();
            }
            imagePickValue.put(obj);


            reloadImageContainer();
            submitImage();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void reloadImageContainer() throws JSONException {
        aq.id(R.id.pui_pics_options_area).visible();

        for(int i = 0; i < 3; i++){
            int removeId = getResources().getIdentifier("btn_remove_0"+(i+1), "id", "me.rewhite.delivery");
            ImageButton btn = (ImageButton)findViewById(removeId);
            aq.id(btn).gone();

            int resId = getResources().getIdentifier("pics_0"+(i+1), "id", "me.rewhite.delivery");
            ImageView iv = aq.id(resId).getImageView();
            aq.id(iv).image(R.mipmap.pui_image_background);
        }


        for(int i =0; i < imagePickValue.length(); i++){
            String url = "";
            if(imagePickValue.getJSONObject(i).isNull("photoUrl")){
                url = imagePickValue.getJSONObject(i).getString("fileUrl");
            }else{
                url = imagePickValue.getJSONObject(i).getString("photoUrl");
            }
            int resId = getResources().getIdentifier("pics_0"+(i+1), "id", "me.rewhite.delivery");
            ImageView iv = aq.id(resId).getImageView();
            aq.id(iv).image(url);

            int resRemoveId = getResources().getIdentifier("btn_remove_0"+(i+1), "id", "me.rewhite.delivery");
            ImageButton btn = (ImageButton)findViewById(resRemoveId);
            aq.id(btn).image(R.mipmap.pui_btn_image_delete_normal);
            if(url != "" && url != null){
                aq.id(btn).visible();
            }
        }
        aq.id(R.id.btn_remove_01).clicked(this, "removeImage").tag(1);
        aq.id(R.id.btn_remove_02).clicked(this, "removeImage").tag(2);
        aq.id(R.id.btn_remove_03).clicked(this, "removeImage").tag(3);
    }

    void createExternalStoragePublicPicture() {

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);

        try {
            // Make sure the Pictures directory exists.
            path.mkdirs();

            InputStream is = getAssets().open("kakao_default_profile_image.png");// getResources().openRawResource(R.mipmap.kakao_default_profile_image);
            OutputStream os = new FileOutputStream(file);
            byte[] data = new byte[is.available()];
            is.read(data);
            os.write(data);
            is.close();
            os.close();

            MediaScannerConnection.scanFile(this, new String[]{file.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });
        } catch (IOException e) {
            // Unable to create file, likely because external storage is
            // not currently mounted.
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }

    void deleteExternalStoragePublicPicture() {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        file.delete();
    }

    boolean hasExternalStoragePublicPicture() {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        return file.exists();
    }

    /**
     * 임시 저장 파일의 경로를 반환
     */
    private Uri getTempUri() {
        //Uri mImageCaptureUri = Uri.fromFile(new File(this.getExternalCacheDir(), TEMP_PHOTO_FILE));
        File file = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE);
        return Uri.fromFile(getTempFile());
    }

    /**
     * 외장메모리에 임시 이미지 파일을 생성하여 그 파일의 경로를 반환
     */
    private File getTempFile() {
        if (hasExternalStoragePublicPicture()) {
            deleteExternalStoragePublicPicture();
        }
        createExternalStoragePublicPicture();
/*
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File f = new File(path, TEMP_PHOTO_FILE);
*/
        File f = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE);

        try {
            f.createNewFile(); // 외장메모리에 temp.jpg 파일 생성
        } catch (IOException e) {
        }

        return f;
    }

}
