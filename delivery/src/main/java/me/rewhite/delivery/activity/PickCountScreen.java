package me.rewhite.delivery.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.util.CommonUtility;

public class PickCountScreen extends BaseActivity {

    private AQuery aq;
    int totalCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        /*if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }*/
        setContentView(R.layout.content_pick_count_screen);

        Intent intent = getIntent();
        if (intent != null) {
            totalCount = intent.getIntExtra("pickCount", 0);
        }


        aq = new AQuery(this);

        aq.id(R.id.btn_add).clicked(this, "addAction");
        aq.id(R.id.btn_remove).clicked(this, "removeAction");

        aq.id(R.id.text_count).text(totalCount + " 개").typeface(CommonUtility.getNanumBarunTypeface());

        aq.id(R.id.btn_ok).clicked(this, "completeAction");
    }

    public void addAction(View button){
        totalCount++;
        aq.id(R.id.text_count).text(totalCount + " 개");
    }

    public void removeAction(View button){
        totalCount--;
        if(totalCount <= 0){
            totalCount = 0;
        }
        aq.id(R.id.text_count).text(totalCount + " 개");
    }

    public void completeAction(View button){
        if(totalCount == 0){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setCancelable(true).setMessage("수량을 1개이상 입력해주세요.")
                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }

                    }).setNegativeButton("품목등록 취소", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent resultData = new Intent();
                    setResult(Activity.RESULT_CANCELED, resultData);
                    finish();
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else{
            Intent resultData = new Intent();
            resultData.putExtra("result", totalCount);
            setResult(Activity.RESULT_OK, resultData);
            finish();
        }

    }

}
