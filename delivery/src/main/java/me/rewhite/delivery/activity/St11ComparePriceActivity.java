package me.rewhite.delivery.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.ToastUtility;

public class St11ComparePriceActivity extends AppCompatActivity {

    JSONArray pickupItemsV2;
    JSONObject parentData;
    AQuery aq;
    Context ctx = this;
    int finalPrice = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_st11_compare_price);

        aq = new AQuery(this);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                if (intent.getStringExtra("pickupItems") != null) {
                    pickupItemsV2 = new JSONArray(intent.getStringExtra("pickupItems"));
                    parentData = new JSONObject(intent.getStringExtra("orderData"));

                    aq.id(R.id.text_store_name).text(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_NAME));
                    aq.id(R.id.text_nickname).text(parentData.getString("userName"));

                    // 11번가 선결제주문 정보
                    if(parentData.isNull("pickupItemsPartnerV2")){
                        aq.id(R.id.text_11st_count).text("0개");
                    }else{
                        if(!parentData.isNull("pickupItemsPartnerV2")){
                            JSONArray pickupItemsPartnerV2Array = parentData.getJSONArray("pickupItemsPartnerV2");
                            JSONArray partner11stPayment;
                            if(!parentData.isNull("payments")){
                                partner11stPayment = parentData.getJSONArray("payments");
                            }else{
                                partner11stPayment = new JSONArray();
                            }

                            int prepaid = 0;
                            int preExtraCharge = 0;

//                            for(int i = 0 ; i < partner11stPayment.length(); i++){
//                                if("P51".equals(partner11stPayment.getJSONObject(i).getString("paymentType"))){
//                                    prepaid = partner11stPayment.getJSONObject(i).getInt("paymentPrice");
//                                    aq.id(R.id.text_11st_price).text(MZValidator.toNumFormat(prepaid) + " 원");
//                                }
//                            }


                            int orderQuantity = 0;
                            int orderTotalQuantity = 0;
                            int addSeq = 0;
                            String pickupItemProductNames = "";
                            for(int i = 0 ; i < pickupItemsPartnerV2Array.length(); i++){
                                orderQuantity = pickupItemsPartnerV2Array.getJSONObject(i).getInt("pOrderQuantity");
                                orderTotalQuantity += pickupItemsPartnerV2Array.getJSONObject(i).getInt("pOrderQuantity");
                                if(addSeq != 0){
                                    pickupItemProductNames += ", ";
                                }
                                String pOptionName = pickupItemsPartnerV2Array.getJSONObject(i).getString("pProductOptionName");
                                pOptionName = pOptionName.replace("옵션명 1:","");
                                pOptionName = pOptionName.split(" - ")[0];

                                Log.e("prepaid", pickupItemsPartnerV2Array.getJSONObject(i).getInt("pOrderAmount")
                                        +"/"+orderQuantity + "/"+pOptionName);
                                prepaid += pickupItemsPartnerV2Array.getJSONObject(i).getInt("pOrderAmount");
                                // pSellerPrice , 할인되는 경우 참조값은 무엇? (pOrderAmount, pOrderPaymentPrice, pSellerDiscountPrice)
                                pickupItemProductNames += pOptionName + " " + pickupItemsPartnerV2Array.getJSONObject(i).getString("pOrderQuantity") + "개";
                                addSeq++;
                            }

                            aq.id(R.id.text_11st_price).text(MZValidator.toNumFormat(prepaid) + " 원");
                            //aq.id(R.id.text_11st_tax).text(MZValidator.toNumFormat(preExtraCharge) + " 원");

                            Log.e("store prd", pickupItemsV2.toString());
                            int storePrice = 0;
                            String storePrdName = "";
                            int addSeq2 = 0;
                            for(int i = 0; i < pickupItemsV2.length(); i++){
                                storePrice += pickupItemsV2.getJSONObject(i).getInt("confirmPrice");
                                if(addSeq2 != 0){
                                    storePrdName += ", ";
                                }
                                storePrdName += pickupItemsV2.getJSONObject(i).getString("itemTitle");
                                addSeq2++;
                            }


                            aq.id(R.id.text_store_count).text(pickupItemsV2.length() + "개");
                            aq.id(R.id.text_store_product).text(storePrdName);


                            aq.id(R.id.text_final_store_price).text(MZValidator.toNumFormat(storePrice) + " 원");

                            if(storePrice < 20000){
                                storePrice = storePrice + 2000;
                                aq.id(R.id.text_delivery_price).text("2,000 원");
                            }else{
                                aq.id(R.id.text_delivery_price).text("무료배송");
                            }
                            // 옵션선택가격 추가
                            aq.id(R.id.text_delivery_price).text(aq.id(R.id.text_delivery_price).getText().toString());
                            if(preExtraCharge > 0){
                                aq.id(R.id.text_delivery_label).text("배송비");
                            }
                            storePrice += preExtraCharge;

                            aq.id(R.id.text_store_price).text(MZValidator.toNumFormat(storePrice) + " 원");

                            aq.id(R.id.text_11st_count).text(orderTotalQuantity + "개");
                            aq.id(R.id.text_11st_product).text(pickupItemProductNames);

                            aq.id(R.id.paid_image).gone();

                            finalPrice = storePrice - prepaid - preExtraCharge;

                            Log.e("finalPrice", finalPrice + "/" + storePrice + " / " + prepaid);

                            if(finalPrice > 0){
                                // 추가금액 발생
                                aq.id(R.id.st11_payment_info).visible();
                                aq.id(R.id.st11_paid_price).text(MZValidator.toNumFormat(prepaid) + " 원");

                                aq.id(R.id.st11_reqpay_text).text("미결제 금액");
                                aq.id(R.id.st11_reqpay_price).text(MZValidator.toNumFormat(finalPrice) + " 원");

                                aq.id(R.id.st11_add_price_info).visible();
                                aq.id(R.id.text_total_bottom).text("추가요금  "+ MZValidator.toNumFormat(finalPrice) +"원이 발생합니다.");
                            }else if(finalPrice == 0){
                                // 금액 동일
                                aq.id(R.id.st11_payment_info).gone();
                                aq.id(R.id.st11_paid_price).text(MZValidator.toNumFormat(prepaid) + " 원");
                                aq.id(R.id.st11_reqpay_price).text(MZValidator.toNumFormat(finalPrice) + " 원");
                                aq.id(R.id.st11_add_price_info).gone();
                                aq.id(R.id.paid_image).visible();
                                aq.id(R.id.text_total_bottom).text("추가요금  "+ MZValidator.toNumFormat(finalPrice) +"원이 발생합니다.");

                                Intent resultData = new Intent();
                                setResult(Activity.RESULT_OK, resultData);
                                finish();
                            }else{
                                // 환불금액 발생
                                aq.id(R.id.st11_payment_info).visible();
                                aq.id(R.id.st11_paid_price).text(MZValidator.toNumFormat(prepaid) + " 원 (+" + preExtraCharge + "원)");
                                aq.id(R.id.st11_reqpay_text).text("환불예정 금액");
                                aq.id(R.id.st11_reqpay_price).text(MZValidator.toNumFormat(prepaid+preExtraCharge - storePrice) + " 원");

                                aq.id(R.id.st11_add_price_info).visible();
                                aq.id(R.id.text_total_bottom).text("환불 예정금액 "+MZValidator.toNumFormat(prepaid+preExtraCharge - storePrice)+"원이 있어서 \n" +
                                        "리화이트에서 연락드릴 예정입니다.");
                            }

                        }else{
                            //aq.id(R.id.st11_preinput_layout).gone();
                            ToastUtility.show(ctx, "11번가 주문데이터가 비정상입니다.", Toast.LENGTH_LONG);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        aq.id(R.id.btn_submit).clicked(this, "completeAction");
        aq.id(R.id.btn_call).clicked(this, "callAction");

    }

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int INITIAL_REQUEST = 1337;

    public void callAction(View button){
        if ( Build.VERSION.SDK_INT >= 23){
            if (!canAccessCall()) {
                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
            }else{
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setCancelable(true).setMessage("추가요금  "+ MZValidator.toNumFormat(finalPrice) +"원이 발생합니다.\n"+"고객이 선택한 품목 또는 요금이 상이하니 세탁진행전에 꼭 통화 해주세요.\n\n품목등록 취소를 누르시면 처음부터 다시 입력해야합니다.")
                        .setPositiveButton("고객에게 전화걸기", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                //callIntent.setAction("android.intent.action.DIAL");
                                try {
                                    callIntent.setData(Uri.parse("tel:" + parentData.getString("userPhone")));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                if(callIntent.resolveActivity(getPackageManager()) != null){
                                    startActivity(callIntent);
                                }

                                Intent resultData = new Intent();
                                setResult(Activity.RESULT_OK, resultData);
                                finish();
                            }

                        }).setNegativeButton("품목등록 취소", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent resultData = new Intent();
                        setResult(Activity.RESULT_CANCELED, resultData);
                        finish();
                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        }else{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setCancelable(true).setMessage("추가요금  "+ MZValidator.toNumFormat(finalPrice) +"원이 발생합니다.\n"+"고객이 선택한 품목 또는 요금이 상이하니 세탁진행전에 꼭 통화 해주세요.\n\n품목등록 취소를 누르시면 처음부터 다시 입력해야합니다.")
                    .setPositiveButton("고객에게 전화걸기", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            //callIntent.setAction("android.intent.action.DIAL");
                            try {
                                callIntent.setData(Uri.parse("tel:" + parentData.getString("userPhone")));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if(callIntent.resolveActivity(getPackageManager()) != null){
                                startActivity(callIntent);
                            }

                            Intent resultData = new Intent();
                            setResult(Activity.RESULT_OK, resultData);
                            finish();
                        }

                    }).setNegativeButton("품목등록 취소", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent resultData = new Intent();
                    setResult(Activity.RESULT_CANCELED, resultData);
                    finish();
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    public void completeAction(View button){

        // 미수금결제해야할 금액이 있을경우 전화안내로 버튼 스왑
        if(finalPrice > 0){
            aq.id(R.id.btn_submit).gone();
            aq.id(R.id.btn_call).visible();
        }else{
            Intent resultData = new Intent();
            setResult(Activity.RESULT_OK, resultData);
            finish();
        }
    }


    @Override
    public void onBackPressed() {
        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();

    }


    private boolean canAccessCall() {
        return(hasPermission(Manifest.permission.CALL_PHONE));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case INITIAL_REQUEST:
                if (canAccessCall()) {

                }
                else {
                    Toast.makeText(this, "핸드폰번호를 알수 없습니다.", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }

}
