package me.rewhite.delivery.activity;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.WindowManager;
import android.widget.CompoundButton;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.custom.MySwitch;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.StringUtil;
import me.rewhite.delivery.util.TimeUtil;

public class AlarmActivity extends BaseActivity implements MySwitch.OnChangeAttemptListener, CompoundButton.OnCheckedChangeListener {

    public static final String TAG = AlarmActivity.class.getSimpleName();
    MySwitch slideToUnLock;
    AssetFileDescriptor afd;
    MediaPlayer audio_play;
    AQuery aq;
    String oType;
    String orderId;
    Context ctx = this;

    JSONObject parentData;
    Vibrator m_vibrator = null;
    KeyguardManager km = null;
    KeyguardManager.KeyguardLock keyLock = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        km = (KeyguardManager)getSystemService(KEYGUARD_SERVICE);
        keyLock = km.newKeyguardLock(KEYGUARD_SERVICE);
        disableKeyguard();

        aq = new AQuery(this);
        slideToUnLock = (MySwitch)findViewById(R.id.switch3);
        m_vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        if (slideToUnLock != null) {
            slideToUnLock.toggle();
            //slideToUnLock.fixate(true);
            slideToUnLock.setOnCheckedChangeListener(this);
        }
        //slideToUnLock.disableClick();

        Intent intent = getIntent();
        if (intent != null) {
            oType = intent.getStringExtra("deliveryType");
            orderId = intent.getStringExtra("orderId");
            getRemoteData(orderId);
        }

        if("P".equals(oType)){
            aq.id(R.id.text_title).text("수거알림");
            aq.id(R.id.title_desc).text("30분후에 수거 주문이 있습니다.");
        }else if("C".equals(oType)){
            aq.id(R.id.text_title).text("주문이 취소되었습니다.");
            aq.id(R.id.title_desc).text("");
        }else{
            aq.id(R.id.text_title).text("배송알림");
            aq.id(R.id.title_desc).text("30분후에 배송 주문이 있습니다.");
        }


        try {
            afd = getAssets().openFd("sound/sound.mp3");
            audio_play = new MediaPlayer();
            audio_play.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            audio_play.prepare();
            audio_play.start();

            long[] pattern = {50, 100, 100, 200, 100, 300};
            m_vibrator.vibrate(pattern, 0);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void reenableKeyguard() {
        keyLock.reenableKeyguard();
    }

    public void disableKeyguard() {
        keyLock.disableKeyguard();
    }

    private void getRemoteData(String _orderId) {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_DETAIL, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        parentData = jsondata.getJSONObject("data");

                        String userName = parentData.getString("userName");
                        aq.id(R.id.profile_name).text(userName).typeface(CommonUtility.getNanumBarunTypeface());
                        String addressName = parentData.getString("address1");
                        String addressDetailName = parentData.getString("address2");
                        String newAddressName = addressName + " " + addressDetailName;
                        if (!StringUtil.isNullOrEmpty(newAddressName)) {
                            aq.id(R.id.text_address).text(newAddressName).typeface(CommonUtility.getNanumBarunTypeface());
                        }

                        if("P".equals(oType)){
                            String dps = displayTimeString(parentData.getLong("pickupRequestTimeApp"));
                            aq.id(R.id.time_content).text(dps).typeface(CommonUtility.getNanumBarunTypeface());
                        }else{
                            String dps = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
                            aq.id(R.id.time_content).text(dps).typeface(CommonUtility.getNanumBarunTypeface());
                        }

                    } else {

                    }

                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private String displayTimeString(long time) {
        //Date dt = new Date();
        //dt.setTime(time);
        if (SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP) == null) {
            SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.DELIVERY_GAP, "1");
        }
        return TimeUtil.getDateByMDSH(time, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP));
    }

    @Override
    public void onChangeAttempted(boolean isChecked) {
        Log.d(TAG, "onChangeAttemped(checked = " + isChecked + ")");
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.d(TAG, "onChechedChanged(checked = " + isChecked + ")");
        if (!isChecked){
            audio_play.stop();
            m_vibrator.cancel();
            //reenableKeyguard();
            finish();
        }
    }
}
