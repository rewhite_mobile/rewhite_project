package me.rewhite.delivery.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.util.DUtil;

public class PickupItemCopyCount extends BaseActivity {


    AQuery aq;
    JSONObject pickedItem;
    int copyCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickup_item_copy_item);

        aq = new AQuery(this);
        aq.id(R.id.btn_close).clicked(this, "closeAction");
        aq.id(R.id.btn_complete).clicked(this, "saveAction");
        aq.id(R.id.btn_plus).clicked(this, "addItemCountAction");
        aq.id(R.id.btn_minus).clicked(this, "removeItemCountAction");

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("data") != null) {
            }
            try {
                pickedItem = new JSONObject(intent.getStringExtra("pickedItem"));
                aq.id(R.id.item_title).text(pickedItem.getString("itemTitle"));
                Log.i("picked", pickedItem.toString() + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            aq.id(R.id.count_label).text(copyCount + "");
        }

    }

    public void addItemCountAction(View button){
        copyCount += 1;
        aq.id(R.id.count_label).text(copyCount + "");
    }
    public void removeItemCountAction(View button){
        copyCount -= 1;
        if(copyCount < 0){
            copyCount = 0;
        }
        aq.id(R.id.count_label).text(copyCount + "");
    }

    public void closeAction(View button){
        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }


    public void saveAction(View button){
        if(copyCount > 0){
            Intent resultData = new Intent();
            resultData.putExtra("item", pickedItem.toString());
            resultData.putExtra("value", copyCount);
            setResult(Activity.RESULT_OK, resultData);
            finish();
        }else{
            DUtil.alertShow(this, "수량을 다시 확인해주세요.");
        }

    }

    public void numberRemoveAction(View button){
        copyCount = 0;
        aq.id(R.id.price_label).text("0");
    }

}
