package me.rewhite.delivery.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.logic.Constants;

public class FAQActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private AQuery aq;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        aq = new AQuery(this);
        aq.id(R.id.title_text).text("자주묻는질문");
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        Intent myIntent = getIntent();
        int selected = myIntent.getExtras().getInt("section");

        mViewPager.setCurrentItem(selected);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }

    public void closeClicked(View button) {
        finish();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "전체";
                case 1:
                    return "가맹관련 문의";
                case 2:
                    return "이용문의";
                case 3:
                    return "정산 문의";
                case 4:
                    return "기타";
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private AQuery aq;
        WebView webView;
        ProgressDialog dialog;
        public Context ctx = getActivity();
        boolean loadingFinished = true;
        boolean redirect = false;
        private AQuery bq;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_terms, container, false);
            int section = getArguments().getInt(ARG_SECTION_NUMBER);
            String url = "";

            bq = new AQuery(rootView);


            webView = (WebView) rootView.findViewById(R.id.webView);
            webView.setWebViewClient(new MyWebViewClient());
            webView.clearCache(true);
            webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

            // webView.addJavascriptInterface(new
            // JavaScriptInterface(this.getActivity()), "SEP");
            webView.getSettings().setJavaScriptEnabled(true);
            switch (section) {
                case 0:
                    url = Constants.HTML_FAQ + "/" + section + "?s=Y";
                    break;
                case 1:
                    url = Constants.HTML_FAQ + "/6?s=Y";
                    break;
                case 2:
                    url = Constants.HTML_FAQ + "/2?s=Y";
                    break;
                case 3:
                    url = Constants.HTML_FAQ + "/7?s=Y";
                    break;
                case 4:
                    url = Constants.HTML_FAQ + "/5?s=Y";
                    break;
            }

            webView.loadUrl(url);

            // Progress 처리 진행상황을 보기위해
            dialog = new ProgressDialog(getActivity());
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.setInverseBackgroundForced(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setMessage(getString(R.string.web_loading_comment));

            return rootView;
        }

        private class MyWebViewClient extends WebViewClient {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
                if (!loadingFinished) {
                    redirect = true;
                }

                loadingFinished = false;
                view.loadUrl(urlNewString);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap facIcon) {
                loadingFinished = false;
                // SHOW LOADING IF IT ISNT ALREADY VISIBLE
                dialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (!redirect) {
                    loadingFinished = true;
                }

                if (loadingFinished && !redirect) {
                    // HIDE LOADING IT HAS FINISHED
                    dialog.dismiss();
                } else {
                    redirect = false;
                }

            }
        }

        public class JavaScriptInterface {
            Context mContext;

            JavaScriptInterface(Context c) {
                mContext = c;
            }
        }
    }
}
