package me.rewhite.delivery.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidquery.AQuery;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.common.logic.Constants;

public class FindMapActivity extends BaseActivity implements MapView.MapViewEventListener, MapView.POIItemEventListener, MapView.OpenAPIKeyAuthenticationResultListener, MapView.CurrentLocationEventListener {

    AQuery aq;
    private MapView mapView;
    double longitude;
    double latitude;
    private LocationManager locationManager;
    private String provider;

    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int LOCATION_REQUEST = 1337;

    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    initialize();
                }
                else {
                    Toast.makeText(this, "설정에서 위치정보 동의를 해주셔야합니다.", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_map);


        aq = new AQuery(this);

        aq.hardwareAccelerated11();
        aq.id(R.id.top_area).clicked(this, "topmenuClicked");
        aq.id(R.id.title_text).text("고객위치를 지도에서 확인하세요");

        Intent myIntent = getIntent();
        longitude = myIntent.getExtras().getDouble("longitude");
        latitude = myIntent.getExtras().getDouble("latitude");

        if ( Build.VERSION.SDK_INT >= 23){
            if (!canAccessLocation()) {
                requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
            }else{
                initialize();
            }
        }else{
            initialize();
        }
    }

    private void initialize(){

        mapView = new MapView(this);
        //mapView = (MapView)mView.findViewById(R.id.map_view);
        mapView.setDaumMapApiKey(Constants.DAUM_API_KEY);

        mapView.setMapViewEventListener(this); // this에 MapView.MapViewEventListener 구현.
        mapView.setCurrentLocationEventListener(this);
        MapView.setMapTilePersistentCacheEnabled(true);
        mapView.setShowCurrentLocationMarker(true);
        mapView.setMapType(MapView.MapType.Standard);

//        mapView.setCurrentLocationRadius(0);
//        mapView.setDefaultCurrentLocationMarker();

        mapView.setCurrentLocationRadius(100); // meter
        mapView.setCurrentLocationRadiusFillColor(Color.argb(77, 255, 255, 0));
        mapView.setCurrentLocationRadiusStrokeColor(Color.argb(77, 255, 165, 0));

        MapPOIItem.ImageOffset trackingImageAnchorPointOffset = new MapPOIItem.ImageOffset(16, 16); // 좌하단(0,0) 기준 앵커포인트 오프셋
        MapPOIItem.ImageOffset directionImageAnchorPointOffset = new MapPOIItem.ImageOffset(65, 65);
        MapPOIItem.ImageOffset offImageAnchorPointOffset = new MapPOIItem.ImageOffset(15, 15);

        mapView.setCustomCurrentLocationMarkerTrackingImage(R.mipmap.custom_map_present_tracking, trackingImageAnchorPointOffset);
        mapView.setCustomCurrentLocationMarkerDirectionImage(R.mipmap.custom_map_present_direction, directionImageAnchorPointOffset);
        mapView.setCustomCurrentLocationMarkerImage(R.mipmap.custom_map_present, offImageAnchorPointOffset);

        //mapView.setCurrentLocationTrackingMode(MapView.CurrentLocationTrackingMode.TrackingModeOnWithHeading);
        mapView.setCurrentLocationTrackingMode(MapView.CurrentLocationTrackingMode.TrackingModeOff);

        ViewGroup mapViewContainer = (ViewGroup)findViewById(R.id.map_container);
        mapViewContainer.addView(mapView);
    }

    public void topmenuClicked(View button) {
        finish();
    }

    private void addMarkers() {

        MapPOIItem storeMarker = new MapPOIItem();
        storeMarker.setItemName("고객위치");
        storeMarker.setTag(999);
        storeMarker.setMapPoint(MapPoint.mapPointWithGeoCoord(latitude, longitude));
        storeMarker.setMarkerType(MapPOIItem.MarkerType.CustomImage); // 마커타입을 커스텀 마커로 지정.
        storeMarker.setCustomImageResourceId(R.mipmap.marker_user); // 마커 이미지.
        storeMarker.setCustomImageAutoscale(false); // hdpi, xhdpi 등 안드로이드 플랫폼의 스케일을 사용할 경우 지도 라이브러리의 스케일 기능을 꺼줌.
        storeMarker.setCustomImageAnchor(0.5f, 1.0f); // 마커 이미지중 기준이 되는 위치(앵커포인트) 지정 - 마커 이미지 좌측 상단 기준 x(0.0f ~ 1.0f), y(0.0f ~ 1.0f) 값.
        storeMarker.setShowAnimationType(MapPOIItem.ShowAnimationType.SpringFromGround);
        mapView.addPOIItem(storeMarker);
    }

    double currentLat;
    double currentLng;

    @Override
    public void onMapViewInitialized(MapView mapView) {
        //layoutSetup();

        mapView.setMapCenterPointAndZoomLevel(MapPoint.mapPointWithGeoCoord(latitude, longitude), 1, true);
        addMarkers();
    }

    @Override
    public void onMapViewCenterPointMoved(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewZoomLevelChanged(MapView mapView, int i) {

    }

    @Override
    public void onMapViewSingleTapped(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewDoubleTapped(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewLongPressed(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewDragStarted(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewDragEnded(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewMoveFinished(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onDaumMapOpenAPIKeyAuthenticationResult(MapView mapView, int i, String s) {
        Log.i("FindMapActivity",	String.format("Open API Key Authentication Result : code=%d, message=%s", i, s));
    }

    @Override
    public void onPOIItemSelected(MapView mapView, MapPOIItem mapPOIItem) {

    }

    @Override
    public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem) {

    }

    @Override
    public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem, MapPOIItem.CalloutBalloonButtonType calloutBalloonButtonType) {

    }

    @Override
    public void onDraggablePOIItemMoved(MapView mapView, MapPOIItem mapPOIItem, MapPoint mapPoint) {

    }

    @Override
    public void onCurrentLocationUpdate(MapView mapView, MapPoint mapPoint, float v) {
        Log.e("onCurrentLocationUpdate", mapPoint.getMapPointGeoCoord().latitude + " // " + mapPoint.getMapPointGeoCoord().longitude);
        mapView.setMapCenterPointAndZoomLevel(MapPoint.mapPointWithGeoCoord(mapPoint.getMapPointGeoCoord().latitude, mapPoint.getMapPointGeoCoord().longitude), 1, true);

        MapPoint.GeoCoordinate mapPointGeo = mapPoint.getMapPointGeoCoord();
        Log.e("onCurrentLocationUpdate", String.format("MapView onCurrentLocationUpdate (%f,%f) accuracy (%f)", mapPointGeo.latitude, mapPointGeo.longitude, v));
    }

    @Override
    public void onCurrentLocationDeviceHeadingUpdate(MapView mapView, float v) {

    }

    @Override
    public void onCurrentLocationUpdateFailed(MapView mapView) {

    }

    @Override
    public void onCurrentLocationUpdateCancelled(MapView mapView) {

    }
}
