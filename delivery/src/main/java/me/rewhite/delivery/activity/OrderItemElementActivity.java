package me.rewhite.delivery.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Currency;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.OrderDetailElementListViewAdapter;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.data.PickupType;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class OrderItemElementActivity extends BaseActivity {

    AQuery aq;
    private final static String TAG = OrderItemElementActivity.class.getSimpleName();
    public final Context mCtx = this;

    JSONArray repairArray;
    JSONArray additionalArray;
    JSONArray partsArray;
    JSONArray colorsArray;
    JSONArray priceArray;

    int tagNo;
    int orderId;
    int tagLength;
    int bucketIndex = 1;
    String orderStatus = "";

    boolean isModfied = false;

    int pickupMode = -1;
    JSONArray pickupItems;
    ListView bucketListView;
    OrderDetailElementListViewAdapter elementAdapter;

    boolean isInitialized = false;
    private static final int ADD_ELEMENT = 11;
    private static final int SHOW_ELEMENT = 12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_item_detail);

        aq = new AQuery(this);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                if (intent.getStringExtra("data") != null) {

                }
                pickupItems = new JSONArray(intent.getStringExtra("pickupItems"));
                orderStatus = intent.getStringExtra("orderStatus");
                pickupMode = intent.getIntExtra("pickupMode", 0);
                orderId = intent.getIntExtra("orderId", 0);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(pickupItems == null){
            pickupItems = new JSONArray();
        }

        bucketListView = (ListView)findViewById(R.id.listView);
        ImageView imageView = new ImageView(this);
        //setting image resource
        imageView.setImageResource(R.mipmap.pui_bucket_list_bottom);
        //setting image position
        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        imageView.setLayoutParams(lp);
        imageView.setScaleType(ImageView.ScaleType.FIT_START);
        imageView.setAdjustViewBounds(true);

        ImageView imageView2 = new ImageView(this);
        AbsListView.LayoutParams lp2 = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, 1);
        imageView2.setLayoutParams(lp2);

        bucketListView.addFooterView(imageView);
        bucketListView.addHeaderView(imageView2);
        //bucketListView.setFooterDividersEnabled(true);

        elementAdapter = new OrderDetailElementListViewAdapter(mCtx, R.layout.orderitem_list_item, pickupItems);
        elementAdapter.notifyDataSetChanged();
        bucketListView.setAdapter(elementAdapter);

        refreshList();
        aq.id(R.id.btn_back).clicked(this, "closeClicked");
    }

    public void closeClicked(View button) {
        onBackPressed();
    }

    public void submitItems(View button){
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        int price = 0;
        for(int i = 0; i < pickupItems.length(); i++){
            try {
                price += pickupItems.getJSONObject(i).getInt("confirmPrice");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        logger.logPurchase(BigDecimal.valueOf(price), Currency.getInstance("KRW"));

        Intent resultData = new Intent();
        resultData.putExtra("data", pickupItems.toString());
        setResult(Activity.RESULT_OK, resultData);
        finish();
    }

    private void checkEmpty(){
        if(pickupItems.length() > 0){
            aq.id(R.id.empty_view).gone();
            aq.id(R.id.listView).visible();
            aq.id(R.id.guide_bottom).visible();
        }else{
            aq.id(R.id.empty_view).visible();
            aq.id(R.id.listView).gone();
            aq.id(R.id.guide_bottom).gone();
        }
    }

    public void refreshList(){


        checkEmpty();
        elementAdapter = new OrderDetailElementListViewAdapter(mCtx, R.layout.orderitem_list_item, pickupItems);
        Log.i(TAG + "refresh added", pickupItems.toString());
        elementAdapter.notifyDataSetChanged();
        bucketListView.setAdapter(elementAdapter);

        aq.id(R.id.item_count).text(pickupItems.length()+"");
        aq.id(R.id.price_label).text(getTotalPrice());
    }

    public String getTotalPrice(){
        int price = 0;
        for(int i = 0; i < pickupItems.length(); i++){
            try {
                price += pickupItems.getJSONObject(i).getInt("confirmPrice");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        // 배송비 포함 표시
        if(price < 20000){
            price += 2000;
        }
        return MZValidator.toNumFormat(price);
    }

    public void deleteSelected(int position){
        JSONArray temp = new JSONArray();
        for(int i = 0; i < pickupItems.length(); i++){
            if(position != i){
                try {
                    temp.put(pickupItems.getJSONObject(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        pickupItems = temp;
        refreshList();
    }

    public void itemSelected(int position){

        Intent pickIntent = new Intent(mCtx, OrderItemElementDetailActivity.class);
        pickIntent.putExtra("pickupItems", pickupItems.toString());
        pickIntent.putExtra("orderStatus", orderStatus);
        pickIntent.putExtra("pickupItemsPosition", position);
        pickIntent.putExtra("pickupMode", PickupType.PICKUPTYPE_CLEAN);
        //pickIntent.putExtra("priceData", priceArray.toString());

        try {
            pickIntent.putExtra("initTagNo", pickupItems.getJSONObject(position).getInt("tagId"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        pickIntent.putExtra("initTagLength", tagLength);
        pickIntent.putExtra("bucketIndex", position);
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, SHOW_ELEMENT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ADD_ELEMENT:

                    try {
                        Log.e("Add Element", data.getStringExtra("element"));
                        JSONObject getObj = new JSONObject(data.getStringExtra("element"));
                        if(pickupItems == null){
                            pickupItems = new JSONArray();
                            pickupItems.put(getObj);
                        }else{

                            int pos = data.getIntExtra("position",0);

                            if(pickupItems.length() > pos){
                                int tag = pickupItems.getJSONObject(pos).getInt("tagId");
                                int itemId = pickupItems.getJSONObject(pos).getInt("itemId");
                                JSONArray temp = new JSONArray();
                                if(tag == data.getIntExtra("tagNumber",0) && itemId == getObj.getInt("itemId")){
                                    for(int i  =0 ; i < pickupItems.length(); i++){
                                        if(pos == i ){
                                            temp.put(getObj);
                                        }else{
                                            temp.put(pickupItems.getJSONObject(i));
                                        }
                                    }
                                    pickupItems = temp;
                                }else{
                                    pickupItems.put(getObj);
                                }
                            }else{
                                pickupItems.put(getObj);
                            }
                        }

                        bucketIndex = pickupItems.length()+1;
                        refreshList();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
                case SHOW_ELEMENT:
                    //Log.e("Element Modified", data.getStringExtra("items"));
                    isModfied = true;
                    getRemoteData(orderId);
                    break;
            }
        }
    }

    private void getRemoteData(int _orderId) {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_DETAIL, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        pickupItems = jsondata.getJSONObject("data").getJSONArray("pickupItemsV2");
                        refreshList();
                    }

                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(isModfied){
            Intent resultData = new Intent();
            setResult(Activity.RESULT_OK, resultData);
            finish();
        }else{
            super.onBackPressed();
        }
    }


}
