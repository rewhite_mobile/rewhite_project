package me.rewhite.delivery.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.StringUtil;


public class QNAActivity extends BaseActivity {

    private AQuery aq;
    String app_ver = "";
    private Context ctx = this;


    boolean isAgreed = false;
    int selectedIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qna);

        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        PackageManager m = getPackageManager();
        // 설치된 패키지의 버전이름 추출 : 1.0.0.0 스타일
        try {
            app_ver = m.getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        aq.id(R.id.btn_selector).clicked(this, "selectType");
        aq.id(R.id.btn_agree).clicked(this, "agreeClicked");
        aq.id(R.id.btn_ok).clicked(this, "qnaSubmitAction");
    }

    public void closeClicked(View button) {
        finish();
    }

    public void agreeClicked(View button) {
        isAgreed = !isAgreed;
        if (isAgreed) {
            aq.id(R.id.btn_agree).image(R.mipmap.qna_check_terms_agree_pres);
        } else {
            aq.id(R.id.btn_agree).image(R.mipmap.qna_check_terms_agree);
        }

    }

    public void qnaSubmitAction(View button) {
        if (isAgreed) {
            String title = aq.id(R.id.input_title).getText().toString();
            String content = aq.id(R.id.input_content).getText().toString();

            if (!StringUtil.isNullOrEmpty(content)) {
                if (content.length() > 5) {
                    if (selectedIndex != -1) {
                        submitData(title, content);
                    } else {
                        DUtil.alertShow(this, "문의 분류를 선택해주세요");
                    }
                } else {
                    DUtil.alertShow(this, "문의내용은 최소 6자이상이어야합니다.");
                }
            } else {
                DUtil.alertShow(this, "내용을 입력해 주세요.");
            }
        } else {
            DUtil.alertShow(this, "정보 수집에 동의해 주세요.");
        }
    }

    final String items[] = {"이용문의", "가맹 관련 문의", "정산 문의", "기타"};
    String selectorString = "";
    int tempPosition = -1;

    public void selectType(View button) {

        int selectPosition = selectedIndex;
        if (selectPosition < 0) {
            selectPosition = 0;
        }

        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle("수거/배송시 요청사항 선택");
        ab.setSingleChoiceItems(items, selectPosition, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tempPosition = which;
            }
        }).setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (tempPosition == -1) {
                    tempPosition = 0;
                }
                selectedIndex = tempPosition;
                selectorString = items[selectedIndex];
                aq.id(R.id.text_selector).text(selectorString);
            }
        }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        ab.show();
    }

    private void submitData(String _title, String _content) {

        String phoneType = "Android";
        String appId = "me.rewhite.delivery";
        String osVer = Build.VERSION.RELEASE;
        String phoneModel = Build.DEVICE;
        String exInfoString = String.format("PhoneType:%s / AppId:%s / OsVer:%s / Model:%s / AppVer:%s", phoneType, appId, osVer, phoneModel, app_ver);

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        int si = 0;
        switch (selectedIndex) {
            case 0:
                si = 1;
                break;
            case 1:
                si = 6;
                break;
            case 2:
                si = 7;
                break;
            case 3:
                si = 5;
                break;
        }
        params.put("qnaType", si);

        params.put("requestTitle", _title);
        params.put("requestMessage", _content);
        params.put("exInfo", exInfoString);
        params.put("k", 1);

        NetworkClient.post(Constants.QNA_SUBMIT, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.QNA_SUBMIT, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.QNA_SUBMIT, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            Toast.makeText(ctx, "성공적으로 등록되었습니다.", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }
}
