package me.rewhite.delivery.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;

public class PickupItemPriceMod extends BaseActivity implements AdapterView.OnItemClickListener {

    private String[] numbers = {"1","2","3","4","5","6","7","8","9","00","0","-"};
    AQuery aq;
    int pickedPrice = 0;
    String appendPrice = "";
    int modPrice = 0;
    GridView gv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickup_item_price_mod);

        aq = new AQuery(this);
        aq.id(R.id.btn_remove).clicked(this, "numberRemoveAction");
        aq.id(R.id.btn_close).clicked(this, "closeAction");
        aq.id(R.id.btn_complete).clicked(this, "saveAction");

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("data") != null) {
            }
            pickedPrice = intent.getIntExtra("pickedPrice",0);
            modPrice = pickedPrice;
            appendPrice = pickedPrice + "";
            Log.i("already picked", pickedPrice + "");
            aq.id(R.id.price_label).text(MZValidator.toNumFormat(pickedPrice));
        }


        // 커스텀 아답타 생성
        NumberPickAdapter adapter = new NumberPickAdapter (
                getApplicationContext(),
                R.layout.pui_price_layout_row,       // GridView 항목의 레이아웃 row.xml
                numbers);    // 데이터

        gv = (GridView)findViewById(R.id.gridView);
        gv.setAdapter(adapter);  // 커스텀 아답타를 GridView 에 적용
        gv.setOnItemClickListener(this);
    }

    public void closeAction(View button){
        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }


    public void saveAction(View button){
        if(modPrice > 0){
            Intent resultData = new Intent();
            resultData.putExtra("value", modPrice);
            setResult(Activity.RESULT_OK, resultData);
            finish();
        }else{
            DUtil.alertShow(this, "요금을 다시 확인해주세요.");
        }

    }

    public void numberRemoveAction(View button){
        modPrice = 0;
        appendPrice = "";
        aq.id(R.id.price_label).text("0");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.i("itemclock", position + " / " + appendPrice);

        if(position != numbers.length-1){

            if("".equals(appendPrice) && ("0".equals(numbers[position]) || "00".equals(numbers[position]))){

            }else{
                appendPrice += numbers[position];
                if(appendPrice.length() < 6){
                    //modPrice = Integer.parseInt(appendPrice);
                }else{
                    appendPrice = appendPrice.substring(0,6);
                    //modPrice = Integer.parseInt(appendPrice.substring(0,7));
                }
                modPrice = Integer.parseInt(appendPrice);
                Log.i("modPrice", modPrice + "");
                aq.id(R.id.price_label).text(MZValidator.toNumFormat(modPrice));
            }


        }else{
            modPrice = 0;
            appendPrice = "";
            aq.id(R.id.price_label).text("0");
            /*
            if(appendPrice.length() > 1){
                appendPrice = appendPrice.substring(0, appendPrice.length()-1);
                if(appendPrice.length() < 6){
                    modPrice = Integer.parseInt(appendPrice);
                }else{
                    modPrice = Integer.parseInt(appendPrice.substring(0,6));
                }
                Log.i("modPrice", modPrice + "");
                aq.id(R.id.price_label).text(MZValidator.toNumFormat(modPrice));
            }else if(appendPrice.length() == 1){
                appendPrice = "0";
                modPrice = Integer.parseInt(appendPrice);
                Log.i("modPrice", modPrice + "");
                aq.id(R.id.price_label).text(MZValidator.toNumFormat(modPrice));
            }
*/
        }
    }

    class NumberPickAdapter extends BaseAdapter {
        Context context;
        LayoutInflater inf;
        int layout;

        public NumberPickAdapter(Context context,int layout, String[] numbers) {
            this.context = context;
            this.layout = layout;
            inf = (LayoutInflater) context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return numbers.length;
        }

        @Override
        public String getItem(int position) {

            return numbers[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView==null)
                convertView = inf.inflate(layout, null);


            ImageView button = (ImageView)convertView.findViewById(R.id.button);
            TextView tt = (TextView)convertView. findViewById(R.id.label);
            ImageView iv = (ImageView)convertView.findViewById(R.id.delete_icon);


            if(position != numbers.length-1){
                iv.setVisibility(View.GONE);
                tt.setText(numbers[position]);
            }else{
                tt.setText("");
            }


            return convertView;
        }
    }
}
