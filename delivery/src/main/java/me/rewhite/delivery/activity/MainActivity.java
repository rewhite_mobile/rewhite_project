package me.rewhite.delivery.activity;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gun0912.tedpermission.PermissionListener;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.splunk.mint.Mint;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.Manifest;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.Bootstrap;

import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.OrderListItem;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.fragment.IFragment;
import me.rewhite.delivery.fragment.LeftMenuFragment;
import me.rewhite.delivery.fragment.MainFragment;
import me.rewhite.delivery.fragment.MainListFragment;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.service.CallingService;
import me.rewhite.delivery.session.RewhiteException;
import me.rewhite.delivery.session.Session;
import me.rewhite.delivery.session.SessionCallback;
import me.rewhite.delivery.util.CapacityUtil;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.SystemUtil;
import me.rewhite.delivery.util.TimeUtil;
import me.rewhite.delivery.util.ToastUtility;

public class MainActivity extends SlidingFragmentActivity implements IFragment.OnFragmentInteractionListener {

    public static final String FRAGMENT_MAIN = "main";
    public static final String FRAGMENT_LIST = "itemList";
    public static final String FRAGMENT_SETTING = "settings";

    private static final String TAG = "MainActivity";
    public final Context mCtx = this;
    public SharedPreferences preferences;
    public String reservedFileId;
    private boolean mIsTouchDisable; // 연속 터치로 생기는 이슈 막기 위함
    private boolean mFlagFinish;
    private FrameLayout mFragmentLayout;
    private Handler mHandler;
    private Runnable mTask;
    private Fragment mContent;
    private long splashDelay = 100;

    private final SessionCallback rewhiteSessionCallback = new RewhiteSessionStatusCallback();
    private Session session;

    private CharSequence mTitle;
    private String currentFragname;
    private View decorView;

    public static final String[] SMS_PERMS = {
            android.Manifest.permission.READ_SMS,
            android.Manifest.permission.CALL_PHONE,
            android.Manifest.permission.READ_PHONE_STATE,
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
    };
    public static final int SMS_REQUEST = 1338;
    TelephonyManager telephonyManager;
    String number_1 = "";
    String mPhoneNumber = "";

    @Override
    public void onDestroy() {
        super.onDestroy();

        session.removeCallback(rewhiteSessionCallback);
    }

    @Override
    protected void onResume() {
        appVersionCheck();
        super.onResume();
    }

    private String appDetailSettingString = "위치정보 등 사용할 권한이 없습니다. 앱 권한설정 화면으로 이동합니다.\n이동한 화면에서 '권한'메뉴를 선택하시고 전체 허용해주세요~";

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            //Toast.makeText(OrderDetailActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            //Toast.makeText(OrderDetailActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();

            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(mCtx);
            alertDialogBuilder.setCancelable(false).setTitle("앱 권한 설정하기").setMessage(appDetailSettingString)
                    .setPositiveButton("설정화면으로 가기", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SystemUtil.showApplicationDetailSetting(mCtx);
                        }
                    }).setNegativeButton("취소", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }


    };

    //TODO 1.메소드 permission Request Check.
//    protected void RequestPermission(Activity activity) {
//        if(ContextCompat.checkSelfPermission(activity, Manifest.permission.C2D_MESSAGE) != PackageManager.PERMISSION_GRANTED)
//        {
//            if(ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.C2D_MESSAGE)) {
//                return;
//            }
//            else
//            {
//                ActivityCompat.requestPermissions(activity, new String[] { Manifest.permission.C2D_MESSAGE
//                }, MY_PERMISSIONS_REQUEST_GCM);
//            }
//        }
//    }

    public boolean canAccessLocation() {
        return (hasPermission(android.Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessSMS() {
        return (hasPermission(android.Manifest.permission.READ_SMS));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    .setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
            return;
        }

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_GCM:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            .setData(Uri.parse("package:" + getPackageName()));
                    startActivity(intent);
                } else if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "알림을 권한이 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
            case SMS_REQUEST:
                if (canAccessSMS()) {
                    checkOreoVetionResponse();

                    String phoneNumber = number_1.replace("+82", "0");
                    Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                    mPhoneNumber = MZValidator.validTelNumber(phoneNumber);
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            .setData(Uri.parse("package:" + getPackageName()));
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "핸드폰번호를 알수 없습니다.", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
        //TODO 사용되는 이슈 CHECk
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//
////            int hasC2DPermissions = checkSelfPermission(Manifest.permission.C2D_MESSAGE);
//            if (hasC2DPermissions == PackageManager.PERMISSION_GRANTED) {
//                //startCameraIntent();
//            }else{
//                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
//                        .setData(Uri.parse("package:" + getPackageName()));
//                startActivity(intent);
//            }
//        }
    }

    boolean askedForOverlayPermission = false;
    public static final int OVERLAY_PERMISSION_CODE = 99;

    public void addOverlay() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                askedForOverlayPermission = true;
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, OVERLAY_PERMISSION_CODE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OVERLAY_PERMISSION_CODE) {
            askedForOverlayPermission = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    // SYSTEM_ALERT_WINDOW permission not granted...
                    //Toast.makeText(MyProtector.getContext(), "ACTION_MANAGE_OVERLAY_PERMISSION Permission Granted", Toast.LENGTH_SHORT).show();
                    Intent serviceIntent = new Intent(this, CallingService.class);
                    startService(serviceIntent);

                } else {
                    Toast.makeText(mCtx, "ACTION_MANAGE_OVERLAY_PERMISSION Permission Denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private static final int MY_PERMISSIONS_REQUEST_GCM = 226;

    //@TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //ContextCompat.checkSelfPermission();
            //ActivtiyCompat.requestPermissions();

            //checkSelfPermission(mCtx, "com.google.android.c2dm.permission.RECEIVE")
            //requestPermissions("com.google.android.c2dm.permission.RECEIVE", MY_PERMISSIONS_REQUEST_GCM);
//            RequestPermission(this);
        }

        decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_LAYOUT_FLAGS);

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        //t.setScreenName(TAG);
        t.set("&uid", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_ID));
        // Send a screen view.
        t.send(new HitBuilders.EventBuilder().setCategory("USER").setAction("User Sign In").build());

        AQuery aq = new AQuery(this);
        aq.hardwareAccelerated11();

        session = Session.getInstance(this);
        session.addCallback(rewhiteSessionCallback);

        preferences = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);

        mHandler = new Handler();
        mTask = new Runnable() {

            @Override
            public void run() {
                mFlagFinish = false;
            }
        };

        //showFragment(FRAGMENT_MAIN);
        setupSlideMenu(savedInstanceState);

        if (!"true".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Command.IS_TUTORIAL))) {
            TimerTask task = new TimerTask() {

                @Override
                public void run() {
                    DUtil.Log("GetStartedActivity start", "GetStartedActivity MAKE");

                    Intent tutorialIntent = new Intent(MainActivity.this, TutorialActivity.class);
                    tutorialIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    // tutorialIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    // // Task안남기도록 실행
                    startActivity(tutorialIntent);
                }
            };

            Timer timer = new Timer();
            timer.schedule(task, splashDelay);
        } else {

        }

        Log.e("SYSTEM INFO", CapacityUtil.SHOW_MEMORYSIZE() + "\n\n" + SystemUtil.getSystemInfoString(this));
    }

    private void appVersionCheck() {
        RequestParams params = new RequestParams();
        params.add("k", "1");

        NetworkClient.post(Constants.VERSION_CHECK, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.VERSION_CHECK, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    JSONObject json = new JSONObject(result);

                    if ("S0000".equals(json.getString("resultCode"))) {
                        String version = json.getJSONObject("data").getString("ad");

                        int myVerNum = 0;
                        PackageManager m = mCtx.getPackageManager();

                        // 설치된 패키지의 버전이름 추출 : 1.0.0 스타일
                        String app_ver = m.getPackageInfo(mCtx.getPackageName(), 0).versionName;
                        Log.i("appver", version + " / " + app_ver);

                        // 구분자 '.'를 기반으로 5자리 Integer값으로 변환.
                        // 각 Middle, Minor 버전값은 99를 넘지 못한다.
                        myVerNum = MZValidator.convertVersionStringToInteger(app_ver);

                        // 추출된 버전값을 Preferences값에 저장한다.
                        //SharedPreferencesUtility.set(cmd, value);

                        // 서버에서 최신앱 버전정보를 조회하여 5자리 값으로 변환.
                        int currentVerNum = MZValidator.convertVersionStringToInteger(version);
                        Log.i("AppVersionNum", currentVerNum + "/" + myVerNum);

                        boolean isStrict = false;
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                        alertDialogBuilder.setCancelable(false).setMessage(getResources().getString(R.string.appupdate_minor))
                                .setPositiveButton("업데이트", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setData(Uri.parse("market://details?id=" + DUtil.getAppPackageName(mCtx)));
                                        startActivity(intent);
                                        finish();
                                    }
                                });

                        if (currentVerNum - myVerNum <= 0) {

                        } else {
                            if (currentVerNum - myVerNum >= 1000) {
                                // 강제업데이트
                                alertDialogBuilder.setCancelable(false);
                            } else {
                                alertDialogBuilder.setCancelable(true).setNegativeButton("다음에", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // 설치된 버전값이 서버값보다 작을 경우 update알림 :
                                        // Minor업데이트의 경우 필수업데이트가 아님.

                                    }
                                });
                            }

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (PackageManager.NameNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Mint.logException(e);
                }

            }
        });

    }

    private void setupSlideMenu(Bundle savedInstanceState) {
        // check if the content frame contains the menu frame
        // 좌측 슬라이드메뉴를 세팅한다.
        if (findViewById(R.id.menu_frame) == null) {
            setBehindContentView(R.layout.menu_frame);
            getSlidingMenu().setSlidingEnabled(true);
            getSlidingMenu()
                    .setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
            // show home as up so we can toggle
            // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } else {
            // add a dummy view
            View v = new View(this);
            setBehindContentView(v);
            getSlidingMenu().setSlidingEnabled(false);
            getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        }

        getSlidingMenu().setOnOpenListener(new SlidingMenu.OnOpenListener() {
            @Override
            public void onOpen() {
                View view = decorView;
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0); // 0 : 숨기는 옵션 flag
                }
            }
        });

        // set the Above View Fragment
        if (savedInstanceState != null) {
            if (fragmentIndex != 1) {
                selectIndexFragment(1);
                mContent = new MainListFragment(1);
                if (mContent != null) {
                    switchContent(mContent);
                }
            } else {
                mContent = getSupportFragmentManager().getFragment(savedInstanceState, "mContent");
            }
        }

        if (mContent == null) {
            mContent = new MainListFragment(1);
        }

        //selectIndexFragment(1);
        //mContent = new MainFragment(1);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, mContent).commitAllowingStateLoss();
        // set the Behind View Fragment
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.menu_frame, new LeftMenuFragment()).commitAllowingStateLoss();

        // customize the SlidingMenu
        SlidingMenu sm = getSlidingMenu();
        // 슬라이드메뉴가 열리는 정도를 dimen.slidingmenu_offset에 설정한다.
        sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        sm.setShadowWidthRes(R.dimen.shadow_width);
        sm.setShadowDrawable(R.drawable.shadow);
        sm.setBehindScrollScale(0.5f);
        sm.setFadeDegree(0.25f);

        getSlidingMenu().setSlidingEnabled(true);
        getSlidingMenu()
                .setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mIsTouchDisable) {
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    public boolean getTouchDisable() {
        Log.d(TAG, "[getTouchDisable]");
        return mIsTouchDisable;
    }

    public void setTouchDisable(boolean isTouchDisable) {
        Log.d(TAG, "[setTouchDisable]" + isTouchDisable);
        mIsTouchDisable = isTouchDisable;
    }

    public void setSoftInputMode(boolean isAdjustPan) {
        if (isAdjustPan) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        } else {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
    }

    public ArrayList<OrderListItem> orderData;

    public void itemSelected(int position, JSONObject json) {
        String orderId = orderData.get(position).getOrderId();
        Intent intent = new Intent();

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setClass(this, OrderDetailActivity.class);
        intent.putExtra("data", json.toString());
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
    }

    public void itemStoreSelected(String partnerStoreId, String partnerId) {
        Intent intent = new Intent();

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setClass(this, GSOrderListActivity.class);
        intent.putExtra("partnerStoreId", partnerStoreId);
        intent.putExtra("partnerId", partnerId);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
    }

    public void mapClicked(int position, JSONObject json) {
        String orderId = orderData.get(position).getOrderId();

        try {

            double longitude = json.getDouble("longitude");
            double latitude = json.getDouble("latitude");

            Intent intent = new Intent(this, FindMapActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtra("longitude", longitude);
            intent.putExtra("latitude", latitude);
            startActivity(intent);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void goTargetClicked(int position, final JSONObject json) {
        final String orderId = orderData.get(position).getOrderId();

        try {
            DUtil.Log("goTargetClicked", json.toString());
            final String orderStatus = json.getString("orderStatus");
            final String orderSubType = json.getString("orderSubType");
            final String userName = json.getString("userName");
            final String userId = json.getString("userId");
            final String phoneNo = json.getString("userPhone");

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    String orderReqStatus = "";
                    if (!"01".equals(orderStatus) && !"21".equals(orderStatus) && !"13".equals(orderStatus)) {
                        return;
                    } else {
                        if ("01".equals(orderStatus)) {
                            orderReqStatus = "02";
                        } else if ("21".equals(orderStatus)) {
                            orderReqStatus = "22";
                        } else if ("13".equals(orderStatus)) {
                            orderReqStatus = "22";
                        }
                    }

                    final String orderReqFinalStatus = orderReqStatus;
                    modStatus(orderId, orderReqFinalStatus);

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                    alertDialogBuilder.setCancelable(true)
                            .setMessage(userName + "님(" + MZValidator.validTelNumber(phoneNo) + ")과 전화통화를 원하십니까?")
                            .setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    try {
                                        if (Build.VERSION.SDK_INT >= 23) {
                                            if (!canAccessSMS()) {
                                                requestPermissions(SMS_PERMS, SMS_REQUEST);
                                            } else {
                                                checkOreoVetionResponse();
                                                String phoneNumber = number_1.replace("+82", "0");
                                                Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                                                mPhoneNumber = MZValidator.validTelNumber(phoneNumber);
                                            }
                                        } else {
                                            checkOreoVetionResponse();
                                            String phoneNumber = number_1.replace("+82", "0");
                                            Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                                            mPhoneNumber = MZValidator.validTelNumber(phoneNumber);
                                        }

                                        RequestParams params = new RequestParams();
                                        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                                        params.put("userId", userId);
                                        params.put("callerMdn", mPhoneNumber.replace("-", ""));
                                        params.put("calleeMdn", phoneNo.replace("-", ""));
                                        //params.put("originCallerNumber", orderId);
                                        try {
                                            params.put("appUrl", "rewhiteMeUserOrder://orderDetail?orderId=" + orderId);
                                            params.put("title", "[리화이트] " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_NAME) + "입니다");
                                            params.put("text", getStatusText(orderStatus, json));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        params.put("k", 1);

                                        DUtil.Log(Constants.SEND_T_PHONE + " req", params.toString());

                                        NetworkClient.post(Constants.SEND_T_PHONE, params, new AsyncHttpResponseHandler() {

                                            @Override
                                            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                                                // TODO Auto-generated method stub
                                                DUtil.Log(Constants.SEND_T_PHONE, error.getMessage());
                                            }

                                            @Override
                                            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                                                String result;
                                                try {
                                                    result = new String(data, "UTF-8");
                                                    DUtil.Log(Constants.SEND_T_PHONE, result);

                                                    JSONObject jsondata = new JSONObject(result);

                                                    if (Build.VERSION.SDK_INT >= 23) {
                                                        if (!canAccessSMS()) {
                                                            requestPermissions(SMS_PERMS, SMS_REQUEST);
                                                        } else {
                                                            if ("S0000".equals(jsondata.getString("resultCode"))) {
                                                                call(true, phoneNo, userName);
                                                            } else {
                                                                call(false, phoneNo, userName);
                                                            }
                                                        }
                                                    } else {
                                                        if ("S0000".equals(jsondata.getString("resultCode"))) {
                                                            call(true, phoneNo, userName);
                                                        } else {
                                                            call(false, phoneNo, userName);
                                                        }
                                                    }

                                                } catch (UnsupportedEncodingException | JSONException e) {
                                                    // TODO Auto-generated catch block
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                        call(false, phoneNo, userName);
                                    }


                                }
                            })
                            .setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();


                }

            }, 200);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void call(boolean _isTPhone, String _phoneNo, String _userName) {

        final String phoneNo = _phoneNo.replace("-", "");
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        //callIntent.setAction("android.intent.action.DIAL");
        callIntent.setData(Uri.parse("tel:" + phoneNo));
        if (callIntent.resolveActivity(getPackageManager()) != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(callIntent);
        }
/*
        if(!_isTPhone){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setCancelable(true)
                    .setMessage(_userName + "님("+MZValidator.validTelNumber(phoneNo)+")과 전화통화를 원하십니까?")
                    .setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            //callIntent.setAction("android.intent.action.DIAL");
                            callIntent.setData(Uri.parse("tel:" + phoneNo));
                            if(callIntent.resolveActivity(getPackageManager()) != null){
                                startActivity(callIntent);
                            }
                        }})
                    .setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else{
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            //callIntent.setAction("android.intent.action.DIAL");
            callIntent.setData(Uri.parse("tel:" + phoneNo));
            if(callIntent.resolveActivity(getPackageManager()) != null){
                startActivity(callIntent);
            }
        }*/
    }

    private String getStatusText(String _statusCode, JSONObject parentData) throws JSONException {
        String tempText = "";
        String isPayment = parentData.getJSONObject("data").getString("isPayment");

        if ("01".equals(_statusCode) || "02".equals(_statusCode)) {
            String dps = displayTimeString(parentData.getLong("pickupRequestTimeApp"));
            tempText = dps + " 수거";
            //aq.id(R.id.btn_complete_title).text("수거수량 입력완료");
        } else if ("03".equals(_statusCode)) {
            tempText = "검품 후 인수증 발급 예정";

        } else if ("11".equals(_statusCode) || "12".equals(_statusCode) || "13".equals(_statusCode)) {
            if ("Y".equals(isPayment)) {
                String dps = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
                tempText = dps + " 배송";
            } else {
                tempText = "인수증 확인 후 결제요청";
            }
        } else if ("21".equals(_statusCode) || "22".equals(_statusCode)) {
            String dps = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
            if ("Y".equals(isPayment)) {
                tempText = dps + " 배송";
            } else {
                tempText = dps + " 배송(결제요청)";
            }
        } else if ("23".equals(_statusCode) || "24".equals(_statusCode)) {
            tempText = "우리동네 배달세탁소";
        } else {
            tempText = "우리동네 배달세탁소";
        }
        return tempText;
    }

    private String displayTimeString(long time) {
        //Date dt = new Date();
        //dt.setTime(time);
        if (SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP) == null) {
            SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.DELIVERY_GAP, "1");
        }
        return TimeUtil.getDateByMDSH(time, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP));
    }

    private void modStatus(final String orderId, final String _orderStatus) {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("orderStatus", _orderStatus);
        params.put("k", 1);

        NetworkClient.post(Constants.MOD_ORDER_STATUS, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.MOD_ORDER_STATUS, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.MOD_ORDER_STATUS, result);

                    JSONObject jsondata = new JSONObject(result);
                    if ("S0000".equals(jsondata.getString("resultCode"))) {

                        ToastUtility.show(mCtx, "고객에게 알림을 발송했습니다", Toast.LENGTH_SHORT);
                        if (mContent != null) {
                            mContent.onResume();
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public static int fragmentIndex = 1;

    // 현재 선택중인 Fragment Index 저장
    public void selectIndexFragment(int _fragmentIndex) {
        fragmentIndex = _fragmentIndex;
        Log.i("select fragmentIndex :", fragmentIndex + "");

        if (this.getCurrentFocus() != null) {
            InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            mInputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus()
                    .getWindowToken(), 0);
        }


    }

    /*
     * Fragment간의 전환메서드
     */
    public void switchContent(Fragment fragment) {

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment).commitAllowingStateLoss();
        Handler h = new Handler();
        // 50ms의 오차를 주어 Transition Blocking현상 최소화
        h.postDelayed(new Runnable() {
            public void run() {
                getSlidingMenu().showContent();
            }
        }, 50);
    }

    /*
     * 기기의 Back버튼을 눌렀을 경우 2초안에 Back을 2번누르면 앱종료 Home화면이 아닐경우, Home화면으로 이동
     */
    boolean m_isPressedBackButton;
    long m_startTime;
    long m_endTime;

    @Override
    public void onBackPressed() {

        // Fragment의 Index를 체크하여 Home인지를 확인한다.
        if (fragmentIndex == 1) {
            m_endTime = System.currentTimeMillis();

            // Back버튼의 터치간격은 2초로 설정한다. 즉 2초이내에 2번 눌리어야 앱이 종료된다.
            if (m_endTime - m_startTime > 2000) {
                m_isPressedBackButton = false;
            }

            if (m_isPressedBackButton == false) {
                // Back버튼이 2초내에 1번 눌렸을경우에 Toast로 알림메세지를 출력한다.
                m_isPressedBackButton = true;
                m_startTime = System.currentTimeMillis();
                Toast.makeText(this, getString(R.string.toast_back_app_finish),
                        Toast.LENGTH_SHORT).show();
            } else {
                // 앱이 강제종료됨. 여러가지 종료함수를 호출하여 반드시 종료되게 한다.
                impriortyPackage();

            }
        } else {
            // Home화면이 아닌경우, 1번 Fragment로 이동한다.
            selectIndexFragment(1);
            Fragment homeContent = new MainFragment(1);
            if (homeContent != null) {
                switchContent(homeContent);
            }
        }
    }

    public void impriortyPackage() {
        final ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        List<ActivityManager.RunningServiceInfo> serviceList = am.getRunningServices(100);
        for (ActivityManager.RunningServiceInfo service : serviceList) {
            if (service.pid == android.os.Process.myPid()) {
                Intent stop = new Intent();
                stop.setComponent(service.service);
                stopService(stop);
            }
        }

        Intent launchHome = new Intent(Intent.ACTION_MAIN);
        launchHome.addCategory(Intent.CATEGORY_DEFAULT);
        launchHome.addCategory(Intent.CATEGORY_HOME);
        startActivity(launchHome);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                am.killBackgroundProcesses(getPackageName());
            }

        }, 3000);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    boolean isMainMapType = false;

    public void mainViewTypeToggle() {
        isMainMapType = !isMainMapType;

        if (isMainMapType) {
            selectIndexFragment(1);
            Fragment homeContent = new MainFragment(1);
            if (homeContent != null) {
                switchContent(homeContent);
            }
        } else {
            selectIndexFragment(1);
            Fragment homeListContent = new MainListFragment(1);
            if (homeListContent != null) {
                switchContent(homeListContent);
            }
        }
    }

    public void showDetailInfo(String orderId, final int orderType, final JSONObject data) {

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent detailIntent = new Intent();
                detailIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                detailIntent.setClass(mCtx, OrderDetailActivity.class);
                /*
                if(orderType == RewhiteType.ORDERTYPE_PICKUP){
                    detailIntent.setClass(mCtx, OrderDetailActivity.class);
                }else{
                    detailIntent.setClass(mCtx, DeliveryActivity.class);
                }*/
                detailIntent.putExtra("type", orderType);
                detailIntent.putExtra("data", data.toString());

                mCtx.startActivity(detailIntent);
                ((Activity) mCtx).overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        }, 200);

    }


    private class RewhiteSessionStatusCallback implements SessionCallback {

        /**
         * 세션이 오픈되었으면 가입페이지로 이동 한다.
         */
        @Override
        public void onSessionOpened() {
            // 프로그레스바를 보이고 있었다면 중지하고 세션 오픈후 보일 페이지로 이동
            MainActivity.this.onSessionOpened();
            DUtil.Log(TAG, "onSessionOpened");
        }

        @Override
        public void onSessionOpening() {
            // 뺑글이 시작
            DUtil.Log(TAG, "onSessionOpening");
        }

        /**
         * 세션이 삭제되었으니 로그인 화면이 보여야 한다.
         *
         * @param exception 에러가 발생하여 close가 된 경우 해당 exception
         */
        @Override
        public void onSessionClosed(final RewhiteException exception) {
            // TODO Auto-generated method stub
            MainActivity.this.onSessionClosed();
            DUtil.Log(TAG, "onSessionClosed = " + exception.getLocalizedMessage());
        }
    }

    protected void onSessionClosed() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, SignActivity.class);
                mCtx.startActivity(intent);
                ((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                ((Activity) mCtx).finish();
            }
        }, 200);
    }

    protected void onSessionOpened() {
        //Delivery/StoreInformation
        //Log.e("refreshUserInfo", "==================== onSessionOpened ==================== ");
        //Session.getCurrentSession().refreshUserInfo();
    }

    /**
     * 오레오 getLine1Number checkSelfPermission 체크 Verstion 대응 코드
     */
    private void checkOreoVetionResponse() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            number_1 = telephonyManager.getLine1Number();
        }
    }
}
