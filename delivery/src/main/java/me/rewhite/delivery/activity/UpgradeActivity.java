package me.rewhite.delivery.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;

import me.rewhite.delivery.R;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.StringUtil;

public class UpgradeActivity extends AppCompatActivity {

    AQuery aq;
    String descString = "<B><font color=\"#5eaeff\">%s</font></B> 님의<br/>가맹등급은 <B><font color=\"#5eaeff\">%s</font></B> 입니다.";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade);

        aq = new AQuery(this);
        aq.id(R.id.title_text).text("가맹등급");
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        // Enable hardware acceleration if the device has API 11 or above
        aq.hardwareAccelerated11();

        ImageOptions options = new ImageOptions();
        options.round = 80;
        options.ratio = 1.0f;
        options.memCache = true;
        options.targetWidth = 80;
        options.animation = AQuery.FADE_IN_NETWORK;

        String pImage = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_IMAGE);
        if (!StringUtil.isNullOrEmpty(pImage)) {
            aq.id(R.id.profile_image).image(pImage, options);
        }

        String storeName = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_NAME);
        String storeGrade = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_GRADE);
        String gradeString = "BASIC";
        if ("1".equals(storeGrade)) {
            gradeString = "BASIC";
        } else {
            gradeString = "PREMIUM";
        }

        aq.id(R.id.text_desc).text(Html.fromHtml(String.format(descString, storeName, gradeString))).typeface(CommonUtility.getNanumBarunTypeface());

    }

    public void closeClicked(View button){
        finish();
    }

}
