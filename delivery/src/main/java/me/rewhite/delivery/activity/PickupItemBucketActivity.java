package me.rewhite.delivery.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.PickupElementListViewAdapter;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.data.PickupType;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class PickupItemBucketActivity extends BaseActivity {

    AQuery aq;
    private final static String TAG = PickupItemBucketActivity.class.getSimpleName();
    public final Context mCtx = this;

    JSONArray repairArray;
    JSONArray additionalArray;
    JSONArray partsArray;
    JSONArray colorsArray;
    JSONArray priceArray;

    int prevTagNo = -1;
    int tagNo;
    int tagLength;
    int bucketIndex = 1;

    int pickupMode = -1;
    JSONArray pickupItems = null;
    ListView bucketListView;
    PickupElementListViewAdapter elementAdapter;
    int pickCount = -1;

    ProgressDialog mProgressDialog;

    boolean isInitialized = false;
    private static final int ADD_ELEMENT = 11;
    private static final int MOD_TAG = 12;
    private static final int COPY_ELEMENT = 13;
    private static final int MOD_ELEMENT = 14;

    boolean isGSOrder = false;
    String partnerId;
    int priceGroupId;
    private String orderStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickup_item_bucket);

        aq = new AQuery(this);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                if (intent.getStringExtra("pickupItems") != null) {
                    pickupItems = new JSONArray(intent.getStringExtra("pickupItems"));
                }else{
                    pickupItems = new JSONArray();
                }
                orderStatus = intent.getStringExtra("orderStatus");

                pickupMode = intent.getIntExtra("pickupMode", 0);
                pickCount = intent.getIntExtra("pickCount", 0);
                prevTagNo = intent.getIntExtra("prevTagNo",-1);
                Log.e("pickupMode", pickupMode + "");

                if(pickupMode == PickupType.PICKUPTYPE_GS){
                    isGSOrder = true;
                    partnerId = intent.getStringExtra("partnerId");
                    priceGroupId = intent.getIntExtra("priceGroupId", -1);
                }

                Log.e("pickCount", pickCount + "");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(pickupItems == null){
            pickupItems = new JSONArray();
        }else{

        }

        bucketListView = (ListView)findViewById(R.id.listView);
        try {
            //ImageView Setup
            ImageView imageView = new ImageView(this);
            //setting image resource
            imageView.setImageResource(R.mipmap.pui_bucket_list_bottom);
            //setting image position
            AbsListView.LayoutParams lp = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
            imageView.setLayoutParams(lp);
            imageView.setScaleType(ImageView.ScaleType.FIT_START);
            imageView.setAdjustViewBounds(true);

            ImageView imageView2 = new ImageView(this);
            AbsListView.LayoutParams lp2 = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, 1);
            imageView2.setLayoutParams(lp2);

            bucketListView.addFooterView(imageView);
            bucketListView.addHeaderView(imageView2);
            //bucketListView.setFooterDividersEnabled(true);

            elementAdapter = new PickupElementListViewAdapter(mCtx, R.layout.pickitem_list_item, arrangeElement(pickupItems));
            elementAdapter.notifyDataSetChanged();
            bucketListView.setAdapter(elementAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        checkEmpty();
        initialize();

        aq.id(R.id.btn_add).clicked(this, "addAction");
        aq.id(R.id.btn_center_add).clicked(this, "addAction");
        aq.id(R.id.btn_submit).clicked(this, "submitItems");
        aq.id(R.id.btn_back).clicked(this, "closeClicked");
        aq.id(R.id.btn_mod_tag).clicked(this, "modTagAction").enabled(false);
    }

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(PickupItemBucketActivity.this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) mProgressDialog.dismiss();
    }

    public void modTagAction(View button){
        Intent pickIntent = new Intent(mCtx, PickupItemTagMod.class);
        pickIntent.putExtra("initTagNo", tagNo);
        pickIntent.putExtra("initTagLength", tagLength);
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, MOD_TAG);
    }

    public void closeClicked(View button) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
        alertDialogBuilder.setCancelable(true).setMessage("세탁물 등록이 완료되지 않았습니다.\n중단하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
    boolean isFirstExist = false;
    private JSONArray arrangeElement(JSONArray prevData) throws JSONException {

        JSONArray jsonData = prevData;
        Log.e("arrangeElement", jsonData.toString());

        JSONArray tempData = new JSONArray();

        for(int i = 0; i<jsonData.length();i++){
            int res = (tagNo+i)%(int)Math.pow(10, tagLength);
            if(res == 0){
                isFirstExist = true;
            }
        }
        for(int i = 0; i<jsonData.length();i++){
            JSONObject json = new JSONObject();
            json.put("step1index", jsonData.getJSONObject(i).getInt("step1index"));
            json.put("step2index", jsonData.getJSONObject(i).getInt("step2index"));
            json.put("itemId", jsonData.getJSONObject(i).getInt("itemId"));
            json.put("rCount", jsonData.getJSONObject(i).getInt("rCount"));

            int res = (tagNo+i)%(int)Math.pow(10, tagLength);
            if(res < 100 && isFirstExist == true){
                json.put("tagId", res+1);
            }else{
                json.put("tagId", res);
            }

            json.put("itemTitle", jsonData.getJSONObject(i).getString("itemTitle"));
            json.put("sp", jsonData.getJSONObject(i).getInt("sp"));
            json.put("laundryPrice", jsonData.getJSONObject(i).getInt("laundryPrice"));
            json.put("repairPrice", jsonData.getJSONObject(i).getInt("repairPrice"));
            json.put("additionalPrice", jsonData.getJSONObject(i).getInt("additionalPrice"));
            json.put("isService", jsonData.getJSONObject(i).getString("isService"));
            json.put("isReLaundry", jsonData.getJSONObject(i).getString("isReLaundry"));
            json.put("storeMessage", jsonData.getJSONObject(i).getString("storeMessage"));

            json.put("confirmPrice", jsonData.getJSONObject(i).getInt("confirmPrice"));
            json.put("qty", jsonData.getJSONObject(i).getInt("qty"));
            json.put("itemColor", jsonData.getJSONObject(i).getString("itemColor"));
            json.put("laundry", jsonData.getJSONObject(i).getString("laundry"));
            json.put("repair", jsonData.getJSONObject(i).getJSONArray("repair"));
            json.put("additional", jsonData.getJSONObject(i).getJSONArray("additional"));
            json.put("part", jsonData.getJSONObject(i).getJSONArray("part"));
            json.put("photo", jsonData.getJSONObject(i).getJSONArray("photo"));

            tempData.put(json);
        }

        return tempData;
    }

    public void submitItems(View button){
        if(pickCount != pickupItems.length()){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
            alertDialogBuilder.setCancelable(true).setMessage("등록하신 품목 수량과 수거 수량이 다릅니다. 그래도 등록하시겠어요? ")
                    .setPositiveButton("네", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent resultData = new Intent();
                            try {
                                resultData.putExtra("data", arrangeElement(pickupItems).toString());
                                resultData.putExtra("prevTagNo", tagNo);
                                resultData.putExtra("orderStatus", orderStatus);
                                setResult(Activity.RESULT_OK, resultData);
                                finish();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else{
            Intent resultData = new Intent();
            try {
                resultData.putExtra("data", arrangeElement(pickupItems).toString());
                resultData.putExtra("prevTagNo", tagNo);
                resultData.putExtra("orderStatus", orderStatus);
                setResult(Activity.RESULT_OK, resultData);
                finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void checkEmpty(){
        if(pickupItems.length() > 0){
            aq.id(R.id.empty_view).gone();
            aq.id(R.id.listView).visible();
            aq.id(R.id.guide_bottom).visible();
            aq.id(R.id.btn_empty_add).gone();
            aq.id(R.id.btn_add).visible();
        }else{
            aq.id(R.id.empty_view).visible();
            aq.id(R.id.listView).gone();
            aq.id(R.id.guide_bottom).gone();
            aq.id(R.id.btn_empty_add).visible();
            aq.id(R.id.btn_add).gone();
        }
    }

    public void refreshList(){
        checkEmpty();
        try {
            elementAdapter = new PickupElementListViewAdapter(mCtx, R.layout.pickitem_list_item, arrangeElement(pickupItems));
            Log.i("refresh added", pickupItems.toString());
            elementAdapter.notifyDataSetChanged();
            bucketListView.setAdapter(elementAdapter);
            //bucketListView.invalidate();

            aq.id(R.id.item_count).text(pickupItems.length()+"");
            aq.id(R.id.price_label).text(getTotalPrice());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getTotalPrice(){
        int price = 0;
        for(int i = 0; i < pickupItems.length(); i++){
            try {
                price += pickupItems.getJSONObject(i).getInt("confirmPrice");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return MZValidator.toNumFormat(price);
    }

    public void deleteSelected(final int position){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
        alertDialogBuilder.setCancelable(true).setMessage("등록하신 품목을 삭제하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        JSONArray temp = new JSONArray();
                        for(int i = 0; i < pickupItems.length(); i++){
                            if(position != i){
                                try {
                                    temp.put(pickupItems.getJSONObject(i));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        pickupItems = temp;
                        refreshList();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void copySelected(int position){
        //pickupItems.getJSONObject(position)
        Intent pickIntent = new Intent(mCtx, PickupItemCopyCount.class);
        try {
            pickIntent.putExtra("pickedItem", pickupItems.getJSONObject(position).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, COPY_ELEMENT);
    }

    public void itemSelected(int position){

        Intent pickIntent = new Intent(mCtx, PickupItemActivity.class);
        pickIntent.putExtra("pickupItems", pickupItems.toString());
        pickIntent.putExtra("pickupItemsPosition", position);
        pickIntent.putExtra("pickupMode", pickupMode);
        pickIntent.putExtra("repairData", repairArray.toString());
        pickIntent.putExtra("additionalData", additionalArray.toString());
        pickIntent.putExtra("partsData", partsArray.toString());
        pickIntent.putExtra("colorsData", colorsArray.toString());
        pickIntent.putExtra("priceData", priceArray.toString());

        pickIntent.putExtra("initTagNo", tagNo);
        pickIntent.putExtra("initTagLength", tagLength);
        pickIntent.putExtra("bucketIndex", position);
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, MOD_ELEMENT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ADD_ELEMENT:

                    try {
                        Log.e("Add Element", data.getStringExtra("element"));
                        JSONObject getObj = new JSONObject(data.getStringExtra("element"));
                        if(pickupItems == null){
                            pickupItems = new JSONArray();
                            pickupItems.put(getObj);
                        }else{

                            int pos = data.getIntExtra("position",0);

                            if(pickupItems.length() > pos){
                                int tag = pickupItems.getJSONObject(pos).getInt("tagId");
                                int itemId = pickupItems.getJSONObject(pos).getInt("itemId");
                                JSONArray temp = new JSONArray();
                                if(tag == data.getIntExtra("tagNumber",0) && itemId == getObj.getInt("itemId")){
                                    for(int i  =0 ; i < pickupItems.length(); i++){
                                        if(pos == i ){
                                            temp.put(getObj);
                                        }else{
                                            temp.put(pickupItems.getJSONObject(i));
                                        }
                                    }
                                    pickupItems = temp;
                                }else{
                                    pickupItems.put(getObj);
                                }
                            }else{
                                pickupItems.put(getObj);
                            }
                        }

                        bucketIndex = pickupItems.length();
                        //arrangeElement(pickupItems);

                        refreshList();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
                case MOD_ELEMENT:
                    try {
                        Log.e("MOD_ELEMENT", data.getStringExtra("entire"));
                        pickupItems = new JSONArray(data.getStringExtra("entire"));
                        refreshList();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case MOD_TAG:

                    tagNo = data.getIntExtra("value", 0);
                    aq.id(R.id.current_tag_no).text(MZValidator.getTagFormmater(tagNo, tagLength));

                    break;
                case COPY_ELEMENT:
                    try {
                        Log.e("COPY_ELEMENT", data.getStringExtra("item"));
                        JSONObject getObj = new JSONObject(data.getStringExtra("item"));

                        if(pickupItems == null){
                            pickupItems = new JSONArray();
                            pickupItems.put(getObj);
                        }else{
                            int count = data.getIntExtra("value",0);
                            for(int i  =0 ; i < count; i++){
                                JSONObject json = new JSONObject();
                                json.put("step1index", getObj.getInt("step1index"));
                                json.put("step2index", getObj.getInt("step2index"));
                                json.put("itemId", getObj.getInt("itemId"));
                                json.put("rCount", getObj.getInt("rCount"));
                                int res = (tagNo+(pickupItems.length()+1))%(int)Math.pow(10, tagLength);
                                json.put("tagId", res);
                                json.put("itemTitle", getObj.getString("itemTitle"));
                                json.put("sp", getObj.getInt("sp"));

                                json.put("laundryPrice", getObj.getInt("laundryPrice"));

                                int repairPrice = getObj.getInt("repairPrice");
                                int additionalPrice = getObj.getInt("additionalPrice");


                                json.put("repairPrice", 0);
                                json.put("additionalPrice", 0);
                                json.put("isService", "N");
                                json.put("isReLaundry", "N");
                                json.put("storeMessage", getObj.getString("storeMessage"));

                                if(repairPrice > 0 || additionalPrice > 0){
                                    json.put("confirmPrice", getObj.getInt("laundryPrice"));
                                }else{
                                    json.put("confirmPrice", getObj.getInt("confirmPrice"));
                                }


                                json.put("qty", getObj.getInt("qty"));
                                json.put("itemColor", "");
                                json.put("laundry", getObj.getString("laundry"));
                                //json.put("repair", getObj.getJSONArray("repair"));
                                json.put("repair", new JSONArray());
                                json.put("additional", new JSONArray());
                                json.put("part", new JSONArray());
                                json.put("photo", new JSONArray());

                                pickupItems.put(json);
                            }
                        }

                        bucketIndex = pickupItems.length();

                        //pickupItems = arrangeElement(pickupItems);


                        refreshList();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    public void addAction(View button){
        if(!isInitialized) {
            return;
        }
        Intent pickIntent = new Intent(mCtx, PickupItemActivity.class);
        //pickIntent.putExtra("pickupItems", pickupItems.toString());
        pickIntent.putExtra("pickupMode", PickupType.PICKUPTYPE_CLEAN);
        pickIntent.putExtra("repairData", repairArray.toString());
        pickIntent.putExtra("additionalData", additionalArray.toString());
        pickIntent.putExtra("partsData", partsArray.toString());
        Log.e("partsArray addAction", partsArray.toString());
        pickIntent.putExtra("colorsData", colorsArray.toString());
        pickIntent.putExtra("priceData", priceArray.toString());
        pickIntent.putExtra("isFirstExist", isFirstExist);

        pickIntent.putExtra("initTagNo", tagNo);
        pickIntent.putExtra("initTagLength", tagLength);
        pickIntent.putExtra("bucketIndex", pickupItems.length());
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, ADD_ELEMENT);

        //bucketIndex++;
    }

    private void initialize(){
        getPickupInitializeData();
    }

    private void getPickupInitializeData(){
        showDialog();

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.PICKUP_INITIALIZE_DATA, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.PICKUP_INITIALIZE_DATA, error.getMessage());

                dismissDialog();
                errorBackProcess();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                dismissDialog();

                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.PICKUP_INITIALIZE_DATA, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);
                    Log.e("partsArray addAction", jsondata.toString());


                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        JSONObject retData = jsondata.getJSONObject("data");
                        repairArray = retData.getJSONArray("repair");
                        additionalArray = retData.getJSONArray("additional");
                        partsArray = retData.getJSONArray("part");
                        colorsArray = retData.getJSONArray("color");

                        getLastTag();

                    }else{
                        errorBackProcess();
                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private void errorBackProcess(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
        alertDialogBuilder.setCancelable(true).setMessage("서버와의 통신이 원활하지 않습니다. 다시 시도해주세요.")
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void getLastTag(){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.GET_LASTEST_TAG_NO, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GET_LASTEST_TAG_NO, error.getMessage());
                errorBackProcess();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_LASTEST_TAG_NO, result);
                    //dismissDialog();

                    aq.id(R.id.btn_mod_tag).enabled(true);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        JSONArray retData = jsondata.getJSONArray("data");
//                        if(prevTagNo == -1){
//                            tagNo = retData.getInt(0);
//                        }else{
//                            tagNo = prevTagNo;
//                        }
                        tagNo = retData.getInt(0);
                        tagLength = retData.getInt(1);

                        aq.id(R.id.current_tag_no).text(MZValidator.getTagFormmater(tagNo, tagLength));

                        if(pickupItems.length() > 0){
                            refreshList();
                        }

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(isGSOrder){
                                    getGSPriceTable(partnerId, priceGroupId);
                                }else{
                                    getPriceTable();
                                }

                            }}, 200);

                    }else{
                        errorBackProcess();
                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private void getPriceTable(){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));

        if(pickupMode == PickupType.PICKUPTYPE_CLEAN){
            params.put("mode", "G");
        }else if(pickupMode == PickupType.PICKUPTYPE_TODAY){
            params.put("mode", "T");
        }else if(pickupMode == PickupType.PICKUPTYPE_TOMORROW){
            params.put("mode", "N");
        }else if(pickupMode == PickupType.PICKUPTYPE_B2B_HANDYS){
            params.put("mode", "B");
        }

        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_PRICE_TABLE_V2, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_PRICE_TABLE_V2, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_PRICE_TABLE_V2, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        JSONArray obj = jsondata.getJSONArray("data");
                        priceArray = new JSONArray();
                        for(int i = 0 ; i < obj.length(); i++){
                            if(obj.getJSONObject(i).getJSONArray("items").length() > 0){
                                priceArray.put(obj.getJSONObject(i))    ;
                            }
                        }
                        //priceArray = jsondata.getJSONArray("data");
                        isInitialized = true;
                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    // GS
    private void getGSPriceTable(String partnerId, int priceGroupId){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("partnerId", partnerId);
        params.put("priceGroupId", priceGroupId);
        params.put("k", 1);
        NetworkClient.post(Constants.PARTNER_PRICE, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.PARTNER_PRICE, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.PARTNER_PRICE, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        JSONArray obj = jsondata.getJSONArray("data");
                        priceArray = new JSONArray();
                        for(int i = 0 ; i < obj.length(); i++){
                            if(obj.getJSONObject(i).getJSONArray("items").length() > 0){
                                priceArray.put(obj.getJSONObject(i))    ;
                            }
                        }
                        //priceArray = jsondata.getJSONArray("data");
                        isInitialized = true;


                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }
}
