package me.rewhite.delivery.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.delivery.Bootstrap;
import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.session.AccessToken;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.SharedPreferencesUtility.Command;


public class GetStartedActivity extends BaseActivity {

    private static final String TAG = "GetStartedActivity";
    private Context mContext;
    private WalkthroughAdapter mPagerAdapter;

    private final int mPageCount = 5;
    private AQuery aq;
    ImageButton button = null;

    public void call() {
        Intent callIntent = new Intent();
        callIntent.setAction("android.intent.action.DIAL");
        callIntent.setData(Uri.parse("tel:15442951"));
        startActivity(callIntent);
    }

    public void csCallAction(View button) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(true).setMessage("리:화이트 고객센터와 전화통화를 원하십니까?")
                .setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        call();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_getstarted);

        aq = new AQuery(this);

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        aq.id(R.id.bottom_area).gone();
        aq.id(R.id.btn_call).clicked(this, "csCallAction");

        mContext = this;
        mPagerAdapter = new WalkthroughAdapter();

        button = (ImageButton) findViewById(R.id.layout_walkthrough_btn);
        button.setOnClickListener(mOnClickStartBtn);
        button.setVisibility(View.GONE);

        ViewPager pager = (ViewPager) findViewById(R.id.activity_walkthrough_layout);
        pager.setAdapter(mPagerAdapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        aq.id(R.id.indicator_image).image(R.mipmap.g_indicator_01).visible();
                        button.setVisibility(View.GONE);
                        aq.id(R.id.bottom_area).gone();
                        break;

                    case 1:
                        aq.id(R.id.indicator_image).image(R.mipmap.g_indicator_02).visible();
                        button.setVisibility(View.GONE);
                        aq.id(R.id.bottom_area).gone();
                        break;

                    case 2:
                        aq.id(R.id.indicator_image).image(R.mipmap.g_indicator_03).visible();
                        button.setVisibility(View.GONE);
                        aq.id(R.id.bottom_area).gone();
                        break;

                    case 3:
                        aq.id(R.id.indicator_image).image(R.mipmap.g_indicator_04).visible();
                        button.setVisibility(View.GONE);
                        aq.id(R.id.bottom_area).gone();
                        break;

                    case 4:
                        aq.id(R.id.indicator_image).image(R.mipmap.g_indicator_05).gone();
                        button.setVisibility(View.VISIBLE);
                        aq.id(R.id.bottom_area).visible();
                        break;

                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private View.OnClickListener mOnClickStartBtn = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SharedPreferencesUtility.set(Command.IS_GETSTARTED, "true");
            AccessToken.clearAccessTokenFromCache();

            Intent intent = new Intent(mContext, SignActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            startActivity(intent);
            finish();
        }
    };

    private class WalkthroughAdapter extends PagerAdapter {
        @Override
        public int getCount() {
            return mPageCount;
        }

        @Override
        public void destroyItem(View pager, int position, Object view) {
            ((ViewPager) pager).removeView((View) view);
        }

        @Override
        public Object instantiateItem(View pager, int position) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            RelativeLayout view = null;
            ImageView imageView = null;

            switch (position) {
                case 0:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.getstarted_bg01);
                    ((ViewPager) pager).addView(view, 0);
                    break;

                case 1:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.getstarted_bg02);
                    ((ViewPager) pager).addView(view, 0);
                    break;

                case 2:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.getstarted_bg03);
                    ((ViewPager) pager).addView(view, 0);
                    break;

                case 3:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.getstarted_bg04);
                    ((ViewPager) pager).addView(view, 0);
                    break;
                case 4:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.getstarted_bg05);
                    ((ViewPager) pager).addView(view, 0);


                    break;

                default:
                    break;
            }

            return view;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }
    }



}
