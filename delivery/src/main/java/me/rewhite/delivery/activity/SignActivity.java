package me.rewhite.delivery.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.delivery.Bootstrap;
import me.rewhite.delivery.Bootstrap.TrackerName;
import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.fragment.IFragment;
import me.rewhite.delivery.fragment.LoginFragment;
import me.rewhite.delivery.session.AccessTokenRequest;
import me.rewhite.delivery.session.RewhiteException;
import me.rewhite.delivery.session.Session;
import me.rewhite.delivery.session.SessionCallback;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.StringUtil;

//import com.actionbarsherlock.app.SherlockFragmentActivity;


public class SignActivity extends BaseActivity {

	public static String TAG = "SignActivity";
	public static final String FRAGMENT_LOGIN = "sign_login";
	public static final String FRAGMENT_JOIN = "sign_join";
	View decorView;

	public final Context mCtx = this;
	private final SessionCallback mySessionCallback = new RewhiteSessionStatusCallback();
	private Session session;
	public boolean isLoginProcessing = false;

	@Override
	public void onDestroy() {
		super.onDestroy();
		session.removeCallback(mySessionCallback);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if(CommonUtility.isTablet(this)){
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			setContentView(R.layout.tablet_activity_sign);
		}else{
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			setContentView(R.layout.activity_sign);
		}

		Tracker t = ((Bootstrap) getApplication()).getTracker(TrackerName.APP_TRACKER);
		// Set screen name.
		t.setScreenName(TAG);
		// Send a screen view.
		t.send(new HitBuilders.ScreenViewBuilder().build());

		initilize();
	}

	private void initilize() {

		session = Session.getInstance(this);
		session.addCallback(mySessionCallback);

		showFragment(FRAGMENT_LOGIN);
	}

	public void showFragment(String fragmentName) {
		Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);
		showFragment(fragmentName, null);
	}

	public void showFragment(String fragmentName, Bundle args) {
		Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);

		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		Fragment fragment = fm.findFragmentByTag(fragmentName);

		if (FRAGMENT_LOGIN == fragmentName) {
			fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}

		fragment = new LoginFragment();
		Log.e(TAG, "[LoginFragment] Create : " + fragmentName);

		if (null != args) {
			fragment.setArguments(args);
		}

		ft.replace(R.id.main_fragment_container, fragment, fragmentName);
		ft.addToBackStack(fragmentName);
		ft.commitAllowingStateLoss();
		//ft.commit();
	}
	
	ProgressDialog mProgressDialog;

	private class RewhiteSessionStatusCallback implements SessionCallback {

		/**
		 * 세션이 오픈되었으면 가입페이지로 이동 한다.
		 */
		@Override
		public void onSessionOpened() {
			// 뺑글이 종료

			// 프로그레스바를 보이고 있었다면 중지하고 세션 오픈후 보일 페이지로 이동
			SignActivity.this.onSessionOpened();
			DUtil.Log(TAG, "onSessionOpened");
			if (mProgressDialog != null)   mProgressDialog.dismiss();
		}

		@Override
		public void onSessionOpening() {
			// 뺑글이 시작
			isLoginProcessing = true;
			
		}

		/**
		 * 세션이 삭제되었으니 로그인 화면이 보여야 한다.
		 * 
		 * @param exception
		 *            에러가 발생하여 close가 된 경우 해당 exception
		 */
		@Override
		public void onSessionClosed(RewhiteException exception) {
			// TODO Auto-generated method stub
			SignActivity.this.onSessionClosed();
			DUtil.Log(TAG, "onSessionClosed = " + exception.getLocalizedMessage());
			isLoginProcessing = false;
			if (mProgressDialog != null)   mProgressDialog.dismiss();
		}
	}

	public void requestSessionOpen(Context ctx, String email, String password, String deviceToken) {
		DUtil.Log(TAG, "requestSessionOpen Start ");
		/*
		mProgressDialog = new ProgressDialog(Bootstrap.getCurrentActivity());
        mProgressDialog.setMessage("로그인 진행중...");
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();*/
        isLoginProcessing = true;

		tempEmail = email;
		tempPassword = password;

		AccessTokenRequest aRequest = AccessTokenRequest.createRequestWithDeviceToken(ctx, email, password, deviceToken);
		session.open(aRequest);
	}

	String tempEmail;
	String tempPassword;

	protected void onSessionClosed() {
	}

	protected void onSessionOpened() {

        String accessToken = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN);
		Log.e("onSessionOpened", "onSessionOpened : " + accessToken);
		if (StringUtil.isNullOrEmpty(accessToken)) {
			isLoginProcessing = false;
			//Toast.makeText(this, "로그인에 실패했습니다.", Toast.LENGTH_SHORT).show();
			if (mProgressDialog != null) mProgressDialog.dismiss();
			return;
		}

        SharedPreferencesUtility.set(SharedPreferencesUtility.Command.IS_AUTO_LOGIN, "true");

		AccountManager accountManager = AccountManager.get(this); //this is Activity
		String accountId = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_ID);
		String authType = "me.rewhite.delivery.account";
		Account account = new Account(accountId,authType);

		boolean success = accountManager.addAccountExplicitly(account, tempPassword, null);
		if(success){
			Log.e(TAG,"Account created : " + account.toString() + " / " + accountManager.getPassword(account));
		}else{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
				accountManager.removeAccountExplicitly(account);
				accountManager.addAccountExplicitly(account, tempPassword, null);
			}else{
				accountManager.setPassword(account, tempPassword);
				//accountManager.clearPassword(account);
			}

			Log.e(TAG,"Account creation failed. Look at previous logs to investigate");
		}

        new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				isLoginProcessing = false;
				if (mProgressDialog != null)   mProgressDialog.dismiss();

				AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
				logger.logEvent("Register R-Coin Completed");

				Intent intent = new Intent();
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                if(CommonUtility.isTablet(mCtx)){
                    intent.setClass(mCtx, TabletMainActivity.class);
                }else{
                    intent.setClass(mCtx, MainActivity.class);
                }
				mCtx.startActivity(intent);
				((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
				((Activity) mCtx).finish();
			}
		}, 500);

	}

	@Override
	public void onBackPressed() {

		FragmentManager fm = getSupportFragmentManager();
		DUtil.Log(TAG, "[onBackPressed] Back Stack Entry Count : " + fm.getBackStackEntryCount());

		if (fm.getBackStackEntryCount() >= 1) {
			String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
			DUtil.Log(TAG, "[FragmentManager] tag : " + tag + "\nfm.getBackStackEntryCount() : " + fm.getBackStackEntryCount());

			IFragment fragment = (IFragment) fm.findFragmentByTag(tag);
			if (false == fragment.onBackPressed()) {
				fm.popBackStack();
				if (fm.getBackStackEntryCount() == 1) super.onBackPressed();
			}
		} else {
			super.onBackPressed();
		}
	}

}
