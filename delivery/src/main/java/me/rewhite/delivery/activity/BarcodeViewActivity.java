package me.rewhite.delivery.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.zxing.BarcodeFormat;
import me.rewhite.delivery.zxing.MultiFormatWriter;
import me.rewhite.delivery.zxing.WriterException;
import me.rewhite.delivery.zxing.common.BitMatrix;

public class BarcodeViewActivity extends AppCompatActivity {

    AQuery aq;
    String orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_view);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("orderId") != null) {
                orderId = intent.getStringExtra("orderId");
            } else {
                finish();
            }
        }

        aq = new AQuery(this);

        MultiFormatWriter writer =new MultiFormatWriter();

        int blankV = 10 - orderId.length();
        String blankStr = "";
        for(int i = 0; i < blankV; i++){
            blankStr += "0";
        }

        String finaldata = Uri.encode("1" + blankStr + orderId, "utf-8");
        String outputFinalText = Uri.encode("1-" + blankStr + orderId, "utf-8");

        int barWidth = 300;
        int barHeight = 60;


        BitMatrix bm = null;
        try {
            bm = writer.encode(finaldata, BarcodeFormat.CODE_128,barWidth, barHeight);
        } catch (WriterException e) {
            e.printStackTrace();
        }


        Bitmap ImageBitmap = Bitmap.createBitmap(barWidth, barHeight, Bitmap.Config.ARGB_8888);

        for (int i = 0; i < barWidth; i++) {//width
            for (int j = 0; j < barHeight; j++) {//height
                ImageBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK: Color.WHITE);
            }
        }

        aq.id(R.id.barcode_text).text(outputFinalText);

        if (ImageBitmap != null) {
            aq.id(R.id.barcode_container).image(ImageBitmap);
            //qrcode.setImageBitmap(ImageBitmap);
        } else {
            Toast.makeText(getApplicationContext(), "바코드 생성에 실패했습니다.",
                    Toast.LENGTH_SHORT).show();
        }

        aq.id(R.id.btn_close).clicked(this, "closeAction");
    }

    public void closeAction(View button){
        finish();
    }
}
