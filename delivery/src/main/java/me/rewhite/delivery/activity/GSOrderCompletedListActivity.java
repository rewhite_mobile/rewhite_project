package me.rewhite.delivery.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.GSOrderListItem;
import me.rewhite.delivery.adapter.GSOrderListViewAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.GSBaseActivity;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class GSOrderCompletedListActivity extends GSBaseActivity {

    String TAG = "GSOrderCompletedListActivity";
    AQuery aq;
    Context ctx;
    String partnerStoreId;
    String partnerId;
    String partnerStoreName = "";

    String currentSearchType = "I";
    // M : 세탁소회수 (81), N : 세탁소회수완료 (82)
    private ArrayList<GSOrderListItem> orderData;
    int currentPage = 1;
    public JSONArray publicOrderInfo;

    @Override
    public void onResume() {
        super.onResume();

        initialize();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gsorder_completed_list);

        Log.w("Activity", TAG);

        ctx = this;
        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        aq.id(R.id.title_text).typeface(CommonUtility.getNanumBarunTypeface());

        Intent intent = getIntent();
        if (intent != null) {
                partnerStoreId = intent.getStringExtra("partnerStoreId");
                partnerId = intent.getStringExtra("partnerId");
                partnerStoreName = intent.getStringExtra("partnerStoreName");
                //setTitle(partnerStoreName);
                //washingDays = orderInfo.getInt("washingDays");
        }

        aq.id(R.id.btn_a).clicked(this, "showData").tag(0).backgroundColor(Color.parseColor("#f2f2f2")).textColorId(R.color.actionbar_gs_background);
        aq.id(R.id.btn_b).clicked(this, "showData").tag(1).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));
        aq.id(R.id.btn_c).clicked(this, "showData").tag(2).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));

        //initialize();
    }



    int currentSearchIndex = 0;

    public void showData(View button){
        int tag = (int)((Button)button).getTag();
        currentSearchIndex = tag;
        switch (tag){
            case 0:
                currentSearchType = "I"; // 수거 오늘이후도 나오도록
                aq.id(R.id.btn_a).backgroundColor(Color.parseColor("#f2f2f2")).textColorId(R.color.actionbar_gs_background);
                aq.id(R.id.btn_b).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));
                aq.id(R.id.btn_c).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));
                aq.id(R.id.btn_d).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));
                aq.id(R.id.btn_e).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));
                aq.id(R.id.listView).visible();
                break;
            case 1:
                currentSearchType = "E";
                aq.id(R.id.btn_b).backgroundColor(Color.parseColor("#f2f2f2")).textColorId(R.color.actionbar_gs_background);
                aq.id(R.id.btn_a).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));
                aq.id(R.id.btn_c).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));
                aq.id(R.id.btn_d).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));
                aq.id(R.id.btn_e).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));
                aq.id(R.id.listView).visible();
                break;
            case 2:
                currentSearchType = "N";
                aq.id(R.id.btn_c).backgroundColor(Color.parseColor("#f2f2f2")).textColorId(R.color.actionbar_gs_background);
                aq.id(R.id.btn_a).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));
                aq.id(R.id.btn_b).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));
                aq.id(R.id.btn_d).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));
                aq.id(R.id.btn_e).background(R.color.actionbar_gs_background).textColor(Color.parseColor("#f2f2f2"));
                aq.id(R.id.listView).gone();
                break;
        }
        orderData = new ArrayList<>();
        initialize();
    }

    public void setTitle(String _title) {
        aq.id(R.id.title_text).text(_title);
    }

    public void closeClicked(View button) {
        onBackPressed();
    }



    private void initialize(){

        showDialog();

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("mode", currentSearchType);
        params.put("partnerId", partnerId);
        params.put("partnerStoreId", partnerStoreId);
        params.put("page", 1);
        params.put("block", 999);
        params.put("k", 1);

        NetworkClient.post(Constants.PARTNER_ORDER_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.PARTNER_ORDER_LIST, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.PARTNER_ORDER_LIST, result);

                    JSONObject jsondata = new JSONObject(result);
                    //orderListView = (ListView) getActivity().findViewById(R.id.listView);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data") || jsondata.getString("data") == null) {
                            //
                            if (orderData == null) {
                                orderData = new ArrayList<>();
                            } else {

                            }

                            if (orderData.size() > 0) {
                                aq.id(R.id.listView).visible();
                                aq.id(R.id.empty).gone();
                            } else {
                                aq.id(R.id.total_layout).gone();
                                aq.id(R.id.listView).gone();
                                aq.id(R.id.empty).visible();
                            }

                        } else {

                            orderData = new ArrayList<>();

                            final JSONArray orderInfo = jsondata.getJSONArray("data");
                            publicOrderInfo = orderInfo;

                            for (int i = 0; i < orderInfo.length(); i++) {

                                String orderId = orderInfo.getJSONObject(i).getString("orderId");
                                String orderStatus = orderInfo.getJSONObject(i).getString("orderStatus");
                                String storeId = orderInfo.getJSONObject(i).getString("storeId");
                                String isNewPOS = "N";
                                if(!orderInfo.getJSONObject(i).isNull("isNewPOS")){
                                    isNewPOS = orderInfo.getJSONObject(i).getString("isNewPOS");
                                }
                                String deliveryPrice = orderInfo.getJSONObject(i).getString("deliveryPrice");
                                String pickupRequestTimeApp = orderInfo.getJSONObject(i).getString("pickupRequestTimeApp");
                                String deliveryRequestTimeApp = orderInfo.getJSONObject(i).getString("deliveryRequestTimeApp");
                                String registerDateApp = orderInfo.getJSONObject(i).getString("registerDateApp");
                                String orderRequest = orderInfo.getJSONObject(i).getString("orderRequest");
                                String orderPickupItemMessage = orderInfo.getJSONObject(i).getString("orderPickupItemMessage");
                                String isAdditionalPayment = null;
                                if(orderInfo.getJSONObject(i).isNull("isAdditionalPayment")){
                                    isAdditionalPayment = null;
                                }else{
                                    isAdditionalPayment = orderInfo.getJSONObject(i).getString("isAdditionalPayment");
                                }
                                // null : 추가 결제 필요없음
                                // N : 추가 결제 필요한거
                                // Y : 추가 결제 완료

                                if(orderPickupItemMessage.length() > 20){
                                    orderPickupItemMessage = orderPickupItemMessage.substring(0,18) + " ...";
                                }
                                String statusTimeApp = orderInfo.getJSONObject(i).getString("statusTimeApp");
                                String userName = orderInfo.getJSONObject(i).getString("userName");
                                int quantity = orderInfo.getJSONObject(i).getInt("pickupQuantity");
                                String isPayment = orderInfo.getJSONObject(i).getString("isPayment");
                                String payments = null;
                                if(orderInfo.getJSONObject(i).isNull("payments")){
                                    payments = null;
                                }else{
                                    payments = orderInfo.getJSONObject(i).getString("payments");
                                }

                                String isReceivable = orderInfo.getJSONObject(i).getString("isReceivable");

                                int packQuantity = orderInfo.getJSONObject(i).getInt("packQuantity");
                                int boxQuantity = orderInfo.getJSONObject(i).getInt("boxQuantity");
                                String partnerOrderNo = orderInfo.getJSONObject(i).getString("partnerOrderNo");
                                //String partnerStoreId = orderInfo.getJSONObject(i).getString("partnerStoreId");
                                //String partnerId = orderInfo.getJSONObject(i).getString("partnerId");



                                if("22".equals(orderStatus)){
                                    if(currentSearchIndex == 0){
                                        GSOrderListItem aItem = new GSOrderListItem(orderId, orderStatus, null, storeId, userName, deliveryPrice, pickupRequestTimeApp,
                                                deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage, orderInfo.getJSONObject(i).toString(),
                                                quantity, isPayment, payments, isReceivable, isAdditionalPayment
                                                , packQuantity, boxQuantity, partnerOrderNo, partnerStoreName, partnerStoreId, partnerId, isNewPOS);
                                        orderData.add(aItem);
                                    }else if(currentSearchIndex == 2){

                                    }
                                }else{
                                    GSOrderListItem aItem = new GSOrderListItem(orderId, orderStatus, null, storeId, userName, deliveryPrice, pickupRequestTimeApp,
                                            deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage, orderInfo.getJSONObject(i).toString(),
                                            quantity, isPayment, payments, isReceivable, isAdditionalPayment
                                            , packQuantity, boxQuantity, partnerOrderNo, partnerStoreName, partnerStoreId, partnerId, isNewPOS);
                                    orderData.add(aItem);
                                }
                            }

                            aq.id(R.id.listView).visible();

                            orderAdapter = new GSOrderListViewAdapter(orderData, new GSOrderListViewAdapter.MyAdapterListener() {
                                @Override
                                public void buttonViewOnClick(View v, final int position) {
                                    Log.e("buttonViewOnClick", " at position "+position);
                                    try {
                                        Intent intent = new Intent();

                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        intent.setClass(ctx, GSOrderDetailActivity.class);
                                        intent.putExtra("orderId", orderData.get(position).getOrderId() );//orderInfo.getJSONObject(position).toString() );
                                        intent.putExtra("data", orderInfo.getJSONObject(position).toString() );

                                        startActivity(intent);
                                        overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void contentViewOnClick(View v, int position) {
                                    Log.e("contentViewOnClick", " at position "+position);
                                    try {
                                        Intent intent = new Intent();

                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        intent.setClass(ctx, GSOrderDetailActivity.class);
                                        intent.putExtra("orderId", orderData.get(position).getOrderId() );//orderInfo.getJSONObject(position).toString() );
                                        intent.putExtra("data", orderInfo.getJSONObject(position).toString() );

                                        startActivity(intent);
                                        overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                            });
                            RecyclerView myView =  (RecyclerView)findViewById(R.id.listView);
                            myView.setHasFixedSize(true);
                            myView.setAdapter(orderAdapter);
                            LinearLayoutManager llm = new LinearLayoutManager(ctx);
                            llm.setOrientation(LinearLayoutManager.VERTICAL);
                            myView.setLayoutManager(llm);

                            orderAdapter.notifyDataSetChanged();

                            if(currentSearchIndex == 0){
                                aq.id(R.id.total_layout).visible();
                                aq.id(R.id.total_delivery_layout).gone();
                                aq.id(R.id.total_pickup_layout).visible();

                                int pickupCount = 0;
                                int packCount = 0;

                                for(int i = 0 ; i < orderInfo.length(); i++){
                                    if("01".equals(orderInfo.getJSONObject(i).getString("orderStatus"))){
                                        pickupCount++;
                                        packCount += orderInfo.getJSONObject(i).getInt("packQuantity");
                                    }else{

                                    }
                                }

                                aq.id(R.id.text_gs_pickup_count).text(pickupCount + "건");
                                aq.id(R.id.text_gs_pack_count).text(packCount + "개");
                            }else if(currentSearchIndex == 4){
                                aq.id(R.id.total_layout).visible();
                                aq.id(R.id.total_delivery_layout).visible();
                                aq.id(R.id.total_pickup_layout).gone();

                                int deliveryCount = 0;
                                int boxCount = 0;

                                for(int i = 0 ; i < orderInfo.length(); i++){
                                    if("21".equals(orderInfo.getJSONObject(i).getString("orderStatus"))){
                                        deliveryCount++;
                                        boxCount += orderInfo.getJSONObject(i).getInt("boxQuantity");
                                    }else{

                                    }
                                }
                                aq.id(R.id.text_gs_delivery_count).text(deliveryCount + "건");
                                aq.id(R.id.text_gs_box_count).text(boxCount + "개");
                            }else{
                                aq.id(R.id.total_layout).gone();
                            }


                            if (orderData.size() > 0) {

                                aq.id(R.id.empty).gone();
                            } else {
                                aq.id(R.id.total_layout).gone();
                                aq.id(R.id.empty).visible();
                            }
                        }

                    } else {
                        DUtil.Log(Constants.PARTNER_ORDER_LIST, "initialize");
                    }
                    dismissDialog();
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    dismissDialog();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    dismissDialog();
                }
            }
        });
    }


    ProgressDialog mProgressDialog;
    GSOrderListViewAdapter orderAdapter;

    public void showDialog(){
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog(){
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

}
