package me.rewhite.delivery.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.kakao.kakaonavi.KakaoNaviParams;
import com.kakao.kakaonavi.KakaoNaviService;
import com.kakao.kakaonavi.Location;
import com.kakao.kakaonavi.NaviOptions;
import com.kakao.kakaonavi.options.CoordType;
import com.kakao.kakaonavi.options.RpOption;
import com.kakao.kakaonavi.options.VehicleType;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.Bootstrap;
import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.data.PickupType;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.StringUtil;
import me.rewhite.delivery.util.SystemUtil;
import me.rewhite.delivery.util.TimeUtil;
import me.rewhite.delivery.util.ToastUtility;

public class OrderDetailActivity extends BaseActivity {

    AQuery aq;
    Context ctx = this;

    JSONObject parentData;
    String userName;
    String profileImage;
    long pickupRequestTime;
    long deliveryRequestTime;
    long pickupCompleteTime;
    long deliveryCompleteTime;
    String addressName;
    String addressDetailName;
    String orderStatus;
    String orderPrice;
    int orderId;
    int userId;
    int addressSeq;
    double longitude;
    double latitude;
    private static final int PICKUP_COUNT = 7;
    private static final int PICKUP_ITEM = 8;
    private static final int PICKUP_REPAIR_ITEM = 9;
    private static final int SHOW_ITEM = 10;
    private static final int CREATE_ORDER = 11;
    private static final int ALERT_11ST_ADD_ITEM = 12;
    String orderRequest;
    JSONArray pickupItemsV2 = new JSONArray();
    String pickupItems;
    String pickupRepairItems;
    String isPayment;
    String phoneNo;
    String isDeliveryMan;
    int pickQuantity = 0;
    String orderSubType;
    String orderType;

    boolean isConfirmed = true;
    boolean isPaidConfirmed = true;

    ProgressDialog mProgressDialog;
    ImageOptions options;
    private String appDetailSettingString = "카메라를 사용할 권한이 없습니다. 앱 권한설정 화면으로 이동합니다.\n이동한 화면에서 '권한'메뉴를 선택하시고 전체 허용해주세요~";

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_SMS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE
    };
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final String[] SMS_PERMS={
            Manifest.permission.READ_SMS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE
    };
    private static final String[] CALL_PERMS={
            Manifest.permission.READ_SMS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = INITIAL_REQUEST + 3;
    private static final int SMS_REQUEST=INITIAL_REQUEST+4;
    private static final int REQUEST_PHONE_CALL = 123;

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            //Toast.makeText(OrderDetailActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            //Toast.makeText(OrderDetailActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();

            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(ctx);
            alertDialogBuilder.setCancelable(false).setTitle("앱 권한 설정하기").setMessage(appDetailSettingString)
                    .setPositiveButton("설정화면으로 가기", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SystemUtil.showApplicationDetailSetting(ctx);
                        }
                    }).setNegativeButton("취소", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }


    };

    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    .setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
            return;
        }

        switch (requestCode) {

            case SMS_REQUEST:
                if (canAccessSMS()) {
                    /*
                    TODO
                    Oreo버전 telephonyManager 대응
                     */
                    number_1 = telephonyManager.getLine1Number();
                    String phoneNumber = number_1.replace("+82", "0");
                    Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                    mPhoneNumber = MZValidator.validTelNumber(phoneNumber);
                }else {
                    Toast.makeText(this, "핸드폰번호를 알수 없습니다.", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
            case REQUEST_PHONE_CALL:
                if (canAccessCall()) {
                    number_1 = telephonyManager.getLine1Number();
                    String phoneNumber = number_1.replace("+82", "0");
                    Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                    mPhoneNumber = MZValidator.validTelNumber(phoneNumber);
                }else {
                    Toast.makeText(this, "핸드폰번호를 알수 없습니다.", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }
    private boolean canAccessSMS() {
        return (hasPermission(Manifest.permission.READ_SMS));
    }
    private boolean canAccessCall() {
        return(hasPermission(Manifest.permission.CALL_PHONE));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickup);

        telephonyManager =(TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);

        aq = new AQuery(this);

        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(appDetailSettingString)
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE, Manifest.permission.READ_SMS)
                .check();

        options = new ImageOptions();
        options.round = 89;
        options.ratio = 1.0f;
        options.memCache = true;
        options.targetWidth = 89;
        options.animation = AQuery.FADE_IN_NETWORK;
        options.preset = aq.getCachedImage(R.mipmap.kakao_default_profile_image);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                if (intent.getStringExtra("data") == null) {
                    finish();
                }else{
                    parentData = new JSONObject(intent.getStringExtra("data"));

                    orderId = parentData.getInt("orderId");
                    userId = parentData.getInt("userId");
                    addressSeq = parentData.getInt("addressSeq");

                    orderStatus = parentData.getString("orderStatus");
                    orderSubType = parentData.getString("orderSubType");
                    orderType = parentData.getString("orderType");

                    longitude = parentData.getDouble("longitude");
                    latitude = parentData.getDouble("latitude");

                    userName = parentData.getString("userName");

                    if(parentData.isNull("imageThumbPath")){
                        if("106".equals(orderSubType)){
                            aq.id(R.id.profile_image).image(R.mipmap.pro_11st_def);
                        }else{
                            aq.id(R.id.profile_image).image(R.mipmap.kakao_default_profile_image);
                        }
                    }else{
                        profileImage = parentData.getString("imageThumbPath");
                        if("".equals(profileImage) || "null".equals(profileImage)){
                            aq.id(R.id.profile_image).image(R.mipmap.kakao_default_profile_image);
                        }else{
                            aq.id(R.id.profile_image).image(profileImage, options);
                        }
                    }

                    if ("3".equals(orderType)) {
                        if ("301".equals(orderSubType)) {
                            phoneNo = parentData.getString("deliveryRequestMessage");
                            userName = parentData.getString("label");
                            aq.id(R.id.profile_name).text(userName).typeface(CommonUtility.getNanumBarunTypeface());
                        }
                    } else {
                        phoneNo = parentData.getString("userPhone");
                        aq.id(R.id.profile_name).text(userName).typeface(CommonUtility.getNanumBarunTypeface());
                    }

                    aq.id(R.id.btn_pickup_alert).clicked(this, "preAlertAction");
                    aq.id(R.id.btn_delivery_alert).clicked(this, "preAlertAction");

                    addressName = parentData.getString("address1");
                    addressDetailName = parentData.getString("address2");
                    String newAddressName = addressName + " " + addressDetailName;
                    if (!StringUtil.isNullOrEmpty(newAddressName)) {
                        aq.id(R.id.text_address).text(newAddressName).typeface(CommonUtility.getNanumBarunTypeface());
                    }

                    mProgressDialog = new ProgressDialog(OrderDetailActivity.this);
                    mProgressDialog.setMessage("잠시만 기다려주세요");
                    mProgressDialog.setCancelable(false);
                    mProgressDialog.show();
                    getRemoteData(orderId);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        aq.id(R.id.time_title).textColor(0xff999999);
        aq.id(R.id.req_title).textColor(0xff999999);
        aq.id(R.id.address_title).textColor(0xff999999);
        aq.id(R.id.count_title).textColor(0xff999999);
        aq.id(R.id.pickitem_title).textColor(0xff999999);
        //aq.id(R.id.pickitem_repair_title).textColor(0xffa0a0a0);
        aq.id(R.id.totalprice_title).textColor(0xff999999);
        // 11번가 상태표시 숨김처리
        aq.id(R.id.st11_preinput_layout).gone();
        aq.id(R.id.st11_payment_info).gone();
        aq.id(R.id.st11_add_price_info).gone();

        if ("101".equals(orderSubType)) {
            aq.id(R.id.btn_add).clicked(this, "addItemsAction").tag(1);
            aq.id(R.id.btn_mod).clicked(this, "addItemsAction").tag(1);
            aq.id(R.id.top_layout).backgroundColor(0xff5eaeff);
            aq.id(R.id.text_subtype).text("일반세탁");
            aq.id(R.id.icon_order_type).gone();

            aq.id(R.id.btn_find_location).backgroundColor(0xff007fff);

        } else if ("201".equals(orderSubType)) {
            aq.id(R.id.icon_order_type).gone();
        } else if ("105".equals(orderSubType)) {
            aq.id(R.id.btn_add).clicked(this, "addItemsAction").tag(3);
            aq.id(R.id.btn_mod).clicked(this, "addItemsAction").tag(3);
            aq.id(R.id.top_layout).backgroundColor(0xffff7272);
            aq.id(R.id.text_subtype).text("빠른세탁 당일");
            aq.id(R.id.icon_order_type).gone();

            aq.id(R.id.btn_find_location).backgroundColor(0xfffd7070);

        } else if ("104".equals(orderSubType)) {
            aq.id(R.id.btn_add).clicked(this, "addItemsAction").tag(4);
            aq.id(R.id.btn_mod).clicked(this, "addItemsAction").tag(4);
            aq.id(R.id.top_layout).backgroundColor(0xffff845e);
            aq.id(R.id.text_subtype).text("빠른세탁 익일");
            aq.id(R.id.icon_order_type).gone();

            aq.id(R.id.btn_find_location).backgroundColor(0xffff845e);
        } else if ("301".equals(orderSubType)) {
            aq.id(R.id.btn_add).clicked(this, "addItemsAction").tag(5);
            aq.id(R.id.btn_mod).clicked(this, "addItemsAction").tag(5);
            aq.id(R.id.top_layout).backgroundColor(0xffa957e8);
            aq.id(R.id.text_subtype).text("침구 제휴세탁");
            aq.id(R.id.icon_order_type).gone();

            aq.id(R.id.btn_find_location).backgroundColor(0xffa957e8);
        }else if ("106".equals(orderSubType)) {
            // 11번가 주문은 일반세탁주문 입력과 동일
            aq.id(R.id.btn_add).clicked(this, "addItemsAction").tag(1);
            aq.id(R.id.btn_mod).clicked(this, "addItemsAction").tag(1);
            aq.id(R.id.top_layout).backgroundColor(0xff5eaeff);
            aq.id(R.id.text_subtype).text("");

            aq.id(R.id.st11_preinput_layout).visible();
            aq.id(R.id.icon_order_type).image(R.mipmap.icon_ordertype_11st).visible();
            aq.id(R.id.btn_pickup_alert).image(R.drawable.btn_order_detail_alarm_11st);
            aq.id(R.id.btn_delivery_alert).image(R.drawable.btn_order_detail_alarm_11st);
            //
        }else if ("107".equals(orderSubType)) {
            aq.id(R.id.btn_add).clicked(this, "addItemsAction").tag(1);
            aq.id(R.id.btn_mod).clicked(this, "addItemsAction").tag(1);
            aq.id(R.id.top_layout).backgroundColor(0xff5eaeff);
            aq.id(R.id.text_subtype).text("일반세탁(전화)");
            aq.id(R.id.icon_order_type).gone();

            aq.id(R.id.btn_find_location).backgroundColor(0xff007fff);
        }else if ("151".equals(orderSubType)) {
            // GS주문
            aq.id(R.id.btn_add).clicked(this, "addItemsAction").tag(1);
            aq.id(R.id.btn_mod).clicked(this, "addItemsAction").tag(1);
            aq.id(R.id.top_layout).backgroundColor(0xff5eaeff);
            aq.id(R.id.text_subtype).text("일반세탁");
            aq.id(R.id.icon_order_type).gone();

            aq.id(R.id.btn_find_location).backgroundColor(0xff007fff);
        }else{
            aq.id(R.id.btn_add).clicked(this, "addItemsAction").tag(1);
            aq.id(R.id.btn_mod).clicked(this, "addItemsAction").tag(1);
            aq.id(R.id.top_layout).backgroundColor(0xff5eaeff);
            aq.id(R.id.text_subtype).text("일반세탁");
            aq.id(R.id.icon_order_type).gone();

            aq.id(R.id.btn_find_location).backgroundColor(0xff007fff);
        }


        aq.id(R.id.btn_count_add).clicked(this, "addItemsAction").tag(0);
        aq.id(R.id.btn_count_mod).clicked(this, "addItemsAction").tag(0);

        aq.id(R.id.btn_find_location).clicked(this, "findLocationMap");

        String orderAvailable = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_AVAILABLE_ORDER);
        try {
            // 수선가능여부 판단

            if ('1' == orderAvailable.charAt(2)) {
                Log.e("orderAvailble is equal", orderAvailable + "/" + orderAvailable.charAt(2));
                if ("301".equals(orderSubType)) {
                    // 핸디즈 제휴건은 수선버튼 노출방지
                    //aq.id(R.id.btn_repair_add).image(R.mipmap.btn_add_repair_item_disabled).enabled(false);
                } else {
                    //aq.id(R.id.btn_repair_add).visible().enabled(true);
                    //aq.id(R.id.btn_repair_add).clicked(this, "addItemsAction").tag(2);
                }

            } else {
                Log.e("orderAvailble not equal", orderAvailable + "/" + orderAvailable.charAt(2));
                //aq.id(R.id.btn_repair_add).image(R.mipmap.btn_add_repair_item_disabled).enabled(false);
            }
        } catch (NullPointerException e) {
            //Log.e("NullPointerException", e.getMessage());
            //aq.id(R.id.btn_repair_add).image(R.mipmap.btn_add_repair_item_disabled).enabled(false);
        }

        aq.id(R.id.btn_submit).clicked(this, "addCompleteAction");
        //aq.id(R.id.btn_confirm).clicked(this, "pickConfirmAction");
        //aq.id(R.id.btn_completed).clicked(this, "deliveryConfirmAction");
        aq.id(R.id.btn_delivery).clicked(this, "orderCompleteAction");
        aq.id(R.id.picks_layout).gone();
        //aq.id(R.id.repair_layout).gone();
        aq.id(R.id.total_layout).gone();
        aq.id(R.id.delivery_action_layout).gone();
        //aq.id(R.id.text_delivery_area).gone();

        aq.id(R.id.text_orderid).text("주문번호 " + orderId).typeface(CommonUtility.getNanumBarunTypeface());
        //
        aq.id(R.id.btn_back).clicked(this, "closeClicked");
        aq.id(R.id.btn_call).clicked(this, "callClicked");
        aq.id(R.id.btn_show).clicked(this, "showItemsAction");
        aq.id(R.id.btn_delivery_reorder).clicked(this, "reorderAction");

        aq.id(R.id.btn_change_p).clicked(this, "deliveryChange");
        aq.id(R.id.btn_change_d).clicked(this, "deliveryChange");

    }


    private static final int MOD_DELIVERY_OK = 2;
    private static final int MOD_PICKUP_OK = 3;

    public void deliveryChange(View button) {
        if ("01".equals(currentStatusCode) || "02".equals(currentStatusCode)) {
            // 수거일 변경
            Intent receiptIntent = new Intent(this, ModDeliveryInfoActivityV2.class);
            receiptIntent.setAction(Intent.ACTION_GET_CONTENT);
            receiptIntent.putExtra("orderId", String.valueOf(orderId));
            receiptIntent.putExtra("userId", userId);
            receiptIntent.putExtra("addressSeq", addressSeq);
            receiptIntent.putExtra("INFO", parentData.toString());
            receiptIntent.putExtra("MODE", "P");
            startActivityForResult(receiptIntent, MOD_PICKUP_OK);
        }else{
            // 배송일 변경
            Intent receiptIntent = new Intent(this, ModDeliveryInfoActivityV2.class);
            receiptIntent.setAction(Intent.ACTION_GET_CONTENT);
            receiptIntent.putExtra("orderId", String.valueOf(orderId));
            receiptIntent.putExtra("userId", userId);
            receiptIntent.putExtra("addressSeq", addressSeq);
            receiptIntent.putExtra("INFO", parentData.toString());
            receiptIntent.putExtra("MODE", "D");
            startActivityForResult(receiptIntent, MOD_DELIVERY_OK);
        }

    }

    boolean isGPSEnabled;
    boolean isNetworkEnabled;
    boolean locationServiceAvailable;

    @Override
    protected void onDestroy() {
        Log.d("onDestroy", "called onDestroy");
        if (mProgressDialog != null){
            mProgressDialog.dismiss();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    String isReceivable;

    public void findLocationMap(View button) {
        Log.e("findLocation", userName + " / " + latitude + " / "+ longitude);


        if ( Build.VERSION.SDK_INT >= 23){
            if (!canAccessLocation()) {
                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
            }else{

//                TMapTapi tmaptapi = new TMapTapi(this);
//                tmaptapi.setSKPMapAuthentication (Constants.SK_APP_KEY);
//                tmaptapi.invokeRoute(userName, (float)longitude, (float)latitude);

                // KAKAO
                if(KakaoNaviService.getInstance().isKakaoNaviInstalled(ctx)){
                    Location destination = Location.newBuilder(userName, longitude, latitude).build();
                    NaviOptions options = NaviOptions.newBuilder().setCoordType(CoordType.WGS84).setVehicleType(VehicleType.FIRST).setRpOption(RpOption.FAST).build();

                    KakaoNaviParams.Builder builder = KakaoNaviParams.newBuilder(destination)
                            .setNaviOptions(options);

                    KakaoNaviService.getInstance().shareDestination(OrderDetailActivity.this, builder.build());
                }else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setCancelable(true).setMessage("카카오네비가 설치되어있지 않습니다. 설치하러 이동하시겠습니까?")
                            .setPositiveButton("설치하러 이동", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String name="https://play.google.com/store/apps/details?id=com.locnall.KimGiSa";
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(name));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.setPackage("com.android.vending");
                                    startActivity(intent);
                                }

                            }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            }
        }else{
//            TMapTapi tmaptapi = new TMapTapi(this);
//            tmaptapi.setSKPMapAuthentication (Constants.SK_APP_KEY);
//            tmaptapi.invokeRoute(userName, (float)longitude, (float)latitude);

            // KAKAO
            if(KakaoNaviService.getInstance().isKakaoNaviInstalled(ctx)){
                Location destination = Location.newBuilder(userName, longitude, latitude).build();
                NaviOptions options = NaviOptions.newBuilder().setCoordType(CoordType.WGS84).setVehicleType(VehicleType.FIRST).setRpOption(RpOption.FAST).build();

                KakaoNaviParams.Builder builder = KakaoNaviParams.newBuilder(destination)
                        .setNaviOptions(options);

                KakaoNaviService.getInstance().shareDestination(OrderDetailActivity.this, builder.build());
            }else{
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setCancelable(true).setMessage("카카오네비가 설치되어있지 않습니다. 설치하러 이동하시겠습니까?")
                        .setPositiveButton("설치하러 이동", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String name="https://play.google.com/store/apps/details?id=com.locnall.KimGiSa";
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(name));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setPackage("com.android.vending");
                                startActivity(intent);
                            }

                        }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        }

    }


    private String getStatusText(String _statusCode) throws JSONException {
        String tempText = "";
        if ("01".equals(_statusCode) || "02".equals(_statusCode)) {
            String dps = displayTimeString(parentData.getLong("pickupRequestTimeApp"));
            tempText = dps + " 수거";
            //aq.id(R.id.btn_complete_title).text("수거수량 입력완료");
        } else if ("03".equals(_statusCode)) {
            tempText = "검품 후 인수증 발급 예정";

        }else if ("11".equals(_statusCode) || "12".equals(_statusCode) || "13".equals(_statusCode)) {
            if ("Y".equals(isPayment)) {
                String dps = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
                tempText = dps + " 배송";
            }else{
                tempText = "인수증 확인 후 결제요청";
            }
        } else if ("21".equals(_statusCode) || "22".equals(_statusCode)) {
            String dps = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
            if ("Y".equals(isPayment)) {
                tempText = dps + " 배송";
            }else{
                tempText = dps + " 배송(결제요청)";
            }
        } else if ("23".equals(_statusCode) || "24".equals(_statusCode)) {
            tempText = "우리동네 배달세탁소";
        } else {
            tempText = "우리동네 배달세탁소";
        }
        return tempText;
    }

    TelephonyManager telephonyManager;
    String number_1 = "";
    String mPhoneNumber = "";

    public void callClicked(View button) {

        try{
            if ( Build.VERSION.SDK_INT >= 23){
                if (!canAccessCall()) {
                    requestPermissions(CALL_PERMS, REQUEST_PHONE_CALL);
                }else{
                    number_1 = telephonyManager.getLine1Number();
                    String phoneNumber = number_1.replace("+82", "0");
                    Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                    mPhoneNumber = MZValidator.validTelNumber(phoneNumber);
                }
            }else{
                number_1 = telephonyManager.getLine1Number();
                String phoneNumber = number_1.replace("+82", "0");
                Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                mPhoneNumber = MZValidator.validTelNumber(phoneNumber);
            }
            //phoneNo
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setCancelable(true).setMessage(userName + "님("+phoneNo+")과 전화통화를 원하십니까?")
                    .setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            RequestParams params = new RequestParams();
                            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                            params.put("userId", userId);
                            params.put("callerMdn", mPhoneNumber.replace("-", ""));
                            params.put("calleeMdn", phoneNo.replace("-", ""));
                            //params.put("originCallerNumber", orderId);
                            try {
                                params.put("appUrl", "rewhiteMeUserOrder://orderDetail?orderId="+orderId);
                                params.put("title", "[리화이트] " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_NAME) + "입니다");
                                params.put("text", getStatusText(orderStatus));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            params.put("k", 1);

                            DUtil.Log(Constants.SEND_T_PHONE + " req", params.toString());

                            NetworkClient.post(Constants.SEND_T_PHONE, params, new AsyncHttpResponseHandler() {

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                                    // TODO Auto-generated method stub
                                    DUtil.Log(Constants.SEND_T_PHONE, error.getMessage());

                                }

                                @Override
                                public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                                    String result;
                                    try {
                                        result = new String(data, "UTF-8");
                                        DUtil.Log(Constants.SEND_T_PHONE, result);

                                        JSONObject jsondata = new JSONObject(result);

                                        if ("S0000".equals(jsondata.getString("resultCode"))) {
                                            if ( Build.VERSION.SDK_INT >= 23){
                                                if (!canAccessSMS()) {
                                                    requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
                                                }else{
                                                    call(true);
                                                }
                                            }else{
                                                call(true);
                                            }

                                        } else {
                                            if ( Build.VERSION.SDK_INT >= 23){
                                                if (!canAccessSMS()) {
                                                    requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
                                                }else{
                                                    call(true);
                                                }
                                            }else{
                                                call(true);
                                            }

                                        }
                                    } catch (UnsupportedEncodingException e1) {
                                        // TODO Auto-generated catch block
                                        e1.printStackTrace();
                                    } catch (JSONException e1) {
                                        // TODO Auto-generated catch block
                                        e1.printStackTrace();
                                    }
                                }
                            });


                        }
                    }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }catch(Exception ex){
            ex.printStackTrace();
        }

    }

    public void call(boolean _isTPhone) {


        phoneNo = phoneNo.replace("-", "");
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        //callIntent.setAction("android.intent.action.DIAL");
        callIntent.setData(Uri.parse("tel:" + phoneNo));
        if(callIntent.resolveActivity(getPackageManager()) != null){
            startActivity(callIntent);
        }
//
//
//        if(!_isTPhone){
//            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//            alertDialogBuilder.setCancelable(true)
//                    .setMessage(userName + "님("+MZValidator.validTelNumber(phoneNo)+")과 전화통화를 원하십니까?")
//                    .setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {
//
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            Intent callIntent = new Intent(Intent.ACTION_CALL);
//                            //callIntent.setAction("android.intent.action.DIAL");
//                            callIntent.setData(Uri.parse("tel:" + phoneNo));
//                            if(callIntent.resolveActivity(getPackageManager()) != null){
//                                startActivity(callIntent);
//                            }
//                        }})
//                    .setNegativeButton("아니오", new DialogInterface.OnClickListener() {
//
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//
//                        }
//                    });
//            AlertDialog alertDialog = alertDialogBuilder.create();
//            alertDialog.show();
//        }else{
//            Intent callIntent = new Intent(Intent.ACTION_CALL);
//            //callIntent.setAction("android.intent.action.DIAL");
//            callIntent.setData(Uri.parse("tel:" + phoneNo));
//            if(callIntent.resolveActivity(getPackageManager()) != null){
//                startActivity(callIntent);
//            }
//        }

    }

    public void showDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }

        mProgressDialog = new ProgressDialog(OrderDetailActivity.this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }

    public void closeClicked(View button) {
        finish();
    }

    private JSONArray getPickupItem(JSONArray originData) {
        JSONArray result = new JSONArray();
        try {
            for (int i = 0; i < originData.length(); i++) {
                if ("G".equals(originData.getJSONObject(i).getString("pickupType")) || "T".equals(originData.getJSONObject(i).getString("pickupType")) || "N".equals(originData.getJSONObject(i).getString("pickupType"))) {
                    result.put(originData.getJSONObject(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private JSONArray getRepairItem(JSONArray originData) {
        JSONArray result = new JSONArray();
        try {
            for (int i = 0; i < originData.length(); i++) {
                if ("R".equals(originData.getJSONObject(i).getString("pickupType"))) {
                    result.put(originData.getJSONObject(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }



    private void getRemoteData(int _orderId) {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_DETAIL, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        parentData = jsondata.getJSONObject("data");

                        int addressOrderCount = parentData.getInt("addressOrderCount");
                        int addressOrderQuantity = parentData.getInt("addressOrderQuantity");

                        int deliveryPrice = jsondata.getJSONObject("data").getInt("deliveryPrice");

                        if (deliveryPrice < 20000) {
                            deliveryPrice = deliveryPrice + 2000;
                            aq.id(R.id.delivery_display_text).visible();
                        }
                        orderPrice = MZValidator.toNumFormat(deliveryPrice);

                        if ("301".equals(orderSubType)) {
                            //aq.id(R.id.b2b_layout).visible();
                            if (addressOrderCount == 0) {
                                aq.id(R.id.text_address_order_count).text("배송해야할 침구세트가 없습니다.");
                            } else {
                                aq.id(R.id.text_address_order_count).text(addressOrderQuantity + " 개");
                            }
                        }else if("106".equals(orderSubType)){
                            // 11번가 선결제주문 정보
                            if(parentData.isNull("pickupItemsPartnerV2")){
                                aq.id(R.id.text_11st_count).text("0개");
                            }else{
                                if(!parentData.isNull("pickupItemsPartnerV2")){
                                    JSONArray pickupItemsPartnerV2Array = parentData.getJSONArray("pickupItemsPartnerV2");
                                    JSONArray partner11stPayment;
                                    if(!parentData.isNull("payments")){
                                        partner11stPayment = parentData.getJSONArray("payments");
                                    }else{
                                        partner11stPayment = new JSONArray();
                                    }

                                    int prepaid = 0;
                                    for(int i = 0 ; i < partner11stPayment.length(); i++){
                                        if("P51".equals(partner11stPayment.getJSONObject(i).getString("paymentType"))){
                                            prepaid = partner11stPayment.getJSONObject(i).getInt("paymentPrice");
                                            aq.id(R.id.text_11st_price).text(MZValidator.toNumFormat(prepaid) + " 원");
                                        }
                                    }
                                    int orderQuantity = 0;
                                    int addSeq = 0;
                                    String pickupItemProductNames = "";
                                    for(int i = 0 ; i < pickupItemsPartnerV2Array.length(); i++){
                                        // 1000원 추가되는거 체크
//                                        if(!"0".equals(pickupItemsPartnerV2Array.getJSONObject(i).getString("pAdditionalProductNo"))){
//                                            orderQuantity += pickupItemsPartnerV2Array.getJSONObject(i).getInt("pOrderQuantity");
//                                            if(addSeq != 0){
//                                                pickupItemProductNames += ", ";
//                                            }
//                                            pickupItemProductNames += pickupItemsPartnerV2Array.getJSONObject(i).getString("pProductName") + " " + pickupItemsPartnerV2Array.getJSONObject(i).getString("pOrderQuantity") + "개";
//                                            addSeq++;
//                                        }
                                        orderQuantity += pickupItemsPartnerV2Array.getJSONObject(i).getInt("pOrderQuantity");
                                        if(addSeq != 0){
                                            pickupItemProductNames += ", ";
                                        }
                                        String pOptionName = pickupItemsPartnerV2Array.getJSONObject(i).getString("pProductOptionName");
                                        pOptionName = pOptionName.replace("옵션명 1:","");
                                        pOptionName = pOptionName.split(" - ")[0];
                                        pickupItemProductNames += pOptionName + " " + pickupItemsPartnerV2Array.getJSONObject(i).getString("pOrderQuantity") + "개";
                                        addSeq++;
                                    }

                                    aq.id(R.id.text_11st_count).text(orderQuantity + " 개");
                                    if(pickupItemsPartnerV2Array.length() > 0){
                                        aq.id(R.id.text_11st_product).text(pickupItemProductNames);
                                    }else{
                                        aq.id(R.id.text_11st_product).text("내역이 없습니다.");
                                    }


                                    aq.id(R.id.paid_image).gone();
                                    //int deliveryPrice = parentData.getInt("deliveryPrice");
                                    if(deliveryPrice - prepaid > 0){
                                        // 추가금액 발생
                                        if("Y".equals(parentData.getString("isAdditionalPayment"))){
                                            // 추가결제 완료
                                            aq.id(R.id.st11_payment_info).gone();
                                            aq.id(R.id.st11_add_price_info).gone();
                                            aq.id(R.id.paid_image).visible();
                                        }else{
                                            // 추가결제 미완료
                                            aq.id(R.id.st11_payment_info).visible();
                                            aq.id(R.id.st11_paid_price).text(MZValidator.toNumFormat(prepaid) + " 원");
                                            aq.id(R.id.st11_reqpay_text).text("미결제 금액");
                                            aq.id(R.id.st11_reqpay_price).text(MZValidator.toNumFormat(deliveryPrice - prepaid) + " 원");

                                            aq.id(R.id.st11_add_price_info).visible();
                                            aq.id(R.id.st11_addpay_text).text("배송 시 미결제 금액 "+ MZValidator.toNumFormat(deliveryPrice - prepaid) +"원을 \n" +
                                                    "꼭 현금으로 받아주세요.");
                                        }

                                    }else if(deliveryPrice - prepaid == 0){
                                        // 금액 동일
                                        aq.id(R.id.st11_payment_info).gone();
                                        aq.id(R.id.st11_add_price_info).gone();
                                        aq.id(R.id.paid_image).visible();
                                    }else{
                                        // 환불금액 발생
                                        aq.id(R.id.st11_payment_info).visible();
                                        aq.id(R.id.st11_paid_price).text(MZValidator.toNumFormat(prepaid) + " 원");
                                        aq.id(R.id.st11_reqpay_text).text("환불예정 금액");
                                        aq.id(R.id.st11_reqpay_price).text(MZValidator.toNumFormat(prepaid - deliveryPrice) + " 원");

                                        aq.id(R.id.st11_add_price_info).visible();
                                        aq.id(R.id.st11_addpay_text).text("환불 예정금액 "+MZValidator.toNumFormat(prepaid - deliveryPrice)+"원이 있어서 \n" +
                                                "리화이트에서 연락드릴 예정이라고 꼭 말씀해주세요.");
                                        aq.id(R.id.text_11st_desc_02).gone();
                                    }


                                }else{
                                    aq.id(R.id.st11_preinput_layout).gone();
                                    ToastUtility.show(ctx, "11번가 주문데이터가 비정상입니다.", Toast.LENGTH_LONG);
                                }
                            }

                        }else {
                            //aq.id(R.id.b2b_layout).gone();
                        }

                        isReceivable = jsondata.getJSONObject("data").getString("isReceivable");
                        isDeliveryMan = jsondata.getJSONObject("data").getString("isDeliveryMan");
//                        if (!jsondata.getJSONObject("data").isNull("storeOrderNo")) {
//                            String innerCode = jsondata.getJSONObject("data").getString("storeOrderNo");
//                            if (StringUtil.isNullOrEmpty(innerCode) || innerCode == null) {
//                                aq.id(R.id.text_innercode).text("");
//                            } else {
//                                aq.id(R.id.text_innercode).text("" + innerCode);
//                            }
//
//                        }

                        isPayment = jsondata.getJSONObject("data").getString("isPayment");

                        pickQuantity = jsondata.getJSONObject("data").getInt("pickupQuantity");
                        aq.id(R.id.item_count_text).text("총 " + pickQuantity + "개");

                        pickupItems = getPickupItem(jsondata.getJSONObject("data").getJSONArray("pickupItems")).toString();
                        pickupRepairItems = getRepairItem(jsondata.getJSONObject("data").getJSONArray("pickupItems")).toString();

                        aq.id(R.id.text_total_price).text(orderPrice);
                        JSONArray history = jsondata.getJSONObject("data").getJSONArray("history");
                        for (int i = 0; i < history.length(); i++) {
                            if ("03".equals(history.getJSONObject(i).getString("orderStatus")) || "11".equals(history.getJSONObject(i).getString("orderStatus"))) {
                                pickupCompleteTime = history.getJSONObject(i).getLong("statusDateApp");
                            }
                            if ("23".equals(history.getJSONObject(i).getString("orderStatus"))) {
                                deliveryCompleteTime = history.getJSONObject(i).getLong("statusDateApp");
                            }
                        }
                        orderStatus = jsondata.getJSONObject("data").getString("orderStatus");


                        // 세탁목록
                        JSONArray arr = getPickupItem(jsondata.getJSONObject("data").getJSONArray("pickupItems"));
                        String picksText = "";
                        int count = 0;
                        for (int i = 0; i < arr.length(); i++) {
                            if ("N".equals(arr.getJSONObject(i).getString("isOption"))) {
                                picksText += arr.getJSONObject(i).getString("itemTitle") + " x" + arr.getJSONObject(i).getInt("itemQuantity");
                                count += arr.getJSONObject(i).getInt("itemQuantity");
                                if (i == arr.length() - 1) {
                                    picksText += "\n총 " + count + "개";
                                } else {
                                    picksText += ", ";
                                }
                            }
                        }
                        //수선목록
                        JSONArray arr2 = getRepairItem(jsondata.getJSONObject("data").getJSONArray("pickupItems"));
                        String picksRepairText = "";
                        int count2 = 0;
                        for (int i = 0; i < arr2.length(); i++) {
                            if ("N".equals(arr2.getJSONObject(i).getString("isOption"))) {
                                picksRepairText += arr2.getJSONObject(i).getString("itemTitle") + " x" + arr2.getJSONObject(i).getInt("itemQuantity");
                                count2 += arr2.getJSONObject(i).getInt("itemQuantity");
                                if (i == arr2.length() - 1) {
                                    picksRepairText += "\n총 " + count2 + "건";
                                } else {
                                    picksRepairText += ", ";
                                }
                            }
                        }

                        picksText += picksRepairText;

                        Log.e("clean value", arr.length() + " / " + picksText);

                        pickupItemsV2 = jsondata.getJSONObject("data").getJSONArray("pickupItemsV2");
                        if(arr.length() == 0 && pickupItemsV2.length() > 0){
                            count = pickupItemsV2.length();
                            if(count == 1){
                                picksText = pickupItemsV2.getJSONObject(0).getString("itemTitle") + " " +count + "개";
                            }else{
                                picksText = pickupItemsV2.getJSONObject(0).getString("itemTitle") + " 등 " +count + "개";
                            }

                        }

                        Log.e("clean value v2", pickupItemsV2.length() + " / " + picksText);

                        if(!jsondata.getJSONObject("data").isNull("payments")){
                            payments = jsondata.getJSONObject("data").getJSONArray("payments");
                        }else{
                            payments = new JSONArray();
                        }

                        Log.e("paidInfo", orderStatus + " / " + isReceivable + " / " + isPayment + " / " + payments.toString() + " / " + paymentType);
                        if ("23".equals(orderStatus) || "24".equals(orderStatus)) {

                            if ("Y".equals(isPayment)) {
                                if (payments.length() > 0) {
                                    paymentType = payments.getJSONObject(0).getString("paymentType");
                                    if ("P91".equals(paymentType)) {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_paycomp_face_card);
                                    } else if ("P92".equals(paymentType)) {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_paycomp_face_cash);
                                    } else {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_paycomp_mobile);
                                    }
                                } else {
                                    aq.id(R.id.paid_image).image(R.mipmap.paidinfo_not_yet);
                                }
                            }
                        }else if ("22".equals(orderStatus)) {

                            if("Y".equals(isReceivable)){
                                aq.id(R.id.paid_image).image(R.mipmap.paidinfo_paycomp_unpaid);
                            }else{
                                if ("Y".equals(isPayment)) {
                                    if (payments.length() > 0) {
                                        paymentType = payments.getJSONObject(0).getString("paymentType");
                                        if ("P91".equals(paymentType)) {
                                            aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_card);
                                        } else if ("P92".equals(paymentType)) {
                                            aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_cash);
                                        } else {
                                            aq.id(R.id.paid_image).image(R.mipmap.paidinfo_already);
                                        }
                                    }else {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_not_yet);
                                    }
                                }

                            }
                        }else{
                            if ("Y".equals(isPayment)) {

                                if (payments.length() > 0) {
                                    paymentType = payments.getJSONObject(0).getString("paymentType");
                                    if ("P91".equals(paymentType)) {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_card);
                                    } else if ("P92".equals(paymentType)) {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_cash);
                                    } else {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_already);
                                    }
                                } else {
                                    aq.id(R.id.paid_info_layout).gone();
                                }
                            } else {
                                payments = null;
                                aq.id(R.id.paid_info_layout).gone();
                            }
                        }


                        aq.id(R.id.text_items).text(picksText);
                        //aq.id(R.id.text_repair_items).text(picksRepairText);

                        if (arr.length() > 0 || arr2.length() > 0) {
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.btn_show).gone();
                        }else if(pickupItemsV2.length() > 0){
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.btn_show).visible();
                        } else {
                            aq.id(R.id.picks_layout).gone();
                        }
                        if (count2 > 0) {
                            //aq.id(R.id.repair_layout).visible();
                        } else {
                            //aq.id(R.id.repair_layout).gone();
                        }
                    } else {

                    }

                    setStatusScreen(orderStatus);
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    JSONArray payments;
    String paymentType;
    String currentStatusCode = null;

    private String displayTimeString(long time) {
        //Date dt = new Date();
        //dt.setTime(time);
        if (SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP) == null) {
            SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.DELIVERY_GAP, "1");
        }
        return TimeUtil.getDateByMDSH(time, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP));
    }

    private void setStatusScreen(String _statusCode) throws JSONException {
        currentStatusCode = _statusCode;
        //showDialog();
        //aq.id(R.id.delivery_action_layout).gone();
        //aq.id(R.id.pick_action_layout).gone();
        aq.id(R.id.pickup_alert_layout).gone();
        aq.id(R.id.paid_info_layout).gone();
        aq.id(R.id.below_layout).visible();
        aq.id(R.id.btn_change_p).gone();
        aq.id(R.id.btn_change_d).gone();
        aq.id(R.id.btn_count_mod).gone();
        aq.id(R.id.req_message_text).text(parentData.getString("pickupRequestMessage")).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.b2b_layout).gone();
        aq.id(R.id.btn_show).gone();

        if ("01".equals(_statusCode)) {
            aq.id(R.id.text_status).text("수거접수");
            aq.id(R.id.pickup_alert_layout).visible();
            // 수거일변경 버튼 활성화
            aq.id(R.id.btn_change_p).visible();
            aq.id(R.id.btn_change_d).visible();

            // 수거요청 정보추가 16-12-02
            //aq.id(R.id.below_layout).gone();
            aq.id(R.id.pick_action_layout).gone();
            aq.id(R.id.item_count_layout).gone();
            aq.id(R.id.add_area).gone();
            aq.id(R.id.count_add_area).gone();

            //aq.id(R.id.time_title).text("수거희망시간");
            String dps_p = displayTimeString(parentData.getLong("pickupRequestTimeApp"));
            String dps_d = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
            aq.id(R.id.time_content_p).text(dps_p).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.time_content_d).text(dps_d).typeface(CommonUtility.getNanumBarunTypeface());

            aq.id(R.id.item_count_layout).gone();

            if ("301".equals(orderSubType)) {
                aq.id(R.id.b2b_layout).visible();
            } else {
                aq.id(R.id.b2b_layout).gone();
            }
        } else if ("02".equals(_statusCode) || "03".equals(_statusCode)) {
            if ("02".equals(_statusCode)) {
                aq.id(R.id.text_status).text("수거하러 이동중");
                //aq.id(R.id.time_title).text("수거희망시간");
                // 수거일변경 버튼 활성화
                aq.id(R.id.btn_change_p).visible();
                aq.id(R.id.btn_change_d).visible();

                String dps_p = displayTimeString(parentData.getLong("pickupRequestTimeApp"));
                String dps_d = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
                aq.id(R.id.time_content_p).text(dps_p).typeface(CommonUtility.getNanumBarunTypeface());
                aq.id(R.id.time_content_d).text(dps_d).typeface(CommonUtility.getNanumBarunTypeface());

                if ("Y".equals(isDeliveryMan)) {
                    aq.id(R.id.add_area).gone();
                    aq.id(R.id.count_add_area).visible();
                } else {
                    aq.id(R.id.add_area).visible();
                    aq.id(R.id.count_add_area).gone();
                }
                aq.id(R.id.pick_action_layout).gone();

                aq.id(R.id.item_count_layout).gone();
                aq.id(R.id.btn_count_mod).visible();

                if ("301".equals(orderSubType)) {
                    aq.id(R.id.b2b_layout).visible();
                } else {
                    aq.id(R.id.b2b_layout).gone();
                }
                aq.id(R.id.btn_complete_title).text("수량 등록완료");
            } else if ("03".equals(_statusCode)) {
                aq.id(R.id.text_status).text("수거항목을 자세히 등록해주세요.");
                aq.id(R.id.time_title_p).text("수거완료시간");
                String dps_p = displayTimeString(pickupCompleteTime);
                String dps_d = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
                aq.id(R.id.time_content_p).text(dps_p).typeface(CommonUtility.getNanumBarunTypeface());
                aq.id(R.id.time_content_d).text(dps_d).typeface(CommonUtility.getNanumBarunTypeface());
                aq.id(R.id.add_area).visible();
                aq.id(R.id.count_add_area).gone();
                aq.id(R.id.btn_count_mod).gone();

                JSONArray count1 = new JSONArray(pickupItems);
                JSONArray count2 = new JSONArray(pickupRepairItems);
                JSONArray countV2 = pickupItemsV2;

                if (count1.length() > 0 || count2.length() > 0) {
                    aq.id(R.id.pick_action_layout).visible();
                }else if(countV2.length() > 0){
                    aq.id(R.id.pick_action_layout).visible();
                } else {
                    aq.id(R.id.pick_action_layout).gone();
                }


                //aq.id(R.id.inner_code_layout).visible();
                aq.id(R.id.btn_complete_title).text("수거항목 등록완료");
            }

            aq.id(R.id.alert_layout).gone();

        } else if ("11".equals(_statusCode) || "12".equals(_statusCode) || "13".equals(_statusCode)) {
            aq.id(R.id.pre_alert_area).gone();
            aq.id(R.id.add_area).gone();

            aq.id(R.id.count_add_area).gone();
            aq.id(R.id.add_area).gone();
            aq.id(R.id.btn_show).visible();
            aq.id(R.id.btn_mod).gone();

            aq.id(R.id.below_layout).visible();

            if ("11".equals(_statusCode)) {
                aq.id(R.id.text_status).text("세탁 준비중");
            } else if ("12".equals(_statusCode)) {
                aq.id(R.id.text_status).text("세탁 중");
            } else if ("13".equals(_statusCode)) {
                aq.id(R.id.text_status).text("세탁완료");
            }
            aq.id(R.id.pickup_alert_layout).gone();
            aq.id(R.id.delivery_alert_layout).gone();
            aq.id(R.id.paid_info_layout).visible();
            aq.id(R.id.delivery_action_layout).gone();
/*
            aq.id(R.id.text_pickup_time_title).text("수거완료시간").typeface(CommonUtility.getNanumBarunTypeface());
            String dps = displayTimeString(pickupCompleteTime);
            aq.id(R.id.time_content).text(dps).typeface(CommonUtility.getNanumBarunLightTypeface());*/
            //
            //aq.id(R.id.time_title).text("배송희망시간");
            aq.id(R.id.time_title_p).text("수거완료시간");
            String dps_p = displayTimeString(pickupCompleteTime);
            String dps_d = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
            aq.id(R.id.time_content_p).text(dps_p).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.time_content_d).text(dps_d).typeface(CommonUtility.getNanumBarunTypeface());
            //

            aq.id(R.id.pick_action_layout).gone();

            JSONArray count1 = new JSONArray(pickupItems);
            JSONArray count2 = new JSONArray(pickupRepairItems);

            JSONArray countV2 = pickupItemsV2;

            if (count1.length() > 0 || count2.length() > 0) {
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).gone();
            }else if(pickupItemsV2.length() > 0){
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).visible();
            } else {
                aq.id(R.id.picks_layout).gone();
            }
            aq.id(R.id.total_layout).visible();
            //aq.id(R.id.text_delivery_area).visible();
        } else if ("21".equals(_statusCode) || "22".equals(_statusCode)) {
            aq.id(R.id.count_add_area).gone();
            aq.id(R.id.add_area).gone();
            aq.id(R.id.btn_show).visible();
            aq.id(R.id.btn_mod).gone();
            aq.id(R.id.paid_info_layout).visible();


            if ("21".equals(_statusCode)) {
                aq.id(R.id.text_status).text("배송 준비중");
                aq.id(R.id.pickup_alert_layout).gone();
                aq.id(R.id.delivery_alert_layout).visible();
                aq.id(R.id.delivery_action_layout).gone();

                aq.id(R.id.below_layout).visible();

                //aq.id(R.id.btn_change_p).visible();
                aq.id(R.id.btn_change_d).visible();
                // 주소표시하기로 함.
                aq.id(R.id.loc_layout).visible();
                aq.id(R.id.item_count_layout).gone();
                aq.id(R.id.prod_layout).visible();

                //aq.id(R.id.inner_code_layout).gone();
                //aq.id(R.id.pick_action_layout).gone();
                //aq.id(R.id.delivery_action_layout).visible();


            } else if ("22".equals(_statusCode)) {
                if("Y".equals(parentData.getString("isReceivable"))){
                    aq.id(R.id.text_status).text("배송완료 지연(미수금)");
                    aq.id(R.id.paid_image).image(R.mipmap.paidinfo_paycomp_unpaid);
                    aq.id(R.id.delivery_action_layout).gone();
                    aq.id(R.id.btn_change_p).gone();
                    aq.id(R.id.btn_change_d).gone();
                }else {
                    aq.id(R.id.text_status).text("배송 중");
                    aq.id(R.id.delivery_action_layout).visible();
                    if ("106".equals(orderSubType)){
                        aq.id(R.id.reorder_layout).gone();
                        //aq.id(R.id.order_delivery_comp_layout)
                    }else{
                        aq.id(R.id.reorder_layout).visible();
                    }
                    //aq.id(R.id.btn_change_p).visible();
                    aq.id(R.id.btn_change_d).visible();
                }

                aq.id(R.id.pickup_alert_layout).gone();
                aq.id(R.id.delivery_alert_layout).gone();
                aq.id(R.id.below_layout).visible();

                aq.id(R.id.loc_layout).visible();
                aq.id(R.id.item_count_layout).visible();
                aq.id(R.id.prod_layout).visible();
                //aq.id(R.id.inner_code_layout).gone();
                //aq.id(R.id.pick_action_layout).gone();
            }

            //aq.id(R.id.time_title).text("배송희망시간");
            //String dds = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
            //aq.id(R.id.time_content).text(dds).typeface(CommonUtility.getNanumBarunTypeface());

            aq.id(R.id.time_title_p).text("수거완료시간");
            String dps_p = displayTimeString(pickupCompleteTime);
            String dps_d = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
            aq.id(R.id.time_content_p).text(dps_p).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.time_content_d).text(dps_d).typeface(CommonUtility.getNanumBarunTypeface());
            //
            aq.id(R.id.add_area).gone();
            aq.id(R.id.pick_action_layout).gone();


            JSONArray count1 = new JSONArray(pickupItems);
            JSONArray count2 = new JSONArray(pickupRepairItems);
            JSONArray countV2 = pickupItemsV2;

            if (count1.length() > 0 || count2.length() > 0) {
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).gone();
            }else if(pickupItemsV2.length() > 0){
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).visible();
            } else {
                aq.id(R.id.picks_layout).gone();
            }
            //aq.id(R.id.picks_layout).visible();
            aq.id(R.id.total_layout).visible();

            //aq.id(R.id.text_delivery_area).visible();
        } else if ("23".equals(_statusCode) || "24".equals(_statusCode)) {
            aq.id(R.id.count_add_area).gone();
            aq.id(R.id.add_area).gone();
            aq.id(R.id.btn_show).visible();
            aq.id(R.id.btn_mod).gone();
            aq.id(R.id.paid_info_layout).visible();

            if ("23".equals(_statusCode)) {
                aq.id(R.id.text_status).text("배송완료");
            } else if ("24".equals(_statusCode)) {
                aq.id(R.id.text_status).text("방문 수령 완료");
            }
            aq.id(R.id.paid_info_layout).visible();
            aq.id(R.id.pre_alert_area).gone();
            /*
            aq.id(R.id.text_pickup_time_title).text("수거완료시간").typeface(CommonUtility.getNanumBarunTypeface());
            String dps = displayTimeString(pickupCompleteTime);
            aq.id(R.id.text_pickup_time).text(dps).typeface(CommonUtility.getNanumBarunLightTypeface());*/

            //String dds = displayTimeString(deliveryCompleteTime);
            //aq.id(R.id.time_content).text(dds).typeface(CommonUtility.getNanumBarunTypeface());

            aq.id(R.id.time_title_p).text("수거완료시간");
            aq.id(R.id.time_title_d).text("배송완료시간");
            String dps_p = displayTimeString(pickupCompleteTime);
            String dps_d = displayTimeString(deliveryCompleteTime);
            aq.id(R.id.time_content_p).text(dps_p).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.time_content_d).text(dps_d).typeface(CommonUtility.getNanumBarunTypeface());
            //
            aq.id(R.id.add_area).gone();
            JSONArray count1 = new JSONArray(pickupItems);
            JSONArray count2 = new JSONArray(pickupRepairItems);
            JSONArray countV2 = pickupItemsV2;

            if (count1.length() > 0 || count2.length() > 0) {
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).gone();
            }else if(pickupItemsV2.length() > 0){
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).visible();
            } else {
                aq.id(R.id.picks_layout).gone();
            }

            aq.id(R.id.pick_action_layout).gone();
            aq.id(R.id.total_layout).visible();
            //aq.id(R.id.text_delivery_area).visible();
            //aq.id(R.id.delivery_action_layout).visible();

            if ("106".equals(orderSubType)){
                aq.id(R.id.delivery_action_layout).gone();
                aq.id(R.id.reorder_layout).gone();
                //aq.id(R.id.order_delivery_comp_layout)
            }else{
                aq.id(R.id.delivery_action_layout).visible();
                aq.id(R.id.reorder_layout).visible();
                aq.id(R.id.order_delivery_comp_layout).gone();
            }

        } else {
            aq.id(R.id.count_add_area).gone();
            aq.id(R.id.add_area).gone();
            if ("91".equals(_statusCode)) {
                aq.id(R.id.text_status).text("주문취소");
            } else if ("92".equals(_statusCode)) {
                aq.id(R.id.text_status).text("재세탁 신청");
            } else if ("93".equals(_statusCode)) {
                aq.id(R.id.text_status).text("환불");
            } else if ("94".equals(_statusCode)) {
                aq.id(R.id.text_status).text("보상요청");
            } else if ("95".equals(_statusCode)) {
                aq.id(R.id.text_status).text("고객사유로 취소");
            } else if ("96".equals(_statusCode)) {
                aq.id(R.id.text_status).text("주문 거부");
            }
            aq.id(R.id.pre_alert_area).gone();
            aq.id(R.id.add_area).gone();
            //aq.id(R.id.text_delivery_area).visible();
            aq.id(R.id.pick_action_layout).gone();
        }

        dismissDialog();
    }


    public void orderCompleteAction(View button) {
        //modStatus("23");

        if ("Y".equals(isPayment)) {

            if (payments.length() > 0) {
                if ("P91".equals(paymentType)) {
                    // 미수금 경고창
                    //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_card);
                    orderNotPaymentPopup();
                } else if ("P92".equals(paymentType)) {
                    // 미수금 경고창
                    //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_cash);
                    orderNotPaymentPopup();
                } else {
                    //
                    modStatus("23");
                    //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_already);
                }
            } else {
                //aq.id(R.id.paid_info_layout).gone();
                // 미수금 경고창
                orderNotPaymentPopup();
            }

        } else {
            // 미수금 경고창
            orderNotPaymentPopup();
        }
    }

    public void orderNotPaymentPopup(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(true).setMessage("결제가 완료되지 않은 주문 건입니다. 현금(혹은 현장카드결제)을 받으셨나요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        modStatus("23");
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                noPaymentDeliveryCompleteAction();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void noPaymentDeliveryCompleteAction(){
        showDialog();
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.NO_PAYMENT_STAY, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.NO_PAYMENT_STAY, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.NO_PAYMENT_STAY, result);

                    JSONObject jsondata = new JSONObject(result);
                    dismissDialog();
                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        aq.id(R.id.pre_alert_area).gone();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
                        alertDialogBuilder.setCancelable(true).setMessage("배송은 완료되었지만, 미수금이 있어서 \"완료지연(미수금)\" 으로 표기됩니다.\n" +
                                "고객님 결제 완료 시 자동으로 \"배송완료\" 처리됩니다. ")
                                .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void addCompleteAction(View button) {

        try {
            JSONArray arr = new JSONArray(pickupItems);
            JSONArray arr2 = new JSONArray(pickupRepairItems);
            if (arr.length() > 0 || arr2.length() > 0) {
                if(orderStatus != null){
                    submitPickup(orderStatus);
                }else{
                    Toast.makeText(this, "현재 주문상태값이 오류가 발견되었습니다. 앱을 다시 실행해주세요.", Toast.LENGTH_LONG).show();
                }
            } else {
                if ("Y".equals(isDeliveryMan)) {
                    if(orderStatus != null){
                        submitPickup(orderStatus);
                    }else{
                        Toast.makeText(this, "현재 주문상태값이 오류가 발견되었습니다. 앱을 다시 실행해주세요.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    DUtil.alertShow(this, "수거항목을 추가해주세요.");
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private int getTotalPrice() {
        int totalPrice = 0;
        try {
            //JSONArray arr1 = new JSONArray(pickupItems);

            for (int i = 0; i < pickupItemsV2.length(); i++) {
                int price = pickupItemsV2.getJSONObject(i).getInt("confirmPrice");
                totalPrice += price;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        //
        if (totalPrice < 20000 && totalPrice != 0) {
            totalPrice += 2000;
        }
        return totalPrice;
    }

    int prevTagNo = -1;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICKUP_ITEM:
                    try {
                        pickupItems = data.getStringExtra("data");
                        String retOrderStatus = data.getStringExtra("orderStatus");
                        pickupItemsV2 = new JSONArray(pickupItems);

                        prevTagNo = data.getIntExtra("prevTagNo", -1);

                        JSONArray arr = new JSONArray(pickupItems);
                        //Snackbar.make(aq.getView(), "상품을 추가했습니다", Snackbar.LENGTH_SHORT).show();
                        //Toast.makeText(this, "상품을 추가했습니다", Toast.LENGTH_LONG).show();
                        Log.i("pickupItems list", pickupItemsV2.toString());
                        aq.id(R.id.total_layout).visible();
                        //int price = data.getIntExtra("price", 0);
                        String priceString = MZValidator.toNumFormat(getTotalPrice());

                        aq.id(R.id.text_total_price).text(priceString);

                        String picksText = "";
                        int count = 0;
                        if(arr.length() > 0){
                            picksText = arr.getJSONObject(0).getString("itemTitle");
                            aq.id(R.id.text_items).text(picksText + " 등 "+ arr.length() +"개");
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.add_area).gone();

                            if("106".equals(orderSubType)){
                                // 11번가 팝업
                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        Intent St11CompareIntent = new Intent(ctx, St11ComparePriceActivity.class);
                                        if (pickupItemsV2.length() != 0) {
                                            St11CompareIntent.putExtra("pickupItems", pickupItemsV2.toString());
                                            St11CompareIntent.putExtra("orderData", parentData.toString());
                                        }else{
                                            return;
                                        }
                                        St11CompareIntent.setAction(Intent.ACTION_GET_CONTENT);
                                        startActivityForResult(St11CompareIntent, ALERT_11ST_ADD_ITEM);
                                    }
                                }, 200);
                            }else{
                                if(orderStatus != null){
                                    submitPickup(retOrderStatus);
                                }else{
                                    Toast.makeText(this, "현재 주문상태값이 오류가 발견되었습니다. 앱을 다시 실행해주세요.", Toast.LENGTH_LONG).show();
                                }
                            }

                            aq.id(R.id.pick_action_layout).visible();

                        }else {
                            aq.id(R.id.picks_layout).gone();
                            aq.id(R.id.add_area).visible();

                            JSONArray count1 = new JSONArray(pickupItems);
                            JSONArray count2 = new JSONArray(pickupRepairItems);
                            JSONArray countV2 = pickupItemsV2;

                            if (count1.length() > 0 || count2.length() > 0) {
                                aq.id(R.id.pick_action_layout).visible();
                            }else if(countV2.length() > 0){
                                aq.id(R.id.pick_action_layout).visible();
                            } else {
                                aq.id(R.id.pick_action_layout).gone();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
                case PICKUP_REPAIR_ITEM:
                    try {
                        pickupRepairItems = data.getStringExtra("data");
                        JSONArray arr = new JSONArray(pickupRepairItems);
                        //Snackbar.make(aq.getView(), "상품을 추가했습니다", Snackbar.LENGTH_SHORT).show();
                        //Toast.makeText(this, "상품을 추가했습니다", Toast.LENGTH_LONG).show();
                        Log.i("pickupRepairItems list", pickupRepairItems);
                        aq.id(R.id.total_layout).visible();
                        //int price = data.getIntExtra("price", 0);
                        String priceString = MZValidator.toNumFormat(getTotalPrice());

                        aq.id(R.id.text_total_price).text(priceString);

                        String picksText = "";
                        int count = 0;
                        for (int i = 0; i < arr.length(); i++) {
                            if ("N".equals(arr.getJSONObject(i).getString("isOption"))) {
                                picksText += arr.getJSONObject(i).getString("itemTitle") + " x" + arr.getJSONObject(i).getInt("itemQuantity");
                                count += arr.getJSONObject(i).getInt("itemQuantity");
                                if (i == arr.length() - 1) {
                                    picksText += "\n총 " + count + "건";
                                } else {
                                    picksText += ", ";
                                }
                            }
                        }

                        JSONArray count1 = new JSONArray(pickupItems);
                        JSONArray count2 = new JSONArray(pickupRepairItems);
                        JSONArray countV2 = pickupItemsV2;

                        if (count1.length() > 0) {
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.btn_show).gone();
                        }else if(countV2.length() > 0){
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.btn_show).visible();
                        } else {

                            if (count1.length() > 0 || count2.length() > 0) {
                                aq.id(R.id.pick_action_layout).visible();
                            }else if(countV2.length() > 0){
                                aq.id(R.id.pick_action_layout).visible();
                            } else {
                                aq.id(R.id.picks_layout).gone();
                                aq.id(R.id.pick_action_layout).gone();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case PICKUP_COUNT:
                    pickQuantity = data.getIntExtra("result", 0);
                    if (pickQuantity > 0) {
                        //aq.id(R.id.picks_layout).visible();
                        aq.id(R.id.item_count_text).text("총 " + pickQuantity + "개");

                        aq.id(R.id.item_count_layout).visible();
                        aq.id(R.id.pick_action_layout).visible();
                        aq.id(R.id.btn_count_add).gone();
                    } else {
                        //aq.id(R.id.picks_layout).gone();

                        //aq.id(R.id.item_count_layout).gone();
                        aq.id(R.id.pick_action_layout).gone();
                        aq.id(R.id.btn_count_add).visible();
                    }
                    if(orderStatus != null){
                        submitPickup(orderStatus);
                    }else{
                        Toast.makeText(this, "현재 주문상태값이 오류가 발견되었습니다. 앱을 다시 실행해주세요.", Toast.LENGTH_LONG).show();
                    }

                    break;
                case MOD_DELIVERY_OK:
                    Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("USER")
                            .setAction("배송요청일 변경")
                            .setLabel("Order")
                            .build());

                    Toast.makeText(this, "배송요청정보가 변경되었습니다.", Toast.LENGTH_LONG).show();
                    getRemoteData(orderId);

                    break;
                case MOD_PICKUP_OK:
                    Tracker t1 = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                    t1.send(new HitBuilders.EventBuilder()
                            .setCategory("USER")
                            .setAction("수거요청일 변경")
                            .setLabel("Order")
                            .build());

                    Toast.makeText(this, "수거,배송 요청정보가 변경되었습니다.", Toast.LENGTH_LONG).show();
                    getRemoteData(orderId);
                    break;
                case SHOW_ITEM:
                    getRemoteData(orderId);
                    break;
                case ALERT_11ST_ADD_ITEM:
                    if(orderStatus != null){
                        submitPickup(orderStatus);
                    }else{
                        Toast.makeText(this, "현재 주문상태값이 오류가 발견되었습니다. 앱을 다시 실행해주세요.", Toast.LENGTH_LONG).show();
                    }

                    break;
                case CREATE_ORDER:
                    isReordered = true;
                    aq.id(R.id.reorder_layout).gone();
                    Toast.makeText(this, "주문이 정상적으로 생성되었습니다.", Toast.LENGTH_LONG).show();
                    break;

            }
        }else if (resultCode == RESULT_CANCELED) {
            switch (requestCode) {
                case ALERT_11ST_ADD_ITEM:
                    aq.id(R.id.total_layout).gone();
                    getRemoteData(orderId);
                    break;
            }
        }
    }

    boolean isReordered = false;

    public void showItemsAction(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent pickIntent = new Intent(ctx, OrderItemElementActivity.class);
                if (pickupItemsV2.length() != 0) {
                    pickIntent.putExtra("pickupItems", pickupItemsV2.toString());
                    pickIntent.putExtra("orderId", orderId);
                    pickIntent.putExtra("orderStatus", orderStatus);
                }else{
                    return;
                }
                pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(pickIntent, SHOW_ITEM);
            }
        }, 200);

    }

    public void reorderAction(View button){
        //Toast.makeText(getApplicationContext(), "빠른시일내에 해당기능이 제공될 예정입니다.", Toast.LENGTH_LONG).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent createIntent = new Intent(ctx, OrderCreateActivity.class);
                createIntent.putExtra("orderId", String.valueOf(orderId));
                createIntent.putExtra("userId", userId);
                createIntent.putExtra("addressSeq", addressSeq);
                createIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(createIntent, CREATE_ORDER);
            }
        }, 200);
    }

    public void addItemsAction(View button) {
        final int tag = (int) button.getTag();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent pickIntent = new Intent(ctx, PickupItemBucketActivity.class);
                //Intent pickIntent = new Intent(ctx, PriceActivity.class);
                Intent pickCountIntent = new Intent(ctx, PickCountScreen.class);
                if (pickQuantity > 0) {
                    pickCountIntent.putExtra("pickCount", pickQuantity);
                    pickIntent.putExtra("pickCount", pickQuantity);
                }

                int pickupMode;
                switch (tag) {
                    case 0:

                        pickCountIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickCountIntent, PICKUP_COUNT);
                        break;
                    case 1:
                        pickupMode = PickupType.PICKUPTYPE_CLEAN;
                        pickIntent.putExtra("orderStatus", orderStatus);
                        if (!StringUtil.isNullOrEmpty(pickupItems)) {
                            pickIntent.putExtra("pickupItems", pickupItems);
                            pickIntent.putExtra("pickupMode", pickupMode);
                            pickIntent.putExtra("prevTagNo", prevTagNo);
                        }
                        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickIntent, PICKUP_ITEM);
                        break;
                    case 2:
                        /*
                        pickupMode = PickupType.PICKUPTYPE_REPAIR;
                        if (!StringUtil.isNullOrEmpty(pickupRepairItems)) {
                            pickIntent.putExtra("pickupItems", pickupRepairItems);
                            pickIntent.putExtra("pickupMode", pickupMode);
                        }
                        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickIntent, PICKUP_REPAIR_ITEM);*/
                        break;
                    case 3:
                        pickupMode = PickupType.PICKUPTYPE_TODAY;
                        pickIntent.putExtra("orderStatus", orderStatus);
                        if (!StringUtil.isNullOrEmpty(pickupItems)) {
                            pickIntent.putExtra("pickupItems", pickupItems);
                            pickIntent.putExtra("pickupMode", pickupMode);
                            pickIntent.putExtra("prevTagNo", prevTagNo);
                        }
                        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickIntent, PICKUP_ITEM);
                        break;
                    case 4:
                        pickupMode = PickupType.PICKUPTYPE_TOMORROW;
                        pickIntent.putExtra("orderStatus", orderStatus);
                        if (!StringUtil.isNullOrEmpty(pickupItems)) {
                            pickIntent.putExtra("pickupItems", pickupItems);
                            pickIntent.putExtra("pickupMode", pickupMode);
                            pickIntent.putExtra("prevTagNo", prevTagNo);
                        }
                        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickIntent, PICKUP_ITEM);
                        break;
                    case 5:
                        pickupMode = PickupType.PICKUPTYPE_B2B_HANDYS;
                        if (!StringUtil.isNullOrEmpty(pickupItems)) {
                            pickIntent.putExtra("pickupItems", pickupItems);
                            pickIntent.putExtra("pickupMode", pickupMode);
                            pickIntent.putExtra("prevTagNo", prevTagNo);
                        }
                        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickIntent, PICKUP_ITEM);
                        break;
                    default:
                        //pickupMode = PickupType.PICKUPTYPE_CLEAN;
                        pickupMode = PickupType.PICKUPTYPE_CLEAN;
                        pickIntent.putExtra("orderStatus", orderStatus);
                        if (!StringUtil.isNullOrEmpty(pickupItems)) {
                            pickIntent.putExtra("pickupItems", pickupItems);
                            pickIntent.putExtra("pickupMode", pickupMode);
                            pickIntent.putExtra("prevTagNo", prevTagNo);
                        }
                        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickIntent, PICKUP_ITEM);
                        break;
                }
            }
        }, 200);
    }

    public void preAlertAction(View button) {

        if(orderStatus == null){
            return;
        }

        String orderReqStatus = "";
        if (!"01".equals(orderStatus) && !"21".equals(orderStatus) && !"13".equals(orderStatus)) {
            return;
        } else {
            if ("01".equals(orderStatus)) {
                orderReqStatus = "02";
            } else if ("21".equals(orderStatus)) {
                orderReqStatus = "22";
            } else if ("13".equals(orderStatus)) {
                orderReqStatus = "22";
            } else{
                Toast.makeText(this, "현재 진행상태값이 이상합니다. 앱을 다시 실행해주시겠어요?", Toast.LENGTH_LONG).show();
                return;
            }

            final String orderReqFinalStatus = orderReqStatus;
            modStatus(orderReqFinalStatus);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setCancelable(true)
                    .setMessage(userName + "님("+ MZValidator.validTelNumber(phoneNo)+")과 전화통화를 원하십니까?")
                    .setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                if (Build.VERSION.SDK_INT >= 23) {
                                    if (!canAccessSMS()) {
                                        requestPermissions(SMS_PERMS, SMS_REQUEST);
                                    } else {
                                        number_1 = telephonyManager.getLine1Number();
                                        String phoneNumber = number_1.replace("+82", "0");
                                        Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                                        mPhoneNumber = MZValidator.validTelNumber(phoneNumber);
                                    }
                                } else {
                                    number_1 = telephonyManager.getLine1Number();
                                    String phoneNumber = number_1.replace("+82", "0");
                                    Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                                    mPhoneNumber = MZValidator.validTelNumber(phoneNumber);
                                }

                                RequestParams params = new RequestParams();
                                params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                                params.put("userId", userId);
                                params.put("callerMdn", mPhoneNumber.replace("-", ""));
                                params.put("calleeMdn", phoneNo.replace("-", ""));
                                //params.put("originCallerNumber", orderId);
                                try {
                                    params.put("appUrl", "rewhiteMeUserOrder://orderDetail?orderId="+orderId);
                                    params.put("title", "[리화이트] " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_NAME) + "입니다");
                                    params.put("text", getStatusText(orderStatus));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                params.put("k", 1);

                                DUtil.Log(Constants.SEND_T_PHONE + " req", params.toString());

                                NetworkClient.post(Constants.SEND_T_PHONE, params, new AsyncHttpResponseHandler() {

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                                        // TODO Auto-generated method stub
                                        DUtil.Log(Constants.SEND_T_PHONE, error.getMessage());
                                    }

                                    @Override
                                    public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                                        String result;
                                        try {
                                            result = new String(data, "UTF-8");
                                            DUtil.Log(Constants.SEND_T_PHONE, result);

                                            JSONObject jsondata = new JSONObject(result);

                                            if (Build.VERSION.SDK_INT >= 23) {
                                                if (!canAccessSMS()) {
                                                    requestPermissions(SMS_PERMS, SMS_REQUEST);
                                                } else {
                                                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                                                        call(true);
                                                    } else {
                                                        call(false);
                                                    }
                                                }
                                            } else {
                                                if ("S0000".equals(jsondata.getString("resultCode"))) {
                                                    call(true);
                                                } else {
                                                    call(false);
                                                }
                                            }

                                        } catch (UnsupportedEncodingException | JSONException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                    }
                                });

                            }catch(Exception ex){
                                ex.printStackTrace();
                                call(false);
                            }


                        }})
                    .setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

    }

    private void modStatus(final String _orderStatus) {
        showDialog();
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("orderStatus", _orderStatus);
        params.put("k", 1);

        Log.e("modStatus Args", params.toString());

        NetworkClient.post(Constants.MOD_ORDER_STATUS, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.MOD_ORDER_STATUS, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.MOD_ORDER_STATUS, result);

                    JSONObject jsondata = new JSONObject(result);
                    dismissDialog();
                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        aq.id(R.id.pre_alert_area).gone();
                        orderStatus = _orderStatus;

                        if ("02".equals(orderStatus) || "22".equals(orderStatus)) {

                            aq.id(R.id.below_layout).visible();
                            aq.id(R.id.alert_layout).gone();
                            setStatusScreen(orderStatus);
                        } else if ("23".equals(orderStatus)) {
                            ToastUtility.show(ctx, "배송완료 처리되었습니다", Toast.LENGTH_SHORT);
                            finish();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), jsondata.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }


    /*
        수거완료 처리
     */

    int pickupItemSubmitRetry = 0;

    private void submitPickup(final String currentOrderStatus) {
        showDialog();

//        if (getTotalPrice() < 0) {
//            DUtil.alertShow(this, "총금액이 만원이하 입니다.");
//            dismissDialog();
//            return;
//        }

        String api_url = "";
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);

        // 2015-11-06
        // 세탁소로 이동중 -> 세탁준비중 으로 단계축소처리
        //params.put("orderStatus", "03");
        if ("02".equals(currentOrderStatus) || "01".equals(currentOrderStatus)) {
            params.put("orderStatus", "03");
            params.put("pickupQuantity", pickQuantity);
            api_url = Constants.ORDER_PICKUP_COMPLETE;
            if (pickQuantity == 0) {
                DUtil.alertShow(this, "수량을 추가해주세요.");
                dismissDialog();
                return;
            }
        } else if ("03".equals(currentOrderStatus)){
            // 수량입력후 품목 등록
            //params.put("orderStatus", "11");
            params.put("items", pickupItemsV2.toString());
            // V2 API로 교체
            api_url = Constants.ORDER_PICKUP_COMPLETE_V2;

            if (pickupItemsV2.length() == 0) {
                DUtil.alertShow(this, "세탁물을 추가해주세요.");
                dismissDialog();
                return;
            }
        }else{
            dismissDialog();
            return;
        }

        params.put("k", 1);
        params.setHttpEntityIsRepeatable(true);


        Log.i("SUBMIT PARAM", params.toString());

        NetworkClient.post(api_url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                // called before request is started
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_PICKUP_COMPLETE, error.getMessage());
                DUtil.Log(Constants.ORDER_PICKUP_COMPLETE, statusCode + " / " );
                dismissDialog();
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
                Log.d("loopj", "AsyncAndroid onRetry Method");
                Log.d("loopj", "AsyncAndroid retryNo:" + retryNo);
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_PICKUP_COMPLETE, result);

                    dismissDialog();
                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //
                            //Toast.makeText(getApplicationContext(), "접수를 완료했습니다.", Toast.LENGTH_LONG).show();
                        } else {
                            //JSONArray locationInfo = jsondata.getJSONArray("data");
                            Toast.makeText(getApplicationContext(), "접수를 완료했습니다.", Toast.LENGTH_LONG).show();
                            getRemoteData(orderId);
                            //finish();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), jsondata.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }

            @Override
            public void onFinish() {
                Log.d("loopj onFinish", "AsyncAndroid onFinish Method");
                dismissDialog();
            }
        });
    }
}
