package me.rewhite.delivery.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.res.ResourcesCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.kakao.kakaonavi.KakaoNaviParams;
import com.kakao.kakaonavi.KakaoNaviService;
import com.kakao.kakaonavi.Location;
import com.kakao.kakaonavi.NaviOptions;
import com.kakao.kakaonavi.options.CoordType;
import com.kakao.kakaonavi.options.RpOption;
import com.kakao.kakaonavi.options.VehicleType;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.Bootstrap;
import me.rewhite.delivery.R;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.GSBaseActivity;
import me.rewhite.delivery.data.PickupType;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.StringUtil;
import me.rewhite.delivery.util.SystemUtil;
import me.rewhite.delivery.util.TimeUtil;
import me.rewhite.delivery.util.ToastUtility;

public class GSOrderDetailActivity extends GSBaseActivity implements NumberPicker.OnValueChangeListener{

    AQuery aq;
    Context ctx = this;

    JSONObject parentData;
    String userName;
    String profileImage;
    long pickupRequestTime;
    long deliveryRequestTime;
    long pickupCompleteTime;
    long deliveryCompleteTime;
    String addressName;
    String addressDetailName;
    String orderStatus;
    String orderPrice;
    int orderId;
    int userId;
    int pickQuantity = 0;
    int packQuantity;
    int boxQuantity = 0;
    String partnerOrderNo;
    String partnerStoreId;
    String partnerId;
    int priceGroupId;

    //int addressSeq;
    double longitude;
    double latitude;
    private static final int PICKUP_COUNT = 7;
    private static final int PICKUP_ITEM = 8;
    private static final int PICKUP_REPAIR_ITEM = 9;
    private static final int SHOW_ITEM = 10;
    private static final int CREATE_ORDER = 11;
    private static final int ALERT_11ST_ADD_ITEM = 12;
    String orderRequest;
    JSONArray pickupItemsV2 = new JSONArray();
    String pickupItems;
    String pickupRepairItems;
    String isPayment;
    String phoneNo;
    String isDeliveryMan;

    String orderSubType;
    String orderType;

    boolean isConfirmed = true;
    boolean isPaidConfirmed = true;

    ProgressDialog mProgressDialog;
    ImageOptions options;
    private String appDetailSettingString = "카메라를 사용할 권한이 없습니다. 앱 권한설정 화면으로 이동합니다.\n이동한 화면에서 '권한'메뉴를 선택하시고 전체 허용해주세요~";

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_SMS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE
    };
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final String[] SMS_PERMS={
            Manifest.permission.READ_SMS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE
    };
    private static final String[] CALL_PERMS={
            Manifest.permission.READ_SMS,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = INITIAL_REQUEST + 3;
    private static final int SMS_REQUEST=INITIAL_REQUEST+4;
    private static final int REQUEST_PHONE_CALL = 123;

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            //Toast.makeText(OrderDetailActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();

        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            //Toast.makeText(OrderDetailActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
            alertDialogBuilder.setCancelable(false).setTitle("앱 권한 설정하기").setMessage(appDetailSettingString)
                    .setPositiveButton("설정화면으로 가기", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SystemUtil.showApplicationDetailSetting(ctx);
                        }
                    }).setNegativeButton("취소", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }


    };

    // API 23 above Permission Protect statement
    @SuppressLint({"HardwareIds", "MissingPermission"})
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    .setData(Uri.parse("package:" + getPackageName()));
            startActivity(intent);
            return;
        }

        switch (requestCode) {

            case SMS_REQUEST:
                if (canAccessSMS()) {
                    number_1 = telephonyManager.getLine1Number();
                    String phoneNumber = number_1.replace("+82", "0");
                    Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                    mPhoneNumber = MZValidator.validTelNumber(phoneNumber);
                }else {
                    Toast.makeText(this, "핸드폰번호를 알수 없습니다.", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
            case REQUEST_PHONE_CALL:
                if (canAccessCall()) {
                    number_1 = telephonyManager.getLine1Number();
                    String phoneNumber = number_1.replace("+82", "0");
                    Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                    mPhoneNumber = MZValidator.validTelNumber(phoneNumber);
                }else {
                    Toast.makeText(this, "핸드폰번호를 알수 없습니다.", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }
    private boolean canAccessSMS() {
        return (hasPermission(Manifest.permission.READ_SMS));
    }
    private boolean canAccessCall() {
        return(hasPermission(Manifest.permission.CALL_PHONE));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gs_order_detail);

        telephonyManager =(TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);

        aq = new AQuery(this);

        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(appDetailSettingString)
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE, Manifest.permission.READ_SMS)
                .check();

        options = new ImageOptions();
        options.round = 89;
        options.ratio = 1.0f;
        options.memCache = true;
        options.targetWidth = 89;
        options.animation = AQuery.FADE_IN_NETWORK;
        options.preset = aq.getCachedImage(R.mipmap.kakao_default_profile_image);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("orderId") != null) {

            }else{
                finish();
            }

            orderId = Integer.parseInt(intent.getStringExtra("orderId"));

            JSONObject partnerData = null;
            try {
                partnerData = new JSONObject(intent.getStringExtra("data"));
                longitude = partnerData.getDouble("partnerStoreLongitude");
                latitude = partnerData.getDouble("partnerStoreLatitude");
                partnerId = partnerData.getString("partnerId");
                partnerStoreId = partnerData.getString("partnerStoreId");

                addressName = partnerData.getString("partnerStoreAddress1");
                addressDetailName = partnerData.getString("partnerStoreAddress2");
                String newAddressName = addressName + " " + addressDetailName;
                if (!StringUtil.isNullOrEmpty(newAddressName)) {
                    aq.id(R.id.text_address).text(newAddressName).typeface(CommonUtility.getNanumBarunTypeface());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if("7".equals(partnerId)){
                aq.id(R.id.order_refer).image(R.mipmap.icon_list_gs);
                aq.id(R.id.top_layout).backgroundColor(Color.parseColor("#00c3d5"));
            }else{
                aq.id(R.id.order_refer).image(R.mipmap.icon_list_place);
                aq.id(R.id.top_layout).backgroundColor(Color.parseColor("#00aeef"));
            }

            //parentData = new JSONObject(intent.getStringExtra("data"));
            //orderId = parentData.getInt("orderId");

            mProgressDialog = new ProgressDialog(GSOrderDetailActivity.this);
            mProgressDialog.setMessage("잠시만 기다려주세요");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            getRemoteData(orderId);
        }

        aq.id(R.id.time_title).textColor(0xff999999);
        aq.id(R.id.req_title).textColor(0xff999999);
        aq.id(R.id.address_title).textColor(0xff999999);
        aq.id(R.id.count_title).textColor(0xff999999);
        aq.id(R.id.pickitem_title).textColor(0xff999999);
        //aq.id(R.id.pickitem_repair_title).textColor(0xffa0a0a0);
        aq.id(R.id.totalprice_title).textColor(0xff999999);

        aq.id(R.id.btn_count_add).clicked(this, "addItemsAction").tag(0);
        aq.id(R.id.btn_count_mod).clicked(this, "addItemsAction").tag(0);

        //aq.id(R.id.btn_find_location).clicked(this, "findLocationMap");



        String orderAvailable = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_AVAILABLE_ORDER);
        try {
            // 수선가능여부 판단

            if ('1' == orderAvailable.charAt(2)) {
                Log.e("orderAvailble is equal", orderAvailable + "/" + orderAvailable.charAt(2));
            } else {
                Log.e("orderAvailble not equal", orderAvailable + "/" + orderAvailable.charAt(2));
                //aq.id(R.id.btn_repair_add).image(R.mipmap.btn_add_repair_item_disabled).enabled(false);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            //Log.e("NullPointerException", e.getMessage());
            //aq.id(R.id.btn_repair_add).image(R.mipmap.btn_add_repair_item_disabled).enabled(false);
        }

        aq.id(R.id.btn_submit).clicked(this, "addCompleteAction");
        //aq.id(R.id.btn_confirm).clicked(this, "pickConfirmAction");
        //aq.id(R.id.btn_completed).clicked(this, "deliveryConfirmAction");
        aq.id(R.id.btn_delivery).clicked(this, "orderCompleteAction");
        aq.id(R.id.picks_layout).gone();
        //aq.id(R.id.repair_layout).gone();
        aq.id(R.id.total_layout).gone();
        aq.id(R.id.delivery_action_layout).gone();
        //aq.id(R.id.text_delivery_area).gone();

        aq.id(R.id.btn_add).clicked(this, "addItemsAction").tag(1);
        aq.id(R.id.btn_mod).clicked(this, "addItemsAction").tag(1);
        aq.id(R.id.btn_pickup_alert).clicked(this, "preAlertAction");
        aq.id(R.id.btn_delivery_alert).clicked(this, "preAlertAction");
        //
        aq.id(R.id.btn_back).clicked(this, "closeClicked");
        aq.id(R.id.btn_call).clicked(this, "callClicked");
        aq.id(R.id.btn_show).clicked(this, "showItemsAction");
        aq.id(R.id.btn_box_change).clicked(this, "modBoxCountAction");

        aq.id(R.id.btn_barcode).clicked(this, "barcodeShowAction");
    }

    public void barcodeShowAction(View button){
        Intent intent = new Intent();

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setClass(ctx, BarcodeViewActivity.class);
        intent.putExtra("orderId", String.valueOf(orderId));

        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
    }


    private static final int MOD_DELIVERY_OK = 2;
    private static final int MOD_PICKUP_OK = 3;

    boolean isGPSEnabled;
    boolean isNetworkEnabled;
    boolean locationServiceAvailable;

    @Override
    protected void onPause() {
        super.onPause();

    }

    String isReceivable;

    public void findLocationMap(View button) {
        Log.e("findLocation", userName + " / " + latitude + " / "+ longitude);


        if ( Build.VERSION.SDK_INT >= 23){
            if (!canAccessLocation()) {
                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
            }else{

//                TMapTapi tmaptapi = new TMapTapi(this);
//                tmaptapi.setSKPMapAuthentication (Constants.SK_APP_KEY);
//                tmaptapi.invokeRoute(userName, (float)longitude, (float)latitude);

                // KAKAO
                if(KakaoNaviService.getInstance().isKakaoNaviInstalled(ctx)){
                    Location destination = Location.newBuilder(userName, longitude, latitude).build();
                    NaviOptions options = NaviOptions.newBuilder().setCoordType(CoordType.WGS84).setVehicleType(VehicleType.FIRST).setRpOption(RpOption.FAST).build();

                    KakaoNaviParams.Builder builder = KakaoNaviParams.newBuilder(destination)
                            .setNaviOptions(options);

                    KakaoNaviService.getInstance().shareDestination(GSOrderDetailActivity.this, builder.build());
                }else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setCancelable(true).setMessage("카카오네비가 설치되어있지 않습니다. 설치하러 이동하시겠습니까?")
                            .setPositiveButton("설치하러 이동", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String name="https://play.google.com/store/apps/details?id=com.locnall.KimGiSa";
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(name));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.setPackage("com.android.vending");
                                    startActivity(intent);
                                }

                            }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            }
        }else{
//            TMapTapi tmaptapi = new TMapTapi(this);
//            tmaptapi.setSKPMapAuthentication (Constants.SK_APP_KEY);
//            tmaptapi.invokeRoute(userName, (float)longitude, (float)latitude);

            // KAKAO
            if(KakaoNaviService.getInstance().isKakaoNaviInstalled(ctx)){
                Location destination = Location.newBuilder(userName, longitude, latitude).build();
                NaviOptions options = NaviOptions.newBuilder().setCoordType(CoordType.WGS84).setVehicleType(VehicleType.FIRST).setRpOption(RpOption.FAST).build();

                KakaoNaviParams.Builder builder = KakaoNaviParams.newBuilder(destination)
                        .setNaviOptions(options);

                KakaoNaviService.getInstance().shareDestination(GSOrderDetailActivity.this, builder.build());
            }else{
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setCancelable(true).setMessage("카카오네비가 설치되어있지 않습니다. 설치하러 이동하시겠습니까?")
                        .setPositiveButton("설치하러 이동", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String name="https://play.google.com/store/apps/details?id=com.locnall.KimGiSa";
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(name));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setPackage("com.android.vending");
                                startActivity(intent);
                            }

                        }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        }

    }


    private String getStatusText(String _statusCode) throws JSONException {
        String tempText = "";
        if ("01".equals(_statusCode) || "02".equals(_statusCode)) {
            String dps = displayTimeString(parentData.getLong("pickupRequestTimeApp"));
            tempText = dps + " 수거";
            //aq.id(R.id.btn_complete_title).text("수거수량 입력완료");
        } else if ("03".equals(_statusCode)) {
            tempText = "검품 후 인수증 발급 예정";

        }else if ("11".equals(_statusCode) || "12".equals(_statusCode) || "13".equals(_statusCode)) {
            if ("Y".equals(isPayment)) {
                String dps = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
                tempText = dps + " 배송";
            }else{
                tempText = "인수증 확인 후 결제요청";
            }
        } else if ("21".equals(_statusCode) || "22".equals(_statusCode)) {
            String dps = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
            if ("Y".equals(isPayment)) {
                tempText = dps + " 배송";
            }else{
                tempText = dps + " 배송(결제요청)";
            }
        } else if ("23".equals(_statusCode) || "24".equals(_statusCode)) {
            tempText = "우리동네 배달세탁소";
        } else {
            tempText = "우리동네 배달세탁소";
        }
        return tempText;
    }

    TelephonyManager telephonyManager;
    String number_1 = "";
    String mPhoneNumber = "";

    @SuppressLint({"MissingPermission", "HardwareIds"})
    public void callClicked(View button) {

        try{
            if ( Build.VERSION.SDK_INT >= 23){
                if (!canAccessCall()) {
                    requestPermissions(CALL_PERMS, REQUEST_PHONE_CALL);
                }else{
                    number_1 = telephonyManager.getLine1Number();
                    String phoneNumber = number_1.replace("+82", "0");
                    Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                    mPhoneNumber = MZValidator.validTelNumber(phoneNumber);
                }
            }else{
                number_1 = telephonyManager.getLine1Number();
                String phoneNumber = number_1.replace("+82", "0");
                Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                mPhoneNumber = MZValidator.validTelNumber(phoneNumber);
            }
            //phoneNo
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setCancelable(true).setMessage(userName + "님("+MZValidator.validTelNumber(phoneNo)+")과 전화통화를 원하십니까?")
                    .setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            RequestParams params = new RequestParams();
                            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                            params.put("userId", userId);
                            params.put("callerMdn", mPhoneNumber.replace("-", ""));
                            params.put("calleeMdn", phoneNo.replace("-", ""));
                            //params.put("originCallerNumber", orderId);
                            try {
                                params.put("appUrl", "rewhiteMeUserOrder://orderDetail?orderId="+orderId);
                                params.put("title", "[리화이트] " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_NAME) + "입니다");
                                params.put("text", getStatusText(orderStatus));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            params.put("k", 1);

                            DUtil.Log(Constants.SEND_T_PHONE + " req", params.toString());

                            NetworkClient.post(Constants.SEND_T_PHONE, params, new AsyncHttpResponseHandler() {

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                                    // TODO Auto-generated method stub
                                    DUtil.Log(Constants.SEND_T_PHONE, error.getMessage());

                                }

                                @Override
                                public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                                    String result;
                                    try {
                                        result = new String(data, "UTF-8");
                                        DUtil.Log(Constants.SEND_T_PHONE, result);

                                        JSONObject jsondata = new JSONObject(result);

                                        if ("S0000".equals(jsondata.getString("resultCode"))) {
                                            if ( Build.VERSION.SDK_INT >= 23){
                                                if (!canAccessSMS()) {
                                                    requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
                                                }else{
                                                    call(true);
                                                }
                                            }else{
                                                call(true);
                                            }

                                        } else {
                                            if ( Build.VERSION.SDK_INT >= 23){
                                                if (!canAccessSMS()) {
                                                    requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
                                                }else{
                                                    call(true);
                                                }
                                            }else{
                                                call(true);
                                            }

                                        }
                                    } catch (UnsupportedEncodingException e1) {
                                        // TODO Auto-generated catch block
                                        e1.printStackTrace();
                                    } catch (JSONException e1) {
                                        // TODO Auto-generated catch block
                                        e1.printStackTrace();
                                    }
                                }
                            });


                        }
                    }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }catch(Exception ex){
            ex.printStackTrace();
        }

    }

    @SuppressLint("MissingPermission")
    public void call(boolean _isTPhone) {

        phoneNo = phoneNo.replace("-", "");
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        //callIntent.setAction("android.intent.action.DIAL");
        callIntent.setData(Uri.parse("tel:" + phoneNo));
        if(callIntent.resolveActivity(getPackageManager()) != null){
            startActivity(callIntent);
        }

    }

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(GSOrderDetailActivity.this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) mProgressDialog.dismiss();
    }

    public void closeClicked(View button) {
        finish();
    }

    private JSONArray getPickupItem(JSONArray originData) {
        JSONArray result = new JSONArray();
        try {
            for (int i = 0; i < originData.length(); i++) {
                if ("G".equals(originData.getJSONObject(i).getString("pickupType")) || "T".equals(originData.getJSONObject(i).getString("pickupType")) || "N".equals(originData.getJSONObject(i).getString("pickupType"))) {
                    result.put(originData.getJSONObject(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private JSONArray getRepairItem(JSONArray originData) {
        JSONArray result = new JSONArray();
        try {
            for (int i = 0; i < originData.length(); i++) {
                if ("R".equals(originData.getJSONObject(i).getString("pickupType"))) {
                    result.put(originData.getJSONObject(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    String isNewPOS = "N";

    private void getRemoteData(int _orderId) {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        params.put("k", 1);

        Log.e("getRemote", params.toString());

        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_DETAIL, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        parentData = jsondata.getJSONObject("data");

                        // Initialize Start
                        userId = parentData.getInt("userId");
                        isNewPOS = parentData.getString("isNewPOS");

                        //packQuantity = parentData.getInt("packQuantity");
                        //boxQuantity = parentData.getInt("boxQuantity");
                        //addressSeq = parentData.getInt("addressSeq");

                        orderStatus = parentData.getString("orderStatus");
                        orderSubType = parentData.getString("orderSubType");
                        orderType = parentData.getString("orderType");
                        userName = parentData.getString("userName");

                        partnerOrderNo = parentData.getString("partnerOrderNo");

                        if(parentData.isNull("imageThumbPath")){
                            if("106".equals(orderSubType)){
                                aq.id(R.id.profile_image).image(R.mipmap.pro_11st_def);
                            }else{
                                aq.id(R.id.profile_image).image(R.mipmap.kakao_default_profile_image);
                            }
                        }else{
                            profileImage = parentData.getString("imageThumbPath");
                            if("".equals(profileImage) || "null".equals(profileImage)){
                                aq.id(R.id.profile_image).image(R.mipmap.kakao_default_profile_image);
                            }else{
                                aq.id(R.id.profile_image).image(profileImage, options);
                            }
                        }

                        phoneNo = parentData.getString("userPhone");
                        aq.id(R.id.profile_name).text(userName).typeface(CommonUtility.getNanumBarunTypeface());

                        if ("151".equals(orderSubType)) {
                            // GS주문

                            aq.id(R.id.top_layout).backgroundColor(0xff00c3d5);
                            aq.id(R.id.text_subtype).text("일반세탁");

                            aq.id(R.id.btn_find_location).backgroundColor(0xff007fff);
                        }else{

                            aq.id(R.id.top_layout).backgroundColor(0xff00c3d5);
                            aq.id(R.id.text_subtype).text("일반세탁");

                            aq.id(R.id.btn_find_location).backgroundColor(0xff007fff);
                        }

                        if("Y".equals(isNewPOS)){
                            aq.id(R.id.text_orderid).text("주문번호 " + orderId).typeface(CommonUtility.getNanumBarunTypeface());
                        }else{
                            aq.id(R.id.text_orderid).text("주문번호 " + partnerOrderNo).typeface(CommonUtility.getNanumBarunTypeface());
                        }

                        // Initialize End

                        int addressOrderCount = parentData.getInt("addressOrderCount");
                        int addressOrderQuantity = parentData.getInt("addressOrderQuantity");

                        isReceivable = jsondata.getJSONObject("data").getString("isReceivable");
                        isDeliveryMan = jsondata.getJSONObject("data").getString("isDeliveryMan");

                        isPayment = jsondata.getJSONObject("data").getString("isPayment");

                        pickQuantity = jsondata.getJSONObject("data").getInt("pickupQuantity");
                        aq.id(R.id.item_count_text).text("총 " + pickQuantity + "개");

                        pickupItems = getPickupItem(jsondata.getJSONObject("data").getJSONArray("pickupItems")).toString();
                        pickupRepairItems = getRepairItem(jsondata.getJSONObject("data").getJSONArray("pickupItems")).toString();
                        int pr = jsondata.getJSONObject("data").getInt("deliveryPrice");

                        if (pr < 20000) {
                            pr = pr + 2000;
                            aq.id(R.id.delivery_display_text).visible();
                        }
                        orderPrice = MZValidator.toNumFormat(pr);
                        aq.id(R.id.text_total_price).text(orderPrice);
                        JSONArray history = jsondata.getJSONObject("data").getJSONArray("history");
                        for (int i = 0; i < history.length(); i++) {
                            if ("03".equals(history.getJSONObject(i).getString("orderStatus")) || "11".equals(history.getJSONObject(i).getString("orderStatus"))) {
                                pickupCompleteTime = history.getJSONObject(i).getLong("statusDateApp");
                            }
                            if ("23".equals(history.getJSONObject(i).getString("orderStatus"))) {
                                deliveryCompleteTime = history.getJSONObject(i).getLong("statusDateApp");
                            }
                        }
                        orderStatus = jsondata.getJSONObject("data").getString("orderStatus");

                        packQuantity = jsondata.getJSONObject("data").getInt("packQuantity");
                        boxQuantity = jsondata.getJSONObject("data").getInt("boxQuantity");
                        priceGroupId = jsondata.getJSONObject("data").getInt("priceGroupId");


                        // 세탁목록
                        JSONArray arr = getPickupItem(jsondata.getJSONObject("data").getJSONArray("pickupItems"));
                        String picksText = "";
                        int count = 0;
                        for (int i = 0; i < arr.length(); i++) {
                            if ("N".equals(arr.getJSONObject(i).getString("isOption"))) {
                                picksText += arr.getJSONObject(i).getString("itemTitle") + " x" + arr.getJSONObject(i).getInt("itemQuantity");
                                count += arr.getJSONObject(i).getInt("itemQuantity");
                                if (i == arr.length() - 1) {
                                    picksText += "\n총 " + count + "개";
                                } else {
                                    picksText += ", ";
                                }
                            }
                        }
                        //수선목록
                        JSONArray arr2 = getRepairItem(jsondata.getJSONObject("data").getJSONArray("pickupItems"));
                        String picksRepairText = "";
                        int count2 = 0;
                        for (int i = 0; i < arr2.length(); i++) {
                            if ("N".equals(arr2.getJSONObject(i).getString("isOption"))) {
                                picksRepairText += arr2.getJSONObject(i).getString("itemTitle") + " x" + arr2.getJSONObject(i).getInt("itemQuantity");
                                count2 += arr2.getJSONObject(i).getInt("itemQuantity");
                                if (i == arr2.length() - 1) {
                                    picksRepairText += "\n총 " + count2 + "건";
                                } else {
                                    picksRepairText += ", ";
                                }
                            }
                        }

                        picksText += picksRepairText;

                        Log.e("clean value", arr.length() + " / " + picksText);

                        pickupItemsV2 = jsondata.getJSONObject("data").getJSONArray("pickupItemsV2");
                        if(arr.length() == 0 && pickupItemsV2.length() > 0){
                            count = pickupItemsV2.length();
                            if(count == 1){
                                picksText = pickupItemsV2.getJSONObject(0).getString("itemTitle") + " " +count + "개";
                            }else{
                                picksText = pickupItemsV2.getJSONObject(0).getString("itemTitle") + " 등 " +count + "개";
                            }

                        }

                        Log.e("clean value v2", pickupItemsV2.length() + " / " + picksText);

                        if(!jsondata.getJSONObject("data").isNull("payments")){
                            payments = jsondata.getJSONObject("data").getJSONArray("payments");
                        }else{
                            payments = new JSONArray();
                        }

                        Log.e("paidInfo", orderStatus + " / " + isReceivable + " / " + isPayment + " / " + payments.toString() + " / " + paymentType);
                        if ("23".equals(orderStatus) || "24".equals(orderStatus)) {

                            if ("Y".equals(isPayment)) {
                                if (payments.length() > 0) {
                                    paymentType = payments.getJSONObject(0).getString("paymentType");
                                    if ("P91".equals(paymentType)) {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_paycomp_face_card);
                                    } else if ("P92".equals(paymentType)) {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_paycomp_face_cash);
                                    } else {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_paycomp_mobile);
                                    }
                                } else {
                                    aq.id(R.id.paid_image).image(R.mipmap.paidinfo_not_yet);
                                }
                            }
                        }else if ("22".equals(orderStatus)) {

                            if("Y".equals(isReceivable)){
                                aq.id(R.id.paid_image).image(R.mipmap.paidinfo_paycomp_unpaid);
                            }else{
                                if ("Y".equals(isPayment)) {
                                    if (payments.length() > 0) {
                                        paymentType = payments.getJSONObject(0).getString("paymentType");
                                        if ("P91".equals(paymentType)) {
                                            aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_card);
                                        } else if ("P92".equals(paymentType)) {
                                            aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_cash);
                                        } else {
                                            aq.id(R.id.paid_image).image(R.mipmap.paidinfo_already);
                                        }
                                    }else {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_not_yet);
                                    }
                                }

                            }
                        }else{
                            if ("Y".equals(isPayment)) {

                                if (payments.length() > 0) {
                                    paymentType = payments.getJSONObject(0).getString("paymentType");
                                    if ("P91".equals(paymentType)) {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_card);
                                    } else if ("P92".equals(paymentType)) {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_cash);
                                    } else {
                                        aq.id(R.id.paid_image).image(R.mipmap.paidinfo_already);
                                    }
                                } else {
                                    aq.id(R.id.paid_info_layout).gone();
                                }
                            } else {
                                payments = null;
                                aq.id(R.id.paid_info_layout).gone();
                            }
                        }


                        aq.id(R.id.text_items).text(picksText);
                        //aq.id(R.id.text_repair_items).text(picksRepairText);

                        if (arr.length() > 0 || arr2.length() > 0) {
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.btn_show).gone();
                        }else if(pickupItemsV2.length() > 0){
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.btn_show).visible();
                        } else {
                            aq.id(R.id.picks_layout).gone();
                        }
                        if (count2 > 0) {
                            //aq.id(R.id.repair_layout).visible();
                        } else {
                            //aq.id(R.id.repair_layout).gone();
                        }

                        setStatusScreen(orderStatus);
                    } else {
                        dismissDialog();
                        DUtil.alertShow(GSOrderDetailActivity.this, jsondata.getString("message"));

                    }


                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    JSONArray payments;
    String paymentType;
    String currentStatusCode = null;

    private String displayTimeString(long time) {
        //Date dt = new Date();
        //dt.setTime(time);
        if (SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP) == null) {
            SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.DELIVERY_GAP, "1");
        }
        return TimeUtil.getDateByMDSH(time, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP));
    }

    private void setStatusScreen(String _statusCode) throws JSONException {
        currentStatusCode = _statusCode;
        //showDialog();
        //aq.id(R.id.delivery_action_layout).gone();
        //aq.id(R.id.pick_action_layout).gone();
        aq.id(R.id.pickup_alert_layout).gone();
        aq.id(R.id.paid_info_layout).gone();
        aq.id(R.id.below_layout).visible();
        String reqMessage = parentData.getString("pickupRequestMessage");
        if("".equals(reqMessage)){
            reqMessage = "요청사항 없음";
        }
        aq.id(R.id.req_message_text).text(reqMessage).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.btn_show).gone();

        if ("01".equals(_statusCode) || "02".equals(_statusCode)) {
            aq.id(R.id.text_status).text("수거접수");
            if(!"Y".equals(isNewPOS)){
                // 수거완료 버튼 표시
                aq.id(R.id.pickup_alert_layout).visible();
            }else{
                aq.id(R.id.pickup_alert_layout).gone();
            }

            aq.id(R.id.pick_action_layout).gone();
            aq.id(R.id.item_count_layout).gone();
            aq.id(R.id.add_area).gone();
            aq.id(R.id.count_add_area).gone();

            aq.id(R.id.time_title_p).text("주문접수일");
            aq.id(R.id.time_title_d).text("수거예정일");
            //aq.id(R.id.time_title).text("수거희망시간");
            String dps_p = displayTimeString(parentData.getLong("registerDateApp"));
            String dps_d = displayTimeString(parentData.getLong("pickupRequestTimeApp"));
            aq.id(R.id.time_content_p).text(dps_p).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.time_content_d).text(dps_d).typeface(CommonUtility.getNanumBarunTypeface());

            aq.id(R.id.item_count_layout).visible();
            aq.id(R.id.unit_count_layout).gone();
            aq.id(R.id.item_count_text).text("총 " + pickQuantity + "개");
            aq.id(R.id.pack_count_text).text("총 " + packQuantity + "개");
            aq.id(R.id.box_count_layout).gone();

        } else if ( "03".equals(_statusCode)) {
            aq.id(R.id.text_status).text("수거항목을 자세히 등록해주세요.");
            aq.id(R.id.time_title_p).text("주문접수일");
            aq.id(R.id.time_title_d).text("수거완료일");
            String dps_p = displayTimeString(parentData.getLong("registerDateApp"));
            String dps_d = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
            aq.id(R.id.time_content_p).text(dps_p).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.time_content_d).text(dps_d).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.add_area).visible();
            aq.id(R.id.count_add_area).gone();
            aq.id(R.id.btn_count_mod).gone();

            aq.id(R.id.item_count_layout).visible();
            aq.id(R.id.unit_count_layout).visible();
            aq.id(R.id.item_count_text).text("총 " + pickQuantity + "개");
            aq.id(R.id.pack_count_text).text("총 " + packQuantity + "개");
            aq.id(R.id.box_count_layout).gone();

            JSONArray count1 = new JSONArray(pickupItems);
            JSONArray count2 = new JSONArray(pickupRepairItems);
            JSONArray countV2 = pickupItemsV2;

            if (count1.length() > 0 || count2.length() > 0) {
                aq.id(R.id.pick_action_layout).visible();
            }else if(countV2.length() > 0){
                aq.id(R.id.pick_action_layout).visible();
            } else {
                aq.id(R.id.pick_action_layout).gone();
            }



            //aq.id(R.id.inner_code_layout).visible();
            aq.id(R.id.btn_complete_title).text("수거항목 등록완료");

            aq.id(R.id.alert_layout).gone();

        } else if ("11".equals(_statusCode) || "12".equals(_statusCode) || "13".equals(_statusCode)) {
            aq.id(R.id.pre_alert_area).gone();
            aq.id(R.id.add_area).gone();

            aq.id(R.id.count_add_area).gone();
            aq.id(R.id.add_area).gone();
            aq.id(R.id.btn_show).visible();
            aq.id(R.id.btn_mod).gone();

            aq.id(R.id.below_layout).visible();
            aq.id(R.id.item_count_layout).gone();

            if ("11".equals(_statusCode)) {
                aq.id(R.id.text_status).text("세탁 준비중");
            } else if ("12".equals(_statusCode)) {
                aq.id(R.id.text_status).text("세탁 중");
            } else if ("13".equals(_statusCode)) {
                aq.id(R.id.text_status).text("세탁완료");
            }
            aq.id(R.id.pickup_alert_layout).gone();
            aq.id(R.id.delivery_alert_layout).gone();
            aq.id(R.id.paid_info_layout).visible();
            aq.id(R.id.delivery_action_layout).gone();
/*
            aq.id(R.id.text_pickup_time_title).text("수거완료시간").typeface(CommonUtility.getNanumBarunTypeface());
            String dps = displayTimeString(pickupCompleteTime);
            aq.id(R.id.time_content).text(dps).typeface(CommonUtility.getNanumBarunLightTypeface());*/
            //
            //aq.id(R.id.time_title).text("배송희망시간");
            aq.id(R.id.time_title_p).text("수거완료일");
            aq.id(R.id.time_title_d).text("입고예정일");
            String dps_p = displayTimeString(pickupCompleteTime);
            String dps_d = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
            aq.id(R.id.time_content_p).text(dps_p).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.time_content_d).text(dps_d).typeface(CommonUtility.getNanumBarunTypeface());
            //

            aq.id(R.id.pick_action_layout).gone();

            JSONArray count1 = new JSONArray(pickupItems);
            JSONArray count2 = new JSONArray(pickupRepairItems);

            JSONArray countV2 = pickupItemsV2;

            if (count1.length() > 0 || count2.length() > 0) {
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).gone();
            }else if(pickupItemsV2.length() > 0){
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).visible();
            } else {
                aq.id(R.id.picks_layout).gone();
            }
            aq.id(R.id.total_layout).visible();
            //aq.id(R.id.text_delivery_area).visible();
        } else if ("21".equals(_statusCode) || "22".equals(_statusCode)) {
            aq.id(R.id.count_add_area).gone();
            aq.id(R.id.add_area).gone();
            aq.id(R.id.btn_show).visible();
            aq.id(R.id.btn_mod).gone();
            aq.id(R.id.paid_info_layout).visible();
            aq.id(R.id.item_count_layout).gone();
            aq.id(R.id.time_title_p).text("수거완료일");

            if ("21".equals(_statusCode)) {
                aq.id(R.id.text_status).text("배송 준비중");
                aq.id(R.id.pickup_alert_layout).gone();

                if(!"Y".equals(isNewPOS)){
                    aq.id(R.id.delivery_alert_layout).visible();
                }else{
                    aq.id(R.id.delivery_alert_layout).gone();
                }

                aq.id(R.id.delivery_action_layout).gone();

                aq.id(R.id.below_layout).visible();

                //aq.id(R.id.btn_change_p).visible();
                aq.id(R.id.btn_change_d).visible();
                // 주소표시하기로 함.
                aq.id(R.id.loc_layout).visible();

                aq.id(R.id.prod_layout).visible();

                if(boxQuantity == 0){
                    aq.id(R.id.item_count_layout).gone();
                    aq.id(R.id.btn_delivery_alert).text("배송박스 수량 입력하기");
                }else{
                    aq.id(R.id.item_count_layout).visible();
                    aq.id(R.id.unit_count_layout).gone();
                    aq.id(R.id.pack_count_layout).gone();
                    aq.id(R.id.box_count_layout).visible();

                    aq.id(R.id.box_count_text).text(boxQuantity + "개");
                    if ("151".equals(orderSubType)){
                        aq.id(R.id.btn_delivery_alert).text("편의점에 입고완료");
                    }else if ("152".equals(orderSubType)){
                        aq.id(R.id.btn_delivery_alert).text("플레이스에 입고완료");
                    }
                }

                aq.id(R.id.time_title_d).text("입고예정일");

            } else if ("22".equals(_statusCode)) {
                if("Y".equals(parentData.getString("isReceivable"))){
                    aq.id(R.id.text_status).text("입고완료(미수금)");
                    aq.id(R.id.paid_image).image(R.mipmap.paidinfo_paycomp_unpaid);

                    aq.id(R.id.btn_change_p).gone();
                    aq.id(R.id.btn_change_d).gone();
                }else {
                    aq.id(R.id.text_status).text("입고완료");
                }

                aq.id(R.id.btn_box_change).gone();
                aq.id(R.id.pickup_alert_layout).gone();
                aq.id(R.id.delivery_alert_layout).gone();
                aq.id(R.id.below_layout).visible();

                aq.id(R.id.item_count_text).text("총 " + pickQuantity + "개");
                aq.id(R.id.pack_count_text).text("총 " + packQuantity + "개");
                aq.id(R.id.box_count_text).text("총 " + boxQuantity + "개");

                aq.id(R.id.loc_layout).visible();
                aq.id(R.id.item_count_layout).visible();
                aq.id(R.id.prod_layout).visible();
                //aq.id(R.id.inner_code_layout).gone();
                //aq.id(R.id.pick_action_layout).gone();

                aq.id(R.id.time_title_d).text("입고완료일");
            }

            //aq.id(R.id.time_title).text("배송희망시간");
            //String dds = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
            //aq.id(R.id.time_content).text(dds).typeface(CommonUtility.getNanumBarunTypeface());


            String dps_p = displayTimeString(pickupCompleteTime);
            String dps_d = displayTimeString(parentData.getLong("deliveryRequestTimeApp"));
            aq.id(R.id.time_content_p).text(dps_p).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.time_content_d).text(dps_d).typeface(CommonUtility.getNanumBarunTypeface());
            //
            aq.id(R.id.add_area).gone();
            aq.id(R.id.pick_action_layout).gone();


            JSONArray count1 = new JSONArray(pickupItems);
            JSONArray count2 = new JSONArray(pickupRepairItems);
            JSONArray countV2 = pickupItemsV2;

            if (count1.length() > 0 || count2.length() > 0) {
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).gone();
            }else if(pickupItemsV2.length() > 0){
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).visible();
            } else {
                aq.id(R.id.picks_layout).gone();
            }
            //aq.id(R.id.picks_layout).visible();
            aq.id(R.id.total_layout).visible();

            //aq.id(R.id.text_delivery_area).visible();
        } else if ("23".equals(_statusCode) || "24".equals(_statusCode)) {
            aq.id(R.id.count_add_area).gone();
            aq.id(R.id.add_area).gone();
            aq.id(R.id.btn_show).visible();
            aq.id(R.id.btn_mod).gone();
            aq.id(R.id.paid_info_layout).visible();
            aq.id(R.id.item_count_layout).gone();

            if ("23".equals(_statusCode)) {
                aq.id(R.id.text_status).text("수령 완료");
            } else if ("24".equals(_statusCode)) {
                aq.id(R.id.text_status).text("방문수령 완료");
            }
            aq.id(R.id.paid_info_layout).visible();
            aq.id(R.id.pre_alert_area).gone();
            /*
            aq.id(R.id.text_pickup_time_title).text("수거완료시간").typeface(CommonUtility.getNanumBarunTypeface());
            String dps = displayTimeString(pickupCompleteTime);
            aq.id(R.id.text_pickup_time).text(dps).typeface(CommonUtility.getNanumBarunLightTypeface());*/

            //String dds = displayTimeString(deliveryCompleteTime);
            //aq.id(R.id.time_content).text(dds).typeface(CommonUtility.getNanumBarunTypeface());

            aq.id(R.id.time_title_p).text("수거완료일");
            aq.id(R.id.time_title_d).text("수령완료일");
            String dps_p = displayTimeString(pickupCompleteTime);
            String dps_d = displayTimeString(deliveryCompleteTime);
            aq.id(R.id.time_content_p).text(dps_p).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.time_content_d).text(dps_d).typeface(CommonUtility.getNanumBarunTypeface());
            //
            aq.id(R.id.add_area).gone();
            JSONArray count1 = new JSONArray(pickupItems);
            JSONArray count2 = new JSONArray(pickupRepairItems);
            JSONArray countV2 = pickupItemsV2;

            if (count1.length() > 0 || count2.length() > 0) {
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).gone();
            }else if(pickupItemsV2.length() > 0){
                aq.id(R.id.picks_layout).visible();
                aq.id(R.id.btn_show).visible();
            } else {
                aq.id(R.id.picks_layout).gone();
            }

            aq.id(R.id.pick_action_layout).gone();
            aq.id(R.id.total_layout).visible();
            //aq.id(R.id.text_delivery_area).visible();
            //aq.id(R.id.delivery_action_layout).visible();

            if ("106".equals(orderSubType)){
                aq.id(R.id.delivery_action_layout).gone();
                aq.id(R.id.reorder_layout).gone();
                //aq.id(R.id.order_delivery_comp_layout)
            }else{
                aq.id(R.id.delivery_action_layout).visible();
                aq.id(R.id.reorder_layout).visible();
                aq.id(R.id.order_delivery_comp_layout).gone();
            }

        } else {
            aq.id(R.id.item_count_layout).gone();
            aq.id(R.id.count_add_area).gone();
            aq.id(R.id.add_area).gone();
            if ("91".equals(_statusCode)) {
                aq.id(R.id.text_status).text("주문취소");
            } else if ("92".equals(_statusCode)) {
                aq.id(R.id.text_status).text("재세탁 신청");
            } else if ("93".equals(_statusCode)) {
                aq.id(R.id.text_status).text("환불");
            } else if ("94".equals(_statusCode)) {
                aq.id(R.id.text_status).text("보상요청");
            } else if ("95".equals(_statusCode)) {
                aq.id(R.id.text_status).text("고객사유로 취소");
            } else if ("96".equals(_statusCode)) {
                aq.id(R.id.text_status).text("주문 거부");
            }
            aq.id(R.id.pre_alert_area).gone();
            aq.id(R.id.add_area).gone();
            //aq.id(R.id.text_delivery_area).visible();
            aq.id(R.id.pick_action_layout).gone();
        }

        dismissDialog();
    }


    public void orderCompleteAction(View button) {
        //modStatus("23");

        if ("Y".equals(isPayment)) {

            if (payments.length() > 0) {
                if ("P91".equals(paymentType)) {
                    // 미수금 경고창
                    //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_card);
                    orderNotPaymentPopup();
                } else if ("P92".equals(paymentType)) {
                    // 미수금 경고창
                    //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_cash);
                    orderNotPaymentPopup();
                } else {
                    //
                    modStatus("23");
                    //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_already);
                }
            } else {
                //aq.id(R.id.paid_info_layout).gone();
                // 미수금 경고창
                orderNotPaymentPopup();
            }

        } else {
            // 미수금 경고창
            orderNotPaymentPopup();
        }
    }

    public void orderNotPaymentPopup(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(true).setMessage("결제가 완료되지 않은 주문 건입니다. 현금(혹은 현장카드결제)을 받으셨나요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        modStatus("23");
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                noPaymentDeliveryCompleteAction();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void noPaymentDeliveryCompleteAction(){
        showDialog();
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.NO_PAYMENT_STAY, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.NO_PAYMENT_STAY, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.NO_PAYMENT_STAY, result);

                    JSONObject jsondata = new JSONObject(result);
                    dismissDialog();
                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        aq.id(R.id.pre_alert_area).gone();
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
                        alertDialogBuilder.setCancelable(true).setMessage("배송은 완료되었지만, 미수금이 있어서 \"완료지연(미수금)\" 으로 표기됩니다.\n" +
                                "고객님 결제 완료 시 자동으로 \"배송완료\" 처리됩니다. ")
                                .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void addCompleteAction(View button) {

        try {
            JSONArray arr = new JSONArray(pickupItems);
            JSONArray arr2 = new JSONArray(pickupRepairItems);
            if (arr.length() > 0 || arr2.length() > 0) {
                submitPickup();
            } else {
                if ("Y".equals(isDeliveryMan)) {
                    submitPickup();
                } else {
                    DUtil.alertShow(this, "수거항목을 추가해주세요.");
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private int getTotalPrice() {
        int totalPrice = 0;
        try {
            //JSONArray arr1 = new JSONArray(pickupItems);

            for (int i = 0; i < pickupItemsV2.length(); i++) {
                int price = pickupItemsV2.getJSONObject(i).getInt("confirmPrice");
                totalPrice += price;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        //
        if (totalPrice < 20000 && totalPrice != 0) {
            totalPrice += 2000;
        }
        return totalPrice;
    }

    int prevTagNo = -1;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICKUP_ITEM:
                    try {
                        pickupItems = data.getStringExtra("data");
                        pickupItemsV2 = new JSONArray(pickupItems);

                        prevTagNo = data.getIntExtra("prevTagNo", -1);

                        JSONArray arr = new JSONArray(pickupItems);
                        //Snackbar.make(aq.getView(), "상품을 추가했습니다", Snackbar.LENGTH_SHORT).show();
                        //Toast.makeText(this, "상품을 추가했습니다", Toast.LENGTH_LONG).show();
                        Log.i("pickupItems list", pickupItemsV2.toString());
                        aq.id(R.id.total_layout).visible();
                        //int price = data.getIntExtra("price", 0);
                        String priceString = MZValidator.toNumFormat(getTotalPrice());

                        aq.id(R.id.text_total_price).text(priceString);

                        String picksText = "";
                        int count = 0;
                        if(arr.length() > 0){
                            picksText = arr.getJSONObject(0).getString("itemTitle");
                            aq.id(R.id.text_items).text(picksText + " 등 "+ arr.length() +"개");
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.add_area).gone();

                            if("106".equals(orderSubType)){
                                // 11번가 팝업
                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        Intent St11CompareIntent = new Intent(ctx, St11ComparePriceActivity.class);
                                        if (pickupItemsV2.length() != 0) {
                                            St11CompareIntent.putExtra("pickupItems", pickupItemsV2.toString());
                                            St11CompareIntent.putExtra("orderData", parentData.toString());
                                        }else{
                                            return;
                                        }
                                        St11CompareIntent.setAction(Intent.ACTION_GET_CONTENT);
                                        startActivityForResult(St11CompareIntent, ALERT_11ST_ADD_ITEM);
                                    }
                                }, 200);
                            }else{
                                submitPickup();
                            }

                            aq.id(R.id.pick_action_layout).visible();

                        }else {
                            aq.id(R.id.picks_layout).gone();
                            aq.id(R.id.add_area).visible();

                            JSONArray count1 = new JSONArray(pickupItems);
                            JSONArray count2 = new JSONArray(pickupRepairItems);
                            JSONArray countV2 = pickupItemsV2;

                            if (count1.length() > 0 || count2.length() > 0) {
                                aq.id(R.id.pick_action_layout).visible();
                            }else if(countV2.length() > 0){
                                aq.id(R.id.pick_action_layout).visible();
                            } else {
                                aq.id(R.id.pick_action_layout).gone();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
                case PICKUP_REPAIR_ITEM:
                    try {
                        pickupRepairItems = data.getStringExtra("data");
                        JSONArray arr = new JSONArray(pickupRepairItems);
                        //Snackbar.make(aq.getView(), "상품을 추가했습니다", Snackbar.LENGTH_SHORT).show();
                        //Toast.makeText(this, "상품을 추가했습니다", Toast.LENGTH_LONG).show();
                        Log.i("pickupRepairItems list", pickupRepairItems);
                        aq.id(R.id.total_layout).visible();
                        //int price = data.getIntExtra("price", 0);
                        String priceString = MZValidator.toNumFormat(getTotalPrice());

                        aq.id(R.id.text_total_price).text(priceString);

                        String picksText = "";
                        int count = 0;
                        for (int i = 0; i < arr.length(); i++) {
                            if ("N".equals(arr.getJSONObject(i).getString("isOption"))) {
                                picksText += arr.getJSONObject(i).getString("itemTitle") + " x" + arr.getJSONObject(i).getInt("itemQuantity");
                                count += arr.getJSONObject(i).getInt("itemQuantity");
                                if (i == arr.length() - 1) {
                                    picksText += "\n총 " + count + "건";
                                } else {
                                    picksText += ", ";
                                }
                            }
                        }

                        JSONArray count1 = new JSONArray(pickupItems);
                        JSONArray count2 = new JSONArray(pickupRepairItems);
                        JSONArray countV2 = pickupItemsV2;

                        if (count1.length() > 0) {
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.btn_show).gone();
                        }else if(countV2.length() > 0){
                            aq.id(R.id.picks_layout).visible();
                            aq.id(R.id.btn_show).visible();
                        } else {

                            if (count1.length() > 0 || count2.length() > 0) {
                                aq.id(R.id.pick_action_layout).visible();
                            }else if(countV2.length() > 0){
                                aq.id(R.id.pick_action_layout).visible();
                            } else {
                                aq.id(R.id.picks_layout).gone();
                                aq.id(R.id.pick_action_layout).gone();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case PICKUP_COUNT:
                    pickQuantity = data.getIntExtra("result", 0);
                    if (pickQuantity > 0) {
                        //aq.id(R.id.picks_layout).visible();
                        aq.id(R.id.item_count_text).text("총 " + pickQuantity + "개");

                        aq.id(R.id.item_count_layout).visible();
                        aq.id(R.id.pick_action_layout).visible();
                        aq.id(R.id.btn_count_add).gone();
                    } else {
                        //aq.id(R.id.picks_layout).gone();

                        //aq.id(R.id.item_count_layout).gone();
                        aq.id(R.id.pick_action_layout).gone();
                        aq.id(R.id.btn_count_add).visible();
                    }
                    submitPickup();

                    break;
                case MOD_DELIVERY_OK:
                    Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("USER")
                            .setAction("배송요청일 변경")
                            .setLabel("Order")
                            .build());

                    Toast.makeText(this, "배송요청정보가 변경되었습니다.", Toast.LENGTH_LONG).show();
                    getRemoteData(orderId);

                    break;
                case MOD_PICKUP_OK:
                    Tracker t1 = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                    t1.send(new HitBuilders.EventBuilder()
                            .setCategory("USER")
                            .setAction("수거요청일 변경")
                            .setLabel("Order")
                            .build());

                    Toast.makeText(this, "수거,배송 요청정보가 변경되었습니다.", Toast.LENGTH_LONG).show();
                    getRemoteData(orderId);
                    break;
                case SHOW_ITEM:
                    getRemoteData(orderId);
                    break;
                case ALERT_11ST_ADD_ITEM:
                    submitPickup();
                    break;
                case CREATE_ORDER:
                    isReordered = true;
                    aq.id(R.id.reorder_layout).gone();
                    Toast.makeText(this, "주문이 정상적으로 생성되었습니다.", Toast.LENGTH_LONG).show();
                    break;

            }
        }
    }

    boolean isReordered = false;

    public void showItemsAction(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent pickIntent = new Intent(ctx, OrderItemElementActivity.class);
                if (pickupItemsV2.length() != 0) {
                    pickIntent.putExtra("pickupItems", pickupItemsV2.toString());
                    pickIntent.putExtra("orderId", orderId);
                    pickIntent.putExtra("orderStatus", orderStatus);
                }else{
                    return;
                }
                pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(pickIntent, SHOW_ITEM);
            }
        }, 200);

    }


    public void addItemsAction(View button) {
        final int tag = (int) button.getTag();

        Log.e("addItemsAction", tag + "" );

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent pickIntent = new Intent(ctx, PickupItemBucketActivity.class);
                //Intent pickIntent = new Intent(ctx, PriceActivity.class);
                Intent pickCountIntent = new Intent(ctx, PickCountScreen.class);
                if (pickQuantity > 0) {
                    pickCountIntent.putExtra("pickCount", pickQuantity);
                    pickIntent.putExtra("pickCount", pickQuantity);
                }

                int pickupMode;
                switch (tag) {
                    case 0:

                        pickCountIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickCountIntent, PICKUP_COUNT);
                        break;
                    case 1:
                        pickupMode = PickupType.PICKUPTYPE_GS;
                        pickIntent.putExtra("pickupItems", pickupItems);
                        pickIntent.putExtra("pickupMode", pickupMode);
                        pickIntent.putExtra("prevTagNo", prevTagNo);
                        pickIntent.putExtra("partnerId", partnerId);
                        pickIntent.putExtra("priceGroupId", priceGroupId);
                        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickIntent, PICKUP_ITEM);
                        break;
                    default:
                        pickupMode = PickupType.PICKUPTYPE_GS;
                        pickIntent.putExtra("pickupItems", pickupItems);
                        pickIntent.putExtra("pickupMode", pickupMode);
                        pickIntent.putExtra("prevTagNo", prevTagNo);
                        pickIntent.putExtra("partnerId", partnerId);
                        pickIntent.putExtra("priceGroupId", priceGroupId);
                        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(pickIntent, PICKUP_ITEM);
                        break;
                }
            }
        }, 200);
    }

    public void preAlertAction(View button) {

        if(orderStatus == null){
            return;
        }

        if("01".equals(orderStatus)){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setCancelable(true)
                    .setMessage("수거봉투 " + packQuantity +"개\n\n총 수량이 맞나요?\n변경이 불가능하니 정확하게 입력해주세요.")
                    .setPositiveButton("네", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            submitProcess(orderStatus, orderId);
                        }})
                    .setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else if("21".equals(orderStatus)){
            if(boxQuantity == 0){
                unitNumberPickerShow(orderId);
            }else{
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setCancelable(true)
                        .setMessage("배송박스 " + boxQuantity +"개\n\n총 수량이 맞나요?\n변경이 불가능하니 정확하게 입력해주세요.")
                        .setPositiveButton("네", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                submitProcess(orderStatus, orderId);
                            }})
                        .setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        }else{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setCancelable(true)
                    .setMessage("해당 주문건의 상태값이 비정상입니다. 고객센터로 문의부탁드립니다.")
                    .setPositiveButton("네", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }});
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

    }

    public void modBoxCountAction(View button){
        if("21".equals(orderStatus)) {
            unitNumberPickerShow(orderId);
        }
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

        Log.i("value is",""+newVal);

    }

    int inputBoxCount = 0;

    @SuppressLint("NewApi")
    private void unitNumberPickerShow(final int _orderId){
        final Dialog d = new Dialog(GSOrderDetailActivity.this);
        d.setTitle("배송박스 수량 입력" + partnerId);
        if("7".equals(partnerId)){
            d.setContentView(R.layout.dialog_unit_picker);
        }else{
            d.setContentView(R.layout.dialog_place_unit_picker);
        }

        Button b1 = (Button) d.findViewById(R.id.btn_unit_cancel);
        Button b2 = (Button) d.findViewById(R.id.btn_unit_ok);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.unitPicker);
        np.setDividerPadding(1);
        np.setDividerDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.hor_divider, null));
        np.setMaxValue(100);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        b2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                //tv.setText(String.valueOf(np.getValue()));
                Log.e("unitPick", String.valueOf(np.getValue()));
                inputBoxCount = np.getValue();

                if(inputBoxCount > 0){
                    inputBoxQuantity(_orderId, inputBoxCount);
                }
                d.dismiss();
            }
        });
        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    private void inputBoxQuantity(final int _orderId, int boxQuantity){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        params.put("boxQuantity", boxQuantity);
        params.put("k", 1);

        NetworkClient.post(Constants.PARTNER_ORDER_BOX_QUANTITY, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.PARTNER_ORDER_BOX_QUANTITY, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.PARTNER_ORDER_BOX_QUANTITY, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        getRemoteData(_orderId);
                    } else {

                    }

                    dismissDialog();

                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    dismissDialog();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    dismissDialog();
                }
            }
        });
    }

    public void submitProcess(String _orderStatus, final int _orderId){
        showDialog();

        String reqOrderStatus = "";
        if("01".equals(_orderStatus)){
            reqOrderStatus = "03";
        }else if("21".equals(_orderStatus)){
            reqOrderStatus = "22";
        }else{
            dismissDialog();
            ToastUtility.show(this, "변경하려는 주문상태가 비정상입니다. 리화이트 고객센터에 부탁드립니다.");
            return;
        }

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        params.put("orderStatus", reqOrderStatus);
        params.put("partnerId", partnerId);
        params.put("partnerStoreId", partnerStoreId);
        params.put("k", 1);

        NetworkClient.post(Constants.PARTNER_ORDER_STATUS, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.PARTNER_ORDER_STATUS, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.PARTNER_ORDER_STATUS, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        getRemoteData(_orderId);
                    } else {

                    }

                    dismissDialog();

                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    dismissDialog();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    dismissDialog();
                }
            }
        });
    }

    private void modStatus(final String _orderStatus) {
        showDialog();
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("orderStatus", _orderStatus);
        params.put("k", 1);

        Log.e("modStatus Args", params.toString());

        NetworkClient.post(Constants.MOD_ORDER_STATUS, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.MOD_ORDER_STATUS, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.MOD_ORDER_STATUS, result);

                    JSONObject jsondata = new JSONObject(result);
                    dismissDialog();
                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        aq.id(R.id.pre_alert_area).gone();
                        orderStatus = _orderStatus;

                        if ("02".equals(orderStatus) || "22".equals(orderStatus)) {

                            aq.id(R.id.below_layout).visible();
                            aq.id(R.id.alert_layout).gone();
                            setStatusScreen(orderStatus);
                        } else if ("23".equals(orderStatus)) {
                            ToastUtility.show(ctx, "배송완료 처리되었습니다", Toast.LENGTH_SHORT);
                            finish();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), jsondata.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }


    /*
        수거완료 처리
     */
    private void submitPickup() {
        showDialog();

//        if (getTotalPrice() < 0) {
//            DUtil.alertShow(this, "총금액이 만원이하 입니다.");
//            dismissDialog();
//            return;
//        }

        String api_url = "";
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);

        // 2015-11-06
        // 세탁소로 이동중 -> 세탁준비중 으로 단계축소처리
        //params.put("orderStatus", "03");
        if ("02".equals(orderStatus) || "01".equals(orderStatus)) {
            params.put("orderStatus", "03");
            params.put("pickupQuantity", pickQuantity);
            api_url = Constants.ORDER_PICKUP_COMPLETE;
            if (pickQuantity == 0) {
                DUtil.alertShow(this, "수량을 추가해주세요.");
                dismissDialog();
                return;
            }
        } else if ("03".equals(orderStatus)){
            // 수량입력후 품목 등록
            //params.put("orderStatus", "11");
            params.put("items", pickupItemsV2.toString());
            // V2 API로 교체
            api_url = Constants.ORDER_PICKUP_COMPLETE_V2;

            if (pickupItemsV2.length() == 0) {
                DUtil.alertShow(this, "세탁물을 추가해주세요.");
                dismissDialog();
                return;
            }
        }else{
            return;
        }

        params.put("k", 1);
        Log.i("SUBMIT PARAM", params.toString());

        NetworkClient.post(api_url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_PICKUP_COMPLETE, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_PICKUP_COMPLETE, result);

                    dismissDialog();
                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //
                            //Toast.makeText(getApplicationContext(), "접수를 완료했습니다.", Toast.LENGTH_LONG).show();
                        } else {
                            //JSONArray locationInfo = jsondata.getJSONArray("data");
                            Toast.makeText(getApplicationContext(), "접수를 완료했습니다.", Toast.LENGTH_LONG).show();
                            getRemoteData(orderId);
                            //finish();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), jsondata.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }
}
