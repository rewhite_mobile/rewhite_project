package me.rewhite.delivery.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.common.logic.Constants;


@SuppressLint({"NewApi", "ValidFragment", "JavascriptInterface", "SetJavaScriptEnabled"})
public class TermsDetailView extends BaseActivity {

    SharedPreferences preferences;
    private AQuery aq;
    WebView webView;
    ProgressDialog dialog;
    public Context ctx = this;

    String currentTerms;
    String currentTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_terms_detail_view);

        Intent myIntent = getIntent();
        currentTerms = myIntent.getExtras().getString("TERMS_URI");
        currentTitle = myIntent.getExtras().getString("TERMS_TITLE");

        preferences = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
        aq = new AQuery(this);
        aq.id(R.id.title_text).text(currentTitle);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        webView = (WebView) findViewById(R.id.webview_area);
        webView.setWebViewClient(new MyWebViewClient());
        // webView.addJavascriptInterface(new
        // JavaScriptInterface(this.getActivity()), "SEP");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(currentTerms);

        // Progress 처리 진행상황을 보기위해
        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.web_loading_comment));
    }

    public void closeClicked(View button) {
        finish();
    }

    boolean loadingFinished = true;
    boolean redirect = false;

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
            if (!loadingFinished) {
                redirect = true;
            }

            loadingFinished = false;
            view.loadUrl(urlNewString);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap facIcon) {
            loadingFinished = false;
            // SHOW LOADING IF IT ISNT ALREADY VISIBLE
            dialog.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (!redirect) {
                loadingFinished = true;
            }

            if (loadingFinished && !redirect) {
                // HIDE LOADING IT HAS FINISHED
                dialog.dismiss();
            } else {
                redirect = false;
            }

        }
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }
    }
}
