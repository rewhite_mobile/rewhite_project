package me.rewhite.delivery.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.PickupItemAdapter;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.data.PickupType;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class PriceActivity extends BaseActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private AQuery aq;
    private ViewPager mViewPager;
    public final Context ctx = this;
    private JSONArray pickupItems;
    private int pickupMode = PickupType.PICKUPTYPE_CLEAN;

    public void closeClicked(View button) {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price);

        aq = new AQuery(this);
        //pickupItems
        aq.id(R.id.btn_back).clicked(this, "closeClicked");

        Intent intent = getIntent();
        if (intent != null) {
            try {
                String pString = intent.getStringExtra("pickupItems");
                if (pString != null) {
                    pickupItems = new JSONArray(intent.getStringExtra("pickupItems"));
                    Log.i("pickupItems", pickupItems.toString());
                    pickupMode = intent.getIntExtra("pickupMode", PickupType.PICKUPTYPE_CLEAN);
                    if(pickupMode == PickupType.PICKUPTYPE_CLEAN){
                        aq.id(R.id.title_text).text("세탁 항목 등록"); // 일반/기본
                    }else if(pickupMode == PickupType.PICKUPTYPE_REPAIR){
                        aq.id(R.id.title_text).text("수선 항목 등록"); // 수선
                    }else{
                        aq.id(R.id.title_text).text("세탁 항목 등록"); // 일반/기본
                    }

                    initialize(pickupItems);
                } else {
                    initialize(null);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
/*
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    Intent resultData = new Intent();
                    resultData.putExtra("data", sendDataSet.toString());
                    resultData.putExtra("price", getTotalPrice(sendDataSet));
                    setResult(Activity.RESULT_OK, resultData);
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private int getTotalPrice(JSONArray data) throws JSONException {
        int price = 0;
        for (int i = 0; i < data.length(); i++) {
            int p = data.getJSONObject(i).getInt("itemPrice");
            int c = data.getJSONObject(i).getInt("itemQuantity");
            price += p * c;
        }
        return price;
    }

    JSONArray itemInfo;
    TabLayout tabLayout;

    private void initialize(final JSONArray preSelectedItem) {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        String pickupModeParam = "G";
        if(pickupMode == PickupType.PICKUPTYPE_CLEAN){
            pickupModeParam = "G"; // 일반/기본
        }else if(pickupMode == PickupType.PICKUPTYPE_REPAIR){
            pickupModeParam = "R"; // 수선
        }else if(pickupMode == PickupType.PICKUPTYPE_TODAY){
            pickupModeParam = "T"; // 수선
        }else if(pickupMode == PickupType.PICKUPTYPE_TOMORROW){
            pickupModeParam = "N"; // 수선
        }else if(pickupMode == PickupType.PICKUPTYPE_B2B_HANDYS){
            pickupModeParam = "B"; // 수선
        }
        params.put("mode", pickupModeParam);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_PRICE_TABLE, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_PRICE_TABLE, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_PRICE_TABLE, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //

                        } else {
                            itemInfo = jsondata.getJSONArray("data");
                            if (preSelectedItem != null) {
                                for (int i = 0; i < itemInfo.length(); i++) {
                                    JSONArray subData = itemInfo.getJSONObject(i).getJSONArray("items");
                                    for (int j = 0; j < subData.length(); j++) {
                                        int subDataItemId = subData.getJSONObject(j).getInt("itemId");
                                        for (int k = 0; k < preSelectedItem.length(); k++) {
                                            int compData = preSelectedItem.getJSONObject(k).getInt("itemId");
                                            if (subDataItemId == compData) {
                                                itemInfo.getJSONObject(i).getJSONArray("items").getJSONObject(j).put("itemQuantity", preSelectedItem.getJSONObject(k).getInt("itemQuantity"));
                                                Log.i("ADDED", itemInfo.getJSONObject(i).getJSONArray("items").getJSONObject(j).toString());
                                            }
                                        }

                                        if("Y".equals(subData.getJSONObject(j).getString("isOption"))){
                                            JSONObject optionItem = subData.getJSONObject(j);
                                            addAdditionalAction(i, j, optionItem);
                                        }

                                    }
                                }
                                sendDataSet = createFinalData();
                                Log.i("SEND DATA SET", sendDataSet.toString());
                                //mSectionsPagerAdapter.notifyDataSetChanged();
                                //mViewPager.invalidate();
                            }

                            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
                            // Set up the ViewPager with the sections adapter.
                            mViewPager = (ViewPager) findViewById(R.id.container);
                            mViewPager.setAdapter(mSectionsPagerAdapter);

                            tabLayout = (TabLayout) findViewById(R.id.tabs);
                            tabLayout.setupWithViewPager(mViewPager);
                            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                                @Override
                                public void onTabSelected(TabLayout.Tab tab) {
                                    currentTabPosition = tab.getPosition();
                                    mViewPager.setCurrentItem(currentTabPosition, true);
                                    setCurrentAdditionalBar();
                                }

                                @Override
                                public void onTabUnselected(TabLayout.Tab tab) {

                                }

                                @Override
                                public void onTabReselected(TabLayout.Tab tab) {
                                    currentTabPosition = tab.getPosition();
                                }
                            });
                            setCurrentAdditionalBar();
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    int currentTabPosition = 0;

    private String[] itemCategory = {"상의", "하의", "원피스", "아우터", "기타", "악세사리", "이불", "카페트", "커텐", "추가비용"};

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(ctx, position);
        }

        @Override
        public int getCount() {
            return itemInfo.length();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = "";
            try {
                title = itemInfo.getJSONObject(position).getString("itemTitle");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return title;
        }

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    @SuppressLint("ValidFragment")
    public static class PlaceholderFragment extends Fragment {
        private RecyclerView mRecyclerView;
        private RecyclerView.Adapter mAdapter;
        private RecyclerView.LayoutManager mLayoutManager;
        public Context parent;
        private AQuery bq;

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_POSITION = "section_number";
//        private static final String ARG_DATA = "section_data";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(Context ctx, int position) {

            PlaceholderFragment fragment = new PlaceholderFragment(ctx);
            Bundle args = new Bundle();
            args.putInt(ARG_POSITION, position);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        public PlaceholderFragment(Context ctx) {
            parent = ctx;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_price, container, false);
            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.item_list);
            bq = new AQuery(rootView);
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            mRecyclerView.setHasFixedSize(true);

            // use a linear layout manager
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);

            // specify an adapter (see also next example)
            try {

                JSONArray subArr = ((PriceActivity) getActivity()).itemInfo.getJSONObject(getArguments().getInt(ARG_POSITION)).getJSONArray("items");
                if (subArr.length() == 0) {
                    bq.id(R.id.screen_no).visible();
                } else {
                    bq.id(R.id.screen_no).gone();
                }
                mAdapter = new PickupItemAdapter(parent, getArguments().getInt(ARG_POSITION), subArr);
                mRecyclerView.setAdapter(mAdapter);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return rootView;
        }
    }

    private JSONArray sendDataSet = new JSONArray();

    public void addRemoveAction(int category, int position, boolean isAddType) throws JSONException {
        JSONObject item = itemInfo.getJSONObject(category).getJSONArray("items").getJSONObject(position);
        String isOptionString = item.getString("isOption");

        if (isAddType) {
            // 수량추가
            if (item.isNull("itemQuantity")) {
                item.put("itemQuantity", 1);
                item.put("isOption", isOptionString);
            } else {
                int quantity = item.getInt("itemQuantity");
                item.put("itemQuantity", quantity + 1);
                item.put("isOption", isOptionString);
            }
        } else {
            if (item.isNull("itemQuantity")) {
                //item.put("itemQuantity", 1);
            } else {
                int quantity = item.getInt("itemQuantity");
                if (quantity <= 1) {
                    item.put("itemQuantity", 0);
                } else {
                    item.put("itemQuantity", quantity - 1);
                    item.put("isOption", isOptionString);
                }
            }
        }

        sendDataSet = createFinalData();
        Log.i("SEND DATA SET", sendDataSet.toString());
        mSectionsPagerAdapter.notifyDataSetChanged();
        mViewPager.invalidate();
    }

    // 추가비용항목 별도처리
    JSONArray additionalData = new JSONArray();
    public void addAdditionalAction(int section, int position, JSONObject additionalValue){
        JSONObject item = new JSONObject();
        Log.i("additionalData", section + ":::"+ additionalValue.toString());
        try {
            item.put("section", section);
            item.put("position", position);
            item.put("data", additionalValue);
            additionalData.put(item);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //
    int currentSection = 0;
    int currentPosition = 0;

    public void setCurrentAdditionalBar(){

        Log.e("SET ::", "::: "+ currentTabPosition);
        currentSection = currentTabPosition;//tabLayout.getSelectedTabPosition();
        aq.id(R.id.btn_addition_plus).clicked(this, "additionValAddAction").tag(0);
        aq.id(R.id.btn_addition_minus).clicked(this, "additionValRemoveAction").tag(0);

        aq.id(R.id.btn_discount_plus).clicked(this, "additionValAddAction").tag(1);
        aq.id(R.id.btn_discount_minus).clicked(this, "additionValRemoveAction").tag(1);

        try {
            int cate = 0;
            int pos =0;
            int quantity = 0;
            int addPos = 0;
            int discountQuantity = 0;
            int discountPos = 0;

            aq.id(R.id.text_addition_count).text("0 원");
            aq.id(R.id.text_discount_count).text("0 원");
            aq.id(R.id.discount_area).gone();

            for(int i = 0 ; i < additionalData.length(); i++){
                if(additionalData.getJSONObject(i).getInt("section") == tabLayout.getSelectedTabPosition()){
                    cate = additionalData.getJSONObject(i).getInt("section");
                    pos = additionalData.getJSONObject(i).getInt("position");

                    if(additionalData.getJSONObject(i).getJSONObject("data").getInt("storePrice") < 0){
                        discountPos = pos;
                        aq.id(R.id.discount_area).visible();
                    }else{
                        addPos = pos;
                    }

                    if(!itemInfo.getJSONObject(cate).getJSONArray("items").getJSONObject(pos).isNull("itemQuantity")){
                        if(itemInfo.getJSONObject(cate).getJSONArray("items").getJSONObject(pos).getInt("storePrice") < 0){
                            discountQuantity = itemInfo.getJSONObject(cate).getJSONArray("items").getJSONObject(pos).getInt("itemQuantity");
                        }else{
                            quantity = itemInfo.getJSONObject(cate).getJSONArray("items").getJSONObject(pos).getInt("itemQuantity");
                        }

                    }
                }
            }
            aq.id(R.id.text_addition_count).text(1000*quantity + " 원");
            aq.id(R.id.text_discount_count).text(-1000*discountQuantity + " 원");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void additionValAddAction(View button){
        int tag = (int)button.getTag();
        try {
            int cate = 0;
            int pos =0;
            int quantity = 0;
            int addPos = 0;
            int discountQuantity = 0;
            int discountPos = 0;

            aq.id(R.id.text_addition_count).text("0 원");
            aq.id(R.id.text_discount_count).text("0 원");

            for(int i = 0 ; i < additionalData.length(); i++){
                if(additionalData.getJSONObject(i).getInt("section") == currentSection){
                    cate = additionalData.getJSONObject(i).getInt("section");
                    pos = additionalData.getJSONObject(i).getInt("position");
                    if(additionalData.getJSONObject(i).getJSONObject("data").getInt("storePrice") < 0){
                        discountPos = pos;
                    }else{
                        addPos = pos;
                    }

                    if(!itemInfo.getJSONObject(cate).getJSONArray("items").getJSONObject(pos).isNull("itemQuantity")){
                        if(itemInfo.getJSONObject(cate).getJSONArray("items").getJSONObject(pos).getInt("storePrice") < 0){
                            discountQuantity = itemInfo.getJSONObject(cate).getJSONArray("items").getJSONObject(pos).getInt("itemQuantity");
                        }else{
                            quantity = itemInfo.getJSONObject(cate).getJSONArray("items").getJSONObject(pos).getInt("itemQuantity");
                        }
                    }

                }
            }

            if(tag == 0){
                addRemoveAction(cate, addPos, true);
                quantity++;
            }else{
                addRemoveAction(cate, discountPos, true);
                discountQuantity++;
            }
            aq.id(R.id.text_addition_count).text(1000 * (quantity) + " 원");
            aq.id(R.id.text_discount_count).text(-1000*(discountQuantity) + " 원");


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public void additionValRemoveAction(View button){
        int tag = (int)button.getTag();
        try {
            int cate = 0;
            int pos =0;
            int quantity = 0;
            int addPos = 0;
            int discountQuantity = 0;
            int discountPos = 0;

            aq.id(R.id.text_addition_count).text("0 원");
            aq.id(R.id.text_discount_count).text("0 원");

            for(int i = 0 ; i < additionalData.length(); i++){
                if(additionalData.getJSONObject(i).getInt("section") == currentSection){
                    cate = additionalData.getJSONObject(i).getInt("section");
                    pos = additionalData.getJSONObject(i).getInt("position");
                    if(additionalData.getJSONObject(i).getJSONObject("data").getInt("storePrice") < 0){
                        discountPos = pos;
                    }else{
                        addPos = pos;
                    }

                    if(!itemInfo.getJSONObject(cate).getJSONArray("items").getJSONObject(pos).isNull("itemQuantity")){
                        if(itemInfo.getJSONObject(cate).getJSONArray("items").getJSONObject(pos).getInt("storePrice") < 0){
                            discountQuantity = itemInfo.getJSONObject(cate).getJSONArray("items").getJSONObject(pos).getInt("itemQuantity");
                        }else{
                            quantity = itemInfo.getJSONObject(cate).getJSONArray("items").getJSONObject(pos).getInt("itemQuantity");
                        }
                    }
                }
            }

            if(tag == 0){
                addRemoveAction(cate, addPos, false);
                quantity--;
                if(quantity < 0){
                    quantity = 0;
                }
            }else{
                addRemoveAction(cate, discountPos, false);
                discountQuantity--;
                if(discountQuantity < 0){
                    discountQuantity = 0;
                }
            }
            aq.id(R.id.text_addition_count).text(1000*(quantity) + " 원");
            aq.id(R.id.text_discount_count).text(-1000*(discountQuantity) + " 원");


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONArray createFinalData() throws JSONException {
        JSONArray tempArr = new JSONArray();
        int totalPrice = 0;

        for (int i = 0; i < itemInfo.length(); i++) {
            JSONArray t = itemInfo.getJSONObject(i).getJSONArray("items");
            for (int j = 0; j < t.length(); j++) {
                if (!t.getJSONObject(j).isNull("itemQuantity")) {
                    int itemId = t.getJSONObject(j).getInt("itemId");
                    int itemPrice = t.getJSONObject(j).getInt("storePrice");
                    int itemQuantity = t.getJSONObject(j).getInt("itemQuantity");
                    String itemTitle = t.getJSONObject(j).getString("itemTitle");
                    String isOption = t.getJSONObject(j).getString("isOption");

                    if (itemQuantity != 0) {
                        JSONObject data = new JSONObject();
                        data.put("itemId", itemId);
                        data.put("itemPrice", itemPrice);
                        data.put("itemQuantity", itemQuantity);
                        data.put("itemTitle", itemTitle);
                        data.put("isOption", isOption);

                        totalPrice += itemPrice*itemQuantity;

                        tempArr.put(data);
                    }
                }
            }
        }

        aq.id(R.id.total_text).text("총 "+ MZValidator.toNumFormat(totalPrice) + "원");

        return tempArr;
    }
}
