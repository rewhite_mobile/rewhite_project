package me.rewhite.delivery.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;

@SuppressLint({ "NewApi", "JavascriptInterface", "SetJavaScriptEnabled" })
public class HtmlActivity extends BaseActivity {

    private AQuery aq;
    WebView webView;
    ProgressDialog dialog;
    public Context ctx = this;

    String currentTerms;
    String currentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_html);

        Intent myIntent = getIntent();
        currentTerms = myIntent.getExtras().getString("TERMS_URI");
        currentName = myIntent.getExtras().getString("TITLE_NAME");


        aq = new AQuery(this);
        aq.id(R.id.title_text).text(currentName);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        webView = (WebView)findViewById(R.id.webview_area);
        webView.setWebViewClient(new MyWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.clearCache(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.loadUrl(currentTerms);
        //aq.id(R.id.top).gone();
		/*
		aq.id(R.id.title_name).text("FAQ");
		aq.id(R.id.btn_setting).clicked(this, "settingStart");
		aq.id(R.id.btn_info).clicked(this, "infoStart");*/

        // Progress 처리 진행상황을 보기위해
        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.web_loading_comment));
    }
    boolean loadingFinished = true;
    boolean redirect = false;

    public void closeClicked(View button){
        finish();
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
            if (!loadingFinished) {
                redirect = true;
            }

            loadingFinished = false;
            view.loadUrl(urlNewString);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap facIcon) {
            loadingFinished = false;
            // SHOW LOADING IF IT ISNT ALREADY VISIBLE
            dialog.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (!redirect) {
                loadingFinished = true;
            }

            if (loadingFinished && !redirect) {
                // HIDE LOADING IT HAS FINISHED
                dialog.dismiss();
            } else {
                redirect = false;
            }

        }
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }
    }
}
