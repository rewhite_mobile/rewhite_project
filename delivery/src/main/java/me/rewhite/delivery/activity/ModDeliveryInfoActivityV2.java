package me.rewhite.delivery.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.fragment.IFragment;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.DialogUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.TimeUtil;

public class ModDeliveryInfoActivityV2 extends BaseActivity {

    private final static String TAG = ModDeliveryInfoActivityV2.class.getSimpleName();
    public static final String FRAGMENT_DELIVERY = "order_delivery-mod";

    public final Context mCtx = this;
    public JSONObject storeInfo;
    public long deliveryReqTime = 0;
    public long pickupReqTime = 0;
    public String storeClosingDay;
    public String storeDayoff;
    public String deliveryRequestMessage;
    public int washingDays = 3;// 배달일기준 : 수거일기준 3일후
    public int synchronizedLimitTime = 30;

    String orderId;
    long deliveryRequestTime;
    int userId;
    int addressSeq;

    JSONArray arr;
    JSONArray pickArr;
    private AQuery aq;
    public RequestParams modParams;
    int selectedDatePos1 = -1;
    int datePos1 = -1;
    int timePos1 = -1;
    int selectedDatePos2 = -1;
    int datePos2 = -1;
    int timePos2 = -1;
    int totalCount = 0;
    String mode = "D";
    String currentMode = "D";

    private boolean mIsTouchDisable; // 연속 터치로 생기는 이슈 막기 위함
    public JSONObject orderInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_mod);

        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        aq.id(R.id.title_text).typeface(CommonUtility.getNanumBarunTypeface());

        Intent intent = getIntent();
        if (intent != null) {
            try {
                String info = intent.getStringExtra("INFO");
                mode = intent.getStringExtra("MODE");
                orderInfo = new JSONObject(info);
                deliveryReqTime = orderInfo.getLong("deliveryRequestTimeApp");
                pickupReqTime = orderInfo.getLong("pickupRequestTimeApp");
                //washingDays = orderInfo.getInt("washingDays");
                orderId = intent.getStringExtra("orderId");
                userId = intent.getIntExtra("userId", 0);
                addressSeq = intent.getIntExtra("addressSeq", 0);

                currentMode = mode;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if("D".equals(mode)){
            Date pDate = new Date();
            pDate.setTime(pickupReqTime);
            aq.id(R.id.main_d_textview).text(TimeUtil.getDateTimeType5Format(pDate));
            aq.id(R.id.main_t_textview).text(TimeUtil.convertTimestampToStringTime(pickupReqTime));
            getOrderAvailableTime(pickupReqTime, mode);
        }else{
            aq.id(R.id.main_d_textview).text("수거일자를 선택해주세요.");
            aq.id(R.id.main_t_textview).text("수거시간을 선택해주세요.");
            getOrderAvailableTime(0, mode);
        }

        aq.id(R.id.btn_req).clicked(this, "deliveryRequest");
        aq.id(R.id.btn_submit).clicked(this, "deliveryTimeChangeAction");

    }

    final String items[] = {"고객의 부재로 인해 임의변경", "고객의 요청으로 변경", "세탁소 사정으로 변경"};
    int selectedIndex = 0;
    String messageString = "";

    public void deliveryRequest(View button) {
        android.support.v7.app.AlertDialog.Builder ab = new android.support.v7.app.AlertDialog.Builder(this);
        ab.setTitle("변경사유를 선택해주세요.");
        ab.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedIndex = which;
            }
        }).setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                messageString = items[selectedIndex];
                aq.id(R.id.text_message).text(messageString).typeface(CommonUtility.getNanumBarunTypeface());
                deliveryRequestMessage = (selectedIndex+1) + "";
            }
        }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        ab.show();
    }

    public void setTitle(String _title) {
        aq.id(R.id.title_text).text(_title);
    }

    public void closeClicked(View button) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        DUtil.Log(TAG, "[onBackPressed] Back Stack Entry Count : " + fm.getBackStackEntryCount());

        if (fm.getBackStackEntryCount() >= 1) {

            String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
            IFragment fragment = (IFragment) fm.findFragmentByTag(tag);
            if (false == fragment.onBackPressed()) {

                if (fm.getBackStackEntryCount() == 1) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                    alertDialogBuilder.setCancelable(true).setMessage("배송일 변경신청이 완료되지 않았어요.\n중단하시겠어요?")
                            .setPositiveButton("네", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {

                    //clearCaptureView();

                    fm.popBackStack();

                }
            }
        } else {
            super.onBackPressed();
        }
        //finish();
        //super.onBackPressed();
    }

    public void modInfoExcute(RequestParams params){
        final String apiUrl;
        if("D".equals(mode)){
            apiUrl = Constants.ORDER_MOD;
        }else{
            apiUrl = Constants.ORDER_PICKUP_MOD;
        }
        NetworkClient.post(apiUrl, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(apiUrl, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(apiUrl, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            //JSONArray orderInfo = jsondata.getJSONArray("data");

                            Intent resultData = new Intent();
                            //resultData.putExtra("content", orderInfo.toString());
                            setResult(Activity.RESULT_OK, resultData);
                            finish();
                        }

                    } else {
                        Toast.makeText(mCtx, jsondata.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        });
    }

    public void deliveryTimeChangeAction(View button){
        if("".equals(messageString)){
            DUtil.alertShow(this, "변경사유를 선택해주세요.");
        }else{
            if("D".equals(mode)){
                if(selectedDeliveryTimestamp != -1){
                    modInfo();
                }else{
                    DUtil.alertShow(this, "변경일시를 선택해주세요.");
                }
            }else{
                if(selectedPickupTimestamp != -1 && selectedDeliveryTimestamp != -1){
                    modInfo();
                }else{
                    DUtil.alertShow(this, "수거,배송 변경일시를 선택해주세요.");
                }
            }

        }
    }


    public void modInfo() {
        modParams = new RequestParams();
        try {
            modParams.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            modParams.put("orderId", orderInfo.getString("orderId"));
            if("D".equals(mode)){
                modParams.put("deliveryRequestTime", selectedDeliveryTimestamp);
                modParams.put("deliveryRequestMessage", deliveryRequestMessage);
            }else{
                modParams.put("pickupRequestTime", selectedPickupTimestamp);
                modParams.put("deliveryRequestTime", selectedDeliveryTimestamp);
                modParams.put("pickupRequestMessage", deliveryRequestMessage);
            }


            Log.i("modInfo p time", TimeUtil.convertTimestampToString(selectedPickupTimestamp) + "");
            Log.i("modInfo d time", TimeUtil.convertTimestampToString(selectedDeliveryTimestamp) + "");

            modParams.put("k", 1);
            Log.i("modInfo", modParams.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(deliveryRequestMessage == null){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
            alertDialogBuilder.setCancelable(true).setMessage("변경사유를 입력해주세요.")
                    .setPositiveButton("네", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else{
            if("3".equals(deliveryRequestMessage)){
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                String title;
                if("D".equals(mode)){
                    title = "고객과 통화하셔서 배송일시를 다시 조정하셨나요?";
                }else{
                    title = "고객과 통화하셔서 수거/배송일시를 다시 조정하셨나요?";
                }
                alertDialogBuilder.setCancelable(true).setMessage(title)
                        .setPositiveButton("네", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                modInfoExcute(modParams);
                            }
                        }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }else{
                modInfoExcute(modParams);
            }
        }
    }

    /*
        배송일 변경을 위한 날짜시간 초기화 선언
     */
    private void initialize(final JSONArray data, final String _mode) throws JSONException {
        if("D".equals(_mode)){
            aq.id(R.id.sub_d_layout).background(R.drawable.bg_textview_picker_focus);
            aq.id(R.id.icon_step2_date).image(R.mipmap.icon_reorder_date_sel);

            aq.id(R.id.main_d_layout).background(R.color.transparent);
            aq.id(R.id.icon_step1_date).image(R.mipmap.icon_reorder_date_sel);
            aq.id(R.id.icon_step1_time).image(R.mipmap.icon_reorder_clock_sel);
        }else{
            aq.id(R.id.main_d_layout).background(R.drawable.bg_textview_picker_focus);
            aq.id(R.id.icon_step1_date).image(R.mipmap.icon_reorder_date_sel);
            aq.id(R.id.icon_step1_time).image(R.mipmap.icon_reorder_clock_normal);
        }

        ArrayList<String> dateTempArr = new ArrayList<String>();
        for(int i =0; i < data.length(); i++){
            Date dateData = TimeUtil.getDateTimeSimpleDateFormat6(data.getJSONObject(i).getString("date"));
            dateTempArr.add(TimeUtil.getDateTimeType5Format(dateData));
        }
        final String[] dateArr = dateTempArr.toArray(new String[dateTempArr.size()]);

        final TextView mainDTextView;
        if("D".equals(_mode)){
            mainDTextView = (TextView) findViewById(R.id.sub_d_textview);
            mainDTextView.setText(getString(R.string.select_d_date));     // TODO : 디폴트 값으로 셋팅
            mainDTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogUtil.showCustomDialog(ModDeliveryInfoActivityV2.this, dateArr, datePos2, "배송일자를 선택해주세요.", R.mipmap.icon_reorder_date_sel, new View.OnClickListener() {    // TODO : 값 셋팅 필요
                        @Override
                        public void onClick(final View v) {
                            Bundle args = (Bundle) v.getTag();
                            datePos2 =  args.getInt(DialogUtil.KEY_DATA);
                            Log.e("click date Value:", datePos2 + "");
                            try {
                                initTimePicker(data, datePos2, _mode);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //long timestamp = args.getLong(DialogUtil.KEY_DATA, TimeUtil.getUtcTodayMillis());
                            try {
                                mainDTextView.setText(TimeUtil.getDateTimeType5Format(TimeUtil.getDateTimeSimpleDateFormat6(data.getJSONObject(datePos2).getString("date"))));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });
        }else{
            mainDTextView = (TextView) findViewById(R.id.main_d_textview);
            mainDTextView.setText(getString(R.string.select_date));     // TODO : 디폴트 값으로 셋팅
            mainDTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogUtil.showCustomDialog(ModDeliveryInfoActivityV2.this, dateArr, datePos1, "수거일자를 선택해주세요.", R.mipmap.icon_reorder_date_sel, new View.OnClickListener() {    // TODO : 값 셋팅 필요
                        @Override
                        public void onClick(final View v) {
                            Bundle args = (Bundle) v.getTag();
                            datePos1 =  args.getInt(DialogUtil.KEY_DATA);
                            Log.e("click date Value:", datePos1 + "");
                            try {
                                initTimePicker(data, datePos1, _mode);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //long timestamp = args.getLong(DialogUtil.KEY_DATA, TimeUtil.getUtcTodayMillis());
                            try {
                                mainDTextView.setText(TimeUtil.getDateTimeType5Format(TimeUtil.getDateTimeSimpleDateFormat6(data.getJSONObject(datePos1).getString("date"))));
                                resetDeliveryFocus();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });
        }
    }

    private void resetDeliveryFocus(){
        selectedDeliveryTimestamp = -1;
        aq.id(R.id.sub_d_textview).text(getString(R.string.select_d_date));
        aq.id(R.id.sub_t_textview).text(getString(R.string.select_d_time));
        aq.id(R.id.sub_t_layout).background(R.drawable.bg_textview_picker_normal);
        aq.id(R.id.icon_step2_time).image(R.mipmap.icon_reorder_clock_normal);
        aq.id(R.id.sub_d_layout).background(R.drawable.bg_textview_picker_normal);
        aq.id(R.id.icon_step2_date).image(R.mipmap.icon_reorder_date_normal);
        datePos2 = -1;
        timePos2 = -1;
    }

    long selectedDeliveryTimestamp = -1;
    long selectedPickupTimestamp = -1;

    private void initTimePicker(final JSONArray data, final int position, String _mode) throws JSONException {


        if("D".equals(_mode)){
            aq.id(R.id.sub_t_layout).background(R.drawable.bg_textview_picker_focus);
            aq.id(R.id.sub_d_layout).background(R.color.transparent);
            aq.id(R.id.icon_step2_time).image(R.mipmap.icon_reorder_clock_sel);
            timePos2 = -1;
        }else{
            aq.id(R.id.main_t_layout).background(R.drawable.bg_textview_picker_focus);
            aq.id(R.id.main_d_layout).background(R.color.transparent);
            aq.id(R.id.icon_step1_time).image(R.mipmap.icon_reorder_clock_sel);
            timePos1 = -1;
        }


        ArrayList<String> dateTempArr = new ArrayList<String>();
        for(int i =0; i < data.getJSONObject(position).getJSONArray("timeList").length(); i++){
            String temp = data.getJSONObject(position).getJSONArray("timeList").getString(i);
            int startHour = Integer.parseInt(temp.substring(0,2));
            int hourGap = Integer.parseInt(temp.substring(3,5)) - Integer.parseInt(temp.substring(0,2));
            String addString = "";
            if(startHour <= 12){
                addString = "오전 " + startHour + "시 ~ " + (startHour+hourGap) + "시";
            }else{
                addString = "오후 " + startHour%12 + "시 ~ " + (startHour+hourGap)%12 + "시";
            }
            dateTempArr.add(addString);
        }
        final String[] dateArr = dateTempArr.toArray(new String[dateTempArr.size()]);

        final TextView mainTTextView;
        if("D".equals(_mode)){
            mainTTextView = (TextView) findViewById(R.id.sub_t_textview);
            mainTTextView.setText(getString(R.string.select_d_time));     // TODO : 디폴트 값으로 셋팅
            mainTTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogUtil.showCustomDialog(ModDeliveryInfoActivityV2.this, dateArr, timePos2, "배송시간을 선택해주세요.", R.mipmap.icon_reorder_clock_sel, new View.OnClickListener() {    // TODO : 값 셋팅 필요
                        @Override
                        public void onClick(final View v) {
                            Bundle args = (Bundle) v.getTag();
                            timePos2 =  args.getInt(DialogUtil.KEY_DATA);
                            Log.e("click time Value:", timePos2 + "");

                            Date dateData = null;
                            Date deliveryDate = null;
                            try {
                                dateData = TimeUtil.getDateTimeSimpleDateFormat6(data.getJSONObject(position).getString("date"));
                                Log.e("dateData", data.getJSONObject(position).getString("date"));
                                Log.e("dateData toLocaleString", dateData.toLocaleString());

                                Calendar t = Calendar.getInstance ( );
                                t.setTime ( dateData );
                                Log.e("dateData year", t.get(Calendar.YEAR) + "");
                                int startHour = Integer.parseInt(data.getJSONObject(position).getJSONArray("timeList").getString(timePos2).substring(0,2));
                                deliveryDate = TimeUtil.getDate(t.get(Calendar.YEAR), t.get(Calendar.MONTH), t.get(Calendar.DAY_OF_MONTH), startHour);

                                Calendar c = Calendar.getInstance ( );
                                c.setTime ( deliveryDate );

                                selectedDeliveryTimestamp = c.getTimeInMillis();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            mainTTextView.setText(dateArr[timePos2]);

                            aq.id(R.id.sub_t_layout).background(R.drawable.bg_textview_picker_normal);
                            aq.id(R.id.btn_req).image(R.drawable.bg_textview_picker_focus);

                            //Log.d("debug", "amamValue : " + ampmArray[ampmValue] + ", timeValue : " + timeArray[timeValue]);
                        }
                    });
                }
            });
        }else{
            mainTTextView = (TextView) findViewById(R.id.main_t_textview);
            mainTTextView.setText(getString(R.string.select_time));     // TODO : 디폴트 값으로 셋팅
            mainTTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogUtil.showCustomDialog(ModDeliveryInfoActivityV2.this, dateArr, timePos1, "수거시간을 선택해주세요.", R.mipmap.icon_reorder_clock_sel, new View.OnClickListener() {    // TODO : 값 셋팅 필요
                        @Override
                        public void onClick(final View v) {
                            Bundle args = (Bundle) v.getTag();
                            timePos1 =  args.getInt(DialogUtil.KEY_DATA);
                            Log.e("click time Value:", timePos1 + "");

                            Date dateData = null;
                            Date deliveryDate = null;
                            try {
                                dateData = TimeUtil.getDateTimeSimpleDateFormat6(data.getJSONObject(position).getString("date"));
                                Log.e("dateData", data.getJSONObject(position).getString("date"));
                                Log.e("dateData toLocaleString", dateData.toLocaleString());

                                Calendar t = Calendar.getInstance ( );
                                t.setTime ( dateData );
                                Log.e("dateData year", t.get(Calendar.YEAR) + "");
                                int startHour = Integer.parseInt(data.getJSONObject(position).getJSONArray("timeList").getString(timePos1).substring(0,2));
                                deliveryDate = TimeUtil.getDate(t.get(Calendar.YEAR), t.get(Calendar.MONTH), t.get(Calendar.DAY_OF_MONTH), startHour);

                                Calendar c = Calendar.getInstance ( );
                                c.setTime ( deliveryDate );

                                selectedPickupTimestamp = c.getTimeInMillis();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            mainTTextView.setText(dateArr[timePos1]);
                            getOrderAvailableTime(selectedPickupTimestamp, "D");
                            aq.id(R.id.main_t_layout).background(R.drawable.bg_textview_picker_normal);
                            //Log.d("debug", "amamValue : " + ampmArray[ampmValue] + ", timeValue : " + timeArray[timeValue]);
                        }
                    });
                }
            });
        }
    }


    private void getOrderAvailableTime(long _timestamp, final String _mode){
        //GET_ORDER_AVAILABLE_TIME
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("userId", userId);
        params.put("addressSeq", addressSeq);

        Date now = new Date();
        Log.e("now", deliveryReqTime + " / " + now.getTime());
        params.put("pickupRequestTime", _timestamp);
        //params.put("pickupRequestTime", pickupReqTime);

        params.put("k", 1);

        Log.e("getOrderAvailableTime", params.toString());

        NetworkClient.post(Constants.GET_ORDER_AVAILABLE_TIME, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GET_ORDER_AVAILABLE_TIME, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_ORDER_AVAILABLE_TIME, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if("D".equals(_mode)){
                            arr = jsondata.getJSONArray("data");
                            int dateCount = arr.length();
                            DUtil.Log("dateCount", dateCount + "");
                            initialize(arr, _mode);
                        }else{
                            pickArr = jsondata.getJSONArray("data");
                            int dateCount = pickArr.length();
                            DUtil.Log("dateCount", dateCount + "");
                            initialize(pickArr, _mode);
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }

}
