package me.rewhite.delivery.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.fragment.IFragment;
import me.rewhite.delivery.fragment.PickupDetailFragmentV2;
import me.rewhite.delivery.fragment.PickupItemFragmentV2;
import me.rewhite.delivery.fragment.PickupItemSubCategoryFragmentV2;
import me.rewhite.delivery.task.ImageUploadTask;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SystemUtil;

@SuppressWarnings("deprecation")
public class PickupItemActivity extends BaseActivity implements PickupDetailFragmentV2.RewhitePickupListener {

    AQuery aq;
    private final static String TAG = PickupItemActivity.class.getSimpleName();
    public static final String FRAGMENT_CATEGORY = "pickup_category";
    public static final String FRAGMENT_SUB_CATEGORY = "pickup_subcategory";
    public static final String FRAGMENT_DETAIL = "pickup_detail";

    public final Context mCtx = this;

    String mCurrentPhotoPath;
    Uri imageUri;
    Uri photoURI, albumURI;
    boolean isAlbum = false;

    private Fragment mContent;

    private boolean mIsTouchDisable; // 연속 터치로 생기는 이슈 막기 위함
    private boolean mFlagFinish;
    private Handler mHandler;
    private Runnable mTask;
    private String currentFragname;

    public String Step1Name = "";
    public int Step1Index = 0;
    public String Step2Name = "";
    public int Step2Index = 0;

    public String colorPickValue = "";
    public JSONArray pickedRepairArray;
    public JSONArray pickedAdditionalArray;
    public JSONArray pickedPartsArray;
    public int pickedPrice = 0;

    public JSONArray repairArray;
    public JSONArray additionalArray;
    public JSONArray partsArray;
    public JSONArray colorsArray;
    public JSONArray priceArray;

    public int tagNo;
    public int tagLength;
    public int bucketIndex = 0;
    public int pickupMode = -1;

    public JSONArray pickupItems;
    //public int pickupItemsPosition = 0;
    public boolean isFirstExist;

    public String pickedRelaundry = "N";
    public String pickedService = "N";
    public String laundry = "";

    private String appDetailSettingString = "카메라를 사용할 권한이 없습니다. 앱 권한설정 화면으로 이동합니다.\n이동한 화면에서 '권한'메뉴를 선택하시고 전체 허용해주세요~";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            fileName = savedInstanceState.getString("fileName");
        }
        setContentView(R.layout.activity_pickup_item);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                if (intent.getStringExtra("data") != null) {

                }
                repairArray = new JSONArray(intent.getStringExtra("repairData"));
                additionalArray = new JSONArray(intent.getStringExtra("additionalData"));
                partsArray = new JSONArray(intent.getStringExtra("partsData"));


                colorsArray = new JSONArray(intent.getStringExtra("colorsData"));
                priceArray = new JSONArray(intent.getStringExtra("priceData"));
                tagNo = intent.getIntExtra("initTagNo", 0);
                tagLength = intent.getIntExtra("initTagLength", 0);
                bucketIndex = intent.getIntExtra("bucketIndex", 0);
                pickupMode = intent.getIntExtra("pickupMode", 0);
                isFirstExist = intent.getBooleanExtra("isFirstExist", false);


                if(intent.getStringExtra("pickupItems") != null){
                    pickupItems = new JSONArray(intent.getStringExtra("pickupItems"));
                    bucketIndex = intent.getIntExtra("pickupItemsPosition", 0);

                    pickedRepairArray = pickupItems.getJSONObject(bucketIndex).getJSONArray("repair");
                    pickedPartsArray= pickupItems.getJSONObject(bucketIndex).getJSONArray("part");
                    pickedAdditionalArray = pickupItems.getJSONObject(bucketIndex).getJSONArray("additional");
                    colorPickValue = pickupItems.getJSONObject(bucketIndex).getString("itemColor");
                    imagePickValue = pickupItems.getJSONObject(bucketIndex).getJSONArray("photo");

                    pickedRelaundry = pickupItems.getJSONObject(bucketIndex).getString("isReLaundry");
                    pickedService = pickupItems.getJSONObject(bucketIndex).getString("isService");

                    laundry = pickupItems.getJSONObject(bucketIndex).getString("laundry");

                    Step1Index = pickupItems.getJSONObject(bucketIndex).getInt("step1index");
                    Step1Name = priceArray.getJSONObject(Step1Index).getString("itemTitle");
                    Step2Index = pickupItems.getJSONObject(bucketIndex).getInt("step2index");
                    Step2Name = priceArray.getJSONObject(Step1Index).getJSONArray("items").getJSONObject(Step2Index).getString("itemTitle");
                }else{
                    pickedRepairArray = new JSONArray();
                    pickedAdditionalArray = new JSONArray();
                    pickedPartsArray = new JSONArray();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }



        aq = new AQuery(this);
        mHandler = new Handler();
        mTask = new Runnable() {

            @Override
            public void run() {
                mFlagFinish = false;
            }
        };

        RequestPermission(this);

        if(pickupItems != null){
            endScreenJump();
        }else{
            initilize();
        }

    }

    public void submitElement(JSONObject element, int tagNumber, int position){
        try {
            JSONArray tempItems = new JSONArray();
            if(pickupItems != null){
                for(int i = 0; i < pickupItems.length(); i++){
                    if(bucketIndex == i){
                        tempItems.put(element);
                    }else{
                        tempItems.put(pickupItems.getJSONObject(i));
                    }
                }
            }else{
                tempItems.put(element);
            }

            Intent resultData = new Intent();
            resultData.putExtra("entire", tempItems.toString());
            resultData.putExtra("element", element.toString());
            resultData.putExtra("tagNumber", tagNumber);
            resultData.putExtra("position", position);
            setResult(Activity.RESULT_OK, resultData);
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void closeClicked(View button) {
        onBackPressed();
    }

    private void initilize() {
        showFragment(FRAGMENT_CATEGORY);
    }

    public void showFragment(String fragmentName) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);
        showFragment(fragmentName, null, null);
    }

    String transitionName = "";
    public void endScreenJump(){
        showFragment(FRAGMENT_DETAIL);
    }

    public void showFragment(String fragmentName, View sharedElement, Bundle args) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(fragmentName);

        if (FRAGMENT_CATEGORY.equals(fragmentName)) {
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new PickupItemFragmentV2();
        } else if (FRAGMENT_SUB_CATEGORY.equals(fragmentName)) {
            fragment = new PickupItemSubCategoryFragmentV2();

        } else if (FRAGMENT_DETAIL.equals(fragmentName)) {
            fragment = new PickupDetailFragmentV2();
        }

        Log.e(TAG, "[PickupItemFragmentV2] Create : " + fragmentName);

        if (null != args) {
            fragment.setArguments(args);
        }

        //ft.setCustomAnimations(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        ft.replace(R.id.content_frame, fragment, fragmentName);
        ft.addToBackStack(fragmentName);
        //ft.commitAllowingStateLoss();
        ft.commit();

    }

    public static final String[] INITIAL_PERMS={
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    public static final int INITIAL_REQUEST=1337;

    private static final int REQ_CODE_PICK_IMAGE = 0;
    public String TEMP_PHOTO_FILE = "upload.jpg"; // 임시 저장파일
    private final int CAMERA_CAPTURE = 1;
    private final int REQ_CODE_SELECT_IMAGE = 100;
    private final int CROP_PIC = 2;
    private Uri picUri;
    public JSONArray imagePickValue;

    public void setPhotoFileName(String filename){
        TEMP_PHOTO_FILE = filename;
    }

    public String getPhotoFileName(){
        return TEMP_PHOTO_FILE;
    }

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {
            //Toast.makeText(PickupItemActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
            //addIntentCall();
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            //Toast.makeText(PickupItemActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();

            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(mCtx);
            alertDialogBuilder.setCancelable(false).setTitle("앱 권한 설정하기").setMessage(appDetailSettingString)
                    .setPositiveButton("설정화면으로 가기", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SystemUtil.showApplicationDetailSetting(mCtx);
                        }
                    }).setNegativeButton("취소", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }

    };

    protected void RequestPermission(Activity activity)
    {
//        if(ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
//        {
//            if(ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)) {
//                return;
//            }else{
//                ActivityCompat.requestPermissions(activity, INITIAL_PERMS, INITIAL_REQUEST);
//            }
//        }

        TedPermission.with(this)
                .setPermissionListener(permissionlistener)
                .setDeniedMessage(appDetailSettingString)
                .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }
//
//    // API 23 above Permission Protect statement
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//
//        //Log.e("onRequestPermissionResult", grantResults[0] + "");
//        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
////            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
////                    .setData(Uri.parse("package:" + getPackageName()));
////            startActivity(intent);
//
//            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
//            alertDialogBuilder.setCancelable(false).setTitle("앱 권한 설정하기").setMessage(appDetailSettingString)
//                    .setPositiveButton("네", new DialogInterface.OnClickListener() {
//
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            SystemUtil.showApplicationDetailSetting(mCtx);
//                        }
//                    }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {
//
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//
//                }
//            });
//
//            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
//            alertDialog.show();
//        }
//
//        switch(requestCode) {
//
//            case INITIAL_REQUEST:
//                if (canAccessCamera()) {
//                    addIntentCall();
//                }
//                else {
//                    Toast.makeText(this, "사진첩에 접근할수 있는 권한이 없습니다.", Toast.LENGTH_LONG).show();
//                }
//
//                break;
//        }
//    }

    public boolean canAccessCamera() {
        return (hasPermission(Manifest.permission.CAMERA));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    public static boolean isIntentAvailable( Context context, String action){
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent( action);
        List<ResolveInfo> list = packageManager.queryIntentActivities( intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    public void addIntentCall(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("사진 등록하기");

        // add a list
        String[] animals = {"직접 촬영", "사진첩에서 선택", "취소"};
        builder.setItems(animals, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // 직접 촬영
                        captureCamera();
                        break;
                    case 1: // 사진첩에서 선택
                        getAlbum();
                        break;
                    case 2: // 취소

                        break;
                }
            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

//        Display display = getWindowManager().getDefaultDisplay();
//        int mOutputX = display.getWidth();

    }

    public void getAlbum(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
        //intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQ_CODE_SELECT_IMAGE);
    }

    public void captureCamera(){
        if (canAccessCamera()) {
//            setPhotoFileName("upload_" + String.valueOf
//                    (System.currentTimeMillis()) + ".jpg");

            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        Log.e("captureCamera Error", ex.toString());
                    }
                    if (photoFile != null) {
                        Uri providerURI = FileProvider.getUriForFile(this, getPackageName(), photoFile);
                        photoURI = providerURI;

                        Log.i("photoFile", photoFile.toString());
                        Log.i("imageUri", photoURI.toString());

                        if(Build.VERSION.SDK_INT>=24){
                            try{
                                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                                m.invoke(null);
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                        //intent.putExtra( MediaStore.EXTRA_OUTPUT, Uri.fromFile(getTempFile()) );
                        //intent.putExtra(MediaStore.EXTRA_OUTPUT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString());
                        // requestCode지정해서 인텐트 실행
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(intent, CAMERA_CAPTURE);
                    }
                }

            }else{
                Toast.makeText(mCtx, "저장공간이 접근 불가능한 기기입니다.", Toast.LENGTH_SHORT).show();
                return;
            }


            //File file = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE);

        }else {
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(mCtx);
            alertDialogBuilder.setCancelable(false).setTitle("앱 권한 설정하기").setMessage(appDetailSettingString)
                    .setPositiveButton("네", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SystemUtil.showApplicationDetailSetting(mCtx);
                        }
                    }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            //Toast.makeText(this, "카메라를 사용할 권한이 없습니다.", Toast.LENGTH_LONG).show();

        }
    }



    public File createImageFile() throws IOException{
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.KOREA).format(new Date());
        String imageFileName = timestamp + ".jpg";
        //File imageFile = null;
        File storageDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/rewhite/"+imageFileName);

//        if(!storageDir.exists()){
//            Log.i("mCurrentPhotoPath1", storageDir.toString());
//            storageDir.mkdirs();
//        }

        //imageFile = new File(storageDir, imageFileName);
        mCurrentPhotoPath = storageDir.getAbsolutePath();
        Log.i("mCurrentPhotoPath1", mCurrentPhotoPath);

        //setPhotoFileName(imageFileName);

        return storageDir;
    }

    private void galleryAddPic(){
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    /**
     * this function does the crop operation.
     */
    private void performCrop() {
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
//            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
//            File f = new File(path, TEMP_PHOTO_FILE);

            cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            cropIntent.setDataAndType(photoURI, "image/*");

            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);

            if(!isAlbum){
                cropIntent.putExtra("output", photoURI);
            }else{
                cropIntent.putExtra("output", albumURI);
            }

            cropIntent.putExtra("scale", true);

            List list = getPackageManager().queryIntentActivities(cropIntent, 0);
            grantUriPermission("me.rewhite.delivery", photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION);


            try {
                cropIntent.putExtra("return-data", true);

                Intent i = new Intent(cropIntent);
                ResolveInfo res = (ResolveInfo) list.get(0);
                i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                i.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                grantUriPermission(res.activityInfo.packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION|Intent.FLAG_GRANT_READ_URI_PERMISSION);
                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));

                //startActivityForResult(i, CROP_PIC);

                startActivityForResult(
                        Intent.createChooser(i,"Complete action using"),
                        CROP_PIC);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }

        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            Toast toast = Toast
                    .makeText(this, "This device doesn't support the crop action!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private static final int PICK_COLOR = 11;
    private static final int PICK_REPAIR = 12;
    private static final int PICK_PARTS = 13;
    private static final int PICK_ADDITIONAL = 14;
    private static final int PICK_PRICE = 15;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICK_COLOR:
                    int colorIndex = data.getIntExtra("index", 0);
                    String colorValue = data.getStringExtra("value");
                    String colorName = data.getStringExtra("name");
                    Log.e("color", colorIndex + " / " + colorValue + " / " + colorName);
                    //colorPickValue = colorValue;

                    FragmentManager fm = getSupportFragmentManager();
                    String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
                    if(PickupItemActivity.FRAGMENT_DETAIL.equals(tag)){
                        PickupDetailFragmentV2 fragment = (PickupDetailFragmentV2)getSupportFragmentManager().findFragmentById(R.id.content_frame);
                        if(fragment != null){
                            fragment.setColorPickValue(colorValue);
                        }

                    }

                    break;
                case PICK_REPAIR:
                    try {
                        JSONArray ret = new JSONArray(data.getStringExtra("value"));
                        Log.e("REPAIR_PARTS", ret.toString());

                        FragmentManager fm2 = getSupportFragmentManager();
                        String tag2 = fm2.getBackStackEntryAt(fm2.getBackStackEntryCount() - 1).getName();
                        if(PickupItemActivity.FRAGMENT_DETAIL.equals(tag2)){
                            PickupDetailFragmentV2 fragment = (PickupDetailFragmentV2)getSupportFragmentManager().findFragmentById(R.id.content_frame);
                            if(fragment != null){
                                fragment.setRepairPickValue(ret);
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case PICK_PRICE:
                    int retp = data.getIntExtra("value",0);
                    Log.e("PICK_PRICE", retp + "");

                    FragmentManager fm2 = getSupportFragmentManager();
                    String tag2 = fm2.getBackStackEntryAt(fm2.getBackStackEntryCount() - 1).getName();
                    if(PickupItemActivity.FRAGMENT_DETAIL.equals(tag2)){
                        PickupDetailFragmentV2 fragment = (PickupDetailFragmentV2)getSupportFragmentManager().findFragmentById(R.id.content_frame);
                        if(fragment != null){
                            fragment.setPricePickValue(retp);
                        }

                    }
                    break;
                case PICK_PARTS:
                    try {
                        JSONArray ret = new JSONArray(data.getStringExtra("value"));
                        Log.e("PICK_PARTS", ret.toString());

                        FragmentManager fm3 = getSupportFragmentManager();
                        String tag3 = fm3.getBackStackEntryAt(fm3.getBackStackEntryCount() - 1).getName();
                        if(PickupItemActivity.FRAGMENT_DETAIL.equals(tag3)){
                            PickupDetailFragmentV2 fragment = (PickupDetailFragmentV2)getSupportFragmentManager().findFragmentById(R.id.content_frame);
                            if(fragment != null){
                                fragment.setPartsPickValue(ret);
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    break;
                case PICK_ADDITIONAL:
                    try {
                        JSONArray ret = new JSONArray(data.getStringExtra("value"));
                        Log.e("ADDITIONAL_PARTS", ret.toString());

                        FragmentManager fm4 = getSupportFragmentManager();
                        String tag4 = fm4.getBackStackEntryAt(fm4.getBackStackEntryCount() - 1).getName();
                        if(PickupItemActivity.FRAGMENT_DETAIL.equals(tag4)){
                            PickupDetailFragmentV2 fragment = (PickupDetailFragmentV2)getSupportFragmentManager().findFragmentById(R.id.content_frame);
                            if(fragment != null){
                                fragment.setAdditionalPickValue(ret);
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case CAMERA_CAPTURE:
//                    try {
//                        /*the user's device may not support cropping*/
//                        Log.e("CAMERA_CAPTURE", "CAMERA_CAPTURE");
//                        performCrop();
//                        //cropCapturedImage(Uri.fromFile(file));
//                    }
//                    catch(ActivityNotFoundException aNFE){
//                        //display an error message if user device doesn't support
//                        String errorMessage = "Sorry - your device doesn't support the crop action!";
//                        Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
//                        toast.show();
//                    }
                    File fc = new File(mCurrentPhotoPath);
                    mCurrentPhotoPath = reduceBitmapToFile(fc).getAbsolutePath();

                    isAlbum = false;
                    galleryAddPic();
                    uploadFile(mCurrentPhotoPath);

                    break;
                case REQ_CODE_SELECT_IMAGE:
                    isAlbum = true;
                    File albumFile = null;
                    try{
                        albumFile = createImageFile();
                    }catch (IOException e){
                        e.printStackTrace();
                    }

                    if(albumFile != null){
                        albumURI = Uri.fromFile(albumFile);
                    }

//                    try {
//                        final Uri imageUri = data.getData();
//                        final InputStream imageStream = getContentResolver().openInputStream(imageUri);
//                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
//                        //image_view.setImageBitmap(selectedImage);
//                    } catch (FileNotFoundException e) {
//                        e.printStackTrace();
//                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
//                    }

                    photoURI = data.getData();
                    performCrop();

                    break;
                case CROP_PIC:

                    mCurrentPhotoPath = albumURI.getPath();
                    galleryAddPic();
                    uploadFile(mCurrentPhotoPath);

                    break;
            }
        }
    }

    public File reduceBitmapToFile(File file){
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE=75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 80 , outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    ProgressDialog pDialog;

    public void uploadFile(String _filePath){

        pDialog = new ProgressDialog(this);
        //pDialog.setTitle("이미지 업로드");
        pDialog.setMessage("파일 전송중..");
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setIndeterminate(false);
        pDialog.show();

        try{
            HashMap<String, Object> reqParams = new HashMap<String, Object>();

            ImageUploadTask uploadFile = new ImageUploadTask(this, this, preferences);
            uploadFile.setPath(_filePath, pDialog);
            uploadFile.execute(reqParams);
        }catch(Exception e){
            e.printStackTrace();
        }


//        String filePath = null;
//        File f = new File(_filePath);
//        filePath = f.getAbsolutePath();
//        Log.i("imageDATA PATH :::::", "Chosen path = " + filePath);
//        Log.i("File :::::", "getExternalStoragePublicDirectory = " + TEMP_PHOTO_FILE);
//        Log.i("imageDATA PATH :::::", "Chosen path = " + filePath);
//
//        BitmapFactory.Options bfo = new BitmapFactory.Options();
//
//        int size = 6;
//        Bitmap bmpAvator;
//
//        //OutOfMemoryError 날경우 이미지 사이즈 축소
//        while (true) {
//            try {
//                bfo.inSampleSize = size;
//                bmpAvator = BitmapFactory.decodeFile(filePath, bfo);
//
//                //int degrees = GetExifOrientation(filePath);
//                // 회전한 이미지 취득
//                //bmpAvator = GetRotatedBitmap(bmpAvator, degrees);
//                File fileCacheItem = new File(filePath);
//                OutputStream out = null;
//                try {
//                    fileCacheItem.createNewFile();
//                    out = new FileOutputStream(fileCacheItem);
//                    bmpAvator.compress(Bitmap.CompressFormat.JPEG, 80, out);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    try {
//                        out.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                HashMap<String, Object> reqParams = new HashMap<String, Object>();
//                //reqParams.put("filename", TEMP_PHOTO_FILE);
//                //new ImageUploadTask(this, preferences).execute(reqParams);
//
//                ImageUploadTask uploadFile = new ImageUploadTask(this, preferences);
//                uploadFile.setPath(_filePath);
//                uploadFile.execute(reqParams);
//
//                break;
//            } catch (OutOfMemoryError e) {
//                e.printStackTrace();
//                Log.i("inSampleSize :::::", "resize = " + size);
//                size++;
//            }
//        }
    }

    public String getImageNameToUri(Uri data)
    {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(data, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        String imgPath = cursor.getString(column_index);
        String imgName = imgPath.substring(imgPath.lastIndexOf("/")+1);

        return imgName;
    }

    SharedPreferences preferences;

    public void selectPricePicker(){
        Intent pickIntent = new Intent(this, PickupItemPriceMod.class);
        pickIntent.putExtra("pickedPrice", pickedPrice);
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, PICK_PRICE);
    }

    public void selectColorPicker(){

        Intent pickIntent = new Intent(this, PickupItemColorPicker.class);
        pickIntent.putExtra("pickedColor", colorPickValue);
        pickIntent.putExtra("data", colorsArray.toString());
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, PICK_COLOR);

    }

    public void selectRepairPicker(){

        Intent pickIntent = new Intent(this, PickupItemRepairPicker.class);
        pickIntent.putExtra("pickedRepair", pickedRepairArray.toString());
        pickIntent.putExtra("data", repairArray.toString());
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, PICK_REPAIR);

    }

    public void selectAdditionalPicker(){

        Intent pickIntent = new Intent(this, PickupItemAdditionalPicker.class);
        pickIntent.putExtra("pickedAdditional", pickedAdditionalArray.toString());
        pickIntent.putExtra("data", additionalArray.toString());
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, PICK_ADDITIONAL);

    }

    public void selectPartsPicker(){

        Intent pickIntent = new Intent(this, PickupItemPartsPicker.class);
        pickIntent.putExtra("pickedParts", pickedPartsArray.toString());
        pickIntent.putExtra("data", partsArray.toString());
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, PICK_PARTS);

    }

    public boolean getTouchDisable() {
        Log.d(TAG, "[getTouchDisable]");
        return mIsTouchDisable;
    }

    public void setTouchDisable(boolean isTouchDisable) {
        Log.d(TAG, "[setTouchDisable]" + isTouchDisable);
        mIsTouchDisable = isTouchDisable;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        DUtil.Log(TAG, "[onBackPressed] Back Stack Entry Count : " + fm.getBackStackEntryCount());

        if (fm.getBackStackEntryCount() >= 1) {

            String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
            IFragment fragment = (IFragment) fm.findFragmentByTag(tag);
            if (fm.getBackStackEntryCount() == 1) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                alertDialogBuilder.setCancelable(true).setMessage("세탁물 등록이 완료되지 않았습니다.\n" +
                        "중단하시겠어요?")
                        .setPositiveButton("네", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            } else {
                setTouchDisable(true);
                //clearCaptureView();

                fm.popBackStack();

            }
        } else {
            super.onBackPressed();
        }
        //super.onBackPressed();
    }

    @Override
    public void onResult(String command, String result) {

    }

    private int GetExifOrientation(String filepath) {
        int degree = 0;
        ExifInterface exif = null;

        try {
            exif = new ExifInterface(filepath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (exif != null) {
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            if (orientation != -1) {
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        degree = 90;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        degree = 180;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        degree = 270;
                        break;
                }
            }
        }

        return degree;
    }

    private Bitmap GetRotatedBitmap(Bitmap bitmap, int degrees) {
        if (degrees != 0 && bitmap != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

            try {
                Bitmap b2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);

                if (bitmap != b2) {
                    bitmap.recycle();
                    bitmap = b2;
                }
            } catch (OutOfMemoryError e) {
                // 메모리 부족에러시, 원본을 반환
                e.printStackTrace();
            }
        }

        return bitmap;
    }


    public void uploadCompleted(String response) {

        try {
            JSONObject json = new JSONObject(response);
            Log.i("uploadCompleted", response.toString());

            if ("S0000".equals(json.getString("resultCode"))) {
                DUtil.alertShow(this, "이미지 업로드가 완료되었습니다");
                String imgurl = json.getString("data");

                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                File f = new File(path, TEMP_PHOTO_FILE);
                if(f.exists()){
                    f.delete();
                }
                //sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory()))); // 갤러리를 갱신하기 위해..

                FragmentManager fm2 = getSupportFragmentManager();
                String tag2 = fm2.getBackStackEntryAt(fm2.getBackStackEntryCount() - 1).getName();
                if(PickupItemActivity.FRAGMENT_DETAIL.equals(tag2)){
                    PickupDetailFragmentV2 fragment = (PickupDetailFragmentV2)getSupportFragmentManager().findFragmentById(R.id.content_frame);
                    if(fragment != null){
                        fragment.setImagePicsValue(imgurl);
                    }

                }
                //
            } else {
                DUtil.alertShow(this, "이미지 업로드가 실패했습니다.");
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if(pDialog.isShowing()){
            pDialog.dismiss();
        }

    }

    void createExternalStoragePublicPicture() {

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        //File file = new File(getCacheDir(), TEMP_PHOTO_FILE);

        try {
            // Make sure the Pictures directory exists.
            if(!path.exists()){
                path.mkdirs();
            }


            InputStream is = getAssets().open("kakao_default_profile_image.png");// getResources().openRawResource(R.mipmap.kakao_default_profile_image);
            OutputStream os = new FileOutputStream(file);
            byte[] data = new byte[is.available()];
            is.read(data);
            os.write(data);
            is.close();
            os.close();

            MediaScannerConnection.scanFile(this, new String[]{file.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });
        } catch (IOException e) {
            // Unable to create file, likely because external storage is
            // not currently mounted.
            e.printStackTrace();
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }

    void deleteExternalStoragePublicPicture() {
        //File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        //File file = new File(path, TEMP_PHOTO_FILE);
        //Environment.getDownloadCacheDirectory();
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        if(file.exists()){
            file.delete();
        }
    }

    boolean hasExternalStoragePublicPicture() {
        //File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        //File file = new File(path, TEMP_PHOTO_FILE);
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        return file.exists();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        bundle.putString("fileName", fileName);
    }

    String fileName;
    /**
     * 임시 저장 파일의 경로를 반환
     */


}
