
package me.rewhite.delivery.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.DialogUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.TimeUtil;

public class OrderCreateActivity extends BaseActivity {

    final Context context = this;
    String orderId;
    long deliveryRequestTime;
    int userId;
    int addressSeq;
    JSONArray arr;
    private AQuery aq;
    int selectedDatePos = -1;
    int datePos = -1;
    int timePos = -1;
    int totalCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_create);

        aq = new AQuery(this);
        //long todayTimestamp = TimeUtil.getUtcTodayMillis();
        aq.id(R.id.main_d_textview).text(TimeUtil.getDateTimeType5Format(new Date()));
        aq.id(R.id.main_t_textview).text(TimeUtil.convertTimestampToStringTime(TimeUtil.getUtcTodayMillis()));

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("orderId") != null) {
                orderId = intent.getStringExtra("orderId");
                userId = intent.getIntExtra("userId", 0);
                addressSeq = intent.getIntExtra("addressSeq", 0);

                getOrderAvailableTime();
            }
        }

        aq.id(R.id.btn_add).clicked(this, "addAction");
        aq.id(R.id.btn_remove).clicked(this, "removeAction");
        aq.id(R.id.btn_submit).clicked(this, "submitAction");
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        aq.id(R.id.text_count).text(totalCount + " 개");
    }

    private void submitButtonArrange(){
        if(totalCount > 0 && selectedTimestamp != -1){
            submitButtonEnable();
        }else{
            submitButtonDisable();
        }
    }

    private void submitButtonEnable(){
        aq.id(R.id.btn_submit).backgroundColor(Color.parseColor("#333333")).textColor(Color.parseColor("#ffffff")).enabled(true);
    }
    private void submitButtonDisable(){
        aq.id(R.id.btn_submit).backgroundColor(Color.parseColor("#696969")).textColor(Color.parseColor("#bebebe")).enabled(false);
    }

    public void closeClicked(View button) {
        finish();
    }

    public void submitAction(View button){
        if(totalCount <= 0){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setCancelable(true).setMessage("수거수량을 입력해주세요.")
                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else{
            submitProcess();
        }
    }

    private void submitProcess(){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("deliveryRequestTime", selectedTimestamp);
        params.put("pickupQuantity", totalCount);
        params.put("k", 1);

        Log.e("dTime", TimeUtil.convertTimestampToString(selectedTimestamp));

        NetworkClient.post(Constants.ORDER_CREATE, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_CREATE, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_CREATE, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        Intent resultData = new Intent();
                        resultData.putExtra("result", totalCount);
                        setResult(Activity.RESULT_OK, resultData);
                        finish();
                    } else {
                        Toast.makeText(context, jsondata.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void addAction(View button){
        totalCount++;
        aq.id(R.id.text_count).text(totalCount + " 개");

        submitButtonArrange();
    }

    public void removeAction(View button){
        totalCount--;
        if(totalCount <= 0){
            totalCount = 0;
        }
        aq.id(R.id.text_count).text(totalCount + " 개");

        submitButtonArrange();
    }



    private void initialize(final JSONArray data) throws JSONException {
        aq.id(R.id.sub_d_layout).background(R.drawable.bg_textview_picker_focus);
        aq.id(R.id.icon_step2_date).image(R.mipmap.icon_reorder_date_sel);

        aq.id(R.id.main_d_layout).background(R.color.transparent);
        aq.id(R.id.icon_step1_date).image(R.mipmap.icon_reorder_date_sel);
        aq.id(R.id.icon_step1_time).image(R.mipmap.icon_reorder_clock_sel);

        ArrayList<String> dateTempArr = new ArrayList<String>();
        for(int i =0; i < data.length(); i++){
            Date dateData = TimeUtil.getDateTimeSimpleDateFormat6(data.getJSONObject(i).getString("date"));
            dateTempArr.add(TimeUtil.getDateTimeType5Format(dateData));
        }
        final String[] dateArr = dateTempArr.toArray(new String[dateTempArr.size()]);

        final TextView mainDTextView = (TextView) findViewById(R.id.sub_d_textview);
        mainDTextView.setText(getString(R.string.select_d_date));     // TODO : 디폴트 값으로 셋팅
        mainDTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUtil.showCustomDialog(OrderCreateActivity.this, dateArr, datePos, "배송일자를 선택해주세요.", R.mipmap.icon_reorder_date_sel, new View.OnClickListener() {    // TODO : 값 셋팅 필요
                    @Override
                    public void onClick(final View v) {
                        Bundle args = (Bundle) v.getTag();
                        datePos =  args.getInt(DialogUtil.KEY_DATA);
                        Log.e("click date Value:", datePos + "");
                        try {
                            initTimePicker(data, datePos);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //long timestamp = args.getLong(DialogUtil.KEY_DATA, TimeUtil.getUtcTodayMillis());
                        try {
                            mainDTextView.setText(TimeUtil.getDateTimeType5Format(TimeUtil.getDateTimeSimpleDateFormat6(data.getJSONObject(datePos).getString("date"))));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

    }

    long selectedTimestamp = -1;

    private void initTimePicker(final JSONArray data, final int position) throws JSONException {

        aq.id(R.id.sub_t_layout).background(R.drawable.bg_textview_picker_focus);
        aq.id(R.id.sub_d_layout).background(R.color.transparent);
        aq.id(R.id.icon_step2_time).image(R.mipmap.icon_reorder_clock_sel);

        timePos = -1;

        ArrayList<String> dateTempArr = new ArrayList<String>();
        for(int i =0; i < data.getJSONObject(position).getJSONArray("timeList").length(); i++){
            String temp = data.getJSONObject(position).getJSONArray("timeList").getString(i);
            int startHour = Integer.parseInt(temp.substring(0,2));
            int hourGap = Integer.parseInt(temp.substring(3,5)) - Integer.parseInt(temp.substring(0,2));
            String addString = "";
            if(startHour <= 12){
                addString = "오전 " + startHour + "시 ~ " + (startHour+hourGap) + "시";
            }else{
                addString = "오후 " + startHour%12 + "시 ~ " + (startHour+hourGap)%12 + "시";
            }
            dateTempArr.add(addString);
        }
        final String[] dateArr = dateTempArr.toArray(new String[dateTempArr.size()]);

        final TextView mainTTextView = (TextView) findViewById(R.id.sub_t_textview);
        mainTTextView.setText(getString(R.string.select_d_time));     // TODO : 디폴트 값으로 셋팅
        mainTTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUtil.showCustomDialog(OrderCreateActivity.this, dateArr, timePos, "배송시간을 선택해주세요.", R.mipmap.icon_reorder_clock_sel, new View.OnClickListener() {    // TODO : 값 셋팅 필요
                    @Override
                    public void onClick(final View v) {
                        Bundle args = (Bundle) v.getTag();
                        timePos =  args.getInt(DialogUtil.KEY_DATA);
                        Log.e("click time Value:", timePos + "");

                        Date dateData = null;
                        Date deliveryDate = null;
                        try {
                            dateData = TimeUtil.getDateTimeSimpleDateFormat6(data.getJSONObject(position).getString("date"));
                            Log.e("dateData", data.getJSONObject(position).getString("date"));
                            Log.e("dateData toLocaleString", dateData.toLocaleString());

                            Calendar t = Calendar.getInstance ( );
                            t.setTime ( dateData );
                            Log.e("dateData year", t.get(Calendar.YEAR) + "");
                            int startHour = Integer.parseInt(data.getJSONObject(position).getJSONArray("timeList").getString(timePos).substring(0,2));
                            deliveryDate = TimeUtil.getDate(t.get(Calendar.YEAR), t.get(Calendar.MONTH), t.get(Calendar.DAY_OF_MONTH), startHour);

                            Calendar c = Calendar.getInstance ( );
                            c.setTime ( deliveryDate );

                            selectedTimestamp = c.getTimeInMillis();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        mainTTextView.setText(dateArr[timePos]);
                        aq.id(R.id.sub_t_layout).background(R.drawable.bg_textview_picker_normal);
                        aq.id(R.id.box_count_input).background(R.drawable.bg_textview_picker_focus);

                        submitButtonArrange();
                        //Log.d("debug", "amamValue : " + ampmArray[ampmValue] + ", timeValue : " + timeArray[timeValue]);
                    }
                });
            }
        });
    }

    private void getOrderAvailableTime(){
        //GET_ORDER_AVAILABLE_TIME
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("userId", userId);
        params.put("addressSeq", addressSeq);

        Calendar cal = Calendar.getInstance();
        params.put("pickupRequestTime", cal.getTimeInMillis());
        params.put("k", 1);

        Log.e("getOrderAvailableTime", params.toString());

        NetworkClient.post(Constants.GET_ORDER_AVAILABLE_TIME, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GET_ORDER_AVAILABLE_TIME, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_ORDER_AVAILABLE_TIME, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        arr = jsondata.getJSONArray("data");
                        int dateCount = arr.length();
                        DUtil.Log("dateCount", dateCount + "");
                        initialize(arr);
                    } else {

                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }

    private void reorderProcess(){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("deliveryRequestTime", deliveryRequestTime);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_CREATE, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_CREATE, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_CREATE, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {

                    } else {

                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }
}
