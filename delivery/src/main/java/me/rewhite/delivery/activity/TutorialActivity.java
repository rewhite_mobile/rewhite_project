package me.rewhite.delivery.activity;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.delivery.Bootstrap;
import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.SharedPreferencesUtility;


//import com.actionbarsherlock.app.SherlockActivity;


public class TutorialActivity extends BaseActivity {

    private static final String TAG = "TutorialActivity";
    private Context mContext;
    private WalkthroughAdapter mPagerAdapter;

    private final int mPageCount = 4;
    private AQuery aq;
    Button button = null;

    public void closeClicked(View button) {
        finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(CommonUtility.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            setContentView(R.layout.activity_tutorial);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_tutorial);
        }

        SharedPreferencesUtility.set(SharedPreferencesUtility.Command.IS_TUTORIAL, "true");

        aq = new AQuery(this);
        aq.id(R.id.title_text).text("도움말");
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked").gone();

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        mContext = this;
        mPagerAdapter = new WalkthroughAdapter();

        ViewPager pager = (ViewPager) findViewById(R.id.activity_walkthrough_layout);
        pager.setAdapter(mPagerAdapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        aq.id(R.id.indicator_image).image(R.mipmap.indicator_01);
                        aq.id(R.id.desc_image).image(R.mipmap.text_tuto_01);
                        aq.id(R.id.btn_drawer).gone();
                        break;

                    case 1:
                        aq.id(R.id.indicator_image).image(R.mipmap.indicator_02);
                        aq.id(R.id.desc_image).image(R.mipmap.text_tuto_02);
                        aq.id(R.id.btn_drawer).gone();
                        break;

                    case 2:
                        aq.id(R.id.indicator_image).image(R.mipmap.indicator_03);
                        aq.id(R.id.desc_image).image(R.mipmap.text_tuto_03);
                        aq.id(R.id.btn_drawer).gone();
                        break;

                    case 3:
                        aq.id(R.id.indicator_image).image(R.mipmap.indicator_04);
                        aq.id(R.id.desc_image).image(R.mipmap.text_tuto_04);
                        aq.id(R.id.btn_drawer).visible();
                        break;

                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
/*
        PageIndicatorLayout indicator = (PageIndicatorLayout) findViewById(R.id.layout_menu_indicator);
        indicator.setPager(pager);
        indicator.initilize(mPageCount);*/
    }

    private class WalkthroughAdapter extends PagerAdapter {
        @Override
        public int getCount() {
            return mPageCount;
        }

        @Override
        public void destroyItem(View pager, int position, Object view) {
            ((ViewPager) pager).removeView((View) view);
        }

        @Override
        public Object instantiateItem(View pager, int position) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            RelativeLayout view = null;
            ImageView imageView = null;
            Button button = null;

            switch (position) {
                case 0:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough_top_arrange, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.tuto_bg01);
                    ((ViewPager) pager).addView(view, 0);
                    break;

                case 1:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough_top_arrange, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.tuto_bg02);
                    ((ViewPager) pager).addView(view, 0);
                    break;

                case 2:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough_top_arrange, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.tuto_bg03);
                    ((ViewPager) pager).addView(view, 0);
                    break;

                case 3:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough_top_arrange, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.tuto_bg04);
                    ((ViewPager) pager).addView(view, 0);


                    break;

                default:
                    break;
            }

            return view;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }
    }

    private View.OnClickListener mOnClickStartBtn = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SharedPreferencesUtility.set(SharedPreferencesUtility.Command.IS_TUTORIAL, "true");
            //AccessToken.clearAccessTokenFromCache();
//
//            Intent intent = new Intent(mContext, SignActivity.class);
//            //intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);
//
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            //intent.setClass(mContext, SignActivity.class);
//
//            startActivity(intent);
            finish();
        }
    };

}
