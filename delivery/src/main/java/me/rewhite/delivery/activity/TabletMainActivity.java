package me.rewhite.delivery.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.splunk.mint.Mint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.Bootstrap;
import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.OrderListItem;
import me.rewhite.delivery.adapter.TabletMainListViewAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.session.RewhiteException;
import me.rewhite.delivery.session.Session;
import me.rewhite.delivery.session.SessionCallback;
import me.rewhite.delivery.tablet.TableOrderDetailActivity;
import me.rewhite.delivery.tablet.TabletFinanceActivity;
import me.rewhite.delivery.tablet.TabletHtmlActivity;
import me.rewhite.delivery.tablet.TabletLocalOrderListActivity;
import me.rewhite.delivery.tablet.TabletPriceSettingActivity;
import me.rewhite.delivery.tablet.TabletRewhiteActivity;
import me.rewhite.delivery.tablet.TabletSettingActivity;
import me.rewhite.delivery.tablet.TabletStoreInfoActivity;
import me.rewhite.delivery.tablet.TabletUserManageActivity;
import me.rewhite.delivery.tablet.UserSearchActivity;
import me.rewhite.delivery.tablet.VanUsbTestActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class TabletMainActivity extends TabletBaseActivity {

    private static final String TAG = "TabletMainActivity";

    public final Context mCtx = this;
    public SharedPreferences preferences;

    private Handler mHandler;
    private Runnable mTask;
    private Fragment mContent;
    private AQuery aq;

    private TabletMainListViewAdapter orderAdapter;
    private ArrayList<OrderListItem> orderData;

    private boolean mIsTouchDisable; // 연속 터치로 생기는 이슈 막기 위함
    private boolean mFlagFinish;

    private final SessionCallback rewhiteSessionCallback = new RewhiteSessionStatusCallback();
    private Session session;

    private long splashDelay = 100;
    ProgressDialog mProgressDialog;




    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(TabletMainActivity.this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(CommonUtility.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablet_activity_main);

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        //t.setScreenName(TAG);
        t.set("&uid", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_ID));
        // Send a screen view.
        t.send(new HitBuilders.EventBuilder().setCategory("USER").setAction("User Sign In").build());

        aq = new AQuery(this);
        //aq.hardwareAccelerated11();
        aq.id(R.id.text_store_info).text(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_NAME));

        orderListView = (ListView)findViewById(R.id.listView);

        aq.id(R.id.btn_logout).clicked(this, "logoutClicked");




        aq.id(R.id.btn_finance).clicked(this, "financeClicked");
        //
        /*
        TODO VAN TEST 제거
         */
        aq.id(R.id.btn_settings).clicked(this, "settingsClicked");
        //aq.id(R.id.btn_settings).clicked(this, "vanTestClicked");
        aq.id(R.id.btn_price).clicked(this, "priceClicked");
        aq.id(R.id.btn_store_info).clicked(this, "storeInfoClicked");

        aq.id(R.id.notice_area).clicked(this, "noticeClicked");

        session = Session.getInstance(this);
        session.addCallback(rewhiteSessionCallback);

        preferences = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);

        setMaterialRippleLayout((View)findViewById(R.id.btn_order));
        setMaterialRippleLayout((View)findViewById(R.id.btn_order_out));
        setMaterialRippleLayout((View)findViewById(R.id.btn_order_rewhite));
        setMaterialRippleLayout((View)findViewById(R.id.btn_user));
        setMaterialRippleLayout((View)findViewById(R.id.btn_finance));
        setMaterialRippleLayout((View)findViewById(R.id.btn_order_local));

        setMaterialRippleLayout((View)findViewById(R.id.btn_settings));
        setMaterialRippleLayout((View)findViewById(R.id.btn_store_info));
        setMaterialRippleLayout((View)findViewById(R.id.btn_price));
        setMaterialRippleLayout((View)findViewById(R.id.btn_logout));

        mHandler = new Handler();
        mTask = new Runnable() {

            @Override
            public void run() {
                mFlagFinish = false;
            }
        };

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_current_date).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_store_info).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.notice_summary_text).getTextView());

        showDialog();
        loadNoticeElement();
        /*
        TODO 현장접수 only 대응코드
         */
        if("1".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_TYPE))){
            // 모바일 주문만
            aq.id(R.id.btn_order_rewhite).clicked(this, "orderRewhiteClicked");
            aq.id(R.id.btn_order).clicked(this, "onLocalAuthorizedClicked");
            aq.id(R.id.btn_order_out).clicked(this, "onLocalAuthorizedClicked");
            aq.id(R.id.btn_order_local).clicked(this, "onLocalAuthorizedClicked");
            aq.id(R.id.btn_user).clicked(this, "onLocalAuthorizedClicked");
            initialize();

        }else if("2".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_TYPE))){
            // 현장접수만
            aq.id(R.id.btn_order_rewhite).clicked(this, "onMobileAuthorizedClicked");
            aq.id(R.id.btn_order).clicked(this, "orderClicked");
            aq.id(R.id.btn_order_out).clicked(this, "orderOutClicked");
            aq.id(R.id.btn_order_local).clicked(this, "orderLocalClicked");
            aq.id(R.id.btn_user).clicked(this, "userManageClicked");

            localOnlyInitialize();

        }else if("3".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_TYPE))){
            // 모바일+현장 모두
            aq.id(R.id.btn_order_rewhite).clicked(this, "orderRewhiteClicked");
            aq.id(R.id.btn_order).clicked(this, "orderClicked");
            aq.id(R.id.btn_order_out).clicked(this, "orderOutClicked");
            aq.id(R.id.btn_order_local).clicked(this, "orderLocalClicked");
            aq.id(R.id.btn_user).clicked(this, "userManageClicked");
            initialize();
        }

        timerTask = new MainTimerTask();
        mTimer = new Timer();
        mTimer.schedule(timerTask, 5000, 60000);

        if (!"true".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Command.IS_TUTORIAL))) {
            TimerTask task = new TimerTask() {

                @Override
                public void run() {
                    DUtil.Log("GetStartedActivity start", "GetStartedActivity MAKE");

                    Intent tutorialIntent = new Intent(TabletMainActivity.this, TutorialTabletActivity.class);
                    tutorialIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    // tutorialIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    // // Task안남기도록 실행
                    startActivity(tutorialIntent);
                }
            };

            Timer timer = new Timer();
            timer.schedule(task, splashDelay);
        }
    }

    public void vanTestClicked(View button){
        Intent tutorialIntent = new Intent(TabletMainActivity.this, VanUsbTestActivity.class);
        tutorialIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        // tutorialIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        // // Task안남기도록 실행
        startActivity(tutorialIntent);
    }

    public void onLocalAuthorizedClicked(View button){
        DUtil.alertShow(this, "모바일 주문접수 기능만 사용하고 계십니다. 방문접수기능을 사용하려면 리화이트 고객센터로 전화문의해주세요.");
    }
    public void onMobileAuthorizedClicked(View button){
        DUtil.alertShow(this, "방문주문접수 기능만 사용하고 계십니다. 모바일 접수기능을 사용하려면 리화이트 고객센터로 전화문의해주세요.");
    }

    MainTimerTask timerTask;
    Timer mTimer;

    private Handler mTimeHandler = new Handler();
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            Date rightNow = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("M/d (E) a hh:mm", Locale.KOREAN);
            String dateString = formatter.format(rightNow);
            aq.id(R.id.text_current_date).text(dateString);
        }
    };

    class MainTimerTask extends TimerTask {
        public void run() {
            mHandler.post(mUpdateTimeTask);
        }
    }

    @Override
    protected void onDestroy() {
        mTimer.cancel();
        dismissDialog();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        mTimer.cancel();
        dismissDialog();
        super.onPause();
    }

    @Override
    protected void onResume() {

        timerTask = new MainTimerTask();
        mTimer = new Timer();
        mTimer.schedule(timerTask, 5000, 60000);
        appVersionCheck();
        super.onResume();
    }

    private void appVersionCheck() {
        RequestParams params = new RequestParams();
        params.add("k", "1");

        NetworkClient.post(Constants.VERSION_CHECK, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.VERSION_CHECK, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    JSONObject json = new JSONObject(result);

                    if ("S0000".equals(json.getString("resultCode"))) {
                        String version = json.getJSONObject("data").getString("ad");

                        int myVerNum = 0;
                        PackageManager m = mCtx.getPackageManager();

                        // 설치된 패키지의 버전이름 추출 : 1.0.0 스타일
                        String app_ver = m.getPackageInfo(mCtx.getPackageName(), 0).versionName;
                        Log.i("appver", version + " / " + app_ver);

                        // 구분자 '.'를 기반으로 5자리 Integer값으로 변환.
                        // 각 Middle, Minor 버전값은 99를 넘지 못한다.
                        myVerNum = MZValidator.convertVersionStringToInteger(app_ver);

                        // 추출된 버전값을 Preferences값에 저장한다.
                        //SharedPreferencesUtility.set(cmd, value);

                        // 서버에서 최신앱 버전정보를 조회하여 5자리 값으로 변환.
                        int currentVerNum = MZValidator.convertVersionStringToInteger(version);
                        Log.i("AppVersionNum", currentVerNum + "/" + myVerNum);

                        boolean isStrict = false;
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                        alertDialogBuilder.setCancelable(false).setMessage(getResources().getString(R.string.appupdate_minor))
                                .setPositiveButton("업데이트", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setData(Uri.parse("market://details?id=" + DUtil.getAppPackageName(mCtx)));
                                        startActivity(intent);
                                        finish();
                                    }
                                });

                        if (currentVerNum - myVerNum <= 0) {

                        } else {
                            if (currentVerNum - myVerNum >= 1000) {
                                // 강제업데이트
                                alertDialogBuilder.setCancelable(false);
                            } else {
                                alertDialogBuilder.setCancelable(true).setNegativeButton("다음에", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // 설치된 버전값이 서버값보다 작을 경우 update알림 :
                                        // Minor업데이트의 경우 필수업데이트가 아님.

                                    }
                                });
                            }

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (PackageManager.NameNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Mint.logException(e);
                }

            }
        });

    }

    private void localOnlyInitialize(){
        aq.id(R.id.mobile_area).gone();
        aq.id(R.id.local_push_area).visible();

        dismissDialog();
    }

    ListView orderListView;

    private synchronized void initialize() {
        //showDialog();
        aq.id(R.id.mobile_area).visible();
        aq.id(R.id.local_push_area).gone();

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("mode", "Y");
        params.put("page", 1);
        params.put("block", 100);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_LIST, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {

                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_LIST, result);

                    dismissDialog();

                    JSONObject jsondata = new JSONObject(result);
                    if ("S0000".equals(jsondata.getString("resultCode"))) {

                        if (jsondata.isNull("data")) {
                            // 주문내역이 없음
                            orderData = new ArrayList<>();
                            orderAdapter = new TabletMainListViewAdapter(mCtx, R.layout.tablet_main_rewhite_item, orderData);
                            orderAdapter.notifyDataSetChanged();
                            orderListView.setAdapter(orderAdapter);

                            aq.id(R.id.empty).visible();

                        } else {

                            orderListView = aq.id(R.id.listView).getListView();// (ListView)findViewById(R.id.listView);
                            orderData = new ArrayList<>();

                            JSONArray orderInfo = jsondata.getJSONArray("data");

                            if (orderInfo.length() > 0) {

                                for (int i = 0; i < orderInfo.length(); i++) {

                                    String orderId = orderInfo.getJSONObject(i).getString("orderId");
                                    String orderStatus = orderInfo.getJSONObject(i).getString("orderStatus");
                                    String addressSeq = orderInfo.getJSONObject(i).getString("addressSeq");
                                    String storeId = orderInfo.getJSONObject(i).getString("storeId");
                                    String deliveryPrice = orderInfo.getJSONObject(i).getString("deliveryPrice");
                                    String pickupRequestTimeApp = orderInfo.getJSONObject(i).getString("pickupRequestTimeApp");
                                    String deliveryRequestTimeApp = orderInfo.getJSONObject(i).getString("deliveryRequestTimeApp");
                                    String registerDateApp = orderInfo.getJSONObject(i).getString("registerDateApp");
                                    String orderRequest = orderInfo.getJSONObject(i).getString("orderRequest");
                                    String orderPickupItemMessage = orderInfo.getJSONObject(i).getString("orderPickupItemMessage");
                                    String statusTimeApp = orderInfo.getJSONObject(i).getString("statusTimeApp");
                                    String userName = orderInfo.getJSONObject(i).getString("userName");
                                    int quantity = orderInfo.getJSONObject(i).getInt("pickupQuantity");
                                    String isPayment = orderInfo.getJSONObject(i).getString("isPayment");
                                    String isReceivable = orderInfo.getJSONObject(i).getString("isReceivable");
                                    String isAdditionalPayment = orderInfo.getJSONObject(i).getString("isAdditionalPayment");

                                    String payments;
                                    if(orderInfo.getJSONObject(i).isNull("payments")){
                                        payments = null;
                                    }else{
                                        payments = orderInfo.getJSONObject(i).getString("payments");
                                    }

                                    OrderListItem aItem = new OrderListItem(orderId, orderStatus, addressSeq, storeId, userName, deliveryPrice, pickupRequestTimeApp,
                                            deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage, orderInfo.getJSONObject(i).toString(), quantity, isPayment, payments, isReceivable, isAdditionalPayment);
                                    orderData.add(aItem);

                                }

                                if (orderData.size() > 0) {
                                    aq.id(R.id.empty).gone();
                                }

                                aq.id(R.id.list_count).text(""+orderData.size());

                                orderAdapter = new TabletMainListViewAdapter(mCtx, R.layout.tablet_main_rewhite_item, orderData);
                                orderAdapter.notifyDataSetChanged();

                                orderListView.setAdapter(orderAdapter);


                            } else {

                            }

                        }

                        //dismissDialog();
                    } else if ("S9002".equals(jsondata.getString("resultCode"))) {
                        Session.getCurrentSession().close();
                        //dismissDialog();
                    } else {
                        //dismissDialog();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    //dismissDialog();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    //dismissDialog();
                }
            }
        });
    }

    private static final int MAIN_LIST_REFRESH = 13;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case MAIN_LIST_REFRESH:
                if (resultCode == RESULT_OK) {

                }
                initialize();
                break;
        }


    }

    private void setMaterialRippleLayout(View _target){
        MaterialRippleLayout.on(_target)
                .rippleColor(Color.parseColor("#FFffff"))
                .rippleAlpha(0.2f)
                .rippleHover(true)
                .rippleOverlay(true)
                .create();
    }

    public void orderClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setClass(mCtx, UserSearchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("type","INPUT");
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }, 0);
        Log.e(TAG, "click orderClicked");
    }

    public void orderOutClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setClass(mCtx, UserSearchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("type","OUTPUT");
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

            }
        }, 0);
        Log.e(TAG, "click orderOutClicked");
    }

    public void orderLocalClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, TabletLocalOrderListActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

            }
        }, 0);
        Log.e(TAG, "click orderRewhiteClicked");
    }

    public void orderRewhiteClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, TabletRewhiteActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

            }
        }, 0);
        Log.e(TAG, "click orderRewhiteClicked");
    }
    public void userManageClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, TabletUserManageActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                //overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
            }
        }, 0);
        Log.e(TAG, "click userManageClicked");
    }
    public void financeClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, TabletFinanceActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                //overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
            }
        }, 0);
        Log.e(TAG, "click financeClicked");
    }
    public void settingsClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, TabletSettingActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                //overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
            }
        }, 0);
        Log.e(TAG, "click financeClicked");
    }
    public void storeInfoClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, TabletStoreInfoActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                //overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
            }
        }, 0);
        Log.e(TAG, "click financeClicked");
    }
    public void priceClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, TabletPriceSettingActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                //overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
            }
        }, 0);
        Log.e(TAG, "click priceClicked");
    }

    public void itemSelected(int position, JSONObject json) {
        //String orderId = orderData.get(position).getOrderId();
        Intent intent = new Intent();

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setClass(this, TableOrderDetailActivity.class);
        intent.putExtra("data", json.toString());
        startActivityForResult(intent, MAIN_LIST_REFRESH);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        //overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class RewhiteSessionStatusCallback implements SessionCallback {

        /**
         * 세션이 오픈되었으면 가입페이지로 이동 한다.
         */
        @Override
        public void onSessionOpened() {
            // 프로그레스바를 보이고 있었다면 중지하고 세션 오픈후 보일 페이지로 이동
            TabletMainActivity.this.onSessionOpened();
            DUtil.Log(TAG, "onSessionOpened");
        }

        @Override
        public void onSessionOpening() {
            // 뺑글이 시작
            DUtil.Log(TAG, "onSessionOpening");
        }

        /**
         * 세션이 삭제되었으니 로그인 화면이 보여야 한다.
         *
         * @param exception 에러가 발생하여 close가 된 경우 해당 exception
         */
        @Override
        public void onSessionClosed(final RewhiteException exception) {
            // TODO Auto-generated method stub
            TabletMainActivity.this.onSessionClosed();
            DUtil.Log(TAG, "onSessionClosed = " + exception.getLocalizedMessage());
        }
    }

    public void logoutClicked(View button){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
        alertDialogBuilder.setCancelable(true).setMessage("로그아웃 하시겠습니까?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        session.close();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    protected void onSessionClosed() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, SignActivity.class);
                mCtx.startActivity(intent);
                ((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                ((Activity) mCtx).finish();
            }
        }, 200);
    }

    protected void onSessionOpened() {
        //Delivery/StoreInformation
        //Log.e("refreshUserInfo", "==================== onSessionOpened ==================== ");
        //Session.getCurrentSession().refreshUserInfo();
    }

    JSONArray noticeArr;
    String noticeTitle;
    String noticeUrl;

    private void loadNoticeElement(){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("page", 1);
        params.put("block", 1);
        params.put("k", 1);
        NetworkClient.post(Constants.SHOW_NOTICE, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SHOW_NOTICE, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.SHOW_NOTICE, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        noticeArr = jsondata.getJSONArray("data");

                        JSONObject topObj = noticeArr.getJSONObject(0);
                        noticeTitle = topObj.getString("title");
                        noticeUrl = topObj.getString("noticeUrl");
                        aq.id(R.id.notice_summary_text).text("<필수공지> "+noticeTitle);
                        //aq.id(R.id.text_date).text(TimeUtil.convertTimestampToString(topObj.getLong("registerDateApp")));

                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void noticeClicked(View button){
        if(noticeArr != null){
            Intent startIntent = new Intent(this, TabletHtmlActivity.class);

            startIntent.putExtra("title", noticeTitle);
            startIntent.putExtra("url", noticeUrl);
            startIntent.putExtra("icon", "N");

            startActivity(startIntent);
        }

    }
}
