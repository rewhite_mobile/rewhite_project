package me.rewhite.delivery.service;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.tablet.LocalOrderOutputActivity;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.TimeUtil;


/**
 * Created by marines on 2016. 10. 19..
 */

public class CallingService extends Service {
    public static final String EXTRA_CALL_NUMBER = "call_number";
    protected View rootView;
    public static final String TAG = "CallingService";

    //@InjectView(R.id.tv_call_number)
    AQuery aq;
    TextView tv_call_number;
    TextView tv_name;
    TextView tv_address;
    TextView last_order_date;
    boolean isPopupOpened = false;

    String call_number;

    WindowManager.LayoutParams params;
    private WindowManager windowManager;


    @Override
    public IBinder onBind(Intent intent) {

        // Not used
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG,"CallingService onCreate()");

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        Display display = windowManager.getDefaultDisplay();

        int width = (int) (display.getWidth() * 0.5); //Display 사이즈의 90%


        params = new WindowManager.LayoutParams(
                width,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                PixelFormat.TRANSLUCENT);


        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        rootView = layoutInflater.inflate(R.layout.call_popup_top, null);
        isPopupOpened = false;

        aq = new AQuery(rootView);
        tv_call_number = aq.id(R.id.tv_call_number).getTextView();
        tv_name = aq.id(R.id.tv_name).getTextView();
        tv_address = aq.id(R.id.tv_address).getTextView();
        last_order_date = aq.id(R.id.last_order_date).getTextView();

        aq.id(R.id.btn_close).clicked(this, "removePopup");

        aq.id(R.id.bottom_view).clicked(this, "detailOrderAction");

        setDraggable();
    }



    private void setDraggable() {

        rootView.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);

                        if (rootView != null)
                            windowManager.updateViewLayout(rootView, params);
                        return true;
                }
                return false;
            }
        });

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(!isPopupOpened){
            windowManager.addView(rootView, params);
            isPopupOpened = true;
        }
        setExtra(intent);

        return START_REDELIVER_INTENT;
    }


    private void setExtra(Intent intent) {

        if (intent == null) {
            removePopup();
            return;
        }

        call_number = intent.getStringExtra(EXTRA_CALL_NUMBER);
        getUserInfo(call_number);
        Log.i("CallingService", "setExtra= " + call_number);

    }

    private void getUserInfo(String callNumber){
        ///v2/Shop/UserFranchiseeList

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("page", 1);
        params.put("block", 10);
        params.put("search", "phone");
        params.put("keyword", callNumber.replace("-",""));
        params.put("listType", "A");
        params.put("k", 1);
        NetworkClient.post(Constants.USER_SEARCH, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.USER_SEARCH, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.USER_SEARCH, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);


                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if(jsondata.getJSONArray("data").length() == 0 || jsondata.isNull("data")){
                            removePopup();
                        }else{
                            userInfo = jsondata.getJSONArray("data").getJSONObject(0);
                            if (!TextUtils.isEmpty(call_number)) {
                                tv_call_number.setText(call_number);
                                tv_name.setText(userInfo.getString("userName"));
                                tv_address.setText("주소지 : " + userInfo.getString("address1") + userInfo.getString("address2"));
                                Log.i("CallingService", "setExtra= " + call_number);
                                Log.i("CallingService", "jsondata= " + jsondata.getJSONArray("data").toString());
                            }
                            if(userInfo.getInt("orderCount") > 0){
                                getOrderDataByUserId(userInfo.getString("userId"));
                            }else{
                                last_order_date.setText("주문내역 없음");
                            }

                        }

                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    JSONObject userInfo;
    JSONArray orderInfo;
    public void getOrderDataByUserId(String _userId){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("page", 1);
        params.put("block", 10);
        params.put("userId", _userId);
        params.put("k", 1);
        NetworkClient.post(Constants.GET_ORDER_DATA_BY_USERID, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GET_ORDER_DATA_BY_USERID, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_ORDER_DATA_BY_USERID, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);


                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if(jsondata.isNull("data")){
                            last_order_date.setText("주문내역 없음");
                        }else{
                            orderInfo = jsondata.getJSONArray("data");

                            if(jsondata.getJSONArray("data").length() == 0){
                                last_order_date.setText("주문내역 없음");
                            }else{
                                long orderTimestamp = orderInfo.getJSONObject(0).getLong("registerDateApp");

                                last_order_date.setText(TimeUtil.convertTimestampToString(orderTimestamp));
                            }
                        }

                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void detailOrderAction(View button){
        if(orderInfo != null && orderInfo.length() > 0){
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent intent = new Intent();
                    intent.setClass(rootView.getContext(), LocalOrderOutputActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    try {
                        intent.putExtra("userData", userInfo.toString());
                        intent.putExtra("orderId", orderInfo.getJSONObject(0).getString("orderId"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    startActivity(intent);
                    //startActivityForResult(intent, ORDER_LIST_REFRESH);
                    removePopup();
                }
            }, 0);
        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        removePopup();
    }

    public void removePopup(View button) {
        removePopup();
    }

    public void removePopup(){
        if (rootView != null && windowManager != null) windowManager.removeView(rootView);
        isPopupOpened = false;
    }
}
