package me.rewhite.delivery.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.androidquery.AQuery;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.util.HashMap;

import me.rewhite.delivery.activity.PickupItemActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

import static me.rewhite.delivery.util.SharedPreferencesUtility.UserInfo;

/**
 * Created by marines on 2015. 10. 5..
 */
public class ImageUploadTask extends MZAsyncTask<HashMap<String, Object>, Void, Boolean, PickupItemActivity> {
    final PickupItemActivity mActivity;
    private AQuery aq;
    SharedPreferences preferences;
    boolean uploadSuccess;

    HttpURLConnection conn = null;
    DataOutputStream dos = null;

    String lineEnd = "\r\n";
    String twoHyphens = "--";
    String boundary = "*****";

    int bytesRead, bytesAvailable, bufferSize;
    byte[] buffer;
    int maxBufferSize = 1024;
    int serverResponseCode;

    String TAG = "FileUpload";

    String rData;
    ProgressDialog pd;
    private Context mContext;
    String token;
    String secret;

    String fileName;
    File sourceFile;

    public ImageUploadTask(Context _context, PickupItemActivity target, SharedPreferences _preferences) {
        super(target);
        mContext = _context;
        mActivity = target;
        aq = new AQuery(mContext);
        // Enable hardware acceleration if the device has API 11 or above
        aq.hardwareAccelerated11();
        preferences = _preferences;
    }

    public void setPath(String uploadFilePath, ProgressDialog _pd){

        this.fileName = uploadFilePath;
        this.sourceFile = new File(uploadFilePath);
        this.pd = _pd;
    }

    @Override
    protected void onPreExcute() {
        super.onPreExecute();

        pd.show();


    }

    public HttpParams getParams() {
        // Tweak further as needed for your app
        HttpParams params = new BasicHttpParams();
        // set this to false, or else you'll get an Expectation Failed: error
        HttpProtocolParams.setUseExpectContinue(params, false);
        return params;
    }

    @Override
    protected Boolean doInBackground(PickupItemActivity target, HashMap<String, Object>... params) {

        if(!sourceFile.isFile()){
            Log.e("ImageUpload doInBackground", "sourceFile(" + fileName + ") is Not a File");
            return null;
        }else{
            String success = "Success";
            Log.e("ImageUpload doInBackground", "sourceFile(" + fileName + ") is  a File");

            try {

                //File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                File f = sourceFile;//new File(path, target.getPhotoFileName());
                Log.w("imageupload file", f.getAbsolutePath());
                Log.w("imageupload file", target.getPhotoFileName());

                Log.i("api request", Constants.API_HOST + Constants.UPLOAD_IMAGE + "");
                Log.i("filepath", f.getAbsolutePath() + "");
                Log.i("accessToken", SharedPreferencesUtility.get(UserInfo.ACCESS_TOKEN) + "");

                String accessToken = SharedPreferencesUtility.get(UserInfo.ACCESS_TOKEN)+"";

                MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                builder.addTextBody("accessToken", accessToken, ContentType.TEXT_PLAIN);

                builder.addTextBody("k", "1", ContentType.TEXT_PLAIN);
                builder.addPart("file", new FileBody(sourceFile));
                HttpEntity multipart = builder.build();

                InputStream is = null;

                DefaultHttpClient httpClient = new DefaultHttpClient();//AndroidHttpClient.newInstance("Android");//new DefaultHttpClient(getParams());

                HttpPost uploadPost = new HttpPost(Constants.API_HOST + Constants.UPLOAD_IMAGE);
                uploadPost.setEntity(multipart);
                HttpResponse httpResponse = httpClient.execute(uploadPost);

                is = httpResponse.getEntity().getContent();
                //Log.w("httpResponse", EntityUtils.toString(httpEntity));

                if(is != null){
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line = null;

                    while((line = bufferedReader.readLine()) != null){
                        stringBuilder.append(line + "\n");
                    }
                    is.close();

                    StatusLine st = httpResponse.getStatusLine();
                    int c = st.getStatusCode();

                    Log.i("result", stringBuilder.toString());
                    rData = stringBuilder.toString();
                    uploadSuccess = c < 400;
                }else{
                    rData = "FAIL";
                    uploadSuccess = false;
                    Log.w("is", "is null");
                }

                //httpClient.close();

//                if (httpClient.getConnectionManager() != null) {
//                    httpClient.close();
//                    httpClient = null;
//                }

            } catch(OutOfMemoryError e){
                Log.e("MEMORY EXCEPTION: ", e.toString());
            } catch(ConnectTimeoutException e){
                Log.e("Timeout Exception: ", e.toString());
            } catch(SocketTimeoutException ste){
                Log.e("Timeout Exception: ", ste.toString());
            } catch (IOException e){

                Log.e("V IOException [Send Request]","IO Exception");
                if((e != null) && (e.getMessage() != null)){
                    Log.e("V",e.getMessage().toString());
                }

            } catch (Exception e) {
                Log.e("V Exception [Send Request]","Exception Requester");
                if((e != null) && (e.getMessage() != null)){
                    Log.e("V",e.getMessage().toString());
                }

            }
//            finally {
//                if (httpClient != null && httpClient.getConnectionManager() != null) {
//                    httpClient.close();
//                    httpClient = null;
//                }
//            }
        }

        return true;
    }

    @Override
    protected void onCancelled() {
        uploadSuccess = false;
        pd.dismiss();
        Log.i("onCancelled", "Image Upload Cancelled");
    }


    @Override
    protected void onPostExecute(PickupItemActivity target, Boolean loginSuccess) {
        Log.i("onPostExecute", "Image Upload  Completed");

        if(pd.isShowing()){
            pd.dismiss();
        }


        if (uploadSuccess) {
            // 메인메뉴 화면 진입
            Log.i("onPostExecute", "Image Upload Success");
            target.uploadCompleted(rData);
        } else {
            Log.i("onPostExecute", "Image Upload Failed");
            // 로그인 실패 메시지 보여주고 로그인 화면 유지
            DUtil.alertShow(mActivity, "이미지 전송이 실패했습니다.");
        }

    }
}
