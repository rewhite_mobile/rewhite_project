package me.rewhite.delivery.task;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import com.androidquery.AQuery;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.tablet.LocalOrderInputActivity;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

/**
 * Created by marines on 2015. 10. 5..
 */
public class TabletImageUploadTask2 extends MZAsyncTask<HashMap<String, Object>, Void, Boolean, LocalOrderInputActivity> {
    final LocalOrderInputActivity context;
    private AQuery aq;
    SharedPreferences preferences;
    boolean uploadSuccess;

    String rData;
    ProgressDialog pDialog;
    String token;
    String secret;

    public TabletImageUploadTask2(LocalOrderInputActivity target, SharedPreferences _preferences) {
        super(target);
        context = target;
        aq = new AQuery(context);
        // Enable hardware acceleration if the device has API 11 or above
        aq.hardwareAccelerated11();
        preferences = _preferences;
    }

    public HttpParams getParams() {
        // Tweak further as needed for your app
        HttpParams params = new BasicHttpParams();
        // set this to false, or else you'll get an Expectation Failed: error
        HttpProtocolParams.setUseExpectContinue(params, false);
        return params;
    }

    @Override
    protected void onPreExcute(){
        super.onPreExecute();
        pDialog = new ProgressDialog(context);
        pDialog.setTitle("이미지 업로드");
        pDialog.setMessage("파일 전송중..");
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setIndeterminate(false);
        pDialog.show();
    }

    @Override
    protected Boolean doInBackground(LocalOrderInputActivity target, HashMap<String, Object>... params) {
        try {

            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File f = new File(path, target.getPhotoFileName());
            Log.w("imageupload file", target.getPhotoFileName());
            /*
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File f = new File(path, TEMP_PHOTO_FILE);*/

            String accessToken = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN)+"";

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addTextBody("accessToken", accessToken, ContentType.TEXT_PLAIN);

            builder.addTextBody("k", "1", ContentType.TEXT_PLAIN);
            builder.addPart("file", new FileBody(f));
            HttpEntity multipart = builder.build();

            InputStream is = null;

            DefaultHttpClient httpClient = new DefaultHttpClient();//AndroidHttpClient.newInstance("Android");//new DefaultHttpClient(getParams());

            HttpPost uploadPost = new HttpPost(Constants.API_HOST + Constants.UPLOAD_IMAGE);
            uploadPost.setEntity(multipart);
            HttpResponse httpResponse = httpClient.execute(uploadPost);

            is = httpResponse.getEntity().getContent();
            //Log.w("httpResponse", EntityUtils.toString(httpEntity));

            if(is != null){
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuilder stringBuilder = new StringBuilder();
                String line = null;

                while((line = bufferedReader.readLine()) != null){
                    stringBuilder.append(line + "\n");
                }
                is.close();

                StatusLine st = httpResponse.getStatusLine();
                int c = st.getStatusCode();

                Log.i("result", stringBuilder.toString());
                rData = stringBuilder.toString();
                uploadSuccess = c < 400;
            }else{
                rData = "FAIL";
                uploadSuccess = false;
                Log.w("is", "is null");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    protected void onCancelled() {
        uploadSuccess = false;
        Log.i("onCancelled", "Image Upload Cancelled");
    }

    @Override
    protected void onPostExecute(LocalOrderInputActivity target, Boolean loginSuccess) {
        Log.i("onPostExecute", "Image Upload  Completed");
        if(pDialog.isShowing()){
            pDialog.hide();
        }
        if (uploadSuccess) {
            // 메인메뉴 화면 진입
            Log.i("onPostExecute", "Image Upload Success");
            target.uploadCompleted(rData);
        } else {
            Log.i("onPostExecute", "Image Upload Failed");
            // 로그인 실패 메시지 보여주고 로그인 화면 유지
            DUtil.alertShow(context, "이미지 전송이 실패했습니다.");
        }

    }
}
