package me.rewhite.delivery.tablet2.adapter;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import me.rewhite.delivery.R;
import me.rewhite.delivery.util.MZValidator;

/**
 * Created by marines on 2017. 10. 10..
 */

public class OrderElementAdapter extends RecyclerView.Adapter<OrderElementAdapter.ListItemViewHolder> {

    private List<OrderElementModel> items;
    private SparseBooleanArray selectedItems;

    private MyAdapterListener onClickListener;

    public interface MyAdapterListener {

        void btnCopyOnClick(View v, int position);
        void btnPictureOnClick(View v, int position);
        void btnRemoveOnClick(View v, int position);
        void btnOnClick(View v, int position);
    }


    public OrderElementAdapter(List<OrderElementModel> modelData, MyAdapterListener listener) {
        if (modelData == null) {
            throw new IllegalArgumentException("modelData must not be null");
        }
        this.items = modelData;
        onClickListener = listener;

        selectedItems = new SparseBooleanArray();
    }

    /**
     * Adds and item into the underlying data set
     * at the position passed into the method.
     *
     * @param newModelData The item to add to the data set.
     * @param position The index of the item to remove.
     */
    public void addData(OrderElementModel newModelData, int position) {
        items.add(position, newModelData);
        notifyItemInserted(position);
    }

    /**
     * Removes the item that currently is at the passed in position from the
     * underlying data set.
     *
     * @param position The index of the item to remove.
     */
    public void removeData(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    public OrderElementModel getItem(int position) {

        return items.get(position);
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.list_tabletv2_order_element, parent, false);
        return new ListItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder viewHolder, int position) {
        OrderElementModel model = items.get(position);

        Log.e("OrderElementAdapter onBindViewHolder", model.tagId + " / " + model.itemTitle + " / " + model.confirmPrice);

        viewHolder.element_tag.setText(MZValidator.getTagFormmater(model.tagId, 4));
        viewHolder.element_price.setText(MZValidator.toNumFormat(model.confirmPrice));

        AQuery aq = new AQuery(viewHolder.itemView);

        String itemColor = model.itemColor;
        if("".equals(itemColor)){
            aq.id(R.id.color_image).gone();
        }else{
            int resId = viewHolder.itemView.getContext().getResources().getIdentifier("pui_order_element_color_" + itemColor, "mipmap", "me.rewhite.delivery");
            aq.id(R.id.color_image).image(resId).visible();
        }

        String laundry = model.laundry;
        String isRepair = (model.repair.length() > 0)?"수선":null;
        String isAdditional = (model.additional.length() > 0)?"기술":null;
        String isPart = (model.part.length() > 0)?"부속":null;

        String isService = model.isService;
        String isReLaundry = model.isReLaundry;

        String detailString = laundry;

        if(isRepair != null){
            if("".equals(detailString)){
                detailString += "수선("+model.repair.length()+")";
            }else{
                detailString += ", " + "수선("+model.repair.length()+")";
            }
            //aq.id(R.id.icon_02).visible();
        }else{
            //aq.id(R.id.icon_02).gone();
        }
        if(isAdditional != null){
            if("".equals(detailString)){
                detailString += isAdditional +"("+model.additional.length()+")";
            }else{
                detailString += ", " + isAdditional +"("+model.additional.length()+")";
            }

        }
        if(isPart != null){
            if("".equals(detailString)){
                detailString += isPart +"("+model.part.length()+")";
            }else{
                detailString += ", " + isPart +"("+model.part.length()+")";
            }
        }
        if(isService != null){
            if("Y".equals(isService)){
                if("".equals(detailString)){
                    detailString += "무료";
                }else{
                    detailString += ", " + "무료";
                }
            }
        }
        if(isReLaundry != null){
            if("Y".equals(isReLaundry)){
                if("".equals(detailString)){
                    detailString += "재세탁";
                }else{
                    detailString += ", " + "재세탁";
                }
            }
        }
        aq.id(R.id.element_detail).text(detailString);

        boolean isPhoto = false;
        if(model.photo.length() > 0){
            isPhoto = true;

            try {
                ImageOptions options = new ImageOptions();
                options.round = 0;
                options.ratio = 1.0f;
                //options.memCache = true;
                options.fileCache = true;
                options.targetWidth = 60;
                options.animation = AQuery.FADE_IN_NETWORK;

                aq.id(R.id.element_pic).image(model.photo.getJSONObject(0).getString("fileUrl"), options);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            aq.id(R.id.element_pic).image(R.mipmap.attached_picture_placeholder);
        }


        String itemTitle = model.itemTitle;
        itemTitle = itemTitle.replace("\n","");

        switch(model.itemSP){
            case 1:
                itemTitle += "(일반)";
                break;
            case 2:
                itemTitle += "(명품)";
                break;
            case 3:
                itemTitle += "(아동)";
                break;
        }
        viewHolder.element_name.setText(model.itemTitle);

        viewHolder.onClickListener = this.onClickListener;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        }
        else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<Integer>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public static class ListItemViewHolder extends RecyclerView.ViewHolder {
        // ViewHolder
        private MyAdapterListener onClickListener;

        ImageView element_pic;
        TextView element_tag;
        TextView element_name;
        TextView element_detail;
        TextView element_price;
        Button btn_copy;
        Button btn_remove;
        View element_back;

        public ListItemViewHolder(final View itemView) {
            super(itemView);
            element_back = (View) itemView.findViewById(R.id.element_back);
            element_tag = (TextView) itemView.findViewById(R.id.element_tag);
            element_name = (TextView) itemView.findViewById(R.id.element_name);
            element_detail = (TextView) itemView.findViewById(R.id.element_detail);
            element_price = (TextView) itemView.findViewById(R.id.element_price);

            element_pic = (ImageView)itemView.findViewById(R.id.element_pic);
            btn_copy = (Button)itemView.findViewById(R.id.btn_selector_copy);
            btn_remove = (Button)itemView.findViewById(R.id.btn_selector_remove);

            element_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.btnOnClick(v, getAdapterPosition());
                }
            });

            element_pic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.btnPictureOnClick(v, getAdapterPosition());
                }
            });

            btn_copy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.btnCopyOnClick(v, getAdapterPosition());
                }
            });

            btn_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.btnRemoveOnClick(v, getAdapterPosition());
                }
            });


        }
    }
}
