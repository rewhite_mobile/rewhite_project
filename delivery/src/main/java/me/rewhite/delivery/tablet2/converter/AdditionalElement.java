package me.rewhite.delivery.tablet2.converter;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by marines on 2017. 10. 20..
 */

public class AdditionalElement {

    public int additionalItemId;
    //public int additionalType;
    public String additionalItemTitle;
    public int additionalItemSP;
    public int additionalPrice;

    public int storePrice11;
    public int storePrice12;
    public int storePrice13;
    public int storePrice14;

    public void setElement(JSONObject _object){
        try {
            this.additionalItemId = _object.getInt("itemId");
            this.additionalItemTitle = _object.getString("itemTitle");
            this.additionalItemSP = _object.getInt("itemSP");

            switch(this.additionalItemSP){
                case 1:
                    this.additionalPrice = _object.getInt("storePrice1");
                    break;
                case 2:
                    this.additionalPrice = _object.getInt("storePrice2");
                    break;
                case 3:
                    this.additionalPrice = _object.getInt("storePrice3");
                    break;
                case 4:
                    this.additionalPrice = _object.getInt("storePrice4");
                    break;
            }
            this.storePrice11 = _object.getInt("storePrice1");
            this.storePrice12 = _object.getInt("storePrice2");
            this.storePrice13 = _object.getInt("storePrice3");
            this.storePrice14 = _object.getInt("storePrice4");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setElementFromOrder(JSONObject _object){
        try {
            this.additionalItemId = _object.getInt("additionalItemId");
            //this.additionalType = _object.getInt("additionalType");
            this.additionalItemTitle = _object.getString("additionalItemTitle");
            this.additionalItemSP = _object.getInt("additionalItemSP");
            this.additionalPrice = _object.getInt("additionalPrice");

            this.storePrice11 = _object.getInt("storePrice1");
            this.storePrice12 = _object.getInt("storePrice2");
            this.storePrice13 = _object.getInt("storePrice3");
            this.storePrice14 = _object.getInt("storePrice4");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setElementFromInput(JSONObject _object, int sp){
        try{
            this.additionalItemId = _object.getInt("itemId");
            this.additionalItemTitle = _object.getString("itemTitle");
            this.additionalItemSP = sp;
            switch(this.additionalItemSP){
                case 1:
                    this.additionalPrice = _object.getInt("storePrice11");
                    break;
                case 2:
                    this.additionalPrice = _object.getInt("storePrice12");
                    break;
                case 3:
                    this.additionalPrice = _object.getInt("storePrice13");
                    break;
                case 4:
                    this.additionalPrice = _object.getInt("storePrice14");
                    break;
            }

            this.storePrice11 = _object.getInt("storePrice11");
            this.storePrice12 = _object.getInt("storePrice12");
            this.storePrice13 = _object.getInt("storePrice13");
            this.storePrice14 = _object.getInt("storePrice14");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject getJSONObject(int sp){
        JSONObject temp = new JSONObject();

        try {
            temp.put("itemId", this.additionalItemId);
            temp.put("itemTitle", this.additionalItemTitle);
            temp.put("itemSP", sp);
            switch(sp){
                case 1:
                    temp.put("itemPrice", this.storePrice11);
                    break;
                case 2:
                    temp.put("itemPrice", this.storePrice12);
                    break;
                case 3:
                    temp.put("itemPrice", this.storePrice13);
                    break;
                case 4:
                    temp.put("itemPrice", this.storePrice14);
                    break;
            }
            temp.put("storePrice1", this.storePrice11);
            temp.put("storePrice2", this.storePrice12);
            temp.put("storePrice3", this.storePrice13);
            temp.put("storePrice4", this.storePrice14);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return temp;
    }
}
