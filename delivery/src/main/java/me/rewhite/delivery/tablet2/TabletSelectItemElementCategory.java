package me.rewhite.delivery.tablet2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.TabletBaseActivity;

public class TabletSelectItemElementCategory extends TabletBaseActivity implements AdapterView.OnItemClickListener{

    public AQuery aq;
    GridView gvCategory;
    GridView gvElement;

    JSONArray priceData;
    JSONArray cateSubData;
    int pickedItemId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablet_select_item_element_category);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                if (intent.getStringExtra("priceData") != null) {
                    priceData = new JSONArray(intent.getStringExtra("priceData"));
                    pickedItemId = intent.getIntExtra("selectedElementDataItemId", -1);
                    Log.e("priceData", priceData.length() + "");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        aq = new AQuery(this);
        aq.id(R.id.btn_popup_close).clicked(this, "popupClose");

        // 커스텀 아답타 생성
        CategoryPickAdapter adapter = new CategoryPickAdapter(
                this,
                R.layout.tablet_pui_items_layout_row,       // GridView 항목의 레이아웃 row.xml
                priceData);    // 데이터

        gvCategory = (GridView)findViewById(R.id.gridView01);
        gvCategory.setAdapter(adapter);  // 커스텀 아답타를 GridView 에 적용
        gvCategory.setOnItemClickListener(this);


        gvElement = (GridView)findViewById(R.id.gridView02);
        gvElement.setOnItemClickListener(this);
    }

    public void popupClose(View button){
        finish();
    }

    String step1ItemTitle;
    int step1ItemId = -1;
    int step1CateId = -1;
    String step2ItemTitle;
    int step2ItemId = -1;
    JSONArray subData;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if(parent == gvCategory){
//            view.findViewById(R.id.element).setBackgroundResource(R.drawable.tablet_pui_button_drawable_pressed);
//            ((TextView)view.findViewById(R.id.parts_title)).setTextColor(0xFFFFFFFF);
//            Log.e("gridView1 clicked", position + "");

            subData = null;
            try {
                subData = priceData.getJSONObject(position).getJSONArray("items");
                step1ItemTitle = priceData.getJSONObject(position).getString("itemTitle");
                step1CateId = priceData.getJSONObject(position).getInt("categoryId");
                step1ItemId = priceData.getJSONObject(position).getInt("itemId");

                step2ItemTitle = null;
                step2ItemId = -1;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // 커스텀 아답타 생성
            ItemPickAdapter itemAdapter = new ItemPickAdapter(
                    this,
                    R.layout.tablet_pui_items_layout_row,       // GridView 항목의 레이아웃 row.xml
                    subData);    // 데이터
            gvElement.setAdapter(itemAdapter);
            gvElement.invalidate();
        }else if(parent == gvElement){
            Log.e("gridView2 clicked", position + "");
            try {
                step2ItemTitle = subData.getJSONObject(position).getString("itemTitle");
                step2ItemId = subData.getJSONObject(position).getInt("itemId");

                Intent resultData = new Intent();
                resultData.putExtra("step1ItemTitle", step1ItemTitle);
                resultData.putExtra("step1CateId", step1CateId);
                resultData.putExtra("step1ItemId", step1ItemId);
                resultData.putExtra("step2ItemTitle", step2ItemTitle);
                resultData.putExtra("step2ItemId", step2ItemId);
                resultData.putExtra("priceData", subData.getJSONObject(position).toString());

                Log.e("priceData", subData.getJSONObject(position).toString());

                setResult(Activity.RESULT_OK, resultData);
                finish();
            }catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    class CategoryPickAdapter extends BaseAdapter {
        Context context;
        int layout;
        JSONArray categoryData;
        LayoutInflater inf;

        public CategoryPickAdapter(Context context, int layout, JSONArray categoryData) {
            this.context = context;
            this.layout = layout;
            this.categoryData = categoryData;
            inf = (LayoutInflater) context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return categoryData.length();
        }

        @Override
        public Object getItem(int position) {
            JSONObject obj = new JSONObject();
            try {
                obj = categoryData.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return obj;
        }

        @Override
        public long getItemId(int position) {
            try {
                return categoryData.getJSONObject(position).getInt("itemId");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView==null)
                convertView = inf.inflate(layout, null);

            try {
                int itemId = categoryData.getJSONObject(position).getInt("itemId");
                String itemTitle = categoryData.getJSONObject(position).getString("itemTitle");
                boolean isPicked = false;

//                for(int j = 0; j < categoryData.length(); j++){
//                    int pickedCateId = pickedItemId;
//                    if(itemId == pickedCateId){
//                        convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_pressed);
//                        ((TextView)convertView.findViewById(R.id.parts_title)).setTextColor(0xFFFFFFFF);
//                        Log.i("picked initialize", pickedCateId + "");
//                        isPicked = true;
//                    }
//                }
//
//                if(!isPicked){
//                    convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_normal);
//                    ((TextView)convertView.findViewById(R.id.parts_title)).setTextColor(0xFF454545);
//                }
                TextView iv = (TextView)convertView.findViewById(R.id.parts_title);
                iv.setText(categoryData.getJSONObject(position).getString("itemTitle"));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return convertView;
        }
    }

    class ItemPickAdapter extends BaseAdapter {
        Context context;
        int layout;
        JSONArray categorySubData;
        LayoutInflater inf;

        public ItemPickAdapter(Context context, int layout, JSONArray categorySubData) {
            this.context = context;
            this.layout = layout;
            this.categorySubData = categorySubData;
            inf = (LayoutInflater) context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return categorySubData.length();
        }

        @Override
        public Object getItem(int position) {
            JSONObject obj = new JSONObject();
            try {
                obj = categorySubData.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return obj;
        }

        @Override
        public long getItemId(int position) {
            try {
                return categorySubData.getJSONObject(position).getInt("itemId");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView==null)
                convertView = inf.inflate(layout, null);

            try {
                int itemId = categorySubData.getJSONObject(position).getInt("itemId");
                String itemTitle = categorySubData.getJSONObject(position).getString("itemTitle");
                boolean isPicked = false;

//                for(int j = 0; j < categorySubData.length(); j++){
//                    int pickedCateId = pickedItemId;
//                    if(itemId == pickedCateId){
//                        convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_pressed);
//                        ((TextView)convertView.findViewById(R.id.parts_title)).setTextColor(0xFFFFFFFF);
//                        Log.i("picked initialize", pickedCateId + "");
//                        isPicked = true;
//                    }
//                }
//
//                if(!isPicked){
//                    convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_normal);
//                    ((TextView)convertView.findViewById(R.id.parts_title)).setTextColor(0xFF454545);
//                }

                TextView iv = (TextView)convertView.findViewById(R.id.parts_title);
                iv.setText(categorySubData.getJSONObject(position).getString("itemTitle"));


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return convertView;
        }
    }
}
