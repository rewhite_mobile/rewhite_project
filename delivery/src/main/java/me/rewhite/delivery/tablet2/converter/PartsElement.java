package me.rewhite.delivery.tablet2.converter;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by marines on 2017. 10. 20..
 */

public class PartsElement {

    public int partsItemId;
    //public int additionalType;
    public String partsItemTitle;

    public void setElement(JSONObject _object){
        try {
            this.partsItemId = _object.getInt("partId");
            this.partsItemTitle = _object.getString("partTitle");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setElementFromOrder(JSONObject _object){
        try {
            this.partsItemId = _object.getInt("partId");
            this.partsItemTitle = _object.getString("partTitle");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public JSONObject getJSONObject(){
        JSONObject temp = new JSONObject();

        try {
            temp.put("partId", this.partsItemId);
            temp.put("partTitle", this.partsItemTitle);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return temp;
    }
}
