package me.rewhite.delivery.tablet2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.TabletBaseActivity;

public class TabletSelectItemElementParts extends TabletBaseActivity implements AdapterView.OnItemClickListener {

    AQuery aq;
    JSONArray pickedParts;
    JSONArray partsArray;
    GridView gv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablet_select_item_element_repair);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                if (intent.getStringExtra("data") != null) {
                    partsArray = new JSONArray(intent.getStringExtra("data"));
                    Log.e("partsArray", partsArray.toString());
                }
                if (intent.getStringExtra("pickedParts") != null) {
                    pickedParts = new JSONArray(intent.getStringExtra("pickedParts"));
                    Log.i("already picked", pickedParts.toString());
                }else{
                    pickedParts = new JSONArray();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // 커스텀 아답타 생성
        ColorPickAdapter adapter = new ColorPickAdapter (
                getApplicationContext(),
                R.layout.pui_parts_layout_row,       // GridView 항목의 레이아웃 row.xml
                partsArray);    // 데이터

        gv = (GridView)findViewById(R.id.gridView);
        gv.setAdapter(adapter);  // 커스텀 아답타를 GridView 에 적용
        gv.setOnItemClickListener(this);

        aq = new AQuery(this);
        aq.id(R.id.btn_popup_close).clicked(this, "closeAction");
        aq.id(R.id.btn_save).clicked(this, "saveAction");
        aq.id(R.id.title_image).image(R.mipmap.title_parts_picker);

    }


    public void closeAction(View button){
        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }


    public void saveAction(View button){
        Intent resultData = new Intent();
        resultData.putExtra("value", pickedParts.toString());
        setResult(Activity.RESULT_OK, resultData);
        finish();
    }

    int old_position = 0;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
/*
        if(old_position<parent.getChildCount())
            parent.getChildAt(old_position).findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_normal);
            ((TextView)parent.getChildAt(old_position).findViewById(R.id.parts_title)).setTextColor(0xFF454545);
*/
        view.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_pressed);
        ((TextView)view.findViewById(R.id.parts_title)).setTextColor(0xFFFFFFFF);
        Log.e("gridView clicked", position + "");

        try {
            boolean isExisted = false;
            int partId = partsArray.getJSONObject(position).getInt("partId");
            JSONArray temp = new JSONArray();
            if(pickedParts.length() > 0){
                for(int j = 0; j < pickedParts.length(); j++){
                    if(partId == pickedParts.getJSONObject(j).getInt("partId")){
                        //pickedParts.remove(j);
                        view.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_normal);
                        ((TextView)view.findViewById(R.id.parts_title)).setTextColor(0xFF454545);
                        isExisted = true;
                    }else{
                        temp.put(pickedParts.getJSONObject(j));
                    }
                }
            }
            if(!isExisted){
                temp.put(partsArray.getJSONObject(position));
            }

            pickedParts = temp;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        old_position=position;
        Log.i("PartsClicked", pickedParts.toString());
    }

    class ColorPickAdapter extends BaseAdapter {
        Context context;
        int layout;
        JSONArray parts;
        LayoutInflater inf;

        public ColorPickAdapter(Context context, int layout, JSONArray parts) {
            this.context = context;
            this.layout = layout;
            this.parts = parts;
            inf = (LayoutInflater) context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return parts.length();
        }

        @Override
        public Object getItem(int position) {
            JSONObject obj = new JSONObject();
            try {
                obj = parts.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return obj;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView==null)
                convertView = inf.inflate(layout, null);

            try {
                int partId = partsArray.getJSONObject(position).getInt("partId");
                boolean isPicked = false;

                for(int j = 0; j < pickedParts.length(); j++){
                    int pickedPartId = pickedParts.getJSONObject(j).getInt("partId");
                    if(partId == pickedPartId){
                        convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_pressed);
                        ((TextView)convertView.findViewById(R.id.parts_title)).setTextColor(0xFFFFFFFF);
                        Log.i("picked initialize", pickedPartId + "");
                        isPicked = true;
                    }
                }

                if(!isPicked){
                    convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_normal);
                    ((TextView)convertView.findViewById(R.id.parts_title)).setTextColor(0xFF454545);
                }

                TextView iv = (TextView)convertView.findViewById(R.id.parts_title);
                iv.setText(parts.getJSONObject(position).getString("partTitle"));
                //@drawable/pui_button_drawable_normal

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return convertView;
        }
    }
}
