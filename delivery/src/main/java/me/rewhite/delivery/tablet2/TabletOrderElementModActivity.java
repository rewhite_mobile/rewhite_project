package me.rewhite.delivery.tablet2;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.balysv.materialripple.MaterialRippleLayout;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.tablet2.adapter.DividerItemDecoration;
import me.rewhite.delivery.tablet2.adapter.OrderElementAdapter;
import me.rewhite.delivery.tablet2.adapter.OrderElementModel;
import me.rewhite.delivery.tablet2.converter.AdditionalElement;
import me.rewhite.delivery.tablet2.converter.PartsElement;
import me.rewhite.delivery.tablet2.converter.RepairElement;
import me.rewhite.delivery.tablet2.mod.TabletElementPriceMod;
import me.rewhite.delivery.tablet2.mod.TabletElementTagMod;
import me.rewhite.delivery.task.TabletImageUploadTaskOrderMod;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class TabletOrderElementModActivity extends TabletBaseActivity  {

    AQuery aq;
    Context mCtx;

    private static final int PICK_COLOR = 11;
    private static final int PICK_REPAIR = 12;
    private static final int PICK_PARTS = 13;
    private static final int PICK_ADDITIONAL = 14;
    private static final int PICK_PRICE = 15;
    public static final int MOD_TAG = 17;
    public static final int SELECT_CATEGORY = 18;
    private static final int COPY_ELEMENT = 19;

    public static final String[] INITIAL_PERMS={
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    public static final String[] STORAGE_PERMS={
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    public static final int INITIAL_REQUEST=1337;
    public static final int STORAGE_REQUEST=INITIAL_REQUEST+6;

    private static final int REQ_CODE_PICK_IMAGE = 0;
    public String TEMP_PHOTO_FILE = "upload.jpg"; // 임시 저장파일
    private final int CAMERA_CAPTURE = 1;
    private final int CROP_PIC = 2;
    private Uri picUri;
    public JSONArray imagePickValue;
    SharedPreferences preferences;

    RecyclerView recyclerView;
    OrderElementAdapter adapter;

    JSONObject orderData;
    int orderId;

    List<OrderElementModel> orderModelData;
    SparseArray<OrderElementModel> orderModelMap;

    public JSONArray priceArray;
    public JSONArray repairArray;
    public JSONArray additionalArray;
    public JSONArray partsArray;
    public JSONArray colorsArray;

    int prevTagNo = -1;
    public int tagNo;
    public int tagLength;
    public int bucketIndex = 0;
    public int pickupMode = -1;
    boolean isFree = false;
    boolean isRetry = false;

    ProgressDialog mProgressDialog;
    boolean isInitialized = false;

    /*
    * 우측패널 입력값
    * */
    public OrderElementModel selectedElementData;
    String tempStep1ItemTitle;
    int tempStep1CateId;
    int tempStep1ItemId;
    String tempStep2ItemTitle;
    int tempStep2ItemId;
    int itemSP = -1;

    int isStep1ChoiceItem = 0;
    int isStep2ChoiceItem = 0;

    public String rightPanelMode = "U";


    OrderElementAdapter.MyAdapterListener myAdapterListener = new OrderElementAdapter.MyAdapterListener() {
        @Override
        public void btnOnClick(View v, int position) {
            Log.d("btnOnClick", "btnOnClick at position "+position);

            rightPanelMode = "U";
            OrderElementModel newModel = new OrderElementModel((OrderElementModel)adapter.getItem(position));

            try {
                Log.w("btnOnClick newmodel", newModel.getJSONObject().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            /*
                TODO
                사진등록후 저장안한 경우에도 adapter에 해당내용이 반영되어 수정된것처럼 표시됨
                deep copy 처리 해야될지?
             */
            setRightPanel(newModel);
            myToggleSelection(position);
            Log.d("btnOnClick tagId", ""+selectedElementData.tagId);
        }

        @Override
        public void btnPictureOnClick(View v, int position) {
            Log.d("btnPictureOnClick", "btnPictureOnClick at position "+position);
        }

        @Override
        public void btnCopyOnClick(View v, int position) {
            Log.d("btnCopyOnClick", "btnCopyOnClick at position "+position);
            OrderElementModel data = adapter.getItem(position);
            JSONObject tempElement = new JSONObject();
            try {
                data.tagId = tagNo;
                data.photo = new JSONArray();
                data.repair = new JSONArray();
                data.additional = new JSONArray();
                data.part = new JSONArray();

                tempElement = data.getCopyJSONObject();
                Log.e("copyItems", tempElement.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            copyItems(tempElement, true);

        }

        @Override
        public void btnRemoveOnClick(View v, int position) {
            Log.d("btnRemoveOnClick", "btnRemoveOnClick at position "+position);
            OrderElementModel data = adapter.getItem(position);
            removeItems(data.tagId, data.orderId, data.itemId);
        }
    };

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent resultData = new Intent();
        resultData.putExtra("orderId", orderId);
        setResult(Activity.RESULT_OK, resultData);
        finish();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablet_order_element_mod);

        mCtx = this;

        showDialog();

        setMaterialRippleLayout((View)findViewById(R.id.btn_mod_price));
        setMaterialRippleLayout((View)findViewById(R.id.btn_submit));
        setMaterialRippleLayout((View)findViewById(R.id.btn_card_submit));
//        setMaterialRippleLayout((View)findViewById(R.id.btn_tag_text));
//        setMaterialRippleLayout((View)findViewById(R.id.btn_new_element));

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("orderData") != null) {
                try {
                    orderData = new JSONObject(intent.getStringExtra("orderData"));
                    orderId = intent.getIntExtra("orderId", -1);
                    Log.e("orderData", orderData.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        aq = new AQuery(this);
        aq.id(R.id.btn_popup_close).clicked(this, "popupClose");
        aq.id(R.id.btn_tag_text).clicked(this, "modTagAction");
        aq.id(R.id.btn_new_element).clicked(this, "createNewEntity");
        aq.id(R.id.btn_selector_category).clicked(this, "selectCategoryAction");
        aq.id(R.id.btn_selector_repair).clicked(this, "selectRepairAction");
        aq.id(R.id.btn_selector_additional).clicked(this, "selectAdditionalAction");
        aq.id(R.id.btn_selector_parts).clicked(this, "selectPartsAction");
        aq.id(R.id.btn_selector_color).clicked(this, "selectColorAction");
        aq.id(R.id.btn_card_submit).clicked(this, "elementModAction");
        aq.id(R.id.btn_mod_price).clicked(this, "priceModAction");
        aq.id(R.id.btn_submit).clicked(this, "submitFinalAction");


        // Step1
        aq.id(R.id.pui_detail_btn_step1_01).clicked(this, "selectType").tag(1);
        aq.id(R.id.pui_detail_btn_step1_02).clicked(this, "selectType").tag(2);
        aq.id(R.id.pui_detail_btn_step1_03).clicked(this, "selectType").tag(3);

        // Step2
        aq.id(R.id.pui_detail_btn_step2_01).clicked(this, "selectWashingMethod").tag(1);
        aq.id(R.id.pui_detail_btn_step2_02).clicked(this, "selectWashingMethod").tag(2);
        aq.id(R.id.pui_detail_btn_step2_03).clicked(this, "selectWashingMethod").tag(3);

        // Price Group
        aq.id(R.id.pui_detail_btn_step3_01).clicked(this, "selectPriceGroup").tag(1);
        aq.id(R.id.pui_detail_btn_step3_02).clicked(this, "selectPriceGroup").tag(2);
        aq.id(R.id.pui_detail_btn_step3_03).clicked(this, "selectPriceGroup").tag(3);

        // Free Group
        aq.id(R.id.pui_detail_btn_etc_free).clicked(this, "selectFreeGroup").tag(1);
        aq.id(R.id.pui_detail_btn_etc_retry).clicked(this, "selectFreeGroup").tag(2);
        aq.id(R.id.pui_detail_btn_etc_none).clicked(this, "selectFreeGroup").tag(3);

        // Photo
        aq.id(R.id.btn_selector_picture_01).clicked(this, "pickPhoto").tag(1);
        aq.id(R.id.btn_selector_picture_02).clicked(this, "pickPhoto").tag(2);
        aq.id(R.id.btn_selector_picture_03).clicked(this, "pickPhoto").tag(3);

        try {
            initialize(orderData.getJSONArray("pickupItemsV2"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void setMaterialRippleLayout(View _target){
        MaterialRippleLayout.on(_target)
                .rippleColor(Color.parseColor("#FFffff"))
                .rippleAlpha(0.2f)
                .rippleHover(true)
                .rippleOverlay(true)
                .create();
    }

    public void submitFinalAction(View button){
        Intent resultData = new Intent();
        resultData.putExtra("orderId", orderId);
        setResult(Activity.RESULT_OK, resultData);
        finish();
    }

    public void pickPhoto(View button){
        int tag = (int)button.getTag();
        int compareTagNo = tag;

        if(selectedElementData.photo != null){
            if(compareTagNo <= selectedElementData.photo.length()){
                // 삭제
                int resId = getResources().getIdentifier("iv_picture_0"+compareTagNo, "id", "me.rewhite.delivery");
                int resPlaceHolderId = getResources().getIdentifier("tablet_toggle_button_type01", "mipmap", "me.rewhite.delivery");
                ImageView iv = aq.id(resId).getImageView();
                aq.id(iv).image(resPlaceHolderId);
                int resRemoveId = getResources().getIdentifier("btn_selector_picture_0"+compareTagNo, "id", "me.rewhite.delivery");
                ImageButton btn = (ImageButton)findViewById(resRemoveId);
                //aq.id(btn).image(R.mipmap.icon_button_camera).clicked(this, "pickPhoto").tag(compareTagNo);

                try {
                    JSONArray tempPhotoArray = new JSONArray();
                    for(int i = 0 ; i < selectedElementData.photo.length(); i++){
                        if(compareTagNo == (i+1)){

                        }else{
                            JSONObject obj = selectedElementData.photo.getJSONObject(i);
                            tempPhotoArray.put(obj);
                        }
                    }
                    selectedElementData.photo = tempPhotoArray;

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else {
                if(selectedElementData.photo.length() < 3){

                    try{
                        if ( Build.VERSION.SDK_INT >= 23){
                            if (!canAccessStorage()) {
                                requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                            }else{
                                addIntentCall();
                            }
                        }else{
                            addIntentCall();
                        }
                    }
                    catch(NullPointerException ex){
                        addIntentCall();
                    }


                }else{
                    DUtil.alertShow(this, "사진 3장만 첨부 가능합니다.\n" +
                            "등록하신 사진 삭제 후에 \n" +
                            "다시 추가해주세요.");
                }
            }

        }else{
            // 추가
            try{
                if ( Build.VERSION.SDK_INT >= 23){
                    if (!canAccessStorage()) {
                        requestPermissions(STORAGE_PERMS, STORAGE_REQUEST);
                    }else{
                        addIntentCall();
                    }
                }else{
                    addIntentCall();
                }
            }
            catch(NullPointerException ex){
                addIntentCall();
            }
        }
    }

    public void priceModAction(View button){

        if(selectedElementData.itemSP == -1 || isStep1ChoiceItem == -1 || selectedElementData.itemId == -1){

            Log.w("calculatePrice","입력항목 부족으로 가격설정 불가" + selectedElementData.itemSP + " / " + isStep1ChoiceItem + " / " + selectedElementData.itemId );
            DUtil.alertShow(this, "세탁품목을 먼저 선택해주세요.");
            return;
        }

        Intent pickIntent = new Intent(this, TabletElementPriceMod.class);
        pickIntent.putExtra("pickedPrice", selectedElementData.confirmPrice);
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, PICK_PRICE);
    }

    public void selectType(View button){
        int tag = (int)button.getTag();
        isStep1ChoiceItem = tag;

        switch(tag){
            case 1:
                aq.id(R.id.repair_layout).gone();
                aq.id(R.id.laundry_layout).visible();
                // 수선 입력항목 제거
                JSONArray blankArray = new JSONArray();
                selectedElementData.repair = blankArray;
                aq.id(R.id.btn_selector_repair).getButton().setText("선택");
                aq.id(R.id.btn_selector_repair).background(R.drawable.tablet_toggle_button_type01).textColor(Color.parseColor("#576874"));
                break;
            case 2:
                aq.id(R.id.repair_layout).visible();
                aq.id(R.id.laundry_layout).gone();
                // 세탁 입력항목 제거
                RadioGroup step2Group = (RadioGroup)findViewById(R.id.radiogroup_step2);
                step2Group.clearCheck();
                selectedElementData.laundry = "";
                break;
            case 3:
                aq.id(R.id.repair_layout).visible();
                aq.id(R.id.laundry_layout).visible();
                break;
        }

        calculatePrice(true);
    }

    public void selectFreeGroup(View button){
        int tag = (int)button.getTag();
        switch(tag){
            case 1:
                isFree = true;
                isRetry = false;
                selectedElementData.isService = "Y";
                selectedElementData.isReLaundry = "N";
                break;
            case 2:
                isFree = false;
                isRetry = true;
                selectedElementData.isService = "N";
                selectedElementData.isReLaundry = "Y";
                break;
            case 3:
                isFree = false;
                isRetry = false;
                selectedElementData.isService = "N";
                selectedElementData.isReLaundry = "N";
                break;
        }

        calculatePrice(true);
    }



    public void selectWashingMethod(View button){
        int tag = (int)button.getTag();
        isStep2ChoiceItem = tag;

        switch(tag){
            case 1:
                selectedElementData.laundry = "드라이";
                break;
            case 2:
                selectedElementData.laundry = "물세탁";
                break;
            case 3:
                selectedElementData.laundry = "다림질";
                break;
        }

        calculatePrice(false);
    }

    public void selectPriceGroup(View button){
        int tag = (int)button.getTag();

        switch(tag){
            case 1:
                selectedElementData.itemSP = 1;
                break;
            case 2:
                selectedElementData.itemSP = 2;
                break;
            case 3:
                selectedElementData.itemSP = 3;
                break;
            case 4:
                selectedElementData.itemSP = 4;
                break;
        }

        calculatePrice(true);
    }

    public void elementModAction(View button){
        if(tagNo == selectedElementData.tagId){
            // 신규등록
            if(selectedElementData.itemSP == -1 || isStep1ChoiceItem == -1 || selectedElementData.itemId == -1){
                Log.w("calculatePrice","입력항목 부족으로 계산불가" + selectedElementData.itemSP + " / " + isStep1ChoiceItem + " / " + selectedElementData.itemId );
                DUtil.alertShow(this, "필수항목을 선택해주세요.");
                return;
            }else{

                if("".equals(selectedElementData.laundry) && (isStep1ChoiceItem == 1 || isStep1ChoiceItem == 3)){
                    Log.w("calculatePrice","입력항목 부족으로 계산불가" + selectedElementData.itemSP + " / " + isStep1ChoiceItem + " / " + selectedElementData.itemId );
                    DUtil.alertShow(this, "필수항목을 선택해주세요.");
                    return;
                }else{
                    if(isStep1ChoiceItem == 2 || isStep1ChoiceItem == 3){
                        if(selectedElementData.repair.length() == 0){
                            Log.w("calculatePrice","수선항목 미선택"  );
                            DUtil.alertShow(this, "수선할 항목이 선택되지 않았습니다.");
                            return;
                        }else{
                            try {
                                copyItems(selectedElementData.getJSONObject(), true);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }else{
                        try {
                            copyItems(selectedElementData.getJSONObject(), true);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }else{
            if(selectedElementData.itemSP == -1 || isStep1ChoiceItem == -1 || selectedElementData.itemId == -1){
                Log.w("calculatePrice","입력항목 부족으로 계산불가" + selectedElementData.itemSP + " / " + isStep1ChoiceItem + " / " + selectedElementData.itemId );
                DUtil.alertShow(this, "필수항목을 선택해주세요.");
                return;
            }else{
                if("".equals(selectedElementData.laundry) && (isStep1ChoiceItem == 1 || isStep1ChoiceItem == 3)){
                    Log.w("calculatePrice","입력항목 부족으로 계산불가" + selectedElementData.itemSP + " / " + isStep1ChoiceItem + " / " + selectedElementData.itemId );
                    DUtil.alertShow(this, "필수항목을 선택해주세요.");
                    return;
                }else{
                    if(isStep1ChoiceItem == 2 || isStep1ChoiceItem == 3) {
                        if (selectedElementData.repair.length() == 0) {
                            Log.w("calculatePrice","수선항목 미선택"  );
                            DUtil.alertShow(this, "수선할 항목이 선택되지 않았습니다.");
                            return;
                        }else{
                            updateElement(selectedElementData);
                        }
                    }else{
                        updateElement(selectedElementData);
                    }

                }

            }
        }
    }

    private void updateElement(final OrderElementModel _elementData){
        // 삭제 후 생성
        final int updateTagId = _elementData.tagId;
        int deleteTargetItemId;

        if(_elementData.oldItemId == _elementData.itemId){
            deleteTargetItemId = _elementData.itemId;
        }else{
            deleteTargetItemId = _elementData.oldItemId;
        }

        if(updateTagId == -1){
            return;
        }

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("mode", "D");
        params.put("orderId", orderId);
        params.put("itemId", deleteTargetItemId);
        params.put("tagId", updateTagId);
        params.put("k", 1);

        Log.e("updateElement removeItems reqParam", params.toString());

        NetworkClient.post(Constants.REMOVE_ORDER_ITEM_ELEMENT, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.REMOVE_ORDER_ITEM_ELEMENT, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.REMOVE_ORDER_ITEM_ELEMENT, result);
                    //dismissDialog();
                    copyItems(_elementData.getJSONObject(), false);

                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void selectCategoryAction(View button){

        Intent pickIntent = new Intent(this, TabletSelectItemElementCategory.class);
        pickIntent.putExtra("priceData", priceArray.toString());
        if(selectedElementData != null){
            pickIntent.putExtra("selectedElementDataItemId", selectedElementData.itemId);
        }
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, SELECT_CATEGORY);
    }

    public void selectRepairAction(View button){
        if(selectedElementData.itemSP == -1){
            DUtil.alertShow(this, "요금등급을 먼저 선택해주세요.");
        }else{
            Intent pickIntent = new Intent(this, TabletSelectItemElementRepair.class);
            if(selectedElementData != null) {
                if(selectedElementData.repair != null){
                    pickIntent.putExtra("pickedRepair", selectedElementData.repair.toString());
                }

            }
            pickIntent.putExtra("itemSP", selectedElementData.itemSP);
            pickIntent.putExtra("repairData", repairArray.toString());
            pickIntent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(pickIntent, PICK_REPAIR);
        }

    }

    public void selectColorAction(){

        Intent pickIntent = new Intent(this, TabletSelectItemElementColor.class);
        if(selectedElementData != null) {
            if(selectedElementData.itemColor != null){
                pickIntent.putExtra("pickedColor", selectedElementData.itemColor);
            }
        }
        pickIntent.putExtra("data", colorsArray.toString());
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, PICK_COLOR);

    }

    public void selectAdditionalAction(){
        if(selectedElementData.itemSP == -1){
            DUtil.alertShow(this, "요금등급을 먼저 선택해주세요.");
        }else{
            Intent pickIntent = new Intent(this, TabletSelectItemElementAdditional.class);
            if(selectedElementData != null) {
                if(selectedElementData.additional != null){
                    // 요금등급표 정보 삽입
                    pickIntent.putExtra("pickedAdditional", selectedElementData.additional.toString());
                }
            }
            pickIntent.putExtra("data", additionalArray.toString());
            pickIntent.putExtra("itemSP", selectedElementData.itemSP);
            pickIntent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(pickIntent, PICK_ADDITIONAL);
        }


    }

    public void selectPartsAction(){

        Intent pickIntent = new Intent(this, TabletSelectItemElementParts.class);
        if(selectedElementData != null) {
            if(selectedElementData.part != null){
                pickIntent.putExtra("pickedParts", selectedElementData.part.toString());
            }
        }
        pickIntent.putExtra("data", partsArray.toString());
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, PICK_PARTS);

    }

    /////

    public void modTagAction(View button){
        Intent pickIntent = new Intent(this, TabletElementTagMod.class);
        pickIntent.putExtra("initTagNo", tagNo);
        pickIntent.putExtra("initTagLength", tagLength);
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pickIntent, MOD_TAG);
    }

    public void createNewEntity(View button){

        setRightBlankPanel();

    }

    public void copyItems(JSONObject itemObject, boolean isTagUpdate){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        String isTagUpdateString = isTagUpdate?"Y":"N";
        params.put("tagUpdate", isTagUpdateString);
        params.put("items", itemObject.toString());
        params.put("k", 1);
        Log.e("copyItems params", params.toString());

        NetworkClient.post(Constants.APPEND_ORDER_ITEM_ELEMENT, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.APPEND_ORDER_ITEM_ELEMENT, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.APPEND_ORDER_ITEM_ELEMENT, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);
                    getLastTag(false);


                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void removeItems(int tagId, final int orderId, int itemid){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("mode", "D");
        params.put("orderId", orderId);
        params.put("itemId", itemid);
        params.put("tagId", tagId);
        params.put("k", 1);

        Log.e("removeItems reqParam", params.toString());

        NetworkClient.post(Constants.REMOVE_ORDER_ITEM_ELEMENT, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.REMOVE_ORDER_ITEM_ELEMENT, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.REMOVE_ORDER_ITEM_ELEMENT, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);
                    getDataByOrderId(orderId);

                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(TabletOrderElementModActivity.this);
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    private void dataInitialize(JSONArray pickupItems){

        orderModelData = new ArrayList<OrderElementModel>();
        orderModelMap = new SparseArray<OrderElementModel>();
        try {
            if(pickupItems.length() > 0){
                for(int i =0; i < pickupItems.length(); i++){
                    OrderElementModel model = new OrderElementModel();
                    model.orderId = pickupItems.getJSONObject(i).getInt("orderId");

                    model.oldItemId = pickupItems.getJSONObject(i).getInt("itemId");
                    model.itemId = pickupItems.getJSONObject(i).getInt("itemId");
                    model.priceType = pickupItems.getJSONObject(i).getInt("priceType");
                    model.tagId = pickupItems.getJSONObject(i).getInt("tagId");
                    model.itemSP = pickupItems.getJSONObject(i).getInt("itemSP");
                    model.itemTitle = pickupItems.getJSONObject(i).getString("itemTitle");
                    model.itemColor = pickupItems.getJSONObject(i).getString("itemColor");
                    model.laundry = pickupItems.getJSONObject(i).getString("laundry");
                    model.laundryPrice = pickupItems.getJSONObject(i).getInt("laundryPrice");
                    model.repairPrice = pickupItems.getJSONObject(i).getInt("repairPrice");
                    model.additionalPrice = pickupItems.getJSONObject(i).getInt("additionalPrice");
                    model.confirmPrice = pickupItems.getJSONObject(i).getInt("confirmPrice");
                    model.modFinalPrice =pickupItems.getJSONObject(i).getInt("confirmPrice");
                    model.isService = pickupItems.getJSONObject(i).getString("isService");
                    model.isReLaundry = pickupItems.getJSONObject(i).getString("isReLaundry");
                    model.storeMessage = pickupItems.getJSONObject(i).getString("storeMessage");

                    model.storePrice1 = pickupItems.getJSONObject(i).getInt("storePrice1");
                    model.storePrice2 = pickupItems.getJSONObject(i).getInt("storePrice2");
                    model.storePrice3 = pickupItems.getJSONObject(i).getInt("storePrice3");
                    model.storePrice4 = pickupItems.getJSONObject(i).getInt("storePrice4");

                    model.photo = pickupItems.getJSONObject(i).getJSONArray("photo");

                    JSONArray tempRepair = pickupItems.getJSONObject(i).getJSONArray("repair");
                    JSONArray tempPickRepair = new JSONArray();
                    for(int j = 0; j < tempRepair.length(); j++){
                        RepairElement ae = new RepairElement();
                        ae.setElementFromOrder(tempRepair.getJSONObject(j));
                        tempPickRepair.put(j,ae.getJSONObject(itemSP));
                    }
                    model.repair = tempPickRepair;

                    JSONArray tempAdditional = pickupItems.getJSONObject(i).getJSONArray("additional");
                    JSONArray tempPickAdditional = new JSONArray();
                    for(int j = 0; j < tempAdditional.length(); j++){
                        AdditionalElement ae = new AdditionalElement();
                        ae.setElementFromOrder(tempAdditional.getJSONObject(j));
                        tempPickAdditional.put(j,ae.getJSONObject(itemSP));
                    }
                    model.additional = tempPickAdditional;

                    JSONArray tempPart = pickupItems.getJSONObject(i).getJSONArray("part");
                    JSONArray tempPickPart = new JSONArray();
                    for(int j = 0; j < tempPart.length(); j++){
                        PartsElement ae = new PartsElement();
                        ae.setElement(tempPart.getJSONObject(j));
                        tempPickPart.put(j,ae.getJSONObject());
                    }
                    model.part = tempPickPart;

                    orderModelData.add(model);
                    orderModelMap.put(model.id, model);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Collections.sort(orderModelData, myComparator);

        adapter = new OrderElementAdapter(orderModelData, myAdapterListener);
        recyclerView.setAdapter(adapter);

        arrangeView();
    }

    //Comparator 를 만든다.
    private final static Comparator myComparator = new Comparator<OrderElementModel>() {

        private final Collator collator = Collator.getInstance();

        @Override
        public int compare(OrderElementModel object1, OrderElementModel object2) {
//            if (val.floatValue() > f.val.floatValue()) {
//                return 1;
//            }
//            else if (val.floatValue() <  f.val.floatValue()) {
//                return -1;
//            }
//            else {
//                return 0;
//            }

            return collator.compare(object1.tagId+"", object2.tagId+"");
        }
    };


    private void initialize(JSONArray _initData){

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST);
        recyclerView.addItemDecoration(itemDecoration);

        // this is the default; this call is actually only necessary with custom ItemAnimators
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        dataInitialize(_initData);
        getPickupInitializeData();
    }

    private void getPickupInitializeData(){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.PICKUP_INITIALIZE_DATA, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.PICKUP_INITIALIZE_DATA, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    //DUtil.Log(Constants.PICKUP_INITIALIZE_DATA, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        JSONObject retData = jsondata.getJSONObject("data");
                        repairArray = retData.getJSONArray("repair");
                        additionalArray = retData.getJSONArray("additional");
                        partsArray = retData.getJSONArray("part");
                        colorsArray = retData.getJSONArray("color");


                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getLastTag();
                            }}, 200);

                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private void getLastTag(Boolean ...isOnCreate){

        boolean tagBoolean  = true; // It the default value you want to give
        if(isOnCreate.length == 1)
        {
            tagBoolean = isOnCreate[0];  // Overrided Value
        }
        final boolean tagBool = tagBoolean;

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.GET_LASTEST_TAG_NO, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GET_LASTEST_TAG_NO, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_LASTEST_TAG_NO, result);
                    dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        JSONArray retData = jsondata.getJSONArray("data");
                        if(prevTagNo == -1){
                            tagNo = retData.getInt(0);
                        }else{
                            tagNo = prevTagNo;
                        }

                        tagLength = retData.getInt(1);

                        aq.id(R.id.btn_tag_text).getButton().setText(MZValidator.getTagFormmater(tagNo, tagLength));

                        setRightBlankPanel();
                        arrangeView();

                        if(!tagBool){
                            getDataByOrderId(orderId);
                        }else{
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    getPriceTable();
                                }}, 200);
                        }

                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private void getPriceTable(){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("mode", "G");

        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_PRICE_TABLE_V2, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_PRICE_TABLE_V2, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    //DUtil.Log(Constants.ORDER_PRICE_TABLE_V2, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        JSONArray obj = jsondata.getJSONArray("data");
                        priceArray = new JSONArray();
                        for(int i = 0 ; i < obj.length(); i++){
                            if(obj.getJSONObject(i).getJSONArray("items").length() > 0){
                                priceArray.put(obj.getJSONObject(i))    ;
                            }
                        }
                        //priceArray = jsondata.getJSONArray("data");
                        isInitialized = true;

                        arrangeView();
                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void setRightBlankPanel(){
        rightPanelMode = "C";
        isFree = false;
        isRetry = false;
        isStep1ChoiceItem = -1;
        isStep2ChoiceItem = -1;
        currentItemPriceData = null;
        aq.id(R.id.origin_price_label).text(MZValidator.toNumFormat(0));
        aq.id(R.id.add_price_label).text(MZValidator.toNumFormat(0));
        aq.id(R.id.total_price_label).text(MZValidator.toNumFormat(0));

        aq.id(R.id.btn_card_submit).getButton().setText("새로 추가하기");

        selectedElementData = new OrderElementModel();
        selectedElementData.laundry = "";
        JSONArray blankArray = new JSONArray();
        selectedElementData.repair = blankArray;
        selectedElementData.additional = blankArray;
        selectedElementData.part = blankArray;
        selectedElementData.tagId = tagNo;

        aq.id(R.id.tag_label).text(MZValidator.getTagFormmater(tagNo, tagLength));

        aq.id(R.id.itemTitleLabel).text("카테고리 > 품목명");

        RadioGroup step1Group = (RadioGroup)findViewById(R.id.radiogroup_step1);
        RadioGroup step2Group = (RadioGroup)findViewById(R.id.radiogroup_step2);
        RadioGroup step3Group = (RadioGroup)findViewById(R.id.radiogroup_step3);
        RadioGroup step5Group = (RadioGroup)findViewById(R.id.radiogroup_step5);
        step1Group.clearCheck();
        step2Group.clearCheck();
        step3Group.clearCheck();
        step5Group.clearCheck();

        aq.id(R.id.pui_detail_btn_etc_none).checked(true);
        aq.id(R.id.btn_selector_repair).getButton().setText("선택");
        aq.id(R.id.btn_selector_repair).background(R.drawable.tablet_toggle_button_type01).textColor(Color.parseColor("#576874"));
        aq.id(R.id.btn_selector_additional).getButton().setText("선택");
        aq.id(R.id.btn_selector_additional).background(R.drawable.tablet_toggle_button_type01).textColor(Color.parseColor("#576874"));
        aq.id(R.id.btn_selector_parts).getButton().setText("선택");
        aq.id(R.id.btn_selector_parts).background(R.drawable.tablet_toggle_button_type01).textColor(Color.parseColor("#576874"));

        aq.id(R.id.btn_selector_color).background(R.drawable.tablet_toggle_button_type01);
        aq.id(R.id.btn_selector_color).getButton().setText("선택");

        for(int i = 0 ; i < 2; i++){
            int resId = getResources().getIdentifier("iv_picture_0"+(i+1), "id", "me.rewhite.delivery");
            int resPlaceHolderId = getResources().getIdentifier("tablet_toggle_button_type01", "mipmap", "me.rewhite.delivery");
            ImageView iv = aq.id(resId).getImageView();
            aq.id(iv).image(resPlaceHolderId);
            int resRemoveId = getResources().getIdentifier("btn_selector_picture_0"+(i+1), "id", "me.rewhite.delivery");
            ImageButton btn = (ImageButton)findViewById(resRemoveId);
            aq.id(btn).image(R.mipmap.icon_button_camera).clicked(this, "pickPhoto").tag((i+1));
        }

        calculatePrice(false);
    }

    public void setRightPanel(OrderElementModel _data){
        aq.id(R.id.btn_card_submit).getButton().setText("수정하기");
        aq.id(R.id.tag_label).text(MZValidator.getTagFormmater(_data.tagId, tagLength));
        aq.id(R.id.itemTitleLabel).text(_data.itemTitle);

        selectedElementData = _data;
        try {
            Log.w("setRightPanel", _data.getJSONObject().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // 처리방식
        if(_data.repair.length() > 0){
            if(!"".equals(_data.laundry)){
                // 세탁수선
                aq.id(R.id.pui_detail_btn_step1_03).checked(true);
                aq.id(R.id.laundry_layout).visible();
                aq.id(R.id.repair_layout).visible();
                isStep1ChoiceItem = 3;

            }else{
                // 수선
                aq.id(R.id.pui_detail_btn_step1_02).checked(true);
                aq.id(R.id.laundry_layout).gone();
                aq.id(R.id.repair_layout).visible();
                isStep1ChoiceItem = 2;
            }
        }else{
            // 세탁
            aq.id(R.id.pui_detail_btn_step1_01).checked(true);
            aq.id(R.id.laundry_layout).visible();
            aq.id(R.id.repair_layout).gone();
            isStep1ChoiceItem = 1;
        }

        if(isStep1ChoiceItem == 1 || isStep1ChoiceItem == 3){
            // Step2
            if("드라이".equals(_data.laundry)){
                aq.id(R.id.pui_detail_btn_step2_01).checked(true);
            }else if("물세탁".equals(_data.laundry)){
                aq.id(R.id.pui_detail_btn_step2_02).checked(true);
            }else if("다림질".equals(_data.laundry)){
                aq.id(R.id.pui_detail_btn_step2_03).checked(true);
            }
        }

        // Step3
        switch(_data.itemSP){
            case 1:
                //calculatedPrice = itemData.getInt("storePrice1");
                aq.id(R.id.pui_detail_btn_step3_01).checked(true);
                break;
            case 2:
                //calculatedPrice = itemData.getInt("storePrice2");
                aq.id(R.id.pui_detail_btn_step3_02).checked(true);
                break;
            case 3:
                //calculatedPrice = itemData.getInt("storePrice3");
                aq.id(R.id.pui_detail_btn_step3_03).checked(true);
                break;
        }

        if("Y".equals(_data.isReLaundry)){
            isFree = false;
            isRetry = true;
            aq.id(R.id.pui_detail_btn_etc_retry).checked(true);
        }else if("Y".equals(_data.isService)){
            isFree = true;
            isRetry = false;
            aq.id(R.id.pui_detail_btn_etc_free).checked(true);
        }else{
            isFree = false;
            isRetry = false;
            aq.id(R.id.pui_detail_btn_etc_none).checked(true);
        }

        // Repair
        if(_data.repair.length() > 0){
            String addAppendString = "";
            try {
                for(int i =0 ; i < _data.repair.length(); i++){
                    if(i == 0){
                        addAppendString = _data.repair.getJSONObject(i).getString("itemTitle");
                    }else{
                        addAppendString = addAppendString + ", " + _data.repair.getJSONObject(i).getString("itemTitle");
                    }
                }
                if(addAppendString.length() > 10){
                    addAppendString = addAppendString.substring(0,10)+"...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            aq.id(R.id.btn_selector_repair).getButton().setText(addAppendString);
            aq.id(R.id.btn_selector_repair).background(R.drawable.tablet_toggle_button_type01_selected).textColor(Color.parseColor("#ffffff"));
            //aq.id(R.id.pui_detail_btn_additional).getButton().setText("추가 / 변경하기");
        }else{
            aq.id(R.id.btn_selector_repair).getButton().setText("선택");
            aq.id(R.id.btn_selector_repair).background(R.drawable.tablet_toggle_button_type01).textColor(Color.parseColor("#576874"));
        }

        // Additional
        if(_data.additional.length() > 0){
            String addAppendString = "";
            try {
                for(int i =0 ; i < _data.additional.length(); i++){
                    if(i == 0){
                        addAppendString = _data.additional.getJSONObject(i).getString("itemTitle");
                    }else{
                        addAppendString = addAppendString + ", " + _data.additional.getJSONObject(i).getString("itemTitle");
                    }
                }
                if(addAppendString.length() > 10){
                    addAppendString = addAppendString.substring(0,10)+"...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            aq.id(R.id.btn_selector_additional).getButton().setText(addAppendString);
            aq.id(R.id.btn_selector_additional).background(R.drawable.tablet_toggle_button_type01_selected).textColor(Color.parseColor("#ffffff"));
            //aq.id(R.id.pui_detail_btn_additional).getButton().setText("추가 / 변경하기");
        }else{
            aq.id(R.id.btn_selector_additional).getButton().setText("선택");
            aq.id(R.id.btn_selector_additional).background(R.drawable.tablet_toggle_button_type01).textColor(Color.parseColor("#576874"));
        }

        for(int i = _data.photo.length() ; i < 2; i++){
            int resId = getResources().getIdentifier("iv_picture_0"+(i+1), "id", "me.rewhite.delivery");
            int resPlaceHolderId = getResources().getIdentifier("tablet_toggle_button_type01", "mipmap", "me.rewhite.delivery");
            ImageView iv = aq.id(resId).getImageView();
            aq.id(iv).image(resPlaceHolderId);
            int resRemoveId = getResources().getIdentifier("btn_selector_picture_0"+(i+1), "id", "me.rewhite.delivery");
            ImageButton btn = (ImageButton)findViewById(resRemoveId);
            aq.id(btn).image(R.mipmap.icon_button_camera).clicked(this, "pickPhoto").tag((i+1));
        }
        // Photo
        for(int i =0; i < _data.photo.length(); i++){
            try {

                String url = "";
                if(_data.photo.getJSONObject(i).isNull("photoUrl")){
                    String fileUrl = _data.photo.getJSONObject(i).getString("fileUrl");
                    _data.photo.getJSONObject(i).put("photoUrl", fileUrl);
                    url = fileUrl;
                }else{
                    String photoUrl = _data.photo.getJSONObject(i).getString("photoUrl");
                    _data.photo.getJSONObject(i).put("fileUrl", photoUrl);
                    url = photoUrl;
                }

                int resId = getResources().getIdentifier("iv_picture_0"+(i+1), "id", "me.rewhite.delivery");
                ImageOptions op = new ImageOptions();
                op.targetWidth = 50;
                op.ratio = 60.f/100.f;

                ImageView iv = aq.id(resId).getImageView();
                aq.id(iv).image(url, op);

                int resRemoveId = getResources().getIdentifier("btn_selector_picture_0"+(i+1), "id", "me.rewhite.delivery");
                ImageButton btn = (ImageButton)findViewById(resRemoveId);
                aq.id(btn).image(R.mipmap.icon_button_trash);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // part
        if(_data.part.length() > 0){
            String partsAppendString = "";
            try {
                for(int i =0 ; i < _data.part.length(); i++){
                    if(i == 0){
                        partsAppendString = _data.part.getJSONObject(i).getString("partTitle");
                    }else{
                        partsAppendString = partsAppendString + ", " + _data.part.getJSONObject(i).getString("partTitle");
                    }
                }
                if(partsAppendString.length() > 10){
                    partsAppendString = partsAppendString.substring(0,10)+"...";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("partsPickValue", partsAppendString);
            aq.id(R.id.btn_selector_parts).getButton().setText(partsAppendString);
            aq.id(R.id.btn_selector_parts).background(R.drawable.tablet_toggle_button_type01_selected).textColor(Color.parseColor("#ffffff"));
            //aq.id(R.id.pui_detail_btn_additional).getButton().setText("추가 / 변경하기");
        }else{
            aq.id(R.id.btn_selector_parts).getButton().setText("선택");
            aq.id(R.id.btn_selector_parts).background(R.drawable.tablet_toggle_button_type01).textColor(Color.parseColor("#576874"));
        }

        // Color
        if("".equals(_data.itemColor) || _data.itemColor == null || "null".equals(_data.itemColor)){
            aq.id(R.id.btn_selector_color).background(R.drawable.tablet_toggle_button_type01).textColor(Color.parseColor("#576874"));
            aq.id(R.id.btn_selector_color).getButton().setText("선택");
        }else{
            if(_data.itemColor.contains("_")){
                aq.id(R.id.btn_selector_color).background(R.drawable.tablet_toggle_button_type01_selected).textColor(Color.parseColor("#ffffff"));
                //"img_st", "img_dot", "img_mix"
                String colorName = "";
                if("img_st".equals(_data.itemColor)){
                    colorName = "줄무늬";
                }else if("img_dot".equals(_data.itemColor)){
                    colorName = "땡땡이";
                }else if("img_mix".equals(_data.itemColor)){
                    colorName = "혼합";
                }
                aq.id(R.id.btn_selector_color).getButton().setText(colorName);
            }else{
                aq.id(R.id.btn_selector_color).backgroundColor(Color.parseColor("#FF"+_data.itemColor));
            }
        }

        calculatePrice(false);
    }

    public void popupClose(View button){
        Intent resultData = new Intent();
        resultData.putExtra("orderId", orderId);
        setResult(Activity.RESULT_OK, resultData);
        finish();
    }

    public void arrangeView(){

        try {
            int totalPrice = 0;
            JSONArray pickupItems = orderData.getJSONArray("pickupItemsV2");
            if(pickupItems.length() > 0){
                for(int i =0; i < pickupItems.length(); i++){
                    OrderElementModel model = new OrderElementModel();
                    model.confirmPrice = pickupItems.getJSONObject(i).getInt("confirmPrice");
                    totalPrice += model.confirmPrice;
                }
                aq.id(R.id.text_total_price).text(MZValidator.toNumFormat(totalPrice) + " 원");
            }
            aq.id(R.id.btn_submit).getButton().setText("총 "+pickupItems.length()+"개 품목 입력하기");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getDataByOrderId(int _orderId) {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    //DUtil.Log(Constants.ORDER_DETAIL, result);

                    JSONObject resultJson = new JSONObject(result);

                    if ("S0000".equals(resultJson.getString("resultCode"))) {
                        if (resultJson.isNull("data") || resultJson.getJSONObject("data") == null) {

                        } else {

                            JSONObject jsondata = resultJson.getJSONObject("data");
                            orderData = jsondata;

                            dataInitialize(jsondata.getJSONArray("pickupItemsV2"));

                        }

                    } else {
                        aq.id(R.id.empty).visible();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

//    @Override
//    public void onClick(View view) {
//        int idx = recyclerView.getChildPosition(view);
//        Log.e("onClick", "onClick : " + idx + " / " + view.getId());
//
//        if (view.getId() == R.id.btn_selector_remove) {
//            // remove item
//
//            Log.e("onClick", "btn_selector_remove : " + idx);
//        }else if (view.getId() == R.id.btn_selector_copy) {
//            Log.e("onClick", "btn_selector_copy : " + idx);
//        }else if (view.getId() == R.id.element_pic) {
//            Log.e("onClick", "element_pic : " + idx);
//        }else{
//            Log.e("onClick", "others : " + idx);
//
//            myToggleSelection(idx);
//
//            OrderElementModel data = adapter.getItem(idx);
//            //View innerContainer = view.findViewById(R.id.container_inner_item);
//
//        }
//    }

    private void myToggleSelection(int idx) {
        adapter.toggleSelection(idx);
    }

//    @Override
//    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
////        gestureDetector.onTouchEvent(e);
//        return false;
//    }
//
//    @Override
//    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
//
//    }
//
//    @Override
//    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
//
//    }

//    private class RecyclerViewDemoOnGestureListener extends GestureDetector.SimpleOnGestureListener {
//        @Override
//        public boolean onSingleTapConfirmed(MotionEvent e) {
//            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
//            onClick(view);
//            return super.onSingleTapConfirmed(e);
//        }
//
//        public void onLongPress(MotionEvent e) {
//            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
//
//            int idx = recyclerView.getChildPosition(view);
//            myToggleSelection(idx);
//            super.onLongPress(e);
//        }
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PICK_COLOR:
                    int colorIndex = data.getIntExtra("index", 0);
                    String colorValue = data.getStringExtra("value");
                    String colorName = data.getStringExtra("name");
                    Log.e("color", colorIndex + " / " + colorValue + " / " + colorName);
                    //colorPickValue = colorValue;

                    selectedElementData.itemColor = colorValue;
                    // Color
                    if("".equals(colorValue)){
                        aq.id(R.id.btn_selector_color).background(R.drawable.tablet_toggle_button_type01).textColor(Color.parseColor("#576874"));
                    }else{
                        if(colorValue.contains("_")){
                            aq.id(R.id.btn_selector_color).background(R.drawable.tablet_toggle_button_type01_selected).textColor(Color.parseColor("#ffffff"));
                            //"img_st", "img_dot", "img_mix"
                        }else{
                            aq.id(R.id.btn_selector_color).backgroundColor(Color.parseColor("#FF"+colorValue));
                        }
                        aq.id(R.id.btn_selector_color).getButton().setText(colorName);
                    }

                    break;
                case PICK_REPAIR:
                    try {
                        JSONArray ret = new JSONArray(data.getStringExtra("value"));
                        Log.e("REPAIR_PARTS", ret.toString());

                        selectedElementData.repair = ret;
                        if(selectedElementData.repair.length() > 0){

                            String addAppendString = "";
                            try {
                                for(int i =0 ; i < selectedElementData.repair.length(); i++){
                                    if(i == 0){
                                        addAppendString = selectedElementData.repair.getJSONObject(i).getString("itemTitle");
                                    }else{
                                        addAppendString = addAppendString + ", " + selectedElementData.repair.getJSONObject(i).getString("itemTitle");
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.e("additionalPickValue", addAppendString);

                            if(addAppendString.length() > 10){
                                aq.id(R.id.btn_selector_repair).getButton().setText(addAppendString.substring(0,10)+"...");

                            }else{
                                aq.id(R.id.btn_selector_repair).getButton().setText(addAppendString);
                            }
                            aq.id(R.id.btn_selector_repair).background(R.drawable.tablet_toggle_button_type01_selected).textColor(Color.parseColor("#ffffff"));
                            calculatePrice(true);

                        }else{
                            aq.id(R.id.btn_selector_repair).background(R.drawable.tablet_toggle_button_type01).textColor(Color.parseColor("#576874"));
                            aq.id(R.id.btn_selector_repair).getButton().setText("선택하기");
                            calculatePrice(true);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case PICK_PRICE:
                    int retp = data.getIntExtra("value",0);
                    Log.e("PICK_PRICE", retp + "");
                    selectedElementData.modFinalPrice = retp;
                    selectedElementData.confirmPrice = selectedElementData.modFinalPrice;
                    calculatePrice(false);

//                    TabletPickupDetailFragmentV2 fragment3 = (TabletPickupDetailFragmentV2)getSupportFragmentManager().findFragmentById(R.id.content_frame);
//                    if(fragment3 != null){
//                        fragment3.setPricePickValue(retp);
//                    }
                    break;
                case PICK_PARTS:
                    try {
                        JSONArray ret = new JSONArray(data.getStringExtra("value"));
                        Log.e("PICK_PARTS", ret.toString());

                        selectedElementData.part = ret;
                        if(selectedElementData.part.length() > 0){

                            String addAppendString = "";
                            try {
                                for(int i =0 ; i < selectedElementData.part.length(); i++){
                                    if(i == 0){
                                        addAppendString = selectedElementData.part.getJSONObject(i).getString("partTitle");
                                    }else{
                                        addAppendString = addAppendString + ", " + selectedElementData.part.getJSONObject(i).getString("partTitle");
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.e("additionalPickValue", addAppendString);

                            if(addAppendString.length() > 10){
                                aq.id(R.id.btn_selector_parts).getButton().setText(addAppendString.substring(0,10)+"...");

                            }else{
                                aq.id(R.id.btn_selector_parts).getButton().setText(addAppendString);
                            }

                            calculatePrice(false);
                            aq.id(R.id.btn_selector_parts).background(R.drawable.tablet_toggle_button_type01_selected).textColor(Color.parseColor("#ffffff"));

                        }else{
                            calculatePrice(false);

                            aq.id(R.id.btn_selector_parts).background(R.drawable.tablet_toggle_button_type01);
                            aq.id(R.id.btn_selector_parts).getButton().setText("선택하기");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    break;
                case PICK_ADDITIONAL:
                    try {
                        JSONArray ret = new JSONArray(data.getStringExtra("value"));
                        Log.e("ELEMENT_ADDITIONAL", ret.toString());

                        selectedElementData.additional = ret;
                        if(selectedElementData.additional.length() > 0){

                            String addAppendString = "";
                            try {
                                for(int i =0 ; i < selectedElementData.additional.length(); i++){
                                    if(i == 0){
                                        addAppendString = selectedElementData.additional.getJSONObject(i).getString("itemTitle");
                                    }else{
                                        addAppendString = addAppendString + ", " + selectedElementData.additional.getJSONObject(i).getString("itemTitle");
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.e("additionalPickValue", addAppendString);

                            if(addAppendString.length() > 10){
                                aq.id(R.id.btn_selector_additional).getButton().setText(addAppendString.substring(0,10)+"...");

                            }else{
                                aq.id(R.id.btn_selector_additional).getButton().setText(addAppendString);
                            }

                            calculatePrice(true);
                            aq.id(R.id.btn_selector_additional).background(R.drawable.tablet_toggle_button_type01_selected).textColor(Color.parseColor("#ffffff"));

                        }else{
                            calculatePrice(true);

                            aq.id(R.id.btn_selector_additional).background(R.drawable.tablet_toggle_button_type01);
                            aq.id(R.id.btn_selector_additional).getButton().setText("선택하기");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case CAMERA_CAPTURE:
//                    try {
//                        /*the user's device may not support cropping*/
//                        Log.e("CAMERA_CAPTURE", "CAMERA_CAPTURE");
//                        performCrop();
//                        //cropCapturedImage(Uri.fromFile(file));
//                    }
//                    catch(ActivityNotFoundException aNFE){
//                        //display an error message if user device doesn't support
//                        String errorMessage = "Sorry - your device doesn't support the crop action!";
//                        Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
//                        toast.show();
//                    }
                    String filePath = null;

                    File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                    File f = new File(path, TEMP_PHOTO_FILE);
                    filePath = f.getAbsolutePath();
                    Log.i("imageDATA PATH :::::", "Chosen path = " + filePath);
                    Log.i("File :::::", "getExternalStoragePublicDirectory = " + TEMP_PHOTO_FILE);
                    Log.i("imageDATA PATH :::::", "Chosen path = " + filePath);

                    BitmapFactory.Options bfo = new BitmapFactory.Options();
                    //bfo.inInputShareable = true;
                    //bfo.inDither=false;
                    //bfo.inTempStorage=new byte[16 * 1024];
                    //bfo.inPurgeable = true;
                    //bfo.inJustDecodeBounds = true;
                    // Get dimensions of image first (takes very little time)
                    //bfo.inJustDecodeBounds = true;
                    //bfo.inDither = false;
                    //bfo.inPreferredConfig = Bitmap.Config.RGB_565;
                    int size = 6;
                    Bitmap bmpAvator;

                    //OutOfMemoryError 날경우 이미지 사이즈 축소
                    while (true) {
                        try {
                            bfo.inSampleSize = size;
                            bmpAvator = BitmapFactory.decodeFile(filePath, bfo);

                            int degrees = GetExifOrientation(filePath);
                            // 회전한 이미지 취득
                            bmpAvator = GetRotatedBitmap(bmpAvator, degrees);
                            File fileCacheItem = new File(filePath);
                            OutputStream out = null;
                            try {
                                fileCacheItem.createNewFile();
                                out = new FileOutputStream(fileCacheItem);
                                bmpAvator.compress(Bitmap.CompressFormat.JPEG, 80, out);
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                try {
                                    out.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            HashMap<String, Object> reqParams = new HashMap<String, Object>();
                            //reqParams.put("filename", TEMP_PHOTO_FILE);
                            new TabletImageUploadTaskOrderMod(this, preferences).execute(reqParams);


                            break;
                        } catch (OutOfMemoryError e) {
                            e.printStackTrace();
                            Log.i("inSampleSize :::::", "resize = " + size);
                            size++;
                        }
                    }
                    break;
                case CROP_PIC:
                    //Bundle extras = data.getExtras();
                    // get the cropped bitmap
                    //Bitmap thePic = extras.getParcelable("data");
                    Log.e("CROP_PIC", "CROP_PIC");


                    break;
                case MOD_TAG:

                    tagNo = data.getIntExtra("value", 0);
                    Log.e("MOD_TAG RESULT", MZValidator.getTagFormmater(tagNo, tagLength));

//                    TabletPickupItemBlankFragmentV2 fragment6 = (TabletPickupItemBlankFragmentV2)getSupportFragmentManager().findFragmentById(R.id.content_frame);
//                    if(fragment6 != null){
//                        fragment6.setTagValue();
//                    }

                    aq.id(R.id.btn_tag_text).getButton().setText(MZValidator.getTagFormmater(tagNo, tagLength));

                    setRightBlankPanel();

                    break;
                case SELECT_CATEGORY:

                    tempStep1ItemTitle = data.getStringExtra("step1ItemTitle");
                    tempStep1CateId = data.getIntExtra("step1CateId", 0);
                    tempStep1ItemId = data.getIntExtra("step1ItemId", 0);
                    tempStep2ItemTitle = data.getStringExtra("step2ItemTitle");
                    tempStep2ItemId = data.getIntExtra("step2ItemId", 0);
                    try {
                        currentItemPriceData = new JSONObject(data.getStringExtra("priceData"));
                        selectedElementData.storePrice1 = currentItemPriceData.getInt("storePrice1");
                        selectedElementData.storePrice2 = currentItemPriceData.getInt("storePrice2");
                        selectedElementData.storePrice3 = currentItemPriceData.getInt("storePrice3");
                        selectedElementData.storePrice4 = currentItemPriceData.getInt("storePrice4");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    selectedElementData.itemTitle = tempStep2ItemTitle;
                    selectedElementData.itemId = tempStep2ItemId;

                    aq.id(R.id.itemTitleLabel).text(tempStep1ItemTitle + " > " + tempStep2ItemTitle);

                    calculatePrice(true);

                    break;

            }
        }
    }

    JSONObject currentItemPriceData;

    public void calculatePrice(boolean isCalculate){

        if(selectedElementData.itemSP == -1 || isStep1ChoiceItem == -1 || selectedElementData.itemId == -1){
            Log.w("calculatePrice","입력항목 부족으로 계산불가" + selectedElementData.itemSP + " / " + isStep1ChoiceItem + " / " + selectedElementData.itemId );
            if(currentItemPriceData != null){
                Log.w("calculatePrice", currentItemPriceData.toString());
            }
            return;
        }

        int sItemSP = selectedElementData.itemSP;
        if(isStep1ChoiceItem == 2){
            selectedElementData.laundryPrice = 0;
        }else{
            switch(sItemSP){
                case 1:
                    selectedElementData.laundryPrice = selectedElementData.storePrice1;
                    break;
                case 2:
                    selectedElementData.laundryPrice = selectedElementData.storePrice2;
                    break;
                case 3:
                    selectedElementData.laundryPrice = selectedElementData.storePrice3;
                    break;
                case 4:
                    selectedElementData.laundryPrice = selectedElementData.storePrice4;
                    break;
            }
        }

        // 추가요금
        if(selectedElementData.additional != null && selectedElementData.additional.length() > 0) {
            int addAppendPrice = 0;
            try {
                for (int i = 0; i < selectedElementData.additional.length(); i++) {
                    if (sItemSP == 1) {
                        addAppendPrice += selectedElementData.additional.getJSONObject(i).getInt("storePrice1");
                    } else if (sItemSP == 2) {
                        addAppendPrice += selectedElementData.additional.getJSONObject(i).getInt("storePrice2");
                    } else if (sItemSP == 3) {
                        addAppendPrice += selectedElementData.additional.getJSONObject(i).getInt("storePrice3");
                    } else if (sItemSP == 3) {
                        addAppendPrice += selectedElementData.additional.getJSONObject(i).getInt("storePrice4");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            selectedElementData.additionalPrice = addAppendPrice;
        }else{
            selectedElementData.additionalPrice = 0;
        }

        // 수선요금
        if(selectedElementData.repair != null && selectedElementData.repair.length() > 0) {

            int addRepairPrice = 0;
            try {
                for (int i = 0; i < selectedElementData.repair.length(); i++) {
                    if (sItemSP == 1) {
                        addRepairPrice += selectedElementData.repair.getJSONObject(i).getInt("storePrice1");
                    } else if (sItemSP == 2) {
                        addRepairPrice += selectedElementData.repair.getJSONObject(i).getInt("storePrice2");
                    } else if (sItemSP == 3) {
                        addRepairPrice += selectedElementData.repair.getJSONObject(i).getInt("storePrice3");
                    } else if (sItemSP == 4) {
                        addRepairPrice += selectedElementData.repair.getJSONObject(i).getInt("storePrice4");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            selectedElementData.repairPrice = addRepairPrice;
        }else{
            selectedElementData.repairPrice = 0;
        }


        if(isFree || isRetry){
            selectedElementData.confirmPrice = 0;
        }else{
            selectedElementData.confirmPrice = selectedElementData.laundryPrice + selectedElementData.additionalPrice + selectedElementData.repairPrice;
        }

        if(isCalculate){
            selectedElementData.modFinalPrice = -1;
        }

        if(selectedElementData.confirmPrice != selectedElementData.modFinalPrice){
            if(selectedElementData.modFinalPrice != -1){
                selectedElementData.confirmPrice = selectedElementData.modFinalPrice;
            }else{
                selectedElementData.modFinalPrice = selectedElementData.confirmPrice;
            }
        }

        aq.id(R.id.origin_price_label).text(MZValidator.toNumFormat(selectedElementData.laundryPrice));
        aq.id(R.id.add_price_label).text(MZValidator.toNumFormat(selectedElementData.additionalPrice + selectedElementData.repairPrice));
        aq.id(R.id.total_price_label).text(MZValidator.toNumFormat(selectedElementData.confirmPrice));
    }



    public void setPhotoFileName(String filename){
        TEMP_PHOTO_FILE = filename;
    }

    public String getPhotoFileName(){
        return TEMP_PHOTO_FILE;
    }

    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch(requestCode) {

            case STORAGE_REQUEST:
                if (canAccessStorage()) {
                    addIntentCall();
                }
                else {
                    Toast.makeText(this, "사진첩에 접근할수 있는 권한이 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public boolean canAccessStorage() {
        return(hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    public static boolean isIntentAvailable( Context context, String action){
        final PackageManager packageManager = context.getPackageManager();
        final Intent intent = new Intent( action);
        List<ResolveInfo> list = packageManager.queryIntentActivities( intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    public void addIntentCall(){
        // 사진 촬영
        Display display = getWindowManager().getDefaultDisplay();
        int mOutputX = display.getWidth();

        setPhotoFileName("upload_" + String.valueOf
                (System.currentTimeMillis()) + ".jpg");

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //File file = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE);
        intent.putExtra( MediaStore.EXTRA_OUTPUT, Uri.fromFile(getTempFile()) );
        //intent.putExtra(MediaStore.EXTRA_OUTPUT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString());
        // requestCode지정해서 인텐트 실행
        startActivityForResult(intent, CAMERA_CAPTURE);


    }

    /**
     * this function does the crop operation.
     */
    private void performCrop() {
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File f = new File(path, TEMP_PHOTO_FILE);
            cropIntent.setDataAndType(Uri.fromFile(f), "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            //cropIntent.putExtra("outputX", 200);
            //cropIntent.putExtra("outputY", 200);
            // retrieve data on return
            cropIntent.putExtra("scale", true);
            //cropIntent.putExtra("return-data", true);
            cropIntent.putExtra( MediaStore.EXTRA_OUTPUT, getTempUri());
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, CROP_PIC);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            Toast toast = Toast
                    .makeText(this, "This device doesn't support the crop action!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private int GetExifOrientation(String filepath) {
        int degree = 0;
        ExifInterface exif = null;

        try {
            exif = new ExifInterface(filepath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (exif != null) {
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            if (orientation != -1) {
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        degree = 90;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        degree = 180;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        degree = 270;
                        break;
                }
            }
        }

        return degree;
    }

    private Bitmap GetRotatedBitmap(Bitmap bitmap, int degrees) {
        if (degrees != 0 && bitmap != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

            try {
                Bitmap b2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);

                if (bitmap != b2) {
                    bitmap.recycle();
                    bitmap = b2;
                }
            } catch (OutOfMemoryError e) {
                // 메모리 부족에러시, 원본을 반환
                e.printStackTrace();
            }
        }

        return bitmap;
    }

    public void uploadCompleted(String response) {

        try {
            JSONObject json = new JSONObject(response);
            Log.i("uploadCompleted", response.toString());

            if ("S0000".equals(json.getString("resultCode"))) {
                DUtil.alertShow(this, "이미지 업로드가 완료되었습니다");
                String imgurl = json.getString("data");

                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                File f = new File(path, TEMP_PHOTO_FILE);
                if(f.exists()){
                    f.delete();
                }
                setImagePicsValue(imgurl);
//                //sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory()))); // 갤러리를 갱신하기 위해..
//
//                TabletPickupDetailFragmentV2 fragment = (TabletPickupDetailFragmentV2)getSupportFragmentManager().findFragmentById(R.id.content_frame);
//                if(fragment != null){
//                    fragment.setImagePicsValue(imgurl);
//                }
//                //
            } else {
                DUtil.alertShow(this, "이미지 업로드가 실패했습니다.");
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void setImagePicsValue(String _addImageURL){
        try {
            JSONObject obj = new JSONObject();
            obj.put("photoUrl", _addImageURL);

            JSONArray tempPhoto = selectedElementData.photo;
            if(tempPhoto == null){
                tempPhoto = new JSONArray();
            }
            tempPhoto.put(obj);
            selectedElementData.photo = tempPhoto;

            //aq.id(R.id.pui_pics_options_area).visible();

            for(int i =0; i < selectedElementData.photo.length(); i++){
                String url = selectedElementData.photo.getJSONObject(i).getString("photoUrl");
                int resId = getResources().getIdentifier("iv_picture_0"+(i+1), "id", "me.rewhite.delivery");
                ImageOptions op = new ImageOptions();
                op.targetWidth = 50;
                op.ratio = 60.f/100.f;

                ImageView iv = aq.id(resId).getImageView();
                aq.id(iv).image(url, op);

                int resRemoveId = getResources().getIdentifier("btn_selector_picture_0"+(i+1), "id", "me.rewhite.delivery");
                ImageButton btn = (ImageButton)findViewById(resRemoveId);
                aq.id(btn).image(R.mipmap.icon_button_trash);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("imagePicks added", selectedElementData.photo.toString());
    }

    void createExternalStoragePublicPicture() {

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        //File file = new File(getCacheDir(), TEMP_PHOTO_FILE);

        try {
            // Make sure the Pictures directory exists.
            if(!path.exists()){
                path.mkdirs();
            }


            InputStream is = getAssets().open("kakao_default_profile_image.png");// getResources().openRawResource(R.mipmap.kakao_default_profile_image);
            OutputStream os = new FileOutputStream(file);
            byte[] data = new byte[is.available()];
            is.read(data);
            os.write(data);
            is.close();
            os.close();

            MediaScannerConnection.scanFile(this, new String[]{file.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });
        } catch (IOException e) {
            // Unable to create file, likely because external storage is
            // not currently mounted.
            e.printStackTrace();
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }

    void deleteExternalStoragePublicPicture() {
        //File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        //File file = new File(path, TEMP_PHOTO_FILE);
        //Environment.getDownloadCacheDirectory();
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        if(file.exists()){
            file.delete();
        }
    }

    boolean hasExternalStoragePublicPicture() {
        //File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        //File file = new File(path, TEMP_PHOTO_FILE);
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        return file.exists();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        bundle.putString("fileName", fileName);
    }

    String fileName;
    /**
     * 임시 저장 파일의 경로를 반환
     */
    private Uri getTempUri() {
        //Uri mImageCaptureUri = Uri.fromFile(new File(this.getExternalCacheDir(), TEMP_PHOTO_FILE));
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        fileName = file.getAbsolutePath();
        return Uri.fromFile(file);
    }

    /**
     * 외장메모리에 임시 이미지 파일을 생성하여 그 파일의 경로를 반환
     */
    private File getTempFile() {
        if (hasExternalStoragePublicPicture()) {
            Log.i("hasExternal", "HAVED");
            deleteExternalStoragePublicPicture();
            Log.i("deleteExternal", "DELETED");
        }
        Log.i("createExternal", "getTempFile()");
        createExternalStoragePublicPicture();
/*
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File f = new File(path, TEMP_PHOTO_FILE);
*/
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File f = new File(path, TEMP_PHOTO_FILE);

        try {
            f.createNewFile(); // 외장메모리에 temp.jpg 파일 생성
        } catch (IOException e) {
            Log.e("cklee", "fileCreation fail" );
            e.printStackTrace();
        }

        return f;
    }

}
