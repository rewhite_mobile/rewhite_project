package me.rewhite.delivery.tablet2.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.OrderListItem;
import me.rewhite.delivery.adapter.TabletMainListViewAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.util.SharedPreferencesUtility;

/**
 * 주문관리
 */
public class TabletV2Content_Mileage extends Fragment {

    private ViewGroup mView = null;
    ListView orderListView;
    public final Context mCtx = getActivity();
    public SharedPreferences preferences;

    private Fragment mContent;
    private AQuery aq;

    private TabletMainListViewAdapter orderAdapter;
    private ArrayList<OrderListItem> orderData;

    private long splashDelay = 100;
    ProgressDialog mProgressDialog;

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    public TabletV2Content_Mileage() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.tablet_main_v2_content_mileage, container, false);
        preferences = getActivity().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);

        aq = new AQuery(mView);

        showDialog();

        /*
        TODO 현장접수 only 대응코드
         */
        if("1".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_TYPE))){
            // 모바일 주문만
            initialize();

        }else if("2".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_TYPE))){
            // 현장접수만
            localOnlyInitialize();

        }else if("3".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_TYPE))){
            // 모바일+현장 모두
            initialize();
        }

        return mView;
    }

    private void localOnlyInitialize(){
        aq.id(R.id.mobile_area).gone();
        aq.id(R.id.local_push_area).visible();

        dismissDialog();
    }

    private synchronized void initialize() {
        //showDialog();
        aq.id(R.id.mobile_area).visible();
        aq.id(R.id.local_push_area).gone();

    }




}
