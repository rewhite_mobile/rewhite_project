package me.rewhite.delivery.tablet2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;

public class TabletSelectItemElementColor extends BaseActivity implements AdapterView.OnItemClickListener {

    AQuery aq;
    String pickedColor = "";
    JSONArray colorsArray;
    /*
    String colors[] = {"ffffff", "c0c0c0", "25272f", "000000", "cf0000", "ff6b2a", "ffc90c", "fff3cc", "d1ad7e", "8f6d2c", "523922", "4f280d", "a8edff", "003679", "091438",
            "48285c", "004f52", "06753d", "74c04b", "2b340c", "ffbcea", "img_st", "img_dot", "img_mix"};*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickup_item_color_picker);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                if (intent.getStringExtra("data") != null) {
                    colorsArray = new JSONArray(intent.getStringExtra("data"));
                    Log.e("color", intent.getStringExtra("data"));
                }
                if (intent.getStringExtra("pickedColor") != null) {
                    pickedColor = intent.getStringExtra("pickedColor");
                    Log.e("pickedColor", pickedColor + "");
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // 커스텀 아답타 생성
        ColorPickAdapter adapter = new ColorPickAdapter (
                getApplicationContext(),
                R.layout.pui_color_layout_row,       // GridView 항목의 레이아웃 row.xml
                colorsArray);    // 데이터

        GridView gv = (GridView)findViewById(R.id.gridView);
        gv.setAdapter(adapter);  // 커스텀 아답타를 GridView 에 적용
        gv.setOnItemClickListener(this);

        aq = new AQuery(this);
        aq.id(R.id.btn_close).clicked(this, "closeAction");
    }

    public void closeAction(View button){
        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }

    public void submitAction(int position){
        Intent resultData = new Intent();
        resultData.putExtra("index", position);
        try {
            resultData.putExtra("value", colorsArray.getJSONObject(position).getString("code"));
            resultData.putExtra("name", colorsArray.getJSONObject(position).getString("title"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setResult(Activity.RESULT_OK, resultData);
        finish();
    }

    int old_position=0;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        if(old_position<parent.getChildCount())
            parent.getChildAt(old_position).findViewById(R.id.coverView).setVisibility(View.GONE);

        view.findViewById(R.id.coverView).setVisibility(View.VISIBLE);
        Log.e("gridView clicked", position + "");
        old_position=position;

        submitAction(position);
    }

    class ColorPickAdapter extends BaseAdapter {
        Context context;
        int layout;
        JSONArray colors;
        LayoutInflater inf;

        public ColorPickAdapter(Context context, int layout, JSONArray colors) {
            this.context = context;
            this.layout = layout;
            this.colors = colors;
            inf = (LayoutInflater) context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return colors.length();
        }

        @Override
        public Object getItem(int position) {
            String obj = "";
            try {
                obj = colors.getJSONObject(position).getString("code");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return obj;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView==null)
                convertView = inf.inflate(layout, null);
            ImageView iv = (ImageView)convertView.findViewById(R.id.imageView);
            try {
                int resId = getResources().getIdentifier("pui_item_color_"+colors.getJSONObject(position).getString("code"), "mipmap", "me.rewhite.delivery" );
                iv.setImageResource(resId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return convertView;
        }
    }
}
