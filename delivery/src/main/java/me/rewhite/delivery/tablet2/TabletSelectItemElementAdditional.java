package me.rewhite.delivery.tablet2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.tablet2.converter.AdditionalElement;
import me.rewhite.delivery.util.MZValidator;

public class TabletSelectItemElementAdditional extends TabletBaseActivity implements AdapterView.OnItemClickListener {

    AQuery aq;
    JSONArray pickedAdditional;
    JSONArray additionalArray;
    int itemSP;
    /*
    String colors[] = {"ffffff", "c0c0c0", "25272f", "000000", "cf0000", "ff6b2a", "ffc90c", "fff3cc", "d1ad7e", "8f6d2c", "523922", "4f280d", "a8edff", "003679", "091438",
            "48285c", "004f52", "06753d", "74c04b", "2b340c", "ffbcea", "img_st", "img_dot", "img_mix"};*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablet_select_item_element_repair);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                if (intent.getStringExtra("data") != null) {
                    itemSP = intent.getIntExtra("itemSP", -1);
                    additionalArray = new JSONArray(intent.getStringExtra("data"));
                }
                if (intent.getStringExtra("pickedAdditional") != null) {
//                    JSONArray tempAdditionalArray = new JSONArray(intent.getStringExtra("pickedAdditional"));
//                    for(int i = 0; i < tempAdditionalArray.length(); i++ ){
//                        AdditionalElement ae = new AdditionalElement();
//                        ae.setElement(tempAdditionalArray.getJSONObject(i));
//                        tempAdditionalArray.put(ae.getJSONObject(itemSP));
//                    }

                    pickedAdditional = new JSONArray(intent.getStringExtra("pickedAdditional"));
                }else{
                    pickedAdditional = new JSONArray();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // 커스텀 아답타 생성
        AdditionalPickAdapter adapter = new AdditionalPickAdapter (
                getApplicationContext(),
                R.layout.pui_additional_layout_row,       // GridView 항목의 레이아웃 row.xml
                additionalArray);    // 데이터

        GridView gv = (GridView)findViewById(R.id.gridView);
        gv.setAdapter(adapter);  // 커스텀 아답타를 GridView 에 적용
        gv.setOnItemClickListener(this);

        aq = new AQuery(this);
        aq.id(R.id.btn_popup_close).clicked(this, "closeAction");
        aq.id(R.id.btn_save).clicked(this, "saveAction");
        aq.id(R.id.title_image).image(R.mipmap.title_additional_picker);
    }

    public void closeAction(View button){
        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }

    public void saveAction(View button){
        Intent resultData = new Intent();
        resultData.putExtra("value", pickedAdditional.toString());
        setResult(Activity.RESULT_OK, resultData);
        finish();
    }

    int old_position=0;

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        view.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_pressed);
        view.findViewById(R.id.price_bg).setBackgroundResource(R.drawable.pui_button_drawable_pressed_black);
        view.findViewById(R.id.spare_area).setBackgroundColor(0xff25272f);
        ((TextView)view.findViewById(R.id.title_price)).setTextColor(0xFFFFFFFF);
        ((TextView)view.findViewById(R.id.title_content)).setTextColor(0xFF000000);
        Log.e("gridView clicked", position + "");

        try {
            boolean isExisted = false;
            int itemId = additionalArray.getJSONObject(position).getInt("itemId");
            JSONArray temp = new JSONArray();
            if(pickedAdditional.length() > 0){
                for(int j = 0; j < pickedAdditional.length(); j++){
                    if(itemId == pickedAdditional.getJSONObject(j).getInt("itemId")){
                        //pickedParts.remove(j);
                        view.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_normal);
                        view.findViewById(R.id.price_bg).setBackgroundResource(R.drawable.pui_button_drawable_normal_gray);
                        view.findViewById(R.id.spare_area).setBackgroundColor(0xffe5e5e5);
                        ((TextView)view.findViewById(R.id.title_price)).setTextColor(0xFF666666);
                        ((TextView)view.findViewById(R.id.title_content)).setTextColor(0xFF666666);
                        isExisted = true;
                    }else{
                        AdditionalElement ae = new AdditionalElement();
                        ae.setElement(pickedAdditional.getJSONObject(j));
                        temp.put(ae.getJSONObject(itemSP));
                    }
                }
            }

            if(!isExisted){
                AdditionalElement ae = new AdditionalElement();
                ae.setElementFromInput(additionalArray.getJSONObject(position), itemSP);
                temp.put(ae.getJSONObject(itemSP));
            }

            pickedAdditional = temp;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        old_position=position;
        Log.i("AdditionalClicked", pickedAdditional.toString());

    }

    class AdditionalPickAdapter extends BaseAdapter {
        Context context;
        int layout;
        JSONArray addItems;
        LayoutInflater inf;

        public AdditionalPickAdapter(Context context, int layout, JSONArray addItems) {
            this.context = context;
            this.layout = layout;
            this.addItems = addItems;
            inf = (LayoutInflater) context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return addItems.length();
        }

        @Override
        public JSONObject getItem(int position) {
            JSONObject obj = new JSONObject();
            try {
                obj = addItems.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return obj;
        }

        @Override
        public long getItemId(int position) {
            try {
                return addItems.getJSONObject(position).getInt("itemId");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView==null)
                convertView = inf.inflate(layout, null);

            try {
                int itemId = additionalArray.getJSONObject(position).getInt("itemId");
                boolean isPicked = false;
                for(int j = 0; j < pickedAdditional.length(); j++){
                    int pickedItemId = pickedAdditional.getJSONObject(j).getInt("itemId");
                    if(itemId == pickedItemId){
                        convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_pressed);
                        convertView.findViewById(R.id.price_bg).setBackgroundResource(R.drawable.pui_button_drawable_pressed_black);
                        convertView.findViewById(R.id.spare_area).setBackgroundColor(0xff25272f);
                        ((TextView)convertView.findViewById(R.id.title_price)).setTextColor(0xFFFFFFFF);
                        ((TextView)convertView.findViewById(R.id.title_content)).setTextColor(0xFF000000);
                        Log.i("picked initialize", pickedItemId + "");
                        isPicked = true;
                    }
                }

                if(!isPicked){
                    convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_normal);
                    convertView.findViewById(R.id.price_bg).setBackgroundResource(R.drawable.pui_button_drawable_normal_gray);
                    convertView.findViewById(R.id.spare_area).setBackgroundColor(0xffe5e5e5);
                    ((TextView)convertView.findViewById(R.id.title_price)).setTextColor(0xFF666666);
                    ((TextView)convertView.findViewById(R.id.title_content)).setTextColor(0xFF666666);
                }

                TextView iv = (TextView)convertView.findViewById(R.id.title_content);
                TextView ivp = (TextView)convertView.findViewById(R.id.title_price);
                iv.setText(addItems.getJSONObject(position).getString("itemTitle"));

//                int itemSP = addItems.getJSONObject(position).getInt("itemSP");
                switch(itemSP){
                    case 1:
                        ivp.setText(MZValidator.toNumFormat(addItems.getJSONObject(position).getInt("storePrice11")));
                        break;
                    case 2:
                        ivp.setText(MZValidator.toNumFormat(addItems.getJSONObject(position).getInt("storePrice12")));
                        break;
                    case 3:
                        ivp.setText(MZValidator.toNumFormat(addItems.getJSONObject(position).getInt("storePrice13")));
                        break;
                    case 4:
                        ivp.setText(MZValidator.toNumFormat(addItems.getJSONObject(position).getInt("storePrice14")));
                        break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return convertView;
        }
    }
}
