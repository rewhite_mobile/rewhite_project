package me.rewhite.delivery.tablet2.adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by marines on 2017. 10. 10..
 */

public class OrderElementModel implements Cloneable{

    /*
    "pickupItemsV2":[
    {"orderId":39127,"itemId":1,"priceType":101,"tagId":1102,"itemSP":1,"itemTitle":"정장 상의","itemColor":"","laundry":"드라이",
    "laundryPrice":3000,"repairPrice":0,"additionalPrice":0,"confirmPrice":3000,"isService":"N","isReLaundry":"N",
    "storeMessage":"","repair":[],"additional":[],"part":[],"photo":[]}
    ],
     */

    private static int nextId = 0;

    public int orderId = -1;

    public int oldItemId = -1;
    public int itemId = -1;
    public int priceType;
    public int tagId = -1;
    public int itemSP = -1;
    public String itemTitle = "";
    public String itemColor = "";
    public String laundry = "";
    public int laundryPrice = 0;
    public int repairPrice = 0;
    public int additionalPrice = 0;
    public int confirmPrice = 0;
    public int modFinalPrice = -1;
    public String isService = "N";
    public String isReLaundry = "N";
    public String storeMessage = "";

    public int storePrice1 = 0;
    public int storePrice2 = 0;
    public int storePrice3 = 0;
    public int storePrice4 = 0;

    public JSONArray repair = new JSONArray();
    public JSONArray additional  = new JSONArray();
    public JSONArray part = new JSONArray();
    public JSONArray photo  = new JSONArray();

    public int id = ++nextId;

    public OrderElementModel(){

    }

    public OrderElementModel(final OrderElementModel _rModel){
        OrderElementModel _model = null;
        try {
            _model = (OrderElementModel) _rModel.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        this.orderId = _model.orderId;
        this.oldItemId = _model.oldItemId;
        this.itemId = _model.itemId;
        this.priceType = _model.priceType;
        this.tagId = _model.tagId;
        this.itemSP = _model.itemSP;
        this.itemTitle = _model.itemTitle;
        this.itemColor = _model.itemColor;
        this.laundry = _model.laundry;
        this.laundryPrice = _model.laundryPrice;
        this.repairPrice = _model.repairPrice;
        this.additionalPrice = _model.additionalPrice;
        this.confirmPrice = _model.confirmPrice;
        this.modFinalPrice = _model.modFinalPrice;
        this.isService = _model.isService;
        this.isReLaundry = _model.isReLaundry;
        this.storeMessage = _model.storeMessage;
        this.storePrice1 = _model.storePrice1;
        this.storePrice2 = _model.storePrice2;
        this.storePrice3 = _model.storePrice3;
        this.storePrice4 = _model.storePrice4;
        this.repair = _model.repair;
        this.additional = _model.additional;
        this.part = _model.part;
        this.photo = _model.photo;
    }
    /**
     * 이 객체의 복제품을 리턴한다.
     */
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
//
//    public OrderElementModel clone()
//    {
//        //내 객체 생성
//        OrderElementModel objReturn;
//
//        try
//        {
//            objReturn = super.clone();
//            ((OrderElementModel)objReturn).objA = this.objA.clone();
//            return objReturn;
//        }
//        catch (CloneNotSupportedException e)
//        {
//            e.printStackTrace();
//            return null;
//        }
//    }//end clone

    public JSONObject getJSONObject() throws JSONException {

        JSONObject json = new JSONObject();
        //json.put("step1index", jsonData.getJSONObject(i).getInt("step1index"));
        //json.put("step2index", jsonData.getJSONObject(i).getInt("step2index"));
        json.put("itemId", itemId);
        json.put("tagId", tagId);

        json.put("itemTitle", itemTitle);
        json.put("sp", itemSP);
        json.put("laundryPrice", laundryPrice);
        json.put("repairPrice", repairPrice);
        json.put("additionalPrice", additionalPrice);
        json.put("isService", isService);
        json.put("isReLaundry", isReLaundry);
        json.put("storeMessage", storeMessage);

        if(modFinalPrice != confirmPrice){
            json.put("confirmPrice", modFinalPrice);
        }else{
            json.put("confirmPrice", confirmPrice);
        }

        json.put("qty", 1);
        json.put("itemColor", itemColor);
        json.put("laundry", laundry);
        json.put("repair", repair);
        json.put("additional", additional);
        json.put("part", part);
        json.put("photo", photo);

        return json;
    }

    public JSONObject getCopyJSONObject() throws JSONException {

        JSONObject json = new JSONObject();
        //json.put("step1index", jsonData.getJSONObject(i).getInt("step1index"));
        //json.put("step2index", jsonData.getJSONObject(i).getInt("step2index"));
        json.put("itemId", itemId);
        json.put("tagId", tagId);

        json.put("itemTitle", itemTitle);
        json.put("sp", itemSP);
        json.put("laundryPrice", laundryPrice);
        json.put("repairPrice", 0);
        json.put("additionalPrice", 0);
        json.put("isService", isService);
        json.put("isReLaundry", isReLaundry);
        json.put("storeMessage", storeMessage);

        json.put("confirmPrice", confirmPrice);
        json.put("qty", 1);
        json.put("itemColor", itemColor);
        json.put("laundry", laundry);
        json.put("repair", new JSONArray());
        json.put("additional", new JSONArray());
        json.put("part", new JSONArray());
        json.put("photo", new JSONArray());

        return json;
    }


}
