package me.rewhite.delivery.tablet2.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.OrderListItem;
import me.rewhite.delivery.adapter.TabletMainListViewAdapterV2;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.session.Session;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TabletV2Content_Home.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class TabletV2Content_Home extends Fragment {

    private ViewGroup mView = null;
    ListView orderListView;
    public final Context mCtx = getActivity();
    public SharedPreferences preferences;

    private Fragment mContent;
    private AQuery aq;

    private TabletMainListViewAdapterV2 orderAdapter;
    private ArrayList<OrderListItem> orderData;

    private long splashDelay = 100;
    ProgressDialog mProgressDialog;

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    public TabletV2Content_Home() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.tablet_main_v2_content_home, container, false);
        preferences = getActivity().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);

        aq = new AQuery(mView);
        orderListView = (ListView)mView.findViewById(R.id.listView);

        showDialog();

        /*
        TODO 현장접수 only 대응코드
         */
        if("1".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_TYPE))){
            // 모바일 주문만
            initialize();

        }else if("2".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_TYPE))){
            // 현장접수만
            localOnlyInitialize();

        }else if("3".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_TYPE))){
            // 모바일+현장 모두
            initialize();
        }

        return mView;
    }

    private void localOnlyInitialize(){
        aq.id(R.id.mobile_area).gone();
        aq.id(R.id.local_push_area).visible();

        dismissDialog();
    }

    private synchronized void initialize() {
        //showDialog();
        aq.id(R.id.mobile_area).visible();
        aq.id(R.id.local_push_area).gone();

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("mode", "Y");
        params.put("page", 1);
        params.put("block", 200);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_LIST, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {

                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_LIST, result);

                    dismissDialog();

                    JSONObject jsondata = new JSONObject(result);
                    if ("S0000".equals(jsondata.getString("resultCode"))) {

                        if (jsondata.isNull("data")) {
                            // 주문내역이 없음
                            orderData = new ArrayList<>();
                            orderAdapter = new TabletMainListViewAdapterV2(mCtx, R.layout.tablet_main_v2_order_item, orderData);
                            orderAdapter.notifyDataSetChanged();
                            orderListView.setAdapter(orderAdapter);

                            aq.id(R.id.empty).visible();

                        } else {

                            orderListView = aq.id(R.id.listView).getListView();// (ListView)findViewById(R.id.listView);
                            orderData = new ArrayList<>();

                            JSONArray orderInfo = jsondata.getJSONArray("data");

                            if (orderInfo.length() > 0) {

                                for (int i = 0; i < orderInfo.length(); i++) {

                                    String orderId = orderInfo.getJSONObject(i).getString("orderId");
                                    String orderStatus = orderInfo.getJSONObject(i).getString("orderStatus");
                                    String addressSeq = orderInfo.getJSONObject(i).getString("addressSeq");
                                    String storeId = orderInfo.getJSONObject(i).getString("storeId");
                                    String deliveryPrice = orderInfo.getJSONObject(i).getString("deliveryPrice");
                                    String pickupRequestTimeApp = orderInfo.getJSONObject(i).getString("pickupRequestTimeApp");
                                    String deliveryRequestTimeApp = orderInfo.getJSONObject(i).getString("deliveryRequestTimeApp");
                                    String registerDateApp = orderInfo.getJSONObject(i).getString("registerDateApp");
                                    String orderRequest = orderInfo.getJSONObject(i).getString("orderRequest");
                                    String orderPickupItemMessage = orderInfo.getJSONObject(i).getString("orderPickupItemMessage");
                                    String statusTimeApp = orderInfo.getJSONObject(i).getString("statusTimeApp");
                                    String userName = orderInfo.getJSONObject(i).getString("userName");
                                    int quantity = orderInfo.getJSONObject(i).getInt("pickupQuantity");
                                    String isPayment = orderInfo.getJSONObject(i).getString("isPayment");
                                    String isReceivable = orderInfo.getJSONObject(i).getString("isReceivable");
                                    String isAdditionalPayment = orderInfo.getJSONObject(i).getString("isAdditionalPayment");

                                    String payments;
                                    if(orderInfo.getJSONObject(i).isNull("payments")){
                                        payments = null;
                                    }else{
                                        payments = orderInfo.getJSONObject(i).getString("payments");
                                    }

                                    OrderListItem aItem = new OrderListItem(orderId, orderStatus, addressSeq, storeId, userName, deliveryPrice, pickupRequestTimeApp,
                                            deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage, orderInfo.getJSONObject(i).toString(), quantity, isPayment, payments, isReceivable, isAdditionalPayment);
                                    orderData.add(aItem);

                                }

                                if (orderData.size() > 0) {
                                    aq.id(R.id.empty).gone();
                                }

                                aq.id(R.id.text_order_count).text("("+orderData.size()+")");

                                orderAdapter = new TabletMainListViewAdapterV2(getActivity(), R.layout.tablet_main_v2_order_item, orderData);
                                orderAdapter.notifyDataSetChanged();

                                orderListView.setAdapter(orderAdapter);


                            } else {

                            }

                        }

                        //dismissDialog();
                    } else if ("S9002".equals(jsondata.getString("resultCode"))) {
                        Session.getCurrentSession().close();
                        //dismissDialog();
                    } else {
                        //dismissDialog();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    //dismissDialog();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    //dismissDialog();
                }
            }
        });
    }




}
