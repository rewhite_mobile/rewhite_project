package me.rewhite.delivery.tablet2.mod;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;

public class TabletElementTagMod extends TabletBaseActivity {

    private String[] numbers = {"1","2","3","4","5","6","7","8","9","00","0","-"};
    AQuery aq;
    int initTagNo = 0;
    int initTagLength = 0;
    String appendTag = "";
    int modTag = 0;
    GridView gv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablet_activity_pickup_item_tag_mod);

        aq = new AQuery(this);

        aq.id(R.id.btn_del).clicked(this, "numberRemoveAction");
        aq.id(R.id.btn_del_1).clicked(this, "numberDelete1Action");
        aq.id(R.id.btn_popup_close).clicked(this, "closeAction");
        aq.id(R.id.btn_complete).clicked(this, "saveAction");

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("data") != null) {
            }
            initTagNo = intent.getIntExtra("initTagNo",0);
            initTagLength = intent.getIntExtra("initTagLength",0);

            Log.i("already picked", initTagNo + "");
            aq.id(R.id.tag_label).text(MZValidator.getTagFormmater(initTagNo, initTagLength));
        }

        modTag = 0;
        appendTag = "";
        aq.id(R.id.after_tag_label).text("0");
        aq.id(R.id.num_text).text("0");

        aq.id(R.id.btn_0).clicked(this, "numpadClickAction").tag(0);
        aq.id(R.id.btn_1).clicked(this, "numpadClickAction").tag(1);
        aq.id(R.id.btn_2).clicked(this, "numpadClickAction").tag(2);
        aq.id(R.id.btn_3).clicked(this, "numpadClickAction").tag(3);
        aq.id(R.id.btn_4).clicked(this, "numpadClickAction").tag(4);
        aq.id(R.id.btn_5).clicked(this, "numpadClickAction").tag(5);
        aq.id(R.id.btn_6).clicked(this, "numpadClickAction").tag(6);
        aq.id(R.id.btn_7).clicked(this, "numpadClickAction").tag(7);
        aq.id(R.id.btn_8).clicked(this, "numpadClickAction").tag(8);
        aq.id(R.id.btn_9).clicked(this, "numpadClickAction").tag(9);
    }

    public void numpadClickAction(View button){
        int tag = (int)button.getTag();

        if(appendTag.length() < 7){
            appendTag = appendTag + tag;
            modTag = Integer.parseInt(appendTag);

            aq.id(R.id.after_tag_label).text(MZValidator.getTagFormmater(modTag, initTagLength));
            aq.id(R.id.num_text).text(MZValidator.getTagFormmater(modTag, initTagLength));
        }

    }

    public void closeAction(View button){
        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }


    public void saveAction(View button){
        if(modTag > 0){
            Intent resultData = new Intent();
            resultData.putExtra("value", modTag);
            setResult(Activity.RESULT_OK, resultData);
            finish();
        }else{
            DUtil.alertShow(this, "택번호를 다시 확인해주세요.");
        }

    }

    public void numberDelete1Action(View button){
        if(appendTag.length() > 0){
            appendTag = appendTag.substring(0, appendTag.length()-1);
            if(appendTag.length() > 0){
                modTag = Integer.parseInt(appendTag);
            }else{
                modTag = 0;
            }

            aq.id(R.id.after_tag_label).text(MZValidator.getTagFormmater(modTag, initTagLength));
            aq.id(R.id.num_text).text(MZValidator.getTagFormmater(modTag, initTagLength));
        }else{
            modTag = 0;
            appendTag = "";
            aq.id(R.id.after_tag_label).text("0");
            aq.id(R.id.num_text).text("0");
        }
    }

    public void numberRemoveAction(View button){
        modTag = 0;
        appendTag = "";
        aq.id(R.id.after_tag_label).text("0");
        aq.id(R.id.num_text).text("0");
    }

}
