package me.rewhite.delivery.tablet2.main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.logic.Constants;

/**
 * 주문관리
 */
public class TabletV2Content_Order extends Fragment {

    private ViewGroup mView = null;
    public final Context mCtx = getActivity();
    public SharedPreferences preferences;

    private Fragment mContent;
    private AQuery aq;

    private long splashDelay = 100;
    ProgressDialog mProgressDialog;

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    public TabletV2Content_Order() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.tablet_main_v2_content_order, container, false);
        preferences = getActivity().getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);

        aq = new AQuery(mView);

        showDialog();
        initialize();

        return mView;
    }

    private synchronized void initialize() {

        dismissDialog();
    }




}
