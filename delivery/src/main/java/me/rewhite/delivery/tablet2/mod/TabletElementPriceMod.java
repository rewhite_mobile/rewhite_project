package me.rewhite.delivery.tablet2.mod;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.activity.BaseActivity;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;

public class TabletElementPriceMod extends BaseActivity {

    private String[] numbers = {"1","2","3","4","5","6","7","8","9","00","0","-"};
    AQuery aq;
    int pickedPrice = 0;
    String appendPrice = "";
    int modPrice = 0;
    GridView gv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tablet_activity_pickup_item_price_mod);

        aq = new AQuery(this);
        aq.id(R.id.btn_del).clicked(this, "numberRemoveAction");
        aq.id(R.id.btn_del_1).clicked(this, "numberDelete1Action");
        aq.id(R.id.btn_popup_close).clicked(this, "closeAction");
        aq.id(R.id.btn_complete).clicked(this, "saveAction");

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("data") != null) {
            }
            pickedPrice = intent.getIntExtra("pickedPrice",0);
            Log.i("already picked", pickedPrice + "");
            aq.id(R.id.price_label).text(MZValidator.toNumFormat(pickedPrice));
        }

        modPrice = 0;
        appendPrice = "";
        aq.id(R.id.after_price_label).text("0");
        aq.id(R.id.num_text).text("0");

        aq.id(R.id.btn_0).clicked(this, "numpadClickAction").tag(0);
        aq.id(R.id.btn_1).clicked(this, "numpadClickAction").tag(1);
        aq.id(R.id.btn_2).clicked(this, "numpadClickAction").tag(2);
        aq.id(R.id.btn_3).clicked(this, "numpadClickAction").tag(3);
        aq.id(R.id.btn_4).clicked(this, "numpadClickAction").tag(4);
        aq.id(R.id.btn_5).clicked(this, "numpadClickAction").tag(5);
        aq.id(R.id.btn_6).clicked(this, "numpadClickAction").tag(6);
        aq.id(R.id.btn_7).clicked(this, "numpadClickAction").tag(7);
        aq.id(R.id.btn_8).clicked(this, "numpadClickAction").tag(8);
        aq.id(R.id.btn_9).clicked(this, "numpadClickAction").tag(9);

    }

    public void numpadClickAction(View button){
        int tag = (int)button.getTag();

        if(appendPrice.length() < 7){
            appendPrice = appendPrice + tag;
            modPrice = Integer.parseInt(appendPrice);

            aq.id(R.id.after_price_label).text(MZValidator.toNumFormat(modPrice));
            aq.id(R.id.num_text).text(MZValidator.toNumFormat(modPrice));
        }

    }

    public void closeAction(View button){
        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }


    public void saveAction(View button){
        if(modPrice > 0){
            Intent resultData = new Intent();
            resultData.putExtra("value", modPrice);
            setResult(Activity.RESULT_OK, resultData);
            finish();
        }else{
            DUtil.alertShow(this, "요금을 다시 확인해주세요.");
        }

    }

    public void numberDelete1Action(View button){
        if(appendPrice.length() > 0){
            appendPrice = appendPrice.substring(0, appendPrice.length()-1);
            if(appendPrice.length() == 0){
                modPrice = 0;
            }else {
                modPrice = Integer.parseInt(appendPrice);
            }

            aq.id(R.id.after_price_label).text(MZValidator.toNumFormat(modPrice));
            aq.id(R.id.num_text).text(MZValidator.toNumFormat(modPrice));
        }else{
            modPrice = 0;
            appendPrice = "";
            aq.id(R.id.after_price_label).text("0");
            aq.id(R.id.num_text).text("0");
        }
    }

    public void numberRemoveAction(View button){
        modPrice = 0;
        appendPrice = "";
        aq.id(R.id.after_price_label).text("0");
        aq.id(R.id.num_text).text("0");
    }

}
