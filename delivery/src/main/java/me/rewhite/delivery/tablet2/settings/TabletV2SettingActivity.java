package me.rewhite.delivery.tablet2.settings;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.TabletFragment.TabletFAQFragment;
import me.rewhite.delivery.TabletFragment.TabletNoticeFragment;
import me.rewhite.delivery.TabletFragment.TabletQNAFragment;
import me.rewhite.delivery.TabletFragment.TabletSMSRechargeFragment;
import me.rewhite.delivery.TabletFragment.TabletSystemManagementFragment;
import me.rewhite.delivery.common.activity.TabletBaseActivity;
import me.rewhite.delivery.tablet.TabletHtmlActivity;
import me.rewhite.delivery.util.CommonUtility;

public class TabletV2SettingActivity extends TabletBaseActivity {

    private final static String TAG = TabletV2SettingActivity.class.getSimpleName();
    public static final String FRAGMENT_NOTICE = "notice";
    public static final String FRAGMENT_FAQ = "faq";
    public static final String FRAGMENT_SYSTEM = "system";
    public static final String FRAGMENT_SMS = "sms";
    public static final String FRAGMENT_QA = "qa";

    public static final int MOD_BIZNO_RESULT = 99;

    AQuery aq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablet_v2_setting);

        aq = new AQuery(this);
        aq.id(R.id.btn_back).clicked(this, "closeClicked");

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.screen_title).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_1).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_2).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_3).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_4).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.btn_5).getTextView());

        aq.id(R.id.btn_1).clicked(this, "showData").tag(0).background(R.mipmap.tablet_subnav_selected);
        aq.id(R.id.btn_2).clicked(this, "showData").tag(1).background(R.mipmap.tablet_subnav_normal);
        aq.id(R.id.btn_3).clicked(this, "showData").tag(2).background(R.mipmap.tablet_subnav_normal);
        aq.id(R.id.btn_4).clicked(this, "showData").tag(3).background(R.mipmap.tablet_subnav_normal);
        aq.id(R.id.btn_5).clicked(this, "showData").tag(4).background(R.mipmap.tablet_subnav_normal);
        aq.id(R.id.btn_sms).clicked(this, "smsChargeAction");

        aq.id(R.id.rep_icon).image(R.mipmap.icon_top_settings_notice);

        showFragment(FRAGMENT_NOTICE);
    }

    public void showData(View button){
        int tag = (int)((Button)button).getTag();
        aq.id(R.id.btn_sms).gone();
        switch (tag){
            case 0:
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("공지사항");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_settings_notice);
                showFragment(FRAGMENT_NOTICE);
                break;
            case 1:
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("자주묻는질문");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_settings_faq);
                showFragment(FRAGMENT_FAQ);
                break;
            case 2:
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("시스템설정");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_settings_system);
                showFragment(FRAGMENT_SYSTEM);
                break;
            case 3:
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("문자 충전 및 설정");
                aq.id(R.id.btn_sms).visible();
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_settings_sms);
                showFragment(FRAGMENT_SMS);
                break;
            case 4:
                aq.id(R.id.btn_5).background(R.mipmap.tablet_subnav_selected);
                aq.id(R.id.btn_1).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_2).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_4).background(R.mipmap.tablet_subnav_normal);
                aq.id(R.id.btn_3).background(R.mipmap.tablet_subnav_normal);

                aq.id(R.id.screen_title).text("문의하기");
                aq.id(R.id.rep_icon).image(R.mipmap.icon_top_settings_qna);
                showFragment(FRAGMENT_QA);
                break;
        }

    }

    public void closeClicked(View button){
        finish();
    }

    public void smsChargeAction(View button){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false).setMessage("서비스 준비 중입니다. 상세문의는 1544-2951로 전화해주세요.")
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


    }

    public void showFragment(String fragmentName) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);
        showFragment(fragmentName, null, null);
    }

    String transitionName = "";

    public void showFragment(String fragmentName, View sharedElement, Bundle args) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(fragmentName);

        if (FRAGMENT_NOTICE.equals(fragmentName)) {
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletNoticeFragment();
        }else if(FRAGMENT_FAQ.equals(fragmentName)){
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletFAQFragment();
        }else if(FRAGMENT_SYSTEM.equals(fragmentName)){
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletSystemManagementFragment();
        }else if(FRAGMENT_SMS.equals(fragmentName)){
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletSMSRechargeFragment();
        }else if(FRAGMENT_QA.equals(fragmentName)){
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new TabletQNAFragment();
        }

        Log.e(TAG, "[TabletStoreInfoFragment] Create : " + fragmentName);

        if (null != args) {
            fragment.setArguments(args);
        }

        //ft.setCustomAnimations(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        ft.replace(R.id.content_frame, fragment, fragmentName);
        ft.addToBackStack(fragmentName);
        //ft.commitAllowingStateLoss();
        ft.commit();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case MOD_BIZNO_RESULT:
                    String bizId = data.getStringExtra("bizNo");
                    Log.i("bizId", bizId);

                    Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_SYSTEM);
                    if (fragment != null) {
                        ((TabletSystemManagementFragment) fragment).onActivityResult(requestCode, resultCode, data);
                    }

                    break;
            }
        }else{

        }
    }


    public void itemSelected(int position, String _url, String _title) {
        Intent startIntent = new Intent(this, TabletHtmlActivity.class);

        startIntent.putExtra("title", _title);
        startIntent.putExtra("url", _url);
        startIntent.putExtra("icon", "N");

        startActivity(startIntent);
    }

}
