package me.rewhite.delivery.layout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import me.rewhite.delivery.R;

@SuppressLint("NewApi")
public class PageIndicatorLayout extends LinearLayout {
	private Context mContext;

	private int mCount = 1;
	private int mIndex = 0;

	public PageIndicatorLayout(Context context) {
		super(context);
		mContext = context;
		initilize();
	}

	public PageIndicatorLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		initilize();
	}

	public PageIndicatorLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
		initilize();
	}

	private void initilize() {
		// setupView();
	}

	public void initilize(int count) {
		mCount = count;
		// setupView();
	}

	public void setPager(ViewPager pager) {
		pager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				move(position);
			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
			}

			@Override
			public void onPageScrollStateChanged(int state) {
			}
		});
	}

	private void move(int index) {
		mIndex = (index <= (mCount - 1)) ? index : mCount;
		// setupView();
	}

	private void setupView() {
		LinearLayout layout = (LinearLayout) findViewById(R.id.layout_menu_indicator);
		layout.removeAllViews();

		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT);
		params.leftMargin = 6;
		params.rightMargin = 6;

		for (int i = 0; i < mCount; i++) {
			if (i == mIndex) {
				// select
				ImageView imageView = new ImageView(mContext);
				imageView.setImageResource(R.mipmap.ic_launcher);
				imageView.setLayoutParams(params);
				layout.addView(imageView);
			} else {
				// normal
				ImageView imageView = new ImageView(mContext);
				imageView.setImageResource(R.mipmap.ic_launcher);
				imageView.setLayoutParams(params);
				layout.addView(imageView);
			}
		}

		if (mCount != 1)
			layout.setVisibility(View.VISIBLE);
	}
}
