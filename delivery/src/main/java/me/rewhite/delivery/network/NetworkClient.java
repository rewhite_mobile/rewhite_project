package me.rewhite.delivery.network;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ConnectionPoolTimeoutException;

import java.io.IOException;
import java.net.UnknownHostException;

import me.rewhite.delivery.session.AccessToken;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.SharedPreferencesUtility.UserInfo;
import me.rewhite.delivery.util.StringUtil;


public class NetworkClient {

	private static final String BASE_URL = CommonUtility.SERVER_URL;

	private static AsyncHttpClient client = new AsyncHttpClient();

	public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		client.get(getAbsoluteUrl(url), params, responseHandler);
	}

	public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		String gcmId = SharedPreferencesUtility.get(UserInfo.GCM_ID);
		String accessToken = AccessToken.createFromCache().getAccessTokenString();
		if (!StringUtil.isNullOrEmpty(gcmId) && !StringUtil.isNullOrEmpty(accessToken)) {
			client.addHeader("Authorization", "device_token='" + gcmId + "',access_token='" + accessToken + "'");
		}
		//client.setTimeout(10000);
		client.setMaxRetriesAndTimeout(3, 10000);

		client.allowRetryExceptionClass(IOException.class);
		client.allowRetryExceptionClass(IllegalArgumentException.class);
		client.allowRetryExceptionClass(ConnectTimeoutException.class);
		client.blockRetryExceptionClass(UnknownHostException.class);
		client.blockRetryExceptionClass(ConnectionPoolTimeoutException.class);

		client.post(getAbsoluteUrl(url), params, responseHandler);
	}

	public static void postExt(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		String gcmId = SharedPreferencesUtility.get(UserInfo.GCM_ID);
		String accessToken = AccessToken.createFromCache().getAccessTokenString();
		if (!StringUtil.isNullOrEmpty(gcmId) && !StringUtil.isNullOrEmpty(accessToken)) {
			client.addHeader("Authorization", "device_token='" + gcmId + "',access_token='" + accessToken + "'");
		}
		//client.setTimeout(10000);
		client.setMaxRetriesAndTimeout(3, 10000);

		client.allowRetryExceptionClass(IOException.class);
		client.allowRetryExceptionClass(IllegalArgumentException.class);
		client.allowRetryExceptionClass(ConnectTimeoutException.class);
		client.blockRetryExceptionClass(UnknownHostException.class);
		client.blockRetryExceptionClass(ConnectionPoolTimeoutException.class);
		client.post(url, params, responseHandler);
	}

	private static String getAbsoluteUrl(String relativeUrl) {
		return BASE_URL + relativeUrl;
	}


}
