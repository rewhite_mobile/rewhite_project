package me.rewhite.delivery.fragment;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import kankan.wheel.widget.OnWheelClickedListener;
import kankan.wheel.widget.OnWheelScrollListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.AbstractWheelTextAdapter;
import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.PickupItemActivity;
import me.rewhite.delivery.util.ToastUtility;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PickupItemSubCategoryFragmentV2.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PickupItemSubCategoryFragmentV2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PickupItemSubCategoryFragmentV2 extends Fragment implements IFragment{

    private final static String TAG = "PickupItemFragmentV2";
    private PickupItemActivity mActivity;

    private OnFragmentInteractionListener mListener;
    public AQuery aq;

    ProgressDialog dialog;
    public ArrayList<String> optionItems = new ArrayList<String>();
    private boolean scrolling = false;
    private RelativeLayout.LayoutParams layoutParams;
    private View rootselectview;
    private View rootbackview;
    private String fragmentName = PickupItemActivity.FRAGMENT_DETAIL;

    private String mainCateName;
    private int mainCateIndex;

    private ViewGroup mView = null;

    public PickupItemSubCategoryFragmentV2() {
        // Required empty public constructor
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    public static PickupItemSubCategoryFragmentV2 newInstance(String mainCategoryName, int mainCategoryIndex) {
        PickupItemSubCategoryFragmentV2 fragment = new PickupItemSubCategoryFragmentV2();
        Bundle args = new Bundle();
        args.putString("name", mainCategoryName);
        args.putInt("index", mainCategoryIndex);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = (PickupItemActivity)getActivity();

        if (getArguments() != null) {
            mainCateName = getArguments().getString("name");
            mainCateIndex = getArguments().getInt("index");

            try {
                if(mActivity.priceArray != null && mActivity.priceArray.length() >= 0){
                    JSONArray data = mActivity.priceArray.getJSONObject(mActivity.Step1Index).getJSONArray("items");
                    for(int i = 0; i < data.length(); i++){
                        optionItems.add(data.getJSONObject(i).getString("itemTitle"));
                    }
                }else{
                    ToastUtility.show(mActivity, "데이터가 비정상적입니다. 고객센터로 문의해주세요.", Toast.LENGTH_LONG);
                }
            } catch (JSONException | NullPointerException e) {
                e.printStackTrace();
                ToastUtility.show(mActivity, "데이터가 비정상적입니다. 고객센터로 문의해주세요.", Toast.LENGTH_LONG);
                getActivity().finish();
            }
        }
    }

    WheelView wheelview;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_pickup_item_sub_v2, container, false);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    v.removeOnLayoutChangeListener(this);
                }
            });
        }
        aq = new AQuery(mView);

        mActivity = (PickupItemActivity)getActivity();

        rootselectview = aq.id(R.id.rootselectview).getView();
        rootbackview = aq.id(R.id.rootbackview).getView();
        rootbackview.setBackgroundColor(0xffffffff);

        RelativeLayout layout = (RelativeLayout)mView.findViewById(R.id.wheel_area);
        aq.id(R.id.title_text).text(mainCateName);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mView.findViewById(R.id.title_text).setTransitionName("currentLabel");
        }


        wheelview = (WheelView)mView.findViewById(R.id.wheelview);
        wheelview.setVisibleItems(6);
        //ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<String>(this, optionItems);
        //adapter.setTextSize(22);
        wheelview.setViewAdapter(new PickItemAdapter(getActivity()));
        wheelview.setCurrentItem(0);

        wheelview.addClickingListener(new OnWheelClickedListener() {
            @Override
            public void onItemClicked(WheelView wheel, int itemIndex) {
                //wheel.scroll(itemIndex, 200);
                wheel.setCurrentItem(itemIndex, true);
                Log.e("onItemClicked()", itemIndex + "");
            }
        });

        wheelview.addScrollingListener( new OnWheelScrollListener() {
            public void onScrollingStarted(WheelView wheel) {

                scrolling = true;
                aq.id(R.id.guide_desc).gone();
                aq.id(R.id.btn_select).gone();

                //aq.id(R.id.current_label).gone();
                //aq.id(R.id.current_count).gone();

                aq.id(R.id.selectview).background(R.drawable.wheel_pick_current_moving);
                //rootbackview.setBackgroundColor(0x00000000);
                rootselectview.setBackgroundColor(0x00000000);

                aq.id(R.id.rootbackview).getView().animate()
                        .setStartDelay(0)
                        .setInterpolator(new AccelerateInterpolator())
                        .alpha(0)
                        .scaleX(1)
                        .scaleY(1);
                aq.id(R.id.current_label).getView().animate()
                        .setStartDelay(0)
                        .setInterpolator(new AccelerateInterpolator())
                        .alpha(0)
                        .scaleX(0.5f)
                        .scaleY(0.5f);
                aq.id(R.id.btn_select).getView().animate()
                        .setStartDelay(0)
                        .setInterpolator(new AccelerateInterpolator())
                        .alpha(0)
                        .scaleX(.5f)
                        .scaleY(.5f);
            }

            public void onScrollingFinished(WheelView wheel) {

                scrolling = false;
                Log.e("getCurrentItem()", wheel.getCurrentItem() + "");
                if(wheel.getCurrentItem() == 0){
                    aq.id(R.id.guide_desc).visible();
                }
                aq.id(R.id.selectview).background(R.drawable.wheel_pick_current);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    rootbackview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {

                            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                rootbackview.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            } else {
                                rootbackview.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            }

                            rootbackview.post(new Runnable()
                            {
                                @Override
                                public void run(){

                                    int cx = (rootbackview.getLeft() + rootbackview.getRight()) / 2;
                                    int cy = (rootbackview.getTop() + rootbackview.getBottom()) / 2;
                                    int finalRadius = Math.max(rootbackview.getWidth(), rootbackview.getHeight());
                                    //create your anim here
                                    Animator anim = null;
                                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                                        anim = ViewAnimationUtils.createCircularReveal(rootbackview, cx, cy, 0, finalRadius);
                                    }
                                    anim.start();
                                }
                            });


                        }
                    });
                }

                setCurrentLabel(wheel.getCurrentItem());
                aq.id(R.id.btn_select).image(R.mipmap.pui_btn_select).visible();

            }
        });

        aq.id(R.id.btn_select).clicked(this, "selectAction");
        aq.id(R.id.btn_close).clicked(this, "closeAction");
        aq.id(R.id.btn_back).clicked(this, "backAction");

        setCurrentLabel(wheelview.getCurrentItem());

        return  mView;
    }

    public void closeAction(View button){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setCancelable(true).setMessage("세탁물 등록이 완료되지 않았습니다.\n" +
                "중단하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
    public void backAction(View button){
        getActivity().onBackPressed();
    }

    private void setCurrentLabel(int _index){
        aq.id(R.id.rootbackview).getView().animate()
                .setStartDelay(0)
                .setInterpolator(new OvershootInterpolator())
                .alpha(1)
                .scaleX(1)
                .scaleY(1);
        aq.id(R.id.current_label).getView().animate()
                .setStartDelay(0)
                .setInterpolator(new OvershootInterpolator())
                .alpha(1)
                .scaleX(1)
                .scaleY(1);
        aq.id(R.id.btn_select).getView().animate()
                .setStartDelay(150)
                .setInterpolator(new OvershootInterpolator())
                .alpha(1)
                .scaleX(1)
                .scaleY(1);

        aq.id(R.id.title_text).text(mainCateName + " > " + optionItems.get(_index));
        aq.id(R.id.current_label).text(optionItems.get(_index)).textColor(0xFF000000).visible();
    }

    public void selectAction(View button){
        int cx = rootselectview.getRight();//(rootselectview.getLeft() + rootselectview.getRight()) / 2;
        int cy = rootselectview.getTop();
        int finalRadius = Math.max(rootselectview.getWidth(), rootselectview.getHeight());

        mActivity.Step2Name = optionItems.get(wheelview.getCurrentItem());
        mActivity.Step2Index = wheelview.getCurrentItem();

        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(rootselectview, cx, cy, 0, finalRadius);
            rootselectview.setBackgroundColor(0xff01bff3);
            aq.id(R.id.current_label).textColor(0xFFFFFFFF);
            aq.id(R.id.current_count).textColor(0xFFFFFFFF);
            aq.id(R.id.btn_select).image(R.mipmap.pui_btn_select_hover);
            anim.start();

            ViewGroup sharedView = (ViewGroup) mView.findViewById(R.id.title_area);
            TextView mainCategoryName = (TextView) mView.findViewById(R.id.title_text);
            mainCategoryName.setTransitionName("currentLabel");

            String transitionName = "pickupItemSubCategoryTitle";

            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Fragment fragment = fm.findFragmentByTag(fragmentName);

            fragment = PickupDetailFragmentV2.newInstance(optionItems.get(wheelview.getCurrentItem()), wheelview.getCurrentItem());


            setSharedElementReturnTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(R.transition.change_transform));
            setExitTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(android.R.transition.fade));
            fragment.setSharedElementEnterTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(R.transition.change_transform));
            fragment.setEnterTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(android.R.transition.fade));

            ft.addSharedElement(mainCategoryName, "currentLabel")
                    .addSharedElement(sharedView, transitionName)
                    .replace(R.id.content_frame, fragment)
                    .addToBackStack(fragmentName)
                    .commit();
        }else{
            PickupDetailFragmentV2 subFragment = PickupDetailFragmentV2.newInstance(optionItems.get(wheelview.getCurrentItem()), wheelview.getCurrentItem());

            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Fragment fragment = fm.findFragmentByTag(fragmentName);

            ft
                    .replace(R.id.content_frame, subFragment)
                    .addToBackStack(fragmentName)
                    .commit();
        }

    }

    /**
     * Adapter for countries
     */
    private class PickItemAdapter extends AbstractWheelTextAdapter {
        // Countries names


        /**
         * Constructor
         */
        protected PickItemAdapter(Context context) {
            super(context, R.layout.pickitem_layout, NO_RESOURCE);

            setItemTextResource(R.id.text_label);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            View view = super.getItem(index, cachedView, parent);

            TextView label = (TextView)view.findViewById(R.id.text_label);
            String v = optionItems.get(index);
            v = v.replace("\n","");
            v = v.replace("\r","");
            label.setText(v);

            return view;
        }

        @Override
        public int getItemsCount() {
            return optionItems.size();
        }

        @Override
        protected CharSequence getItemText(int index) {
            return optionItems.get(index);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
