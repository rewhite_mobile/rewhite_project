package me.rewhite.delivery.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.util.CommonUtility;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DatePickFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DatePickFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DatePickFragment extends Fragment {
    private static final String ARG_DAY = "day";
    //private static final String ARG_YOIL = "yoil";
    AQuery aq;
    boolean isSelected = false;

    public static DatePickFragment create(int day) {
        DatePickFragment fragment = new DatePickFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_DAY, day);
        //bundle.putInt(ARG_YOIL, yoil);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void setSelected() {
        isSelected = true;
    }

    public void setUnselected() {
        isSelected = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_date_pick, container, false);
        aq = new AQuery(rootView);
        final Bundle arguments = getArguments();
        aq.id(R.id.date_text).text(arguments.getInt(ARG_DAY) + "").typeface(CommonUtility.getNanumBarunTypeface());

        //aq.id(R.id.day_text).text(m_week);

        //Log.i("DatePick", arguments.getInt(ARG_DAY) + "");

        if (arguments != null) {
            /*
            btItem.setText(
                    getString(R.string.page_number_1d,
                            arguments.getInt(ARG_PAGE_NUMBER) + 1));
            btItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(getActivity())
                            .setMessage(getString(R.string.page_number_1d,
                                    arguments.getInt(ARG_PAGE_NUMBER) + 1))
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            });*/
        } else {
            //btItem.setVisibility(View.GONE);
        }
        return rootView;
    }
}
