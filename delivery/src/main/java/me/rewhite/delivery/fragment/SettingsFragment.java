package me.rewhite.delivery.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.AboutActivity;
import me.rewhite.delivery.activity.FAQActivity;
import me.rewhite.delivery.activity.HtmlActivity;
import me.rewhite.delivery.activity.MainActivity;
import me.rewhite.delivery.activity.QNAActivity;
import me.rewhite.delivery.activity.TutorialActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.session.Session;
import me.rewhite.delivery.util.SystemUtil;


public class SettingsFragment extends Fragment implements IFragment {

    private static final String TAG = "SettingsFragment";
    AQuery aq;
    private MainActivity mMainActivity = null;
    private Context ctx;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {

        Log.d(TAG, "[onResume]");
        mMainActivity.setTouchDisable(false);

        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mMainActivity = (MainActivity) getActivity();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ctx = getActivity();
        aq = new AQuery(getActivity(), view);
        aq.id(R.id.logo).gone();
        aq.id(R.id.title_text).text("설정").visible();
        aq.id(R.id.notice_layout).clicked(this, "htmlClicked");
        aq.id(R.id.faq_layout).clicked(this, "faqClicked");

        aq.id(R.id.help_layout).clicked(this, "helpClicked");
        aq.id(R.id.qna_layout).clicked(this, "qnaClicked");
        aq.id(R.id.about_layout).clicked(this, "aboutClicked");
        aq.id(R.id.settings_layout).clicked(this, "settingsClicked");
        aq.id(R.id.btn_logout).clicked(this, "logoutClicked");

        aq.hardwareAccelerated11();
        aq.id(R.id.top_area).clicked(this, "topmenuClicked");

        return view;
    }

    public void settingsClicked(View button){
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setCancelable(false).setTitle("앱 권한 설정하기").setMessage("앱 권한설정 화면으로 이동합니다. 전체 허용해주세요~")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SystemUtil.showApplicationDetailSetting(getActivity());
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void logoutClicked(View button) {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
        alertDialogBuilder.setCancelable(false).setTitle("로그아웃").setMessage("정말 로그아웃 하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Session.getCurrentSession().close();
                        getActivity().finish();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void topmenuClicked(View button) {
        mMainActivity.getSlidingMenu().toggle();
    }

    public void helpClicked(View button) {
        if (getActivity() == null) {
            return;
        }

        if (getActivity() instanceof MainActivity) {
            MainActivity ra = (MainActivity) getActivity();

            Intent intent = new Intent(ra, TutorialActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            ra.startActivity(intent);
        }
    }

    public void aboutClicked(View button) {
        if (getActivity() == null) {
            return;
        }

        if (getActivity() instanceof MainActivity) {
            MainActivity ra = (MainActivity) getActivity();

            Intent intent = new Intent(ra, AboutActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            ra.startActivity(intent);
        }
    }

    public void qnaClicked(View button) {
        if (getActivity() == null) {
            return;
        }

        if (getActivity() instanceof MainActivity) {
            MainActivity ra = (MainActivity) getActivity();

            Intent intent = new Intent(ra, QNAActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            ra.startActivity(intent);
        }
    }

    public void faqClicked(View button) {
        if (getActivity() == null) {
            return;
        }

        if (getActivity() instanceof MainActivity) {
            MainActivity ra = (MainActivity) getActivity();
            Intent intent = new Intent(ra, FAQActivity.class);
            intent.putExtra("section", 0);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            ra.startActivity(intent);
        }
    }

    public void htmlClicked(View button) {
        if (getActivity() == null) {
            return;
        }

        if (getActivity() instanceof MainActivity) {
            MainActivity ra = (MainActivity) getActivity();
            Intent intent = new Intent(ra,
                    HtmlActivity.class);
            intent.putExtra("TERMS_URI", Constants.HTML_NOTICE);
            intent.putExtra("TITLE_NAME", "공지사항");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            ra.startActivity(intent);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }



}
