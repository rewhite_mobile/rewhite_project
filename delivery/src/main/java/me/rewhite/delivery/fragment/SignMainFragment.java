package me.rewhite.delivery.fragment;

import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.delivery.Bootstrap;
import me.rewhite.delivery.Bootstrap.TrackerName;
import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.SignActivity;

//import com.actionbarsherlock.app.SherlockFragment;


public class SignMainFragment extends Fragment {

	private static final String TAG = "SignMainFragment";
	private static final String KEY_CONTENT = "SignMainFragment";
	private SignActivity mSignActivity = null;
	private ViewGroup mView;
	private AQuery aq;
	static SignActivity parent;

	public SignMainFragment() {
	}

	public static SignMainFragment newInstance(String content) {
		SignMainFragment fragment = new SignMainFragment();
		Bundle bdl = new Bundle();
		bdl.putString(KEY_CONTENT, content);

		fragment.setArguments(bdl);

		return fragment;
	}

	private String mContent = "???";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
			mContent = savedInstanceState.getString(KEY_CONTENT);
		}
		
		Tracker t = ((Bootstrap) getActivity().getApplication()).getTracker(TrackerName.APP_TRACKER);
		// Set screen name.
		t.setScreenName(TAG);
		// Send a screen view.
		t.send(new HitBuilders.ScreenViewBuilder().build());
		
		mSignActivity = (SignActivity) getActivity();
		mView = (ViewGroup) inflater.inflate(R.layout.fragment_signmain_top, container, false);
		aq = new AQuery(mSignActivity, mView);
		
		PackageManager m = mSignActivity.getPackageManager();
		String app_ver;
		try {
			app_ver = m.getPackageInfo(mSignActivity.getPackageName(), 0).versionName;
			//aq.id(R.id.version_text).text("ver " + app_ver);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		aq.id(R.id.layout_arrow).clicked(this, "transitionLoginClicked");
		aq.id(R.id.btn_register).clicked(this, "joinClicked");

		return mView;
	}
	
	public void transitionLoginClicked(View button){
		Log.d(TAG, "[transitionLoginClicked] ");
		//mSignActivity.buttonTransitionClicked(true);
	}

	public void joinClicked(View button) {
		Log.d(TAG, "[joinClicked] ");
/*
		Intent intent = new Intent();
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.setClass(mSignActivity, MemberActivity.class);

		mSignActivity.startActivity(intent);*/
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(KEY_CONTENT, mContent);
	}

}
