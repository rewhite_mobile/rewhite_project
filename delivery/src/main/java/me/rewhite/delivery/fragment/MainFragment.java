package me.rewhite.delivery.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import net.daum.mf.map.api.MapPOIItem;
import net.daum.mf.map.api.MapPoint;
import net.daum.mf.map.api.MapView;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.MainActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.data.RewhiteType;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.session.Session;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;


public class MainFragment extends Fragment implements IFragment, LocationListener, MapView.MapViewEventListener, MapView.POIItemEventListener, MapView.OpenAPIKeyAuthenticationResultListener {

    private static final String TAG = "MainFragment";
    AQuery aq;

    boolean markerClicked;
    boolean locationTag = true;
    private MainActivity mMainActivity = null;
    private HashMap<Integer, MapPOIItem> mHashMap = new HashMap<Integer, MapPOIItem>();
    private LocationManager locationManager;
    private String provider;
    private Context ctx;
    private String currentLocationAddress = "";
    TextView tab01txt;
    TextView tab02txt;

    private int currentSelectedTab = 0;
    boolean isMapInitialized = false;
    private MapView mapView;
    private View mView;
    Fragment oit;

    private int mPos = -1;
    private boolean isDaumMapViewInitialized = false;

    public MainFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public MainFragment(int pos) {
        mPos = pos;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
/*
        if (mapView != null) {
            mapView = null;
        }*/
    }

    @Override
    public void onPause() {
        super.onPause();
/*
        ViewGroup mapViewContainer = (ViewGroup) mView.findViewById(R.id.map_container);
        mapViewContainer.removeAllViews();
        mapView = null;*/
    }

    @Override
    public void onResume() {

        Log.d(TAG, "[onResume]");
        mMainActivity.setTouchDisable(false);

        super.onResume();

        getCountOrders();

        if (!isDaumMapViewInitialized) {
            mapView = new MapView(mView.getContext());
            //mapView = (MapView)mView.findViewById(R.id.map_view);
            mapView.setDaumMapApiKey(Constants.DAUM_API_KEY);
            mapView.setMapViewEventListener(this); // this에 MapView.MapViewEventListener 구현.
            mapView.setPOIItemEventListener(this);
            MapView.setMapTilePersistentCacheEnabled(true);
            mapView.setShowCurrentLocationMarker(true);
            isDaumMapViewInitialized = true;

            ViewGroup mapViewContainer = (ViewGroup) mView.findViewById(R.id.map_container);
            mapViewContainer.addView(mapView);
        }

        clearBottomInfo();

        aq.id(R.id.btn_tab01).image(R.mipmap.btn_tab_selected);
        aq.id(R.id.btn_tab02).image(R.mipmap.btn_tab_normal);
        aq.id(R.id.tab_text_01).textColor(Color.parseColor("#ffffff"));
        aq.id(R.id.tab_text_02).textColor(Color.parseColor("#c5e2ff"));
        currentSelectedTab = 0;
        getPickupData("A");



    }

    public void viewTypeToggle(View button){
        mMainActivity.mainViewTypeToggle();
    }

    private void clearBottomInfo() {
        if (oit != null) {
            getChildFragmentManager().beginTransaction()
                    .remove(oit).commit();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_main, container, false);

        mMainActivity = (MainActivity) getActivity();
        ctx = getActivity();
        aq = new AQuery(getActivity(), mView);
        aq.hardwareAccelerated11();
        aq.id(R.id.top_area).clicked(this, "topmenuClicked");

        aq.id(R.id.tab_text_01).typeface(CommonUtility.getNanumBarunTypeface()).textColor(Color.parseColor("#ffffff"));
        aq.id(R.id.tab_text_02).typeface(CommonUtility.getNanumBarunTypeface()).textColor(Color.parseColor("#c5e2ff"));

        tab01txt = aq.id(R.id.tab_text_01).getTextView();
        tab01txt.setPaintFlags(tab01txt.getPaintFlags() | Paint.FAKE_BOLD_TEXT_FLAG);
        tab02txt = aq.id(R.id.tab_text_02).getTextView();
        tab02txt.setPaintFlags(tab02txt.getPaintFlags() | Paint.FAKE_BOLD_TEXT_FLAG);

        aq.id(R.id.btn_tab01).clicked(this, "tab01Clicked");
        aq.id(R.id.btn_tab02).clicked(this, "tab02Clicked");

        aq.id(R.id.btn_toggle).clicked(this, "viewTypeToggle");

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, true);

        if (provider == null) {  //위치정보 설정이 안되어 있으면 설정하는 엑티비티로 이동합니다
            new AlertDialog.Builder(ctx)
                    .setTitle("위치서비스 동의")
                    .setNeutralButton("이동", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
                        }
                    }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    getActivity().finish();
                }
            }).show();
        } else {   //위치 정보 설정이 되어 있으면 현재위치를 받아옵니다
            locationManager.requestLocationUpdates(provider, 1, 1, this);
            //setUpMapIfNeeded();
        }



        String storeAddress = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_ADDRESS);
        aq.id(R.id.address_top_title).typeface(CommonUtility.getNanumBarunTypeface()).text(storeAddress);

        //getPickupData("A");

        aq.id(R.id.count_text_01).text(SharedPreferencesUtility.get(SharedPreferencesUtility.OrderCount.Pickup));
        aq.id(R.id.count_text_02).text(SharedPreferencesUtility.get(SharedPreferencesUtility.OrderCount.Delivery));

        return mView;
    }

    public void getCountOrders() {
        //ORDER_SUMMARY_COUNT
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_SUMMARY_COUNT, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_SUMMARY_COUNT, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_SUMMARY_COUNT, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //

                        } else {
                            int a = jsondata.getJSONObject("data").getInt("A");
                            int b = jsondata.getJSONObject("data").getInt("B");

                            SharedPreferencesUtility.set(SharedPreferencesUtility.OrderCount.Pickup, a+"");
                            SharedPreferencesUtility.set(SharedPreferencesUtility.OrderCount.Delivery, b+"");

                            aq.id(R.id.count_text_01).text(SharedPreferencesUtility.get(SharedPreferencesUtility.OrderCount.Pickup));
                            aq.id(R.id.count_text_02).text(SharedPreferencesUtility.get(SharedPreferencesUtility.OrderCount.Delivery));
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }


    public void topmenuClicked(View button) {
        mMainActivity.getSlidingMenu().toggle();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }

    public void tab01Clicked(View button) {
        aq.id(R.id.btn_tab01).image(R.mipmap.btn_tab_selected);
        aq.id(R.id.btn_tab02).image(R.mipmap.btn_tab_normal);
        aq.id(R.id.tab_text_01).textColor(Color.parseColor("#ffffff"));
        aq.id(R.id.tab_text_02).textColor(Color.parseColor("#c5e2ff"));
        currentSelectedTab = 0;
        getPickupData("A");

    }

    public void tab02Clicked(View button) {

        aq.id(R.id.btn_tab01).image(R.mipmap.btn_tab_normal);
        aq.id(R.id.btn_tab02).image(R.mipmap.btn_tab_selected);
        aq.id(R.id.tab_text_01).textColor(Color.parseColor("#c5e2ff"));
        aq.id(R.id.tab_text_02).textColor(Color.parseColor("#ffffff"));
        currentSelectedTab = 1;

        getPickupData("B");

    }

    private void removeMarkers() {
        /*
        for (int i = 0; i < mHashMap.size(); i++) {
            mapView.removePOIItem(mHashMap.get(i));
        }*/
        mapView.removeAllPOIItems();
        mHashMap.clear();
    }

    private void addMarkers() throws JSONException {

        String coordinates[] = {SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LATITUDE), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LONGITUDE)};
        double lat = Double.parseDouble(coordinates[0]);
        double lng = Double.parseDouble(coordinates[1]);

        MapPOIItem storeMarker = new MapPOIItem();
        storeMarker.setItemName(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_NAME));
        storeMarker.setTag(999);
        storeMarker.setMapPoint(MapPoint.mapPointWithGeoCoord(lat, lng));
        storeMarker.setMarkerType(MapPOIItem.MarkerType.CustomImage); // 마커타입을 커스텀 마커로 지정.
        storeMarker.setCustomImageResourceId(R.mipmap.marker_home); // 마커 이미지.
        storeMarker.setCustomImageAutoscale(false); // hdpi, xhdpi 등 안드로이드 플랫폼의 스케일을 사용할 경우 지도 라이브러리의 스케일 기능을 꺼줌.
        storeMarker.setCustomImageAnchor(0.5f, 1.0f); // 마커 이미지중 기준이 되는 위치(앵커포인트) 지정 - 마커 이미지 좌측 상단 기준 x(0.0f ~ 1.0f), y(0.0f ~ 1.0f) 값.
        storeMarker.setShowAnimationType(MapPOIItem.ShowAnimationType.SpringFromGround);
        mapView.addPOIItem(storeMarker);

        if (dataSet == null) {
            return;
        }

        for (int i = 0; i < dataSet.length(); i++) {

            MapPOIItem customMarker = new MapPOIItem();
            customMarker.setItemName(dataSet.getJSONObject(i).getString("userName"));
            customMarker.setTag(i);
            customMarker.setMapPoint(MapPoint.mapPointWithGeoCoord(dataSet.getJSONObject(i).getDouble("latitude"), dataSet.getJSONObject(i).getDouble("longitude")));
            customMarker.setMarkerType(MapPOIItem.MarkerType.CustomImage); // 마커타입을 커스텀 마커로 지정.
            customMarker.setCustomImageResourceId(R.mipmap.marker_user); // 마커 이미지.
            customMarker.setCustomImageAutoscale(false); // hdpi, xhdpi 등 안드로이드 플랫폼의 스케일을 사용할 경우 지도 라이브러리의 스케일 기능을 꺼줌.
            customMarker.setCustomImageAnchor(0.5f, 1.0f); // 마커 이미지중 기준이 되는 위치(앵커포인트) 지정 - 마커 이미지 좌측 상단 기준 x(0.0f ~ 1.0f), y(0.0f ~ 1.0f) 값.
            customMarker.setShowAnimationType(MapPOIItem.ShowAnimationType.SpringFromGround);

            mapView.addPOIItem(customMarker);
            mHashMap.put(i, customMarker);
        }
    }

    //--------------------------------------------------------------------------------------
    // IFragment Implement
    //--------------------------------------------------------------------------------------
    private void getPickupData(final String orderType) {

        clearBottomInfo();

        if ("A".equals(orderType)) {
            currentSelectedTab = 0;
        } else {
            currentSelectedTab = 1;
        }
        //initialize(currentSelectedTab);

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("mode", orderType);
        params.put("page", 1);
        params.put("block", 20);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_LIST, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {

                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_LIST, result);

                    JSONObject jsondata = new JSONObject(result);
                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없음
                            dataSet = null;
                        } else {
                            dataSet = jsondata.getJSONArray("data");
                            if (dataSet.length() > 0) {

                                for (int i = 0; i < dataSet.length(); i++) {
                                    int orderId = dataSet.getJSONObject(i).getInt("orderId");
                                }

                                // storeId, pickupRequestTime, deliveryRequestTime, pickupRequestMessage
                            } else {

                            }

                        }

                        if (mapView != null) {
                            removeMarkers();
                            addMarkers();
                        }

                    } else if ("S9002".equals(jsondata.getString("resultCode"))) {
                        Session.getCurrentSession().close();
                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private JSONArray dataSet;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {//위치설정 엑티비티 종료 후
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                Criteria criteria = new Criteria();
                provider = locationManager.getBestProvider(criteria, true);
                if (provider == null) {//사용자가 위치설정동의 안했을때 종료
                    getActivity().finish();
                } else {//사용자가 위치설정 동의 했을때
                    locationManager.requestLocationUpdates(provider, 1L, 2F, this);
                    //setUpMapIfNeeded();
                }
                break;
        }
    }

    private void layoutSetup() {
        final RelativeLayout layout = (RelativeLayout) mView.findViewById(R.id.map_container);
        ViewTreeObserver vto = layout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                int width = layout.getMeasuredWidth();
                int Height = layout.getMeasuredHeight();

                Log.i("layoutSetup", width + " // " + Height + " // " + ScreenSize());

                final RelativeLayout.LayoutParams mLayoutParam = (RelativeLayout.LayoutParams) layout.getLayoutParams();
                mLayoutParam.height = ScreenSize(); //(위에 설명 드린 소스를 사용하여 사이즈를 구하여 가로 사이즈를 입력);
                layout.setLayoutParams(mLayoutParam);
                layout.setGravity(Gravity.TOP);
            }
        });
    }


    /**
     * </span></p><p><span style="font-size: 11pt;">
     *
     * @return 4:2 사이즈 비율 세로사이즈 리턴<b> <br /></b></span></p><p><span style="font-size: 11pt;"><b></b>
     */
    public int ScreenSize() {
        int size = 0;
        //아래주석을 풀면 해상도 사이즈를 구해서 세로 사이즈를 구합니다.</span></p><p><span style="font-size: 11pt;">
        DisplayMetrics mDisplay = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mDisplay);
        size = mDisplay.widthPixels;

        return size;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (locationTag) {//한번만 위치를 가져오기 위해서 tag를 주었습니다
            Log.d("myLog", "onLocationChanged: !!" + "onLocationChanged!!");
            double lat = location.getLatitude();
            double lng = location.getLongitude();

            //Toast.makeText(ctx, "위도  : " + lat + " 경도: " + lng, Toast.LENGTH_SHORT).show();
            locationTag = false;
        }
    }


    /*
        DAUM MAP INTERFACE
    */
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onMapViewInitialized(MapView mapView) {
        //layoutSetup();

        String coordinates[] = {SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LATITUDE), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LONGITUDE)};
        double lat = Double.parseDouble(coordinates[0]);
        double lng = Double.parseDouble(coordinates[1]);

        mapView.setMapCenterPointAndZoomLevel(MapPoint.mapPointWithGeoCoord(lat, lng), 1, true);
        isMapInitialized = true;

        BlankFragment oit = new BlankFragment();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.order_element, oit).commit();
    }

    @Override
    public void onMapViewCenterPointMoved(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewZoomLevelChanged(MapView mapView, int i) {

    }

    @Override
    public void onMapViewSingleTapped(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewDoubleTapped(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewLongPressed(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewDragStarted(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewDragEnded(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onMapViewMoveFinished(MapView mapView, MapPoint mapPoint) {

    }

    @Override
    public void onDaumMapOpenAPIKeyAuthenticationResult(MapView mapView, int i, String s) {

    }

    @Override
    public void onPOIItemSelected(MapView mapView, MapPOIItem mapPOIItem) {
        int currentType = RewhiteType.ORDERTYPE_PICKUP;
        if (currentSelectedTab == 0) {
            currentType = RewhiteType.ORDERTYPE_PICKUP;
        } else {
            currentType = RewhiteType.ORDERTYPE_DELIVERY;
        }

        try {
            if (mapPOIItem.getTag() == 999) {
                return;
            }
            oit = OrderItemFragment.create(dataSet.getJSONObject(mapPOIItem.getTag()), currentType);
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.order_element, oit).setCustomAnimations(R.anim.fadein, R.anim.fadeout).commit();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem) {

    }

    @Override
    public void onCalloutBalloonOfPOIItemTouched(MapView mapView, MapPOIItem mapPOIItem, MapPOIItem.CalloutBalloonButtonType calloutBalloonButtonType) {

    }

    @Override
    public void onDraggablePOIItemMoved(MapView mapView, MapPOIItem mapPOIItem, MapPoint mapPoint) {

    }
}
