package me.rewhite.delivery.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.MainActivity;
import me.rewhite.delivery.adapter.OrderListItem;
import me.rewhite.delivery.adapter.OrderListViewAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;


public class OrderUnpaidItemListFragment extends Fragment implements IFragment{

    private static final String TAG = "ItemListFragment";
    AQuery aq;
    private MainActivity mMainActivity = null;
    private Context ctx;
    private OrderListViewAdapter orderAdapter;
    private ArrayList<OrderListItem> orderData;
    int currentPage = 1;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public OrderUnpaidItemListFragment() {
    }

    @Override
    public void onResume() {

        Log.d(TAG, "[onResume]");
        mMainActivity.setTouchDisable(false);
        super.onResume();

        orderData = new ArrayList<>();
        currentPage = 1;
        aq.id(R.id.empty).visible();
        mMainActivity.orderData = orderData;

        initialize();
    }

    String currentSearchType = "Z";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mMainActivity = (MainActivity) getActivity();
        super.onCreate(savedInstanceState);


    }
    int blocksize = 30;
    public void initialize() {

        DUtil.Log(Constants.UNPAID_LIST, "initialize");

        orderListView = (ListView) getActivity().findViewById(R.id.listView);
        footerView = getActivity().getLayoutInflater().inflate(R.layout.loadmore_layout, null);
        AQuery bq = new AQuery(footerView);
        bq.id(R.id.btn_loadmore).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // Starting a new async task
                currentPage++;
                initialize();
            }
        });
        if(footerView != null){
            if(orderListView.getFooterViewsCount() == 0){
                orderListView.addFooterView(footerView);
            }
            //orderListView.removeFooterView(footerView);
        }

        showDialog();
        //ORDER_SUMMARY_COUNT
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        //params.put("mode", currentSearchType);
        params.put("page", currentPage);
        params.put("block", blocksize);
        params.put("k", 1);

        NetworkClient.post(Constants.UNPAID_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.UNPAID_LIST, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.UNPAID_LIST, result);

                    JSONObject jsondata = new JSONObject(result);
                    //orderListView = (ListView) getActivity().findViewById(R.id.listView);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data") || jsondata.getString("data") == null) {
                            //
                            if (orderData == null) {
                                orderData = new ArrayList<>();
                            } else {
                                if (orderData.size() == 0 && currentPage == 1) {
                                    orderAdapter = new OrderListViewAdapter(ctx, R.layout.order_history_item, orderData);
                                    orderAdapter.notifyDataSetChanged();
                                    //orderListView.setAdapter(orderAdapter);
                                    aq.id(orderListView).gone();
                                    aq.id(R.id.empty).visible();
                                } else {
                                    Toast.makeText(getActivity(), "더이상 내용이 없습니다", Toast.LENGTH_SHORT).show();
                                    if (footerView != null) {
                                        aq.id(footerView).gone();
                                    }
                                }
                            }

                        } else {

                            if (orderData == null) {
                                orderData = new ArrayList<>();
                            }

                            JSONArray orderInfo = jsondata.getJSONArray("data");

                            for (int i = 0; i < orderInfo.length(); i++) {

                                String orderId = orderInfo.getJSONObject(i).getString("orderId");
                                String orderStatus = orderInfo.getJSONObject(i).getString("orderStatus");
                                String addressSeq = orderInfo.getJSONObject(i).getString("addressSeq");
                                String storeId = orderInfo.getJSONObject(i).getString("storeId");
                                String deliveryPrice = orderInfo.getJSONObject(i).getString("deliveryPrice");
                                String pickupRequestTimeApp = orderInfo.getJSONObject(i).getString("pickupRequestTimeApp");
                                String deliveryRequestTimeApp = orderInfo.getJSONObject(i).getString("deliveryRequestTimeApp");
                                String registerDateApp = orderInfo.getJSONObject(i).getString("registerDateApp");
                                String orderRequest = orderInfo.getJSONObject(i).getString("orderRequest");
                                String orderPickupItemMessage = orderInfo.getJSONObject(i).getString("orderPickupItemMessage");
                                String isAdditionalPayment = null;
                                if(orderInfo.getJSONObject(i).isNull("isAdditionalPayment")){
                                    isAdditionalPayment = null;
                                }else{
                                    isAdditionalPayment = orderInfo.getJSONObject(i).getString("isAdditionalPayment");
                                }
                                if(orderPickupItemMessage.length() > 20){
                                    orderPickupItemMessage = orderPickupItemMessage.substring(0,18) + " ...";
                                }
                                String statusTimeApp = orderInfo.getJSONObject(i).getString("statusTimeApp");
                                String userName = orderInfo.getJSONObject(i).getString("userName");
                                int quantity = orderInfo.getJSONObject(i).getInt("pickupQuantity");
                                String isPayment = orderInfo.getJSONObject(i).getString("isPayment");
                                String payments = null;
                                if(orderInfo.getJSONObject(i).isNull("payments")){
                                    payments = null;
                                }else{
                                    payments = orderInfo.getJSONObject(i).getString("payments");
                                }
                                String isReceivable = orderInfo.getJSONObject(i).getString("isReceivable");

                                OrderListItem aItem = new OrderListItem(orderId, orderStatus, addressSeq, storeId, userName, deliveryPrice, pickupRequestTimeApp,
                                        deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage, orderInfo.getJSONObject(i).toString(), quantity, isPayment, payments, isReceivable, isAdditionalPayment);
                                orderData.add(aItem);

                            }

                            orderListView = (ListView) getActivity().findViewById(R.id.listView);
                            aq.id(orderListView).visible();

                            mMainActivity.orderData = orderData;
                            orderAdapter = new OrderListViewAdapter(ctx, R.layout.order_history_item, orderData);
                            orderAdapter.notifyDataSetChanged();
                            orderListView.setAdapter(orderAdapter);
                            if (currentPage != 1) {
                                int currentPosition = orderListView.getFirstVisiblePosition();
                                orderListView.setSelectionFromTop(currentPosition + 1, 0);
                            }

                            //orderListView.setSelectionFromTop(currentPage*30 + 1, 0);

                            if (orderData.size() > 0) {
                                aq.id(R.id.empty).gone();
                                DUtil.Log("ORDER DATA SIZE", currentPage * blocksize + " ::: " + orderData.size());

                                if (currentPage * blocksize == orderData.size()) {
                                    aq.id(footerView).visible();
                                }else{
                                    if (footerView != null) {
                                        aq.id(footerView).gone();

                                        //orderListView.removeFooterView(footerView);
                                    }
                                }

                            } else {
                                aq.id(R.id.empty).visible();
                            }
                        }

                    } else {
                        DUtil.Log(Constants.UNPAID_LIST, "initialize");
                    }
                    dismissDialog();
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    dismissDialog();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    dismissDialog();
                }
            }
        });
    }
    ProgressDialog mProgressDialog;
    View footerView;

    public void showDialog(){
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog(){
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    ListView orderListView;
    TabLayout tabLayout;

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_item_list, container, false);

        ctx = getActivity();
        aq = new AQuery(getActivity(), view);
        aq.hardwareAccelerated11();

        aq.id(R.id.logo).gone();
        aq.id(R.id.title_text).text("미수금내역").visible();

        aq.id(R.id.top_area).clicked(this, "topmenuClicked");

        orderListView = (ListView) getActivity().findViewById(R.id.listView);
        // Creating a button - Load More

        return view;
    }


    public void topmenuClicked(View button) {
        mMainActivity.getSlidingMenu().toggle();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }


    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }
}
