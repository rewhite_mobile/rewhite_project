package me.rewhite.delivery.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.MainActivity;
import me.rewhite.delivery.adapter.MainListViewAdapter;
import me.rewhite.delivery.adapter.OrderListItem;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.session.Session;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.TimeUtil;


public class MainListFragment extends Fragment implements IFragment {

    private static final String TAG = "MainListFragment";
    AQuery aq;

    private MainActivity mMainActivity = null;
    private MainListViewAdapter orderAdapter;
    private ArrayList<OrderListItem> orderData;
    int currentPage = 1;

    private Context ctx;
    TextView tab01txt;
    TextView tab02txt;
    View footerView;
    boolean isListviewInitialized = false;

    private int currentSelectedTab = 0;
    private View mView;

    private int mPos = -1;
    private boolean isInitialized = false;

    public MainListFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public MainListFragment(int pos) {
        mPos = pos;
    }


    @Override
    public void onResume() {

        Log.d(TAG, "[onResume]");
        mMainActivity.setTouchDisable(false);

        super.onResume();
        try{
            orderListView = (ListView) getActivity().findViewById(R.id.listView);
        }catch (NullPointerException e){

        }


        getCountOrders();

        if(isInitialized){
            if(currentSelectedTab == 0){
                orderData = new ArrayList<>();
                getPickupData("A");
            }else{
                orderData = new ArrayList<>();
                getPickupData("B");
            }
        }else{
            aq.id(R.id.btn_tab01).image(R.mipmap.btn_tab_selected);
            aq.id(R.id.btn_tab02).image(R.mipmap.btn_tab_normal);
            aq.id(R.id.tab_text_01).textColor(Color.parseColor("#343434"));
            aq.id(R.id.tab_text_02).textColor(Color.parseColor("#ffffff"));
            currentSelectedTab = 0;
            currentPage = 1;

            orderData = new ArrayList<>();
            getPickupData("A");
        }

    }

    public void clickBarcode(View button){
        IntentIntegrator integrator = new IntentIntegrator(getActivity());
        integrator.initiateScan();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
             // handle scan result
            Log.e("resukt", scanResult.getContents());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_main_list, container, false);

        mMainActivity = (MainActivity) getActivity();
        ctx = getActivity();
        aq = new AQuery(getActivity(), mView);
        aq.hardwareAccelerated11();
        aq.id(R.id.top_area).clicked(this, "topmenuClicked");
        orderListView = (ListView) getActivity().findViewById(R.id.listView);

        aq.id(R.id.tab_text_01).typeface(CommonUtility.getNanumBarunTypeface()).textColor(Color.parseColor("#343434"));
        aq.id(R.id.tab_text_02).typeface(CommonUtility.getNanumBarunTypeface()).textColor(Color.parseColor("#ffffff"));

        tab01txt = (TextView)mView.findViewById(R.id.tab_text_01);
        tab01txt.setPaintFlags(tab01txt.getPaintFlags() | Paint.FAKE_BOLD_TEXT_FLAG);
        tab02txt = (TextView)mView.findViewById(R.id.tab_text_02);
        tab02txt.setPaintFlags(tab02txt.getPaintFlags() | Paint.FAKE_BOLD_TEXT_FLAG);

        aq.id(R.id.btn_tab01).clicked(this, "tab01Clicked");
        aq.id(R.id.btn_tab02).clicked(this, "tab02Clicked");

        // 바코드스캔 개발중
        aq.id(R.id.btn_barcode).clicked(this, "clickBarcode").gone();

        String storeAddress = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_ADDRESS);
        aq.id(R.id.address_top_title).typeface(CommonUtility.getNanumBarunTypeface()).text(storeAddress);
        //getPickupData("A");

        aq.id(R.id.count_text_01).text(SharedPreferencesUtility.get(SharedPreferencesUtility.OrderCount.Pickup));
        aq.id(R.id.count_text_02).text(SharedPreferencesUtility.get(SharedPreferencesUtility.OrderCount.Delivery));

        String coordinates[] = {SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LATITUDE), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LONGITUDE)};
        double lat = Double.parseDouble(coordinates[0]);
        double lng = Double.parseDouble(coordinates[1]);

        //aq.id(R.id.btn_toggle).clicked(this, "viewTypeToggle");
        aq.id(R.id.conv_layout).gone();

        Log.e("STORE_AVAILABLE_ORDER", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_AVAILABLE_ORDER));
        String storeAvailableOrder = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_AVAILABLE_ORDER);
        if(storeAvailableOrder.length() > 7){
            if( "1".equals(storeAvailableOrder.substring(7,8)) ){
                aq.id(R.id.gs_date_label).text( TimeUtil.convertTimestampToStringDate((new Date()).getTime()) + " 현재").visible();
                getGSCountOrders();
                aq.id(R.id.conv_layout).clicked(this, "gsOrderManageAction").visible();
            }else {
                aq.id(R.id.conv_layout).gone();
            }
        }else{
            aq.id(R.id.conv_layout).gone();
        }

        return mView;
    }

    public void gsOrderManageAction(View button){
        Fragment fragment = new GSOrderStoreListFragment();
        if (getActivity() instanceof MainActivity) {
            MainActivity ra = (MainActivity) getActivity();
            ra.switchContent(fragment);
        }
    }

    public void getGSCountOrders(){
        //SHOW_GS_ORDER_COUNT
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("partnerId", 7);
        params.put("k", 1);
        NetworkClient.post(Constants.SHOW_GS_ORDER_COUNT, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SHOW_GS_ORDER_COUNT, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.SHOW_GS_ORDER_COUNT, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //
                            aq.id(R.id.text_gs_pickup_count).text(0 + "건");
                            aq.id(R.id.text_gs_delivery_count).text(0 + "건");
                        } else {
                            int pickupCount = 0;
                            int deliveryCount = 0;

                            JSONArray orderData = jsondata.getJSONArray("data");
                            for(int i = 0 ; i < orderData.length(); i++){
                                if("01".equals(orderData.getJSONObject(i).getString("orderStatus"))){
                                    pickupCount += orderData.getJSONObject(i).getInt("orderStatusCount");
                                }else if("21".equals(orderData.getJSONObject(i).getString("orderStatus"))){
                                    deliveryCount += orderData.getJSONObject(i).getInt("orderStatusCount");
                                }else{

                                }

                            }

                            aq.id(R.id.text_gs_pickup_count).text(pickupCount + "건");
                            aq.id(R.id.text_gs_delivery_count).text(deliveryCount + "건");
                        }

                    } else {
                        aq.id(R.id.text_gs_pickup_count).text(0 + "건");
                        aq.id(R.id.text_gs_delivery_count).text(0 + "건");
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void viewTypeToggle(View button){
        mMainActivity.mainViewTypeToggle();
    }

    public void getCountOrders() {
        //ORDER_SUMMARY_COUNT
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_SUMMARY_COUNT, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_SUMMARY_COUNT, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_SUMMARY_COUNT, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //

                        } else {
                            int a = jsondata.getJSONObject("data").getInt("A");
                            int b = jsondata.getJSONObject("data").getInt("B");

                            SharedPreferencesUtility.set(SharedPreferencesUtility.OrderCount.Pickup, a + "");
                            SharedPreferencesUtility.set(SharedPreferencesUtility.OrderCount.Delivery, b + "");

                            aq.id(R.id.count_text_01).text(SharedPreferencesUtility.get(SharedPreferencesUtility.OrderCount.Pickup));
                            aq.id(R.id.count_text_02).text(SharedPreferencesUtility.get(SharedPreferencesUtility.OrderCount.Delivery));
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }


    public void topmenuClicked(View button) {
        mMainActivity.getSlidingMenu().toggle();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }

    public void tab01Clicked(View button) {
        aq.id(R.id.btn_tab01).image(R.mipmap.btn_tab_selected);
        aq.id(R.id.btn_tab02).image(R.mipmap.btn_tab_normal);
        aq.id(R.id.tab_text_01).textColor(Color.parseColor("#343434"));
        aq.id(R.id.tab_text_02).textColor(Color.parseColor("#ffffff"));
        currentSelectedTab = 0;
        currentPage = 1;

        orderData = new ArrayList<>();
        getPickupData("A");

    }

    public void tab02Clicked(View button) {

        aq.id(R.id.btn_tab01).image(R.mipmap.btn_tab_normal);
        aq.id(R.id.btn_tab02).image(R.mipmap.btn_tab_selected);
        aq.id(R.id.tab_text_01).textColor(Color.parseColor("#ffffff"));
        aq.id(R.id.tab_text_02).textColor(Color.parseColor("#343434"));
        currentSelectedTab = 1;
        currentPage = 1;

        orderData = new ArrayList<>();
        getPickupData("B");

    }

    ListView orderListView;
    //--------------------------------------------------------------------------------------
    // IFragment Implement
    //--------------------------------------------------------------------------------------
    private synchronized void getPickupData(final String orderType) {
        //showDialog();
        if ("A".equals(orderType)) {
            currentSelectedTab = 0;
        } else {
            currentSelectedTab = 1;
        }
        //initialize(currentSelectedTab);

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("mode", orderType);
        params.put("page", 1);
        params.put("block", 200);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_LIST, error.getMessage());
                //dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {

                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_LIST, result);

                    JSONObject jsondata = new JSONObject(result);
                    if ("S0000".equals(jsondata.getString("resultCode"))) {

                        isInitialized = true;

                        if (jsondata.isNull("data")) {
                            // 주문내역이 없음
                            orderData = new ArrayList<>();
                            orderAdapter = new MainListViewAdapter(ctx, R.layout.main_list_item_v2, orderData);
                            orderAdapter.notifyDataSetChanged();
                            orderListView.setAdapter(orderAdapter);
                            
                            aq.id(R.id.empty).visible();

                        } else {

                            orderListView = aq.id(R.id.listView).getListView();// (ListView)findViewById(R.id.listView);
                            if (orderData == null) {
                                orderData = new ArrayList<>();
                            }

                            JSONArray orderInfo = jsondata.getJSONArray("data");
                            if (orderInfo.length() > 0) {

                                for (int i = 0; i < orderInfo.length(); i++) {

                                    String orderId = orderInfo.getJSONObject(i).getString("orderId");
                                    String orderStatus = orderInfo.getJSONObject(i).getString("orderStatus");
                                    String addressSeq = orderInfo.getJSONObject(i).getString("addressSeq");
                                    String storeId = orderInfo.getJSONObject(i).getString("storeId");
                                    String deliveryPrice = orderInfo.getJSONObject(i).getString("deliveryPrice");
                                    String pickupRequestTimeApp = orderInfo.getJSONObject(i).getString("pickupRequestTimeApp");
                                    String deliveryRequestTimeApp = orderInfo.getJSONObject(i).getString("deliveryRequestTimeApp");
                                    String registerDateApp = orderInfo.getJSONObject(i).getString("registerDateApp");
                                    String orderRequest = orderInfo.getJSONObject(i).getString("orderRequest");
                                    String orderPickupItemMessage = orderInfo.getJSONObject(i).getString("orderPickupItemMessage");
                                    String statusTimeApp = orderInfo.getJSONObject(i).getString("statusTimeApp");
                                    String userName = orderInfo.getJSONObject(i).getString("userName");
                                    int quantity = orderInfo.getJSONObject(i).getInt("pickupQuantity");
                                    String isPayment = orderInfo.getJSONObject(i).getString("isPayment");
                                    String isReceivable = orderInfo.getJSONObject(i).getString("isReceivable");

                                    String isAdditionalPayment = null;
                                    if(orderInfo.getJSONObject(i).isNull("isAdditionalPayment")){
                                        isAdditionalPayment = null;
                                    }else{
                                        isAdditionalPayment = orderInfo.getJSONObject(i).getString("isAdditionalPayment");
                                    }

                                    String payments = null;
                                    if(orderInfo.getJSONObject(i).isNull("payments")){
                                        payments = null;
                                    }else{
                                        payments = orderInfo.getJSONObject(i).getJSONArray("payments").toString();
                                    }

                                    if ("A".equals(orderType)) {
                                        isPayment = "A";
                                        OrderListItem aItem = new OrderListItem(orderId, orderStatus, addressSeq, storeId, userName, deliveryPrice, pickupRequestTimeApp,
                                                deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage, orderInfo.getJSONObject(i).toString(), quantity, isPayment, payments, isReceivable, isAdditionalPayment);
                                        orderData.add(aItem);
                                    }else{
                                        if("22".equals(orderStatus)){
                                            if("Y".equals(isReceivable)){

                                            }else{
                                                OrderListItem aItem = new OrderListItem(orderId, orderStatus, addressSeq, storeId, userName, deliveryPrice, pickupRequestTimeApp,
                                                        deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage, orderInfo.getJSONObject(i).toString(), quantity, isPayment, payments, isReceivable, isAdditionalPayment);
                                                orderData.add(aItem);
                                            }
                                        }else{
                                            OrderListItem aItem = new OrderListItem(orderId, orderStatus, addressSeq, storeId, userName, deliveryPrice, pickupRequestTimeApp,
                                                    deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage, orderInfo.getJSONObject(i).toString(), quantity, isPayment, payments, isReceivable, isAdditionalPayment);
                                            orderData.add(aItem);
                                        }

                                    }
                                }

                                if (orderData.size() > 0) {
                                    if ("A".equals(orderType)) {
                                        SharedPreferencesUtility.set(SharedPreferencesUtility.OrderCount.Pickup, orderData.size() + "");
                                        aq.id(R.id.count_text_01).text(SharedPreferencesUtility.get(SharedPreferencesUtility.OrderCount.Pickup));
                                    }else{
                                        SharedPreferencesUtility.set(SharedPreferencesUtility.OrderCount.Delivery, orderData.size() + "");
                                        aq.id(R.id.count_text_02).text(SharedPreferencesUtility.get(SharedPreferencesUtility.OrderCount.Delivery));
                                    }

                                    aq.id(R.id.empty).gone();
                                }else{
                                    if ("A".equals(orderType)) {
                                        SharedPreferencesUtility.set(SharedPreferencesUtility.OrderCount.Pickup, "0");
                                        aq.id(R.id.count_text_01).text("0");
                                    }else{
                                        SharedPreferencesUtility.set(SharedPreferencesUtility.OrderCount.Delivery, "0");
                                        aq.id(R.id.count_text_02).text("0");
                                    }
                                }
                                mMainActivity.orderData = orderData;
                                orderAdapter = new MainListViewAdapter(ctx, R.layout.main_list_item_v2, orderData);
                                orderAdapter.notifyDataSetChanged();

                                orderListView.setAdapter(orderAdapter);
                                if (currentPage != 1) {
                                    int currentPosition = orderListView.getFirstVisiblePosition();
                                    orderListView.setSelectionFromTop(currentPosition + 1, 0);
                                }

                            } else {
                                orderData = new ArrayList<>();
                                orderAdapter = new MainListViewAdapter(ctx, R.layout.main_list_item_v2, orderData);
                                orderAdapter.notifyDataSetChanged();
                                orderListView.setAdapter(orderAdapter);

                                aq.id(R.id.empty).visible();
                            }

                        }

                        //dismissDialog();
                    } else if ("S9002".equals(jsondata.getString("resultCode"))) {
                        DUtil.alertShow(getActivity(), "로그인상태가 정상적이지 않습니다. 다시 로그인 부탁드립니다.");
                        Session.getCurrentSession().close();
                        //dismissDialog();
                    } else {
                        DUtil.alertShow(getActivity(), jsondata.getString("message"));
                        Session.getCurrentSession().close();
                        //dismissDialog();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    //dismissDialog();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    //dismissDialog();
                }
            }
        });
    }

    ProgressDialog mProgressDialog;
    public void showDialog(){
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog(){
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

}
