package me.rewhite.delivery.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.delivery.Bootstrap;
import me.rewhite.delivery.Bootstrap.TrackerName;
import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.SignActivity;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.ValidateUtil;

//import com.actionbarsherlock.app.SherlockFragment;


public class SignMainBottomFragment extends Fragment {

	private static final String TAG = "SignMainBottomFragment";
	private static final String KEY_CONTENT = "SignMainBottomFragment";
	private String mContent = "???";
	private SignActivity mSignActivity = null;
	private ViewGroup mView;
	private AQuery aq;
	private TextView input_email_text;
	private TextView input_password_text;

	final int DRAWABLE_LEFT = 0;
	final int DRAWABLE_TOP = 1;
	final int DRAWABLE_RIGHT = 2;
	final int DRAWABLE_BOTTOM = 3;

	public static SignMainBottomFragment newInstance(String content) {

		SignMainBottomFragment fragment = new SignMainBottomFragment();
		Bundle bdl = new Bundle();
		bdl.putString(KEY_CONTENT, content);
		fragment.setArguments(bdl);

		return fragment;
	}

	public void inputClearClicked(View button) {
		ImageButton v = (ImageButton) button;
		switch ((int) v.getTag()) {
			case 0 :
				aq.id(R.id.input_email_text).text("");
				break;
			case 1 :
				aq.id(R.id.input_password_text).text("");
				break;
			default :
				break;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
			mContent = savedInstanceState.getString(KEY_CONTENT);
		}
		
		Tracker t = ((Bootstrap) getActivity().getApplication()).getTracker(TrackerName.APP_TRACKER);
		// Set screen name.
		t.setScreenName(TAG);
		// Send a screen view.
		t.send(new HitBuilders.ScreenViewBuilder().build());
		
		String message = getArguments().getString(KEY_CONTENT);
		mSignActivity = (SignActivity) getActivity();
		mView = (ViewGroup) inflater.inflate(R.layout.fragment_signmain_bottom, container, false);
		aq = new AQuery(mSignActivity, mView);
		aq.id(R.id.btn_login).clicked(this, "loginClicked");

		aq.id(R.id.btn_clear_01).tag(0).clicked(this, "inputClearClicked");
		aq.id(R.id.btn_clear_02).tag(1).clicked(this, "inputClearClicked");
		
		aq.id(R.id.btn_find_password).clicked(this, "findPasswordClicked");
		aq.id(R.id.layout_arrow).clicked(this, "transitionLoginClicked");

		input_email_text = (TextView) mView.findViewById(R.id.input_email_text);
		input_password_text = (TextView) mView.findViewById(R.id.input_password_text);
		/*
		 * input_email_text.setOnTouchListener(new OnTouchListener() {
		 * @Override
		 * public boolean onTouch(View v, MotionEvent event) {
		 * if (event.getAction() == MotionEvent.ACTION_UP) {
		 * if (event.getRawX() >= (input_email_text.getRight() -
		 * input_email_text.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
		 * // your action here
		 * input_email_text.setText("");
		 * return true;
		 * }
		 * }
		 * return false;
		 * }
		 * });
		 * input_password_text.setOnTouchListener(new OnTouchListener() {
		 * @Override
		 * public boolean onTouch(View v, MotionEvent event) {
		 * if (event.getAction() == MotionEvent.ACTION_UP) {
		 * if (event.getRawX() >= (input_password_text.getRight() -
		 * input_password_text.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds()
		 * .width())) {
		 * // your action here
		 * input_password_text.setText("");
		 * return true;
		 * }
		 * }
		 * return false;
		 * }
		 * });
		 */
		input_email_text.setOnFocusChangeListener(new OnFocusChangeListener() {

			public void onFocusChange(View view, boolean hasfocus) {
				int img2 = R.mipmap.set_whitebg;
				if (hasfocus) {
					int img = R.mipmap.login_main_icn_email_able;
					
					input_email_text.setCompoundDrawablesWithIntrinsicBounds(img, 0, img2, 0);

				} else {
					int img = R.mipmap.login_main_icn_email_disable;
					input_email_text.setCompoundDrawablesWithIntrinsicBounds(img, 0, img2, 0);
				}
			}
		});
		input_password_text.setOnFocusChangeListener(new OnFocusChangeListener() {

			public void onFocusChange(View view, boolean hasfocus) {
				int img2 = R.mipmap.set_whitebg;
				if (hasfocus) {
					int img = R.mipmap.login_main_icn_lock_able;
					input_password_text.setCompoundDrawablesWithIntrinsicBounds(img, 0, img2, 0);
				} else {
					int img = R.mipmap.login_main_icn_lock_disable;
					input_password_text.setCompoundDrawablesWithIntrinsicBounds(img, 0, img2, 0);
				}
			}
		});

		return mView;
	}
	
	public void transitionLoginClicked(View button){
		Log.d(TAG, "[transitionLoginClicked] ");
		//mSignActivity.buttonTransitionClicked(false);
	}
 
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(KEY_CONTENT, mContent);
	}
	
	public void findPasswordClicked(View button){
		
		Log.i(TAG, "[findPasswordClicked] ");
		/*
		Intent intent = new Intent();
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.setClass(mSignActivity, FindActivity.class);

		mSignActivity.startActivity(intent);*/
	}

	public void loginClicked(View button) {
		Log.d(TAG, "[loginClicked] ");
		
		if(mSignActivity.isLoginProcessing){
			return;
		}

		String emailAddressInput = aq.id(R.id.input_email_text).getText().toString();
		String passwordInput = aq.id(R.id.input_password_text).getText().toString();

		if (ValidateUtil.checkEmail(emailAddressInput)) {
			if (ValidateUtil.validPassword(passwordInput) == 1) {
				mSignActivity.requestSessionOpen(getActivity(), emailAddressInput, passwordInput,
						SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));
			} else {
				DUtil.alertShow(mSignActivity, "사용할수 없는 비밀번호입니다.");
				//DialogUtility.showOneButtonAlertDialog(getActivity(), null, "사용할수 없는 비밀번호입니다.", "확인", null);
			}
		} else {
			DUtil.alertShow(mSignActivity, "이메일형식이 유효하지않습니다.");
			//DialogUtility.showOneButtonAlertDialog(getActivity(), null, "이메일형식이 유효하지않습니다.", "확인", null);
		}

	}

}
