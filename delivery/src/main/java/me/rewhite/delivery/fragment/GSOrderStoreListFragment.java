package me.rewhite.delivery.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.kakao.kakaonavi.KakaoNaviParams;
import com.kakao.kakaonavi.KakaoNaviService;
import com.kakao.kakaonavi.Location;
import com.kakao.kakaonavi.NaviOptions;
import com.kakao.kakaonavi.options.CoordType;
import com.kakao.kakaonavi.options.RpOption;
import com.kakao.kakaonavi.options.VehicleType;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.GSOrderListActivity;
import me.rewhite.delivery.activity.MainActivity;
import me.rewhite.delivery.adapter.GSStoreListItem;
import me.rewhite.delivery.adapter.StoreListViewAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;


public class GSOrderStoreListFragment extends Fragment implements IFragment{

    private static final String TAG = "GSOrderStoreListFragment";
    AQuery aq;
    private MainActivity mMainActivity = null;
    private Context ctx;
    private StoreListViewAdapter storeAdapter;
    private ArrayList<GSStoreListItem> storeData;

    String partnerStoreAddress;
    int partnerStoreVisitHour;
    long partnerStoreLongitude, partnerStoreLatitude;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public GSOrderStoreListFragment() {
    }

    @Override
    public void onResume() {
        mMainActivity.setTouchDisable(false);
        super.onResume();
        initialize();
    }

    String currentSearchType = "F";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mMainActivity = (MainActivity) getActivity();
        super.onCreate(savedInstanceState);

        Log.w("Activity", TAG);
    }
    int blocksize = 100;

    public void initialize() {

        //Log.e(Constants.SHOW_GS_ORDER_COUNT, "initialize");

        //storeListView = (ListView) getActivity().findViewById(R.id.listView);

        showDialog();
        //ORDER_SUMMARY_COUNT
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        //params.put("partnerId", 7);
        params.put("k", 1);
        NetworkClient.post(Constants.SHOW_GS_ORDER_COUNT, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SHOW_GS_ORDER_COUNT, error.getMessage());
                dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    Log.e(Constants.SHOW_GS_ORDER_COUNT, result);

                    JSONObject jsondata = new JSONObject(result);
                    //orderListView = (ListView) getActivity().findViewById(R.id.listView);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //
                            Log.e("jsondata", "null");
                            storeData = new ArrayList<>();

                        } else {
                            Log.e("jsondata", "not null");
                            storeData = new ArrayList<>();

                            JSONArray storeInfo = jsondata.getJSONArray("data");

                            String partnerId = "";
                            String partnerStoreId = "";
                            String partnerStoreName = "";
                            ArrayList<String> storeArray = new ArrayList<>();

                            for (int i = 0; i < storeInfo.length(); i++) {

                                partnerStoreId = storeInfo.getJSONObject(i).getString("partnerStoreId");

                                if(storeArray.size() == 0){
                                    Log.e("storeArray", "add");
                                    storeArray.add(partnerStoreId);

                                }else{
                                    boolean isExisted = false;
                                    for(int j = 0 ; j < storeArray.size(); j++){
                                        if(partnerStoreId.equals(storeArray.get(j))){
                                            Log.e("storeArray", "through");
                                            isExisted = true;
                                        }else{
                                            isExisted = false;
                                            Log.e("storeArray", "add");
                                        }
                                    }
                                    if(!isExisted){
                                        storeArray.add(partnerStoreId);
                                    }
                                }
                            }



                            for(int i = 0 ; i < storeArray.size(); i++){
                                int pickupCount = 0;
                                int deliveryCount = 0;
                                int returnReqOrderCount = 0;
                                int storedBoxCount = 0;

                                String pStoreId = "";
                                String pId = "";
                                String pStoreName = "";
                                String isNewPOS = "N";
                                double pStoreLongitude = -1;
                                double pStoreLatitude = -1;
                                int pStoreVisitHour = -1;
                                String pStoreAddress = "";

                                for (int j = 0; j < storeInfo.length(); j++) {

                                    String comparePartnerStoreId = storeInfo.getJSONObject(j).getString("partnerStoreId");

                                    if(comparePartnerStoreId.equals(storeArray.get(i))){
                                        pStoreId = storeInfo.getJSONObject(j).getString("partnerStoreId");
                                        pId = storeInfo.getJSONObject(j).getString("partnerId");
                                        pStoreName = storeInfo.getJSONObject(j).getString("partnerStoreName");
                                        if(storeInfo.getJSONObject(j).isNull("isNewPOS")){
                                            isNewPOS = "N";
                                        }else{
                                            isNewPOS = storeInfo.getJSONObject(j).getString("isNewPOS");
                                        }

                                        pStoreLongitude = storeInfo.getJSONObject(j).getDouble("longitude");
                                        pStoreLatitude = storeInfo.getJSONObject(j).getDouble("latitude");
                                        pStoreVisitHour = storeInfo.getJSONObject(j).getInt("visitHour");
                                        pStoreAddress = storeInfo.getJSONObject(j).getString("partnerStoreAddress1") + " " + storeInfo.getJSONObject(j).getString("partnerStoreAddress2");

                                        if("01".equals(storeInfo.getJSONObject(j).getString("orderStatus"))){
                                            pickupCount += storeInfo.getJSONObject(j).getInt("orderStatusCount");
                                        }else if("21".equals(storeInfo.getJSONObject(j).getString("orderStatus"))){
                                            deliveryCount += storeInfo.getJSONObject(j).getInt("orderStatusCount");
                                        }else if("81".equals(storeInfo.getJSONObject(j).getString("orderStatus"))){
                                            returnReqOrderCount += storeInfo.getJSONObject(j).getInt("orderStatusCount");
                                        }else if("82".equals(storeInfo.getJSONObject(j).getString("orderStatus"))){
                                            storedBoxCount += storeInfo.getJSONObject(j).getInt("orderStatusCount");
                                        }else{

                                        }
                                    }
                                }

                                GSStoreListItem aItem = new GSStoreListItem(pStoreId, pId, pStoreName, pickupCount, deliveryCount, pStoreAddress, pStoreVisitHour, pStoreLongitude, pStoreLatitude, returnReqOrderCount, storedBoxCount, isNewPOS);
                                storeData.add(aItem);

                            }

                            storeAdapter = new StoreListViewAdapter(storeData, new StoreListViewAdapter.MyAdapterListener() {

                                @Override
                                public void naviButtonViewOnClick(View v, final int position) {
                                    Log.e("naviButtonViewOnClick", " at position " + position);
                                    if ( Build.VERSION.SDK_INT >= 23){
                                        if (!mMainActivity.canAccessLocation()) {
                                            requestPermissions(mMainActivity.SMS_PERMS, mMainActivity.SMS_REQUEST);
                                        }else{

                                            // KAKAO
                                            if(KakaoNaviService.getInstance().isKakaoNaviInstalled(ctx)){
                                                Location destination = Location.newBuilder(storeData.get(position).getPartnerStoreName(), storeData.get(position).getPartnerStoreLongitude(), storeData.get(position).getPartnerStoreLatitude()).build();
                                                NaviOptions options = NaviOptions.newBuilder().setCoordType(CoordType.WGS84).setVehicleType(VehicleType.FIRST).setRpOption(RpOption.FAST).build();

                                                KakaoNaviParams.Builder builder = KakaoNaviParams.newBuilder(destination)
                                                        .setNaviOptions(options);

                                                KakaoNaviService.getInstance().navigate(getActivity(), builder.build());
                                            }else{
                                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
                                                alertDialogBuilder.setCancelable(true).setMessage("카카오네비가 설치되어있지 않습니다. 설치하러 이동하시겠습니까?")
                                                        .setPositiveButton("설치하러 이동", new DialogInterface.OnClickListener() {

                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                String name="https://play.google.com/store/apps/details?id=com.locnall.KimGiSa";
                                                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(name));
                                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                                intent.setPackage("com.android.vending");
                                                                startActivity(intent);
                                                            }

                                                        }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                    }
                                                });
                                                AlertDialog alertDialog = alertDialogBuilder.create();
                                                alertDialog.show();
                                            }
                                        }
                                    }else{

                                        // KAKAO
                                        if(KakaoNaviService.getInstance().isKakaoNaviInstalled(ctx)){
                                            Location destination = Location.newBuilder(storeData.get(position).getPartnerStoreName(), storeData.get(position).getPartnerStoreLongitude(), storeData.get(position).getPartnerStoreLatitude()).build();
                                            NaviOptions options = NaviOptions.newBuilder().setCoordType(CoordType.WGS84).setVehicleType(VehicleType.FIRST).setRpOption(RpOption.FAST).build();

                                            KakaoNaviParams.Builder builder = KakaoNaviParams.newBuilder(destination)
                                                    .setNaviOptions(options);

                                            KakaoNaviService.getInstance().shareDestination(getActivity(), builder.build());
                                        }else{
                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
                                            alertDialogBuilder.setCancelable(true).setMessage("카카오네비가 설치되어있지 않습니다. 설치하러 이동하시겠습니까?")
                                                    .setPositiveButton("설치하러 이동", new DialogInterface.OnClickListener() {

                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            String name="https://play.google.com/store/apps/details?id=com.locnall.KimGiSa";
                                                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(name));
                                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            intent.setPackage("com.android.vending");
                                                            startActivity(intent);
                                                        }

                                                    }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                }
                                            });
                                            AlertDialog alertDialog = alertDialogBuilder.create();
                                            alertDialog.show();
                                        }
                                    }
                                }

                                @Override
                                public void orderViewOnClick(View v, int position) {
                                    Log.e("orderViewOnClick", " at position "+position);
                                    Intent intent = new Intent();

                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    intent.setClass(getActivity(), GSOrderListActivity.class);
                                    intent.putExtra("partnerStoreId", storeData.get(position).getPartnerStoreId());
                                    intent.putExtra("partnerId", storeData.get(position).getPartnerId());
                                    intent.putExtra("partnerStoreName", storeData.get(position).getPartnerStoreName());
                                    intent.putExtra("isNewPOS", storeData.get(position).getIsNewPOS());
                                    startActivity(intent);
                                    getActivity().overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                                }
                            });

                            RecyclerView myView =  (RecyclerView)rootView.findViewById(R.id.listView);
                            myView.setHasFixedSize(true);
                            myView.setAdapter(storeAdapter);
                            LinearLayoutManager llm = new LinearLayoutManager(ctx);
                            llm.setOrientation(LinearLayoutManager.VERTICAL);
                            myView.setLayoutManager(llm);
//                            myView.addOnItemTouchListener(
//                                    new RecyclerItemClickListener(ctx, myView, new RecyclerItemClickListener.OnItemClickListener() {
//                                        @Override
//                                        public void onItemClick(View view, int position) {
//
//                                            Intent intent = new Intent();
//
//                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                                            intent.setClass(getActivity(), GSOrderListActivity.class);
//                                            intent.putExtra("partnerStoreId", storeData.get(position).getPartnerStoreId());
//                                            intent.putExtra("partnerId", storeData.get(position).getPartnerId());
//                                            intent.putExtra("partnerStoreName", storeData.get(position).getPartnerStoreName());
//                                            startActivity(intent);
//                                            getActivity().overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
//                                        }
//
//                                        @Override
//                                        public void onLongItemClick(View view, int position) {
//                                            // do whatever
//                                        }
//                                    })
//                            );

                            if (storeData.size() > 0) {
                                aq.id(R.id.empty).gone();
                                DUtil.Log("ORDER DATA SIZE", 1 * blocksize + " ::: " + storeData.size());

                            } else {
                                aq.id(R.id.empty).visible();
                            }
                        }

                    } else {
                        DUtil.Log(Constants.ORDER_LIST, "initialize");
                    }
                    dismissDialog();
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    dismissDialog();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    dismissDialog();
                }
            }
        });
    }
    ProgressDialog mProgressDialog;

    public void showDialog(){
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog(){
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    View rootView;

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_gs_store_list, container, false);

        ctx = getActivity();
        aq = new AQuery(rootView);
        aq.hardwareAccelerated11();

        aq.id(R.id.logo).gone();
        aq.id(R.id.title_text).text("리화이트 플레이스 주문").visible();

        aq.id(R.id.top_area).clicked(this, "topmenuClicked");
        //aq.id(R.id.listView);
        //storeListView = (ListView) getActivity().findViewById(R.id.listView);
        // Creating a button - Load More
        initialize();

        return rootView;
    }


    public void topmenuClicked(View button) {
        mMainActivity.getSlidingMenu().toggle();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }


    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }
}
