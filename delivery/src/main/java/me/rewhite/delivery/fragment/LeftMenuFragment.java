package me.rewhite.delivery.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.MainActivity;
import me.rewhite.delivery.activity.UpgradeActivity;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.StringUtil;

//import com.pkmmte.view.CircularImageView;

/**
 * Fragment used for managing interactions for and presentation of a navigation
 * drawer. See the <a href=
 * "https://developer.android.com/design/patterns/navigation-drawer.html#Interaction"
 * > design guidelines</a> for a complete explanation of the behaviors
 * implemented here.
 */
public class LeftMenuFragment extends Fragment {

	public final static String ITEM_TITLE = "title";
	public final static String ITEM_ICON = "icon";
	SharedPreferences preferences;
	private AQuery aq;
	private Handler mHandler;
	private float ratio;

	public LeftMenuFragment() {
	}

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }


    @Override
    public void onResume() {
        super.onResume();

        Log.e("onresume", "resume");

        if (aq != null) {
            initialize();
        }else{
            aq = new AQuery(getActivity());
            aq.hardwareAccelerated11();

            initialize();
        }
    }

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Indicate that this fragment would like to influence the set of
		// actions in the action bar.
		setHasOptionsMenu(true);

		preferences = getActivity().getSharedPreferences(Constants.PREF_NAME,
				Context.MODE_PRIVATE);

		aq = new AQuery(getActivity());
		// Enable hardware acceleration if the device has API 11 or above
		aq.hardwareAccelerated11();

        initialize();
    }

    public void initialize() {
        ImageOptions options = new ImageOptions();
        options.round = 90;
        options.ratio = 1.0f;
        options.memCache = true;
        options.targetWidth = 90;
        options.animation = AQuery.FADE_IN_NETWORK;

        String pImage = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_IMAGE);
        if (!StringUtil.isNullOrEmpty(pImage)) {
            aq.id(R.id.profile_image).image(pImage, options);
        }

        String storeName = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_NAME);
        String storeAddress = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_ADDRESS);
        String storeIdentifier = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_ID);

        aq.id(R.id.profile_name)
                .typeface(CommonUtility.getNanumBarunTypeface()).text(storeName);
        aq.id(R.id.profile_location_name).typeface(
                CommonUtility.getNanumBarunLightTypeface()).text(storeIdentifier);

        aq.id(R.id.menu_title_01).typeface(
                CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.menu_title_02).typeface(
                CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.menu_title_03).typeface(
                CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.menu_title_04).typeface(
                CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.menu_title_05).typeface(
                CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.menu_title_06).typeface(
                CommonUtility.getNanumBarunTypeface());

        aq.id(R.id.btn_cs_call).clicked(this, "csCallAction");

        aq.id(R.id.menu_01).clicked(this, "menuClick").tag(1);
        aq.id(R.id.menu_02).clicked(this, "menuClick").tag(2);
        aq.id(R.id.menu_03).clicked(this, "menuClick").tag(3);
        aq.id(R.id.menu_04).clicked(this, "menuClick").tag(4);
        aq.id(R.id.menu_05).clicked(this, "menuClick").tag(5);
        aq.id(R.id.menu_06).clicked(this, "menuClick").tag(6);

        aq.id(R.id.menu_00).gone();
        aq.id(R.id.line_00).gone();

        Log.e("STORE_AVAILABLE_ORDER", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_AVAILABLE_ORDER));
        String storeAvailableOrder = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_AVAILABLE_ORDER);
        if(storeAvailableOrder.length() > 7){
            if( "1".equals(storeAvailableOrder.substring(7,8)) ){
                aq.id(R.id.menu_00).clicked(this, "menuClick").tag(0).visible();
                aq.id(R.id.menu_title_00).typeface(CommonUtility.getNanumBarunTypeface()).visible();
                aq.id(R.id.line_00).visible();
            }else{
                aq.id(R.id.menu_00).gone();
                aq.id(R.id.line_00).gone();
            }
        }else{
            aq.id(R.id.menu_00).gone();
            aq.id(R.id.line_00).gone();
        }

        PackageManager m = getActivity().getPackageManager();
        // 설치된 패키지의 버전이름 추출 : 1.0.0 스타일
        String app_ver = "";
        try {
            app_ver = m.getPackageInfo(getActivity().getPackageName(), 0).versionName;
            aq.id(R.id.version_text).text("버전정보 v" + app_ver);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //ORDER_SUMMARY_COUNT
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_SUMMARY_COUNT, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_SUMMARY_COUNT, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_SUMMARY_COUNT, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //

                        } else {
                            int a = jsondata.getJSONObject("data").getInt("A");
                            int b = jsondata.getJSONObject("data").getInt("B");

                            SharedPreferencesUtility.set(SharedPreferencesUtility.OrderCount.Pickup, a+"");
                            SharedPreferencesUtility.set(SharedPreferencesUtility.OrderCount.Delivery, b+"");

                            //aq.id(R.id.count_elem_2).text(SharedPreferencesUtility.get(SharedPreferencesUtility.OrderCount.Pickup));
                            //aq.id(R.id.count_elem_3).text(SharedPreferencesUtility.get(SharedPreferencesUtility.OrderCount.Delivery));
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

	public void upgradeClicked(View button){
        if (getActivity() == null) {
            return;
        }

        if (getActivity() instanceof MainActivity) {
            MainActivity ra = (MainActivity) getActivity();
            Intent intent = new Intent(ra,
                    UpgradeActivity.class);
            intent.putExtra("TERMS_URI", Constants.HTML_NOTICE);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            ra.startActivity(intent);
        }
	}

	public void menuClick(View button){
		int tag = (int)button.getTag();
		Fragment fragment;

		switch(tag){
            case 0:
                fragment = new GSOrderStoreListFragment();
                break;
			case 1:
				fragment = new MainListFragment();
				break;
			case 2:
				fragment = new SearchFragment();
				break;
			case 3:
                // 전체주문내역
				fragment = new OrderAllListFragment();
				break;
            case 4:
                // 미수금내역
                fragment = new OrderUnpaidItemListFragment();
                break;
            case 5:
                // 배송완료내역
                fragment = new OrderCompletedListFragment();
                break;
            case 6:
                fragment = new SettingsFragment();
                break;
			default:
				fragment = new MainListFragment();
				break;
		}

        /*
        if (getActivity() instanceof MainActivity) {
            MainActivity ra = (MainActivity) getActivity();
            if (tag == 0) {
                ra.getSlidingMenu().setSlidingEnabled(false);
                ra.getSlidingMenu()
                        .setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
            } else {
                ra.getSlidingMenu().setSlidingEnabled(true);
                ra.getSlidingMenu()
                        .setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
            }
        }
*/


		switchFragment(fragment);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.fragment_navigation_drawer, null);
	}

	// the meat of switching the above fragment
	private void switchFragment(Fragment fragment) {
		if (getActivity() == null) {
			return;
		}

		if (getActivity() instanceof MainActivity) {
			MainActivity ra = (MainActivity) getActivity();
			ra.switchContent(fragment);
		}
	}

    public void csCallAction(View button) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setCancelable(true).setMessage("리화이트 고객센터와 전화통화를 원하십니까?")
                .setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        call();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void call() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        //callIntent.setAction("android.intent.action.DIAL");
        callIntent.setData(Uri.parse("tel:027072429"));
        if(callIntent.resolveActivity(getActivity().getPackageManager()) != null){
            startActivity(callIntent);
        }
    }

}
