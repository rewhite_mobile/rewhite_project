package me.rewhite.delivery.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.MainActivity;
import me.rewhite.delivery.adapter.OrderListItem;
import me.rewhite.delivery.adapter.OrderListViewAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;


public class SearchFragment extends Fragment implements IFragment{

    private static final String TAG = "ItemListFragment";
    AQuery aq;
    private MainActivity mMainActivity = null;
    private Context ctx;
    private OrderListViewAdapter orderAdapter;
    private ArrayList<OrderListItem> orderData;

    String currentSearchType = "phone";
    ListView orderListView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SearchFragment() {
    }



    @Override
    public void onResume() {

        Log.d(TAG, "[onResume]");
        mMainActivity.setTouchDisable(false);
        super.onResume();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mMainActivity = (MainActivity) getActivity();
        super.onCreate(savedInstanceState);
    }

    public void searchAction(View button){
        if("phone".equals(currentSearchType)){
            String searchString = aq.id(R.id.text_search).getText().toString();
            if(searchString.length() > 3){
                // 전화번호 뒷자리 4
            }
            search(aq.id(R.id.text_search).getText().toString());
        }else{
            search(aq.id(R.id.text_search).getText().toString());
        }

    }

    public void search(String _search) {
        //ORDER_SUMMARY_COUNT
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("search", currentSearchType);
        params.put("mode", "Z");
        params.put("keyword", _search);
        params.put("page", 1);
        params.put("block", 50);
        params.put("k", 1);
        Log.e("search Req", params.toString());

        hideKeyboard();

        NetworkClient.post(Constants.ORDER_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_LIST, error.getMessage());
                aq.id(R.id.result_area).gone();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_LIST, result);

                    JSONObject jsondata = new JSONObject(result);
                    orderListView = (ListView) getActivity().findViewById(R.id.listView);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        aq.id(R.id.result_area).visible();
                        if (jsondata.isNull("data")) {
                            //
                            aq.id(orderListView).gone();
                            aq.id(R.id.empty).visible();
                            aq.id(R.id.result_area).gone();
                        } else {
                            orderData = new ArrayList<>();
                            JSONArray orderInfo = jsondata.getJSONArray("data");

                            aq.id(R.id.result_text).text("'" + orderInfo.length() + "건'");

                            for (int i = 0; i < orderInfo.length(); i++) {

                                String orderId = orderInfo.getJSONObject(i).getString("orderId");
                                String orderStatus = orderInfo.getJSONObject(i).getString("orderStatus");
                                String addressSeq = orderInfo.getJSONObject(i).getString("addressSeq");
                                String storeId = orderInfo.getJSONObject(i).getString("storeId");
                                String deliveryPrice = orderInfo.getJSONObject(i).getString("deliveryPrice");
                                String pickupRequestTimeApp = orderInfo.getJSONObject(i).getString("pickupRequestTimeApp");
                                String deliveryRequestTimeApp = orderInfo.getJSONObject(i).getString("deliveryRequestTimeApp");
                                String registerDateApp = orderInfo.getJSONObject(i).getString("registerDateApp");
                                String orderRequest = orderInfo.getJSONObject(i).getString("orderRequest");
                                String orderPickupItemMessage = orderInfo.getJSONObject(i).getString("orderPickupItemMessage");
                                String statusTimeApp = orderInfo.getJSONObject(i).getString("statusTimeApp");
                                String userName = orderInfo.getJSONObject(i).getString("userName");
                                int quantity = orderInfo.getJSONObject(i).getInt("pickupQuantity");
                                String isPayment = orderInfo.getJSONObject(i).getString("isPayment");
                                String payments = null;
                                if(orderInfo.getJSONObject(i).isNull("payments")){
                                    payments = null;
                                }else{
                                    payments = orderInfo.getJSONObject(i).getString("payments");
                                }
                                String isReceivable = orderInfo.getJSONObject(i).getString("isReceivable");
                                String isAdditionalPayment = null;
                                if(orderInfo.getJSONObject(i).isNull("isAdditionalPayment")){
                                    isAdditionalPayment = null;
                                }else{
                                    isAdditionalPayment = orderInfo.getJSONObject(i).getString("isAdditionalPayment");
                                }

                                OrderListItem aItem = new OrderListItem(orderId, orderStatus, addressSeq, storeId, userName, deliveryPrice, pickupRequestTimeApp,
                                        deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage, orderInfo.getJSONObject(i).toString(), quantity, isPayment, payments, isReceivable, isAdditionalPayment);
                                orderData.add(aItem);

                            }

                            if(orderData.size() > 0){
                                aq.id(R.id.empty).gone();
                                aq.id(orderListView).visible();
                            }else{
                                aq.id(orderListView).gone();
                                aq.id(R.id.empty).visible();
                                aq.id(R.id.result_area).gone();
                            }

                            mMainActivity.orderData = orderData;
                            orderAdapter = new OrderListViewAdapter(ctx, R.layout.order_history_item, orderData);
                            orderAdapter.notifyDataSetChanged();

                            orderListView.setAdapter(orderAdapter);
                        }

                    } else {
                        aq.id(orderListView).gone();
                        aq.id(R.id.empty).visible();
                        aq.id(R.id.result_area).gone();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private InputMethodManager imm;

    private void hideKeyboard(){

        imm.hideSoftInputFromWindow(aq.id(R.id.text_search).getEditText().getWindowToken(), 0);

    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        ctx = getActivity();
        aq = new AQuery(view);
        aq.hardwareAccelerated11();

        imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        aq.id(R.id.logo).gone();
        aq.id(R.id.empty).gone();
        aq.id(R.id.result_area).gone();
        aq.id(R.id.title_text).text("주문 검색").visible();

        aq.id(R.id.desc_text).text("'-'를 제외한 번호 전체 또는 뒤 4자리");

        aq.id(R.id.top_area).clicked(this, "topmenuClicked");
        aq.id(R.id.btn_search).clicked(this, "searchAction");

        aq.id(R.id.btn_phone).clicked(this, "showData").tag(0);
        aq.id(R.id.btn_orderno).clicked(this, "showData").tag(1);
        aq.id(R.id.btn_tag).clicked(this, "showData").tag(2);

        return view;
    }

    public void showData(View button){
        int tag = (int)((ImageButton)button).getTag();
        switch (tag){
            case 0:
                currentSearchType = "phone";
                aq.id(R.id.btn_phone).image(R.mipmap.btn_tab_selected);//.background(R.color.white).textColorId(R.color.black);
                aq.id(R.id.btn_orderno).image(R.mipmap.btn_tab_normal);;//.background(R.color.actionbar_background).textColorId(R.color.white);
                aq.id(R.id.btn_tag).image(R.mipmap.btn_tab_normal);;//.background(R.color.actionbar_background).textColorId(R.color.white);
                aq.id(R.id.tab_text_01).textColor(Color.parseColor("#3d9eff"));
                aq.id(R.id.tab_text_02).textColor(Color.parseColor("#d7e9ff"));
                aq.id(R.id.tab_text_03).textColor(Color.parseColor("#d7e9ff"));
                aq.id(R.id.desc_text).text("'-'를 제외한 번호 전체 또는 뒤 4자리");
                aq.id(R.id.result_area).gone();
                aq.id(R.id.text_search).text("").getEditText().setHint("고객 전화번호 입력");
                break;
            case 1:
                currentSearchType = "orderId";
                aq.id(R.id.btn_orderno).image(R.mipmap.btn_tab_selected);//.background(R.color.white).textColorId(R.color.black);
                aq.id(R.id.btn_phone).image(R.mipmap.btn_tab_normal);//.background(R.color.actionbar_background).textColorId(R.color.white);
                aq.id(R.id.btn_tag).image(R.mipmap.btn_tab_normal);//.background(R.color.actionbar_background).textColorId(R.color.white);
                aq.id(R.id.tab_text_01).textColor(Color.parseColor("#d7e9ff"));
                aq.id(R.id.tab_text_02).textColor(Color.parseColor("#3d9eff"));
                aq.id(R.id.tab_text_03).textColor(Color.parseColor("#d7e9ff"));
                aq.id(R.id.desc_text).text("주문번호 5자리를 입력해주세요.");
                aq.id(R.id.result_area).gone();
                aq.id(R.id.text_search).text("").getEditText().setHint("주문번호 입력");
                break;
            case 2:
                currentSearchType = "tagId";
                aq.id(R.id.btn_orderno).image(R.mipmap.btn_tab_normal);//.background(R.color.white).textColorId(R.color.black);
                aq.id(R.id.btn_phone).image(R.mipmap.btn_tab_normal);//.background(R.color.actionbar_background).textColorId(R.color.white);
                aq.id(R.id.btn_tag).image(R.mipmap.btn_tab_selected);//.background(R.color.actionbar_background).textColorId(R.color.white);
                aq.id(R.id.tab_text_01).textColor(Color.parseColor("#d7e9ff"));
                aq.id(R.id.tab_text_02).textColor(Color.parseColor("#d7e9ff"));
                aq.id(R.id.tab_text_03).textColor(Color.parseColor("#3d9eff"));
                aq.id(R.id.desc_text).text("TAG번호를 입력해주세요.");
                aq.id(R.id.result_area).gone();
                aq.id(R.id.text_search).text("").getEditText().setHint("TAG번호 입력");
                break;
        }

        try{
            orderData = new ArrayList<>();
            mMainActivity.orderData = orderData;
            orderAdapter = new OrderListViewAdapter(ctx, R.layout.order_history_item, orderData);
            orderAdapter.notifyDataSetChanged();
            orderListView.setAdapter(orderAdapter);
            orderListView.invalidate();
        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }

    public void topmenuClicked(View button) {
        mMainActivity.getSlidingMenu().toggle();
    }


    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }


    @Override
    public boolean onBackPressed() {
        return false;

    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }
}
