package me.rewhite.delivery.fragment;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import kankan.wheel.widget.OnWheelClickedListener;
import kankan.wheel.widget.OnWheelScrollListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.AbstractWheelTextAdapter;
import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.PickupItemActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PickupItemFragmentV2.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PickupItemFragmentV2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PickupItemFragmentV2 extends Fragment implements IFragment{

    private final static String TAG = "PickupItemFragmentV2";
    private PickupItemActivity mActivity;

    private OnFragmentInteractionListener mListener;
    public AQuery aq;

    ProgressDialog dialog;
    public ArrayList<String> optionItems = new ArrayList<String>();
    private boolean scrolling = false;
    private RelativeLayout.LayoutParams layoutParams;
    private View rootselectview;
    private View rootbackview;
    private String fragmentName = PickupItemActivity.FRAGMENT_SUB_CATEGORY;

    private ViewGroup mView = null;

    public PickupItemFragmentV2() {
        // Required empty public constructor
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    public static PickupItemFragmentV2 newInstance(String param1, String param2) {
        PickupItemFragmentV2 fragment = new PickupItemFragmentV2();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }
    WheelView wheelview;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_pickup_item_fragment_v2, container, false);
        aq = new AQuery(mView);

        mActivity = (PickupItemActivity)getActivity();

        try {
            JSONArray data = mActivity.priceArray;
            if(optionItems.size() == 0){
                for(int i = 0 ; i < data.length(); i++){
                    optionItems.add(data.getJSONObject(i).getString("itemTitle"));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        rootselectview = aq.id(R.id.rootselectview).getView();
        rootbackview = aq.id(R.id.rootbackview).getView();
        rootbackview.setBackgroundColor(0xffffffff);

        RelativeLayout layout = (RelativeLayout)mView.findViewById(R.id.wheel_area);

        wheelview = (WheelView)mView.findViewById(R.id.wheelview);
        wheelview.setVisibleItems(6);
        //ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<String>(this, optionItems);
        //adapter.setTextSize(22);
        wheelview.setViewAdapter(new PickItemAdapter(getActivity()));
        wheelview.setCurrentItem(0);

        wheelview.addClickingListener(new OnWheelClickedListener() {
            @Override
            public void onItemClicked(WheelView wheel, int itemIndex) {
                //wheel.scroll(itemIndex, 200);
                wheel.setCurrentItem(itemIndex, true);
                Log.e("onItemClicked()", itemIndex + "");
            }
        });

        wheelview.addScrollingListener( new OnWheelScrollListener() {
            public void onScrollingStarted(WheelView wheel) {

                scrolling = true;
                aq.id(R.id.guide_desc).gone();
                aq.id(R.id.btn_select).gone();

                //aq.id(R.id.current_label).gone();
                //aq.id(R.id.current_count).gone();

                aq.id(R.id.selectview).background(R.drawable.wheel_pick_current_moving);
                //rootbackview.setBackgroundColor(0x00000000);
                rootselectview.setBackgroundColor(0x00000000);

                aq.id(R.id.rootbackview).getView().animate()
                        .setStartDelay(0)
                        .setInterpolator(new AccelerateInterpolator())
                        .alpha(0)
                        .scaleX(1)
                        .scaleY(1);
                aq.id(R.id.current_label).getView().animate()
                        .setStartDelay(0)
                        .setInterpolator(new AccelerateInterpolator())
                        .alpha(0)
                        .scaleX(0.5f)
                        .scaleY(0.5f);
                aq.id(R.id.current_count).getView().animate()
                        .setStartDelay(0)
                        .setInterpolator(new AccelerateInterpolator())
                        .alpha(0)
                        .scaleX(0.5f)
                        .scaleY(0.5f);
                aq.id(R.id.btn_select).getView().animate()
                        .setStartDelay(0)
                        .setInterpolator(new AccelerateInterpolator())
                        .alpha(0)
                        .scaleX(.5f)
                        .scaleY(.5f);
            }
            public void onScrollingFinished(WheelView wheel) {
                int cx = (rootbackview.getLeft() + rootbackview.getRight()) / 2;
                int cy = (rootbackview.getTop() + rootbackview.getBottom()) / 2;
                int finalRadius = Math.max(rootbackview.getWidth(), rootbackview.getHeight());

                scrolling = false;
                Log.e("getCurrentItem()", wheel.getCurrentItem() + "");
                if(wheel.getCurrentItem() == 0){
                    aq.id(R.id.guide_desc).visible();
                }
                aq.id(R.id.selectview).background(R.drawable.wheel_pick_current);
                Animator anim = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    anim = ViewAnimationUtils.createCircularReveal(rootbackview, cx, cy, 0, finalRadius);
                    //rootbackview.setBackgroundColor(0xffffffff);
                    anim.start();
                }
                setCurrentLabel(wheel.getCurrentItem());
                aq.id(R.id.btn_select).image(R.mipmap.pui_btn_select).visible();
            }
        });

        aq.id(R.id.btn_select).clicked(this, "selectAction");
        aq.id(R.id.btn_close).clicked(this, "closeAction");
        setCurrentLabel(wheelview.getCurrentItem());

        return  mView;
    }


    public void closeAction(View button){
        getActivity().finish();
    }

    private void setCurrentLabel(int _index){
        aq.id(R.id.rootbackview).getView().animate()
                .setStartDelay(0)
                .setInterpolator(new OvershootInterpolator())
                .alpha(1)
                .scaleX(1)
                .scaleY(1);
        aq.id(R.id.current_label).getView().animate()
                .setStartDelay(0)
                .setInterpolator(new OvershootInterpolator())
                .alpha(1)
                .scaleX(1)
                .scaleY(1);
        aq.id(R.id.current_count).getView().animate()
                .setStartDelay(50)
                .setInterpolator(new OvershootInterpolator())
                .alpha(1)
                .scaleX(1)
                .scaleY(1);
        aq.id(R.id.btn_select).getView().animate()
                .setStartDelay(150)
                .setInterpolator(new OvershootInterpolator())
                .alpha(1)
                .scaleX(1)
                .scaleY(1);

        if(optionItems.size() < _index && optionItems.size() > 0){
            aq.id(R.id.current_label).text(optionItems.get(0)).textColor(0xFF000000).visible();
        }else if(optionItems.size() == 0){
            aq.id(R.id.current_label).text("").textColor(0xFF000000).visible();
        }else{
            aq.id(R.id.current_label).text(optionItems.get(_index)).textColor(0xFF000000).visible();
        }

        try {
            aq.id(R.id.current_count).text("(" + mActivity.priceArray.getJSONObject(_index).getJSONArray("items").length()+")").textColor(0xFF01bff3).visible();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void selectAction(View button){
        int cx = rootselectview.getRight();//(rootselectview.getLeft() + rootselectview.getRight()) / 2;
        int cy = rootselectview.getTop();
        int finalRadius = Math.max(rootselectview.getWidth(), rootselectview.getHeight());

        mActivity.Step1Name = optionItems.get(wheelview.getCurrentItem());
        mActivity.Step1Index = wheelview.getCurrentItem();

        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(rootselectview, cx, cy, 0, finalRadius);
            rootselectview.setBackgroundColor(0xff01bff3);
            aq.id(R.id.current_label).textColor(0xFFFFFFFF);
            aq.id(R.id.current_count).textColor(0xFFFFFFFF);
            aq.id(R.id.btn_select).image(R.mipmap.pui_btn_select_hover);
            anim.start();

            ViewGroup sharedView = (ViewGroup) mView.findViewById(R.id.transname);
            TextView mainCategoryName = (TextView) mView.findViewById(R.id.current_label);
            mainCategoryName.setTransitionName("currentLabel");

            String transitionName = "pickupItemSubCategoryTitle";

            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Fragment fragment = fm.findFragmentByTag(fragmentName);

            fragment = PickupItemSubCategoryFragmentV2.newInstance(optionItems.get(wheelview.getCurrentItem()), wheelview.getCurrentItem());


            setSharedElementReturnTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(R.transition.change_transform));
            setExitTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(android.R.transition.fade));
            fragment.setSharedElementEnterTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(R.transition.change_transform));
            fragment.setEnterTransition(TransitionInflater.from(
                    getActivity()).inflateTransition(android.R.transition.fade));

            ft.addSharedElement(mainCategoryName, "currentLabel")
                    .addSharedElement(sharedView, transitionName)
                    .replace(R.id.content_frame, fragment)
                    .addToBackStack(fragmentName)
                    .commit();
        }else{
            //PickupItemSubCategoryFragmentV2 subFragment = PickupItemSubCategoryFragmentV2.newInstance(optionItems.get(wheelview.getCurrentItem()), wheelview.getCurrentItem());;

            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Fragment fragment = fm.findFragmentByTag(fragmentName);
            fragment = PickupItemSubCategoryFragmentV2.newInstance(optionItems.get(wheelview.getCurrentItem()), wheelview.getCurrentItem());

            ft
                    .replace(R.id.content_frame, fragment)
                    .addToBackStack(fragmentName)
                    .commit();
        }

    }

    /**
     * Adapter for countries
     */
    private class PickItemAdapter extends AbstractWheelTextAdapter {
        // Countries names


        /**
         * Constructor
         */
        protected PickItemAdapter(Context context) {
            super(context, R.layout.pickitem_layout, NO_RESOURCE);

            setItemTextResource(R.id.text_label);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            View view = super.getItem(index, cachedView, parent);

            TextView label = (TextView)view.findViewById(R.id.text_label);
            label.setText(optionItems.get(index));

            return view;
        }

        @Override
        public int getItemsCount() {
            return optionItems.size();
        }

        @Override
        protected CharSequence getItemText(int index) {
            return optionItems.get(index);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
