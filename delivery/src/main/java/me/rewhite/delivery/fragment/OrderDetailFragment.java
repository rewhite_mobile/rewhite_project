package me.rewhite.delivery.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import java.util.ArrayList;
import java.util.List;

import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.OrderDetailAdapter;
import me.rewhite.delivery.layout.MyLinearLayoutManager;
import me.rewhite.delivery.view.OrderItem;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrderDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrderDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrderDetailFragment extends Fragment implements IFragment{

    private Context ctx;
    private String orderId;
    private String orderStatus;
    private String userId;
    private OnFragmentInteractionListener mListener;

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;

    private AQuery aq;

    public static OrderDetailFragment newInstance(Bundle args) {
        OrderDetailFragment fragment = new OrderDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public OrderDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            orderId = getArguments().getString("orderId");
            orderStatus = getArguments().getString("orderStatus");
            userId = getArguments().getString("userId");
        }
    }

    List<OrderItem> mItems;
    private List<OrderItem> getDummyData(){
        mItems = new ArrayList<OrderItem>();

        OrderItem orderElement = new OrderItem();
        orderElement.setProductId("123123123123");
        orderElement.setProductImagePath("http://cfile25.uf.tistory.com/image/21660F3651B998272E5252");
        orderElement.setProductName("와이셔츠");
        orderElement.setCount(2);
        mItems.add(orderElement);

        orderElement = new OrderItem();
        orderElement.setProductId("123123123123");
        orderElement.setProductImagePath("http://cfile1.uf.tistory.com/image/1937B34851207EBE231D29");
        orderElement.setProductName("남성정장 한벌");
        orderElement.setCount(3);
        mItems.add(orderElement);

        return mItems;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_detail, container, false);
        ctx = getActivity();
        aq = new AQuery(view);
        aq.id(R.id.profile_name).text(userId);

        mRecyclerView = (RecyclerView)view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new MyLinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new OrderDetailAdapter(getDummyData(),new OrderDetailAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                Log.i("OrderDetailAdapter", "OnItemClickListener position : " + position);
            }
        });
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }

}
