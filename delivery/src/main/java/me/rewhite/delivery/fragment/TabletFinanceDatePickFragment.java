package me.rewhite.delivery.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.util.MZValidator;


public class TabletFinanceDatePickFragment extends Fragment {
    private static final String ARG_DATA = "data";
    //private static final String ARG_YOIL = "yoil";
    AQuery aq;
    boolean isSelected = false;
    JSONObject data;

    public static TabletFinanceDatePickFragment create(JSONObject json) {
        TabletFinanceDatePickFragment fragment = new TabletFinanceDatePickFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_DATA, json.toString());
        fragment.setArguments(bundle);
        return fragment;
    }

    public void setSelected() {
        isSelected = true;
    }

    public void setUnselected() {
        isSelected = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tablet_finance_fragment_date_pick, container, false);
        aq = new AQuery(rootView);
        final Bundle arguments = getArguments();
        try {
            data = new JSONObject(arguments.getString(ARG_DATA));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            aq.id(R.id.date_text).text(data.getString("calculateDateString"));
            aq.id(R.id.price_text).text(MZValidator.toNumFormat(data.getInt("totalPrice"))  + "원");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rootView;
    }
}
