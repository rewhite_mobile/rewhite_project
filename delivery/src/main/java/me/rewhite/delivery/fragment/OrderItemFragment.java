package me.rewhite.delivery.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.MainActivity;
import me.rewhite.delivery.data.RewhiteType;
import me.rewhite.delivery.util.TimeUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderItemFragment extends Fragment {

    private static final String mViewType = "viewType";
    private static final String mData = "data";
    private JSONObject data;
    private int viewType;

    private String nickName;
    private String profilePath;
    private String orderStatus;
    private long hopeTime;
    private String orderId;

    private AQuery aq;
    private ImageView imageView;

    public static OrderItemFragment create(JSONObject _data, int viewtype) {
        OrderItemFragment fragment = new OrderItemFragment();
        Bundle args = new Bundle();
        args.putString(mData, _data.toString());
        args.putInt(mViewType, viewtype);
        fragment.setArguments(args);
        return fragment;
    }

    public OrderItemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            viewType = getArguments().getInt(mViewType);
            try {
                data = new JSONObject(getArguments().getString(mData));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_item, container, false);

        aq = new AQuery(getActivity(), view);
        //nickname:'marines2',profilePath:'http://i.imgur.com/2PBLK.jpg', orderType:'N+D', hopeTime:'201507030800', orderId:'123123123'

        if(data != null){
            try {
                nickName = data.getString("userName");
                profilePath = data.getString("imageThumbPath");
                orderId = data.getString("orderId");
                String address = data.getString("address1");
                String addressDetail = data.getString("address2");

                if(viewType == RewhiteType.ORDERTYPE_PICKUP){
                    //aq.id(R.id.button_title).text("수거하러 가기");
                    aq.id(R.id.hopetime_label).text("수거희망시간");
                    hopeTime = data.getLong("pickupRequestTimeApp");
                }else{
                    //aq.id(R.id.button_title).text("배송하러 가기");
                    aq.id(R.id.hopetime_label).text("배송희망시간");
                    hopeTime = data.getLong("deliveryRequestTimeApp");
                }
                Date dt = new Date();
                dt.setTime(hopeTime);
                aq.id(R.id.hopetime).text(TimeUtil.getHopeFormat(dt));
                aq.id(R.id.order_text).text(address + " " + addressDetail);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //circularImageView = (CircularImageView) getActivity().findViewById(R.id.profileImage);
        //circularImageView.addShadow();
        aq.id(R.id.nickname_text).text(nickName);

        ImageOptions options = new ImageOptions();
        options.round = 65;
        options.ratio = 1.0f;
        //options.memCache = true;
        //options.targetWidth = 70;
        options.animation = AQuery.FADE_IN_NETWORK;
        options.fallback = R.mipmap.kakao_default_profile_image;

        aq.id(R.id.profileImage).image(profilePath, options);

        aq.id(R.id.btn_order).clicked(this, "orderDetailClicked");

        return view;
    }

    public void orderDetailClicked(View button){
        if (getActivity() instanceof MainActivity) {
            MainActivity ra = (MainActivity) getActivity();
            ra.showDetailInfo(orderId, viewType, data);
        }
    }
}
