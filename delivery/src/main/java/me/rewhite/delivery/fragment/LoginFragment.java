package me.rewhite.delivery.fragment;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.delivery.Bootstrap;
import me.rewhite.delivery.Bootstrap.TrackerName;
import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.MainActivity;
import me.rewhite.delivery.activity.SignActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.ValidateUtil;

//import android.view.ViewAnimationUtils;

//import com.actionbarsherlock.app.SherlockFragment;


@SuppressLint("NewApi")
public class LoginFragment extends Fragment implements IFragment {

    private static final String TAG = "SignMainBottomFragment";
    private static final String KEY_CONTENT = "SignMainBottomFragment";
    private boolean mIsUpState = true;

    private SignActivity mSignActivity = null;

    private ViewGroup mView;
    private AQuery aq;
    private TextView input_email_text;
    private TextView input_password_text;

    //private List<Fragment> fragments;

    private String mContent = "???";

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(CommonUtility.isTablet(getActivity())){
            mView = (ViewGroup) inflater.inflate(R.layout.tablet_fragment_login, container, false);
        }else{
            mView = (ViewGroup) inflater.inflate(R.layout.fragment_login, container, false);
        }

        mSignActivity = (SignActivity) getActivity();

        Tracker t = ((Bootstrap) getActivity().getApplication()).getTracker(TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        aq = new AQuery(mSignActivity, mView);
        aq.id(R.id.btn_login).clicked(this, "loginClicked");

        //aq.id(R.id.btn_clear_01).tag(0).clicked(this, "inputClearClicked");
        //aq.id(R.id.btn_clear_02).tag(1).clicked(this, "inputClearClicked");

        aq.id(R.id.btn_find_password).clicked(this, "findPasswordClicked");

        input_email_text = (TextView) mView.findViewById(R.id.input_email_text);
        input_password_text = (TextView) mView.findViewById(R.id.input_password_text);

        if("".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_ID)) || SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_ID) == null){

        }else{
            input_email_text.setText(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_ID));
        }

        input_password_text.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            public void onFocusChange(View view, boolean hasfocus) {
                if (hasfocus) {
                    if (input_password_text.getText().length() > 0) {
                        //aq.id(R.id.btn_clear_02).visible();
                    }
                    aq.id(R.id.btn_find_password).gone();
                } else {
                    //aq.id(R.id.btn_clear_02).gone();
                    aq.id(R.id.btn_find_password).visible();
                }
            }
        });

        aq.id(R.id.btn_refresh).clicked(this, "loginRefreshClicked");
        aq.id(R.id.btn_other_login).clicked(this, "loginOtherClicked");


        // Account
        accountManager = AccountManager.get(getActivity()); //this is Activity
        String authType = "me.rewhite.delivery.account";
        Account[] allRewhiteAccounts = accountManager.getAccountsByType(authType);

        if(allRewhiteAccounts.length > 0){
            aq.id(R.id.refresh_layout).visible();
            aq.id(R.id.default_layout).gone();
        }else{
            aq.id(R.id.refresh_layout).gone();
            aq.id(R.id.default_layout).visible();
        }

        // btn_other_login

        return mView;
    }

    AccountManager accountManager;
    int accountIndex = 0;

    public void loginRefreshClicked(View button){

        final String dt = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID);
        String authType = "me.rewhite.delivery.account";
        final Account[] allRewhiteAccounts = accountManager.getAccountsByType(authType);

        if(allRewhiteAccounts.length == 1){
            mSignActivity.requestSessionOpen(getActivity(), allRewhiteAccounts[0].name, accountManager.getPassword(allRewhiteAccounts[0]), dt);
        }else{
            String[] items = new String[allRewhiteAccounts.length];
            for(int i = 0 ; i < allRewhiteAccounts.length; i++){
                items[i] = allRewhiteAccounts[i].name;
            }

            AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
            ab.setTitle("로그인할 계정을 선택해주세요.");
            ab.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    accountIndex = which;
                }
            }).setPositiveButton("확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.w("autoLogin", allRewhiteAccounts[accountIndex].name + " / " + accountManager.getPassword(allRewhiteAccounts[accountIndex]));
                    mSignActivity.requestSessionOpen(getActivity(), allRewhiteAccounts[accountIndex].name, accountManager.getPassword(allRewhiteAccounts[accountIndex]), dt);
                }
            }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            ab.show();
        }

    }

    public void loginOtherClicked(View button){
        aq.id(R.id.refresh_layout).gone();
        aq.id(R.id.default_layout).visible();
    }

/*
    private void setMaterialEffect() {
        // previously visible view

        final View myView = aq.id(R.id.button_area).getView();//getActivity().findViewById(R.id.btn_login);

        // get the center for the clipping circle
        int cx = (myView.getLeft() + myView.getRight()) / 2;
        int cy = (myView.getTop() + myView.getBottom()) / 2;

        // get the initial radius for the clipping circle
        int initialRadius = myView.getWidth();

        // create the animation (the final radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                //myView.setVisibility(View.INVISIBLE);
            }
        });

        // start the animation
        anim.start();
    }*/

    @Override
    public void onResume() {
        Log.d(TAG, "[onResume]");
        super.onResume();

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }


    private void passThrough() {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setClass(mSignActivity, MainActivity.class);
        mSignActivity.startActivity(intent);
        mSignActivity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        mSignActivity.finish();
    }

    public void loginClicked(View button) {
        Log.d(TAG, "[loginClicked] ");

        if (mSignActivity.isLoginProcessing) {
            return;
        }

        String userIdInput = aq.id(R.id.input_email_text).getText().toString();
        String passwordInput = aq.id(R.id.input_password_text).getText().toString();

        if (userIdInput.length() > 5) {
            if (ValidateUtil.validPassword(passwordInput) == 1) {
                String dt = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID);
                mSignActivity.requestSessionOpen(getActivity(), userIdInput, passwordInput,dt);

                //passThrough();
            } else {
                DUtil.alertShow(mSignActivity, "사용할수 없는 비밀번호입니다.");
                //DialogUtility.showOneButtonAlertDialog(getActivity(), null, "사용할수 없는 비밀번호입니다.", "확인", null);
            }
        } else {
            DUtil.alertShow(mSignActivity, "아이디는 6자리 이상입니다.");
        }


    }

    @Override
    public void onResult(String command, String result) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {
        // TODO Auto-generated method stub

    }

    @Override
    public void dismiss() {

    }
}
