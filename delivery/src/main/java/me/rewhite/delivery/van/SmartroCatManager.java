package me.rewhite.delivery.van;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import me.rewhite.delivery.util.MZCrypto;
import me.rewhite.delivery.util.UnicodeFormatter;

/**
 * Created by marines on 16. 7. 28..
 */
public class SmartroCatManager extends Service {

    public static final String INTENT_ACTION = "intent.action.rewhite.van.service";
    protected SocketChannel mChannel;
    protected ReceiveThread mThread;
    public int RECV_BUFFER_SIZE = 1024 * 1024;

    static final String serverAddr = "192.168.0.28";
    static final 	int CLIENT_PORT = 5555;

    //public static final int CONNECTION_REQ = 0;    // 서비스에 대한 명령어
    public static final int CONNECTION_CHECK = 1;    // 서비스에 대한 명령어
    public static final int PRINT_REQUEST = 2;    // 서비스에 대한 명령어
    public static final int SIGN_REQUEST = 3;    // 서비스에 대한 명령어
    public static final int CARDPAY_REQUEST = 4;    // 서비스에 대한 명령어
    public static final int CASHPAY_NUMBER_REQUEST = 5;    // 서비스에 대한 명령어
    public static final int CASHPAY_RECEIPT_REQUEST = 6;    // 서비스에 대한 명령어

    final Messenger mMessenger = new Messenger(new IncomingHandler());    // 클라이언트가 메세지를 보내는 도구

    public void showDialog(String _message) {
        Intent intent = new Intent();
        intent.setAction(INTENT_ACTION);
        intent.putExtra("RETCODE", "8888");
        intent.putExtra("MESSAGE", _message);
        intent.putExtra("RESULT", true);
        sendBroadcast(intent);
    }

    public void dismissDialog() {
        Intent intent = new Intent();
        intent.setAction(INTENT_ACTION);
        intent.putExtra("RETCODE", "8888");
        intent.putExtra("MESSAGE", "");
        intent.putExtra("RESULT", false);
        sendBroadcast(intent);
    }

    // 클라이언트로부터의 메세지를 받았을 때 처리할 클래스
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg){
            switch(msg.what){

                case CONNECTION_CHECK :
                    //Toast.makeText(getApplicationContext(), "Messenger Service", Toast.LENGTH_SHORT).show();
                    connectionCheck();

                    break;
                case PRINT_REQUEST :
                    //Toast.makeText(getApplicationContext(), "Messenger Service", Toast.LENGTH_SHORT).show();
                    byte[] cont = msg.getData().getByteArray("content");
                    print(cont);
                    break;
                case SIGN_REQUEST :
                    //Toast.makeText(getApplicationContext(), "Messenger Service", Toast.LENGTH_SHORT).show();
                    byte[] sign = msg.getData().getByteArray("sign");
                    sign(sign);
                    break;
                case CARDPAY_REQUEST :
                    //Toast.makeText(getApplicationContext(), "Messenger Service", Toast.LENGTH_SHORT).show();
                    byte[] payauth = msg.getData().getByteArray("payauth");
                    cardPay(payauth);
                    break;
                case CASHPAY_NUMBER_REQUEST:
                    byte[] noReq = msg.getData().getByteArray("noReq");
                    numberReq(noReq);
                    break;
                case CASHPAY_RECEIPT_REQUEST:
                    byte[] authReceipt = msg.getData().getByteArray("authReceipt");
                    authReceiptReq(authReceipt);
                    break;
                default :
                    super.handleMessage(msg);
            }
        }
    }

    public boolean isConnected() {
        if (mChannel == null) return false;
        Log.e("isConnected : ", mChannel.isConnected() + "");
        return mChannel.isConnected();
    }

    public void connectionCheck(){
        showDialog("연결상태 확인중..");

        String serviceType = null;
        try {
            serviceType = UnicodeFormatter.stringToHex("9001");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String body = serviceType + "1c1c";
        Log.e("body connectionCheck : ", body + "");

        requestCode(UnicodeFormatter.hexToByteArray(body));
    }

    public void print(byte[] content){
        showDialog("프린트 시도중..");

        String serviceType = null;
        currentType = "5301";
        try {
            serviceType = UnicodeFormatter.stringToHex(currentType);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String body = serviceType + UnicodeFormatter.byteArrayToHex(content);
        Log.e("body print : ", body + "");

        requestCode(UnicodeFormatter.hexToByteArray(body));
    }

    public void sign(byte[] sign){
        String serviceType = null;
        currentType = "5101";
        try {
            serviceType = UnicodeFormatter.stringToHex(currentType);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String body = serviceType + UnicodeFormatter.byteArrayToHex(sign);
        Log.e("body sign : ", body + "");

        requestCode(UnicodeFormatter.hexToByteArray(body));
    }

    String currentType = "";

    public void cardPay(byte[] payAuth){
        showDialog("카드결제 시도..");

        String serviceType = null;
        currentType = "0101";
        try {
            serviceType = UnicodeFormatter.stringToHex(currentType);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String body = serviceType + UnicodeFormatter.byteArrayToHex(payAuth);
        Log.e("body cardPay : ", body + "");

        requestCode(UnicodeFormatter.hexToByteArray(body));
    }

    public void numberReq(byte[] req){
        String serviceType = null;
        currentType = "5103";
        try {
            serviceType = UnicodeFormatter.stringToHex(currentType);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String body = serviceType + UnicodeFormatter.byteArrayToHex(req);
        Log.e("body numberReq : ", body + "");

        requestCode(UnicodeFormatter.hexToByteArray(body));
    }
    public void authReceiptReq(byte[] req){
        showDialog("현금영수증 발행요청..");

        String serviceType = null;
        currentType = "0101";
        try {
            serviceType = UnicodeFormatter.stringToHex(currentType);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String body = serviceType + UnicodeFormatter.byteArrayToHex(req);
        Log.e("body authReceiptReq : ", body + "");

        requestCode(UnicodeFormatter.hexToByteArray(body));
    }

    private void requestCode(byte[] _reqBytes){
        try {
            int reqSize = _reqBytes.length + 7;

            //String hexSize = UnicodeFormatter.stringToHex(String.valueOf(reqSize));

            String hexSize = UnicodeFormatter.stringToHex(String.format(Locale.KOREA, "%04d", reqSize));

            byte STX = 0x02;
            byte ETX = 0x03;
            byte[] calculateByte = UnicodeFormatter.hexToByteArray(hexSize + UnicodeFormatter.byteArrayToHex(_reqBytes) + UnicodeFormatter.byteToHex(ETX));
            byte BCC = MZCrypto.getBCC(calculateByte);

            byte[] reqBody = UnicodeFormatter.hexToByteArray(UnicodeFormatter.byteToHex(STX) + hexSize + UnicodeFormatter.byteArrayToHex(_reqBytes) + UnicodeFormatter.byteToHex(ETX) + UnicodeFormatter.byteToHex(BCC));

            Log.e("requestCode : ", "reqSize : " + hexSize + "\n" + UnicodeFormatter.byteArrayToHex(reqBody));

            ByteBuffer buffer = ByteBuffer.allocate(reqBody.length);
            buffer = ByteBuffer.wrap(reqBody);
            int ret = send(buffer);
            Log.e("send ret : ", ret + "");
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }catch(Exception e) {
            Log.e("tag", "Data send error.\n" + e.getLocalizedMessage());
        }
    }

    private void sendAck06(){
        showDialog("결제승인 완료처리 중..");

        byte[] reqBody = UnicodeFormatter.hexToByteArray("060606");
        Log.e("sendAck06 requestCode : ", UnicodeFormatter.byteArrayToHex(reqBody));

        ByteBuffer buffer = ByteBuffer.allocate(reqBody.length);
        buffer = ByteBuffer.wrap(reqBody);
        int ret = send(buffer);
    }

    protected void setNetworkThreadPolicy() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.e("onCreate : ", "onCreate");
        Toast.makeText(getApplicationContext(), "service start..", Toast.LENGTH_SHORT).show();
        //sdcard에 있는 test.mp3 을 찾아검사
        boolean connRet = connect(serverAddr, Short.parseShort("5555"), 5, rHandler);
        if(connRet){
            Log.e("connection : ", "Success");
        }else{
            Log.e("connection : ", "Failed");
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.e("onStartCommand : ", "onStartCommand");

        Toast.makeText(getApplicationContext(), "service start..", Toast.LENGTH_SHORT).show();
        //sdcard에 있는 test.mp3 을 찾아검사
        boolean connRet = connect(serverAddr, Short.parseShort("5555"), 5, rHandler);
        if(connRet){
            Log.e("connection : ", "Success");
        }else{
            Log.e("connection : ", "Failed");
        }
        return START_STICKY;
    }

    public ReceiveEventHandler rHandler = new ReceiveEventHandler() {
        @Override
        public void onReceived(ByteBuffer buffer, int len) {
            byte[] getBytes = new byte[len];
            getBytes = buffer.array();
            String s = UnicodeFormatter.byteArrayToHex(getBytes);

            if(getBytes[0] == 0x02){
                String resTypeHex = UnicodeFormatter.byteToHex(getBytes[5]) + UnicodeFormatter.byteToHex(getBytes[6])+UnicodeFormatter.byteToHex(getBytes[7])+UnicodeFormatter.byteToHex(getBytes[8]);
                byte[] resTypeBytes = UnicodeFormatter.hexToByteArray(resTypeHex);
                String resTypeStr = new String(resTypeBytes,0,resTypeBytes.length);
                Log.e("Socket onReceived resTypeStr :: ", resTypeStr);

                if("0102".equals(resTypeStr)) {

                    byte[] pat = new byte[1];
                    pat[0] = (byte)0x1C;
                    List<byte[]> ret = UnicodeFormatter.split(getBytes, pat);

                    byte[] retCode = ret.get(16);
                    String retCodeHex = UnicodeFormatter.byteArrayToHex(ret.get(16));
                    try {
                        if(retCodeHex.equals(UnicodeFormatter.stringToHex("00"))){
                            showDialog("결제가 성공적으로 완료되었습니다.");
                            Intent intent = new Intent();
                            intent.setAction(INTENT_ACTION);
                            intent.putExtra("RETCODE", "0102");
                            intent.putExtra("RESULT", true);
                            sendBroadcast(intent);

                            sendAck06();
                        }else{
                            Log.e("onReceived 0102 retHex :: ", retCodeHex + " / " + new String(ret.get(17), "euc-kr"));
                            if(retCodeHex.equals(UnicodeFormatter.stringToHex("ZZ"))){

                            }

                            Intent intent = new Intent();
                            intent.setAction(INTENT_ACTION);
                            intent.putExtra("RETCODE", "0102");
                            intent.putExtra("RESULT", false);
                            intent.putExtra("MESSAGE", new String(ret.get(17), "euc-kr"));
                            sendBroadcast(intent);

                            dismissDialog();
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                }else if("9998".equals(resTypeStr)){
                    // 단말기 상태정보
                    String deviceStatusCodeHex = UnicodeFormatter.byteToHex(getBytes[10]) + UnicodeFormatter.byteToHex(getBytes[11]);
                    byte[] deviceStatusCodeBytes = UnicodeFormatter.hexToByteArray(deviceStatusCodeHex);
                    String deviceStatusCode = new String(deviceStatusCodeBytes,0,deviceStatusCodeBytes.length);

                    Log.e("[deviceStatusCode]==", deviceStatusCode);
                    if("01".equals(deviceStatusCode)){
                        showDialog("카드 삽입 대기 중입니다.");
                    }else if("02".equals(deviceStatusCode)){
                        showDialog("카드를 읽었습니다.");
                    }else if("03".equals(deviceStatusCode)){
                        showDialog("비밀번호 입력 대기 중입니다.");
                    }else if("04".equals(deviceStatusCode)){
                        showDialog("비밀번호 입력이 완료 되었습니다.");
                    }else if("05".equals(deviceStatusCode)){
                        showDialog("서명 입력 대기 중입니다.");
                    }else if("06".equals(deviceStatusCode)){
                        showDialog("서명 입력이 완료 되었습니다.");
                    }else if("07".equals(deviceStatusCode)){
                        showDialog("VAN 통신 중입니다.");
                    }else if("08".equals(deviceStatusCode)){
                        showDialog("VAN 통신이 완료되었습니다.");
                    }else if("09".equals(deviceStatusCode)){
                        showDialog("망취소 중입니다.");
                    }else if("10".equals(deviceStatusCode)){
                        showDialog("망취소가 완료되었습니다.");
                    }

                }

            }else if(getBytes[0] == 0x10){
                Log.e("[Process]==", "END");
                dismissDialog();
            }else{
                //dismissDialog();
            }
            Log.e("Socket onReceived", s);
        }

        @Override
        public void onClosed() {
            Log.e("Socket onClosed", "");
        }

        @Override
        public void onThreadEvent() {
            Log.e("Socket onThreadEvent", "");
        }
    };

    public boolean connect(String ipAddress, short port, int timeout, ReceiveEventHandler handler) {
        setNetworkThreadPolicy();

        try     {
            if (mChannel != null && mChannel.isConnected() == true)
                return false;

            mChannel = SocketChannel.open();
            mChannel.configureBlocking(false);
            mChannel.socket().setReceiveBufferSize(RECV_BUFFER_SIZE);

            mChannel.connect(new InetSocketAddress(ipAddress, port));

            Selector selector = Selector.open();
            SelectionKey clientKey = mChannel.register(selector, SelectionKey.OP_CONNECT);

            if (selector.select(timeout*1000) > 0) {
                if (clientKey.isConnectable()) {
                    if (mChannel.finishConnect()) {
                        mThread = new ReceiveThread(mChannel, handler);
                        mThread.start();
                        return true;
                    }
                }
                mChannel.close();
                mChannel = null;
                return false;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public void close() {
        setNetworkThreadPolicy();
        try {
            if (mChannel != null) {
                if (mThread != null) {
                    mThread.mIsRunning = false;
                    mThread.join();
                }
                mChannel.close();
                mChannel = null;
                System.out.println("tcp client channel closed");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int send(ByteBuffer buffer) {
        setNetworkThreadPolicy();
        if (mChannel == null || !mChannel.isConnected()) return -1;
        try {
            return mChannel.write(buffer);
        } catch (IOException ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    public interface ReceiveEventHandler {
        public void onReceived(ByteBuffer buffer, int len);
        public void onClosed();
        public void onThreadEvent();
    }

    protected class ReceiveThread extends Thread {
        private SocketChannel mChannel;
        public boolean mIsRunning = false;

        private ReceiveEventHandler mHandler;

        private static final int BUFFER_SIZE = 1024 * 4;

        public ReceiveThread(SocketChannel channel, ReceiveEventHandler handler) {
            mChannel = channel;
            mHandler = handler;
            mIsRunning = true;
        }

        @Override
        public void run() {
            System.out.println("receive thread start");

            try {
                Selector selector = Selector.open();
                mChannel.register(selector, SelectionKey.OP_READ);

                while (mIsRunning) {
                    //System.out.println("receive thread mIsRunning / selector isOpen : "+selector.isOpen());

                    if (selector.select(2*1000) > 0) {
                        Set<SelectionKey> selectedKey = selector.selectedKeys();
                        Iterator<SelectionKey> iterator = selectedKey.iterator();

                        System.out.println("selector.select(2*1000) : " + selectedKey.toString());

                        while (iterator.hasNext()) {
                            SelectionKey key = iterator.next();
                            iterator.remove();

                            if (key.isReadable()) {
                                SocketChannel channel = (SocketChannel)key.channel();
                                ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
                                buffer.clear();

                                int ret;
                                try {
                                    ret = channel.read(buffer);
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                    if (mHandler != null) mHandler.onClosed();
                                    key.cancel();
                                    continue;
                                }

                                System.out.println("receive thread return value :" + ret);

                                if (ret <= 0) {
                                    System.out.println("SocketChannel.read returned " + ret);
                                    if (mHandler != null) mHandler.onClosed();
                                    key.cancel();
                                    continue;
                                }

                                buffer.rewind();

                                if (mHandler != null) mHandler.onReceived(buffer, ret);
                            }
                        }
                    }else{
                        //System.out.println("selector.select(2*1000) : " + selector.select(2*1000));
                    }
                    if (mHandler != null) mHandler.onThreadEvent();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            System.out.println("receive thread end");
        }
    }

    @Override
    public void onDestroy() {
        System.out.println("service destroy..");
        close();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    @Override
    public boolean onUnbind(Intent intent)
    {
        return super.onUnbind(intent);
    }
}
