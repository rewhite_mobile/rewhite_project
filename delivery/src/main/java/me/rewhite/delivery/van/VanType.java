package me.rewhite.delivery.van;

/**
 * Created by marines on 2017. 3. 21..
 */

public class VanType {

    public final static String EMPTY = "사용안함";
    public final static String SMARTRO = "SMARTRO";
    public final static String FIRSTDATA = "FIRSTDATA";
    public final static String KSNET = "KS-NET";

}
