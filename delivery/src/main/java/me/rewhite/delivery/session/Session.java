package me.rewhite.delivery.session;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.fragment.CustomDialogFragment;
import me.rewhite.delivery.session.RewhiteException.ErrorType;
import me.rewhite.delivery.util.CapacityUtil;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.SharedPreferencesUtility.UserInfo;
import me.rewhite.delivery.util.StringUtil;
import me.rewhite.delivery.util.SystemUtil;
import me.rewhite.delivery.util.TimeUtil;


public class Session implements Authorizer.OnAuthorizationListener, CustomDialogFragment.CustomDialogListener {

	private static Session currentSession;
	private static final String REDIRECT_URL_PREFIX = "rewhite/delivery";
	private static final String REDIRECT_URL_POSTFIX = "://oauth";
	private static final String COOKIE_SEPERATOR = ";";
	private static final String COOKIE_NAME_VALUE_DELIMITER = "=";
	private static final int DEFAULT_TOKEN_REQUEST_TIME_MILLIS = 3 * 60 * 60 * 1000; // 3 hours
	private static final int RETRY_TOKEN_REQUEST_TIME_MILLIS = 5 * 60 * 1000; // 5 minutes
	private static final String EXPIRES_IN_MILLIS = "expiresInMillis";
	public static final int AUTHORIZATION_CODE_REQUEST = 1;
	public static final int ACCESS_TOKEN_REQUEST = 2;

	private final Object INSTANCE_LOCK = new Object();

	private Context context;
	private AQuery aq;
	private final List<SessionCallback> sessionCallbacks;
	private final Handler sessionCallbackHandler;
	// 아래 값들은 변경되는 값으로 INSTANCE_LOCK의 보호를 받는다.
	private SessionState state;
	private AccessToken accessToken;

	//private GetterAccessToken getterAccessToken;

	public static synchronized Session getInstance(final Context context) {
		if (currentSession != null) {
			return currentSession;
		} else {
			return initialize(context);
		}
	}

	public static synchronized Session initialize(final Context context) {
		if (currentSession != null) {
			currentSession.clearCallbacks();
			currentSession.close();
		}

		currentSession = new Session(context);
		return currentSession;
	}

	private Session(final Context context) {
		if (context == null) throw new RewhiteException(ErrorType.ILLEGAL_ARGUMENT, "cannot create Session without Context.");

		this.context = context;

		this.sessionCallbacks = new ArrayList<SessionCallback>();
		this.sessionCallbackHandler = new Handler(Looper.getMainLooper()); //세션 callback은 main thread에서 호출되도록 한다.

		synchronized (INSTANCE_LOCK) {
			accessToken = AccessToken.createFromCache();
			if (accessToken.hasToken()) {
				this.state = SessionState.OPENED;
			} else {
				this.state = SessionState.CLOSED;
				close();
			}

		}
	}

	/**
	 * 세션 오픈을 진행한다. <br/>
	 * {@link SessionState#OPENED} 상태이면 바로 종료. <br/>
	 * {@link SessionState#CLOSED} 상태이면 authorization code 요청. 에러/취소시 {@link SessionState#CLOSED} <br/>
	 * {@link SessionState#OPENABLE} 상태이면 code 또는 refresh token 이용하여 access token 을 받아온다. 에러/취소시
	 * {@link SessionState#CLOSED}, refresh 취소시에만 {@link SessionState#OPENABLE} 유지. <br/>
	 * param으로 받은 콜백으로 그 결과를 전달한다. <br/>
	 *
	 * @param authType
	 *            인증받을 타입.
	 * @param callerActivity
	 *            세션오픈을 호출한 activity
	 */
	public void open(final AccessTokenRequest _request) {
		final SessionState currentState = getState();
		context = _request.getContext();
        if (currentState.isOpened()) {
            //    String accessToken = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN);
            if (!StringUtil.isNullOrEmpty(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN))) {
                // 이미 open이 되어 있다.
                DUtil.Log("currentState.isOpened()", "이미 open이 되어 있다");
                notifySessionState(currentState, null);
                return;
            } else {
                // 재로그인
                requestAccessToken(_request);
            }
        }else{
			try {
				synchronized (INSTANCE_LOCK) {
					switch (state) {
						case CLOSED :
							DUtil.Log("INSTANCE_LOCK CLOSED", "CLOSED");
							requestAccessToken(_request);
							break;
						case OPENABLE :
							DUtil.Log("INSTANCE_LOCK OPENABLE", "OPENABLE");
							if (!accessToken.hasToken()) {
								DUtil.Log("INSTANCE_LOCK OPENABLE", "requestAccessToken nohave");
								requestAccessToken(_request);
							} else {
								DUtil.Log("INSTANCE_LOCK OPENABLE", "requestAccessToken have");
								//accessToken.createFromResponse(json);
								final SessionState previous = state;
								state = SessionState.OPENED;
								RewhiteException sException = new RewhiteException(RewhiteException.ErrorType.AUTHORIZATION_FAILED, "SESSION AUTHORIZE");
								onStateChange(previous, state, null, sException, true);

								//close(new RewhiteException(ErrorType.AUTHORIZATION_FAILED, "can not request access token because both authorization code and refresh token are invalid."), false);

								requestAccessToken(_request);
							}
							break;
						case OPENED:
							DUtil.Log("INSTANCE_LOCK OPENED", "OPENED");
							break;
						default :
							throw new RewhiteException(ErrorType.AUTHORIZATION_FAILED, "current session state is not possible to open. state = " + state);
					}
				}
			} catch (RewhiteException e) {
				close(e, false);
			}
		}

	}

	public void registerMember(final JoinMemberRequest _request) {
		aq = new AQuery(_request.getContext());

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("email", _request.getEmail());
		params.put("password", _request.getPassword());
		params.put("mobile_number", _request.getMobileNumber());
		params.put("auth_number", _request.getAuthNumber());
		AjaxCallback.setNetworkLimit(1);
		aq.ajax(CommonUtility.SERVER_URL + Constants.MEMBER_REGISTER, params, JSONObject.class, new AjaxCallback<JSONObject>() {

			@Override
			public void callback(String url, JSONObject json, AjaxStatus status) {

				if (status.getCode() >= 400) {
					// IF ERROR
					Log.i("requestPhoneAuth StatusCode", status.getCode() + "");
					Log.i("requestPhoneAuth StatusError", status.getError());
				} else {
					// IF NORMAL
					if (json == null) {
						Log.i("RETURN", "" + status.getMessage());
						return;
					}

					try {
						if (json.getBoolean("result")) {
							//Toast.makeText(mActivity, json.getString("result_msg"), Toast.LENGTH_SHORT);
							AccessTokenRequest aRequest = AccessTokenRequest.createRequestWithDeviceToken(context, _request.getEmail(),
									_request.getPassword(), _request.getDeviceToken());
							requestAccessToken(aRequest);
						} else {
							
							Toast.makeText(aq.getContext(), json.getString("result_msg"), Toast.LENGTH_SHORT).show();
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					Log.i("requestPhoneAuth RESULT", json.toString());
				}
			}
		});
	}

	private void requestAccessToken(final AccessTokenRequest _request) {

		aq = new AQuery(_request.getContext());

		String url = CommonUtility.SERVER_URL + Constants.LOGIN;
		SharedPreferencesUtility.set(UserInfo.LOGIN_ID, _request.getEmail());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("loginId", _request.getEmail());
		params.put("secretKey", _request.getPassword());
        params.put("deviceToken", _request.getDeviceToken());

        params.put("deviceInformation", CapacityUtil.SHOW_MEMORYSIZE() +"\n\n" + SystemUtil.getSystemInfoString(getContext()) );
        params.put("k", "1");

		DUtil.Log("login params", params.toString());

		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
		cb.url(url).type(JSONObject.class).params(params).weakHandler(this, "loginProcessCallback");
		cb.header("Authorization", "device_token='" + _request.getDeviceToken() + "'");

		aq.ajax(cb);
	}

	public void refreshUserInfo(){

		Log.e("refreshUserInfo", "==================== excuted ==================== ");

		aq = new AQuery(getContext());

		String url = CommonUtility.SERVER_URL + Constants.SHOW_STORE_INFO;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("accessToken", SharedPreferencesUtility.get(UserInfo.ACCESS_TOKEN));
		params.put("k", "1");

		DUtil.Log("userinfo params", params.toString());

		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
		cb.url(url).type(JSONObject.class).params(params).weakHandler(this, "userInfoCallback");
		//cb.header("Authorization", "device_token='" + _request.getDeviceToken() + "'");

		aq.ajax(cb);
	}

	public void userInfoCallback(String url, JSONObject json, AjaxStatus status) {
		if (status.getCode() >= 400) {
			// IF ERROR
			Log.e("userInfoCallback StatusCode", status.getCode() + "");
			Log.e("userInfoCallback StatusError", status.getError());
		} else {
			// IF NORMAL
			if (json == null) {
				Log.i("RETURN", "" + status.getMessage());
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
				alertDialogBuilder.setMessage("서버와의 통신이 실패했습니다.")
						.setPositiveButton("확인", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								
							}
						});
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
				// 네트워크 오류페이지로 이동
				//goNetworkErrorPage();
				return;
			}

			try {
				Log.w("userInfoCallback >>", "" + json.toString());
				if ("S0000".equals(json.getString("resultCode"))) {
					//AccessToken.createFromResponse(json);

                    JSONObject userInfo = json.getJSONObject("data");
                    //String accessToken = userInfo.getString("accessToken");
                    //String loginId = userInfo.getString("loginId");
                    String storeId = userInfo.getInt("storeId") + "";
                    String storeName = userInfo.getString("storeName");
                    String masterName = userInfo.getString("masterName");
					String storeVirtualName = userInfo.getString("storeDisplayName");
					String storeAddress = userInfo.getString("storeAddress1");
					String longitude = userInfo.getString("longitude");
					String latitude = userInfo.getString("latitude");
					String storeGrade = userInfo.getString("storeGrade");
					String storePhone = userInfo.getString("storeTelephone");
					String storeVirtualPhone = userInfo.getString("storeVirtualPhone");
                    String storeImage = userInfo.getString("storeImage1");
					String availableOrder = userInfo.getString("availableOrder");
					String storeType = userInfo.getString("storeType");
					String isVisitPrice = userInfo.getString("isVisitPrice");

					Calendar current = Calendar.getInstance();
					String deliveryInfoX;
					String deliveryDurationX;

					if(TimeUtil.getWeekDayFromDate(current.getTime()) == 5){
						// 토요일
						deliveryInfoX = userInfo.getString("storeDeliveryTimeSat");
					}else if(TimeUtil.getWeekDayFromDate(current.getTime()) == 6){
						// 일요일
						deliveryInfoX = userInfo.getString("storeDeliveryTimeSun");
					}else{
						deliveryInfoX = userInfo.getString("storeDeliveryTime");
					}
					deliveryDurationX = deliveryInfoX.split("\\|")[0];

                    // TODO
                    //SharedPreferencesUtility.set(UserInfo.ACCESS_TOKEN, accessToken);
                    //SharedPreferencesUtility.set(UserInfo.LOGIN_ID, loginId);
                    SharedPreferencesUtility.set(UserInfo.STORE_ID, storeId);
                    SharedPreferencesUtility.set(UserInfo.STORE_NAME, storeName);
                    SharedPreferencesUtility.set(UserInfo.STORE_OWNER, masterName);
					SharedPreferencesUtility.set(UserInfo.STORE_VIRTUAL_NAME, storeVirtualName);
					SharedPreferencesUtility.set(UserInfo.STORE_ADDRESS, storeAddress);
					SharedPreferencesUtility.set(UserInfo.LONGITUDE, longitude);
					SharedPreferencesUtility.set(UserInfo.LATITUDE, latitude);
					SharedPreferencesUtility.set(UserInfo.STORE_GRADE, storeGrade);
					SharedPreferencesUtility.set(UserInfo.STORE_PHONE, storePhone);
					SharedPreferencesUtility.set(UserInfo.STORE_VIRTUAL_PHONE, storeVirtualPhone);
					SharedPreferencesUtility.set(UserInfo.STORE_PHONE, storeVirtualPhone);

                    SharedPreferencesUtility.set(UserInfo.STORE_IMAGE, storeImage);
					SharedPreferencesUtility.set(UserInfo.STORE_AVAILABLE_ORDER, availableOrder);
					SharedPreferencesUtility.set(UserInfo.STORE_TYPE, storeType);

					SharedPreferencesUtility.set(UserInfo.DELIVERY_GAP, deliveryDurationX);
					//
					SharedPreferencesUtility.set(UserInfo.IS_VISIT_PRICE, isVisitPrice);

//					final SessionState previous = state;
//					state = SessionState.OPENED;
//					RewhiteException sException = new RewhiteException(RewhiteException.ErrorType.AUTHORIZATION_FAILED, "SESSION AUTHORIZE");
//					onStateChange(previous, state, null, sException, true);
				} else {
					Toast.makeText(context.getApplicationContext(), json.getString("message"), Toast.LENGTH_SHORT).show();
					AccessToken.createEmptyToken();
					close();
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void loginProcessCallback(String url, JSONObject json, AjaxStatus status) {
		if (status.getCode() >= 400) {
			// IF ERROR
			Log.e("loginProcessCallback StatusCode", status.getCode() + "");
			Log.e("loginProcessCallback StatusError", status.getError() + "");

			Toast.makeText(context, "서버와의 통신이 실패했습니다.", Toast.LENGTH_LONG);
			AccessToken.createEmptyToken();
			close();
		} else {
			// IF NORMAL
			if (json == null) {
				Log.i("RETURN", "" + status.getMessage());
				Toast.makeText(context,"서버와의 통신이 실패했습니다.", Toast.LENGTH_SHORT);

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
				alertDialogBuilder.setMessage("서버와의 통신이 실패했습니다.")
						.setPositiveButton("확인", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								
							}
						});
				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();
				// 네트워크 오류페이지로 이동
				//goNetworkErrorPage();
				return;
			}

			try {
				Log.w("loginProcessCallback >>", "" + json.toString());
				if ("S0000".equals(json.getString("resultCode"))) {
					AccessToken.createFromResponse(json);

                    JSONObject userInfo = json.getJSONObject("data");
                    String accessToken = userInfo.getString("accessToken");
                    String loginId = userInfo.getString("loginId");
                    String storeId = userInfo.getInt("storeId") + "";
                    String storeName = userInfo.getString("storeName");
                    String masterName = userInfo.getString("masterName");
                    String storeVirtualName = userInfo.getString("storeDisplayName");
                    String storeAddress = userInfo.getString("storeAddress1");
                    String longitude = userInfo.getString("longitude");
                    String latitude = userInfo.getString("latitude");
                    String storeGrade = userInfo.getString("storeGrade");
                    String storePhone = userInfo.getString("storeTelephone");
                    String storeVirtualPhone = userInfo.getString("storeVirtualPhone");
                    String storeImage = userInfo.getString("storeImage1");
					String availableOrder = userInfo.getString("availableOrder");
					String storeType = userInfo.getString("storeType");
					String isVisitPrice = userInfo.getString("isVisitPrice");

					Calendar current = Calendar.getInstance();
					String deliveryInfoX;
					String deliveryDurationX;

					if(TimeUtil.getWeekDayFromDate(current.getTime()) == 5){
						// 토요일
						deliveryInfoX = userInfo.getString("storeDeliveryTimeSat");
					}else if(TimeUtil.getWeekDayFromDate(current.getTime()) == 6){
						// 일요일
						deliveryInfoX = userInfo.getString("storeDeliveryTimeSun");
					}else{
						deliveryInfoX = userInfo.getString("storeDeliveryTime");
					}
					deliveryDurationX = deliveryInfoX.split("\\|")[0];

                    // TODO
                    SharedPreferencesUtility.set(UserInfo.ACCESS_TOKEN, accessToken);
                    SharedPreferencesUtility.set(UserInfo.LOGIN_ID, loginId);
                    SharedPreferencesUtility.set(UserInfo.STORE_ID, storeId);
                    SharedPreferencesUtility.set(UserInfo.STORE_NAME, storeName);
					SharedPreferencesUtility.set(UserInfo.STORE_OWNER, masterName);
                    SharedPreferencesUtility.set(UserInfo.STORE_VIRTUAL_NAME, storeVirtualName);
                    SharedPreferencesUtility.set(UserInfo.STORE_ADDRESS, storeAddress);
                    SharedPreferencesUtility.set(UserInfo.LONGITUDE, longitude);
                    SharedPreferencesUtility.set(UserInfo.LATITUDE, latitude);
                    SharedPreferencesUtility.set(UserInfo.STORE_GRADE, storeGrade);
                    SharedPreferencesUtility.set(UserInfo.STORE_PHONE, storePhone);
                    SharedPreferencesUtility.set(UserInfo.STORE_VIRTUAL_PHONE, storeVirtualPhone);
                    SharedPreferencesUtility.set(UserInfo.STORE_IMAGE, storeImage);
					SharedPreferencesUtility.set(UserInfo.STORE_AVAILABLE_ORDER, availableOrder);
					SharedPreferencesUtility.set(UserInfo.STORE_TYPE, storeType);

					SharedPreferencesUtility.set(UserInfo.DELIVERY_GAP, deliveryDurationX);
					SharedPreferencesUtility.set(UserInfo.IS_VISIT_PRICE, isVisitPrice);

					final SessionState previous = state;
					state = SessionState.OPENED;
					//RewhiteException sException = new RewhiteException(RewhiteException.ErrorType.AUTHORIZATION_FAILED, "SESSION AUTHORIZE");
					onStateChange(previous, state, null, null, true);
				} else {
					Toast.makeText(context.getApplicationContext(), json.getString("message"), Toast.LENGTH_SHORT).show();
					AccessToken.createEmptyToken();
					close();
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.i("loginProcessCallback RESULT", json.toString());
		}
	}

	/**
	 * 토큰 갱신이 가능한지 여부를 반환한다.
	 * 토큰 갱신은 background로 사용자가 모르도록 진행한다.
	 *
	 * @return 토큰 갱신을 진행할 때는 true, 토큰 갱신을 하지 못할때는 false를 return 한다.
	 */
	public boolean implicitOpen() {
		if (isOpened() || (isOpenable() && accessToken.hasToken())) {
			open(null);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 명시적 강제 close(로그아웃/탈퇴). request중 인 것들은 모두 실패를 받게 된다.
	 * token을 삭제하기 때문에 authorization code부터(로그인 버튼) 다시 받아서 세션을 open 해야한다.
	 */
	public void close(final RewhiteException rewhiteException, final boolean forced) {
		synchronized (INSTANCE_LOCK) {
			final SessionState previous = state;
			state = SessionState.CLOSED;
			accessToken = AccessToken.createEmptyToken();
			SharedPreferencesUtility.logoutClear();
			onStateChange(previous, state, null, rewhiteException, forced);
		}
	}

	public void close() {
		RewhiteException sException = new RewhiteException(RewhiteException.ErrorType.AUTHORIZATION_FAILED, "SESSION AUTHORIZE");
		close(sException, true);
	}

	private void onStateChange(final SessionState previousState, final SessionState newState, final RequestType requestType,
			final RewhiteException exception, boolean forced) {
		if (!forced && (previousState == newState) && exception == null) {
			return;
		}

		DUtil.Log(
				"Rewhite Session",
				String.format("Session State changed : %s -> %s \n ex = %s, request_type = %s", previousState, newState, (exception != null ? ", ex="
						+ exception.getMessage() : ""), requestType));

		// 사용자에게 opening을 state를 알려줄 필요는 없는듯.
		if (newState.isOpenable()) return;

		notifySessionState(newState, exception);
	}

	private void notifySessionState(final SessionState newState, final RewhiteException exception) {
		final List<SessionCallback> dumpSessionCallbacks = new ArrayList<SessionCallback>(sessionCallbacks);
		Runnable runCallbacks = new Runnable() {

			public void run() {
				if (newState.isOpened()) {
					//registerTokenManger(DEFAULT_TOKEN_REQUEST_TIME_MILLIS);
				} else if (newState.isClosed()) {
					//deregisterTokenManger();
				}

				for (SessionCallback callback : dumpSessionCallbacks) {
					if (newState.isOpened()) {
						callback.onSessionOpened();
					} else if (newState.isClosed()) {
						callback.onSessionClosed(exception);
					}

				}
			}
		};
		//세션 callback은 main thread에서 호출되도록 한다.
		sessionCallbackHandler.post(runCallbacks);
	}

	/**
	 * 현재 세션이 가지고 있는 access token이 유효한지를 검사후 세션의 상태를 반환한다.
	 * 만료되었다면 opened 상태가 아닌 opening상태가 반환된다.
	 * 
	 * @return 세션의 상태
	 */
	public final SessionState checkState() {
		synchronized (INSTANCE_LOCK) {
			if (state.isOpened()) {
				synchronized (INSTANCE_LOCK) {
					state = SessionState.OPENABLE;
					//requestType = null;
					//authorizationCode = AuthorizationCode.createEmptyCode();
				}
			}
			return state;
		}
	}

	/**
	 * 현재 세션의 상태
	 * 
	 * @return 세션의 상태
	 */
	public SessionState getState() {
		synchronized (INSTANCE_LOCK) {
			return state;
		}
	}

	public static synchronized Session getCurrentSession() {
		if (currentSession == null) {
			throw new IllegalStateException("Session is not initialized. Call Session#initializeSession first.");
		}

		return currentSession;
	}

	/**
	 * 현재 세션이 열린 상태인지 여부를 반환한다.
	 * 
	 * @return 세션이 열린 상태라면 true, 그외의 경우 false를 반환한다.
	 */
	public final boolean isOpened() {
		final SessionState state = checkState();
		return state == SessionState.OPENED;
	}

	/**
	 * 현재 세션이 오픈중(갱신 포함) 상태인지 여부를 반환한다.
	 *
	 * @return 세션 오픈 진행 중이면 true, 그외 경우는 false를 반환한다.
	 */
	public boolean isOpenable() {
		final SessionState state = checkState();
		return state == SessionState.OPENABLE;
	}

	/**
	 * 현재 세션이 닫힌 상태인지 여부를 반환한다.
	 * 
	 * @return 세션이 닫힌 상태라면 true, 그외의 경우 false를 반환한다.
	 */
	public final boolean isClosed() {
		final SessionState state = checkState();
		return state == SessionState.CLOSED;
	}

	/**
	 * 현재 세션이 가지고 있는 access token을 반환한다.
	 * 
	 * @return access token
	 */
	public final String getAccessToken() {
		synchronized (INSTANCE_LOCK) {
			if (accessToken == null) {
				accessToken = AccessToken.createFromCache();
				return (accessToken.getAccessTokenString() == null) ? null : accessToken.getAccessTokenString();
			} else {
				return accessToken.getAccessTokenString();
			}

		}
	}

	/**
	 * 로그인 activity를 이용하여 sdk에서 필요로 하는 activity를 띄운다.
	 * 따라서 해당 activity의 결과를 로그인 activity가 받게 된다.
	 * 해당 결과를 세션이 받아서 다음 처리를 할 수 있도록 로그인 activity의 onActivityResult에서 해당 method를 호출한다.
	 * 
	 * @param requestCode
	 * @param resultCode
	 * @param data
	 * @return
	 */
	public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
		return requestCode == AUTHORIZATION_CODE_REQUEST;
	}

	/**
	 * 세션 상태 변화 콜백을 받고자 할때 콜백을 등록한다.
	 * 
	 * @param callback
	 *            추가할 세션 콜백
	 */
	public void addCallback(final SessionCallback callback) {
		synchronized (sessionCallbacks) {
			if (callback != null && !sessionCallbacks.contains(callback)) {
				sessionCallbacks.add(callback);
			}
		}
	}

	/**
	 * 더이상 세션 상태 변화 콜백을 받고 싶지 않을 때 삭제한다.
	 * 
	 * @param callback
	 *            삭제할 콜백
	 */
	public void removeCallback(final SessionCallback callback) {
		synchronized (sessionCallbacks) {
			if (callback != null) {
				sessionCallbacks.remove(callback);
			}
		}
	}

	private void clearCallbacks() {
		synchronized (sessionCallbacks) {
			sessionCallbacks.clear();
		}
	}

	public Context getContext() {
		return context;
	}

	/**
	 * @author Marines
	 * @Minwise
	 */
	private enum SessionState {
		/**
		 * memory와 cache에 session 정보가 없는 전혀 상태.
		 * 처음 session에 접근할 때 또는 session을 close(예를 들어 로그아웃, 탈퇴)한 상태.
		 * open({@link me.rewhite.delivery.Session.RequestType#GETTING_AUTHORIZATION_CODE}) : 성공 -
		 * {@link #OPENABLE}, 실패 - 그대로 CLOSED
		 * close(명시적 close) : 그대로 CLOSED
		 */
		CLOSED,
		/**
		 * {@link #CLOSED}상태에서 token을 발급 받기 위해 authorization code를 발급 받아 valid한 authorization code를
		 * 가지고 있는 상태.
		 * 또는 토큰이 만료되었으나 refresh token을 가지고 있는 상태.
		 * open({@link me.rewhite.delivery.Session.RequestType#GETTING_ACCESS_TOKEN} 또는
		 * {@link me.rewhite.delivery.Session.RequestType#REFRESHING_ACCESS_TOKEN}) : 성공 -
		 * {@link #OPENED}, 실패 - {@link #CLOSED} close(명시적 close) : {@link #CLOSED}
		 */
		OPENABLE,

		/**
		 * access token을 성공적으로 발급 받아 valid access token을 가지고 있는 상태.
		 * 토큰 만료 : {@link #OPENABLE} close(명시적 close) : {@link #CLOSED}
		 */
		OPENED,

		/**
		 * {@link #CLOSED} 상태에서 authcode를 요청하는 경우, code나 refresh token으로 token을 요청하는 경우 중간 단계.
		 *
		 */
		OPENING;

		private boolean isClosed() {
			return this == SessionState.CLOSED;
		}

		private boolean isOpenable() {
			return this == SessionState.OPENABLE;
		}

		private boolean isOpened() {
			return this == SessionState.OPENED;
		}

		private boolean isOpening() {
			return this == SessionState.OPENING;
		}
	}

	private enum RequestType {
		GETTING_AUTHORIZATION_CODE, GETTING_ACCESS_TOKEN, REFRESHING_ACCESS_TOKEN;

		private boolean isAuthorizationCodeRequest() {
			return this == RequestType.GETTING_AUTHORIZATION_CODE;
		}

		private boolean isAccessTokenRequest() {
			return this == RequestType.GETTING_ACCESS_TOKEN;
		}

		private boolean isRefreshingTokenRequest() {
			return this == RequestType.REFRESHING_ACCESS_TOKEN;
		}
	}

	@Override
	public void onAuthorizationCompletion(AuthorizationResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDialogPositiveClick(DialogFragment dialog, int mode, String param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog, int mode, String param) {
		// TODO Auto-generated method stub
		
	}
}
