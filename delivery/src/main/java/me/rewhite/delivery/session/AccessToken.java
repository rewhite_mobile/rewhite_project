package me.rewhite.delivery.session;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.StringUtil;


public class AccessToken implements Parcelable {

	private static final String ACCESS_TOKEN_KEY = "accessToken";
	private static final String USER_KEY = "loginId";
	//private static final String EMAIL_AUTH_CHECK = "email_auth_check";

	private static final String CACHE_ACCESS_TOKEN = "me.rewhite.delivery.token.AccessToken";
	private static final Date MIN_DATE = new Date(Long.MIN_VALUE);
	private static final Date MAX_DATE = new Date(Long.MAX_VALUE);
	private static final Date DEFAULT_EXPIRATION_TIME = MAX_DATE;
	private static final Date ALREADY_EXPIRED_EXPIRATION_TIME = MIN_DATE;

	private String accessTokenString;
	private String user_key;
	private String email_auth_check;

	public static AccessToken createEmptyToken() {
		DUtil.Log("AccessToken", "createEmptyToken");
		return new AccessToken("", "");
	}

	public static AccessToken createFromCache() {
		DUtil.Log("AccessToken", "createFromCache");
		final String accessToken = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN);
		final String userKey = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_ID);
		//final String emailAuthCheck = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL_AUTH_CHECK);
		//
		return new AccessToken(accessToken, userKey);
	}

	public static void clearAccessTokenFromCache() {
		SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN, "");
		SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_ID, "");

	}

	public static void saveAccessTokenToCache(final String accessTokenString, final String user_key) {
		SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN, accessTokenString);
		SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_ID, user_key);
	}

	public static AccessToken createFromResponse(final JSONObject resultObj) {
		String accessToken;
		String userKey;
		String emailAuthCheck;

		try {
			accessToken = resultObj.getJSONObject("data").getString(ACCESS_TOKEN_KEY);
			userKey = resultObj.getJSONObject("data").getString(USER_KEY);
			//emailAuthCheck = resultObj.getString(EMAIL_AUTH_CHECK);

			if (accessToken == null) return null;

			saveAccessTokenToCache(accessToken, userKey);
			// 일단 refresh token의 expires_in은 나중에
			return new AccessToken(accessToken, userKey);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	private AccessToken(final String accessTokenString, final String userKey) {
		this.accessTokenString = accessTokenString;
		this.user_key = userKey;

		DUtil.Log("AccessToken userKey", userKey);
	}

	// access token 갱신시에는 refresh token이 내려오지 않을 수도 있다.
	public void updateAccessToken(final AccessToken newAccessToken) {
		this.accessTokenString = newAccessToken.accessTokenString;
		this.user_key = newAccessToken.user_key;

		saveAccessTokenToCache(this.accessTokenString, this.user_key);
	}

	public String getAccessTokenString() {
		return this.accessTokenString;
	}

	public String getUserKey() {
		return this.user_key;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean hasToken() {
		return !StringUtil.isNullOrEmpty(this.accessTokenString);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(this.accessTokenString);
		dest.writeString(this.user_key);
		dest.writeString(this.email_auth_check);
	}

	public AccessToken(Parcel in) {
		this.accessTokenString = in.readString();
		this.user_key = in.readString();
		this.email_auth_check = in.readString();
	}

	public static final Parcelable.Creator<AccessToken> CREATOR = new Parcelable.Creator<AccessToken>() {

		public AccessToken createFromParcel(Parcel in) {
			return new AccessToken(in);
		}

		public AccessToken[] newArray(int size) {
			return new AccessToken[size];
		}
	};
}
