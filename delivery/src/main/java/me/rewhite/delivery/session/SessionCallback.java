package me.rewhite.delivery.session;

public interface SessionCallback {

	/**
	 * 세션의 상태 변경에 따른 콜백
	 * 세션이 오픈되었을 때, 세션이 만료되어 닫혔을 때 세션 콜백을 넘기게 된다.
	 */
	void onSessionOpened();

	/**
	 * memory와 cache에 session 정보가 전혀 없는 상태.
	 * 일반적으로 로그인 버튼이 보이고 사용자가 클릭시 동의를 받아 access token 요청을 시도한다.
	 * 
	 * @param exception
	 *            close된 이유가 에러가 발생한 경우에 해당 exception.
	 *
	 */
	void onSessionClosed(final RewhiteException exception);

	void onSessionOpening();
}
