package me.rewhite.delivery.session;


import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;


public class JoinMemberRequest extends AuthRequestBase {

	private String keyHash;
	private String mobileNumber;
	private String authNumber;
	private Context ctx;

	public static JoinMemberRequest createRequest(final Context context, final String email, final String password, final String mobileNumber,
			final String authNumber) {
		DUtil.Log("JoinMemberRequest createRequest", email + " / " + password + " / " + mobileNumber + " / " + authNumber);
		String ctn = mobileNumber.replace("-", "");
		return new JoinMemberRequest(context, email, password).setAuthNumber(authNumber).setMobileNumber(ctn);
	}

	private JoinMemberRequest(final Context context, final String email, final String password) {
		super(email, password, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));
		ctx = context;
		this.keyHash = DUtil.getKeyHash(context);
	}

	private JoinMemberRequest(final Parcel parcel) {
		readFromParcel(parcel);
	}

	public boolean isAccessTokenRequestWithAuthNumber() {
		return authNumber != null;
	}

	public Context getContext() {
		return ctx;
	}

	public String getKeyHash() {
		return keyHash;
	}

	public String getAuthNumber() {
		return authNumber;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	private JoinMemberRequest setAuthNumber(final String authNumber) {
		this.authNumber = authNumber;
		return this;
	}

	private JoinMemberRequest setMobileNumber(final String mobileNumber) {
		this.mobileNumber = mobileNumber;
		return this;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel parcel, final int i) {
		super.writeToParcel(parcel, i);
		parcel.writeString(keyHash);
		parcel.writeString(mobileNumber);
		parcel.writeString(authNumber);
	}

	public void readFromParcel(final Parcel parcel) {
		super.readFromParcel(parcel);
		keyHash = parcel.readString();
		mobileNumber = parcel.readString();
		authNumber = parcel.readString();
	}

	public static final Parcelable.Creator<JoinMemberRequest> CREATOR = new Parcelable.Creator<JoinMemberRequest>() {

		public JoinMemberRequest createFromParcel(Parcel in) {
			return new JoinMemberRequest(in);
		}

		public JoinMemberRequest[] newArray(int size) {
			return new JoinMemberRequest[size];
		}
	};
}
