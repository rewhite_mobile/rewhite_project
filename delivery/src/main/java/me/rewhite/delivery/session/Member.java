package me.rewhite.delivery.session;

import android.content.Context;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.common.logic.RewhiteCMD;
import me.rewhite.delivery.common.logic.RewhiteResult;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.SharedPreferencesUtility.UserInfo;


class SingletonEnforcer {
}

public class Member {

	private static Member _instance;
	Context ctx;
	AQuery aq;
	MemberInfoListener mListener = null;

	public static Member getInstance(Context _ctx) {
		if (_instance != null) {
			return _instance;
		} else {
			_instance = new Member(_ctx, new SingletonEnforcer());
			return _instance;
		}
	}

	private Member(Context _ctx, SingletonEnforcer _enforcer) {
		ctx = _ctx;
		aq = new AQuery(ctx);
	}

	public void setInterface(MemberInfoListener listener) {
		mListener = listener;
	}

	public void removeAccount(String _password) {

		DUtil.Log("removeAccount ", Constants.REMOVE_ACCOUNT);
		String url = CommonUtility.SERVER_URL + Constants.REMOVE_ACCOUNT;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("password", _password);

		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
		cb.url(url).type(JSONObject.class).params(params).weakHandler(this, "removeAccountCallback");
		cb.header("Authorization", "device_token='" + SharedPreferencesUtility.get(UserInfo.GCM_ID) + "',access_token='"
				+ AccessToken.createFromCache().getAccessTokenString() + "'");

		aq.ajax(cb);
	}

	public void changeNickname(String _newNickname) {
		DUtil.Log("removeAccount ", Constants.CHANGE_NICKNAME);
		String url = CommonUtility.SERVER_URL + Constants.CHANGE_NICKNAME;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("nickname", _newNickname);

		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
		cb.url(url).type(JSONObject.class).params(params).weakHandler(this, "changeNicknameCallback");
		cb.header("Authorization", "device_token='" + SharedPreferencesUtility.get(UserInfo.GCM_ID) + "',access_token='"
				+ AccessToken.createFromCache().getAccessTokenString() + "'");

		aq.ajax(cb);
	}

	public void changePassword(String _oldPassword, String _newPassword) {
		DUtil.Log("removeAccount ", Constants.CHANGE_PASSWORD);
		String url = CommonUtility.SERVER_URL + Constants.CHANGE_PASSWORD;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("password", _oldPassword);
		params.put("password_new", _newPassword);

		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
		cb.url(url).type(JSONObject.class).params(params).weakHandler(this, "changePasswordCallback");
		cb.header("Authorization", "device_token='" + SharedPreferencesUtility.get(UserInfo.GCM_ID) + "',access_token='"
				+ AccessToken.createFromCache().getAccessTokenString() + "'");

		aq.ajax(cb);
	}

	public void changePhoneNumber(String _newPhoneNumber, String _authNumber) {
		DUtil.Log("removeAccount ", Constants.CHANGE_PHONENO);
		String url = CommonUtility.SERVER_URL + Constants.CHANGE_PHONENO;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mobile_number", _newPhoneNumber);
		params.put("auth_number", _authNumber);

		AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
		cb.url(url).type(JSONObject.class).params(params).weakHandler(this, "changePhoneNumberCallback");
		cb.header("Authorization", "device_token='" + SharedPreferencesUtility.get(UserInfo.GCM_ID) + "',access_token='"
				+ AccessToken.createFromCache().getAccessTokenString() + "'");

		aq.ajax(cb);
	}

	public void requestPhoneAuth(String _phoneNumber) {
	    DUtil.Log("requestPhoneAuth ", Constants.REQUEST_CTN_AUTH);
	    String url = CommonUtility.SERVER_URL + Constants.REQUEST_CTN_AUTH;
	    Map<String, Object> params = new HashMap<String, Object>();
	    params.put("mobile_number", _phoneNumber);
	    
	    AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
	    cb.url(url).type(JSONObject.class).params(params).weakHandler(this, "requestPhoneAuthCallback");
	    aq.ajax(cb);
	}
	
	public void removeAccountCallback(String url, JSONObject json, AjaxStatus status) {
		if (status.getCode() >= 400) {
			// IF ERROR
			Log.i("removeAccountCallback StatusCode", status.getCode() + "");
			Log.i("removeAccountCallback StatusError", status.getError());
		} else {
			// IF NORMAL
			if (json == null) {
				Log.i("RETURN", "" + status.getMessage());
				if (mListener != null) {
					DUtil.Log("removeAccountCallback", "MOD FAILED");
					mListener.onResult(RewhiteCMD.MEMBER_REMOVE_ACCOUNT, RewhiteResult.FAILED);
				}
				return;
			}

			try {
				Log.i("removeAccountCallback >>", "" + json.toString());
				if (json.getBoolean("result")) {
					if (mListener != null) {
						DUtil.Log("removeAccountCallback", "MOD SUCCESS");
						mListener.onResult(RewhiteCMD.MEMBER_REMOVE_ACCOUNT, RewhiteResult.SUCCESS);
					}
				} else {
					if (mListener != null) {
						DUtil.Log("removeAccountCallback", "MOD FAILED");
						mListener.onResult(RewhiteCMD.MEMBER_REMOVE_ACCOUNT, RewhiteResult.FAILED);
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				if (mListener != null) {
					DUtil.Log("removeAccountCallback", "MOD FAILED");
					mListener.onResult(RewhiteCMD.MEMBER_REMOVE_ACCOUNT, RewhiteResult.FAILED);
				}
				e.printStackTrace();
			}
		}
	}

	public void changeNicknameCallback(String url, JSONObject json, AjaxStatus status) {
		if (status.getCode() >= 400) {
			// IF ERROR
			Log.i("changePasswordCallback StatusCode", status.getCode() + "");
			Log.i("changePasswordCallback StatusError", status.getError());
		} else {
			// IF NORMAL
			if (json == null) {
				Log.i("RETURN", "" + status.getMessage());
				if (mListener != null) {
					DUtil.Log("changeNicknameCallback", "MOD FAILED");
					mListener.onResult(RewhiteCMD.MEMBER_MOD_NICKNAME, RewhiteResult.FAILED);
				}
				return;
			}

			try {
				Log.i("changeNicknameCallback >>", "" + json.toString());
				if (json.getBoolean("result")) {
					if (mListener != null) {
						DUtil.Log("changeNicknameCallback", "MOD SUCCESS");
						mListener.onResult(RewhiteCMD.MEMBER_MOD_NICKNAME, RewhiteResult.SUCCESS);
					}
				} else {
					if (mListener != null) {
						DUtil.Log("changeNicknameCallback", "MOD FAILED");
						mListener.onResult(RewhiteCMD.MEMBER_MOD_NICKNAME, RewhiteResult.FAILED);
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				if (mListener != null) {
					DUtil.Log("changeNicknameCallback", "MOD FAILED");
					mListener.onResult(RewhiteCMD.MEMBER_MOD_NICKNAME, RewhiteResult.FAILED);
				}
				e.printStackTrace();
			}
		}

	}

	public void changePasswordCallback(String url, JSONObject json, AjaxStatus status) {
		if (status.getCode() >= 400) {
			// IF ERROR
			Log.i("changePasswordCallback StatusCode", status.getCode() + "");
			Log.i("changePasswordCallback StatusError", status.getError());
		} else {
			// IF NORMAL
			if (json == null) {
				Log.i("RETURN", "" + status.getMessage());
				if (mListener != null) {
					DUtil.Log("changePasswordCallback", "MOD FAILED");
					mListener.onResult(RewhiteCMD.MEMBER_MOD_PASSWORD, RewhiteResult.FAILED);
				}
				return;
			}

			try {
				Log.i("changePasswordCallback >>", "" + json.toString());
				if (json.getBoolean("result")) {
					if (mListener != null) {
						DUtil.Log("changePasswordCallback", "MOD SUCCESS");
						mListener.onResult(RewhiteCMD.MEMBER_MOD_PASSWORD, RewhiteResult.SUCCESS);
					}
				} else {
					if (mListener != null) {
						DUtil.Log("changePasswordCallback", "MOD FAILED");
						mListener.onResult(RewhiteCMD.MEMBER_MOD_PASSWORD, RewhiteResult.FAILED);
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				if (mListener != null) {
					DUtil.Log("changePasswordCallback", "MOD FAILED");
					mListener.onResult(RewhiteCMD.MEMBER_MOD_PASSWORD, RewhiteResult.FAILED);
				}
				e.printStackTrace();
			}
		}
	}

	public void changePhoneNumberCallback(String url, JSONObject json, AjaxStatus status) {
		if (status.getCode() >= 400) {
			// IF ERROR
			Log.i("changePhoneNumberCallback StatusCode", status.getCode() + "");
			Log.i("changePhoneNumberCallback StatusError", status.getError());
		} else {
			// IF NORMAL
			if (json == null) {
				Log.i("RETURN", "" + status.getMessage());
				if (mListener != null) {
					DUtil.Log("changePhoneNumberCallback", "MOD FAILED");
					mListener.onResult(RewhiteCMD.MEMBER_MOD_PHONENO, RewhiteResult.FAILED);
				}
				return;
			}

			try {
				Log.i("changePhoneNumberCallback >>", "" + json.toString());
				if (json.getBoolean("result")) {
					if (mListener != null) {
						DUtil.Log("changePhoneNumberCallback", "MOD SUCCESS");
						mListener.onResult(RewhiteCMD.MEMBER_MOD_PHONENO, RewhiteResult.SUCCESS);
					}
				} else {
					if (mListener != null) {
						DUtil.Log("changePhoneNumberCallback", "MOD FAILED");
						mListener.onResult(RewhiteCMD.MEMBER_MOD_PHONENO, RewhiteResult.FAILED);
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				if (mListener != null) {
					DUtil.Log("changePhoneNumberCallback", "MOD FAILED");
					mListener.onResult(RewhiteCMD.MEMBER_MOD_PHONENO, RewhiteResult.FAILED);
				}
				e.printStackTrace();
			}
		}
	}

	public void requestPhoneAuthCallback(String url, JSONObject json, AjaxStatus status) {
       if (status.getCode() >= 400) {
            // IF ERROR
            Log.i("removeAccountCallback StatusCode", status.getCode() + "");
            Log.i("removeAccountCallback StatusError", status.getError());
        } else {
            // IF NORMAL
            if (json == null) {
                Log.i("RETURN", "" + status.getMessage());
                if (mListener != null) {
                    DUtil.Log("requestPhoneAuthCallback", "MOD FAILED");
                    mListener.onResult(RewhiteCMD.MEMBER_AUTH_MOBILE, RewhiteResult.FAILED);
                }
                return;
            }

            try {
                Log.i("requestPhoneAuthCallback >>", "" + json.toString());
                if (json.getBoolean("result")) {
                    if (mListener != null) {
                        DUtil.Log("requestPhoneAuthCallback", "MOD SUCCESS");
                        mListener.onResult(RewhiteCMD.MEMBER_AUTH_MOBILE, RewhiteResult.SUCCESS);
                    }
                } else {
                    if (mListener != null) {
                        DUtil.Log("requestPhoneAuthCallback", "MOD FAILED");
                        mListener.onResult(RewhiteCMD.MEMBER_AUTH_MOBILE, RewhiteResult.FAILED);
                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                if (mListener != null) {
                    DUtil.Log("requestPhoneAuthCallback", "MOD FAILED");
                    mListener.onResult(RewhiteCMD.MEMBER_AUTH_MOBILE, RewhiteResult.FAILED);
                }
                e.printStackTrace();
            }
        }
    }
	    
	public interface MemberInfoListener {

		void onResult(String command, String result);
	}
}
