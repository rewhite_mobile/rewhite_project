package me.rewhite.delivery.session;

public class RewhiteException extends RuntimeException {

	private static final long serialVersionUID = 3738785191273730776L;
	private ErrorType errorType;

	public enum ErrorType {
		ILLEGAL_ARGUMENT, MISS_CONFIGURATION, CANCELED_OPERATION, AUTHORIZATION_FAILED, UNSPECIFIED_ERROR
	}

	public RewhiteException(final ErrorType errorType, final String msg) {
		super(msg);
		this.errorType = errorType;
	}

	public boolean isCancledOperation() {
		return errorType == ErrorType.CANCELED_OPERATION;
	}

	public RewhiteException(final String msg) {
		super(msg);
	}

	public RewhiteException(final Throwable t) {
		super(t);
	}

	public RewhiteException(final String s, final Throwable t) {
		super(s, t);
	}

	public ErrorType getErrorType() {
		return errorType;
	}

	public String getMessage() {
		if (errorType != null) return errorType.toString() + " : " + super.getMessage();
		else
			return super.getMessage();
	}
}
