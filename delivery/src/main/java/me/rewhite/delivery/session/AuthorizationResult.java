package me.rewhite.delivery.session;

import android.net.Uri;


public class AuthorizationResult {
	public enum RESULT_CODE {
        SUCCESS,
        CANCEL,
        PASS,   // 다음 방법으로 진행하라
        OAUTH_ERROR,
        ERROR
    }

    private final int requestCode;
    private final RESULT_CODE resultCode;
    private String redirectURL;
    private String accessToken;
    private String resultMessage;

    public static AuthorizationResult createSuccessAuthCodeResult(final String redirectURL) {
        return new AuthorizationResult(Session.AUTHORIZATION_CODE_REQUEST, RESULT_CODE.SUCCESS).setRedirectURL(redirectURL);
    }
/*
    public static AuthorizationResult createSuccessAccessTokenResult(final AccessToken accessToken) {
        return new AuthorizationResult(Session.ACCESS_TOKEN_REQUEST, RESULT_CODE.SUCCESS).setAccessToken(accessToken);
    }*/

    public static AuthorizationResult createCancelResult(int requestType, final String resultMessage) {
        return new AuthorizationResult(requestType, RESULT_CODE.CANCEL).setResultMessage(resultMessage);
    }

    public static AuthorizationResult createAuthCodeCancelResult(final String resultMessage) {
        return new AuthorizationResult(Session.AUTHORIZATION_CODE_REQUEST, RESULT_CODE.CANCEL).setResultMessage(resultMessage);
    }

    public static AuthorizationResult createAccessTokenOAuthErrorResult(final String resultMessage) {
        return new AuthorizationResult(Session.ACCESS_TOKEN_REQUEST, RESULT_CODE.OAUTH_ERROR).setResultMessage(resultMessage);
    }

    public static AuthorizationResult createAccessTokenErrorResult(final String resultMessage) {
        return new AuthorizationResult(Session.ACCESS_TOKEN_REQUEST, RESULT_CODE.ERROR).setResultMessage(resultMessage);
    }

    public static AuthorizationResult createAuthCodeOAuthErrorResult(final String resultMessage) {
        return new AuthorizationResult(Session.AUTHORIZATION_CODE_REQUEST, RESULT_CODE.OAUTH_ERROR).setResultMessage(resultMessage);
    }

    public static AuthorizationResult createAuthCodePassResult() {
        return new AuthorizationResult(Session.AUTHORIZATION_CODE_REQUEST, RESULT_CODE.PASS);
    }

    public boolean isSuccess() {
        return resultCode == RESULT_CODE.SUCCESS;
    }

    public boolean isCanceled() {
        return resultCode == RESULT_CODE.CANCEL;
    }

    public boolean isPass() {
        return resultCode == RESULT_CODE.PASS;
    }

    public boolean isAuthError() {
        return resultCode == RESULT_CODE.OAUTH_ERROR;
    }

    public boolean isError() {
        return resultCode == RESULT_CODE.ERROR;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public boolean isAuthorizationCodeRequest(){
        return requestCode == Session.AUTHORIZATION_CODE_REQUEST;
    }

    public boolean isAccessTokenRequest(){
        return requestCode == Session.ACCESS_TOKEN_REQUEST;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public Uri getRedirectUri() {
        if(redirectURL == null)
            return null;
        else
            return Uri.parse(redirectURL);
    }
/*
    public AccessToken getAccessToken() {
        return accessToken;
    }*/

    public String getResultMessage() {
        return resultMessage;
    }

    private AuthorizationResult(final int requestCode, final RESULT_CODE result) {
        this.requestCode = requestCode;
        this.resultCode = result;
    }

    private AuthorizationResult setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
        return this;
    }
/*
    private AuthorizationResult setAccessToken(AccessToken accessToken) {
        this.accessToken = accessToken;
        return this;
    }*/

    private AuthorizationResult setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
        return this;
    }
}
