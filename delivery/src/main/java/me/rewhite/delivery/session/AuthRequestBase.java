package me.rewhite.delivery.session;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


public class AuthRequestBase implements Parcelable {
	
	private String email;
    private String password;
    private String deviceToken;
    private Bundle extras;

    protected AuthRequestBase(final String email, final String password, final String deviceToken) {
        this.email = email;
        this.password = password;
        this.deviceToken = deviceToken;
    }

    protected AuthRequestBase() {
    }

    protected void addExtraString(final String key, final String value){
        if(extras == null){
            extras = new Bundle();
        }
        extras.putString(key, value);
    }

    public int getRequestCode() {
        return Session.AUTHORIZATION_CODE_REQUEST;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
    
    public String getDeviceToken() {
        return deviceToken;
    }

    public Bundle getExtras() { return extras; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(email);
        parcel.writeString(password);
        parcel.writeString(deviceToken);
        parcel.writeBundle(extras);
    }

    public void readFromParcel(final Parcel parcel) {
    	email = parcel.readString();
    	password = parcel.readString();
    	deviceToken = parcel.readString();
        extras = parcel.readBundle();
    }

}
