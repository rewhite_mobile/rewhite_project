package me.rewhite.delivery.common.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import me.rewhite.delivery.common.logic.RewhiteAuthenticator;

/**
 * Created by marines on 2017. 3. 31..
 */

public class AuthenticatorService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        RewhiteAuthenticator authenticator = new RewhiteAuthenticator(this);
        return authenticator.getIBinder();
    }
}
