package me.rewhite.delivery.util;

/**
 * Created by marines on 2017. 11. 1..
 */

public class BarcodeStringUtil {


    public static String getLocalOrderBarcodeString(int storeId, int orderId){
        // Local 주문번호
        String prefix = "01";
        //
        String storeIdString = String.format("%05d", storeId);
        String orderIdString = String.format("%09d", orderId);

        int checkDigit = checkSum(prefix+storeIdString+orderIdString);

        return prefix+storeIdString+orderIdString+checkDigit;
    }


    public static int checkSum(String code){
        int val=0;
        for(int i=0;i<code.length();i++){
            val+=((int)Integer.parseInt(code.charAt(i)+""))*((i%2==0)?1:3);
        }

        int checksum_digit = 10 - (val % 10);
        if (checksum_digit == 10) checksum_digit = 0;

        return checksum_digit;
    }
}
