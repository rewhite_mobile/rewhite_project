package me.rewhite.delivery.common.logic;

public class Constants {

	public static String PREF_NAME = "REWHITE_DELIVERY";
    public static String DAUM_API_KEY = "cb3a127554215958184db75e3e0ee7e2";
	public static String SK_APP_KEY = "40c77124-6111-3bf3-aa4c-bd520e132851";

	public static String PACKAGE_NAME = "me.rewhite.delivery";
	public static String DB_NAME = "";

	public static String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo2O4D/XuIHG8jcLhFMugq51r9l22kuOH/lnHVXv0y2yTtkVdLdU10Vq2XyDKeMedPyA4KF82GQgqwaMKEHv2Mo+Nf2wWGBfsRWsVfm4p8PzXKBaRAS61FQ6R6N1OPHp5Dk/+qzfvEXNzXWKOs9MlG22NMo/Led96zD9Y6K0TFghVWqDarfxK1YArBvnZmm6YkAocxI3fsebvLKStG6+qqkIXdWJeIFDDlYYEYBZtIAv308kYuzhAHSj5IKrHHSoQNKgiRIjcHlC5SWpa5k8cquOmS8+tvYov9+JIAgqZS6uWayrvVzxJhO3DL/NFb0zPHstyxTyVt1EGleCB4hyY8QIDAQAB";

	public static String DEBUG = "debug";
	public static String REAL = "real";
	public static String DEBUG_MODE = Constants.DEBUG;

	public static String RWWW_HOST = "https://www.rewhite.me";
	public static String TWWW_HOST = "http://twww.rewhite.me";
	public static String RAPI_HOST = "https://api.rewhite.me";
	public static String TAPI_HOST = "https://tapi.rewhite.me:443";
	public static String STORE_HOST = "http://www.rewhite.me/shop/";

	public static String WWW_HOST = (Constants.REAL.equals(DEBUG_MODE))?Constants.RWWW_HOST:Constants.TWWW_HOST;
	public static String API_HOST = (Constants.REAL.equals(DEBUG_MODE))?Constants.RAPI_HOST:Constants.TAPI_HOST;


	public static String VERSION_CHECK = "/Management/GetAppInfo";
	public static String MEMBER_REGISTER = "";
    public static String LOGIN = "/Delivery/Login";
    public static String AUTH_ACCESS_TOKEN = "/Delivery/VerifyAccessToken";
    public static String USER_INFO = "";
	public static String QNA_SUBMIT = "/Delivery/SetQNA";

	public static String SHOW_STORE_INFO = "/Delivery/StoreInformation";

	// GS25
	public static String SHOW_GS_ORDER_COUNT = "/v3/Delivery/PartnerOrderCount";
	public static String PARTNER_ORDER_LIST = "/v3/Delivery/PartnerOrderList";
	public static String PARTNER_ORDER_STATUS = "/v3/Delivery/PartnerOrderStatus";
	public static String PARTNER_ORDER_BOX_QUANTITY = "/v3/Delivery/PartnerOrderBoxQuantity";
	public static String PARTNER_PRICE = "/v3/Delivery/PartnerPriceForDelivery";

    // ORDER
	public static String PICKUP_INITIALIZE_DATA = "/v2/Delivery/PickupInitialzeData";
	public static String GET_LASTEST_TAG_NO = "/v2/Delivery/StoreTagId";
    public static String ORDER_SUMMARY_COUNT = "/Delivery/OrderStatusCountForDelivery";
    public static String ORDER_LIST = "/Delivery/OrderList";
    public static String UNPAID_LIST = "/v2/Delivery/OrderListReceivable";
    public static String ORDER_DETAIL = "/Delivery/OrderView";
    public static String ORDER_PICKUP_COMPLETE = "/Delivery/OrderAddItems";
	public static String ORDER_PICKUP_COMPLETE_V2 = "/v2/Delivery/OrderAddItems";
    public static String MOD_ORDER_STATUS = "/Delivery/OrderStatus";
	public static String NO_PAYMENT_STAY = "/Delivery/OrderStatusIsReceivable";
    public static String ORDER_PRICE_TABLE = "/Delivery/StorePrice";
	public static String ORDER_PRICE_TABLE_V2 = "/v2/Delivery/StorePrice";
	public static String ORDER_MOD = "/Delivery/OrderUpdate";
	public static String ORDER_PICKUP_MOD = "/v2/Delivery/OrderPickupRequestTimeUpdate";
	public static String ORDER_CREATE = "/v2/Delivery/ReOrder";
	public static String GET_ORDER_AVAILABLE_TIME = "/v2/Delivery/OrderAvailableTime";
	public static String REMOVE_ORDER_ITEM_ELEMENT = "/v2/Delivery/OrderItemsUpdate";
	public static String APPEND_ORDER_ITEM_ELEMENT = "/v2/Delivery/OrderAddItem";


	public static String UPLOAD_IMAGE = "/v2/Delivery/PickupImage";
	public static String TAG_ADD_PICTURES = "/v2/Delivery/OrderAddPhotoAfterItems";
	public static String TAG_REMOVE_PICTURES = "/v2/Delivery/OrderRemovePhoto";

	// SNS Login
	public static String API_USER_AUTHORIZE = "";

	public static String REMOVE_ACCOUNT = "";
	public static String CHANGE_NICKNAME = "";
	public static String CHANGE_PASSWORD = "";
	public static String CHANGE_PHONENO = "";
	public static String REQUEST_CTN_AUTH = "";

	public static String HTML_NOTICE = "https://www.rewhite.me/customer/notice?s=y";
	public static String HTML_FAQ = "https://www.rewhite.me/customer/faq";
	public static String TERMS_USE = WWW_HOST + "/customer/tosshop";
	public static String TERMS_PRIVACY = WWW_HOST + "/customer/privacyshop";
	public static String TERMS_PRIVATE_SUPPORT = WWW_HOST + "/customer/agreementthird";
	public static String TERMS_LOCATION = WWW_HOST + "/customer/location";
	public static String TERMS_OSS = WWW_HOST + "/customer/opensource";

	// SHOP+
	// USER
	public static String USER_SEARCH = "/v2/Shop/UserFranchiseeList";
	public static String USER_ADD = "/v2/Shop/UserFranchiseeProc";
	public static String USER_MOD = "/v2/Shop/UserFranchiseeUpdate";
	public static String GET_ORDER_DATA_BY_USERID = "/v2/Shop/OrderListForFranchisee";
	public static String GET_STORE_INFORMATION = "/v2/Shop/StoreInformation";
	public static String GET_REWHITE_FINANCE = "/v2/Shop/CalculateList";
	public static String GET_LOCAL_FINANCE = "/v2/Shop/FranchiseeCalculateList";
	public static String ADD_ORDER_DIRECT = "/v2/Shop/OrderDirectForFranchisee";
	public static String OUT_PAY_ORDER_DIRECT = "/v2/Shop/OrderDirectPaymentForFranchisee";
    public static String OUT_ORDER_DIRECT = "/v2/Shop/OrderDirectReleaseForFranchisee";
	public static String CANCEL_LOCAL_ORDER = "/v2/Shop/OrderDirectCancelForFranchisee";

	public static String SHOW_MONTHLY_FINANCE = "https://shop.rewhite.me/external/storecalculatemonthly";
	public static String SHOW_NOTICE = "/v2/Shop/Notice";
	public static String SHOW_FAQ = "/v2/Shop/Faq";
	public static String GET_PRICE_LIST = "/v2/Shop/StorePriceList";
	public static String GET_PARTS_PRICE_LIST = "/v2/Shop/ItemPartV2List";

	public static String GET_ITEM_CATEGORY = "/v2/Shop/ItemClassIncludeCategory";
	public static String ADD_PRICE_ITEM = "/v2/Shop/ItemV2Proc";
	public static String SET_PRICE_ITEM = "/v2/Shop/StorePriceProc";
	public static String SET_PARTS_ITEM = "/v2/Shop/ItemPartV2Proc";

	// Partner
	public static String SEND_T_PHONE = "/v2/Delivery/SendTPhone";
	public static String SET_UPLUS_070_CALLBACK = "https://icentrex.uplus.co.kr/RestApi/setringcallback";
	public static String UPLUS_070_CALLBACK = "/v2/partner/upluscallback/";
}
