package me.rewhite.delivery.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

import me.rewhite.delivery.R;


public class DialogUtil {
    public static final String KEY_NP_AMPM = "np_ampm";
    public static final String KEY_NP_TIME = "np_time";
    public static final String KEY_DATA = "np_data";
    public static final String KEY_DATE = "date";

    private static Date mDate;

    public static Dialog showDateDialog(Context context, long time, final View.OnClickListener okListener) {

        Context contextThemeWrapper = new ContextThemeWrapper(
                context,
                android.R.style.Theme_Holo_Light);

        LayoutInflater localInflater = ((Activity) context).getLayoutInflater().cloneInContext(contextThemeWrapper);

        View view = localInflater.inflate(R.layout.layout_dialog_date, null);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        dialog.getWindow().getAttributes().y = context.getResources().getDimensionPixelSize(R.dimen.dialog_date_pos_y);     // TODO : 해당 좌표 변경 필요
//        WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
//        params.width = WindowManager.LayoutParams.MATCH_PARENT;
//        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        dialog.getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);

        dialog.setContentView(view);
        dialog.show();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime((time == -1) ? new Date() : new Date(time));
        mDate = calendar.getTime();

        final TextView d_textview = (TextView) view.findViewById(R.id.d_textview);
        try {
            d_textview.setText(time == -1 ? context.getResources().getString(R.string.select_date)
                    : TimeUtil.getDateTimeType5Format(calendar.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        final DatePicker picker = (DatePicker) view.findViewById(R.id.date_picker);
//        picker.setMinDate();
//        picker.setMaxDate();

        picker.init(calendar.get(calendar.YEAR), calendar.get(calendar.MONTH), calendar.get(calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker datePicker, int _year, int _month, int _day) {
                Log.d("debug", "i : " + _year + " , i1 : " + _month + " , i2 : " + _day);

                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, _year);
                calendar.set(Calendar.MONTH, _month );
                calendar.set(Calendar.DATE, _day);

                mDate = calendar.getTime();
            }
        });

        Button okBtn = (Button) view.findViewById(R.id.btn_ok);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                if (okListener != null) {
                    Log.d("debug", "ok click : " + mDate.toString());
                    args.putLong(KEY_DATE, mDate.getTime());
                    v.setTag(args);
                    okListener.onClick(v);
                }
                dialog.dismiss();
            }
        });

        return dialog;
    }

    public static Dialog showTimeDialog(Context context, int ampmValue, int timeValue, final View.OnClickListener okListener) {

        Context contextThemeWrapper = new ContextThemeWrapper(
                context,
                android.R.style.Theme_Holo_Light);

        LayoutInflater localInflater = ((Activity) context).getLayoutInflater().cloneInContext(contextThemeWrapper);

        View view = localInflater.inflate(R.layout.layout_dialog_time, null);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        dialog.getWindow().getAttributes().y = context.getResources().getDimensionPixelSize(R.dimen.dialog_time_pos_y);     // TODO : 해당 좌표 변경 필요
        dialog.setContentView(view);
        dialog.show();

        String[] ampmArray = context.getResources().getStringArray(R.array.np_ampm_arr);
        String[] timeArray = context.getResources().getStringArray(R.array.np_time_arr);

        final TextView tTextView = (TextView) view.findViewById(R.id.t_textview);
        tTextView.setText(ampmValue == -1 ? context.getResources().getString(R.string.select_time)
                : ampmArray[ampmValue] + " " + timeArray[timeValue]);

        final NumberPicker timeAMPMPicker = (NumberPicker) view.findViewById(R.id.np_ampm);
        timeAMPMPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        timeAMPMPicker.setMinValue(ampmValue <= 0 ? 0 : ampmValue);
        timeAMPMPicker.setMaxValue(ampmArray.length - 1);
        timeAMPMPicker.setDisplayedValues(ampmArray);
        timeAMPMPicker.setValue(timeValue <= 0 ? 0 : timeValue);
        timeAMPMPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal)
            {
            }
        });

        final NumberPicker timePicker = (NumberPicker) view.findViewById(R.id.np_time);
        timePicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        timePicker.setMinValue(0);
        timePicker.setMaxValue(timeArray.length - 1);
        timePicker.setDisplayedValues(timeArray);
        timePicker.setValue(0);  // TODO: 설정값으로 바뀌줘야 함
        timePicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal)
            {
            }
        });

        Button okBtn = (Button) view.findViewById(R.id.btn_ok);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                if (okListener != null) {
                    Log.d("debug", timeAMPMPicker.getValue() + "ok click : " + timePicker.getValue());
                    args.putInt(KEY_NP_AMPM, timeAMPMPicker.getValue());
                    args.putInt(KEY_NP_TIME, timePicker.getValue());
                    v.setTag(args);
                    okListener.onClick(v);
                }
                dialog.dismiss();
            }
        });

        return dialog;
    }

    public static Dialog showCustomDialog(Context context, String[] array, int selValue, String titleValue, int iconResId, final View.OnClickListener okListener) {
        return DialogUtil.showCustomDialog(context, array, selValue, titleValue, iconResId, R.dimen.dialog_time_pos_y, okListener);
    }

    public static Dialog showCustomDialog(Context context, final String[] array, int selValue, String titleValue, int iconResId, int posYDP, final View.OnClickListener okListener) {

        Context contextThemeWrapper = new ContextThemeWrapper(
                context,
                android.R.style.Theme_Holo_Light);

        LayoutInflater localInflater = ((Activity) context).getLayoutInflater().cloneInContext(contextThemeWrapper);

        View view = localInflater.inflate(R.layout.layout_dialog_string, null);

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        //dialog.getWindow().getAttributes().y = context.getResources().getDimensionPixelSize(R.dimen.dialog_time_pos_y);     // TODO : 해당 좌표 변경 필요
        dialog.getWindow().getAttributes().y = context.getResources().getDimensionPixelSize(posYDP);     // TODO : 해당 좌표 변경 필요
        dialog.setContentView(view);
        dialog.show();

        final ImageView iconView = (ImageView)view.findViewById(R.id.icon_step2_time);
        iconView.setImageResource(iconResId);

        final TextView tTextView = (TextView) view.findViewById(R.id.t_textview);
        tTextView.setText(selValue == -1 ? titleValue
                : array[selValue]);

        final NumberPicker customPicker = (NumberPicker) view.findViewById(R.id.np_data);
        customPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        customPicker.setMinValue(0);
        customPicker.setMaxValue(array.length - 1);
        customPicker.setDisplayedValues(array);
        customPicker.setValue(selValue <= 0 ? 0 : selValue);
        customPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal)
            {
                tTextView.setText(array[newVal]);
            }
        });

        Button okBtn = (Button) view.findViewById(R.id.btn_ok);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                if (okListener != null) {
                    Log.d("debug", "ok click : " + customPicker.getValue());
                    args.putInt(KEY_DATA, customPicker.getValue());

                    v.setTag(args);
                    okListener.onClick(v);
                }
                dialog.dismiss();
            }
        });

        return dialog;
    }

}