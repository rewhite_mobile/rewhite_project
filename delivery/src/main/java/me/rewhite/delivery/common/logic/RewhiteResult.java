package me.rewhite.delivery.common.logic;

public class RewhiteResult {
	
	public static final String INIT = "init";
    public static final String COMPLETED = "completed";
    
    public static final String SUCCESS = "success";
    public static final String FAILED = "failed";

}
