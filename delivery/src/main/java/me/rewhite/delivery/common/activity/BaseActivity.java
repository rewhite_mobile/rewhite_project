package me.rewhite.delivery.common.activity;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import me.rewhite.delivery.Bootstrap;
import me.rewhite.delivery.R;

/**
 * Created by marines on 2015. 10. 22..
 */
public class BaseActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback{
    protected static Activity self;
    View decorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            //window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }

        //decorView = getWindow().getDecorView();
        //decorView.setSystemUiVisibility(View.SYSTEM_UI_LAYOUT_FLAGS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Bootstrap.setCurrentActivity(this);
        self = BaseActivity.this;
    }

    @Override
    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    private void clearReferences() {
        Activity currActivity = Bootstrap.getCurrentActivity();
        if (currActivity != null && currActivity.equals(this)) {
            Bootstrap.setCurrentActivity(null);
        }
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        char pressedKey = (char) event.getUnicodeChar();
//        String Barcode += "" + String.valueOf(pressedKey);
//        Toast.makeText(getApplicationContext(), "barcode--->>>" + Barcode, 1)
//                .show();
//        return true;
//    }
}
