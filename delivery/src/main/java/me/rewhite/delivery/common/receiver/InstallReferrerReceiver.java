package me.rewhite.delivery.common.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.analytics.CampaignTrackingReceiver;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.delivery.Bootstrap;


/**
 * Created by marines on 2015. 11. 10..
 */
public class InstallReferrerReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String referrerString = "";
        if (intent.getAction().equals("com.android.vending.INSTALL_REFERRER")) {
            //Bundle extras = intent.getExtras();
            //referrerString = extras.getString("referrer");

            String referrer = intent.getStringExtra("referrer");
            Log.d("Referrer", "== REFERRER : " + referrer);

            Tracker t = ((Bootstrap) context.getApplicationContext()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
            t.send(new HitBuilders.EventBuilder()
                    .setCategory("APP_INSTALL_REFER_Android")
                    .setAction("Referral")
                    .setLabel(referrer)
                    .build());

            new CampaignTrackingReceiver().onReceive(context, intent);
        }
    }
}
