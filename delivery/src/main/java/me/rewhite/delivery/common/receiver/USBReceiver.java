package me.rewhite.delivery.common.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import me.rewhite.delivery.util.DUtil;

/**
 * Created by marines on 2017. 8. 21..
 */

public class USBReceiver extends BroadcastReceiver {

    private Context mContext = null;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras().getBoolean("connected")) {
            //do your stuff
            DUtil.Log("USBReceiver onReceive", "connected");
        }
    }


}
