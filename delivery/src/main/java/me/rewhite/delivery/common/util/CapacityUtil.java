package me.rewhite.delivery.util;

import android.os.Environment;
import android.os.StatFs;

import java.io.File;
import java.text.DecimalFormat;

/**
 * Created by marines on 2017. 3. 30..
 */

public class CapacityUtil {

    private static String TAG = "StorageStatus";

    public static String SHOW_MEMORYSIZE()
    {
        String ret = "=== MemoryStatus ===\n";
        //ret += "Total Internal MemorySize : " + FormatSize(GetTotalInternalMemorySize()) + "\n";
        //ret += "Available Internal MemorySize : " + FormatSize(GetAvailableInternalMemorySize()) + "\n";

        DecimalFormat precision = new DecimalFormat("0.0");
        ret += "Internal Storage Available rate : " + precision.format(getRate(GetAvailableInternalMemorySize(),GetTotalInternalMemorySize())*100)  + "%\n";

        if(IsExternalMemoryAvailable())
        {
            //ret += "Total External MemorySize : " + FormatSize(GetTotalExternalMemorySize()) + "\n";
            //ret += "Available External MemorySize : " + FormatSize(GetAvailableExternalMemorySize()) + "\n";

            ret += "External Storage Available rate : " + precision.format(getRate(GetAvailableExternalMemorySize(),GetTotalExternalMemorySize())*100)  + "%";
        }
        return ret;
    }

    private static double getRate(float start, float end){
        return (double)(start/end);
    }

    private static boolean IsExternalMemoryAvailable()
    {
        return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
    }

    private static long GetTotalInternalMemorySize()
    {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();

        return totalBlocks * blockSize;
    }

    private static long GetAvailableInternalMemorySize()
    {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();

        return availableBlocks * blockSize;
    }

    private static long GetTotalExternalMemorySize()
    {
        if(IsExternalMemoryAvailable())
        {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long totalBlocks = stat.getBlockCount();

            return totalBlocks * blockSize;
        }
        else
        {
            return -1;
        }
    }

    private static long GetAvailableExternalMemorySize()
    {
        if(IsExternalMemoryAvailable())
        {
            File path = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSize();
            long availableBlocks = stat.getAvailableBlocks();

            return availableBlocks * blockSize;
        }
        else
        {
            return -1;
        }
    }

    private static String FormatSize(long size)
    {
        String suffix = null;

        if (size >= 1024)
        {
            suffix = "KB";
            size /= 1024;
            if (size >= 1024)
            {
                suffix = "MB";
                size /= 1024;
            }
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0)
        {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }

        if (suffix != null)
        {
            resultBuffer.append(suffix);
        }

        return resultBuffer.toString();
    }
}
