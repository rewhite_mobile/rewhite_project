package me.rewhite.delivery.util;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

import me.rewhite.delivery.common.util.Logger;

/**
 * Created by marines on 15. 6. 23..
 */
public class ToastUtility {
    private static android.widget.Toast mToast;
    private static Toast sToast = null;
    private static final String TAG = "ToastUtil";

    public static void show(Context context, int resourceId) {
        show(context, resourceId, android.widget.Toast.LENGTH_SHORT);
    }

    public static void show(Context context, int resourceId, int length) {
        if (mToast == null) {
            mToast = android.widget.Toast.makeText(context, resourceId, length);
        } else {
            mToast.setText(resourceId);
        }

        mToast.show();
    }

    public static void show(Context context, CharSequence text) {
        show(context, text, android.widget.Toast.LENGTH_SHORT);
    }

    public static void show(Context context, CharSequence text, int length) {
        if (mToast == null) {
            mToast = android.widget.Toast.makeText(context, text, length);
        } else {
            mToast.setText(text);
        }

        mToast.show();
    }
    /**
     * 토스트 중앙표시
     * @param context
     * @param text
     * @param duration
     */
    public static void showMakeText(Context context, CharSequence text, int duration) {
        showMakeText(context, text, duration, Gravity.CENTER);
    }

    /**
     * 토스트 하단표시
     * @param context
     * @param text
     * @param duration
     */
    public static void showMakeTextBottom(Context context, CharSequence text, int duration) {
        showMakeText(context, text, duration, Gravity.BOTTOM, 0, 200);
    }

    /**
     * 토스트 기본표시(중앙)
     * @param context
     * @param resId
     * @param duration
     */
    public static void showMakeText(Context context, int resId, int duration) {
        showMakeText(context, context.getResources().getString(resId), duration);
    }

    public static void showMakeText(Context context, CharSequence text, int duration, int gravity) {
        showMakeText(context, text, duration, gravity, 0, 0);
    }

    public static void showMakeText(Context context, CharSequence text, int duration, int gravity, int xOffset, int yOffset) {
        try {
            if (sToast != null) {
                sToast.setText(text);
                sToast.setDuration(duration);
            } else {
                sToast = Toast.makeText(context.getApplicationContext(), text, duration);
            }
            sToast.setGravity(gravity, xOffset, yOffset);
            sToast.show();
        } catch (Exception e) {
            Logger.e(TAG, "showMakeText err.", e);
        }
    }
}

