package me.rewhite.delivery.common.activity;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import me.rewhite.delivery.Bootstrap;
import me.rewhite.delivery.R;

/**
 * Created by marines on 2015. 10. 22..
 */
public class TabletBaseActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback{
    protected static Activity self;
    View decorView;

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
//    }

    private static final String TYPEFACE_NANUMBARUNGOTHIC_PATH = "font/NanumBarunGothic.ttf";
    private static final String TYPEFACE_NANUMBARUNGOTHIC_LIGHT_PATH = "font/NanumBarunGothicLight.ttf";
    private static final String TYPEFACE_NANUMBARUNGOTHIC_ULTRA_LIGHT_PATH = "font/NanumBarunGothicUltraLight.ttf";
    private static final String TYPEFACE_NANUMBARUNGOTHIC_BOLD_PATH = "font/NanumBarunGothicBold.ttf";

    @Override
    public void setContentView(View view)
    {
        super.setContentView(view);

        //FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), TYPEFACE_NANUMBARUNGOTHIC_LIGHT_PATH);
        //fontChanger.replaceFonts((ViewGroup)this.findViewById(android.R.id.content));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            //window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        }

        //decorView = getWindow().getDecorView();
        //decorView.setSystemUiVisibility(View.SYSTEM_UI_LAYOUT_FLAGS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Bootstrap.setCurrentActivity(this);
        self = TabletBaseActivity.this;
    }

    @Override
    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    private void clearReferences() {
        Activity currActivity = Bootstrap.getCurrentActivity();
        if (currActivity != null && currActivity.equals(this)) {
            Bootstrap.setCurrentActivity(null);
        }
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        char pressedKey = (char) event.getUnicodeChar();
//        String Barcode += "" + String.valueOf(pressedKey);
//        Toast.makeText(getApplicationContext(), "barcode--->>>" + Barcode, 1)
//                .show();
//        return true;
//    }
}
