package me.rewhite.delivery.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

/**
 * Created by marines on 16. 7. 25..
 */
public class MZCrypto {

    public static String key = "me.rewhite.shopp";
    /**
     * AES 방식의 암호화
     *
     * @param message
     * @return
     * @throws Exception
     */
    public static String encrypt(String message) {
        // KeyGenerator kgen = KeyGenerator.getInstance("AES");
        // kgen.init(128);
        // use key coss2
        byte[] encrypted = null;
        String temp = null;

        try{
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");

            // Instantiate the cipher
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

            encrypted = cipher.doFinal(message.getBytes());

            temp = byteArrayToHex(encrypted);
        }catch(Exception e){
            encrypted = null;
            temp = null;
            e.printStackTrace();
        }

        return temp;
    }

    /**
     * AES 방식의 복호화
     *
     * @param message
     * @return
     * @throws Exception
     */
    public static String decrypt(String encrypted)  {
        // KeyGenerator kgen = KeyGenerator.getInstance("AES");
        // kgen.init(128);
        // use key coss2
        byte[] original = null;
        String temp = null;

        try{
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);

            original = cipher.doFinal(hexToByteArray(encrypted));

            temp = new String(original);
        }catch(Exception e){
            original = null;
            temp = null;
            e.printStackTrace();
        }

        return temp;
    }

    /**
     * Base64 인코딩
     */
    public static String getBase64encode(String content){
        return Base64.encodeToString(content.getBytes(), 0);
    }

    /**
     * Base64 디코딩
     */
    public static String getBase64decode(String content){
        return new String(Base64.decode(content, 0));
    }

    public static String makeSha512Key(String str) {
        MessageDigest sha512;
        String tempEncrypted = "";

        try {
            sha512 = MessageDigest.getInstance("SHA-512");
            sha512.update(str.getBytes());

            byte[] mb = sha512.digest();
            for (int i = 0; i < mb.length; i++) {
                byte temp = mb[i];
                String s = Integer.toHexString(new Byte(temp));
                while (s.length() < 2) {
                    s = "0" + s;
                }
                s = s.substring(s.length() - 2);
                tempEncrypted += s;
            }
        } catch (NoSuchAlgorithmException e) {
            return str + "-" + System.currentTimeMillis();
        }


        return tempEncrypted;
    }

    /**
     * function md5 encryption for passwords
     *
     * @param password
     * @return passwordEncrypted
     */
    public static final String md5(final String password) {
        try {

            MessageDigest digest = java.security.MessageDigest
                    .getInstance("MD5");
            digest.update(password.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (byte element : messageDigest) {
                String h = Integer.toHexString(0xFF & element);
                while (h.length() < 2) {
                    h = "0" + h;
                }
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
    // 문자열을 헥사 스트링으로 변환하는 메서드
    public static String stringToHex(String s) {
        String result = "";

        for (int i = 0; i < s.length(); i++) {
            result += String.format("%02X", (int) s.charAt(i));
        }

        return result;
    }
    /**
     * hex to byte[] : 16진수 문자열을 바이트 배열로 변환한다.
     *
     * @param hex
     *            hex string
     * @return
     */
    public static byte[] hexToByteArray(String hex) {
        if (hex == null || hex.length() == 0) {
            return null;
        }

        byte[] ba = new byte[hex.length() / 2];
        for (int i = 0; i < ba.length; i++) {
            ba[i] = (byte) Integer
                    .parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return ba;
    }

    /**
     * byte[] to hex : unsigned byte(바이트) 배열을 16진수 문자열로 바꾼다.
     *
     * @param ba
     *            byte[]
     * @return
     */
    public static String byteArrayToHex(byte[] ba) {
        if (ba == null || ba.length == 0) {
            return null;
        }

        StringBuffer sb = new StringBuffer(ba.length * 2);
        String hexNumber;
        for (byte element : ba) {
            hexNumber = "0" + Integer.toHexString(0xff & element);

            sb.append(hexNumber.substring(hexNumber.length() - 2));
        }
        return sb.toString();
    }

    public static byte getBCC(byte[] _source){
        byte LRC = 0;
        byte tbcc = 0x0;
        byte text_8bit;

        for (int i = 0; i < _source.length; i++) {
            text_8bit = _source[i];

            LRC = (byte)(LRC ^ text_8bit);
        }

        tbcc = (byte)(LRC | 0x20);

        return tbcc;
    }

    /**
     * RSA 방식의 암호화
     *
     * @param message
     * @return
     * @throws Exception
     */
    public static String RSAencrypt(InputStream is, String original)
            throws Exception {

        // InputStream instream = new FileInputStream(filePath);
        byte[] encodedKey = new byte[is.available()];
        is.read(encodedKey);

        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedKey);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PublicKey pkPublic = kf.generatePublic(publicKeySpec);

        Cipher pkCipher = Cipher
                .getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding");
        pkCipher.init(Cipher.ENCRYPT_MODE, pkPublic);
        byte[] encryptedInByte = pkCipher.doFinal(original.getBytes("UTF-8"));

        return byteArrayToHex(encryptedInByte);
    }

    /**
     *      * 지정된 비밀키를 가지고 오는 메서드      * @param keyValue : 암호화키값      * @return
     *      * @throws Exception      
     */
    public static Key getTripleKey(String keyValue) throws Exception {

        // Security.addProvider(new BouncyCastleProvider());

        keyValue = keyValue + keyValue.substring(0, 16);
        // Log.i("getTripleKey ", keyValue);
		/*
		 * byte[] keyPaddingBytes = new byte[8]; byte[] originKey =
		 * keyValue.getBytes("utf-8"); byte[] key8 = new byte[8]; for (int i =
		 * 0; i < 8; i++) { key8[i] = originKey[i]; } byte[] key24 = new
		 * byte[24]; key24 = concatenateByteArrays(keyValue.getBytes("utf-8"),
		 * key8); Log.i("key ", keyValue); logBytes(key24, "key24");
		 */
        // SecretKeySpec key = new
        // SecretKeySpec(concatenateByteArrays(keyValue.getBytes("utf-8"),
        // keyPaddingBytes), "DESede");
        // SecretKey keySpec = new
        // SecretKeySpec(UnicodeFormatter.hexToByteArray(keyValue), "DESede");
        DESedeKeySpec desKeySpec = new DESedeKeySpec(
                UnicodeFormatter.hexToByteArray(keyValue));
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
        Key rtkey = keyFactory.generateSecret(desKeySpec);
        return rtkey;
    }

    public static void logBytes(byte[] src, String tag) {
        String logs = "";
        for (byte a : src) {
            logs = logs + "0x" + UnicodeFormatter.byteToHex(a) + " ";
        }
        Log.i(tag, logs);
    }

    static byte[] concatenateByteArrays(byte[] a, byte[] b) {

        byte[] result = new byte[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    public static byte[] intToBytes(final int i) {
        ByteBuffer bb = ByteBuffer.allocate(4);
        bb.putInt(i);
        return bb.array();
    }

    // 1 BIT only
    public static int byteArrayToInt(byte[] b) {
        return (b[0] & 0xFF);
    }

    public static byte[] xor(final byte[] input, final byte[] secret) {
        final byte[] output = new byte[input.length];
        if (secret.length == 0) {
            throw new IllegalArgumentException("empty security key");
        }
        int spos = 0;
        for (int pos = 0; pos < input.length; ++pos) {
            output[pos] = (byte) (input[pos] ^ secret[spos]);
            ++spos;
            if (spos >= secret.length) {
                spos = 0;
            }
        }
        return output;
    }

}
