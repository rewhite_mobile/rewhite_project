package me.rewhite.delivery.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.text.TextPaint;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import me.rewhite.delivery.R;


@SuppressLint("NewApi")
public class DUtil {

	public static final boolean DEBUG = true;
	private static final String SAVE_TAG = "Audi";
	private static final String TAG = "Daoneday";
	private static Handler handler = new Handler();
	private static CustomProgressDialog progressDialog;

	public static void Log(String paramString1, String paramString2) {
		if (DUtil.DEBUG && paramString1 != null && paramString2 != null) {
			android.util.Log.i(paramString1, paramString2);
		}
	}

	public static void alertShow(Activity _mActivity, String messageString) {
		if(!_mActivity.isFinishing()) {
			AlertDialog.Builder localBuilder = new AlertDialog.Builder(_mActivity);
			localBuilder.setTitle("알림");
			localBuilder.setMessage(messageString);
			localBuilder.setPositiveButton("닫기", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt) {
					paramAnonymousDialogInterface.dismiss();
				}
			});
			localBuilder.show();
		}
	}

	public static String applyNewLineFix(TextView paramTextView, int paramInt, String paramString) {
		TextPaint localTextPaint = paramTextView.getPaint();
		StringBuilder localStringBuilder = new StringBuilder();
		int i;
		do {
			i = localTextPaint.breakText(paramString, true, paramInt, null);
			if (i > 0) {
				localStringBuilder.append(paramString.substring(0, i)).append("\n");
				paramString = paramString.substring(i);
			}
		} while (i > 0);
		return localStringBuilder.toString().substring(0, -1 + localStringBuilder.length());
	}

	public static void changeAllButton(Button[] paramArrayOfButton) {
		int i = -1;
		while (true) {
			i++;
			if (i >= paramArrayOfButton.length) return;
			paramArrayOfButton[i].setSelected(false);
		}
	}

	public static void changeList(ListView[] paramArrayOfListView, int paramInt) {
		int i = -1;
		while (true) {
			i++;
			if (i >= paramArrayOfListView.length) return;
			if (paramInt != i) paramArrayOfListView[i].setVisibility(4);
			else
				paramArrayOfListView[i].setVisibility(0);
		}
	}

	public static void changeTextView(TextView[] paramArrayOfTextView, int paramInt) {
		int i = -1;
		while (true) {
			i++;
			if (i >= paramArrayOfTextView.length) return;
			if (paramInt != i) paramArrayOfTextView[i].setSelected(false);
			else
				paramArrayOfTextView[i].setSelected(true);
		}
	}

	public static void changeView(View[] paramArrayOfView, int paramInt) {
		int i = -1;
		while (true) {
			i++;
			if (i >= paramArrayOfView.length) return;
			if (paramInt != i) paramArrayOfView[i].setVisibility(4);
			else
				paramArrayOfView[i].setVisibility(0);
		}
	}

	public static String compareDateInfo() {
		return new SimpleDateFormat("yyyyMMdd").format(new Date());
	}

	public static String currentDateInfo() {
		return new SimpleDateFormat("MM.dd").format(new Date());
	}

	public static void dismissAsyncTask(Activity _mActivity, AsyncTask paramAsyncTask) {
		if ((paramAsyncTask != null) && (!paramAsyncTask.isCancelled())) {
			Log(_mActivity.getLocalClassName(), "dismissAsyncTask");
			paramAsyncTask.cancel(true);
		}
	}

	public static void dismissProgressDialog() {
		if (progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	public static String getPhoneNumber(Activity _mActivity) {
		String str = ((TelephonyManager) _mActivity.getSystemService("phone")).getLine1Number();
		if (str != null) str = str.replace("+82", "0");
		return str;
	}

	public static String getPreference(Context paramContext, String paramString1, String paramString2) {
		return paramContext.getSharedPreferences("Audi", 1).getString(paramString1, paramString2);
	}

	public static String getPriceFormat(String paramString) {
		return new DecimalFormat("#,###").format(Integer.parseInt(paramString.trim()));
	}

	public static String getVersionName(Context paramContext) {
		try {
			String str = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).versionName;
			return str;
		} catch (PackageManager.NameNotFoundException localNameNotFoundException) {
		}
		return null;
	}

	public static void goMarket(final Activity _mActivity, final String paramString) {
		handler.postDelayed(new Runnable() {

			public void run() {
				Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + paramString));
				_mActivity.startActivity(localIntent);
			}
		}, 1000L);
	}

	public static void initLoginState(Activity _mActivity) {
		putPreference(_mActivity, "login", "fail");
		putPreference(_mActivity, "kakaoID", "");
	}

	public static boolean isInstalledApplication(Activity _mActivity, String paramString) {
		PackageManager localPackageManager = _mActivity.getPackageManager();
		try {
			localPackageManager.getPackageInfo(paramString, 1);
			return true;
		} catch (PackageManager.NameNotFoundException localNameNotFoundException) {
		}
		return false;
	}

	public static boolean isLowDevice(Activity _mActivity) {
		return "low".equals(getPreference(_mActivity, "device_state", "high"));
	}

	public static String makeURI(String paramString) {
		return CommonUtility.SERVER_URL + paramString;
	}

	public static boolean putPreference(Context paramContext, String paramString1, String paramString2) {
		SharedPreferences.Editor localEditor = paramContext.getSharedPreferences("Audi", 2).edit();
		localEditor.putString(paramString1, paramString2);
		localEditor.commit();
		return true;
	}

	public static String sequenceFix(String paramString) {
		return paramString.replaceAll("%(?![0-9a-fA-F]{2})", "%25").replaceAll("&amp;", "&").replaceAll("&quot;", "'").replaceAll("&lt;", "<")
				.replaceAll("&gt;", ">").replaceAll("&#39;", "'").replaceAll("&nbsp", " ");
	}

	public static void showProgressDialog(Activity _mActivity) {
		if (progressDialog == null) progressDialog = CustomProgressDialog.show(_mActivity, "", "", true, true, null);
		while (progressDialog.isShowing())
			return;
		progressDialog = CustomProgressDialog.show(_mActivity, "", "", true, true, null);
	}


	public static int getWidth(Context mContext) {
		int width = 0;
		WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		if (Build.VERSION.SDK_INT > 12) {
			Point size = new Point();
			display.getSize(size);
			width = size.x;
		} else {
			width = display.getWidth(); // Deprecated
		}
		return width;
	}

	public static int getHeight(Context mContext) {
		int height = 0;
		WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		if (Build.VERSION.SDK_INT > 12) {
			Point size = new Point();
			display.getSize(size);
			height = size.y;
		} else {
			height = display.getHeight(); // Deprecated
		}
		return height;
	}

	public static void notNull(final Object arg, final String name) {
		if (arg == null) {
			throw new NullPointerException("Argument '" + name + "' cannot be null");
		}
	}

	public static ResolveInfo resolveIntent(final Context context, final Intent intent) {
		return context.getPackageManager().resolveActivity(intent, 0);
	}

	public static PackageInfo getPackageInfo(final Context context) {
		return getPackageInfo(context, 0);
	}

	public static PackageInfo getPackageInfo(final Context context, int flag) {
		try {
			return context.getPackageManager().getPackageInfo(context.getPackageName(), flag);
		} catch (PackageManager.NameNotFoundException e) {
			Log(TAG, "Unable to get PackageInfo : " + e);
		}
		return null;
	}

	public static int getAppVersion(final Context context) {
		return getPackageInfo(context).versionCode;
	}

	public static String getAppPackageName(final Context context) {
		return getPackageInfo(context).packageName;
	}

	public static String getKeyHash(final Context context) {
		PackageInfo packageInfo = getPackageInfo(context, PackageManager.GET_SIGNATURES);
		if (packageInfo == null) return null;

		for (Signature signature : packageInfo.signatures) {
			try {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				return android.util.Base64.encodeToString(md.digest(), android.util.Base64.NO_WRAP);
			} catch (NoSuchAlgorithmException e) {
				Log(TAG, "Unable to get MessageDigest. signature=" + signature + "\n" + e);
			}
		}
		return null;
	}

	public static void updateApp(final Context context, int myVerNum, int latestVerNum, boolean isStrict) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		if (myVerNum < latestVerNum) {
			//alertDialogBuilder.setTitle(context.getResources().getString(R.string.title_update_notice));

			alertDialogBuilder.setMessage(context.getResources().getString(R.string.appupdate_minor)).setCancelable(!isStrict)
					.setPositiveButton("업데이트", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(Intent.ACTION_VIEW);
							intent.setData(Uri.parse("market://details?id=" + getAppPackageName(context)));
							context.startActivity(intent);

							// impriortyPackage();
						}
					});

			if (isStrict) {
				//alertDialogBuilder.setCancelable(false);
			} else {
				alertDialogBuilder.setNegativeButton("취", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				});
			}

			AlertDialog alertDialog = alertDialogBuilder.create();
			alertDialog.show();

		}
	}
}
