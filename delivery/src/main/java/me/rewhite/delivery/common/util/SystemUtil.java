package me.rewhite.delivery.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by marines on 2017. 3. 30..
 */

public class SystemUtil {

    static String TAG = "SystemUtil";

    public static String getSystemInfoString(Context ctx){

        String ret = "=== Device Info ===\n";

        // application version
        String versionName = "";
        try {
            PackageInfo info = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            versionName = info.versionName;
            ret +=  "APP.VERSION = " + versionName + "\n";
        } catch (PackageManager.NameNotFoundException e) {
            ret +=  "APP.VERSION.ERROR = " + e + "\n";
        }
        ret +=  "OS.VERSION = " + Build.VERSION.RELEASE + "\n";
        ret +=  "MODEL = " + Build.MODEL + "\n";

        ret += "SCREEN WIDTH = " + getScreenWidth(ctx) + "px\n";
        ret += "SCREEN HEIGHT = " + getScreenHeight(ctx) + "px\n";

        ret +=  "CPU_ABI = " + Build.CPU_ABI + "\n";
        ret +=  "MANUFACTURER = " + Build.MANUFACTURER + "\n";
        //ret +=  "PRODUCT = " + Build.PRODUCT + "\n";

        return ret;
    }

    public static void showApplicationDetailSetting(Context ctx){
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                .setData(Uri.parse("package:" + ctx.getPackageName()));
        ctx.startActivity(intent);
    }

    /**
     * 단말기 가로 해상도 구하기
     * @param context
     */
    public static int getScreenWidth(Context context) {
        Display dis = ((WindowManager)
                context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = dis.getWidth();
        Log.i(TAG, "Screen Width = " + width);
        return width;
    }

    /**
     * 단말기 세로 해상도 구하기
     * @param context
     */
    public static int getScreenHeight(Context context) {
        Display dis = ((WindowManager)
                context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int height = dis.getHeight();
        Log.i(TAG, "Screen height = " + height);
        return height;
    }

}
