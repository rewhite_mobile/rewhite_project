package me.rewhite.delivery.common.receiver;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.tablet.LocalOrderOutputActivity;

public class VanReceiver extends BroadcastReceiver {
    private static final String TAG = "VanReceiver";
    private ProgressDialog mProgressDialog;
    int selectedOrderId = -1;

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        String RETCODE = intent.getStringExtra("RETCODE");
        String MESSAGE = intent.getStringExtra("MESSAGE");
        boolean RESULT = intent.getBooleanExtra("RESULT", false);

        Log.w("vanReceiver : ", RETCODE + " , " + MESSAGE);

        if ("5302".equals(RETCODE)) {
            dismissDialog();
            try {
                goOutputScreen(context, intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if ("9998".equals(RETCODE)) {

        } else if ("8888".equals(RETCODE)) {
            if (RESULT) {
                showDialog(context, MESSAGE);
            } else {
                dismissDialog();
            }
        }
    }

    private void goOutputScreen(Context context, Intent intent) throws JSONException {
        JSONObject selectedUserData = new JSONObject(intent.getStringExtra("userData"));

        intent.setClass(context, LocalOrderOutputActivity.class);
        intent.putExtra("orderId", selectedOrderId + "");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("userData", selectedUserData.toString());
        context.startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    //TODO 애미메이션 처리 CHECK
    private void overridePendingTransition(int fade_in, int fade_out) {

    }

    public void showDialog(Context context, final String _message) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(_message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }
}