package me.rewhite.delivery.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import me.rewhite.delivery.R;
import me.rewhite.delivery.common.logic.Constants;


public final class CommonUtility
{
    private static final String TYPEFACE_ROBOTO_PATH = "font/Roboto-Regular.ttf";
    private static final String TYPEFACE_ROBOTO_LIGHT_PATH = "font/Roboto-Light.ttf";
    private static final String TYPEFACE_NANUMBARUNGOTHIC_PATH = "font/NanumBarunGothic.ttf";
    private static final String TYPEFACE_NANUMBARUNGOTHIC_LIGHT_PATH = "font/NanumBarunGothicLight.ttf";
    private static final String TYPEFACE_NANUMBARUNGOTHIC_ULTRA_LIGHT_PATH = "font/NanumBarunGothicUltraLight.ttf";
    private static final String TYPEFACE_NANUMBARUNGOTHIC_BOLD_PATH = "font/NanumBarunGothicBold.ttf";
    
    public static final String SERVER_URL = Constants.API_HOST;//http://api.rewhite.me";
    
    public static final String APIS_SUPPORT = "/apis/support/";
    public static final String APIS_USER = "/apis/user/";

    private static final int LOW_DPI_STATUS_BAR_HEIGHT = 19;
    private static final int MEDIUM_DPI_STATUS_BAR_HEIGHT = 25;
    private static final int HIGH_DPI_STATUS_BAR_HEIGHT = 38;
    private static final int XXXHIGH_DPI_STATUS_BAR_HEIGHT = 50;
    
	/**
	 * Google API project id registered to use GCM.
	 */
    public static final String SENDER_ID = "390116847765";
    // GCM Action
    public static final String DISPLAY_MESSAGE_ACTION = "me.rewhite.delivery.DISPLAY_MESSAGE";
    public static final String EXTRA_MESSAGE = "message";

    /**
	 * Intent used to display a message in the screen.
	 */
	public static final String MESSAGE_ACTION = "me.rewhite.delivery.gcm.message";
	
	/**
	 * Intent's extra that contains the message to be displayed.
	 */
	public static final String EXTRA_EVENT ="event";
	public static final String EXTRA_DATA = "data";
	public static final String EXTRA_DATA2 = "data2";
	
	//========= EVENT BROADCAST ==============
    public static final String EXTRA_DATA_TITLE = "title";
    public static final String EXTRA_DATA_MESSAGE = "message";   
    public static final String EXTRA_DATA_OPTION = "option";

    private static Typeface[] mTypefaceArr;
    private static Typeface mTypeface;

    /**
     * Notifies UI to display a message.
     * <p/>
     * This method is defined in the common helper because it's used both by the
     * UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    public static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }

	/**
	 * Notifies UI to display a message.
	 * <p>
	 * This method is defined in the common helper because it's used both by the
	 * UI and the background service.
	 * 
	 * @param context
	 *            application's context.
	 * @param data
	 *            data to be displayed.
	 */
	public static void sendBroardcastMessage(Context context, String event, String data, String data2)
	{
		Intent intent = new Intent(MESSAGE_ACTION);
		intent.putExtra(EXTRA_EVENT, event);
		intent.putExtra(EXTRA_DATA, data);
		intent.putExtra(EXTRA_DATA2, data2);
		context.sendBroadcast(intent);
	}

	public static String getAppVersion(Context context) {
	    String version = null;
	    
	    try {
	        PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
	        version = info.versionName;
	    } catch(NameNotFoundException e) {}
	    
	    return version;
	}
	
    // onCreate()에서 StatusBar 구하는 메서드 ( Density 이용 )
    public static int getStatusBarSizeOnCreate(Context context){ 
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
 
        switch (displayMetrics.densityDpi) {
            case DisplayMetrics.DENSITY_HIGH:
                return HIGH_DPI_STATUS_BAR_HEIGHT;
            case DisplayMetrics.DENSITY_MEDIUM:
                return MEDIUM_DPI_STATUS_BAR_HEIGHT;
            case DisplayMetrics.DENSITY_LOW:
                return LOW_DPI_STATUS_BAR_HEIGHT;
            case DisplayMetrics.DENSITY_XXXHIGH:
                return XXXHIGH_DPI_STATUS_BAR_HEIGHT;
            default:
                return MEDIUM_DPI_STATUS_BAR_HEIGHT;
        }
    } 
	
    public static int getDisplay(Context context, boolean isWidth)
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        
        if (isWidth) 
        	return displayMetrics.widthPixels;
        else
        	return displayMetrics.heightPixels;
    }
    
    public static String getDisplayTime(String str) {
        
        if (str == null) return null;
        if (str.length() == 0) return null;
        
        Date today = null;
        Date compareDate = null;
        
        try {
            today = TimeUtil.simpleDateFormat(TimeUtil.getToday());
            compareDate = TimeUtil.simpleDateFormat(str);
            
        } catch (Exception e) {
        }
        
        if (today.compareTo(compareDate) == 0) {
            return TimeUtil.getTimeFormat(TimeUtil.getDateTimeSimpleDateFormat(str));
        } else {
            return TimeUtil.getSimpleDisplayDateFormat(compareDate);
        }
    }
    
    public static byte[] getStringToByte(String str) {
        
        if (str == null)    return null;
        
        byte[] data = null;
        try {
            data = str.getBytes("euc-kr");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        
        return data;
    }
    
    public static float pxToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ( metrics.densityDpi / 160f );
        return dp;
    }
    
    public static float pixelsToSp(Context context, Float px) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px/scaledDensity;
    }


    public static void loadTypeface(Context context) {
        if (mTypefaceArr == null) {
            mTypefaceArr = new Typeface[]{Typeface.createFromAsset(context.getAssets(), TYPEFACE_ROBOTO_PATH),
                    Typeface.createFromAsset(context.getAssets(), TYPEFACE_ROBOTO_LIGHT_PATH),
                    Typeface.createFromAsset(context.getAssets(), TYPEFACE_NANUMBARUNGOTHIC_PATH),
                    Typeface.createFromAsset(context.getAssets(), TYPEFACE_NANUMBARUNGOTHIC_LIGHT_PATH),
                    Typeface.createFromAsset(context.getAssets(), TYPEFACE_NANUMBARUNGOTHIC_ULTRA_LIGHT_PATH),
                    Typeface.createFromAsset(context.getAssets(), TYPEFACE_NANUMBARUNGOTHIC_BOLD_PATH)};
        }
    }

    public static Typeface getTypeface() {
        if (mTypefaceArr == null) return null;

        return mTypefaceArr[0];
    }

    public static Typeface getLightTypeface() {
        if (mTypefaceArr == null) return null;

        return mTypefaceArr[1];
    }

    public static Typeface getNanumBarunTypeface() {
        if (mTypefaceArr == null) return null;

        return mTypefaceArr[2];
    }

    public static Typeface getNanumBarunLightTypeface() {
        if (mTypefaceArr == null) return null;

        return mTypefaceArr[3];
    }

    public static Typeface getNanumBarunUltraLightTypeface() {
        if (mTypefaceArr == null) return null;

        return mTypefaceArr[4];
    }
    public static Typeface getNanumBarunBoldTypeface() {
        if (mTypefaceArr == null) return null;

        return mTypefaceArr[5];
    }

    public static boolean isTablet(Context context){
        return context.getResources().getBoolean(R.bool.isTablet);
    }

    public static void setTypefaceNormalSetup(TextView _tv){
        _tv.setTypeface(CommonUtility.getNanumBarunTypeface());
        _tv.setPaintFlags(_tv.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    public static void setTypefaceBoldSetup(TextView _tv){
        _tv.setTypeface(CommonUtility.getNanumBarunBoldTypeface());
        _tv.setPaintFlags(_tv.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    public static void setTypefaceLightSetup(TextView _tv){
        _tv.setTypeface(CommonUtility.getNanumBarunLightTypeface());
        _tv.setPaintFlags(_tv.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    public static void setTypefaceUltraLightSetup(TextView _tv){
        _tv.setTypeface(CommonUtility.getNanumBarunUltraLightTypeface());
        _tv.setPaintFlags(_tv.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }
}
