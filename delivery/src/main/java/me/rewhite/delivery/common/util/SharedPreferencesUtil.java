package me.rewhite.delivery.common.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * SharedPreference Util 클래스.
 *
 * @author jaehun.j
 *
 */
public class SharedPreferencesUtil {

    private static final String TAG = "SharedPreferencesUtil";

    public static SharedPreferences getSharedPreferences(Context context, String name) {
        return context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getSharedPreferencesEditor(Context context, String name) {
        return getSharedPreferences(context, name).edit();
    }

    /**
     * ArrayList<String>형 데이터 저장
     * @param context
     * @param values ArrayList<String>
     */
    public static void putArrayListSharedPreference(Context context, String name, String key, ArrayList<String> values) {
        JSONArray jsonData = new JSONArray();
        for (int i = 0; i < values.size(); i++) {
            jsonData.put(values.get(i));
        }
        putSharedPreference(context, name, key, jsonData.toString());
    }

    /**
     * ArrayList<String>형 데이터 반환
     * @param context
     * @param key
     * @return
     */
    public static ArrayList<String> getArrayListSharedPreference(Context context, String name, String key) {
        String strData = getStringSharedPreference(context, name, key);
        ArrayList<String> dataList = new ArrayList<String>();

        if (strData != null) {
            try {
                JSONArray jsonData = new JSONArray(strData);

                for (int i = 0; i < jsonData.length(); i++) {
                    String dataVuse = jsonData.optString(i);
                    dataList.add(dataVuse);
                }
            } catch (JSONException e) {
                Logger.e(TAG, "getArrayListSharedPreference err.", e);
            }
        }
        return dataList;
    }


    public static HashMap<Integer, String> getHashmapSharedPreference(Context context, String name, String key) {
        Logger.d(TAG, "getHashmapSharedPreference");
        HashMap<Integer, String> hash = new HashMap<>();
        try {
            String data = getStringSharedPreference(context, name, key);
            if (!TextUtils.isEmpty(data)) {
                JSONArray arr = new JSONArray(data);
                for(int i = 0; i < arr.length(); i++) {
                    JSONObject json = arr.getJSONObject(i);
                    hash.put(json.getInt("idx"), json.getString("value"));
                }
            }
        } catch (Exception e) {
            Logger.e(TAG, "getHashmapSharedPreference err.", e);
        }

        return hash;
    }

    public static int getLastHashmapIdx(Context context, String name, String key) {
        int lastIdx = 0;

        HashMap<Integer, String> map = getHashmapSharedPreference(context, name, key);
        for (Integer idx : map.keySet()) {
            if (lastIdx < idx) {
                lastIdx = idx;
            }
        }

        return lastIdx;
    }


    public static void puttHashmapSharedPreference(Context context, String name, String key, HashMap<Integer, String> hash) {
        Logger.d(TAG, "puttHashmapSharedPreference");
        try {
            JSONArray arr = new JSONArray();
            for(Integer index : hash.keySet()) {

                JSONObject json = new JSONObject();
                json.put("idx", index);
                json.put("value", hash.get(index));
                arr.put(json);
            }
            putSharedPreference(context, name, key, arr.toString());
        } catch (JSONException e) {
            Logger.e(TAG, "puttHashmapSharedPreference err.", e);
        }

    }



    public static void putSharedPreference(Context context, String name, String key, String value) {
        getSharedPreferencesEditor(context, name).putString(key, value).commit();
    }

    public static void putSharedPreference(Context context, String name, String key, boolean value) {
        getSharedPreferencesEditor(context, name).putBoolean(key, value).commit();
    }

    public static void putSharedPreference(Context context, String name, String key, float value) {
        getSharedPreferencesEditor(context, name).putFloat(key, value).commit();
    }

    public static void putSharedPreference(Context context, String name, String key, int value) {
        getSharedPreferencesEditor(context, name).putInt(key, value).commit();
    }

    public static void putSharedPreference(Context context, String name, String key, long value) {
        getSharedPreferencesEditor(context, name).putLong(key, value).commit();
    }

    public static void removeSharedPreference(Context context, String name, String key) {
        getSharedPreferencesEditor(context, name).remove(key).commit();
    }

    public static void removeAllSharedPreference(Context context, String name) {
        getSharedPreferencesEditor(context, name).clear().commit();
    }

    public static Map<String, ?> getAllSharedPrefrence(Context context, String name) {
        return getSharedPreferences(context, name).getAll();
    }

    /**
     * String형 value 추출
     * @param context
     * @param name : 파일 이름
     * @param key : 키 값
     * @return 값이 없을 경우 null 반환
     */
    public static String getStringSharedPreference(Context context, String name, String key) {
        return getSharedPreferences(context, name).getString(key, null);
    }

    /**
     * @param context
     * @param name : 파일 이름
     * @param key : 키 값
     * @param defaultValue : 값이 없을 경우 해당 param 반환
     * @return 값이 없을 경우 defaultValue 반환
     */
    public static boolean getBooleanSharedPreference(Context context, String name, String key, boolean defaultValue) {
        return getSharedPreferences(context, name).getBoolean(key, defaultValue);
    }


    /**
     * @param context
     * @param name : 파일 이름
     * @param key : 키 값
     * @return 값이 없을 경우 0 반환
     */
    public static float getFloatSharedPreference(Context context, String name, String key) {
        return getSharedPreferences(context, name).getFloat(key, 0);
    }

    /**
     * @param context
     * @param name : 파일 이름
     * @param key : 키 값
     * @param defValue 값이 없을 경우 반환할 값.
     * @return 값이 없을 경우 defValue 반환
     */
    public static float getFloatSharedPreference(Context context, String name, String key, float defValue) {
        return getSharedPreferences(context, name).getFloat(key, defValue);
    }

    /**
     * @param context
     * @param name : 파일 이름
     * @param key : 키 값
     * @return 값이 없을 경우 0 반환
     */
    public static int getIntSharedPreference(Context context, String name, String key, int defValue) {
        return getSharedPreferences(context, name).getInt(key, defValue);
    }

    /**
     * @param context
     * @param name : 파일 이름
     * @param key : 키 값
     * @return 값이 없을 경우 0 반환
     */
    public static int getIntSharedPreference(Context context, String name, String key) {
        return getSharedPreferences(context, name).getInt(key, 0);
    }


    /**
     * @param context
     * @param name : 파일 이름
     * @param key : 키 값
     * @return 값이 없을 경우 0 반환
     */
    public static long getLongSharedPreference(Context context, String name, String key) {
        return getSharedPreferences(context, name).getLong(key, 0);
    }

    /**
     * 해당 키에 데이터가 존재하는지 유무 반환
     * @param context
     * @param name
     * @param key
     * @return
     */
    public static boolean isContainsData(Context context, String name, String key) {
        return getSharedPreferences(context, name).contains(key);
    }
}
