package me.rewhite.delivery.common.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.PixelFormat;

public class ProgressDialogHelper {
    private static final String TAG = "ProgressDialogHelper";
    private static ProgressDialog progressDialog;
    private static ProgressDialog horizontalDialog;

    public static void showProgressPopup(Activity act, boolean createNew, int resId) {
        showProgressPopup(act, createNew, act.getString(resId));
    }

    public static void showProgressPopup(Activity act, boolean createNew, String msg) {
        if (progressDialog == null || createNew) {
            progressDialog = new ProgressDialog(act);
            progressDialog.setMessage(msg);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.getWindow().setFormat(PixelFormat.TRANSPARENT);
        }

        if (progressDialog != null && !progressDialog.isShowing()) {
            Logger.d(TAG, "progressDialog.show()");
            try {
                CommonUtil.showDialog(progressDialog);
            } catch (Exception e) {
                Logger.e(TAG, "showProgressPopup err.", e);
            }
        }
    }

    public static void closeProgressPopup() {
        CommonUtil.dismissDialog(progressDialog);
    }

    public static boolean isShowingProgress() {
        return progressDialog != null ? progressDialog.isShowing() : false;
    }


    public static void showHorizontalProgress(Activity act, boolean createNew, int totalCount, String msg) {
        if (horizontalDialog == null || createNew) {
            horizontalDialog = new ProgressDialog(act);

            horizontalDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            horizontalDialog.setMessage(msg);
            horizontalDialog.setCancelable(false);
        }
        horizontalDialog.setMax(totalCount);
        if (horizontalDialog != null && !horizontalDialog.isShowing()) {
            Logger.d(TAG, "horizontalDialog show()");
            CommonUtil.showDialog(horizontalDialog);
        }
    }

    /**
     * LocalOredInPutActivity class Use
     *
     * @param activity
     * @param _message
     */
    public static void showDialog(Activity activity, final String _message) {
        Logger.d(TAG, "showDialog_message");
        if (progressDialog != null) progressDialog.dismiss();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(_message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    /**
     * LocalOredInPutActivity class Use
     */
    public static void dismissDialog() {
        Logger.d(TAG, "dismiss");
        if (progressDialog != null) progressDialog.dismiss();
    }

    public static void updateHorizontalProgress(int currentCount) {
        if (horizontalDialog != null) {
            horizontalDialog.setProgress(currentCount);
        }
    }

    public static void closeHorizontalProgress() {
        CommonUtil.dismissDialog(horizontalDialog);
    }

}
