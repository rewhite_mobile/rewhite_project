package me.rewhite.delivery.common.util;


import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.view.View;

import java.io.File;
import java.io.FileFilter;
import java.util.List;
import java.util.regex.Pattern;

/**
 * 전체 공통으로 사용하는 Util Class
 */
public class CommonUtil {

    private static final String TAG = "CommonUtil";

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (context.getPackageName().equals(service.service.getPackageName())
                    && serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }

        return false;
    }

    /**
     * 현재 실행중인 Activity 갯수를 반환
     * @param context
     * @return
     */
    public static int getRunningActivity(Context context){
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> info = activityManager.getRunningTasks(Integer.MAX_VALUE);
        String myPackageName = context.getPackageName();
        for(ActivityManager.RunningTaskInfo runningTaskInfo : info){
            String packageName = runningTaskInfo.topActivity.getPackageName();
            if(myPackageName.equals(packageName)){
                Logger.d(TAG, "running act num : " + runningTaskInfo.numActivities);
                return runningTaskInfo.numActivities;
            }
        }
        return 0;
    }


    public static int getColorDrawableColor(ColorDrawable cd) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 11) {
            return cd.getColor();
        } else {
            Bitmap bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_4444);
            Canvas canvas = new Canvas(bitmap);
            cd.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            cd.draw(canvas);

            int color = bitmap.getPixel(0, 0);
            bitmap.recycle();

            return color;
        }
    }

    public static int getColor(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

    public static Drawable getDrawable(Context context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 21) {
            return context.getResources().getDrawable(id, context.getTheme());
        } else {
            return context.getResources().getDrawable(id);
        }
    }

    public static void setBackground(View view, Drawable drawable) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 16) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }

    public static int getNumOfCores() {
        int coreCnt = 1;
        if(Build.VERSION.SDK_INT >= 17) {
            coreCnt = Runtime.getRuntime().availableProcessors();
        } else {
            // Use saurabh64's answer
            coreCnt = getNumCoresOldPhones();
        }

        return coreCnt;
    }

    /**
     * Gets the number of cores available in this device, across all processors.
     * Requires: Ability to peruse the filesystem at "/sys/devices/system/cpu"
     * @return The number of cores, or 1 if failed to get result
     */
    private static int getNumCoresOldPhones() {
        //Private Class to display only CPU devices in the directory listing
        class CpuFilter implements FileFilter {
            @Override
            public boolean accept(File pathname) {
                //Check if filename is "cpu", followed by a single digit number
                if(Pattern.matches("cpu[0-9]+", pathname.getName())) {
                    return true;
                }
                return false;
            }
        }

        try {
            //Get directory containing CPU info
            File dir = new File("/sys/devices/system/cpu/");
            //Filter to only list the devices we care about
            File[] files = dir.listFiles(new CpuFilter());
            //Return the number of cores (virtual CPU devices)
            return files.length;
        } catch(Exception e) {
            //Default to return 1 core
            return 1;
        }
    }

//  Notification Custom
//    public static void setNotificationBuilder(Context context, NotificationCompat.Builder builder) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            builder.setColor(CommonUtil.getColor(context, R.color.notification_bg));
//            builder.setSmallIcon(R.drawable.ic_noti);
//        } else {
//            builder.setSmallIcon(R.drawable.ic_launcher);
//        }
//    }


    public static boolean chkAppInstalled(Context context, String packageName) {
        boolean appInstalled = false;
        try {
            context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            appInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            appInstalled = false;
        }
        return appInstalled;
    }

    public static boolean isActivityRunning(Activity activity) {
        if (activity == null || activity.isFinishing()) {
            return false;
        }
        return true;
    }

    public static void dismissDialog(DialogInterface dialog) {
        if (dialog == null){
            return;
        }

        try {
            if (dialog instanceof AlertDialog) {
                if (((AlertDialog)dialog).isShowing()) {
                    ((AlertDialog)dialog).dismiss();
                }
                return;
            }

            if (dialog instanceof ProgressDialog) {
                if (((ProgressDialog)dialog).isShowing()) {
                    ((ProgressDialog)dialog).dismiss();
                }
                return;
            }

            if (dialog instanceof Dialog) {
                if (((Dialog)dialog).isShowing()) {
                    ((Dialog)dialog).dismiss();
                }
                return;
            }
        } catch (Exception e) {
            //ignore
            Logger.e(TAG, "dismissDialog err.", e);
        }
    }

    public static void showDialog(DialogInterface dialog) {
        if (dialog == null)
            return;

        try {
            if (dialog instanceof AlertDialog) {
                if (!((AlertDialog)dialog).isShowing()) {
                    ((AlertDialog)dialog).show();
                }
                return;
            }

            if (dialog instanceof ProgressDialog) {
                if (!((ProgressDialog)dialog).isShowing()) {
                    ((ProgressDialog)dialog).show();
                }
                return;
            }

            if (dialog instanceof Dialog) {
                if (!((Dialog)dialog).isShowing()) {
                    ((Dialog)dialog).show();
                }
                return;
            }
        } catch (Exception e) {
            //ignore
            Logger.e(TAG, "showDialog err.", e);
        }

    }

}
