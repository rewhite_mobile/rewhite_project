package me.rewhite.delivery.util;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

import me.rewhite.delivery.session.AccessToken;
import me.rewhite.delivery.util.SharedPreferencesUtility.UserInfo;



/**
 * Helper class used to communicate with the demo server.
 */
public final class ServerUtility
{
	private final String lineEnd = "\r\n";
	private final String twoHyphens = "--" ;
	private final String boundary = "*****" ;

	/**
	 * Issue a POST request file Upload to the server.
	 * 
	 * @param
	 * @param
	 * @throws IOException
	 *             propagated from POST.
	 */
	public String HttpFileUpload(String urlString, String userID,  String fileName) {
		String ret = null;
	  try {
		  FileInputStream mFileInputStream = new FileInputStream(fileName);   
		  URL connectUrl = new URL(urlString);
		  
		  if (userID == null || userID.length() == 0)
		  {
			  userID="-1";
		  }
		  
		  // open connection 
		  HttpURLConnection conn = (HttpURLConnection)connectUrl.openConnection(); 
		  conn.setDoInput(true);
		  conn.setDoOutput(true);
		  conn.setConnectTimeout(5000);
		  conn.setReadTimeout(20000);
		  conn.setUseCaches(false); 
		  conn.setRequestMethod("POST");
		  conn.setRequestProperty("Connection", "Keep-Alive");
		  conn.setRequestProperty("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		  conn.setRequestProperty("Accept-Charset","ko-kr,ko;q=0.8,en-us;q=0.5,en;windows-949,utf-8;q=0.7,*;q=0.3");
		  conn.setRequestProperty("Accept-Encoding","gzip,deflate,sdch");
		  conn.setRequestProperty("Accept-Language","ko-KR,ko;q=0.8,en-US;q=0.6,en;q=0.4");
		  conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:15.0) Gecko/20100101 Firefox/15.0.1");
		  conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
		 
		  // write data
		   DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
		   dos.writeBytes(twoHyphens + boundary + lineEnd);
		   dos.writeBytes("Content-Disposition: form-data; name=\"data\" "+ lineEnd);
		   dos.writeBytes( lineEnd) ;
		   dos.writeBytes("{\"userID\":\"" + userID + "\"}" + lineEnd);
		   dos.writeBytes(twoHyphens + boundary + lineEnd);
		   String fsName = fileName.substring(fileName.lastIndexOf('/')+1);
		   dos.writeBytes("Content-Disposition: form-data; name=\"uploadfile\";filename=\"" + fsName+"\"" + lineEnd);
		   dos.writeBytes("Content-Type: image/jpeg"+ lineEnd);
		   dos.writeBytes(lineEnd);
		  
		   int bytesAvailable = mFileInputStream.available();
		   int maxBufferSize = 1024;
		   int bufferSize = Math.min(bytesAvailable, maxBufferSize);

		   byte[] buffer = new byte[bufferSize];
		   int bytesRead = mFileInputStream.read(buffer, 0, bufferSize);
		   	   
		   // read image
		   while (bytesRead > 0) {
		    dos.write(buffer, 0, bufferSize);
		    bytesAvailable = mFileInputStream.available();
		    bufferSize = Math.min(bytesAvailable, maxBufferSize);
		    bytesRead = mFileInputStream.read(buffer, 0, bufferSize);
		   } 
		   
		   dos.writeBytes(lineEnd);
		   
		   dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
		   dos.flush(); // finish upload..
		   

		   // close streams
		   mFileInputStream.close();

		   // get response
		   int ch;
		   InputStream is = conn.getInputStream();
		   StringBuffer b =new StringBuffer();
		   while( ( ch = is.read() ) != -1 ){
		    b.append( (char)ch );
		   }
		   ret=b.toString(); 
		   dos.close();   
		   
		  } catch (Exception e) {
		  	e.printStackTrace();
		   // TODO: handle exception
		  }  
	  	return ret;
	 }

    public String HttpDownload(String urlString) {
        URL url;
        HttpURLConnection conn;

        try{
            //if you are using https, make sure to import java.net.HttpsURLConnection
            url=new URL(urlString);
            //you need to encode ONLY the values of the parameters
            String param="regnumber=3" + "&sync_table="+ URLEncoder.encode("memo_sync", "UTF-8")+
                    "&language="+ URLEncoder.encode("korean", "UTF-8");

            conn=(HttpURLConnection)url.openConnection();
            //set the output to true, indicating you are outputting(uploading) POST data
            conn.setDoOutput(true);
            //once you set the output to true, you don’t really need to set the request method to post, but I’m doing it anyway
            conn.setRequestMethod("POST");

            //Android documentation suggested that you set the length of the data you are sending to the server, BUT
            // do NOT specify this length in the header by using conn.setRequestProperty(“Content-Length”, length);
            //use this instead.
            conn.setFixedLengthStreamingMode(param.getBytes().length);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            //send the POST out
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(param);
            out.close();

            //build the string to store the response text from the server
            String response= "";

            //start listening to the stream
            Scanner inStream = new Scanner(conn.getInputStream());

            //process the stream and store it in StringBuilder
            while(inStream.hasNextLine())
                response+=(inStream.nextLine());

            Log.d("debug", " str :::::: " + response);
            return response;
        }
        //catch some error
        catch(MalformedURLException ex){
            Log.d("debug", " str :::::: " + ex.toString());
        }
        // and some more
        catch(IOException ex) {
            Log.d("debug", " str :::::: " + ex.toString());
        }

        return null;
    }
    
    public String getHttpJson(String urlString) {
        String ret = null;
        
        try {
            URL connectUrl = new URL(urlString);
            // open connection 
            HttpURLConnection conn = (HttpURLConnection)connectUrl.openConnection(); 
            conn.setRequestMethod("GET");             
            conn.setDoInput(true);                    
            
            InputStream is = conn.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            ret = br.readLine();
            
        } catch (Exception e) {
            e.printStackTrace();
            return ret;
            // TODO: handle exception
        }
        
        return ret;
    }
    
    public String sendHttpJson(String urlString, JSONObject json, boolean isAuthorization) {
        String ret = "";
        
        try {   
            URL connectUrl = new URL(urlString);
            // open connection 
            HttpURLConnection conn = (HttpURLConnection)connectUrl.openConnection(); 
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            
            if (isAuthorization) {
                conn.setRequestProperty("Authorization", "device_token='" + SharedPreferencesUtility.get(UserInfo.GCM_ID) + "',access_token='"
                        + AccessToken.createFromCache().getAccessTokenString() + "'");
            }
          
            // write data
            StringBuffer sb = new StringBuffer();
            
            JSONArray names = json.names();
            for(int i = 0; i < json.length(); i++)
            {
                String key = (String) names.get(i);
                if (i == json.length() - 1) {
                    sb.append(key).append("=").append(json.getString(key));
                } else {
                    sb.append(key).append("=").append(json.getString(key)).append("&");
                }
            }
            
            conn.setFixedLengthStreamingMode(String.valueOf(sb).getBytes().length);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            
            PrintWriter out = new PrintWriter(conn.getOutputStream());
            out.print(sb.toString());
            out.close();
            
            // get response
            Scanner inStream = new Scanner(conn.getInputStream());
            while(inStream.hasNextLine())
                ret+=(inStream.nextLine());
            
            Log.d("debug", sb.toString() + "ret" + ret);
           
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("debug", e.getMessage() + ":ret" + ret);
            return ret;
            // TODO: handle exception
        }
      
        return ret;
     }
}