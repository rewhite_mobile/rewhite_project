package me.rewhite.delivery.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import me.rewhite.delivery.R;


public class CustomProgressDialog extends Dialog {

	LayoutInflater inflater = getLayoutInflater();

	public CustomProgressDialog(Context paramContext) {
		super(paramContext, R.style.AppTheme);
	}

	public static CustomProgressDialog show(Activity paramActivity, CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
		return show(paramActivity, paramCharSequence1, paramCharSequence2, false);
	}

	public static CustomProgressDialog show(Activity paramActivity, CharSequence paramCharSequence1, CharSequence paramCharSequence2,
			boolean paramBoolean) {
		return show(paramActivity, paramCharSequence1, paramCharSequence2, paramBoolean, false, null);
	}

	public static CustomProgressDialog show(Activity paramActivity, CharSequence paramCharSequence1, CharSequence paramCharSequence2,
			boolean paramBoolean1, boolean paramBoolean2) {
		return show(paramActivity, paramCharSequence1, paramCharSequence2, paramBoolean1, paramBoolean2, null);
	}

	public static CustomProgressDialog show(Activity paramActivity, CharSequence paramCharSequence1, CharSequence paramCharSequence2,
			boolean paramBoolean1, boolean paramBoolean2, DialogInterface.OnCancelListener paramOnCancelListener) {
		CustomProgressDialog localCustomProgressDialog = new CustomProgressDialog(paramActivity);
		localCustomProgressDialog.setOnCancelListener(paramOnCancelListener);
		ProgressBar localProgressBar = new ProgressBar(paramActivity);
		//Resources localResources = ;
		localProgressBar.setIndeterminate(true);
		localProgressBar.setIndeterminateDrawable(new ColorDrawable(android.graphics.Color.WHITE));
		localCustomProgressDialog.addContentView(localProgressBar, new ViewGroup.LayoutParams(-2, -2));
		localCustomProgressDialog.show();
		return localCustomProgressDialog;
	}

	public static CustomProgressDialog show(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2,
			boolean paramBoolean1, boolean paramBoolean2, DialogInterface.OnCancelListener paramOnCancelListener) {
		CustomProgressDialog localCustomProgressDialog = new CustomProgressDialog(paramContext);
		localCustomProgressDialog.setOnCancelListener(paramOnCancelListener);
		ProgressBar localProgressBar = new ProgressBar(paramContext);
		localProgressBar.setIndeterminateDrawable(new ColorDrawable(android.graphics.Color.WHITE));
		localCustomProgressDialog.addContentView(localProgressBar, new ViewGroup.LayoutParams(-2, -2));
		localCustomProgressDialog.show();
		return localCustomProgressDialog;
	}
}
