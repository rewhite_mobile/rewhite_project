package me.rewhite.delivery.util;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by marines on 16. 7. 25..
 */
public class UnicodeFormatter {
    static public String byteToHex(byte b) {
        // Returns hex String representation of byte b
        char hexDigit[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
        char[] array = { hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f] };
        return new String(array);
    }

    static public String charToHex(char c) {
        // Returns hex String representation of char c
        byte hi = (byte) (c >>> 8);
        byte lo = (byte) (c & 0xff);
        return byteToHex(hi) + byteToHex(lo);
    }

    // 문자열을 헥사 스트링으로 변환하는 메서드
    public static String stringToHex(String s) throws UnsupportedEncodingException {
        return String.format("%02x", new BigInteger(1, s.getBytes("UTF-8")));
    }

    // 문자열을 헥사 스트링으로 변환하는 메서드
    public static String stringToHex(String s, String charset) throws UnsupportedEncodingException {
        return String.format("%02x", new BigInteger(1, s.getBytes(charset)));
    }

    // hex to byte[]
//    public static byte[] hexToByteArray(String hex) {
//        if (hex == null || hex.length() == 0) { return null; }
//
//        byte[] ba = new byte[hex.length() / 2];
//        for (int i = 0; i < ba.length; i++) {
//            ba[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
//        }
//        return ba;
//    }

    public static String stringJustifyToHex(String ls, String rs, int _len) throws UnsupportedEncodingException {

        String lHex = String.format("%02x", new BigInteger(1, ls.getBytes("euc-kr")));
        String rHex = String.format("%02x", new BigInteger(1, rs.getBytes("euc-kr")));
        String space = String.format("%02x", new BigInteger(1, " ".getBytes("euc-kr")));

        do{
            lHex += space;
        }while( (rHex.length() + lHex.length()) < _len*2);

        Log.e("stringJustifyToHex", lHex + rHex);
        return lHex + rHex;
    }

    public static String CatHexString(String s) throws UnsupportedEncodingException {
        String[] temp = s.split("\n");
        String finalHex = "";
        for(int i = 0; i < temp.length; i++){
            finalHex += String.format("%02x", new BigInteger(1, temp[i].getBytes("euc-kr")));
            finalHex += "0A";
        }

        return finalHex;
    }

    public static byte[] hexToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len/2];

        for(int i = 0; i < len; i+=2){
            data[i/2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));
        }

        return data;
    }
    final protected static char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    public static String byteArrayToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length*2];
        int v;

        for(int j=0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j*2] = hexArray[v>>>4];
            hexChars[j*2 + 1] = hexArray[v & 0x0F];
        }

        return new String(hexChars);
    }


    public static List<byte[]> split(byte[] array, byte[] delimiter) {
        List<byte[]> byteArrays = new LinkedList<>();
        if (delimiter.length == 0) {
            return byteArrays;
        }
        int begin = 0;

        outer:
        for (int i = 0; i < array.length - delimiter.length + 1; i++) {
            for (int j = 0; j < delimiter.length; j++) {
                if (array[i + j] != delimiter[j]) {
                    continue outer;
                }
            }
            byteArrays.add(Arrays.copyOfRange(array, begin, i));
            begin = i + delimiter.length;
        }
        byteArrays.add(Arrays.copyOfRange(array, begin, array.length));
        return byteArrays;
    }

    // byte[] to hex
//    public static String byteArrayToHex(byte[] ba) {
//        if (ba == null || ba.length == 0) { return null; }
//
//        StringBuffer sb = new StringBuffer(ba.length * 2);
//        String hexNumber;
//        for (int x = 0; x < ba.length; x++) {
//            hexNumber = "0" + Integer.toHexString(0xff & ba[x]);
//
//            sb.append(hexNumber.substring(hexNumber.length() - 2));
//        }
//        return sb.toString();
//    }

    // 아래의 방법 외에 다음과 같이 간단한 방법도 존재한다.  :
//   byte[] byteArray = ByteBuffer.allocate(4).putInt(value).array();
    public static byte[] intToByteArray(int value) {
        byte[] byteArray = new byte[4];
        byteArray[0] = (byte)(value >> 24);
        byteArray[1] = (byte)(value >> 16);
        byteArray[2] = (byte)(value >> 8);
        byteArray[3] = (byte)(value);
        return byteArray;
    }

    public static int byteArrayToInt(byte bytes[]) {
        return ((((int)bytes[0] & 0xff) << 24) |
                (((int)bytes[1] & 0xff) << 16) |
                (((int)bytes[2] & 0xff) << 8) |
                (((int)bytes[3] & 0xff)));
    }
}
