package me.rewhite.delivery.util;

import android.content.Context;
import android.content.SharedPreferences;

import me.rewhite.delivery.van.VanType;


public class SharedPreferencesUtility {

	public enum Command {
		IS_AUTO_LOGIN, // 자동 로그인 여부
		IS_TUTORIAL, // tutorial 확인 여부
		IS_GETSTARTED,
		FONT_STYLE, // 글꼴 타입
		FONT_SIZE, // 글자 크기
		SHORTCUT, IS_ONLINE, ONLINE_TYPE, IS_NOTIFICATION, NOTIFICATION_MODE
	}

	public enum UserInfo {
		GCM_ID, LOGIN_ID, EMAIL, NICKNAME, ACCESS_TOKEN, PHONE_NO, STORE_ID, STORE_NAME,STORE_VIRTUAL_NAME, STORE_ADDRESS, LONGITUDE, LATITUDE,
		STORE_GRADE, STORE_PHONE, STORE_VIRTUAL_PHONE, STORE_IMAGE, STORE_AVAILABLE_ORDER, DELIVERY_GAP, STORE_RATE_1, STORE_RATE_2, STORE_TYPE, VAN_TYPE, IS_VISIT_PRICE, STORE_OWNER
	}

	public enum OrderCount{
		Pickup, Delivery
	}

	public enum PARTNER {
		UPLUS_ID, UPLUS_PASSWORD
	}

	public enum Sync{
		Retry, RestoreNetwork, LastNetworkEnabled, LastNetwork
	}

	public enum OnlineType {
		WIFI, MOBILE, NONE
	}

	public enum Van{
		VAN_TYPE, HARDWARE_SERIAL, MODEL_NAME, BAUDRATE, PORT_NAME, IS_CONNECTED, BIZ_NO, CAT_DEVICE_IP
	}

	public enum MemoNotification {
		NONE, SOUND_VIBRATOR, SOUND, VIBRATOR, SILENT
	}

	private static SharedPreferences mPref = null;

	public static void initialize(Context context) {
		mPref = context.getSharedPreferences("me.rewhite.delivery", Context.MODE_PRIVATE);

		if (mPref != null && mPref.getAll().isEmpty()) {
			SharedPreferences.Editor edit = mPref.edit();

			edit.putString(Command.IS_AUTO_LOGIN.toString(), "false");
			edit.putString(Command.IS_TUTORIAL.toString(), "false");
			edit.putString(Command.IS_GETSTARTED.toString(), "false");
			//edit.putString(UserInfo.GCM_ID.toString(), "");
			edit.putString(UserInfo.LOGIN_ID.toString(), "");
			edit.putString(UserInfo.STORE_ID.toString(), "");
			edit.putString(UserInfo.STORE_NAME.toString(), "");
			edit.putString(UserInfo.STORE_OWNER.toString(), "");
			edit.putString(UserInfo.STORE_VIRTUAL_NAME.toString(), "");
			edit.putString(UserInfo.STORE_VIRTUAL_PHONE.toString(), "");
			edit.putString(UserInfo.STORE_PHONE.toString(), "");
			edit.putString(UserInfo.STORE_ADDRESS.toString(), "");
			edit.putString(UserInfo.ACCESS_TOKEN.toString(), "");
			edit.putString(UserInfo.LONGITUDE.toString(), "0");
			edit.putString(UserInfo.LATITUDE.toString(), "0");
			edit.putString(UserInfo.STORE_GRADE.toString(), "");
			edit.putString(UserInfo.STORE_AVAILABLE_ORDER.toString(), "000");
			edit.putString(UserInfo.STORE_TYPE.toString(), "1");
			edit.putString(UserInfo.IS_VISIT_PRICE.toString(), "N");

			edit.putString(UserInfo.DELIVERY_GAP.toString(), "1");
			edit.putString(Van.VAN_TYPE.toString(), VanType.EMPTY);
			edit.putString(Van.CAT_DEVICE_IP.toString(), "192.168.0.1");
			//edit.putString(Van.HARDWARE_SERIAL.toString(), "");
			edit.putString(Van.MODEL_NAME.toString(), "");
			edit.putString(Van.BAUDRATE.toString(), "");
			edit.putString(Van.PORT_NAME.toString(), "");
			//edit.putString(Van.BIZ_NO.toString(), "");
			edit.putString(Van.IS_CONNECTED.toString(), "FALSE");

            edit.putString(OrderCount.Pickup.toString(), "0");
            edit.putString(OrderCount.Delivery.toString(), "0");

            edit.putString(PARTNER.UPLUS_ID.toString(), "");
            edit.putString(PARTNER.UPLUS_PASSWORD.toString(), "");

			edit.putString(Command.IS_NOTIFICATION.toString(), "true");

			edit.commit();
		}
	}

	public static void logoutClear() {
		if (mPref != null && !mPref.getAll().isEmpty()) {
			SharedPreferences.Editor edit = mPref.edit();

			edit.putString(Command.IS_AUTO_LOGIN.toString(), "false");
			//edit.putString(Command.IS_TUTORIAL.toString(), "false");
			//edit.putString(UserInfo.GCM_ID.toString(), "");
			//edit.putString(UserInfo.LOGIN_ID.toString(), "");
			edit.putString(UserInfo.STORE_ID.toString(), "");
			edit.putString(UserInfo.STORE_NAME.toString(), "");
			edit.putString(UserInfo.STORE_OWNER.toString(), "");
			edit.putString(UserInfo.STORE_VIRTUAL_NAME.toString(), "");
			edit.putString(UserInfo.STORE_VIRTUAL_PHONE.toString(), "");
            edit.putString(UserInfo.STORE_PHONE.toString(), "");
			edit.putString(UserInfo.STORE_ADDRESS.toString(), "");
			edit.putString(UserInfo.ACCESS_TOKEN.toString(), "");
			edit.putString(UserInfo.LONGITUDE.toString(), "0");
			edit.putString(UserInfo.LATITUDE.toString(), "0");
			edit.putString(UserInfo.STORE_GRADE.toString(), "");
			edit.putString(UserInfo.STORE_AVAILABLE_ORDER.toString(), "000");
			edit.putString(UserInfo.STORE_TYPE.toString(), "1");
			edit.putString(UserInfo.IS_VISIT_PRICE.toString(), "N");

			//edit.putString(Van.VAN_TYPE.toString(), VanType.EMPTY);
			//edit.putString(Van.HARDWARE_SERIAL.toString(), "");
			//edit.putString(Van.MODEL_NAME.toString(), "");
			//edit.putString(Van.BAUDRATE.toString(), "");
			//edit.putString(Van.PORT_NAME.toString(), "");
			//edit.putString(Van.BIZ_NO.toString(), "");
			//edit.putString(Van.IS_CONNECTED.toString(), "FALSE");

			edit.putString(UserInfo.DELIVERY_GAP.toString(), "1");

			edit.putString(Command.IS_NOTIFICATION.toString(), "true");
			edit.commit();
		}
	}

	public static void clear() {
		if (mPref != null && !mPref.getAll().isEmpty()) {
			SharedPreferences.Editor edit = mPref.edit();

			edit.putString(Command.IS_AUTO_LOGIN.toString(), "false");
			//edit.putString(Command.IS_TUTORIAL.toString(), "false");
			//edit.putString(UserInfo.GCM_ID.toString(), "");
			//edit.putString(UserInfo.LOGIN_ID.toString(), "");
			edit.putString(UserInfo.STORE_ID.toString(), "");
			edit.putString(UserInfo.STORE_NAME.toString(), "");
			edit.putString(UserInfo.STORE_OWNER.toString(), "");
			edit.putString(UserInfo.STORE_VIRTUAL_NAME.toString(), "");
			edit.putString(UserInfo.STORE_VIRTUAL_PHONE.toString(), "");
            edit.putString(UserInfo.STORE_PHONE.toString(), "");
			edit.putString(UserInfo.STORE_ADDRESS.toString(), "");
			edit.putString(UserInfo.ACCESS_TOKEN.toString(), "");
			edit.putString(UserInfo.LONGITUDE.toString(), "0");
			edit.putString(UserInfo.LATITUDE.toString(), "0");
			edit.putString(UserInfo.STORE_GRADE.toString(), "");
			edit.putString(UserInfo.STORE_AVAILABLE_ORDER.toString(), "000");
			edit.putString(UserInfo.STORE_TYPE.toString(), "1");
			edit.putString(UserInfo.IS_VISIT_PRICE.toString(), "N");

//			edit.putString(Van.VAN_TYPE.toString(), VanType.EMPTY);
//			//edit.putString(Van.HARDWARE_SERIAL.toString(), "");
//			edit.putString(Van.MODEL_NAME.toString(), "");
//			edit.putString(Van.BAUDRATE.toString(), "");
//			edit.putString(Van.PORT_NAME.toString(), "");
//			//edit.putString(Van.BIZ_NO.toString(), "");
//			edit.putString(Van.IS_CONNECTED.toString(), "FALSE");

			edit.putString(UserInfo.DELIVERY_GAP.toString(), "1");

			edit.putString(Command.IS_NOTIFICATION.toString(), "true");
			edit.commit();
		}
	}

	public static String get(Object cmd) {
		if (mPref == null) return null;
		return mPref.getString(cmd.toString(), null);
	}

	public static boolean set(Object cmd, String value) {
		if (mPref != null) {
			SharedPreferences.Editor edit = mPref.edit();

			edit.putString(cmd.toString(), value);
			return edit.commit();
		}
		return false;
	}
}