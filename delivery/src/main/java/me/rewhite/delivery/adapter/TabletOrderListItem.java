package me.rewhite.delivery.adapter;

/**
 * Created by marines on 2015. 10. 12..
 */
public class TabletOrderListItem {


    private String orderId;
    private String registerDateApp;
    private String orderPickupItemMessage;
    private String isPayment;
    private String isReceivable;

    public int getDeliveryPrice() {
        return deliveryPrice;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getRegisterDateApp() {
        return registerDateApp;
    }

    public String getOrderPickupItemMessage() {
        return orderPickupItemMessage;
    }

    public String getIsPayment() {
        return isPayment;
    }

    public String getIsReceivable() {
        return isReceivable;
    }

    private int deliveryPrice;

    public String getJsonData() {
        return jsonData;
    }

    private String jsonData;

    // orderId, registerDateApp, orderPickupItemMessage, isPayment, isReceivable, deliveryPrice
    public TabletOrderListItem(String orderId, String registerDateApp,
                               String orderPickupItemMessage, String isPayment, String isReceivable, int deliveryPrice,  String jsonData) {

        this.orderId = orderId;
        this.registerDateApp = registerDateApp;
        this.orderPickupItemMessage = orderPickupItemMessage;
        this.isPayment = isPayment;
        this.isReceivable = isReceivable;
        this.deliveryPrice = deliveryPrice;
        this.jsonData = jsonData;

    }
}
