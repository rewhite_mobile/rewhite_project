package me.rewhite.delivery.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.GSOrderListActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.TimeUtil;


/**
 * Created by marines on 2015. 10. 5..
 */
public class GSOrderListViewAdapter extends RecyclerView.Adapter<GSOrderListViewAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<GSOrderListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;
    public MyAdapterListener onClickListener;

    public interface MyAdapterListener {
        void buttonViewOnClick(View v, int position);
        void contentViewOnClick(View v, int position);
    }

    public GSOrderListViewAdapter (ArrayList<GSOrderListItem> myValues, MyAdapterListener listener){
        this.data= myValues;
        this.onClickListener = listener;
    }

    @Override
    public GSOrderListViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.gs_order_history_item, parent, false);
        aq = new AQuery(listItem);
        return new GSOrderListViewAdapter.MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(GSOrderListViewAdapter.MyViewHolder holder, int position) {

        holder.onClickListener = this.onClickListener;

        GSOrderListItem item = data.get(position);

        String storeName = item.getStoreName();
        String orderPickupItemMessage = item.getOrderPickupItemMessage();
        String orderRequest = item.getOrderRequest();
        String orderId = item.getOrderId();
        String partnerOrderId = item.getPartnerOrderNo();
        String orderStatus = item.getOrderStatus();
        int orderCount = item.getPickupQuantity();
        JSONArray payments = null;
        try {
            if(item.getPayments() != null){
                payments = new JSONArray(item.getPayments());
            }else{
                payments = new JSONArray();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String price = item.getDeliveryPrice();
        int priceValue = Integer.parseInt(price);
        String outputPrice = price;
        if(priceValue < 20000){
            priceValue = priceValue + 2000;
        }
        outputPrice = MZValidator.toNumFormat(priceValue);

        aq.id(holder.priceTextView).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(holder.elementTextView).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(holder.nicknameTextView).text(storeName);

        if("7".equals(item.getPartnerId())){
            holder.orderReferView.setImageResource(R.mipmap.icon_list_gs);
            holder.topInfoArea.setBackgroundColor(Color.parseColor("#00c3d5"));
        }else{
            holder.orderReferView.setImageResource(R.mipmap.icon_list_place);
            holder.topInfoArea.setBackgroundColor(Color.parseColor("#00aeef"));
        }

        try{
            String orderSubType = item.getJsonData().getString("orderSubType");
            Log.e("orderSubType", orderSubType);
            aq.id(holder.paytypeIconView).gone();

            if ("01".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("수거방문예정");
                aq.id(holder.orderStatusTextView).text("수거접수").textColor(0xFF004f56);
                aq.id(holder.pickCountTextView).text(item.getPackQuantity() + "");
                aq.id(holder.priceTextView).text("세탁요금 미정").gone();
                //item.setJsonData().getString();
                aq.id(holder.elementTextView).text("").gone();
                // DELIVERY_GAP 없는경우 처리 필요
                aq.id(holder.pickDateTextView).text("수거접수일 "+TimeUtil.convertTimestampToStringDate2(Long.parseLong(item.getPickupRequestTimeApp())));
            } else if ("02".equals(item.getOrderStatus())) {
                aq.id(holder.orderStatusTextView).text("수거방문예정").textColor(0xFF004f56);
                aq.id(holder.pickCountTextView).text(item.getPackQuantity() + "");
                aq.id(holder.priceTextView).text("세탁요금 미정").gone();
                //aq.id(holder.elementTextView).text("수거후 표시예정");
                aq.id(holder.pickDateTextView).text("수거접수일 "+TimeUtil.convertTimestampToStringDate2(Long.parseLong(item.getPickupRequestTimeApp())));
                aq.id(holder.elementTextView).text("").gone();
            } else if ("03".equals(item.getOrderStatus())) {
                aq.id(holder.orderStatusTextView).text("품목등록").textColor(0xFF004f56);
                aq.id(holder.pickCountTextView).text(item.getPackQuantity() + "");
                aq.id(holder.priceTextView).text("품목을 등록해주세요.");
                //aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(holder.pickDateTextView).text("세탁물 : " + orderCount + "개");
                aq.id(holder.elementTextView).gone();

                aq.id(holder.actionButton).text("자세히보기");
            } else if ("11".equals(item.getOrderStatus())) {
                aq.id(holder.orderStatusTextView).text("세탁 준비중").textColor(0xFF004f56);
                aq.id(holder.count_area).gone();
                aq.id(holder.priceTextView).text(outputPrice + " 원");
                aq.id(holder.pickDateTextView).text(orderPickupItemMessage.trim().replace(System.getProperty("line.separator"), ""));
                aq.id(holder.elementTextView).text(orderPickupItemMessage).gone();
                isPaymentStatus(item, holder);

                aq.id(holder.actionButton).text("자세히보기");
            } else if ("12".equals(item.getOrderStatus())) {
                aq.id(holder.orderStatusTextView).text("세탁중").textColor(0xFF004f56);
                aq.id(holder.count_area).gone();
                aq.id(holder.priceTextView).text(outputPrice + " 원");
                aq.id(holder.pickDateTextView).text(orderPickupItemMessage.trim().replace(System.getProperty("line.separator"), ""));
                aq.id(holder.elementTextView).text(orderPickupItemMessage).gone();
                isPaymentStatus(item, holder);

                aq.id(holder.actionButton).text("자세히보기");
            } else if ("13".equals(item.getOrderStatus())) {
                aq.id(holder.orderStatusTextView).text("세탁완료").textColor(0xFF004f56);
                aq.id(holder.count_area).gone();
                aq.id(holder.priceTextView).text(outputPrice + " 원");
                aq.id(holder.pickDateTextView).text(orderPickupItemMessage.trim().replace(System.getProperty("line.separator"), ""));
                aq.id(holder.elementTextView).text(orderPickupItemMessage).gone();
                isPaymentStatus(item, holder);

                Log.e("orderPickupItemMessage", orderPickupItemMessage);

                aq.id(holder.actionButton).text("자세히보기");
            } else if ("21".equals(item.getOrderStatus())) {

                isPaymentStatus(item, holder);

                aq.id(holder.priceTextView).text(outputPrice + " 원");
                //aq.id(holder.elementTextView).text(orderPickupItemMessage);
                aq.id(holder.pickDateTextView).text("입고예정일 " + TimeUtil.convertTimestampToStringDate2(Long.parseLong(item.getDeliveryRequestTimeApp())));
                aq.id(holder.elementTextView).text(orderPickupItemMessage.trim().replace(System.getProperty("line.separator"), ""));

                if(item.getBoxQuantity() == 0){
                    aq.id(holder.orderStatusTextView).text("배송준비").textColor(0xFF004f56);
                    aq.id(holder.count_area).gone();
                    aq.id(holder.actionButton).text("배송박스 수량 입력하기");
                }else{
                    if("7".equals(item.getPartnerId())){
                        aq.id(holder.orderStatusTextView).text("편의점 입고중").textColor(0xF004f56);
                        aq.id(holder.actionButton).text("편의점에 입고완료");
                    }else{
                        aq.id(holder.orderStatusTextView).text("플레이스 입고중").textColor(0xF004f56);
                        aq.id(holder.actionButton).text("플레이스에 입고완료");
                    }

                    aq.id(holder.pickCountTitleTextView).text("배송박스 수량");
                    aq.id(holder.pickCountTextView).text(item.getBoxQuantity() + "");

                }

            } else if ("22".equals(item.getOrderStatus())) {
                aq.id(holder.priceTextView).text(outputPrice + " 원");
                aq.id(holder.pickDateTextView).text("입고완료일 " + TimeUtil.convertTimestampToStringDate2(Long.parseLong(item.getStatusTimeApp())));
                aq.id(holder.elementTextView).text(orderPickupItemMessage.trim().replace(System.getProperty("line.separator"), ""));

                aq.id(holder.pickCountTitleTextView).text("배송박스 수량");
                aq.id(holder.pickCountTextView).text(item.getBoxQuantity() + "");

                if("Y".equals(item.getIsReceivable())){
                    // 미수금 있는 경우
                    aq.id(holder.paytypeIconView).image(R.mipmap.icon_pay_unpaid).visible();
                    aq.id(holder.orderStatusTextView).text("입고완료(결제전)").textColor(0xFF004f56);
                }else{
                    aq.id(holder.orderStatusTextView).text("입고완료").textColor(0xFF004f56);

                    isPaymentStatus(item, holder);
                }
                aq.id(holder.actionButton).text("자세히보기");

            } else if ("23".equals(item.getOrderStatus())) {
                aq.id(holder.orderStatusTextView).text("수령완료").textColor(0xFF004f56);
                aq.id(holder.pickCountTitleTextView).text("배송박스 수량");
                aq.id(holder.pickCountTextView).text(item.getBoxQuantity() + "");

                aq.id(holder.priceTextView).text(outputPrice + " 원");
                aq.id(holder.pickDateTextView).text("수령완료일 " + TimeUtil.convertTimestampToStringDate2(Long.parseLong(item.getStatusTimeApp())));
                aq.id(holder.elementTextView).text(orderPickupItemMessage.trim().replace(System.getProperty("line.separator"), ""));//.gone();
                //aq.id(holder.elementTextView).text(orderPickupItemMessage).gone();
                aq.id(holder.actionButton).text("자세히보기");
            } else if ("24".equals(item.getOrderStatus())) {
                aq.id(holder.orderStatusTextView).text("방문수령완료").textColor(0xFF004f56);
                aq.id(holder.pickCountTitleTextView).text("배송박스 수량");
                aq.id(holder.pickCountTextView).text(item.getBoxQuantity() + "");

                aq.id(holder.priceTextView).text(outputPrice + " 원");
                aq.id(holder.pickDateTextView).text("수령완료일 " + TimeUtil.convertTimestampToStringDate2(Long.parseLong(item.getStatusTimeApp())));
                aq.id(holder.elementTextView).text(orderPickupItemMessage.trim().replace(System.getProperty("line.separator"), ""));//.gone();
                //aq.id(holder.elementTextView).text(orderPickupItemMessage).gone();
                aq.id(holder.actionButton).text("자세히보기");
            } else if ("91".equals(item.getOrderStatus())) {
                aq.id(holder.orderStatusTextView).text("취소");
                //aq.id(R.id.back).image(R.mipmap.back_order_history_91);
                aq.id(holder.priceAreaView).gone();
                aq.id(holder.pickDateTextView).text("취소일시 " + TimeUtil.convertTimestampToStringDate2(Long.parseLong(item.getStatusTimeApp())));
                aq.id(holder.elementTextView).text(orderPickupItemMessage.trim().replace(System.getProperty("line.separator"), ""));//.gone();
                //aq.id(holder.elementTextView).text(orderPickupItemMessage).gone();
                aq.id(holder.actionButton).text("자세히보기");
            } else {
                aq.id(holder.orderStatusTextView).text("기타");
                //aq.id(R.id.back).image(R.mipmap.back_order_history_99);
                aq.id(holder.priceAreaView).gone();
                aq.id(holder.elementTextView).text(orderPickupItemMessage.trim().replace(System.getProperty("line.separator"), ""));
                aq.id(holder.actionButton).text("자세히보기");
            }

            if("Y".equals(item.getIsNewPOS())){
                aq.id(holder.actionButton).text("자세히보기");
            }

        }catch(JSONException e){
            e.printStackTrace();
        }

        if(partnerOrderId != null){
            if("Y".equals(item.getIsNewPOS())){
                holder.orderIdTextView.setText(String.valueOf(orderId));
            }else{
                holder.orderIdTextView.setText(String.valueOf(partnerOrderId));
            }
        }else{
            holder.orderIdTextView.setText(String.valueOf(orderId));
        }

    }

    public GSOrderListItem getItemValue(int position)  {
        return data.get(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView orderIdTextView;
        private TextView pickCountTextView;
        private TextView pickCountTitleTextView;
        private TextView nicknameTextView;

        private TextView deliveryCountTextView;
        private TextView orderStatusTextView;
        private TextView priceTextView;
        private TextView pickDateTextView;
        private TextView elementTextView;

        private View priceAreaView;
        private View contentView;
        private View count_area;
        private ImageView paytypeIconView;

        private View topInfoArea;
        private ImageView orderReferView;

        public MyAdapterListener onClickListener;
        public Button actionButton;

        public MyViewHolder(View itemView) {
            super(itemView);
            topInfoArea = (View)itemView.findViewById(R.id.top_info);
            priceAreaView = (View)itemView.findViewById(R.id.price_area);
            contentView = (View)itemView.findViewById(R.id.content);
            count_area = (View)itemView.findViewById(R.id.count_area);
            nicknameTextView = (TextView)itemView.findViewById(R.id.text_name);
            orderIdTextView = (TextView)itemView.findViewById(R.id.text_orderid);
            pickCountTextView = (TextView)itemView.findViewById(R.id.text_count);
            pickCountTitleTextView = (TextView)itemView.findViewById(R.id.desc_title_text);
            deliveryCountTextView = (TextView)itemView.findViewById(R.id.text_gs_delivery_count);

            orderReferView = (ImageView)itemView.findViewById(R.id.order_refer);

            orderStatusTextView = (TextView)itemView.findViewById(R.id.order_status_text);
            priceTextView = (TextView)itemView.findViewById(R.id.text_price);
            pickDateTextView = (TextView)itemView.findViewById(R.id.text_pick_date);
            elementTextView = (TextView)itemView.findViewById(R.id.text_element);
            paytypeIconView = (ImageView)itemView.findViewById(R.id.icon_paytype);

            actionButton = (Button)itemView.findViewById(R.id.btn_submit);
            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onClickListener != null){
                        onClickListener.buttonViewOnClick(v, getAdapterPosition());
                    }
                }
            });

            contentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onClickListener != null){
                        onClickListener.contentViewOnClick(v, getAdapterPosition());
                    }
                }
            });
        }

    }

    private void isPaymentStatus(GSOrderListItem item, GSOrderListViewAdapter.MyViewHolder holder) throws JSONException {

        //AQuery bq = new AQuery();
        JSONArray payments = null;
        Log.e("payments value", item.getPayments() + "");
        try {
            if(item.getPayments() != null){
                payments = new JSONArray(item.getPayments());
            }else{
                payments = new JSONArray();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        aq.id(holder.paytypeIconView).visible();
        if(payments != null){
            if("N".equals(item.getIsPayment())){
                if("Y".equals(item.getIsReceivable())){
                    // 미수금
                    aq.id(holder.paytypeIconView).image(R.mipmap.icon_pay_unpaid);
                }else{
                    // 결제방식 미선택
                    aq.id(holder.paytypeIconView).image(R.mipmap.icon_pay_unselect);
                }
            }else{
                // 결제완료
                if(item.getIsAdditionalPayment() == null){
                    if (payments.length() > 0) {
                        String paymentType = payments.getJSONObject(0).getString("paymentType");
                        if ("P91".equals(paymentType)) {
                            if("Y".equals(item.getIsReceivable())){
                                aq.id(holder.paytypeIconView).image(R.mipmap.icon_pay_unpaid);
                            }else{
                                aq.id(holder.paytypeIconView).image(R.mipmap.icon_pay_local_card);
                                Log.e("picon_paytype", "icon_pay_local_card");
                            }
                        } else if ("P92".equals(paymentType)) {
                            if("Y".equals(item.getIsReceivable())){
                                aq.id(holder.paytypeIconView).image(R.mipmap.icon_pay_unpaid);
                            }else{
                                aq.id(holder.paytypeIconView).image(R.mipmap.icon_pay_local_cash);
                            }
                        } else if("P51".equals(paymentType)){
                            /* TODO

                             */
                            aq.id(holder.paytypeIconView).image(R.mipmap.icon_pay_11st_refund);

                        } else {
                            aq.id(holder.paytypeIconView).image(R.mipmap.icon_pay_paid);
                        }
                    }else{
                        aq.id(holder.paytypeIconView).image(R.mipmap.icon_pay_paid);
                    }
                }else if("N".equals(item.getIsAdditionalPayment())){
                    // 추가요금 결제필요

                }else if("Y".equals(item.getIsAdditionalPayment())){
                    aq.id(holder.paytypeIconView).image(R.mipmap.icon_pay_paid);
                }
            }
        }else{
            // 결제방식 미선택
            aq.id(holder.paytypeIconView).image(R.mipmap.icon_pay_unselect);
        }

    }

    public void listItemSelected(View button) {
        int position = (int) button.getTag();
        if (ctx instanceof GSOrderListActivity) {
            ((GSOrderListActivity) ctx).itemSelected(position, data.get(position).getJsonData());
        }

    }
}
