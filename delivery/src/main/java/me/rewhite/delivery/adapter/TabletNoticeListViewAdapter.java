package me.rewhite.delivery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;

import java.util.ArrayList;

import me.rewhite.delivery.R;
import me.rewhite.delivery.tablet.TabletSettingActivity;
import me.rewhite.delivery.util.TimeUtil;


/**
 * Created by marines on 2015. 10. 5..
 */
public class TabletNoticeListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<NoticeListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;


    public TabletNoticeListViewAdapter(Context context, int layout, ArrayList<NoticeListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public NoticeListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        NoticeListItem item = data.get(position);

        aq.id(R.id.index_id).text(item.getNoticeId() + "");
        aq.id(R.id.text_title).text(item.getTitle());
        aq.id(R.id.text_date).text(TimeUtil.convertTimestampToString(item.getRegisterDateApp()));
        aq.id(R.id.content).clicked(this, "listItemSelected").tag(position);

        return convertView;
    }

    public void goTargetClicked(View button){
        int position = (int) button.getTag();

        //((TabletMainActivity) ctx).goTargetClicked(position, data.get(position).getJsonData());
    }

    public void listItemSelected(View button) {
        int position = (int) button.getTag();
        NoticeListItem item = data.get(position);
        ((TabletSettingActivity) ctx).itemSelected(position, item.getNoticeUrl(), item.getTitle());
    }
}
