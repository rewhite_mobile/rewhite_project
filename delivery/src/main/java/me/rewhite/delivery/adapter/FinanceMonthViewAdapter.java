package me.rewhite.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.rewhite.delivery.R;
import me.rewhite.delivery.data.MonthlyFinanceModel;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.MZValidator;

/**
 * Created by marines on 16. 6. 24..
 */
public class FinanceMonthViewAdapter  extends RecyclerView.Adapter
        <FinanceMonthViewAdapter.ListItemViewHolder> {

    private List<MonthlyFinanceModel> items;
    private SparseBooleanArray selectedItems;

    public FinanceMonthViewAdapter(List<MonthlyFinanceModel> modelData) {
        if (modelData == null) {
            throw new IllegalArgumentException("modelData must not be null");
        }
        items = modelData;
        selectedItems = new SparseBooleanArray();
    }

    /**
     * Adds and item into the underlying data set
     * at the position passed into the method.
     *
     * @param newModelData The item to add to the data set.
     * @param position The index of the item to remove.
     */
    public void addData(MonthlyFinanceModel newModelData, int position) {
        items.add(position, newModelData);
        notifyItemInserted(position);
    }

    /**
     * Removes the item that currently is at the passed in position from the
     * underlying data set.
     *
     * @param position The index of the item to remove.
     */
    public void removeData(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    public MonthlyFinanceModel getItem(int position) {
        return items.get(position);
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.tablet_finance_fragment_date_pick, viewGroup, false);

        return new ListItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder viewHolder, int position) {
        MonthlyFinanceModel model = items.get(position);

        CommonUtility.setTypefaceBoldSetup(viewHolder.label);
        CommonUtility.setTypefaceBoldSetup(viewHolder.priceLabel);

        viewHolder.label.setText(model.getLabel());
        viewHolder.priceLabel.setText(MZValidator.toNumFormat(model.getTotalPrice())+" 원");
        viewHolder.itemView.setActivated(selectedItems.get(position, false));

        //aq.id(R.id.date_text).text(data.getString("calculateDateString"));
        //aq.id(R.id.price_text).text(MZValidator.toNumFormat(data.getInt("totalPrice"))  + "원");
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        }
        else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<Integer>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public final static class ListItemViewHolder extends RecyclerView.ViewHolder {
        TextView label;
        TextView priceLabel;

        public ListItemViewHolder(View itemView) {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.date_text);
            priceLabel = (TextView) itemView.findViewById(R.id.price_text);


        }
    }
}
