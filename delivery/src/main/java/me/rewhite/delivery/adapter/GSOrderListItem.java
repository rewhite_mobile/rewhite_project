package me.rewhite.delivery.adapter;

/**
 * Created by marines on 2015. 10. 12..
 */
public class GSOrderListItem extends OrderListItem{

    private int packQuantity;

    public int getPackQuantity() {
        return packQuantity;
    }

    public int getBoxQuantity() {
        return boxQuantity;
    }

    public String getPartnerOrderNo() {
        return partnerOrderNo;
    }

    public String getPartnerStoreName() {
        return partnerStoreName;
    }

    public String getPartnerStoreId() {
        return partnerStoreId;
    }

    public String getPartnerId() {
        return partnerId;
    }
    public String getIsNewPOS() {
        return isNewPOS;
    }

    private int boxQuantity;
    private String partnerOrderNo;
    private String partnerStoreName;
    private String partnerStoreId;
    private String partnerId;
    private String isNewPOS;

    public GSOrderListItem(String orderId, String orderStatus, String addressSeq, String storeId, String storeName, String deliveryPrice,
                           String pickupRequestTimeApp, String deliveryRequestTimeApp, String registerDateApp, String orderRequest, String statusTimeApp,
                           String orderPickupItemMessage, String jsonData, int preQuantity, String isPayment, String payments, String isReceivable, String isAdditionalPayment,
                           int packQuantity, int boxQuantity, String partnerOrderNo, String partnerStoreName, String partnerStoreId, String partnerId, String isNewPOS) {
        super(orderId, orderStatus, addressSeq, storeId, storeName, deliveryPrice, pickupRequestTimeApp, deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage, jsonData, preQuantity, isPayment, payments, isReceivable, isAdditionalPayment);

        this.packQuantity = packQuantity;
        this.boxQuantity = boxQuantity;
        this.partnerOrderNo = partnerOrderNo;
        this.partnerStoreId = partnerStoreId;
        this.partnerId = partnerId;
        this.partnerStoreName = partnerStoreName;
        this.isNewPOS = isNewPOS;
    }
}
