package me.rewhite.delivery.adapter;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import me.rewhite.delivery.R;


/**
 * Created by marines on 2015. 10. 5..
 */
public class StoreListViewAdapter extends RecyclerView.Adapter<StoreListViewAdapter.MyViewHolder> {

    private ArrayList<GSStoreListItem> data;
    AQuery aq;
    public MyAdapterListener onClickListener;

    public interface MyAdapterListener {
        void naviButtonViewOnClick(View v, int position);
        void orderViewOnClick(View v, int position);
    }

    public StoreListViewAdapter (ArrayList<GSStoreListItem> myValues, MyAdapterListener listener){
        this.data = myValues;
        this.onClickListener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.gs_store_item, parent, false);
        aq = new AQuery(listItem);
        return new MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.onClickListener = this.onClickListener;

        GSStoreListItem item = data.get(position);

        String storeName = item.getPartnerStoreName();
        holder.storeNameTextView.setText(storeName);
        holder.pickCountTextView.setText(item.getPickCount() + "건");
        holder.deliveryCountTextView.setText(item.getDeliveryCount() + "건");
        holder.storedOrderCountTextView.setText(item.getStoredOrderCount() + "건");
        holder.storedBoxCountTextView.setText(item.getStoredBoxCount() + "개");

        if("7".equals(item.getPartnerId())){
            holder.orderReferView.setImageResource(R.mipmap.icon_list_gs);
            holder.topInfoArea.setBackgroundColor(Color.parseColor("#00c3d5"));
            holder.naviButton.setBackgroundColor(Color.parseColor("#00b3c4"));
        }else{
            holder.orderReferView.setImageResource(R.mipmap.icon_list_place);
            holder.topInfoArea.setBackgroundColor(Color.parseColor("#00aeef"));
            holder.naviButton.setBackgroundColor(Color.parseColor("#0098d1"));
        }


        int calHour = (item.getPartnerStoreVisitHour() % 12 == 0) ? 12 : item.getPartnerStoreVisitHour() % 12;
        String apm = (item.getPartnerStoreVisitHour() > 12) ? "오후" : "오전";
        String result = apm + " " + calHour + "시 ~ " + (calHour + 1) + "시";

        holder.visitTimeTextView.setText("방문시간 : " + result);
        holder.storeAddressTextView.setText(item.getPartnerStoreAddress());


//            double lat = myValues.getJSONObject(position).getDouble("latitude");
//            holder.lat = lat;
//            double lng = myValues.getJSONObject(position).getDouble("longitude");
//            holder.lng = lng;

    }

    public GSStoreListItem getItemValue(int position)  {
        return data.get(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView storeNameTextView;
        private TextView pickCountTextView;
        private TextView deliveryCountTextView;
        private TextView storedOrderCountTextView;
        private TextView storedBoxCountTextView;
        private TextView visitTimeTextView;
        private TextView storeAddressTextView;
        public Button naviButton;
        public Button listButton;

        private ImageView orderReferView;
        private View topInfoArea;

        public MyAdapterListener onClickListener;

        public double lat;
        public double lng;

        @SuppressLint({"SetJavaScriptEnabled"})
        public MyViewHolder(View itemView) {
            super(itemView);
            topInfoArea = (View)itemView.findViewById(R.id.top_info);
            storeNameTextView = (TextView)itemView.findViewById(R.id.text_store);
            pickCountTextView = (TextView)itemView.findViewById(R.id.text_gs_pickup_count);
            deliveryCountTextView = (TextView)itemView.findViewById(R.id.text_gs_delivery_count);
            storedOrderCountTextView = (TextView)itemView.findViewById(R.id.text_gs_stored_count);
            storedBoxCountTextView = (TextView)itemView.findViewById(R.id.text_gs_stored_box_count);
            visitTimeTextView = (TextView)itemView.findViewById(R.id.visitTimeTextView);
            storeAddressTextView = (TextView)itemView.findViewById(R.id.text_store_address);
            orderReferView = (ImageView)itemView.findViewById(R.id.order_refer);
            naviButton = (Button)itemView.findViewById(R.id.btn_navi);
            naviButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onClickListener != null){
                        onClickListener.naviButtonViewOnClick(v, getAdapterPosition());
                    }
                }
            });
            listButton = (Button)itemView.findViewById(R.id.btn_order_list);
            listButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onClickListener != null){
                        onClickListener.orderViewOnClick(v, getAdapterPosition());
                    }
                }
            });
        }

    }

}
