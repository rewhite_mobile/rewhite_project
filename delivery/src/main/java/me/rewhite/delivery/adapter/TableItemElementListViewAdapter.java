package me.rewhite.delivery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.tablet.TabletOrderInputActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.MZValidator;


/**
 * Created by marines on 2015. 10. 5..
 */
public class TableItemElementListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private JSONArray data;
    private int layout;
    private Context ctx;
    AQuery aq;

    /*
    private String orderId;
    private String orderStatus;
    private String addressSeq;
    private String storeId;
    private String deliveryPrice;
    private String pickupRequestTimeApp;
    private String deliveryRequestTimeApp;
    private String registerDateApp;
    private String orderRequest;
     */

    public TableItemElementListViewAdapter(Context context, int layout, JSONArray data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.length();
    }

    @Override
    public JSONObject getItem(int position) {
        JSONObject obj = new JSONObject();
        try {
            obj = data.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        if(position%2 == 0){
            aq.id(R.id.content).backgroundColor(0xffffffff);
        }else{
            aq.id(R.id.content).backgroundColor(0xfff5f5f5);
        }

        try {
            JSONObject item = data.getJSONObject(position);

            //Log.e("TablePickupElementListViewAdapter elemnt", item.toString());

            CommonUtility.setTypefaceNormalSetup(aq.id(R.id.tag_id).getTextView());
            CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_title).getTextView());
            CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_price).getTextView());
            //CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_detail).getTextView());

            int tagno = item.getInt("tagId");
            aq.id(R.id.tag_id).text(MZValidator.getTagFormmater(tagno,4));

            aq.id(R.id.text_price).text(MZValidator.toNumFormat(item.getInt("confirmPrice")));


            String laundry = item.getString("laundry");
            if("".equals(laundry)){
                aq.id(R.id.icon_01).gone();
            }else{
                aq.id(R.id.icon_01).visible();
            }
            String isRepair = (item.getJSONArray("repair").length() > 0)?"수선":null;
            String isAdditional = (item.getJSONArray("additional").length() > 0)?"추가기술":null;
            String isPart = (item.getJSONArray("part").length() > 0)?"부속품":null;

            boolean isPhoto = false;
            if(item.getJSONArray("photo").length() > 0){
                isPhoto = true;
                aq.id(R.id.icon_03).visible();
            }else{
                aq.id(R.id.icon_03).gone();
            }
            String isService = item.getString("isService");
            String isReLaundry = item.getString("isReLaundry");
            /*
            TODO
             서버로부터 받은 값에 SP 값이 필요함.
             */
            String itemColor = item.getString("itemColor");
            if("".equals(itemColor)){
                aq.id(R.id.icon_04).gone();
            }else{
                int resId = ctx.getResources().getIdentifier("pui_order_element_color_" + itemColor, "mipmap", "me.rewhite.delivery");
                aq.id(R.id.icon_04).image(resId);
            }

            int sp = 0;
            if(item.isNull("sp")){
                sp = item.getInt("itemSP");
            }else{
                sp = item.getInt("sp");
            }

            String itemTitle = item.getString("itemTitle");
            itemTitle = itemTitle.replace("\n","");

            switch(sp){
                case 1:
                    itemTitle += "(일반)";
                    break;
                case 2:
                    itemTitle += "(명품)";
                    break;
                case 3:
                    itemTitle += "(아동)";
                    break;
            }
            aq.id(R.id.text_title).text(itemTitle);

            String detailString = laundry;

            if(isRepair != null){
                if("".equals(detailString)){
                    detailString += "수선("+item.getJSONArray("repair").length()+")";
                }else{
                    detailString += ", " + "수선("+item.getJSONArray("repair").length()+")";
                }
                aq.id(R.id.icon_02).visible();
            }else{
                aq.id(R.id.icon_02).gone();
            }
            if(isAdditional != null){
                if("".equals(detailString)){
                    detailString += isAdditional +"("+item.getJSONArray("additional").length()+")";
                }else{
                    detailString += ", " + isAdditional +"("+item.getJSONArray("additional").length()+")";
                }

            }
            if(isPart != null){
                if("".equals(detailString)){
                    detailString += isPart +"("+item.getJSONArray("part").length()+")";
                }else{
                    detailString += ", " + isPart +"("+item.getJSONArray("part").length()+")";
                }
            }
            if(isService != null){
                if("Y".equals(isService)){
                    if("".equals(detailString)){
                        detailString += "무료서비스";
                    }else{
                        detailString += ", " + "무료서비스";
                    }
                }
            }
            if(isReLaundry != null){
                if("Y".equals(isReLaundry)){
                    if("".equals(detailString)){
                        detailString += "재세탁";
                    }else{
                        detailString += ", " + "재세탁";
                    }
                }
            }
            aq.id(R.id.text_detail).text(detailString);

            aq.id(R.id.btn_delete).clicked(this, "deleteAction").tag(position);
            aq.id(R.id.btn_detail).clicked(this, "detailAction").tag(position);
            aq.id(R.id.btn_copy).clicked(this, "copyItemAction").tag(position);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public void deleteAction(View button) {
        int position = (int) button.getTag();

        ((TabletOrderInputActivity) ctx).deleteSelected(position);
    }
    public void detailAction(View button) {
        int position = (int) button.getTag();

        ((TabletOrderInputActivity) ctx).itemSelected(position);
    }
    public void copyItemAction(View button){
        int position = (int) button.getTag();

        ((TabletOrderInputActivity) ctx).copySelected(position);
    }

}
