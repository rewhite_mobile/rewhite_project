package me.rewhite.delivery.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.rewhite.delivery.R;
import me.rewhite.delivery.tablet.LocalOrderOutputActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.TimeUtil;


/**
 * Created by marines on 2015. 10. 5..
 */
public class TabletOrderListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<TabletOrderListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;

    /*
    private String orderId;
    private String orderStatus;
    private String addressSeq;
    private String storeId;
    private String deliveryPrice;
    private String pickupRequestTimeApp;
    private String deliveryRequestTimeApp;
    private String registerDateApp;
    private String orderRequest;
     */

    public TabletOrderListViewAdapter(Context context, int layout, ArrayList<TabletOrderListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public TabletOrderListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        TabletOrderListItem item = data.get(position);
        String orderId = item.getOrderId();

        CommonUtility.setTypefaceUltraLightSetup(aq.id(R.id.text_items).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_orderno).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_status).getTextView());

        aq.id(R.id.text_items).text(item.getOrderPickupItemMessage());
        aq.id(R.id.text_date).text(TimeUtil.convertTimestampToString2(Long.parseLong(item.getRegisterDateApp())));
        aq.id(R.id.text_orderno).text(item.getOrderId());

        try {
            JSONObject json = new JSONObject(item.getJsonData());
            if(json.getInt("orderStatus") == 24){
                //aq.id(R.id.text_status).text("출고완료");
                if("Y".equals(json.getString("isReceivable"))){
                    aq.id(R.id.text_status).text("외상출고").textColor(Color.parseColor("#f53941"));
                    aq.id(R.id.content).backgroundColor(Color.parseColor("#ffe1e2"));
                }else{
                    aq.id(R.id.text_status).text("출고완료").textColor(Color.parseColor("#a3a3a3"));
                    aq.id(R.id.content).backgroundColor(Color.parseColor("#dcdcdc"));
                }
            }else{
                if(json.getInt("orderStatus") == 91){
                    aq.id(R.id.text_status).text("취소").textColor(Color.parseColor("#a3a3a3"));
                    aq.id(R.id.content).backgroundColor(Color.parseColor("#cccccc"));
                }else if(json.getInt("orderStatus") == 93){
                    aq.id(R.id.text_status).text("환불").textColor(Color.parseColor("#a3a3a3"));
                    aq.id(R.id.content).backgroundColor(Color.parseColor("#cccccc"));
                }else if(json.getInt("orderStatus") == 94){
                    aq.id(R.id.text_status).text("보상요청").textColor(Color.parseColor("#a3a3a3"));
                    aq.id(R.id.content).backgroundColor(Color.parseColor("#cccccc"));
                }else{
                    aq.id(R.id.text_status).text("출고준비").textColor(Color.parseColor("#082c54"));
                    aq.id(R.id.content).backgroundColor(Color.parseColor("#ffffff"));
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        aq.id(R.id.content).clicked(this, "listItemSelected").tag(position);

        return convertView;
    }

    public void listItemSelected(View button) {
        int position = (int) button.getTag();
        if (ctx instanceof LocalOrderOutputActivity) {
            try {
                ((LocalOrderOutputActivity) ctx).itemSelected(position, new JSONObject(data.get(position).getJsonData()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}
