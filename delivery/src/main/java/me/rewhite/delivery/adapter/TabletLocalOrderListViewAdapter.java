package me.rewhite.delivery.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;

import org.json.JSONException;

import java.util.ArrayList;

import me.rewhite.delivery.R;
import me.rewhite.delivery.tablet.TabletLocalOrderListActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.TimeUtil;


/**
 * Created by marines on 2015. 10. 5..
 */
public class TabletLocalOrderListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<OrderListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;

    /*
    private String orderId;
    private String orderStatus;
    private String addressSeq;
    private String storeId;
    private String deliveryPrice;
    private String pickupRequestTimeApp;
    private String deliveryRequestTimeApp;
    private String registerDateApp;
    private String orderRequest;
     */

    public TabletLocalOrderListViewAdapter(Context context, int layout, ArrayList<OrderListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public OrderListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        OrderListItem item = data.get(position);
        String storeName = item.getStoreName();
        String orderPickupItemMessage = item.getOrderPickupItemMessage();
        String orderRequest = item.getOrderRequest();
        String orderId = item.getOrderId();
        int orderCount = item.getPickupQuantity();

        String price = item.getDeliveryPrice();
        int priceValue = Integer.parseInt(price);
        String outputPrice = price;

        outputPrice = MZValidator.toNumFormat(priceValue);

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_orderid).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_price).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_element).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_date).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_time).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_store).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.order_type_text).getTextView());


        aq.id(R.id.text_store).text(storeName);

        try{
            String orderSubType = item.getJsonData().getString("orderSubType");
            String orderTypeString = "일반세탁";
//            if("101".equals(orderSubType)){
//                aq.id(R.id.order_type_text).textColor(0xFF5eaeff).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
//            }else if("201".equals(orderSubType)){
//                orderTypeString = "택배접수";
//                aq.id(R.id.order_type_text).textColor(0xFF787878).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
//            }else if("105".equals(orderSubType)){
//                orderTypeString = "빠른세탁 - 당일";
//                aq.id(R.id.order_type_text).textColor(0xFFfd7070).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
//            }else if("104".equals(orderSubType)){
//                orderTypeString = "빠른세탁 - 익일";
//                aq.id(R.id.order_type_text).textColor(0xFFff845e).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
//            }else if("301".equals(orderSubType)){
//                orderTypeString = "침구제휴세탁";
//                aq.id(R.id.order_type_text).textColor(0xFFa957e8).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
//                aq.id(R.id.text_store).text(item.getJsonData().getString("label")).typeface(CommonUtility.getNanumBarunTypeface());
//            }

            if(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP) == null){
                SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.DELIVERY_GAP, "1");
            }

            aq.id(R.id.date_area).visible();

            if ("01".equals(item.getOrderStatus())) {
                aq.id(R.id.order_type_text).text("수거방문예정").textColor(Color.parseColor("#074bb0"));
//                if("105".equals(orderSubType)){
//                    aq.id(R.id.status_image).image(R.mipmap.img_list_spd_t_01);
//                }else if("104".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_n_01);
//                }else if("301".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_b_01);
//                }else{
//                    aq.id(R.id.back).image(R.mipmap.back_order_history_01);
//                }
                aq.id(R.id.status_image).image(R.mipmap.order_status_circle_01);
                aq.id(R.id.text_price).text("세탁요금 미정");
                //item.setJsonData().getString();

                // DELIVERY_GAP 없는경우 처리 필요
                aq.id(R.id.text_element).text("아직 수거하기 전입니다.");

                String pTime = TimeUtil.getDateByMDSH(Long.parseLong(item.getPickupRequestTimeApp()), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP));
                String[] pTimeCol = pTime.split(" ");
                aq.id(R.id.text_date).text(pTimeCol[0]);
                aq.id(R.id.text_time).text(pTimeCol[1] + " " + pTimeCol[2]).textColor(Color.parseColor("#074bb0"));
                aq.id(R.id.image_clock).image(R.mipmap.order_status_clock_blue);

            } else if ("02".equals(item.getOrderStatus())) {
                aq.id(R.id.order_type_text).text("수거방문 출발").textColor(Color.parseColor("#074bb0"));
                aq.id(R.id.status_image).image(R.mipmap.order_status_circle_01);
//                if("105".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_t_01);
//                }else if("104".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_n_01);
//                }else if("301".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_b_01);
//                }else{
//                    aq.id(R.id.back).image(R.mipmap.back_order_history_01);
//                }
                aq.id(R.id.text_price).text("세탁요금 미정");
                //aq.id(R.id.text_element).text("수거후 표시예정");
                //aq.id(R.id.text_element).text("수거: "+TimeUtil.getDateByMDSH(Long.parseLong(item.getPickupRequestTimeApp()), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP)));
                aq.id(R.id.text_element).text("수거를 위해 출발한 상태입니다.");
                //aq.id(R.id.text_date).text(TimeUtil.getDateByMDSH(Long.parseLong(item.getPickupRequestTimeApp()), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP)));
                String pTime = TimeUtil.getDateByMDSH(Long.parseLong(item.getPickupRequestTimeApp()), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP));
                String[] pTimeCol = pTime.split(" ");
                aq.id(R.id.text_date).text(pTimeCol[0]);
                aq.id(R.id.text_time).text(pTimeCol[1] + " " + pTimeCol[2]).textColor(Color.parseColor("#074bb0"));
                aq.id(R.id.image_clock).image(R.mipmap.order_status_clock_blue);
            } else if ("03".equals(item.getOrderStatus())) {
//                //aq.id(R.id.text_status).text("세탁물 입고중");
//                if("105".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_t_02);
//                }else if("104".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_n_02);
//                }else if("301".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_b_02);
//                }else{
//                    aq.id(R.id.back).image(R.mipmap.back_order_history_02);
//                }
                aq.id(R.id.order_type_text).text("세탁물 입고중").textColor(Color.parseColor("#074bb0"));
                aq.id(R.id.status_image).image(R.mipmap.order_status_circle_02);
                aq.id(R.id.text_price).text("세부항목을 입력해주세요.");
                //aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(R.id.text_element).text("수거수량 : " + orderCount + "개");
                aq.id(R.id.date_area).gone();

            } else if ("11".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("세탁 준비중");
//                if("105".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_t_03);
//                }else if("104".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_n_03);
//                }else if("301".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_b_03);
//                }else{
//                    aq.id(R.id.back).image(R.mipmap.back_order_history_03);
//                }
                aq.id(R.id.order_type_text).text("세탁 준비중").textColor(Color.parseColor("#787878"));
                aq.id(R.id.status_image).image(R.mipmap.order_status_circle_03);
                aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(R.id.text_element).text(orderPickupItemMessage);

                aq.id(R.id.date_area).gone();
            } else if ("12".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("세탁중");
//                if("105".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_t_04);
//                }else if("104".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_n_04);
//                }else if("301".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_b_04);
//                }else{
//                    aq.id(R.id.back).image(R.mipmap.back_order_history_04);
//                }
                aq.id(R.id.order_type_text).text("세탁중").textColor(Color.parseColor("#787878"));
                aq.id(R.id.status_image).image(R.mipmap.order_status_circle_04);
                aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(R.id.text_element).text(orderPickupItemMessage);

                aq.id(R.id.date_area).gone();
            } else if ("13".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("세탁완료");
//                if("105".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_t_05);
//                }else if("104".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_n_05);
//                }else if("301".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_b_05);
//                }else{
//                    aq.id(R.id.back).image(R.mipmap.back_order_history_05);
//                }
                aq.id(R.id.order_type_text).text("세탁완료").textColor(Color.parseColor("#787878"));
                aq.id(R.id.status_image).image(R.mipmap.order_status_circle_05);
                aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(R.id.text_element).text(orderPickupItemMessage);

                aq.id(R.id.date_area).gone();
            } else if ("21".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("배송준비중");
//                if("105".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_t_06);
//                }else if("104".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_n_06);
//                }else if("301".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_b_06);
//                }else{
//                    aq.id(R.id.back).image(R.mipmap.back_order_history_06);
//                }
                aq.id(R.id.order_type_text).text("배송준비중").textColor(Color.parseColor("#ef3737"));
                aq.id(R.id.status_image).image(R.mipmap.order_status_circle_06);
                aq.id(R.id.text_price).text(outputPrice + " 원");
                //aq.id(R.id.text_element).text(orderPickupItemMessage);

                String dTime = TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP));
                String[] dTimeCol = dTime.split(" ");
                aq.id(R.id.text_date).text(dTimeCol[0]);
                aq.id(R.id.text_time).text(dTimeCol[1] + " " + dTimeCol[2]).textColor(Color.parseColor("#ef3737"));
                aq.id(R.id.image_clock).image(R.mipmap.order_status_clock_red);
                aq.id(R.id.text_element).text(orderPickupItemMessage);
            } else if ("22".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("배송중");
//                if("105".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_t_07);
//                }else if("104".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_n_07);
//                }else if("301".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_b_07);
//                }else{
//                    aq.id(R.id.back).image(R.mipmap.back_order_history_07);
//                }
                aq.id(R.id.order_type_text).text("배송중").textColor(Color.parseColor("#ef3737"));
                aq.id(R.id.status_image).image(R.mipmap.order_status_circle_07);
                aq.id(R.id.text_price).text(outputPrice + " 원");

                String dTime = TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP));
                String[] dTimeCol = dTime.split(" ");
                aq.id(R.id.text_date).text(dTimeCol[0]);
                aq.id(R.id.text_time).text(dTimeCol[1] + " " + dTimeCol[2]).textColor(Color.parseColor("#ef3737"));
                aq.id(R.id.image_clock).image(R.mipmap.order_status_clock_red);
                //aq.id(R.id.text_date).text(TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP)));
                aq.id(R.id.text_element).text(orderPickupItemMessage);
                //aq.id(R.id.text_element).text("배송: "+TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP)) + "\n" + orderPickupItemMessage);
            } else if ("23".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("배송완료");
//                if("105".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_t_08);
//                }else if("104".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_n_08);
//                }else if("301".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_b_08);
//                }else{
//                    aq.id(R.id.back).image(R.mipmap.back_order_history_08);
//                }
                aq.id(R.id.order_type_text).text("배송완료").textColor(Color.parseColor("#ef3737"));
                aq.id(R.id.status_image).image(R.mipmap.order_status_circle_08);
                aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(R.id.text_element).text(orderPickupItemMessage);

                aq.id(R.id.date_area).gone();
            } else if ("24".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("방문수령완료");
//                if("105".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_t_24);
//                }else if("104".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_n_24);
//                }else if("301".equals(orderSubType)){
//                    aq.id(R.id.back).image(R.mipmap.img_list_spd_b_24);
//                }else{
//                    aq.id(R.id.back).image(R.mipmap.back_order_history_24);
//                }
                aq.id(R.id.order_type_text).text("방문수령완료");
                aq.id(R.id.status_image).image(R.mipmap.order_status_circle_08);
                aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(R.id.text_element).text(orderPickupItemMessage);

                aq.id(R.id.date_area).gone();
            } else if ("91".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("취소");
//                aq.id(R.id.back).image(R.mipmap.back_order_history_91);
                aq.id(R.id.order_type_text).text("취소");
                aq.id(R.id.status_image).image(R.mipmap.order_status_circle_08);
                aq.id(R.id.price_area).gone();
                aq.id(R.id.text_element).text(orderPickupItemMessage);

                aq.id(R.id.date_area).gone();
            } else {
                //aq.id(R.id.text_status).text("기타");
//                aq.id(R.id.back).image(R.mipmap.back_order_history_99);
                aq.id(R.id.order_type_text).text("취소");
                aq.id(R.id.status_image).image(R.mipmap.order_status_circle_08);
                aq.id(R.id.price_area).gone();
                aq.id(R.id.text_element).text(orderPickupItemMessage);

                aq.id(R.id.date_area).gone();
            }

        }catch(JSONException e){

        }
/*
        aq.id(R.id.icon_01).gone();
        aq.id(R.id.icon_02).gone();
        aq.id(R.id.icon_03).gone();

        for (int i = 0; i < orderRequest.length(); i++) {
            if ("1".equals(String.valueOf(orderRequest.charAt(i)))) {
                switch (i) {
                    case 0:
                        aq.id(R.id.icon_01).visible();
                        break;
                    case 1:
                        aq.id(R.id.icon_02).visible();
                        break;
                    case 2:
                        aq.id(R.id.icon_03).visible();
                        break;
                    default:
                        break;
                }
            }
        }
*/


        aq.id(R.id.text_orderid).text("" + orderId);

        aq.id(R.id.content).clicked(this, "listItemSelected").tag(position);

        return convertView;
    }

    public void listItemSelected(View button) {
        int position = (int) button.getTag();
        if (ctx instanceof TabletLocalOrderListActivity) {
            ((TabletLocalOrderListActivity) ctx).itemSelected(position, data.get(position).getJsonData());
        }

    }
}
