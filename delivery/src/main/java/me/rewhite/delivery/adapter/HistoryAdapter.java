package me.rewhite.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.List;

import me.rewhite.delivery.R;
import me.rewhite.delivery.view.HistoryItem;

/**
 * Created by marines on 15. 7. 30..
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder>{

    List<HistoryItem> mItems;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public HistoryAdapter(List<HistoryItem> itemsData,
                          OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
        this.mItems = itemsData;
    }

    private AQuery aq;
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.order_history_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        aq = new AQuery(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        HistoryItem historyElement = mItems.get(position);
        viewHolder.productTitleElement.setText(historyElement.getProductTitle());
        viewHolder.priceElement.setText(historyElement.getPaidPrice() + "원");
        viewHolder.addressElement.setText(historyElement.getAddress());
        viewHolder.statusElement.setText(historyElement.getStatus());
        //viewHolder.dateElement.setText(historyElement.getTimestamp());

        if (historyElement.getProfilePath() != null) {
            aq.id(R.id.img_thumbnail).image(historyElement.getProfilePath());
        }

        //viewHolder.imgThumbnail.setImageResource(historyElement.getProfilePath());

        viewHolder.container.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public View container;
        public ImageView imgThumbnail;
        public TextView productTitleElement;
        public TextView priceElement;
        public TextView addressElement;
        public TextView statusElement;
        public TextView dateElement;

        public ViewHolder(View itemView) {
            super(itemView);
            container = itemView;
            imgThumbnail = (ImageView)itemView.findViewById(R.id.img_thumbnail);
            productTitleElement = (TextView)itemView.findViewById(R.id.title);
            priceElement = (TextView)itemView.findViewById(R.id.price_text);
            addressElement = (TextView)itemView.findViewById(R.id.address_text);
            statusElement = (TextView)itemView.findViewById(R.id.status_text);
            dateElement = (TextView)itemView.findViewById(R.id.date_text);
        }
    }

}
