package me.rewhite.delivery.adapter;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by marines on 2015. 10. 12..
 */
public class OrderListItem {

    public String getOrderId() {
        return orderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getAddressSeq() {
        return addressSeq;
    }

    public String getStoreId() {
        return storeId;
    }

    public String getDeliveryPrice() {
        return deliveryPrice;
    }

    public String getPickupRequestTimeApp() {
        return pickupRequestTimeApp;
    }

    public String getDeliveryRequestTimeApp() {
        return deliveryRequestTimeApp;
    }

    public String getRegisterDateApp() {
        return registerDateApp;
    }

    public String getOrderRequest() {
        return orderRequest;
    }

    private String orderId;
    private String orderStatus;
    private String addressSeq;
    private String storeId;
    private String deliveryPrice;
    private String pickupRequestTimeApp;
    private String deliveryRequestTimeApp;
    private String registerDateApp;
    private String orderRequest;


    public int getPickupQuantity() {
        return pickupQuantity;
    }

    public void setPickupQuantity(int pickupQuantity) {
        this.pickupQuantity = pickupQuantity;
    }

    private int pickupQuantity;

    public JSONObject getJsonData() {
        return jsonData;
    }

    public void setJsonData(JSONObject jsonData) {
        this.jsonData = jsonData;
    }

    private JSONObject jsonData;

    public String getStoreName() {
        return storeName;
    }

    private String storeName;

    public String getOrderPickupItemMessage() {
        return orderPickupItemMessage;
    }

    private String orderPickupItemMessage;

    public String getStatusTimeApp() {
        return statusTimeApp;
    }

    private String statusTimeApp;

    public String getIsPayment() {
        return isPayment;
    }
    private String isPayment;

    public String getPayments() {
        return payments;
    }
    private String payments;

    public String getIsReceivable() {
        return isReceivable;
    }
    private String isReceivable;

    public String getIsAdditionalPayment() {
        return isAdditionalPayment;
    }
    private String isAdditionalPayment;

    public OrderListItem(String orderId, String orderStatus, String addressSeq, String storeId, String storeName, String deliveryPrice,
                         String pickupRequestTimeApp, String deliveryRequestTimeApp, String registerDateApp, String orderRequest, String statusTimeApp,
                         String orderPickupItemMessage, String jsonData, int preQuantity, String isPayment, String payments, String isReceivable, String isAdditionalPayment) {
        this.orderId = orderId;
        this.orderStatus = orderStatus;
        this.addressSeq = addressSeq;
        this.storeId = storeId;
        this.storeName = storeName;
        this.deliveryPrice = deliveryPrice;
        this.pickupRequestTimeApp = pickupRequestTimeApp;
        this.deliveryRequestTimeApp = deliveryRequestTimeApp;
        this.registerDateApp = registerDateApp;
        this.orderRequest = orderRequest;
        this.statusTimeApp = statusTimeApp;
        this.orderPickupItemMessage = orderPickupItemMessage;
        this.pickupQuantity = preQuantity;
        this.isPayment = isPayment;
        this.payments = payments;
        this.isReceivable = isReceivable;
        this.isAdditionalPayment = isAdditionalPayment;
        try {
            this.jsonData = new JSONObject(jsonData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
