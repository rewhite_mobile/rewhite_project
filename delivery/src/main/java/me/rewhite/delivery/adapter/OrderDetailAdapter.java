package me.rewhite.delivery.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.List;

import me.rewhite.delivery.R;
import me.rewhite.delivery.view.OrderItem;

/**
 * Created by marines on 15. 7. 30..
 */
public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.ViewHolder>{

    List<OrderItem> mItems;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public OrderDetailAdapter(List<OrderItem> itemsData,
                              OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
        this.mItems = itemsData;
    }

    private AQuery aq;
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_ordered_detail_card_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        aq = new AQuery(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        OrderItem orderElement = mItems.get(position);
        viewHolder.productTitleElement.setText(orderElement.getProductName());
        viewHolder.actionElement.setText("EDIT");

        aq.id(viewHolder.imgThumbnail).image(orderElement.getProductImagePath());
        //viewHolder.imgThumbnail.setImageResource(historyElement.getProfilePath());

        viewHolder.actionElement.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public View container;
        public ImageView imgThumbnail;
        public TextView productTitleElement;
        public TextView actionElement;


        public ViewHolder(View itemView) {
            super(itemView);
            container = itemView;
            imgThumbnail = (ImageView)itemView.findViewById(R.id.img_thumbnail);
            productTitleElement = (TextView)itemView.findViewById(R.id.title);
            actionElement = (TextView)itemView.findViewById(R.id.action_text);

        }
    }

}
