package me.rewhite.delivery.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.tablet.TabletPriceSettingActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.SharedPreferencesUtility;

/**
 * Created by marines on 16. 6. 30..
 */
public class PriceExpandableAdapter extends BaseExpandableListAdapter {

    TabletPriceSettingActivity mActivity;
    Context mContext;
    JSONArray priceData;
    AQuery aq;

    public PriceExpandableAdapter(Context context, JSONArray _priceData ) {
        mContext = context;
        mActivity = (TabletPriceSettingActivity)context;
        priceData = _priceData;
    }

    @Override
    public int getGroupCount() {
        return priceData.length();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            return priceData.getJSONObject(groupPosition).getJSONArray("items").length();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public JSONObject getGroup(int groupPosition) {
        try {
            return priceData.getJSONObject(groupPosition);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public JSONObject getChild(int groupPosition, int childPosition) {
        try {
            return priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view;
        if(convertView == null) {
            view = getParentGenericView();
        } else {
            view = convertView;
        }

        TextView groupTitle = (TextView)view.findViewById(R.id.group_title);
        CommonUtility.setTypefaceBoldSetup(groupTitle);

        try {
            groupTitle.setText(priceData.getJSONObject(groupPosition).getString("itemTitle"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // 그룹을 펼칠때와 닫을때 아이콘을 변경해 준다.
        AQuery bq = new AQuery(view);
        ImageView iv = (ImageView)view.findViewById(R.id.group_arrow);
        if(isExpanded){
            bq.id(R.id.group_arrow).image(R.mipmap.icon_up_arrow);
        }else{
            bq.id(R.id.group_arrow).image(R.mipmap.icon_down_arrow);
        }

        return view;
    }

    private static final int ITEM_GROUP = 0;
    private static final int ITEM_CHILD = 1;
    private static final int ITEM_INDEX = 2;

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view;
        if(convertView == null) {
            view = getChildGenericView();
        } else {
            view = convertView;
        }

        aq = new AQuery(view);
        aq.id(R.id.price_01).clicked(this, "priceClickAction").tag(groupPosition*10000 + childPosition*10 + 0);
        aq.id(R.id.price_02).clicked(this, "priceClickAction").tag(groupPosition*10000 + childPosition*10 + 1);
        aq.id(R.id.price_03).clicked(this, "priceClickAction").tag(groupPosition*10000 + childPosition*10 + 2);

        LinearLayout ll = (LinearLayout)view.findViewById(R.id.element_layout);

        if("Y".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.IS_VISIT_PRICE))){
            // 방문시 일반금액과 수거배달금액 요금표가 다르게 존재하는 경우
            aq.id(R.id.price_04).clicked(this, "priceClickAction").tag(groupPosition*10000 + childPosition*10 + 3);
            ll.setWeightSum(6f);
            //ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,6f));
        }else{
            aq.id(R.id.price_04).gone();
            ll.setWeightSum(5f);
            //ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT,5f));
        }

        aq.id(R.id.btn_use).clicked(this, "useClickAction").tag(groupPosition*10000 + childPosition*10 + 4);

        try {
            TextView text = (TextView)view.findViewById(R.id.item_title);
            TextView price01 = (TextView)view.findViewById(R.id.price_01);
            TextView price02 = (TextView)view.findViewById(R.id.price_02);
            TextView price03 = (TextView)view.findViewById(R.id.price_03);
            TextView price04 = (TextView)view.findViewById(R.id.price_04);

            CommonUtility.setTypefaceNormalSetup(text);
            CommonUtility.setTypefaceBoldSetup(price01);
            CommonUtility.setTypefaceBoldSetup(price02);
            CommonUtility.setTypefaceBoldSetup(price03);
            CommonUtility.setTypefaceBoldSetup(price04);

            if("N".equals(priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getString("isUse"))){
                aq.id(R.id.btn_use).image(R.mipmap.pui_item_color_uncheck);
                aq.id(R.id.content).backgroundColor(Color.parseColor("#e5e5e5"));
                aq.id(R.id.price_01).backgroundColor(Color.parseColor("#666666"));
                aq.id(R.id.price_02).backgroundColor(Color.parseColor("#666666"));
                aq.id(R.id.price_03).backgroundColor(Color.parseColor("#666666"));
                aq.id(R.id.price_04).backgroundColor(Color.parseColor("#666666"));
            }else{
                aq.id(R.id.btn_use).image(R.mipmap.pui_item_color_check);
                aq.id(R.id.content).backgroundColor(Color.parseColor("#ffffff"));
                aq.id(R.id.price_01).background(R.drawable.pui_button_price);
                aq.id(R.id.price_02).background(R.drawable.pui_button_price);
                aq.id(R.id.price_03).background(R.drawable.pui_button_price);
                aq.id(R.id.price_04).background(R.drawable.pui_button_price);
            }

            text.setText(priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getString("itemTitle"));
            price02.setText(priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getString("storePrice2"));
            price03.setText(priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getString("storePrice3"));

            if("Y".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.IS_VISIT_PRICE))){
                price01.setText(priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getString("storePrice4"));
                price04.setText(priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getString("storePrice1"));
            }else{
                price01.setText(priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getString("storePrice1"));
                price04.setText(priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getString("storePrice4"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view;
    }

    public void useClickAction(View button){
        int tagValue = (int)button.getTag();
        int groupPosition = tagValue/10000;
        int childPosition = (tagValue%10000)/10;

        Bundle keyBundle = new Bundle();
        keyBundle.putInt("groupPosition", groupPosition);
        keyBundle.putInt("childPosition", childPosition);
        try {
            keyBundle.putString("data", priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mActivity.useModProcess(keyBundle);
    }

    public void priceClickAction(View button){
        int tagValue = (int)button.getTag();
        int groupPosition = tagValue/10000;
        int childPosition = (tagValue%10000)/10;
        int buttonIndex = tagValue%10;
        //Log.i("priceClickAction", groupPosition + "," + childPosition + "," + buttonIndex);

        Bundle keyBundle = new Bundle();
        keyBundle.putInt("groupPosition", groupPosition);
        keyBundle.putInt("childPosition", childPosition);
        keyBundle.putInt("buttonIndex", buttonIndex);
        int priceValue = 0;
        try {
            if(buttonIndex == 0){
                // 방문 일반요금
                if("Y".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.IS_VISIT_PRICE))){
                    priceValue = priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getInt("storePrice4");
                }else{
                    priceValue = priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getInt("storePrice1");
                }

            }else if(buttonIndex == 1){
                priceValue = priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getInt("storePrice2");
            }else if(buttonIndex == 2){
                priceValue = priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getInt("storePrice3");
            }else if(buttonIndex == 3){
                // 리화이트 요금
                if("Y".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.IS_VISIT_PRICE))){
                    priceValue = priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getInt("storePrice1");
                }else{
                    priceValue = priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).getInt("storePrice4");
                }
            }
            keyBundle.putInt("value", priceValue);
            keyBundle.putString("data", priceData.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        mActivity.priceModProcess(keyBundle);
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    //Child의 View의 XML을 생성
    public View getChildGenericView() {
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.price_expandable_list_child_item, null);
        return view;
    }

    //Parent(Group)의 View의 XML을 생성
    public View getParentGenericView() {
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.price_expandable_list_group_item, null);
        return view;
    }
}
