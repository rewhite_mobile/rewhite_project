package me.rewhite.delivery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import me.rewhite.delivery.R;
import me.rewhite.delivery.tablet2.main.TabletMainV2Activity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.StringUtil;
import me.rewhite.delivery.util.TimeUtil;


/**
 * Created by marines on 2015. 10. 5..
 */
public class TabletMainListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<OrderListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;


    public TabletMainListViewAdapter(Context context, int layout, ArrayList<OrderListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public OrderListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        OrderListItem item = data.get(position);
        String orderPickupItemMessage = item.getOrderPickupItemMessage();
        String orderId = item.getOrderId();
        //String orderInnerCode = item.getStoreId();
        int orderCount = item.getPickupQuantity();

        double distance;
        String meter;
        String deliveryInfo;
        String deliveryDuration;
        String deliveryTime;
        int saleStartTime = 6;

        try{
            String orderSubType = item.getJsonData().getString("orderSubType");
            String orderTypeString = "일반세탁";

            CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_user).getTextView());
            CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_status).getTextView());
            CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_date).getTextView());
            CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_address).getTextView());


            aq.id(R.id.status_image).visible();
            aq.id(R.id.text_user).text(item.getJsonData().getString("userName"));

            if ("Y".equals(item.getIsPayment())) {
                if(item.getPayments() == null){
                    //aq.id(R.id.text_payment).text("결제완료");
                    aq.id(R.id.status_image).image(R.mipmap.icon_tablet_main_pay_completed);
                }else{
                    JSONArray payments = new JSONArray(item.getPayments());
                    if (payments.length() > 0) {
                        String paymentType = payments.getJSONObject(0).getString("paymentType");
                        if ("P91".equals(paymentType)) {
                            //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_card);
                            if("Y".equals(item.getIsReceivable())){
                                //aq.id(R.id.text_payment).text("완료지연(미수금)");
                                aq.id(R.id.status_image).image(R.mipmap.icon_tablet_main_pay_receivable);
                            }else{
                                //aq.id(R.id.text_payment).text("카드단말기 요청");
                                aq.id(R.id.status_image).image(R.mipmap.icon_tablet_main_pay_card_req);
                            }
                        } else if ("P92".equals(paymentType)) {
                            if("Y".equals(item.getIsReceivable())){
                                //aq.id(R.id.text_payment).text("완료지연(미수금)");
                                aq.id(R.id.status_image).image(R.mipmap.icon_tablet_main_pay_receivable);
                            }else{
                                //aq.id(R.id.text_payment).text("현금결제 선택");
                                aq.id(R.id.status_image).image(R.mipmap.icon_tablet_main_pay_cash_req);
                            }
                            //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_face_cash);
                        } else {
                            //aq.id(R.id.text_payment).text("결제완료");
                            aq.id(R.id.status_image).image(R.mipmap.icon_tablet_main_pay_completed);
                            //aq.id(R.id.paid_image).image(R.mipmap.paidinfo_already);
                        }
                    } else {
                        //aq.id(R.id.text_payment).text("결제완료");
                        aq.id(R.id.status_image).image(R.mipmap.icon_tablet_main_pay_completed);
                    }
                }

            } else if ("N".equals(item.getIsPayment())) {
                if("Y".equals(item.getIsReceivable())){
                    //aq.id(R.id.text_payment).text("완료지연(미수금)");
                    aq.id(R.id.status_image).image(R.mipmap.icon_tablet_main_pay_receivable);
                }else{
                    //aq.id(R.id.text_payment).text("결제방식 선택 전");
                    aq.id(R.id.status_image).image(R.mipmap.icon_tablet_main_pay_not_selected);
                }
            } else{
                //aq.id(R.id.text_payment).text("");
                aq.id(R.id.status_image).gone();
            }

            if("101".equals(orderSubType)){
                aq.id(R.id.type_icon_image).image(R.mipmap.icon_main_ordertype_01);
                aq.id(R.id.text_desc).textColor(0xFF787878).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("201".equals(orderSubType)){
                //aq.id(R.id.type_icon_image).image(R.mipmap.icon_main_ordertype_01);
                orderTypeString = "택배접수";
                aq.id(R.id.text_desc).textColor(0xFF787878).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("105".equals(orderSubType)){
                aq.id(R.id.type_icon_image).image(R.mipmap.icon_main_ordertype_02);
                orderTypeString = "빠른세탁 - 당일";
                aq.id(R.id.text_desc).textColor(0xFFfc4444).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("104".equals(orderSubType)){
                aq.id(R.id.type_icon_image).image(R.mipmap.icon_main_ordertype_03);
                orderTypeString = "빠른세탁 - 익일";
                aq.id(R.id.text_desc).textColor(0xFFff845e).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("301".equals(orderSubType)){
                aq.id(R.id.type_icon_image).image(R.mipmap.icon_main_ordertype_04);
                orderTypeString = "침구 제휴세탁";
                aq.id(R.id.text_desc).textColor(0xFFa957e8).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
                aq.id(R.id.text_nickname).text(item.getJsonData().getString("label")).typeface(CommonUtility.getNanumBarunTypeface());
            }


            //Log.e("item.getJsonData()", item.getJsonData().toString());

            distance = (item.getJsonData().getDouble("distance"))*1000;//locationStore.distanceTo(locationCustomer);
            meter = String.format("%.1f" , distance);

            //aq.id(R.id.text_distance).text(meter + " m");

            /*
            String storeOrderNo = item.getJsonData().getString("storeOrderNo");
            if(item.getJsonData().isNull("storeOrderNo") || StringUtil.isNullOrEmpty(storeOrderNo) || storeOrderNo == null){
                aq.id(R.id.text_inner_code).text("").gone();
            }else{
                aq.id(R.id.text_inner_code).text(storeOrderNo);
            }*/


            String addressName = item.getJsonData().getString("address1");
            String addressDetailName = item.getJsonData().getString("address2");
            String newAddressName = addressName + " " + addressDetailName;
            if (!StringUtil.isNullOrEmpty(newAddressName)) {
                aq.id(R.id.text_address).text(newAddressName);
            }

            deliveryInfo = item.getJsonData().getString("storeDeliveryTime");
            deliveryDuration = deliveryInfo.split("\\|")[0];
            saleStartTime = Integer.parseInt(deliveryInfo.split("\\|")[1]);
            deliveryTime = deliveryInfo.split("\\|")[2];
            //Log.i("deliveryTime : ", deliveryTime);



            if ("01".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("수거방문예정");
                aq.id(R.id.text_status).text("수거");
                aq.id(R.id.text_date).text("" + TimeUtil.getDateByMDSH(Long.parseLong(item.getPickupRequestTimeApp()), deliveryDuration));
                //aq.id(R.id.text_element).text("수거후 표시예정");
                aq.id(R.id.status_image).gone();
            } else if ("02".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("수거방문예정");
                aq.id(R.id.text_status).text("수거");
                aq.id(R.id.text_date).text("" + TimeUtil.getDateByMDSH(Long.parseLong(item.getPickupRequestTimeApp()), deliveryDuration));
                //aq.id(R.id.text_element).text("수거후 표시예정");
                aq.id(R.id.status_image).gone();
            } else if ("03".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("세탁물 입고중");
                aq.id(R.id.text_status).text("수거완료");
                aq.id(R.id.text_date).text("" + TimeUtil.getDateByMDSH(Long.parseLong(item.getPickupRequestTimeApp()), deliveryDuration));
                //aq.id(R.id.text_element).text("수거수량 : " + orderCount + "개 (상세등록필요)");
                aq.id(R.id.status_image).image(R.mipmap.icon_tablet_main_input_item);
            } else if ("21".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("배송준비중");
                aq.id(R.id.text_status).text("배송");
                aq.id(R.id.text_date).text("" + TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), deliveryDuration));
                //aq.id(R.id.text_element).text(orderPickupItemMessage);
            } else if ("22".equals(item.getOrderStatus())) {
                aq.id(R.id.text_status).text("배송");
                aq.id(R.id.text_date).text("" + TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), deliveryDuration));
            }

        }catch(JSONException e){

        }

        //aq.id(R.id.text_store).text(storeName);
        aq.id(R.id.text_order_id).text(orderId).typeface(CommonUtility.getNanumBarunTypeface());

        aq.id(R.id.content).clicked(this, "listItemSelected").tag(position);

        aq.id(R.id.btn_go_target).clicked(this, "goTargetClicked").tag(position);

        try {
            if(data.get(position).getJsonData().getString("orderStatus").equals("01")){
                aq.id(R.id.btn_go_target).image(R.mipmap.btn_go_target).enabled(true);
            }else if(data.get(position).getJsonData().getString("orderStatus").equals("21")){
                aq.id(R.id.btn_go_target).image(R.mipmap.btn_go_target).enabled(true);
            }else{
                aq.id(R.id.btn_go_target).image(R.mipmap.btn_go_target_disabled).enabled(false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public void goTargetClicked(View button){
        int position = (int) button.getTag();

        //((TabletMainActivity) ctx).goTargetClicked(position, data.get(position).getJsonData());
    }

    public void listItemSelected(View button) {
        int position = (int) button.getTag();

        ((TabletMainV2Activity) ctx).itemSelected(position, data.get(position).getJsonData());
    }
}
