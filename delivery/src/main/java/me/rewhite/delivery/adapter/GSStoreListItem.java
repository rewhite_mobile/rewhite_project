package me.rewhite.delivery.adapter;

/**
 * Created by marines on 2015. 10. 12..
 */
public class GSStoreListItem {

    public String getPartnerStoreId() {
        return partnerStoreId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public int getPickCount() {
        return pickCount;
    }
    public int getDeliveryCount() {
        return deliveryCount;
    }
    public int getStoredOrderCount() {
        return storedOrderCount;
    }
    public int getStoredBoxCount() {
        return storedBoxCount;
    }
    public String getPartnerStoreName() {
        return partnerStoreName;
    }
    public String getIsNewPOS() {
        return isNewPOS;
    }


    private String partnerStoreId;
    private String partnerId;
    private String partnerStoreName;
    private String isNewPOS;
    private int pickCount;
    private int deliveryCount;
    private int storedOrderCount;
    private int storedBoxCount;

    private String partnerStoreAddress;

    public String getPartnerStoreAddress() {
        return partnerStoreAddress;
    }

    public int getPartnerStoreVisitHour() {
        return partnerStoreVisitHour;
    }

    public double getPartnerStoreLongitude() {
        return partnerStoreLongitude;
    }

    public double getPartnerStoreLatitude() {
        return partnerStoreLatitude;
    }

    private int partnerStoreVisitHour;
    private double partnerStoreLongitude;
    private double partnerStoreLatitude;


    public GSStoreListItem(String partnerStoreId, String partnerId, String storeName, int pickCount, int deliveryCount,
                           String partnerStoreAddress, int partnerStoreVisitHour, double partnerStoreLongitude, double partnerStoreLatitude,
                           int storedOrderCount, int storedBoxCount, String isNewPOS) {
        this.partnerId = partnerId;
        this.partnerStoreId = partnerStoreId;
        this.partnerStoreName = storeName;
        this.pickCount = pickCount;
        this.deliveryCount = deliveryCount;
        this.storedOrderCount = storedOrderCount;
        this.storedBoxCount = storedBoxCount;
        this.partnerStoreAddress = partnerStoreAddress;
        this.partnerStoreVisitHour = partnerStoreVisitHour;
        this.partnerStoreLongitude = partnerStoreLongitude;
        this.partnerStoreLatitude = partnerStoreLatitude;
        this.isNewPOS = isNewPOS;

    }
}
