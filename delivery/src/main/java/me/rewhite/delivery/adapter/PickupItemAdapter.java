package me.rewhite.delivery.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.PriceActivity;
import me.rewhite.delivery.util.MZValidator;

/**
 * Created by marines on 2015. 10. 16..
 */
public class PickupItemAdapter extends RecyclerView.Adapter<PickupItemAdapter.PriceViewHolder> {

    private JSONArray mDataset;
    private AQuery aq;
    private Context ctx;
    private int section;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class PriceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public View mView;
        protected int position;
        protected IViewHolderClick mListener;
        protected ImageButton btn_add;
        protected ImageButton btn_remove;
        protected TextView text_count;
        protected TextView text_title;
        protected TextView text_price;
        protected int count;
        protected JSONObject data;

        public PriceViewHolder(View v, IViewHolderClick listener) {
            super(v);
            mListener = listener;
            btn_add = (ImageButton) v.findViewById(R.id.btn_add);
            btn_remove = (ImageButton) v.findViewById(R.id.btn_remove);
            text_count = (TextView) v.findViewById(R.id.text_count);
            text_title = (TextView) v.findViewById(R.id.text_title);
            text_price = (TextView) v.findViewById(R.id.text_price);
            btn_add.setOnClickListener(this);
            btn_remove.setOnClickListener(this);

            mView = v;
        }

        public void initFromData(JSONObject data, int position) {
            this.position = position;
            this.data = data;
            try {
                text_title.setText(data.getString("itemTitle"));
                String price = MZValidator.toNumFormat(Integer.parseInt(data.getString("itemPrice")));
                text_price.setText(price+" 원");
                //aq.id(R.id.text_title).text(mDataset.getJSONObject(position).getString("itemTitle"));
                if (!data.isNull("itemQuantity")) {
                    text_count.setText(data.getInt("itemQuantity") + " 개");
                    Log.e("viewHolder initData " + position, data.getInt("itemQuantity") + " 개");
                }else{
                    text_count.setText("0 개");
                }

                //aq.id(R.id.btn_add).clicked(this, "addAction").tag(position);
                //aq.id(R.id.btn_remove).clicked(this, "removeAction").tag(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onClick(View v) {
            if (v == btn_add) {
                count++;
                text_count.setText(count + " 개");
                mListener.onAddChanged((ImageButton) v, getPosition(), this);
            } else {
                count--;
                if (count < 0) {
                    count = 0;
                }
                text_count.setText(count + " 개");
                mListener.onRemovedChanged((ImageButton) v, getPosition(), this);
            }

        }

        public interface IViewHolderClick {
            void onAddChanged(ImageButton button, int position, PriceViewHolder viewHolder);

            void onRemovedChanged(ImageButton button, int position, PriceViewHolder viewHolder);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PickupItemAdapter(Context ctx, int section, JSONArray myDataset) {
        try {
            JSONArray recvData = new JSONArray();
            //JSONArray additionData = new JSONArray();
            for(int i = 0; i < myDataset.length(); i++){
                if("N".equals(myDataset.getJSONObject(i).getString("isOption"))){
                    recvData.put(myDataset.getJSONObject(i));
                }else{
                    //additionData.put(myDataset.getJSONObject(i));
                    //((PriceActivity) ctx).addAdditionalAction(section, myDataset.getJSONObject(i));
                }
            }
            mDataset = recvData;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.section = section;
        this.ctx = ctx;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PickupItemAdapter.PriceViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pickup_item_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        PriceViewHolder vh = new PriceViewHolder(v, new PickupItemAdapter.PriceViewHolder.IViewHolderClick() {
            @Override
            public void onAddChanged(ImageButton button, int position, PriceViewHolder viewHolder) {
                try {
                    ((PriceActivity) ctx).addRemoveAction(section, position, true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRemovedChanged(ImageButton button, int position, PriceViewHolder viewHolder) {
                try {
                    ((PriceActivity) ctx).addRemoveAction(section, position, false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        aq = new AQuery(v);

        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(PriceViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        //holder.mView.setText(mDataset[position]);

        try {
            holder.initFromData(mDataset.getJSONObject(position), position);
            Log.e("viewHolder initData " + position, mDataset.getJSONObject(position).getInt("itemQuantity") + " 개");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length();
    }


}
