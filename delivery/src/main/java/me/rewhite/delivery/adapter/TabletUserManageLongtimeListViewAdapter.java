package me.rewhite.delivery.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.rewhite.delivery.R;
import me.rewhite.delivery.tablet.UserSearchActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.MZValidator;


/**
 * Created by marines on 2015. 10. 5..
 */
public class TabletUserManageLongtimeListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<TabletUserListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;

    /*
    private String orderId;
    private String orderStatus;
    private String addressSeq;
    private String storeId;
    private String deliveryPrice;
    private String pickupRequestTimeApp;
    private String deliveryRequestTimeApp;
    private String registerDateApp;
    private String orderRequest;
     */

    public TabletUserManageLongtimeListViewAdapter(Context context, int layout, ArrayList<TabletUserListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public TabletUserListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.index_id).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_price).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_phone).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_username).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_address).getTextView());

        TabletUserListItem item = data.get(position);
        String userName = item.getUserName();
        try {
            JSONObject userData = new JSONObject(item.getJsonData());
            aq.id(R.id.index_id).text(userData.getString("RowNumber")).typeface(CommonUtility.getNanumBarunTypeface());;
            aq.id(R.id.text_price).text(userData.getString("receivableCount") + " 일").typeface(CommonUtility.getNanumBarunTypeface());;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        aq.id(R.id.text_phone).text(MZValidator.validTelNumber(item.getPhone())).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.text_username).text(userName).typeface(CommonUtility.getNanumBarunTypeface());;
        aq.id(R.id.text_address).text(item.getAddress1() + " " + item.getAddress2()).typeface(CommonUtility.getNanumBarunTypeface());;

        aq.id(R.id.content).clicked(this, "listItemSelected").tag(position);

        return convertView;
    }

    public void listItemSelected(View button) {
        int position = (int) button.getTag();
        if (ctx instanceof UserSearchActivity) {
            try {
                ((UserSearchActivity) ctx).itemSelected(position, new JSONObject(data.get(position).getJsonData()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
}
