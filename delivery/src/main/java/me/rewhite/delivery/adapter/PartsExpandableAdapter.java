package me.rewhite.delivery.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.tablet.TabletPriceSettingActivity;
import me.rewhite.delivery.util.CommonUtility;

/**
 * Created by marines on 16. 6. 30..
 */
public class PartsExpandableAdapter extends BaseAdapter {

    TabletPriceSettingActivity mActivity;
    Context mContext;
    JSONArray priceData;
    AQuery aq;

    public PartsExpandableAdapter(Context context, JSONArray _priceData ) {
        mContext = context;
        mActivity = (TabletPriceSettingActivity)context;
        priceData = _priceData;
    }

    @Override
    public int getCount() {
        return priceData.length();
    }

    @Override
    public JSONObject getItem(int position) {
        try {
            return priceData.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        try {
            return priceData.getJSONObject(position).getLong("partId");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if(convertView == null) {
            view = getChildGenericView();
        } else {
            view = convertView;
        }

        aq = new AQuery(view);
        aq.id(R.id.btn_use).clicked(this, "useClickAction").tag(position);
        aq.id(R.id.price_01).gone();
        aq.id(R.id.price_02).gone();
        aq.id(R.id.price_03).gone();
        aq.id(R.id.price_04).gone();

        try {
            TextView text = (TextView)view.findViewById(R.id.item_title);

            CommonUtility.setTypefaceNormalSetup(text);

            if("N".equals(priceData.getJSONObject(position).getString("isUse"))){
                aq.id(R.id.btn_use).image(R.mipmap.pui_item_color_uncheck);
                aq.id(R.id.content).backgroundColor(Color.parseColor("#e5e5e5"));
            }else{
                aq.id(R.id.btn_use).image(R.mipmap.pui_item_color_check);
                aq.id(R.id.content).backgroundColor(Color.parseColor("#ffffff"));
            }

            text.setText(priceData.getJSONObject(position).getString("partTitle"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view;
    }

    public void useClickAction(View button){
        int tagValue = (int)button.getTag();
        int position = tagValue;

        Bundle keyBundle = new Bundle();
        keyBundle.putInt("position", position);
        try {
            keyBundle.putString("data", priceData.getJSONObject(position).toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mActivity.usePartModProcess(keyBundle);
    }

    //Child의 View의 XML을 생성
    public View getChildGenericView() {
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.price_expandable_list_child_item, null);
        return view;
    }

}
