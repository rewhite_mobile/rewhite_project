package me.rewhite.delivery.adapter;

/**
 * Created by marines on 2015. 10. 12..
 */
public class NoticeListItem {

    private String noticeId;
    private String noticeType;
    private String title;

    private long registerDateApp;
    private String RowNumber;

    public String getNoticeId() {
        return noticeId;
    }

    public String getNoticeType() {
        return noticeType;
    }

    public String getTitle() {
        return title;
    }

    public long getRegisterDateApp() {
        return registerDateApp;
    }

    public String getRowNumber() {
        return RowNumber;
    }

    public String getNoticeTypeTitle() {
        return noticeTypeTitle;
    }

    public String getNoticeUrl() {
        return noticeUrl;
    }

    private String noticeTypeTitle;
    private String noticeUrl;


    public NoticeListItem(String noticeId, String noticeType, String title, long registerDateApp, String RowNumber, String noticeTypeTitle, String noticeUrl) {
        this.noticeId = noticeId;
        this.noticeType = noticeType;
        this.title = title;
        this.registerDateApp = registerDateApp;
        this.RowNumber = RowNumber;
        this.noticeTypeTitle = noticeTypeTitle;
        this.noticeUrl = noticeUrl;
    }
}
