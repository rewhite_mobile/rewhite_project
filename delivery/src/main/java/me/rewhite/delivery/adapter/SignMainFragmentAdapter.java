package me.rewhite.delivery.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import me.rewhite.delivery.activity.SignActivity;
import me.rewhite.delivery.fragment.SignMainBottomFragment;
import me.rewhite.delivery.fragment.SignMainFragment;


public class SignMainFragmentAdapter extends FragmentPagerAdapter {

	protected static final String[] CONTENT = new String[]{"SignMain", "Login"};
	private List<Fragment> fragments;
	private SignActivity mSignActivity;

	public SignMainFragmentAdapter(SignActivity mSignActivity) {
		super(mSignActivity.getSupportFragmentManager());
		this.fragments = getFragments();
		this.mSignActivity = mSignActivity;
	}

	private List<Fragment> getFragments() {

		List<Fragment> fList = new ArrayList<Fragment>();
		fList.add(SignMainFragment.newInstance(CONTENT[0]));
		fList.add(SignMainBottomFragment.newInstance(CONTENT[1]));

		return fList;
	}

	@Override
	public Fragment getItem(int position) {
		return this.fragments.get(position);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return this.fragments.size();
	}

}
