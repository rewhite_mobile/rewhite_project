package me.rewhite.delivery.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.MainActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.TimeUtil;


/**
 * Created by marines on 2015. 10. 5..
 */
public class OrderListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<OrderListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;

    /*
    private String orderId;
    private String orderStatus;
    private String addressSeq;
    private String storeId;
    private String deliveryPrice;
    private String pickupRequestTimeApp;
    private String deliveryRequestTimeApp;
    private String registerDateApp;
    private String orderRequest;
     */

    public OrderListViewAdapter(Context context, int layout, ArrayList<OrderListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public OrderListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        OrderListItem item = data.get(position);
        String storeName = item.getStoreName();
        String orderPickupItemMessage = item.getOrderPickupItemMessage();
        String orderRequest = item.getOrderRequest();
        String orderId = item.getOrderId();
        int orderCount = item.getPickupQuantity();
        JSONArray payments = null;
        try {
            if(item.getPayments() != null){
                payments = new JSONArray(item.getPayments());
            }else{
                payments = new JSONArray();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String price = item.getDeliveryPrice();
        int priceValue = Integer.parseInt(price);
        String outputPrice = price;
        if(priceValue < 20000){
            priceValue = priceValue + 2000;
        }
        outputPrice = MZValidator.toNumFormat(priceValue);

        aq.id(R.id.text_date).text(TimeUtil.convertTimestampToString(Long.parseLong(item.getStatusTimeApp())));

        aq.id(R.id.text_price).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.text_element).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.text_store).text(storeName);



        try{
            String orderSubType = item.getJsonData().getString("orderSubType");
            Log.e("orderSubType", orderSubType);
            // orderSubType 11번가 : "106"
            aq.id(R.id.order_refer).gone();
            aq.id(R.id.icon_paytype).gone();
            aq.id(R.id.order_type_text).visible();

            String orderTypeString = "일반세탁";
            if("101".equals(orderSubType)){
                aq.id(R.id.order_type_text).textColor(0xFF999999).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("201".equals(orderSubType)){
                orderTypeString = "택배접수";
                aq.id(R.id.order_type_text).textColor(0xFF787878).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("105".equals(orderSubType)){
                orderTypeString = "빠른세탁 - 당일";
                aq.id(R.id.order_type_text).textColor(0xFFfd7070).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("104".equals(orderSubType)){
                orderTypeString = "빠른세탁 - 익일";
                aq.id(R.id.order_type_text).textColor(0xFFff845e).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("301".equals(orderSubType)){
                orderTypeString = "침구제휴세탁";
                aq.id(R.id.order_type_text).textColor(0xFFa957e8).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
                aq.id(R.id.text_store).text(item.getJsonData().getString("label")).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("106".equals(orderSubType)){
                aq.id(R.id.order_refer).visible();
                aq.id(R.id.order_type_text).gone();
            }else if("107".equals(orderSubType)){
                orderTypeString = "전화주문";
                aq.id(R.id.order_refer).gone();
                aq.id(R.id.order_type_text).textColor(0xFFf25822).text(orderTypeString).visible();
            }else{
                aq.id(R.id.order_refer).gone();
                aq.id(R.id.icon_paytype).gone();
                aq.id(R.id.order_type_text).gone();
            }

            if(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP) == null){
                SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.DELIVERY_GAP, "1");
            }

            if ("01".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("수거방문예정");
                aq.id(R.id.order_status_text).text("수거접수").textColor(0xFF5eaeff);

                aq.id(R.id.text_price).text("세탁요금 미정").gone();
                //item.setJsonData().getString();
                aq.id(R.id.text_element).text("").gone();
                // DELIVERY_GAP 없는경우 처리 필요
                aq.id(R.id.text_pick_date).text(""+TimeUtil.getDateByMDSH(Long.parseLong(item.getPickupRequestTimeApp()), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP)));
            } else if ("02".equals(item.getOrderStatus())) {
                aq.id(R.id.order_status_text).text("수거방문예정").textColor(0xFF0060d8);

                aq.id(R.id.text_price).text("세탁요금 미정").gone();
                //aq.id(R.id.text_element).text("수거후 표시예정");
                aq.id(R.id.text_pick_date).text(""+TimeUtil.getDateByMDSH(Long.parseLong(item.getPickupRequestTimeApp()), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP)));
                aq.id(R.id.text_element).text("").gone();
            } else if ("03".equals(item.getOrderStatus())) {
                aq.id(R.id.order_status_text).text("품목등록");

                aq.id(R.id.text_price).text("품목을 등록해주세요.");
                //aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(R.id.text_pick_date).text("수거수량 : " + orderCount + "개");
                aq.id(R.id.text_element).text("수거수량 : " + orderCount + "개").gone();
            } else if ("11".equals(item.getOrderStatus())) {
                aq.id(R.id.order_status_text).text("세탁 준비중").textColor(0xFF5eaeff);

                aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(R.id.text_pick_date).text(orderPickupItemMessage);
                aq.id(R.id.text_element).text(orderPickupItemMessage).gone();
                isPaymentStatus(item, convertView);
            } else if ("12".equals(item.getOrderStatus())) {
                aq.id(R.id.order_status_text).text("세탁중").textColor(0xFF5eaeff);

                aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(R.id.text_pick_date).text(orderPickupItemMessage);
                aq.id(R.id.text_element).text(orderPickupItemMessage).gone();
                isPaymentStatus(item, convertView);
            } else if ("13".equals(item.getOrderStatus())) {
                aq.id(R.id.order_status_text).text("세탁완료").textColor(0xFF0060d8);;

                aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(R.id.text_pick_date).text(orderPickupItemMessage);
                aq.id(R.id.text_element).text(orderPickupItemMessage).gone();
                isPaymentStatus(item, convertView);
            } else if ("21".equals(item.getOrderStatus())) {
                aq.id(R.id.order_status_text).text("배송준비중").textColor(0xFF5eaeff);
                isPaymentStatus(item, convertView);

                aq.id(R.id.text_price).text(outputPrice + " 원");
                //aq.id(R.id.text_element).text(orderPickupItemMessage);
                aq.id(R.id.text_pick_date).text(TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP)));
                aq.id(R.id.text_element).text(orderPickupItemMessage);


            } else if ("22".equals(item.getOrderStatus())) {
                aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(R.id.text_pick_date).text(TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.DELIVERY_GAP)));
                aq.id(R.id.text_element).text(orderPickupItemMessage);

                if("Y".equals(item.getIsReceivable())){
                    // 미수금 있는 경우
                    aq.id(R.id.icon_paytype).visible();
                    aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_unpaid);
                    aq.id(R.id.order_status_text).text("배송완료 지연").textColor(0xFF0060d8);
                }else{
                    aq.id(R.id.order_status_text).text("배송중").textColor(0xFF0060d8);

                    isPaymentStatus(item, convertView);
                }
            } else if ("23".equals(item.getOrderStatus())) {
                aq.id(R.id.order_status_text).text("배송완료").textColor(0xFF0060d8);

                aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(R.id.text_pick_date).text(orderPickupItemMessage);//.gone();
                aq.id(R.id.text_element).text(orderPickupItemMessage).gone();
            } else if ("24".equals(item.getOrderStatus())) {
                aq.id(R.id.order_status_text).text("방문수령완료").textColor(0xFF0060d8);

                aq.id(R.id.text_price).text(outputPrice + " 원");
                aq.id(R.id.text_pick_date).text(orderPickupItemMessage);//.gone();
                aq.id(R.id.text_element).text(orderPickupItemMessage).gone();
            } else if ("91".equals(item.getOrderStatus())) {
                aq.id(R.id.order_status_text).text("취소");
                //aq.id(R.id.back).image(R.mipmap.back_order_history_91);
                aq.id(R.id.price_area).gone();
                aq.id(R.id.text_pick_date).text("").gone();
                aq.id(R.id.text_element).text(orderPickupItemMessage);
            } else {
                aq.id(R.id.order_status_text).text("기타");
                //aq.id(R.id.back).image(R.mipmap.back_order_history_99);
                aq.id(R.id.price_area).gone();
                aq.id(R.id.text_pick_date).text("").gone();
                aq.id(R.id.text_element).text(orderPickupItemMessage);
            }

        }catch(JSONException e){
            e.printStackTrace();
        }

        aq.id(R.id.text_orderid).text("" + orderId);

        aq.id(R.id.content).clicked(this, "listItemSelected").tag(position);

        return convertView;
    }

    private void isPaymentStatus(OrderListItem item, View mView) throws JSONException {

        AQuery bq = new AQuery(mView);
        JSONArray payments = null;
        Log.e("payments value", item.getPayments() + "");
        try {
            if(item.getPayments() != null){
                payments = new JSONArray(item.getPayments());
            }else{
                payments = new JSONArray();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        bq.id(R.id.icon_paytype).visible();
        if(payments != null){
            if("N".equals(item.getIsPayment())){
                if("Y".equals(item.getIsReceivable())){
                    // 미수금
                    bq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_unpaid);
                }else{
                    // 결제방식 미선택
                    bq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_unselect);
                }
            }else{
                // 결제완료
                if(item.getIsAdditionalPayment() == null){
                    if (payments.length() > 0) {
                        String paymentType = payments.getJSONObject(0).getString("paymentType");
                        if ("P91".equals(paymentType)) {
                            if("Y".equals(item.getIsReceivable())){
                                bq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_unpaid);
                            }else{
                                bq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_local_card);
                                Log.e("picon_paytype", "icon_pay_local_card");
                            }
                        } else if ("P92".equals(paymentType)) {
                            if("Y".equals(item.getIsReceivable())){
                                bq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_unpaid);
                            }else{
                                bq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_local_cash);
                            }
                        } else if("P51".equals(paymentType)){
                            /* TODO

                             */
                            bq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_11st_refund);

                        } else {
                            bq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_paid);
                        }
                    }else{
                        bq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_paid);
                    }
                }else if("N".equals(item.getIsAdditionalPayment())){
                    // 추가요금 결제필요

                }else if("Y".equals(item.getIsAdditionalPayment())){
                    bq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_paid);
                }
            }
        }else{
            // 결제방식 미선택
            bq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_unselect);
        }

    }

    public void listItemSelected(View button) {
        int position = (int) button.getTag();
        if (ctx instanceof MainActivity) {
            ((MainActivity) ctx).itemSelected(position, data.get(position).getJsonData());
        }

    }
}
