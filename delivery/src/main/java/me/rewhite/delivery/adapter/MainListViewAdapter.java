package me.rewhite.delivery.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.MainActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.StringUtil;
import me.rewhite.delivery.util.TimeUtil;


/**
 * Created by marines on 2015. 10. 5..
 */
public class MainListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<OrderListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;

    /*
    private String orderId;
    private String orderStatus;
    private String addressSeq;
    private String storeId;
    private String deliveryPrice;
    private String pickupRequestTimeApp;
    private String deliveryRequestTimeApp;
    private String registerDateApp;
    private String orderRequest;
     */

    public MainListViewAdapter(Context context, int layout, ArrayList<OrderListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public OrderListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        OrderListItem item = data.get(position);
        String orderPickupItemMessage = item.getOrderPickupItemMessage();
        String orderId = item.getOrderId();
        //String orderInnerCode = item.getStoreId();
        int orderCount = item.getPickupQuantity();



        double distance;
        String meter;
        String deliveryInfo;
        String deliveryDuration;
        String deliveryTime;
        int saleStartTime = 6;

        try{
            String orderSubType = item.getJsonData().getString("orderSubType");
            String orderTypeString = "일반세탁";

            aq.id(R.id.text_nickname).text(item.getJsonData().getString("userName")).typeface(CommonUtility.getNanumBarunTypeface());

            if("106".equals(orderSubType)){
                // 11번가 주문일때
                JSONArray partner11stPayment;
                if(!item.getJsonData().isNull("payments")){
                    partner11stPayment = item.getJsonData().getJSONArray("payments");
                }else{
                    partner11stPayment = new JSONArray();
                }

                int prepaid = 0;
                for(int i = 0 ; i < partner11stPayment.length(); i++){
                    if("P51".equals(partner11stPayment.getJSONObject(i).getString("paymentType"))){
                        prepaid = partner11stPayment.getJSONObject(i).getInt("paymentPrice");
                        aq.id(R.id.text_11st_price).text(MZValidator.toNumFormat(prepaid) + " 원");
                    }
                }

                int deliveryPrice = Integer.parseInt(item.getDeliveryPrice());
                //Log.e("11st pay", partner11stPayment.toString() + "\nprepaid : " + prepaid + " / deliveryPrice : " + deliveryPrice);

                Log.e("deliveryPrice", deliveryPrice +", "+prepaid);

                if(deliveryPrice - prepaid > 0){
                    // 추가금액 발생
                    Log.e("item.getIsAdditionalPayment()", item.getJsonData().toString() + "");

                    if("Y".equals(item.getJsonData().getString("isAdditionalPayment"))) {
                        aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_paid);
                    }else{
                        // 추가결제 미완료
                        aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_11st_add_unpaid);
                    }
//
//                    if("Y".equals(item.getIsAdditionalPayment())){
//                        // 추가결제 완료
//                        aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_paid);
//                    }else{
//                        // 추가결제 미완료
//                        aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_11st_add_unpaid);
//                    }
                }else if(deliveryPrice - prepaid == 0){
                    // 금액 동일
                    aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_paid);
                }else{
                    // 환불금액 발생
                    aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_11st_refund);
                }
            }else{
                // 일반주문일때
                if("N".equals(item.getIsPayment())){
                    if("Y".equals(item.getIsReceivable())){
                        // 미수금
                        aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_unpaid);
                    }else{
                        // 결제방식 미선택
                        aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_unselect);
                    }
                }else{
                    // 결제완료
                    if(item.getIsAdditionalPayment() == null){
                        if(item.getPayments() == null){
                            aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_paid);
                        }else{
                            JSONArray payments = new JSONArray(item.getPayments());
                            if (payments.length() > 0) {
                                String paymentType = payments.getJSONObject(0).getString("paymentType");
                                if ("P91".equals(paymentType)) {
                                    if("Y".equals(item.getIsReceivable())){
                                        aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_unpaid);
                                    }else{
                                        aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_local_card);
                                    }
                                } else if ("P92".equals(paymentType)) {
                                    if("Y".equals(item.getIsReceivable())){
                                        aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_unpaid);
                                    }else{
                                        aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_local_cash);
                                    }
                                } else {
                                    aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_paid);
                                }
                            }else{
                                aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_paid);
                            }
                        }
                    }else if("N".equals(item.getIsAdditionalPayment())){
                        // 추가요금 결제필요
//                    aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_paid);
                    }else if("Y".equals(item.getIsAdditionalPayment())){
                        aq.id(R.id.icon_paytype).image(R.mipmap.icon_pay_paid);
                    }
                }
            }


            aq.id(R.id.order_refer).gone();

            if("101".equals(orderSubType)){
                aq.id(R.id.type_icon_image).image(R.mipmap.icon_main_ordertype_01);
                aq.id(R.id.text_desc).textColor(0xFF999999).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("201".equals(orderSubType)){
                //aq.id(R.id.type_icon_image).image(R.mipmap.icon_main_ordertype_01);
                orderTypeString = "택배접수";
                aq.id(R.id.text_desc).textColor(0xFFff5151).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("105".equals(orderSubType)){
                aq.id(R.id.type_icon_image).image(R.mipmap.icon_main_ordertype_02);
                orderTypeString = "빠른세탁 - 당일";
                aq.id(R.id.text_desc).textColor(0xFFff5151).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("104".equals(orderSubType)){
                aq.id(R.id.type_icon_image).image(R.mipmap.icon_main_ordertype_03);
                orderTypeString = "빠른세탁 - 익일";
                aq.id(R.id.text_desc).textColor(0xFFff845e).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("301".equals(orderSubType)){
                aq.id(R.id.type_icon_image).image(R.mipmap.icon_main_ordertype_04);
                orderTypeString = "침구 제휴세탁";
                aq.id(R.id.text_desc).textColor(0xFFa957e8).text(orderTypeString).typeface(CommonUtility.getNanumBarunTypeface());
                aq.id(R.id.text_nickname).text(item.getJsonData().getString("label")).typeface(CommonUtility.getNanumBarunTypeface());
            }else if("106".equals(orderSubType)){
                // 11번가 주문
                aq.id(R.id.type_icon_image).image(R.mipmap.icon_main_ordertype_05);
                aq.id(R.id.text_desc).text("");
                aq.id(R.id.btn_go_target).image(R.mipmap.btn_call_before_start);
                aq.id(R.id.order_refer).visible().image(R.mipmap.img_logo_11street);
            }else if("107".equals(orderSubType)){
                // 11번가 주문
                aq.id(R.id.type_icon_image).image(R.mipmap.icon_main_ordertype_01);
                aq.id(R.id.text_desc).text("");
                aq.id(R.id.btn_go_target).image(R.mipmap.btn_call_before_start);
                aq.id(R.id.order_refer).visible().image(R.mipmap.img_logo_phone);
            }


            //Log.e("item.getJsonData()", item.getJsonData().toString());

            distance = (item.getJsonData().getDouble("distance"))*1000;//locationStore.distanceTo(locationCustomer);
            meter = String.format("%.1f" , distance);

            //aq.id(R.id.text_distance).text(meter + " m");

            /*
            String storeOrderNo = item.getJsonData().getString("storeOrderNo");
            if(item.getJsonData().isNull("storeOrderNo") || StringUtil.isNullOrEmpty(storeOrderNo) || storeOrderNo == null){
                aq.id(R.id.text_inner_code).text("").gone();
            }else{
                aq.id(R.id.text_inner_code).text(storeOrderNo);
            }*/


            String addressName = item.getJsonData().getString("address1");
            String addressDetailName = item.getJsonData().getString("address2");
            String newAddressName = addressName + " " + addressDetailName;
            if (!StringUtil.isNullOrEmpty(newAddressName)) {
                aq.id(R.id.text_address).text(newAddressName).typeface(CommonUtility.getNanumBarunTypeface());
            }

            deliveryInfo = item.getJsonData().getString("storeDeliveryTime");
            deliveryDuration = deliveryInfo.split("\\|")[0];
            saleStartTime = Integer.parseInt(deliveryInfo.split("\\|")[1]);
            deliveryTime = deliveryInfo.split("\\|")[2];
            //Log.i("deliveryTime : ", deliveryTime);



            if ("01".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("수거방문예정");
                aq.id(R.id.text_date).text("" + TimeUtil.getDateByMDSH(Long.parseLong(item.getPickupRequestTimeApp()), deliveryDuration)).typeface(CommonUtility.getNanumBarunTypeface());
                //aq.id(R.id.text_element).text("수거후 표시예정");
                aq.id(R.id.icon_paytype).gone();
            } else if ("02".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("수거방문예정");
                aq.id(R.id.text_date).text("" + TimeUtil.getDateByMDSH(Long.parseLong(item.getPickupRequestTimeApp()), deliveryDuration)).typeface(CommonUtility.getNanumBarunTypeface());
                //aq.id(R.id.text_element).text("수거후 표시예정");
                aq.id(R.id.icon_paytype).gone();
            } else if ("03".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("세탁물 입고중");
                aq.id(R.id.text_date).text("" + TimeUtil.getDateByMDSH(Long.parseLong(item.getPickupRequestTimeApp()), deliveryDuration)).typeface(CommonUtility.getNanumBarunTypeface());
                //aq.id(R.id.text_element).text("수거수량 : " + orderCount + "개 (상세등록필요)");
                aq.id(R.id.icon_paytype).gone();
            } else if ("21".equals(item.getOrderStatus())) {
                //aq.id(R.id.text_status).text("배송준비중");
                aq.id(R.id.text_date).text("" + TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), deliveryDuration)).typeface(CommonUtility.getNanumBarunTypeface());
                //aq.id(R.id.text_element).text(orderPickupItemMessage);
                aq.id(R.id.icon_paytype).visible();
            } else if ("22".equals(item.getOrderStatus())) {
                aq.id(R.id.text_date).text("" + TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), deliveryDuration)).typeface(CommonUtility.getNanumBarunTypeface());
                aq.id(R.id.icon_paytype).visible();
            }

        }catch(JSONException e){
            e.printStackTrace();
        }

        //aq.id(R.id.text_store).text(storeName);
        aq.id(R.id.text_order_id).text(orderId).typeface(CommonUtility.getNanumBarunTypeface());

        aq.id(R.id.content).clicked(this, "listItemSelected").tag(position);

        aq.id(R.id.btn_map).clicked(this, "mapClicked").tag(position);
        aq.id(R.id.btn_go_target).clicked(this, "goTargetClicked").tag(position);

        try {
            String orderSubType = item.getJsonData().getString("orderSubType");
            if(data.get(position).getJsonData().getString("orderStatus").equals("01")){
                if("106".equals(orderSubType)){
                    // 11번가
                    aq.id(R.id.btn_go_target).image(R.mipmap.btn_call_before_start).enabled(true);
                }else{
                    // 일반주문, 전화주문
                    aq.id(R.id.btn_go_target).image(R.mipmap.btn_go_target).enabled(true);
                }
            }else if(data.get(position).getJsonData().getString("orderStatus").equals("21")){
                if("106".equals(orderSubType)){
                    // 11번가
                    aq.id(R.id.btn_go_target).image(R.mipmap.btn_call_before_start).enabled(true);
                }else{
                    // 일반주문, 전화주문
                    aq.id(R.id.btn_go_target).image(R.mipmap.btn_go_target).enabled(true);
                }
            }else{
                aq.id(R.id.btn_go_target).image(R.mipmap.btn_go_target_disabled).enabled(false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public void mapClicked(View button){
        int position = (int) button.getTag();

        ((MainActivity) ctx).mapClicked(position, data.get(position).getJsonData());
    }

    public void goTargetClicked(View button){
        int position = (int) button.getTag();

        ((MainActivity) ctx).goTargetClicked(position, data.get(position).getJsonData());
    }

    public void listItemSelected(View button) {
        int position = (int) button.getTag();

        ((MainActivity) ctx).itemSelected(position, data.get(position).getJsonData());
    }
}
