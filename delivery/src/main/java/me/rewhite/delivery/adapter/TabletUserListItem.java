package me.rewhite.delivery.adapter;

/**
 * Created by marines on 2015. 10. 12..
 */
public class TabletUserListItem {


    private String userName;

    public String getUserName() {
        return userName;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getPhone() {
        return phone;
    }

    private String address1;
    private String address2;
    private String phone;
    private String imageThumbPath;

    public String getJsonData() {
        return jsonData;
    }

    private String jsonData;

    public String getImageThumbPath(){
        return imageThumbPath;
    }

    //userName, address1, address2, phone, imageThumbPath
    public TabletUserListItem(String userName, String address1,
                              String address2, String phone, String imageThumbPath, String jsonData) {

        this.userName = userName;
        this.address1 = address1;
        this.address2 = address2;
        this.phone = phone;
        this.imageThumbPath = imageThumbPath;
        this.jsonData = jsonData;

    }
}
