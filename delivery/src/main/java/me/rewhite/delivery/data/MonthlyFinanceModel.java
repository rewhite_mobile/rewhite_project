package me.rewhite.delivery.data;

public class MonthlyFinanceModel {
    private static int nextId = 0;
    String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getOnlinePrice() {
        return onlinePrice;
    }

    public void setOnlinePrice(int onlinePrice) {
        this.onlinePrice = onlinePrice;
    }

    public int getOfflinePrice() {
        return offlinePrice;
    }

    public void setOfflinePrice(int offlinePrice) {
        this.offlinePrice = offlinePrice;
    }

    public String getLinkurl() {
        return linkurl;
    }

    public void setLinkurl(String linkurl) {
        this.linkurl = linkurl;
    }

    int totalPrice = 0;
    int onlinePrice = 0;
    int offlinePrice = 0;
    String linkurl;
    int id = ++nextId;
}
