package me.rewhite.delivery.data;

/**
 * Created by kimhyunwoo on 15. 8. 10..
 */
public class PickupType {
    public static int PICKUPTYPE_CLEAN = 1;
    public static int PICKUPTYPE_REPAIR = 2;
    public static int PICKUPTYPE_TODAY = 3;
    public static int PICKUPTYPE_TOMORROW = 4;
    public static int PICKUPTYPE_B2B_HANDYS = 5;
    public static int PICKUPTYPE_GS = 6;
}
