package me.rewhite.delivery.view;

/**
 * Created by marines on 15. 7. 30..
 */
public class HistoryItem {

    // 내역조회 PK
    private String orderId;

    private String nickName;
    private String profilePath;
    private String address;
    private int paidPrice;
    private String status;
    private String productTitle;
    private long timestamp;

    public HistoryItem(){

    }

    public HistoryItem(String orderId, String nickName, String profilePath, String address, int paidPrice, String status, String productTitle, long timestamp) {
        this.orderId = orderId;
        this.nickName = nickName;
        this.profilePath = profilePath;
        this.address = address;
        this.paidPrice = paidPrice;
        this.status = status;
        this.productTitle = productTitle;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "orderId:" + orderId + ",profilePath:" + profilePath + ",address:" + address + ",paidPrice:" + paidPrice + ",status:" + status + ",productTitle:" + productTitle;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPaidPrice() {
        return paidPrice;
    }

    public void setPaidPrice(int paidPrice) {
        this.paidPrice = paidPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }


}
