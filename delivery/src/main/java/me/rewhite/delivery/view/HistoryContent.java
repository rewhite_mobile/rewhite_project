package me.rewhite.delivery.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by marines on 15. 7. 30..
 */
public class HistoryContent {

    /**
     * An array of sample (dummy) items.
     */
    public static List<HistoryItem> ITEMS = new ArrayList<HistoryItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, HistoryItem> ITEM_MAP = new HashMap<String, HistoryItem>();

    static {
        // Add 3 sample items.
        //addItem(new HistoryItem("1", "Marines", "http://cfile217.uf.daum.net/image/161E3C594DF0242B16D503", "서울시 영등포구 신길동 신길자이APT 103동 1101호", 23000, "statuscode", "와이셔츠 3개", 5));

    }

    private static void addItem(HistoryItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.getOrderId(), item);
    }

}
