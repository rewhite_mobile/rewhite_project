package me.rewhite.delivery.view;

/**
 * Created by marines on 15. 7. 30..
 */
public class OrderItem {


    // 내역조회 PK
    private String productId;
    private String productImagePath;
    private String productName;
    private int price;
    private int count;

    private int timestamp;

    public OrderItem() {

    }

    public OrderItem(String productId, String productName, String productImagePath, int price, int count, int timestamp) {
        this.productId = productId;
        this.productName = productName;
        this.productImagePath = productImagePath;
        this.price = price;
        this.count = count;
        this.timestamp = timestamp;

    }

    @Override
    public String toString() {
        return "productId:" + productId + ",productName:" + productName + ",productImagePath:" + productImagePath + ",price:" + price + ",count:" + count + ",timestamp:" + timestamp;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductImagePath() {
        return productImagePath;
    }

    public void setProductImagePath(String productImagePath) {
        this.productImagePath = productImagePath;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }
}
