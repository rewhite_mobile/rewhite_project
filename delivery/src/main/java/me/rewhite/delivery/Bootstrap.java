package me.rewhite.delivery;

import android.app.Activity;
import android.content.Context;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

import io.fabric.sdk.android.Fabric;
import me.rewhite.delivery.common.util.Logger;
import me.rewhite.delivery.util.SharedPreferencesUtility;

public class Bootstrap extends MultiDexApplication {
    private static final String TAG = "Bootstrap";

    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    private static volatile Bootstrap instance = null;
    private static volatile Activity currentActivity = null;
    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    /**
     * singleton 애플리케이션 객체를 얻는다.
     *
     * @return singleton 애플리케이션 객체
     */
    public static Bootstrap getGlobalApplicationContext() {
        if (instance == null)
            throw new IllegalStateException("this application does not inherit me.rewhite.delivery");
        return instance;
    }

    public static Activity getCurrentActivity() {
        Log.d("Bootstrap", "++ currentActivity : " + (currentActivity != null ? currentActivity.getClass().getSimpleName() : ""));
        return currentActivity;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        Bootstrap.currentActivity = currentActivity;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            analytics = GoogleAnalytics.getInstance(this);
            analytics.setLocalDispatchPeriod(1800);

            tracker = analytics.newTracker("UA-65272889-3");
            tracker.enableExceptionReporting(true);
            tracker.enableAdvertisingIdCollection(true);
            tracker.enableAutoActivityTracking(true);
            mTrackers.put(trackerId, tracker);
        }
        return mTrackers.get(trackerId);
    }

    /**
     * 이미지 로더, 이미지 캐시, 요청 큐를 초기화한다.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        //로그 활성화

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        instance = this;
        SharedPreferencesUtility.initialize(this);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();

        // Initialize Fabric with the debug-disabled crashlytics.
        Fabric.with(this, crashlyticsKit);

        //Fabric.with(this, new Crashlytics());


//		Typekit.getInstance()
//				.addNormal(Typekit.createFromAsset(this, TYPEFACE_NANUMBARUNGOTHIC_PATH))
//				.addBold(Typekit.createFromAsset(this, TYPEFACE_NANUMBARUNGOTHIC_BOLD_PATH))
//				.addCustom1(Typekit.createFromAsset(this, TYPEFACE_NANUMBARUNGOTHIC_ULTRA_LIGHT_PATH))
//				.addCustom2(Typekit.createFromAsset(this, TYPEFACE_NANUMBARUNGOTHIC_LIGHT_PATH));


//		Mint.setApplicationEnvironment(Mint.appEnvironmentRelease);
//		Mint.initAndStartSession(instance, "6cc18f06");
//		Mint.enableLogging(true);
//		Mint.setUserIdentifier(AccessToken.createFromCache().getUserKey());
//		Mint.setLogging(10);
//		Mint.startANRMonitoring(5000, true);
    }

    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }

    /**
     * 애플리케이션 종료시 singleton 어플리케이션 객체 초기화한다.
     */
    @Override
    public void onTerminate() {
        super.onTerminate();
        instance = null;
    }


    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     * <p>
     * A single tracker is usually enough for most purposes. In case you do need
     * multiple trackers, storing them all in Application object helps ensure
     * that they are created only once per application instance.
     */
    public enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg:
        // roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a
        // company.
    }
}
