package me.rewhite.delivery.TabletFragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.tablet.LocalOrderInputActivity;
import me.rewhite.delivery.tablet.TabletOrderInputActivity;


public class TabletPickupItemSubCategoryFragmentV2 extends Fragment implements AdapterView.OnItemClickListener{

    private final static String TAG = "TabletPickupItemSubCategoryFragmentV2";
    private TabletOrderInputActivity mActivity;
    private LocalOrderInputActivity lActivity;

    public AQuery aq;

    private String fragmentName;

    private String mainCateName;
    private int mainCateIndex;
    JSONArray data;
    GridView gv;

    private ViewGroup mView = null;
    boolean isLocalHost = false;

    public TabletPickupItemSubCategoryFragmentV2() {
        // Required empty public constructor
    }

    public static TabletPickupItemSubCategoryFragmentV2 newInstance(String mainCategoryName, int mainCategoryIndex) {
        TabletPickupItemSubCategoryFragmentV2 fragment = new TabletPickupItemSubCategoryFragmentV2();
        Bundle args = new Bundle();
        args.putString("name", mainCategoryName);
        args.putInt("index", mainCategoryIndex);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getActivity() instanceof TabletOrderInputActivity) {
            isLocalHost = false;
            mActivity = (TabletOrderInputActivity)getActivity();
            fragmentName = TabletOrderInputActivity.FRAGMENT_DETAIL;
        }else{
            isLocalHost = true;
            lActivity = (LocalOrderInputActivity)getActivity();
            fragmentName = LocalOrderInputActivity.FRAGMENT_DETAIL;
        }


        if (getArguments() != null) {
            mainCateName = getArguments().getString("name");
            mainCateIndex = getArguments().getInt("index");
            try {
                if(isLocalHost){
                    data = lActivity.priceArray.getJSONObject(lActivity.Step1Index).getJSONArray("items");
                }else{
                    data = mActivity.priceArray.getJSONObject(mActivity.Step1Index).getJSONArray("items");
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.tablet_fragment_pickup_item_sub_v2, container, false);
        aq = new AQuery(mView);

        if (getActivity() instanceof TabletOrderInputActivity) {
            isLocalHost = false;
            mActivity = (TabletOrderInputActivity)getActivity();
        }else{
            isLocalHost = true;
            lActivity = (LocalOrderInputActivity)getActivity();
        }

        aq.id(R.id.text_main_cate).text(mainCateName);
        //aq.id(R.id.title_text).text(mainCateName + " > " + optionItems.get(_index));

        // 커스텀 아답타 생성
        ColorPickAdapter adapter = new ColorPickAdapter (
                getActivity(),
                R.layout.tablet_pui_parts_layout_row,       // GridView 항목의 레이아웃 row.xml
                data);    // 데이터

        gv = (GridView)mView.findViewById(R.id.gridView);
        gv.setAdapter(adapter);  // 커스텀 아답타를 GridView 에 적용
        gv.setOnItemClickListener(this);

        aq.id(R.id.btn_select).clicked(this, "selectAction");
        aq.id(R.id.btn_close).clicked(this, "closeAction");
        aq.id(R.id.btn_back).clicked(this, "backAction");

        return  mView;
    }

    public void closeAction(View button){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setCancelable(true).setMessage("세탁물 등록이 완료되지 않았습니다.\n" +
                "중단하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(isLocalHost){
                            lActivity.showFragment(LocalOrderInputActivity.FRAGMENT_BLANK);
                        }else{
                            mActivity.showFragment(TabletOrderInputActivity.FRAGMENT_BLANK);
                        }

                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
    public void backAction(View button){
        getActivity().onBackPressed();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
/*
        if(old_position<parent.getChildCount())
            parent.getChildAt(old_position).findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_normal);
            ((TextView)parent.getChildAt(old_position).findViewById(R.id.parts_title)).setTextColor(0xFF454545);
*/
        try {

            if(isLocalHost){
                lActivity.Step2Name = data.getJSONObject(position).getString("itemTitle");
                lActivity.Step2Index = position;
                TabletPickupDetailFragmentV2Local subFragment = TabletPickupDetailFragmentV2Local.newInstance(lActivity.Step2Name, position);
                view.findViewById(R.id.element).setBackgroundResource(R.drawable.tablet_pui_button_drawable_pressed);
                ((TextView)view.findViewById(R.id.parts_title)).setTextColor(0xFFFFFFFF);

                //subFragment = TabletPickupDetailFragmentV2.newInstance(mActivity.Step2Name, position);

                FragmentManager fm = lActivity.getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                //Fragment fragment = fm.findFragmentByTag(fragmentName);

                ft
                        .replace(R.id.content_frame, subFragment)
                        //.addToBackStack(fragmentName)
                        .commitAllowingStateLoss();
                        //.commit();
            }else{
                mActivity.Step2Name = data.getJSONObject(position).getString("itemTitle");
                mActivity.Step2Index = position;
                TabletPickupDetailFragmentV2 subFragment = TabletPickupDetailFragmentV2.newInstance(mActivity.Step2Name, position);
                view.findViewById(R.id.element).setBackgroundResource(R.drawable.tablet_pui_button_drawable_pressed);
                ((TextView)view.findViewById(R.id.parts_title)).setTextColor(0xFFFFFFFF);

                //subFragment = TabletPickupDetailFragmentV2.newInstance(mActivity.Step2Name, position);

                FragmentManager fm = mActivity.getSupportFragmentManager();
                fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction ft = fm.beginTransaction();
                //Fragment fragment = fm.findFragmentByTag(fragmentName);

                ft
                        .replace(R.id.content_frame, subFragment)
                        //.addToBackStack(fragmentName)
                        .commitAllowingStateLoss();
                        //.commit();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Log.i("PartsClicked", mActivity.priceArray.toString());
    }

    class ColorPickAdapter extends BaseAdapter {
        Context context;
        int layout;
        JSONArray parts;
        LayoutInflater inf;

        public ColorPickAdapter(Context context, int layout, JSONArray parts) {
            this.context = context;
            this.layout = layout;
            this.parts = parts;
            inf = (LayoutInflater) context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return data.length();
        }

        @Override
        public Object getItem(int position) {
            JSONObject obj = new JSONObject();
            try {
                obj = data.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return obj;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView==null)
                convertView = inf.inflate(layout, null);

            try {
                /*
                int partId = parts.getJSONObject(position).getInt("itemId");
                for(int j = 0; j < parts.length(); j++){
                    int pickedPartId = parts.getJSONObject(j).getInt("itemId");
                    if(partId == pickedPartId){
                        convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_pressed);
                        ((TextView)convertView.findViewById(R.id.parts_title)).setTextColor(0xFFFFFFFF);
                        Log.i("picked initialize", pickedPartId + "");
                    }
                }*/

                TextView iv = (TextView)convertView.findViewById(R.id.parts_title);
                iv.setText(data.getJSONObject(position).getString("itemTitle"));
                //@drawable/pui_button_drawable_normal

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return convertView;
        }
    }

}
