package me.rewhite.delivery.TabletFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.PickupItemActivity;
import me.rewhite.delivery.tablet.LocalOrderInputActivity;
import me.rewhite.delivery.tablet.TabletOrderInputActivity;


public class TabletPickupItemFragmentV2 extends Fragment implements AdapterView.OnItemClickListener{

    private final static String TAG = "TabletPickupItemFragmentV2";
    private TabletOrderInputActivity mActivity;
    private LocalOrderInputActivity lActivity;
    boolean isLocalHost = false;

    JSONArray parentPriceArray;

    public AQuery aq;
    GridView gv;

    ProgressDialog dialog;
    public ArrayList<String> optionItems = new ArrayList<String>();
    private boolean scrolling = false;
    private RelativeLayout.LayoutParams layoutParams;
    private View rootselectview;
    private View rootbackview;
    private String fragmentName = PickupItemActivity.FRAGMENT_SUB_CATEGORY;

    private ViewGroup mView = null;

    public TabletPickupItemFragmentV2() {
        // Required empty public constructor
    }

    public static TabletPickupItemFragmentV2 newInstance(String param1, String param2) {
        TabletPickupItemFragmentV2 fragment = new TabletPickupItemFragmentV2();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.tablet_fragment_pickup_item_fragment_v2, container, false);
        aq = new AQuery(mView);

        if (getActivity() instanceof TabletOrderInputActivity) {
            isLocalHost = false;
            mActivity = (TabletOrderInputActivity)getActivity();
            parentPriceArray = mActivity.priceArray;
        }else{
            isLocalHost = true;
            lActivity = (LocalOrderInputActivity)getActivity();
            parentPriceArray = lActivity.priceArray;
        }

        try {
            if(optionItems.size() == 0){
                for(int i = 0 ; i < parentPriceArray.length(); i++){
                    optionItems.add(parentPriceArray.getJSONObject(i).getString("itemTitle"));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // 커스텀 아답타 생성
        ColorPickAdapter adapter = new ColorPickAdapter (
                getActivity(),
                R.layout.tablet_pui_parts_layout_row,       // GridView 항목의 레이아웃 row.xml
                parentPriceArray);    // 데이터

        gv = (GridView)mView.findViewById(R.id.gridView);
        gv.setAdapter(adapter);  // 커스텀 아답타를 GridView 에 적용
        gv.setOnItemClickListener(this);

        aq.id(R.id.btn_close).clicked(this, "closeAction");
        aq.id(R.id.btn_save).clicked(this, "saveAction");
        aq.id(R.id.btn_select).clicked(this, "selectAction");
        aq.id(R.id.btn_close).clicked(this, "closeAction");
        //setCurrentLabel(wheelview.getCurrentItem());

        return  mView;
    }


    public void closeAction(View button){
        if(isLocalHost){
            lActivity.showFragment(LocalOrderInputActivity.FRAGMENT_BLANK);
        }else{
            mActivity.showFragment(LocalOrderInputActivity.FRAGMENT_BLANK);
        }

    }

    int old_position = 0;


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
/*
        if(old_position<parent.getChildCount())
            parent.getChildAt(old_position).findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_normal);
            ((TextView)parent.getChildAt(old_position).findViewById(R.id.parts_title)).setTextColor(0xFF454545);
*/
        try {
            if(isLocalHost){
                lActivity.Step1Name = parentPriceArray.getJSONObject(position).getString("itemTitle");
                lActivity.Step1Index = position;
            }else{
                mActivity.Step1Name = parentPriceArray.getJSONObject(position).getString("itemTitle");
                mActivity.Step1Index = position;
            }


            view.findViewById(R.id.element).setBackgroundResource(R.drawable.tablet_pui_button_drawable_pressed);
            ((TextView)view.findViewById(R.id.parts_title)).setTextColor(0xFFFFFFFF);
            Log.e("gridView clicked", position + "");

            FragmentManager fm = getActivity().getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Fragment fragment = fm.findFragmentByTag(fragmentName);
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = TabletPickupItemSubCategoryFragmentV2.newInstance(parentPriceArray.getJSONObject(position).getString("itemTitle"), position);

            ft
                    .replace(R.id.content_frame, fragment)
                    //.addToBackStack(fragmentName)
                    .commitAllowingStateLoss();

            old_position=position;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Log.i("PartsClicked", mActivity.priceArray.toString());
    }

    class ColorPickAdapter extends BaseAdapter {
        Context context;
        int layout;
        JSONArray parts;
        LayoutInflater inf;

        public ColorPickAdapter(Context context, int layout, JSONArray parts) {
            this.context = context;
            this.layout = layout;
            this.parts = parts;
            inf = (LayoutInflater) context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return parts.length();
        }

        @Override
        public Object getItem(int position) {
            JSONObject obj = new JSONObject();
            try {
                obj = parts.getJSONObject(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return obj;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView==null)
                convertView = inf.inflate(layout, null);

            try {
                /*
                int partId = parts.getJSONObject(position).getInt("itemId");
                for(int j = 0; j < parts.length(); j++){
                    int pickedPartId = parts.getJSONObject(j).getInt("itemId");
                    if(partId == pickedPartId){
                        convertView.findViewById(R.id.element).setBackgroundResource(R.drawable.pui_button_drawable_pressed);
                        ((TextView)convertView.findViewById(R.id.parts_title)).setTextColor(0xFFFFFFFF);
                        Log.i("picked initialize", pickedPartId + "");
                    }
                }*/

                TextView iv = (TextView)convertView.findViewById(R.id.parts_title);
                iv.setText(parts.getJSONObject(position).getString("itemTitle"));
                //@drawable/pui_button_drawable_normal

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return convertView;
        }
    }
}
