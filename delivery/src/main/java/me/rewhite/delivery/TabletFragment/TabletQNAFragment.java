package me.rewhite.delivery.TabletFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.util.CommonUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabletQNAFragment extends Fragment {

    private ViewGroup mView = null;
    AQuery aq;
    JSONObject data;

    public TabletQNAFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_tablet_qna, container, false);

        aq = new AQuery(mView);
        //aq.id(R.id.result).text(mActivity.storeInfo.toString());

        CommonUtility.setTypefaceLightSetup(aq.id(R.id.text_01).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_02).getTextView());

        return mView;
    }

}
