package me.rewhite.delivery.TabletFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabletUserSearchFragment extends Fragment {

    private ViewGroup mView = null;
    AQuery aq;

    public TabletUserSearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_tablet_blank, container, false);
        aq = new AQuery(mView);

        return mView;
    }


}
