package me.rewhite.delivery.TabletFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.PartsExpandableAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.tablet.TabletPriceSettingActivity;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabletPartsPriceTableFragment extends Fragment {

    private ViewGroup mView = null;
    AQuery aq;
    TabletPriceSettingActivity mActivity;
    JSONObject data;
    ListView list;

    public TabletPartsPriceTableFragment() {
        // Required empty public constructor
    }

    public void refreshListView(int position, String isUse){
        try {
            priceArray.getJSONObject(position).put("isUse",isUse);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_tablet_parts_table, container, false);
        mActivity = (TabletPriceSettingActivity)getActivity();

        aq = new AQuery(mView);

        list = (ListView)mView.findViewById(R.id.list);


//        // 그룹이 닫힐 경우 이벤트
//        list.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//            @Override
//            public void onGroupCollapse(int groupPosition) {
//                Toast.makeText(getActivity(), "g Collapse = " + groupPosition,
//                        Toast.LENGTH_SHORT).show();
//                //list.getExpandableListAdapter().getGroup(groupPosition)
//            }
//        });
//
//        // 그룹이 열릴 경우 이벤트
//        list.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//            @Override
//            public void onGroupExpand(int groupPosition) {
//                Toast.makeText(getActivity(), "g Expand = " + groupPosition,
//                        Toast.LENGTH_SHORT).show();
//            }
//        });

        initialize();

        return mView;
    }

    PartsExpandableAdapter adapter;
    JSONArray priceArray;

    private void initialize(){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.GET_PARTS_PRICE_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GET_PARTS_PRICE_LIST, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_PARTS_PRICE_LIST, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        priceArray = jsondata.getJSONArray("data");
                        list = (ListView)mView.findViewById(R.id.list);
                        adapter = new PartsExpandableAdapter(getActivity(), priceArray);
                        list.setAdapter(adapter);

                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

}
