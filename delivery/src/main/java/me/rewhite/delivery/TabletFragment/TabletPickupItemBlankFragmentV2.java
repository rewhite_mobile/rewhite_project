package me.rewhite.delivery.TabletFragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;
import me.rewhite.delivery.activity.PickupItemTagMod;
import me.rewhite.delivery.tablet.LocalOrderInputActivity;
import me.rewhite.delivery.tablet.TabletOrderInputActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.MZValidator;


public class TabletPickupItemBlankFragmentV2 extends Fragment{

    private final static String TAG = "TabletPickupItemBlankFragmentV2";
    private TabletOrderInputActivity mActivity;
    private LocalOrderInputActivity lActivity;
    boolean isLocalHost = false;

    public AQuery aq;

    ProgressDialog dialog;

    private ViewGroup mView = null;

    public TabletPickupItemBlankFragmentV2() {
        // Required empty public constructor
    }

    public static TabletPickupItemBlankFragmentV2 newInstance(String param1, String param2) {
        TabletPickupItemBlankFragmentV2 fragment = new TabletPickupItemBlankFragmentV2();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.tablet_fragment_pickup_blank_fragment_v2, container, false);
        aq = new AQuery(mView);

        if (getActivity() instanceof TabletOrderInputActivity) {
            isLocalHost = false;
            mActivity = (TabletOrderInputActivity)getActivity();
            aq.id(R.id.current_tag_no).text(MZValidator.getTagFormmater(mActivity.tagNo, mActivity.tagLength));
        }else{
            isLocalHost = true;
            lActivity = (LocalOrderInputActivity)getActivity();
            aq.id(R.id.current_tag_no).text(MZValidator.getTagFormmater(lActivity.tagNo, lActivity.tagLength));
        }

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_top).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.current_tag_no).getTextView());


        aq.id(R.id.btn_add).clicked(this, "additemAction");
        aq.id(R.id.btn_exit).clicked(this, "exitAction");
        aq.id(R.id.btn_mod_tag).clicked(this, "modTagAction");

        return  mView;
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        Log.d(TAG, "onActivityResult()");
//        super.onActivityResult(requestCode, resultCode, data);
//
//        Fragment fragment = getFragmentManager().findFragmentByTag(FragmentTag.FRAGMENT_TAG_HISTORY);
//        if (fragment != null) {
//            ((FragmentHistory) fragment).onActivityResult(requestCode, resultCode, data);
//        }
//    }

    public void setTagValue(){
        aq.id(R.id.current_tag_no).text(MZValidator.getTagFormmater(mActivity.tagNo, mActivity.tagLength));
    }

    public void modTagAction(View button){
        Intent pickIntent = new Intent(getActivity(), PickupItemTagMod.class);
        pickIntent.putExtra("initTagNo", mActivity.tagNo);
        pickIntent.putExtra("initTagLength", mActivity.tagLength);
        pickIntent.setAction(Intent.ACTION_GET_CONTENT);
        getActivity().startActivityForResult(pickIntent, TabletOrderInputActivity.MOD_TAG);
    }

    public void exitAction(View button){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setCancelable(true).setMessage("세탁물 등록이 완료되지 않았습니다.\n중단하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void additemAction(View button){

        if(isLocalHost){
            lActivity.addActionStatement();
        }else{
            mActivity.addActionStatement();
        }
    }

}
