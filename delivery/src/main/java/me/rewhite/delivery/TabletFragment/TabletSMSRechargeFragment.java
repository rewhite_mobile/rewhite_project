package me.rewhite.delivery.TabletFragment;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.util.CommonUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabletSMSRechargeFragment extends Fragment {

    private ViewGroup mView = null;
    AQuery aq;
    JSONObject data;
    Context mCtx;

    String content01 = "저희 세탁소를 찾아주셔서  대단히 감사 드리며,  고객님의 옷을 소중히 생각하겠습니다.";
    String content02 = "접수하신 세탁물의 세탁 내역 입니다. \n<인수증보기> \nwww.rewhite.me";
    String content03 = "세탁(또는 수선)이 완료되었으니, 편하신 시간에 방문해주세요. ";
    String content04 = "고객님의 소중한 세탁물을 장기보관 중이니 빠른 시일 내에 방문 부탁 드립니다.";
    String content05 = "현재 세탁요금 XXX,XXX원이 미입금 상태이니, 방문 결제 부탁 드립니다.";

    boolean isSelected01 = false;
    boolean isSelected02 = false;
    boolean isSelected03 = false;
    boolean isSelected04 = false;
    boolean isSelected05 = false;
    boolean smsActivate[] = {isSelected01, isSelected02, isSelected03, isSelected04, isSelected05};
    int smsActivateRes[] = {R.id.btn_sms_status_01, R.id.btn_sms_status_02, R.id.btn_sms_status_03, R.id.btn_sms_status_04, R.id.btn_sms_status_05};

    public TabletSMSRechargeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_tablet_sms_recharge, container, false);
        mCtx = getActivity();
        aq = new AQuery(mView);
        //aq.id(R.id.result).text(mActivity.storeInfo.toString());

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_van_name).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_cat_model).getTextView());

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_title_01).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_title_02).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_title_03).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_title_04).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_title_05).getTextView());

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_content_01).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_content_02).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_content_03).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_content_04).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.text_content_05).getTextView());

        aq.id(R.id.text_content_01).text(content01);
        aq.id(R.id.text_content_02).text(content02);
        aq.id(R.id.text_content_03).text(content03);
        aq.id(R.id.text_content_04).text(content04);
        aq.id(R.id.text_content_05).text(content05);



        for(int i = 0 ; i < smsActivate.length; i++){
            if(smsActivate[i]){
                aq.id(smsActivateRes[i]).image(R.mipmap.btn_tablet_sms_send_auto);
            }else{
                aq.id(smsActivateRes[i]).image(R.mipmap.btn_tablet_sms_send_pause);
            }
        }



        aq.id(R.id.btn_sms_status_01).clicked(this, "smsTypeActivateAction").tag(0);
        aq.id(R.id.btn_sms_status_02).clicked(this, "smsTypeActivateAction").tag(1);
        aq.id(R.id.btn_sms_status_03).clicked(this, "smsTypeActivateAction").tag(2);
        aq.id(R.id.btn_sms_status_04).clicked(this, "smsTypeActivateAction").tag(3);
        aq.id(R.id.btn_sms_status_05).clicked(this, "smsTypeActivateAction").tag(4);

        return mView;
    }

    public void smsTypeActivateAction(View button){
        final int tag = (int)(button.getTag());
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
        String msg = "현재 문자 자동발송이 중단된 상태입니다. 지금 충전하시면 바로 사용하실 수 있어요.";
        String msgOk = "자동 발송을 중지하시겠어요?";
        String msgNo = "자동 발송으로 전환하시겠어요?";

        String tempMsg = "";
        if(smsActivate[tag]){
            tempMsg = msgOk;
        }else{
            tempMsg = msgNo;
        }

        alertDialogBuilder.setCancelable(false).setMessage(tempMsg)
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        smsActivate[tag] = !smsActivate[tag];
                        if(smsActivate[tag]){
                            aq.id(smsActivateRes[tag]).image(R.mipmap.btn_tablet_sms_send_auto);
                        }else{
                            aq.id(smsActivateRes[tag]).image(R.mipmap.btn_tablet_sms_send_pause);
                        }
                    }
                }).setNegativeButton("취소", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }



}
