package me.rewhite.delivery.TabletFragment;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import fdk.FDK_Module;
import me.rewhite.delivery.R;
import me.rewhite.delivery.tablet.BizNoModActivity;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;
import me.rewhite.delivery.util.UnicodeFormatter;
import me.rewhite.delivery.van.CatManager;
import me.rewhite.delivery.van.FDKCatManager;
import me.rewhite.delivery.van.VanType;

import static android.app.Activity.RESULT_OK;
import static me.rewhite.delivery.tablet.TabletSettingActivity.MOD_BIZNO_RESULT;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabletSystemManagementFragment extends Fragment {

    private ViewGroup mView = null;
    AQuery aq;
    Context mCtx;

    JSONObject data;

    String currentVanType = VanType.FIRSTDATA;

    ProgressDialog mProgressDialog;
    private Messenger mService = null;    // 서비스와 통신하는데 사용되는 메신저
    private boolean mBound = false;    // 서비스 연결 여부

    public void showDialog(String _message) {
        if (mProgressDialog == null){
            mProgressDialog = new ProgressDialog(getActivity());
        }
        mProgressDialog.setMessage(_message);
        mProgressDialog.setCancelable(false);
        if(!mProgressDialog.isShowing()){
            mProgressDialog.show();
        }
    }

    public void showDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("잠시만 기다려주세요");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void dismissDialog() {
        if (mProgressDialog != null) mProgressDialog.dismiss();
    }

    VanReceiver vanReceiver;
    int LIMIT_PAY_DIST = 50000;

    @Override
    public void onStart() {
        // TODO Auto-generated method stub

        //Register BroadcastReceiver
        //to receive event from our service
        vanReceiver = new VanReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CatManager.INTENT_ACTION);
        getActivity().registerReceiver(vanReceiver, intentFilter);

        super.onStart();
    }

    private class VanReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            // TODO Auto-generated method stub

            String RETCODE = arg1.getStringExtra("RETCODE");
            String MESSAGE = arg1.getStringExtra("MESSAGE");
            boolean RESULT = arg1.getBooleanExtra("RESULT", false);

            Log.e("fragment vanReceiver : ", RETCODE + " , " + MESSAGE);

            if("0102".equals(RETCODE)){
                if(RESULT){
                    //payCardCompleted(RETCODE);
                }else{
                    //DUtil.alertShow(TabletVanCardPaymentActivity.this, MESSAGE);
                }
            }else if("9998".equals(RETCODE)){

            }else if("8888".equals(RETCODE)){
                if(RESULT){
                    showDialog(MESSAGE);
                }else{

                }
            }else if("9001".equals(RETCODE)){

            }else if("5202".equals(RETCODE)){
                // 기기정보 수신 성공

                aq.id(R.id.iv_connection).image(R.mipmap.pui_order_element_color_74c04b);
                aq.id(R.id.text_connection_status).text("정상");

                try {
                    final JSONObject message = new JSONObject(MESSAGE);
                    setSmartroConnectionInfo(message);

//                    final Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            //Do something after 100ms
//                            smartroConnectPrint(message);
//                        }
//                    }, 2000);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else if("5302".equals(RETCODE)){
                // 테스트 출력 성공
                Log.e("fragment vanReceiver 5302 : ", "테스트 출력 성공");
            }

            dismissDialog();
        }
    }

    public void setSmartroConnectionInfo(JSONObject _connData){

        try {
            aq.id(R.id.text_model_title).text("단말기 번호");
            aq.id(R.id.text_cat_model).text(_connData.getString("deviceId"));

            aq.id(R.id.text_device_version_title).text("단말기 버전");
            aq.id(R.id.text_hw_serial).text(_connData.getString("deviceVersion"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public  void smartroConnectPrint(JSONObject _connData){
        if(mBound) {
            Message msg = Message.obtain(null, CatManager.PRINT_REQUEST, 0, 0);

            String contentString = "1c";
            String printString = "";

            try {
                contentString += UnicodeFormatter.stringToHex("A1");
                Log.w("A1", contentString);
                contentString += "1c";
                //////////////////
                printString += "0A";
                printString += "17" + UnicodeFormatter.stringToHex("단말기 연결성공", "euc-kr") + "18" + "0A";
                printString += "0A";
                printString += "0A";
                printString += UnicodeFormatter.stringToHex("단말기 번호 : " + _connData.getString("deviceId"), "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("점포 사업자번호 : " + _connData.getString("storeId"), "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("상호명 : " + _connData.getString("storeName"), "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("대표자명 : " + _connData.getString("storeOwnerName"), "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("전화번호 : " + _connData.getString("storePhone"), "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("상점 주소 : " + _connData.getString("storeAddress").substring(0,10), "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("단말기 버전 : " + _connData.getString("deviceVersion"), "euc-kr") + "0A";
                printString += "0A";

                printString += "1D1D1D";
                //////////////////
                String hexSize = UnicodeFormatter.stringToHex(String.format(Locale.KOREA, "%04d", (printString.length()/2)+4));
                contentString += hexSize + printString;
                contentString += "1c";
                contentString += "1c";
            } catch (UnsupportedEncodingException|JSONException e) {
                e.printStackTrace();
            }
            byte[] content = new byte[contentString.length()/2];
            content = UnicodeFormatter.hexToByteArray(contentString);

            Log.w("print content", contentString);

            try{
                Bundle b = new Bundle();
                b.putByteArray("content", content); // for example
                msg.setData(b);
                mService.send(msg);
            }catch(RemoteException e){
                e.printStackTrace();
            }
        }else{
            DUtil.alertShow(getActivity(), "PRINT가 연결되지 않았습니다. 연결상태를 확인해주세요.");
        }
    }

    @Override
    public void onStop(){
        super.onStop();
        if(mBound){
            getActivity().unbindService(mConnection);
            mBound = false;
        }
        getActivity().unregisterReceiver(vanReceiver);
    }

    // ServiceConnection 인터페이스를 구현하는 객체를 생성한다.
    private ServiceConnection mConnection = new ServiceConnection(){

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = new Messenger(service);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0){
            mService = null;
            mBound = false;
        }
    };


    public TabletSystemManagementFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_tablet_system_manage, container, false);
        aq = new AQuery(mView);

        mCtx = getActivity();

        aq.id(R.id.btn_connection).clicked(this, "getConnectionInfo");
        aq.id(R.id.btn_smartro_connection).clicked(this, "SmartroConnectionCheck");
        //aq.id(R.id.btn_download).clicked(this, "catDownloadAction");
        aq.id(R.id.btn_mod_bizno).clicked(this, "biznoModAction");
        //aq.id(R.id.btn_print).clicked(this, "printCat");
        aq.id(R.id.btn_van_type_0).clicked(this, "selectVanType").tag(0);
        aq.id(R.id.btn_van_type_1).clicked(this, "selectVanType").tag(1);
        aq.id(R.id.btn_van_type_2).clicked(this, "selectVanType").tag(2);
        //aq.id(R.id.btn_device_info).clicked(this, "getDeviceInfo");

        currentVanType = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.VAN_TYPE);

        aq.id(R.id.text_van_name).text(currentVanType);
        aq.id(R.id.device_info_area).gone();

        if(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.BIZ_NO) == null || "".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.BIZ_NO))){
            aq.id(R.id.text_bizno).text("CAT단말기를 사용하려면 입력해주세요.");
        }else{
            aq.id(R.id.text_bizno).text(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.BIZ_NO));
            aq.id(R.id.text_serial).text(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.HARDWARE_SERIAL));
        }


        if(currentVanType.equals(VanType.EMPTY) || currentVanType == null || "".equals(currentVanType)){
            aq.id(R.id.device_info_area).gone();
            aq.id(R.id.smartro_input_area).gone();
            aq.id(R.id.fdk_input_area).gone();
            aq.id(R.id.van_selector_01).image(R.mipmap.pui_order_element_color_000000);
            aq.id(R.id.van_selector_02).image(R.mipmap.pui_order_element_color_ffffff);
            aq.id(R.id.van_selector_03).image(R.mipmap.pui_order_element_color_ffffff);
        }else if(currentVanType.equals(VanType.FIRSTDATA)){

            try {
                FDK_Module.Deliver_App_Resources_to_FDK("FTDI", getActivity());
            } catch (Exception e) {
                e.printStackTrace();
            }

            aq.id(R.id.fdk_only_01).visible();
            aq.id(R.id.fdk_only_01_line).visible();
            aq.id(R.id.device_info_area).visible();
            aq.id(R.id.fdk_input_area).visible();
            aq.id(R.id.smartro_input_area).gone();
            aq.id(R.id.van_selector_01).image(R.mipmap.pui_order_element_color_ffffff);
            aq.id(R.id.van_selector_02).image(R.mipmap.pui_order_element_color_000000);
            aq.id(R.id.van_selector_03).image(R.mipmap.pui_order_element_color_ffffff);
            aq.id(R.id.text_van_name).text(currentVanType);

            try {
                onClick_SearchCAT();
                //readCATInformation_ISD();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(currentVanType.equals(VanType.SMARTRO)){
            aq.id(R.id.device_info_area).visible();
            aq.id(R.id.fdk_only_01).gone();
            aq.id(R.id.fdk_only_01_line).gone();
            aq.id(R.id.smartro_input_area).visible();
            aq.id(R.id.fdk_input_area).gone();
            aq.id(R.id.van_selector_01).image(R.mipmap.pui_order_element_color_ffffff);
            aq.id(R.id.van_selector_02).image(R.mipmap.pui_order_element_color_ffffff);
            aq.id(R.id.van_selector_03).image(R.mipmap.pui_order_element_color_000000);
            aq.id(R.id.text_van_name).text(currentVanType);

            String successDeviceIP = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.CAT_DEVICE_IP);
            if(successDeviceIP != null) {
                aq.id(R.id.text_ip_address).text(successDeviceIP);

                networkAuthSmartro();
            }
        }

        return mView;
    }

    public void SmartroConnectionCheck(View button){
        try {
            networkScan();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    ArrayList<String> ipList;
    private void networkScan() throws UnknownHostException {
        ipList = new ArrayList();
        new ScanIpTask().execute();
    }

    public void networkAuthSmartro(){
        String successDeviceIP = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.CAT_DEVICE_IP);
        if(successDeviceIP != null){
            Log.w("Smartro success ip try", successDeviceIP);

            aq.id(R.id.iv_connection).image(R.mipmap.pui_order_element_color_74c04b);
            aq.id(R.id.text_connection_status).text("정상");

            aq.id(R.id.text_ip_address).text(successDeviceIP);

            connectionClose();

            Intent catIntent = new Intent(getActivity(), CatManager.class);
            catIntent.putExtra("ServerIP", successDeviceIP);
            getActivity().bindService(catIntent, mConnection, Context.BIND_AUTO_CREATE);
            //Toast.makeText(mCtx, "Done", Toast.LENGTH_LONG).show();

            SharedPreferencesUtility.set(SharedPreferencesUtility.Van.IS_CONNECTED, "TRUE");
        }else{
            aq.id(R.id.iv_connection).image(R.mipmap.pui_order_element_color_cf0000);
            aq.id(R.id.text_connection_status).text("연결안됨");

            SharedPreferencesUtility.set(SharedPreferencesUtility.Van.IS_CONNECTED, "FALSE");
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case MOD_BIZNO_RESULT:
                    String bizId = data.getStringExtra("bizNo");
                    String hwSerial = data.getStringExtra("hwSerial");
                    SharedPreferencesUtility.set(SharedPreferencesUtility.Van.BIZ_NO, bizId);
                    SharedPreferencesUtility.set(SharedPreferencesUtility.Van.HARDWARE_SERIAL, hwSerial);
                    aq.id(R.id.text_bizno).text(bizId);
                    aq.id(R.id.text_serial).text(hwSerial);

                    break;
            }
        }else{

        }
    }

    public void biznoModAction(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setClass(getActivity(), BizNoModActivity.class);
                getActivity().startActivityForResult(intent, MOD_BIZNO_RESULT);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }, 0);
    }

    public void getConnectionInfo(View button){

        if(currentVanType.equals(VanType.FIRSTDATA)){
            try {
                readCATInformation_ISD();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if(currentVanType.equals(VanType.SMARTRO)){
            String successDeviceIP = SharedPreferencesUtility.get(SharedPreferencesUtility.Van.CAT_DEVICE_IP);

            if(mBound){
                Log.w("Smartro STORE_INFO_REQUEST", successDeviceIP);
                Message msg = Message.obtain(null, CatManager.STORE_INFO_REQUEST, 0, 0);
                try{
//                    Bundle b = new Bundle();
//                    b.putString("ip", successDeviceIP); // for example
//                    msg.setData(b);
                    mService.send(msg);

                }catch(RemoteException e){
                    e.printStackTrace();
                }
            }else{
                Log.w("Smartro mBound", "null");
            }
        }

    }


    final String vanTypeItems[] = {VanType.EMPTY, VanType.FIRSTDATA, VanType.SMARTRO};
    int selectedVanIndex = 0;

    public void selectVanType(View button){
        int tag = (int)button.getTag();
        selectedVanIndex = tag;
        currentVanType = vanTypeItems[selectedVanIndex];
        SharedPreferencesUtility.set(SharedPreferencesUtility.Van.VAN_TYPE, currentVanType);

        if(tag == 0){
            aq.id(R.id.van_selector_01).image(R.mipmap.pui_order_element_color_000000);
            aq.id(R.id.van_selector_02).image(R.mipmap.pui_order_element_color_ffffff);
            aq.id(R.id.van_selector_03).image(R.mipmap.pui_order_element_color_ffffff);
            aq.id(R.id.text_van_name).text("사용안함");
            aq.id(R.id.device_info_area).gone();
            aq.id(R.id.fdk_input_area).gone();
            aq.id(R.id.smartro_input_area).gone();
        }else if(tag == 1){
            aq.id(R.id.van_selector_01).image(R.mipmap.pui_order_element_color_ffffff);
            aq.id(R.id.van_selector_02).image(R.mipmap.pui_order_element_color_000000);
            aq.id(R.id.van_selector_03).image(R.mipmap.pui_order_element_color_ffffff);
            aq.id(R.id.text_van_name).text(VanType.FIRSTDATA);
            aq.id(R.id.device_info_area).visible();
            aq.id(R.id.fdk_only_01).visible();
            aq.id(R.id.fdk_only_01_line).visible();
            aq.id(R.id.fdk_input_area).visible();
            aq.id(R.id.smartro_input_area).gone();

            if("TRUE".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.IS_CONNECTED)) && VanType.FIRSTDATA.equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.VAN_TYPE))){
                aq.id(R.id.text_cat_model).text(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.MODEL_NAME));
                aq.id(R.id.text_hw_serial).text(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.HARDWARE_SERIAL));
                aq.id(R.id.text_port_info).text(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.PORT_NAME));
                //aq.id(R.id.text_baudrate).text(SharedPreferencesUtility.get(SharedPreferencesUtility.Van.BAUDRATE));
                aq.id(R.id.iv_connection).image(R.mipmap.pui_order_element_color_74c04b);
                aq.id(R.id.text_connection_status).text("정상");
                aq.id(R.id.btn_layout).visible();
            }else{
                aq.id(R.id.text_cat_model).text("연결안됨");
                aq.id(R.id.text_hw_serial).text("연결안됨");
                aq.id(R.id.text_port_info).text("연결안됨");
                //aq.id(R.id.text_baudrate).text("연결안됨");
                aq.id(R.id.iv_connection).image(R.mipmap.pui_order_element_color_cf0000);
                aq.id(R.id.text_connection_status).text("연결안됨");
                try {
                    onClick_SearchCAT();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }else if(tag == 2){
            aq.id(R.id.van_selector_01).image(R.mipmap.pui_order_element_color_ffffff);
            aq.id(R.id.van_selector_02).image(R.mipmap.pui_order_element_color_ffffff);
            aq.id(R.id.van_selector_03).image(R.mipmap.pui_order_element_color_000000);
            aq.id(R.id.text_van_name).text(VanType.SMARTRO);
            aq.id(R.id.device_info_area).visible();
            aq.id(R.id.fdk_only_01).gone();
            aq.id(R.id.fdk_only_01_line).gone();

            aq.id(R.id.fdk_input_area).gone();
            aq.id(R.id.smartro_input_area).visible();

            try {
                networkScan();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
    }

    public void ShowMsg(String strMsg)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();     //닫기
            }
        });
        alert.setMessage(strMsg);
        alert.show();
    }

    public void onClick_SearchCAT() throws Exception {
        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        iProcID = FDK_Module.Creat();
        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/Util/SearchCAT");

        if(0 == iRet)
        {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        }
        else if(-1000 == iRet)
        {
            strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        }
        else
        {
            strMsg = "에러[" + iRet + "]\r\n";
            strData = "";
            strData = FDK_Module.Output(iProcID, "ErrorInfo");
            strMsg = strMsg + strData;
        }
        if(0 != iRet)
            ShowMsg(strMsg);

        //Output Sample
        if(0 == iRet || -1000 == iRet)
        {

            aq.id(R.id.text_port_info).text(FDK_Module.Output(iProcID, "Port") + "");
            SharedPreferencesUtility.set(SharedPreferencesUtility.Van.PORT_NAME, FDK_Module.Output(iProcID, "Port"));
            //aq.id(R.id.text_baudrate).text(FDK_Module.Output(iProcID, "Baudrate") + "");
            SharedPreferencesUtility.set(SharedPreferencesUtility.Van.BAUDRATE, FDK_Module.Output(iProcID, "Baudrate"));
            aq.id(R.id.btn_layout).visible();

            strMsg = "Output List :\r\n";
            strMsg += "Port : " + FDK_Module.Output(iProcID, "Port") + "\r\n";
            strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
            strMsg += "Baudrate : " + FDK_Module.Output(iProcID, "Baudrate") + "\r\n";
            strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
            //ShowMsg(strMsg);

        }

        FDK_Module.Destroy(iProcID);
    }

    public void registerDevice() throws Exception
    {
        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        iProcID = FDK_Module.Creat();

        FDK_Module.Input(iProcID, "Business Number", SharedPreferencesUtility.get(SharedPreferencesUtility.Van.BIZ_NO));
        FDK_Module.Input(iProcID, "CAT Serial Number", SharedPreferencesUtility.get(SharedPreferencesUtility.Van.HARDWARE_SERIAL));

        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/SecuritySafe_Tell/CATDataDownload_DL");
        if(0 == iRet)
        {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        }
        else if(-1000 == iRet)
        {
            strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
        }
        else
        {
            strMsg = "에러[" + iRet + "]\r\n";
            strData = "";
            strData = FDK_Module.Output(iProcID, "ErrorInfo");
            strMsg = strMsg + strData;
        }
        ShowMsg(strMsg);

        //Output Sample
        if(0 == iRet || -1000 == iRet)
        {
            strMsg = "Output List :\r\n";
            strMsg += "STX : " + FDK_Module.Output(iProcID, "STX") + "\r\n";
            strMsg += "Work Type : " + FDK_Module.Output(iProcID, "Work Type") + "\r\n";
            strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
            strMsg += "Response Code : " + FDK_Module.Output(iProcID, "Response Code") + "\r\n";
            strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
            strMsg += "Business Number : " + FDK_Module.Output(iProcID, "Business Number") + "\r\n";
            strMsg += "FS3 : " + FDK_Module.Output(iProcID, "FS3") + "\r\n";
            strMsg += "CAT ID : " + FDK_Module.Output(iProcID, "CAT ID") + "\r\n";
            strMsg += "FS4 : " + FDK_Module.Output(iProcID, "FS4") + "\r\n";
            strMsg += "Representative Name : " + FDK_Module.Output(iProcID, "Representative Name") + "\r\n";
            strMsg += "FS5 : " + FDK_Module.Output(iProcID, "FS5") + "\r\n";
            strMsg += "Merchant Name : " + FDK_Module.Output(iProcID, "Merchant Name") + "\r\n";
            strMsg += "FS6 : " + FDK_Module.Output(iProcID, "FS6") + "\r\n";
            strMsg += "Merchant Address : " + FDK_Module.Output(iProcID, "Merchant Address") + "\r\n";
            strMsg += "FS7 : " + FDK_Module.Output(iProcID, "FS7") + "\r\n";
            strMsg += "Merchant Phone Number : " + FDK_Module.Output(iProcID, "Merchant Phone Number") + "\r\n";
            strMsg += "FS8 : " + FDK_Module.Output(iProcID, "FS8") + "\r\n";
            strMsg += "Agent Phone Number : " + FDK_Module.Output(iProcID, "Agent Phone Number") + "\r\n";
            strMsg += "FS9 : " + FDK_Module.Output(iProcID, "FS9") + "\r\n";
            strMsg += "ETX : " + FDK_Module.Output(iProcID, "ETX") + "\r\n";
            strMsg += "LRC : " + FDK_Module.Output(iProcID, "LRC") + "\r\n";
            ShowMsg(strMsg);
        }

        iRet = FDK_Module.Destroy(iProcID);
    }


    public void readCATInformation_ISD() throws Exception
    {
        int iProcID = 0;
        int iRet = 0;
        String strMsg = "", strData = "";

        aq.id(R.id.iv_connection).image(R.mipmap.pui_order_element_color_cf0000);
        aq.id(R.id.text_connection_status).text("연결안됨");

        iProcID = FDK_Module.Creat();
        iRet = FDK_Module.Execute(iProcID, "PaymentTerminal/Read/CATInformationSecurityCertification_ISD");

        if(0 == iRet)
        {
            strMsg = "성공[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
            Log.e("readCATInformation_ISD", strMsg);

            aq.id(R.id.iv_connection).image(R.mipmap.pui_order_element_color_74c04b);
            aq.id(R.id.text_connection_status).text("정상");
        }
        else if(-1000 == iRet)
        {
            strMsg = "실패[" + iRet + "]\r\n" + "Response Code : ";
            strData = "";
            strData = FDK_Module.Output(iProcID, "Response Code");
            strMsg = strMsg + strData;
            Log.e("readCATInformation_ISD", strMsg);

            aq.id(R.id.iv_connection).image(R.mipmap.pui_order_element_color_cf0000);
            aq.id(R.id.text_connection_status).text("연결안됨");
        }
        else
        {
            strMsg = "에러[" + iRet + "]\r\n";
            strData = "";
            strData = FDK_Module.Output(iProcID, "ErrorInfo");
            strMsg = strMsg + strData;
            Log.e("readCATInformation_ISD", strMsg);
        }
        //ShowMsg(strMsg);

        //Output Sample
        if(0 == iRet || -1000 == iRet)
        {

            if(0 == iRet){
                SharedPreferencesUtility.set(SharedPreferencesUtility.Van.IS_CONNECTED, "TRUE");
            }else{
                SharedPreferencesUtility.set(SharedPreferencesUtility.Van.IS_CONNECTED, "FALSE");
            }

            aq.id(R.id.text_cat_model).text(FDK_Module.Output(iProcID, "H/W Model Name") + "");
            SharedPreferencesUtility.set(SharedPreferencesUtility.Van.MODEL_NAME, FDK_Module.Output(iProcID, "H/W Model Name"));
            aq.id(R.id.text_hw_serial).text(FDK_Module.Output(iProcID, "H/W Serial Number") + "");
            SharedPreferencesUtility.set(SharedPreferencesUtility.Van.HARDWARE_SERIAL, FDK_Module.Output(iProcID, "H/W Serial Number"));

            strMsg = "Output List :\r\n";
            strMsg += "STX : " + FDK_Module.Output(iProcID, "STX") + "\r\n";
            strMsg += "Work Type : " + FDK_Module.Output(iProcID, "Work Type") + "\r\n";
            strMsg += "FS1 : " + FDK_Module.Output(iProcID, "FS1") + "\r\n";
            strMsg += "H/W Model Name : " + FDK_Module.Output(iProcID, "H/W Model Name") + "\r\n";
            strMsg += "FS2 : " + FDK_Module.Output(iProcID, "FS2") + "\r\n";
            strMsg += "F/W Version : " + FDK_Module.Output(iProcID, "F/W Version") + "\r\n";
            strMsg += "FS3 : " + FDK_Module.Output(iProcID, "FS3") + "\r\n";
            strMsg += "H/W Serial Number : " + FDK_Module.Output(iProcID, "H/W Serial Number") + "\r\n";
            strMsg += "FS4 : " + FDK_Module.Output(iProcID, "FS4") + "\r\n";
            strMsg += "Integrity Info : " + FDK_Module.Output(iProcID, "Integrity Info") + "\r\n";
            strMsg += "FS5 : " + FDK_Module.Output(iProcID, "FS5") + "\r\n";
            strMsg += "ETX : " + FDK_Module.Output(iProcID, "ETX") + "\r\n";
            strMsg += "LRC : " + FDK_Module.Output(iProcID, "LRC") + "\r\n";
            //ShowMsg(strMsg);
            Log.e("register", strMsg);

        }

        FDK_Module.Destroy(iProcID);
    }

    public void printCat(){

            Message msg = Message.obtain(null, FDKCatManager.PRINT_REQUEST, 0, 0);

            String contentString = "1c";
            String printString = "";

            try {
                contentString += UnicodeFormatter.stringToHex("A1");
                Log.w("A1", contentString);
                contentString += "1c";
                //////////////////
                printString += "17"+UnicodeFormatter.stringToHex("      세탁물 내역서", "euc-kr") + "18"+"0A";
                printString += "0A";
                printString += "0A";
                printString += "0A";
                printString += UnicodeFormatter.stringToHex("고객명 : 김현우 (주문번호 : 12298)", "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("연락처 : 010-9977-8079", "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("수거일 : 2016-07-03 오후 5:13:58", "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("배송예정일 : 2016-07-06 오후 5:19:33", "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("주소 : 힐스테이트 501-1403", "euc-kr") + "0A";
                printString += "0A";

                printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr")+ "0A";
                printString += UnicodeFormatter.stringJustifyToHex("TAG No.  품목", "금액", 47) + "0A";
                printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr");
                printString += UnicodeFormatter.stringJustifyToHex("1-804    끈 원피스", "1,100", 47) + "0A";

                printString += "0A";
                printString += UnicodeFormatter.stringJustifyToHex("         배송비 (20,000원 이하)","2,000", 47) + "0A";
                printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr");
                printString += "13"+UnicodeFormatter.stringJustifyToHex("총 수량 :","1개", 47) +  "140A";
                printString += "13"+UnicodeFormatter.stringJustifyToHex("총 금액 :","3,100원", 47) +  "140A";
                printString += UnicodeFormatter.stringToHex("-----------------------------------------------", "euc-kr");
                printString += "0A";
                printString += "0A";

                printString += "13"+UnicodeFormatter.stringToHex("명파세탁소(테스트)", "euc-kr") + "140A";
                printString += UnicodeFormatter.stringToHex("050-7974-1394", "euc-kr") + "0A";
                printString += "0A";
                printString += "0A";
                printString += UnicodeFormatter.stringToHex("저희 세탁소를 찾아주셔서 감사합니다.", "euc-kr") + "0A";
                printString += UnicodeFormatter.stringToHex("고객님의 소중한 의류를 성심껏 관리하겠습니다.", "euc-kr") + "0A";
                printString += "0A";
                printString += "0A";
                printString += "0E"+UnicodeFormatter.stringToHex("우리동네 모바일 세탁소 리화이트", "euc-kr") + "0F0A";
                printString += UnicodeFormatter.stringToHex("www.rewhite.me", "euc-kr") + "0A";
                printString += "0A";
                printString += "0A";

                printString += "11"+UnicodeFormatter.stringToHex("10100012331", "euc-kr") + "120A";
                printString += "0A";
                printString += "0A";

                printString += "1D1D1D";
                //////////////////
                String hexSize = UnicodeFormatter.stringToHex(String.format(Locale.KOREA, "%04d", (printString.length()/2)+4));
                contentString += hexSize + printString;

                contentString += "1c";
                contentString += "1c";
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            byte[] content = new byte[contentString.length()/2];
            content = UnicodeFormatter.hexToByteArray(contentString);


    }



    private class ScanIpTask extends AsyncTask<Void, String, Void> {
        ProgressDialog asyncDialog = new ProgressDialog(getActivity());
        /*
        Scan IP 192.168.1.100~192.168.1.110
        you should try different timeout for your network/devices
         */
        static final String subnet = "192.168.0.";
        static final int lower = 1;
        static final int upper = 255;
        static final int timeout = 50;

        @Override
        protected void onPreExecute() {
            ipList.clear();
//            adapter.notifyDataSetInvalidated();
            //Toast.makeText(mCtx, "Scan IP...", Toast.LENGTH_LONG).show();

            asyncDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            asyncDialog.setMessage("내부 네트워크에서 결제단말기를 찾는중입니다...");

            // show dialog
            asyncDialog.show();
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {

            for (int i = lower; i <= upper; i++) {
                String host = subnet + i;
                int progressValue = (int)(((float)i/255)*100);
                asyncDialog.setProgress(progressValue);


                try {
                    InetAddress inetAddress = InetAddress.getByName(host);
                    if (inetAddress.isReachable(timeout)){
                        publishProgress(inetAddress.getCanonicalHostName());
                    }
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            String tempIP = values[0].replaceAll("\\/","");
            ipList.add(tempIP);

            Log.w("ipList value onProgressUpdate", tempIP);

            //Toast.makeText(mCtx, values[0], Toast.LENGTH_LONG).show();
            if(checkConnect(tempIP)){
                this.cancel(true);
            }

        }

        @Override
        protected void onCancelled() {
            if(asyncDialog.isShowing()){
                asyncDialog.dismiss();
            }
            Log.w("async onCancelled", ipList.toString());
            networkAuthSmartro();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(asyncDialog.isShowing()){
                asyncDialog.dismiss();
            }

            Log.w("ipList", ipList.toString());
            networkAuthSmartro();

        }
    }

    public void connectionClose() {
        setNetworkThreadPolicy();
        try {
            if (mChannel != null) {
                if (mThread != null) {
                    mThread.mIsRunning = false;
                    mThread.join();
                }
                mChannel.close();
                mChannel = null;
                System.out.println("tcp client channel closed");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean checkConnect(String _ipaddress){
        boolean connRet = connect(_ipaddress, (short)5555, 5, rHandler);

        if(connRet){
            Log.e("connection : ", "Success");
            Log.e("CAT_DEVICE_IP saved : ", "ip saved");
            SharedPreferencesUtility.set(SharedPreferencesUtility.Van.CAT_DEVICE_IP, _ipaddress);
        }else{
            Log.e("connection : ", "Failed");
        }

        return connRet;
    }


    protected void setNetworkThreadPolicy() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }
    protected SocketChannel mChannel;
    protected ReceiveThread mThread;
    public int RECV_BUFFER_SIZE = 1024 * 1024;

    public boolean connect(String ipAddress, short port, int timeout, ReceiveEventHandler handler) {
        setNetworkThreadPolicy();

        try     {
            if (mChannel != null && mChannel.isConnected() == true)
                return false;

            mChannel = SocketChannel.open();
            mChannel.configureBlocking(false);
            mChannel.socket().setReceiveBufferSize(RECV_BUFFER_SIZE);

            mChannel.connect(new InetSocketAddress(ipAddress, port));

            Selector selector = Selector.open();
            SelectionKey clientKey = mChannel.register(selector, SelectionKey.OP_CONNECT);

            if (selector.select(timeout*1000) > 0) {
                if (clientKey.isConnectable()) {
                    if (mChannel.finishConnect()) {
                        mThread = new ReceiveThread(mChannel, handler);
                        mThread.start();
                        return true;
                    }
                }
                mChannel.close();
                mChannel = null;
                return false;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public interface ReceiveEventHandler {
        public void onReceived(ByteBuffer buffer, int len);
        public void onClosed();
        public void onThreadEvent();
    }

    protected class ReceiveThread extends Thread {
        private SocketChannel mChannel;
        public boolean mIsRunning = false;

        private ReceiveEventHandler mHandler;

        private static final int BUFFER_SIZE = 1024 * 4;

        public ReceiveThread(SocketChannel channel, ReceiveEventHandler handler) {
            mChannel = channel;
            mHandler = handler;
            mIsRunning = true;
        }

        @Override
        public void run() {
            System.out.println("fragment receive thread start");

            try {
                Selector selector = Selector.open();
                mChannel.register(selector, SelectionKey.OP_READ);

                while (mIsRunning) {
                    //System.out.println("receive thread mIsRunning / selector isOpen : "+selector.isOpen());

                    if (selector.select(2*1000) > 0) {
                        Set<SelectionKey> selectedKey = selector.selectedKeys();
                        Iterator<SelectionKey> iterator = selectedKey.iterator();

                        System.out.println("selector.select(2*1000) : " + selectedKey.toString());

                        while (iterator.hasNext()) {
                            SelectionKey key = iterator.next();
                            iterator.remove();

                            if (key.isReadable()) {
                                SocketChannel channel = (SocketChannel)key.channel();
                                ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
                                buffer.clear();

                                int ret;
                                try {
                                    ret = channel.read(buffer);
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                    if (mHandler != null) mHandler.onClosed();
                                    key.cancel();
                                    continue;
                                }

                                System.out.println("receive thread return value :" + ret);

                                if (ret <= 0) {
                                    System.out.println("SocketChannel.read returned " + ret);
                                    if (mHandler != null) mHandler.onClosed();
                                    key.cancel();
                                    continue;
                                }

                                buffer.rewind();

                                if (mHandler != null) mHandler.onReceived(buffer, ret);
                            }
                        }
                    }else{
                        //System.out.println("selector.select(2*1000) : " + selector.select(2*1000));
                    }
                    if (mHandler != null) mHandler.onThreadEvent();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            System.out.println("receive thread end");
        }
    }

    public ReceiveEventHandler rHandler = new ReceiveEventHandler() {
        @Override
        public void onReceived(ByteBuffer buffer, int len) {
            byte[] getBytes = new byte[len];
            getBytes = buffer.array();
            String s = UnicodeFormatter.byteArrayToHex(getBytes);

            if(getBytes[0] == 0x02) {
                String resTypeHex = UnicodeFormatter.byteToHex(getBytes[5]) + UnicodeFormatter.byteToHex(getBytes[6]) + UnicodeFormatter.byteToHex(getBytes[7]) + UnicodeFormatter.byteToHex(getBytes[8]);
                byte[] resTypeBytes = UnicodeFormatter.hexToByteArray(resTypeHex);
                String resTypeStr = new String(resTypeBytes, 0, resTypeBytes.length);
                Log.e("Socket onReceived resTypeStr :: ", resTypeStr);
            }

            Log.e("Socket onReceived", s);
        }

        @Override
        public void onClosed() {
            Log.e("Socket onClosed", "");
        }

        @Override
        public void onThreadEvent() {
            Log.e("Socket onThreadEvent", "");
        }
    };
}
