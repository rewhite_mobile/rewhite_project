package me.rewhite.delivery.TabletFragment;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.FinanceMonthViewAdapter;
import me.rewhite.delivery.custom.DividerItemDecoration;
import me.rewhite.delivery.custom.MyYAxisValueFormatter;
import me.rewhite.delivery.data.MonthlyFinanceModel;
import me.rewhite.delivery.tablet.TabletFinanceActivity;
import me.rewhite.delivery.tablet.TabletMonthlyReportActivity;
/**
 * A simple {@link Fragment} subclass.
 */
public class TabletRewhiteFinanceFragment extends Fragment implements RecyclerView.OnItemTouchListener, OnChartValueSelectedListener, View.OnClickListener {

    private ViewGroup mView = null;
    AQuery aq;
    TabletFinanceActivity mActivity;
    JSONArray financeInfo;
    String financeType;

    protected BarChart mChart;
    private Typeface mTf;
    GestureDetectorCompat gestureDetector;

    public TabletRewhiteFinanceFragment(){
        if(getArguments() != null){
            financeType = getArguments().getString("financeType");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_tablet_rewhite_finance, container, false);
        mActivity = (TabletFinanceActivity)getActivity();
        financeInfo = mActivity.rewhiteFinanceInfo;
        if(mActivity.FRAGMENT_LOCALHOST_FINANCE.equals(financeType)){
            financeInfo = mActivity.localFinanceInfo;
        }else{
            financeInfo = mActivity.rewhiteFinanceInfo;
        }

        aq = new AQuery(mView);
        //aq.id(R.id.result).text(mActivity.financeInfo.toString());
        recyclerView = (RecyclerView) mView.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(aq.getContext());
        // actually VERTICAL is the default,
        // just remember: LinearLayoutManager
        // supports HORIZONTAL layout out of the box
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        // you can set the first visible item like this:
        layoutManager.scrollToPosition(0);
        recyclerView.setLayoutManager(layoutManager);


        // allows for optimizations if all items are of the same size:
        recyclerView.setHasFixedSize(true);
        gestureDetector =
                new GestureDetectorCompat(getActivity(), new RecyclerViewMonthlyOnGestureListener());

        try {
            setDatePicker();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(financeInfo != null){
            chartInitialize();
        }else{

        }


        return mView;
    }

    private void chartInitialize(){
        mChart = (BarChart)mView.findViewById(R.id.chart1);
        mChart.setOnChartValueSelectedListener(this);

        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);

        mChart.setDescription("");

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mChart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        mChart.setDrawGridBackground(false);

        float minValue = 0.f;
        for(int i =0; i < financeInfo.length(); i++){
            try {
                float cp = (float)financeInfo.getJSONObject(i).getLong("calculatePrice");
                if(cp < minValue){
                    minValue = cp;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        mTf = Typeface.createFromAsset(getActivity().getAssets(), "font/Roboto-Regular.ttf");

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(mTf);
        xAxis.setDrawGridLines(true);
        xAxis.setGridColor(Color.rgb(230, 233, 235));
        xAxis.setSpaceBetweenLabels(2);
        xAxis.setTextSize(11f);

        YAxisValueFormatter custom = new MyYAxisValueFormatter();

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTypeface(mTf);
        leftAxis.setLabelCount(8, false);
        leftAxis.setDrawGridLines(false);
        //leftAxis.setDrawZeroLine(true);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(0f);
        leftAxis.setSpaceBottom(0f);
        leftAxis.setAxisMinValue(0);
        //leftAxis.setAxisMinValue(minValue); // this replaces setStartAtZero(true)

        mChart.getAxisRight().setEnabled(false);
//        YAxis rightAxis = mChart.getAxisRight();
//        rightAxis.setDrawGridLines(false);
//        rightAxis.setTypeface(mTf);
//        rightAxis.setLabelCount(8, false);
//        rightAxis.setValueFormatter(custom);
//        rightAxis.setSpaceTop(15f);
//        rightAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)

        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART_INSIDE);
        //l.setForm(Legend.LegendForm.SQUARE);
        //l.setFormSize(11f);
        l.setYOffset(0f);
        l.setTextSize(12f);
        l.setYEntrySpace(0f);

        setData(financeInfo.length(), 50);
    }

    private void setData(int count, float range) {
        try {
            ArrayList<String> xVals = new ArrayList<String>();
            for (int i = 0; i < count; i++) {
                xVals.add(financeInfo.getJSONObject(i).getString("calculateDateString"));
            }

            ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
            ArrayList<BarEntry> yVals2 = new ArrayList<BarEntry>();
            ArrayList<BarEntry> yVals3 = new ArrayList<BarEntry>();

            for (int i = 0; i < count; i++) {
                float mult = (range + 1);
                float val = (float)financeInfo.getJSONObject(i).getLong("totalPrice"); //(float) (Math.random() * mult);
                yVals1.add(new BarEntry(val, i));
            }
            for (int i = 0; i < count; i++) {
                float val = (float)financeInfo.getJSONObject(i).getLong("onlinePrice");
                yVals2.add(new BarEntry(val, i));
            }

            for (int i = 0; i < count; i++) {
                float val = (float)financeInfo.getJSONObject(i).getLong("offlinePrice");
                yVals3.add(new BarEntry(val, i));
            }

            BarDataSet set1, set2, set3;

            if (mChart.getData() != null &&
                    mChart.getData().getDataSetCount() > 0) {
                set1 = (BarDataSet)mChart.getData().getDataSetByIndex(0);
                set2 = (BarDataSet)mChart.getData().getDataSetByIndex(1);
                set3 = (BarDataSet)mChart.getData().getDataSetByIndex(2);
                set1.setYVals(yVals1);
                set2.setYVals(yVals2);
                set3.setYVals(yVals3);
                mChart.getData().setXVals(xVals);
                mChart.getData().notifyDataChanged();
                mChart.notifyDataSetChanged();
            } else {
                set1 = new BarDataSet(yVals1, "전체");
                set1.setBarSpacePercent(35f);
                set1.setColor(Color.rgb(255, 142, 156));
                set2 = new BarDataSet(yVals2, "리화이트");
                set2.setColor(Color.rgb(197, 255, 140));
                set3 = new BarDataSet(yVals3, "현금");
                set3.setColor(Color.rgb(141, 235, 255));

                ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
                dataSets.add(set1);
                dataSets.add(set2);
                dataSets.add(set3);

                BarData data = new BarData(xVals, dataSets);
                data.setValueTextSize(11f);
                data.setValueTypeface(mTf);

                data.setGroupSpace(80f);

                mChart.setData(data);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mChart.invalidate();
    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

    }

    public void onNothingSelected() {
    }


    RecyclerView recyclerView;
    FinanceMonthViewAdapter adapter;
    private List<MonthlyFinanceModel> items;
    int itemCount;

    private void setDatePicker() throws JSONException {

        items = new ArrayList<MonthlyFinanceModel>();
        if(financeInfo != null){
            for(int i = 0; i < financeInfo.length(); i++){
                MonthlyFinanceModel fModel = new MonthlyFinanceModel();
                fModel.setLabel(financeInfo.getJSONObject(i).getString("calculateDateString"));
                //Log.e("url", mActivity.financeInfo.getJSONObject(i).getString("calculateDateUrl"));
                fModel.setLinkurl(financeInfo.getJSONObject(i).getString("calculateDateUrl"));
                fModel.setTotalPrice(financeInfo.getJSONObject(i).getInt("totalPrice"));
                fModel.setOnlinePrice(financeInfo.getJSONObject(i).getInt("onlinePrice"));
                fModel.setOfflinePrice(financeInfo.getJSONObject(i).getInt("offlinePrice"));

                items.add(fModel);
            }
        }

        adapter = new FinanceMonthViewAdapter(items);
        recyclerView.setAdapter(adapter);

        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(getActivity(), DividerItemDecoration.HORIZONTAL_LIST);
        recyclerView.addItemDecoration(itemDecoration);

        // this is the default; this call is actually only necessary with custom ItemAnimators
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        // onClickDetection is done in this Activity's onItemTouchListener
        // with the help of a GestureDetector;
        // Tip by Ian Lake on G+ in a comment to this post:
        // https://plus.google.com/+LucasRocha/posts/37U8GWtYxDE
        recyclerView.addOnItemTouchListener(this);

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        gestureDetector.onTouchEvent(e);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
        Log.i("recycle index", rv.getChildAdapterPosition(view) + "");
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public static String getQueryString(String url, String tag) {
        try {
            Uri uri= Uri.parse(url);
            return uri.getQueryParameter(tag);
        }catch(Exception e){
            Log.e("getQueryString","getQueryString() " + e.getMessage());
        }
        return "";
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.container_list_item) {
            int idx = recyclerView.getChildPosition(view);
            Log.e("recycle click", idx + "");

            MonthlyFinanceModel data = adapter.getItem(idx);
            String link = data.getLinkurl();
            String tagValue = getQueryString(link,"p");

            Log.e("link", link + " / " + tagValue);

            Intent startIntent = new Intent(getActivity(), TabletMonthlyReportActivity.class);
            startIntent.putExtra("pKey", tagValue);
            startIntent.putExtra("name", data.getLabel());
            startIntent.putExtra("type", "R");
            getActivity().startActivity(startIntent);
        }

    }

    private class RecyclerViewMonthlyOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {

            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            //Log.i("recycle click", "onSingleTapConfirmed");
            int idx = recyclerView.getChildPosition(view);
            Log.i("recycle click", idx + "");

            MonthlyFinanceModel data = adapter.getItem(idx);
            String link = data.getLinkurl();
//            String[] linkArr = link.split("[?p=]");
//            String p = linkArr[1];

            String tagValue = getQueryString(link,"p");

            //Log.e("link", link + " / " + tagValue);

            Intent startIntent = new Intent(getActivity(), TabletMonthlyReportActivity.class);
            startIntent.putExtra("pKey", tagValue);
            startIntent.putExtra("name", data.getLabel());
            startIntent.putExtra("type", "R");
            getActivity().startActivity(startIntent);
            //onClick(view);
            return super.onSingleTapConfirmed(e);
        }

        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
        }
    }
}
