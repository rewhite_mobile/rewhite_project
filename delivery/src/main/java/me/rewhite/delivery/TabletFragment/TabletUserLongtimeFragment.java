package me.rewhite.delivery.TabletFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.TabletUserListItem;
import me.rewhite.delivery.adapter.TabletUserManageLongtimeListViewAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabletUserLongtimeFragment extends Fragment {

    private ViewGroup mView = null;
    AQuery aq;
    JSONArray usersArr;

    public TabletUserLongtimeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_tablet_users_list, container, false);
        aq = new AQuery(mView);
        //aq.id(R.id.result).text(mActivity.storeInfo.toString());

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.title_01).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.title_02).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.title_03).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.title_04).getTextView());

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.empty_desc).getTextView());


        aq.id(R.id.title_04).text("보관일");

        initialize();

        return mView;
    }

    ListView userListView;
    private TabletUserManageLongtimeListViewAdapter userAdapter;
    private ArrayList<TabletUserListItem> userArrayData;

    private void initialize(){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("listType", "B");
        params.put("page", 1);
        params.put("block", 1000);
        params.put("k", 1);
        NetworkClient.post(Constants.USER_SEARCH, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.USER_SEARCH, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.USER_SEARCH, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        usersArr = jsondata.getJSONArray("data");

                        if(usersArr.length() > 0){
                            aq.id(R.id.empty_desc).gone();

                            JSONObject topObj = usersArr.getJSONObject(0);
                            userListView = aq.id(R.id.listView).getListView();// (ListView)findViewById(R.id.listView);
                            userArrayData = new ArrayList<>();

                            for (int i = 0; i < usersArr.length(); i++) {

                                String userName = usersArr.getJSONObject(i).getString("userName");
                                String address1 = usersArr.getJSONObject(i).getString("address1");
                                String address2 = usersArr.getJSONObject(i).getString("address2");
                                String phone = usersArr.getJSONObject(i).getString("phone");
                                String imageThumbPath = usersArr.getJSONObject(i).getString("imageThumbPath");
                                String jsonString = usersArr.getJSONObject(i).toString();

                                //String noticeId, String noticeType, String title, long registerDateApp, String RowNumber, String noticeTypeTitle, String noticeUrl
                                TabletUserListItem aItem = new TabletUserListItem(userName, address1, address2, phone, imageThumbPath, jsonString);
                                userArrayData.add(aItem);

                            }

                            aq.id(R.id.list_count).text(""+userArrayData.size());

                            userAdapter = new TabletUserManageLongtimeListViewAdapter(getActivity(), R.layout.tablet_user_list_item, userArrayData);
                            userAdapter.notifyDataSetChanged();

                            userListView.setAdapter(userAdapter);
                        }else{
                            aq.id(R.id.empty_desc).visible();
                        }

                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

}
