package me.rewhite.delivery.TabletFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.PriceExpandableAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.tablet.TabletPriceSettingActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabletAdditionalPriceTableFragment extends Fragment {

    private ViewGroup mView = null;
    AQuery aq;
    TabletPriceSettingActivity mActivity;
    JSONObject data;
    ExpandableListView list;

    public TabletAdditionalPriceTableFragment() {
        // Required empty public constructor
    }

    public void refreshListView(int groupPosition, int childPosition, int storePrice1, int storePrice2, int storePrice3, int storePrice4, String isUse){
        try {
            priceArray.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).put("storePrice1",storePrice1);
            priceArray.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).put("storePrice2",storePrice2);
            priceArray.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).put("storePrice3",storePrice3);
            priceArray.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).put("storePrice4",storePrice4);
            priceArray.getJSONObject(groupPosition).getJSONArray("items").getJSONObject(childPosition).put("isUse",isUse);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_tablet_price_table, container, false);
        mActivity = (TabletPriceSettingActivity)getActivity();

        aq = new AQuery(mView);

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.desc01).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.desc02).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.desc03).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.desc04).getTextView());

        LinearLayout ll = (LinearLayout)mView.findViewById(R.id.title_area);
        if("Y".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.IS_VISIT_PRICE))){
            ll.setWeightSum(6f);
        }else{
            aq.id(R.id.desc04).gone();
            ll.setWeightSum(5f);
        }

        list = (ExpandableListView)mView.findViewById(R.id.list);
        list.setOnChildClickListener(new ExpandableListView.OnChildClickListener(){

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Log.w("onChildClick", groupPosition + " / " + childPosition);
                Toast.makeText(getActivity(), "c click = " + childPosition,
                        Toast.LENGTH_SHORT).show();
                return false;
            }
        });

//        // 그룹이 닫힐 경우 이벤트
//        list.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//            @Override
//            public void onGroupCollapse(int groupPosition) {
//                Toast.makeText(getActivity(), "g Collapse = " + groupPosition,
//                        Toast.LENGTH_SHORT).show();
//                //list.getExpandableListAdapter().getGroup(groupPosition)
//            }
//        });
//
//        // 그룹이 열릴 경우 이벤트
//        list.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//            @Override
//            public void onGroupExpand(int groupPosition) {
//                Toast.makeText(getActivity(), "g Expand = " + groupPosition,
//                        Toast.LENGTH_SHORT).show();
//            }
//        });

        initialize();

        return mView;
    }

    PriceExpandableAdapter adapter;
    JSONArray priceArray;

    public void initialize(){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("mode", "Z");
        params.put("k", 1);
        NetworkClient.post(Constants.GET_PRICE_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GET_PRICE_LIST, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_PRICE_LIST, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        priceArray = jsondata.getJSONArray("data");
                        list = (ExpandableListView)mView.findViewById(R.id.list);
                        adapter = new PriceExpandableAdapter(getActivity(), priceArray);
                        list.setAdapter(adapter);

                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

}
