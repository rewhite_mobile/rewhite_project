package me.rewhite.delivery.TabletFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import me.gujun.android.taggroup.TagGroup;
import me.rewhite.delivery.R;
import me.rewhite.delivery.util.CommonUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabletSalesInfoFragment extends Fragment {

    private ViewGroup mView = null;
    AQuery aq;
//    TabletStoreInfoActivity mActivity;
    JSONObject data;

    public TabletSalesInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_tablet_sales_info, container, false);

        if(getArguments() != null){
            String storeInfoString = getArguments().getString("storeInfo");
            try {
                data = new JSONObject(storeInfoString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

//        mActivity = (TabletStoreInfoActivity)getActivity();
        //data = mActivity.storeInfo;

        aq = new AQuery(mView);
        //aq.id(R.id.result).text(mActivity.storeInfo.toString());

        initialize();

        return mView;
    }

    private void initialize(){
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.title_01).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.title_02).getTextView());

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_01).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_02).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_03).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_04).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_05).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_06).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_07).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_08).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_09).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_10).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_11).getTextView());


        //availableOrder : 10111
        try {
            String availableOrder = data.getString("availableOrder");
            String availableOrderString = "";
            for(int i = 0 ; i < availableOrder.length(); i++){
                switch(i){
                    case 0:
                        if("1".equals(Character.toString(availableOrder.charAt(i)))){
                            availableOrderString += "일반";
                        }
                        break;
                    case 1:
                        if("1".equals(Character.toString(availableOrder.charAt(i)))){
                            availableOrderString += ", 택배";
                        }
                        break;
                    case 2:
                        if("1".equals(Character.toString(availableOrder.charAt(i)))){
                            availableOrderString += ", 수선";
                        }
                        break;
                    case 3:
                        if("1".equals(Character.toString(availableOrder.charAt(i)))){
                            availableOrderString += ", 익일";
                        }
                        break;
                    case 4:
                        if("1".equals(Character.toString(availableOrder.charAt(i)))){
                            availableOrderString += ", 당일";
                        }
                        break;
                }
            }

            aq.id(R.id.text_store_available).text(availableOrderString);
            aq.id(R.id.text_washingdays).text(data.getString("washingDays") + " 일");

            String cDay = data.getString("storeClosingDay");
            String[] cDayArr = cDay.split("\\|");
            String cDayString = "";
            if("every".equals(cDayArr[0])){
                cDayString = "매주 ";
            }else if("odd".equals(cDayArr[0])){
                cDayString = "1,3주 ";
            }else if("even".equals(cDayArr[0])){
                cDayString = "2,4주 ";
            }

            String yoilStr = "";

            for(int i = 0; i < cDayArr[1].length(); i++){
                if("1".equals(Character.toString(cDayArr[1].charAt(i)))){
                    if(!"".equals(yoilStr)){
                        yoilStr += ",";
                    }
                    switch (i){
                        case 0:
                            yoilStr += "월";
                            break;
                        case 1:
                            yoilStr += "화";
                            break;
                        case 2:
                            yoilStr += "수";
                            break;
                        case 3:
                            yoilStr += "목";
                            break;
                        case 4:
                            yoilStr += "금";
                            break;
                        case 5:
                            yoilStr += "토";
                            break;
                        case 6:
                            yoilStr += "일";
                            break;
                    }
                }
            }

            aq.id(R.id.text_closing_day).text(cDayString + yoilStr + " 요일");
            aq.id(R.id.text_business_hour).text(data.getString("storeBusinessHours"));
            aq.id(R.id.text_business_hour_sat).text(data.getString("storeBusinessHoursSat"));
            //aq.id(R.id.text_dayoff).text();

            String[] dayOffs = data.getString("storeDayoff").split(",");
            TagGroup mTagGroup = (TagGroup) mView.findViewById(R.id.dayoff_tag_group);
            mTagGroup.setTags(dayOffs);

            int duration = Integer.parseInt(data.getString("storeDeliveryTime").split("\\|")[0]);

            aq.id(R.id.text_duration).text(duration + " 시간");

            if("0".equals(data.getString("storeDeliveryCount"))){
                aq.id(R.id.text_max_unit).text("제한없음");
            }else{
                aq.id(R.id.text_max_unit).text(data.getString("storeDeliveryCount") + " 건");
            }

            int startAppend = Integer.parseInt(data.getString("storeDeliveryTime").split("\\|")[1]);
            String timetable = data.getString("storeDeliveryTime").split("\\|")[2];

            String tTable = "";
            for(int i = 0 ; i < timetable.length(); i++){
                if("1".equals(Character.toString(timetable.charAt(i)))){
                    if("".equals(tTable)){
                        tTable += ((6 + startAppend) + duration*i) + "시~" + ((6 + startAppend) + duration*(i+1)) +"시";
                    }else{
                        tTable += ","+((6 + startAppend) + duration*i) + "시~" + ((6 + startAppend) + duration*(i+1)) +"시";
                    }
                }
            }

            TagGroup deliveryGroup = (TagGroup) mView.findViewById(R.id.delivery_tag_group);
            deliveryGroup.setTags(tTable.split(","));

            String timetable2 = data.getString("storeDeliveryTimeSat").split("\\|")[2];
            String tTable2 = "";
            for(int i = 0 ; i < timetable2.length(); i++){
                if("1".equals(Character.toString(timetable2.charAt(i)))){
                    if("".equals(tTable2)){
                        tTable2 += ((6 + startAppend) + duration*i) + "시~" + ((6 + startAppend) + duration*(i+1)) +"시";
                    }else{
                        tTable2 += ","+((6 + startAppend) + duration*i) + "시~" + ((6 + startAppend) + duration*(i+1)) +"시";
                    }
                }
            }
            TagGroup deliverySatGroup = (TagGroup) mView.findViewById(R.id.delivery_sat_tag_group);
            deliverySatGroup.setTags(tTable2.split(","));

            aq.id(R.id.text_service_area).text(data.getString("storeServiceAreaText"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}