package me.rewhite.delivery.TabletFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.rewhite.delivery.R;
import me.rewhite.delivery.adapter.NoticeListItem;
import me.rewhite.delivery.adapter.TabletNoticeListViewAdapter;
import me.rewhite.delivery.common.logic.Constants;
import me.rewhite.delivery.network.NetworkClient;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.SharedPreferencesUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabletFAQFragment extends Fragment {

    private ViewGroup mView = null;
    AQuery aq;
    JSONArray noticeArr;

    public TabletFAQFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_tablet_notice, container, false);
        aq = new AQuery(mView);
        //aq.id(R.id.result).text(mActivity.storeInfo.toString());

        initialize();

        return mView;
    }

    ListView noticeListView;
    private TabletNoticeListViewAdapter noticeAdapter;
    private ArrayList<NoticeListItem> noticeArrayData;

    private void initialize(){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("page", 1);
        params.put("block", 100);
        params.put("k", 1);
        NetworkClient.post(Constants.SHOW_FAQ, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SHOW_FAQ, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.SHOW_FAQ, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        noticeArr = jsondata.getJSONArray("data");

//                        aq.id(R.id.index_id).gone();
//                        aq.id(R.id.text_title).text("<필수공지> "+topObj.getString("title"));
//                        aq.id(R.id.text_date).text(TimeUtil.convertTimestampToString(topObj.getLong("registerDateApp")));

                        if(noticeArr.length() > 0){
                            JSONObject topObj = noticeArr.getJSONObject(0);

                            noticeListView = aq.id(R.id.listView).getListView();// (ListView)findViewById(R.id.listView);
                            noticeArrayData = new ArrayList<>();

                            for (int i = 0; i < noticeArr.length(); i++) {

                                String noticeId = noticeArr.getJSONObject(i).getString("faqId");
                                String noticeType = noticeArr.getJSONObject(i).getString("faqType");
                                String title = noticeArr.getJSONObject(i).getString("title");
                                long registerDateApp = noticeArr.getJSONObject(i).getLong("registerDateApp");
                                String RowNumber = noticeArr.getJSONObject(i).getString("RowNumber");
                                String noticeTypeTitle = noticeArr.getJSONObject(i).getString("faqTypeTitle");
                                String noticeUrl = noticeArr.getJSONObject(i).getString("faqUrl");

                                //String noticeId, String noticeType, String title, long registerDateApp, String RowNumber, String noticeTypeTitle, String noticeUrl
                                NoticeListItem aItem = new NoticeListItem(noticeId, noticeType, title, registerDateApp, RowNumber, noticeTypeTitle, noticeUrl);
                                noticeArrayData.add(aItem);

                            }


                            aq.id(R.id.list_count).text(""+noticeArrayData.size());

                            noticeAdapter = new TabletNoticeListViewAdapter(getActivity(), R.layout.tablet_notice_item, noticeArrayData);
                            noticeAdapter.notifyDataSetChanged();

                            noticeListView.setAdapter(noticeAdapter);
                        }



                    }
                }catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

}
