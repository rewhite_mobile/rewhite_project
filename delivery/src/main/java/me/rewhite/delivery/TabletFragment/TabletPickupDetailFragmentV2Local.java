package me.rewhite.delivery.TabletFragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.tablet.LocalOrderInputActivity;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.DUtil;
import me.rewhite.delivery.util.MZValidator;
import me.rewhite.delivery.util.SharedPreferencesUtility;


public class TabletPickupDetailFragmentV2Local extends Fragment{

    private final static String TAG = "TabletPickupDetailFragmentV2Local";
    private LocalOrderInputActivity mActivity;

    public AQuery aq;
    JSONObject itemData;
    int calculatedPrice = 0;
    int modPrice = 0;
    int repairPrice = 0;
    int additionalPrice = 0;
    int totalPrice = 0;
    String laundry = "";
    String isService = "N";
    String isReLaundry = "N";
    int sp = 0;

    JSONArray pickItems;
    int pickupItemsPosition;

    private ViewGroup mView = null;


    public TabletPickupDetailFragmentV2Local() {
        // Required empty public constructor
    }


    public static TabletPickupDetailFragmentV2Local newInstance(String mainCategoryName, int mainCategoryIndex) {
        TabletPickupDetailFragmentV2Local fragment = new TabletPickupDetailFragmentV2Local();
        Bundle args = new Bundle();
        args.putString("name", mainCategoryName);
        args.putInt("index", mainCategoryIndex);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    int currentBucketIndex;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.tablet_fragment_pickup_item_detail_v2, container, false);

        aq = new AQuery(mView);
        mActivity = (LocalOrderInputActivity)getActivity();

        if (getArguments() != null) {
            if(getArguments().getString("pickupItems") == null){
                currentBucketIndex = mActivity.bucketIndex;
            }else{
                try {
                    pickItems = new JSONArray(getArguments().getString("pickupItems"));
                    pickupItemsPosition = getArguments().getInt("pickupItemsPosition");
                    currentBucketIndex = pickupItemsPosition;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else{
            currentBucketIndex = mActivity.bucketIndex;
        }

        hideOptionsMenu();

        int tagNo = 0;
        try {
            JSONArray data = mActivity.priceArray;
            itemData = data.getJSONObject(mActivity.Step1Index).getJSONArray("items").getJSONObject(mActivity.Step2Index);
            CommonUtility.setTypefaceBoldSetup(aq.id(R.id.title_text).getTextView());
            aq.id(R.id.title_text).text(mActivity.Step2Name);
            tagNo = (mActivity.tagNo+currentBucketIndex)%(int)Math.pow(10, mActivity.tagLength);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Category 표시
        //aq.id(R.id.title_text).text(mActivity.Step1Name + " > " + mActivity.Step2Name);
        //aq.id(R.id.btn_back).clicked(this, "categoryMod");
        aq.id(R.id.btn_category_mod).clicked(this, "closeAction");

        if(tagNo == 0){
            tagNo = tagNo+1;
        }

        // 상품처리 버튼
        aq.id(R.id.pui_detail_btn_step1_01).clicked(this, "step1SelectAction").tag(1);
        aq.id(R.id.pui_detail_btn_step1_02).clicked(this, "step1SelectAction").tag(2);
        aq.id(R.id.pui_detail_btn_step1_03).clicked(this, "step1SelectAction").tag(3);

        // 상세선택 버튼
        aq.id(R.id.pui_detail_btn_step2_01).clicked(this, "step2SelectAction").tag(1);
        aq.id(R.id.pui_detail_btn_step2_02).clicked(this, "step2SelectAction").tag(2);
        aq.id(R.id.pui_detail_btn_step2_03).clicked(this, "step2SelectAction").tag(3);

        aq.id(R.id.pui_detail_btn_step25_01).clicked(this, "step25SelectAction").tag(1);
        aq.id(R.id.pui_detail_btn_step25_02).clicked(this, "step25SelectAction").tag(2);

        // 요금등급 버튼
        try {
            aq.id(R.id.pui_detail_btn_step3_01).clicked(this, "step3SelectAction").tag(1).text("일반\n" + itemData.getInt("storePrice1") + "");
            aq.id(R.id.pui_detail_btn_step3_02).clicked(this, "step3SelectAction").tag(2).text("명품\n" + itemData.getInt("storePrice2") + "");
            aq.id(R.id.pui_detail_btn_step3_03).clicked(this, "step3SelectAction").tag(3).text("아동\n" + itemData.getInt("storePrice3") + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        aq.id(R.id.pui_detail_btn_etc_free).clicked(this, "stepEtcAction").tag(1);
        aq.id(R.id.pui_detail_btn_etc_retry).clicked(this, "stepEtcAction").tag(2);

        aq.id(R.id.pui_detail_btn_additional).clicked(this, "additionalSelectAction");
        aq.id(R.id.pui_detail_btn_options).clicked(this, "optionsSelectAction");

        aq.id(R.id.pui_detail_btn_colors).clicked(this, "selectColorPick");
        aq.id(R.id.pui_btn_color_mod).clicked(this, "selectColorPick");

        aq.id(R.id.pui_detail_btn_step4_01).clicked(this, "selectRepairAction");
        aq.id(R.id.icon_step1).image(R.drawable.pui_circle_yellow);

        aq.id(R.id.pui_detail_btn_add_pics).clicked(this, "picsAddAction");

        aq.id(R.id.btn_price_mod).clicked(this, "priceModAction");
        aq.id(R.id.btn_submit).clicked(this, "submitAction");

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.tag_label).getTextView());
        aq.id(R.id.tag_label).text(MZValidator.getTagFormmater(tagNo, mActivity.tagLength));
        
        if(mActivity.pickupItems != null){
            if(mActivity.pickupItems.length() > 0 && currentBucketIndex < mActivity.pickupItems.length()){
                try {
                    setPickedDataArrange();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.element_title_area_01).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.element_title_area_02).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.element_title_area_03).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.element_title_area_04).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.element_title_area_05).getTextView());

        return  mView;
    }

    public void priceModAction(View button){
        if("Y".equals(isService) || "Y".equals(isReLaundry)){
            DUtil.alertShow(getActivity(), "무료 또는 재 세탁 품목으로 요금 조정이 불가능합니다.");
        }else{
            mActivity.pickedPrice = totalPrice;
            mActivity.selectPricePicker();

        }

    }

    public void setPickedDataArrange() throws JSONException {
        int step1selectindex = 0;

        JSONObject pickedCurrentItems = mActivity.pickupItems.getJSONObject(currentBucketIndex);
        sp = pickedCurrentItems.getInt("sp");
        laundry = pickedCurrentItems.getString("laundry");
        totalPrice = pickedCurrentItems.getInt("confirmPrice");
        aq.id(R.id.icon_step1).image(R.drawable.pui_circle);
        // Step1
        if(pickedCurrentItems.getJSONArray("repair").length() > 0){
            if(!"".equals(laundry)){
                // 세탁수선
                aq.id(R.id.clean_type_area).visible();
                aq.id(R.id.element_must_repair).visible();
                aq.id(R.id.price_type_area).visible();
                aq.id(R.id.pui_detail_btn_step1_03).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                step1selectindex = 3;
            }else{
                // 수선
                aq.id(R.id.element_must_repair).visible();
                aq.id(R.id.clean_type_area).gone();
                aq.id(R.id.price_type_area).visible();
                aq.id(R.id.pui_detail_btn_step1_02).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                step1selectindex = 2;
            }
        }else{
            // 세탁
            aq.id(R.id.clean_type_area).visible();
            aq.id(R.id.element_must_repair).gone();
            aq.id(R.id.price_type_area).visible();
            aq.id(R.id.pui_detail_btn_step1_01).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
            step1selectindex = 1;
        }
        if(step1selectindex == 1 || step1selectindex == 3){
            // Step2
            if("드라이".equals(pickedCurrentItems.getString("laundry"))){
                aq.id(R.id.pui_detail_btn_step2_01).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
            }else if("물세탁".equals(pickedCurrentItems.getString("laundry"))){
                aq.id(R.id.pui_detail_btn_step2_02).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
            }else if("다림질".equals(pickedCurrentItems.getString("laundry"))){
                aq.id(R.id.pui_detail_btn_step2_03).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
            }
        }

        // Step3
        switch(sp){
            case 1:
                sp = 1;
                calculatedPrice = itemData.getInt("storePrice1");
                aq.id(R.id.pui_detail_btn_step3_01).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                break;
            case 2:
                sp = 2;
                calculatedPrice = itemData.getInt("storePrice2");
                aq.id(R.id.pui_detail_btn_step3_02).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                break;
            case 3:
                sp = 3;
                calculatedPrice = itemData.getInt("storePrice3");
                aq.id(R.id.pui_detail_btn_step3_03).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                break;
        }

        // 수선내역
        if(step1selectindex == 1 || step1selectindex == 3){
            aq.id(R.id.repair_element_label_area).visible();
            JSONArray repairValue = pickedCurrentItems.getJSONArray("repair");
            String addAppendString = "";
            try {
                for(int i =0 ; i < repairValue.length(); i++){
                    if(i == 0){
                        addAppendString = repairValue.getJSONObject(i).getString("itemTitle");
                    }else{
                        addAppendString = addAppendString + ", " + repairValue.getJSONObject(i).getString("itemTitle");
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("partsPickValue", addAppendString);

            aq.id(R.id.pui_detail_repair_element_label).text(addAppendString);

            aq.id(R.id.pui_detail_btn_step4_01).getButton().setText("추가 / 변경하기");
        }
        // Step3

        showOptionsMenu();

        if("Y".equals(mActivity.pickedRelaundry)){
            isService = "N";
            isReLaundry = "Y";
            isRetry = true;
            isFree = false;
            aq.id(R.id.pui_detail_btn_etc_retry).background(R.drawable.pui_button_drawable_pressed_gray).textColor(0xFFFFFFFF);
        }else if("Y".equals(mActivity.pickedService)){
            isService = "Y";
            isReLaundry = "N";
            isRetry = false;
            isFree = true;
            aq.id(R.id.pui_detail_btn_etc_free).background(R.drawable.pui_button_drawable_pressed_gray).textColor(0xFFFFFFFF);
        }

        // Additional
        if(mActivity.pickedAdditionalArray.length() > 0){
            aq.id(R.id.additional_element_label_area).visible();
            String addAppendString = "";
            try {
                for(int i =0 ; i < mActivity.pickedAdditionalArray.length(); i++){
                    if(i == 0){
                        addAppendString = mActivity.pickedAdditionalArray.getJSONObject(i).getString("itemTitle");
                    }else{
                        addAppendString = addAppendString + ", " + mActivity.pickedAdditionalArray.getJSONObject(i).getString("itemTitle");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            aq.id(R.id.pui_additional_element).text(addAppendString);
            aq.id(R.id.pui_detail_btn_additional).getButton().setText("추가 / 변경하기");
        }

        // part
        if(mActivity.pickedPartsArray.length() > 0){
            aq.id(R.id.options_element_label_area).visible();
            String partsAppendString = "";
            try {
                for(int i =0 ; i < mActivity.pickedPartsArray.length(); i++){
                    if(i == 0){
                        partsAppendString = mActivity.pickedPartsArray.getJSONObject(i).getString("partTitle");
                    }else{
                        partsAppendString = partsAppendString + ", " + mActivity.pickedPartsArray.getJSONObject(i).getString("partTitle");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("partsPickValue", partsAppendString);
            aq.id(R.id.pui_element_options).text(partsAppendString);
            aq.id(R.id.pui_detail_btn_options).getButton().setText("추가 / 변경하기");
        }

        // Colorpick
        if(!"".equals(pickedCurrentItems.getString("itemColor"))){
            aq.id(R.id.before_color_area).gone();
            aq.id(R.id.after_color_area).visible();

            Resources res = getResources();
            Bitmap src = BitmapFactory.decodeResource(res, getResources().getIdentifier("pui_item_color_"+pickedCurrentItems.getString("itemColor"), "mipmap", "me.rewhite.delivery" ));
            RoundedBitmapDrawable dr = RoundedBitmapDrawableFactory.create(res, src);
            dr.setCornerRadius(Math.max(src.getWidth(), src.getHeight()) / 10.0f);
            aq.id(R.id.pui_btn_color_picked).getImageView().setImageDrawable(dr);
        }

        // Photo
        if(mActivity.imagePickValue.length() > 0){
            aq.id(R.id.pui_pics_options_area).visible();

            for(int i =0; i < mActivity.imagePickValue.length(); i++){
                String url = mActivity.imagePickValue.getJSONObject(i).getString("photoUrl");
                int resId = getResources().getIdentifier("pics_0"+(i+1), "id", "me.rewhite.delivery");
                ImageView iv = aq.id(resId).getImageView();
                aq.id(iv).image(url);

                int resRemoveId = getResources().getIdentifier("btn_remove_0"+(i+1), "id", "me.rewhite.delivery");
                ImageButton btn = (ImageButton)mView.findViewById(resRemoveId);
                aq.id(btn).image(R.mipmap.pui_btn_image_delete_normal);
            }
            aq.id(R.id.btn_remove_01).clicked(this, "removeImage").tag(1);
            aq.id(R.id.btn_remove_02).clicked(this, "removeImage").tag(2);
            aq.id(R.id.btn_remove_03).clicked(this, "removeImage").tag(3);
        }

        calculatePrice(true);

    }

    public void submitAction(View button){
        JSONObject submitObj = new JSONObject();
        try {
            int itemId = mActivity.priceArray.getJSONObject(mActivity.Step1Index).getJSONArray("items").getJSONObject(mActivity.Step2Index).getInt("itemId");
            submitObj.put("step1index", mActivity.Step1Index);
            submitObj.put("step2index", mActivity.Step2Index);

            submitObj.put("itemId", itemId);
            submitObj.put("rCount", currentBucketIndex);
            submitObj.put("tagId", mActivity.tagNo + currentBucketIndex);
            String itemTitle = mActivity.priceArray.getJSONObject(mActivity.Step1Index).getJSONArray("items").getJSONObject(mActivity.Step2Index).getString("itemTitle");
            submitObj.put("itemTitle", itemTitle);
            submitObj.put("sp", sp);
            submitObj.put("laundryPrice", calculatedPrice);
            submitObj.put("repairPrice", repairPrice);
            submitObj.put("additionalPrice", additionalPrice);
            submitObj.put("isService", isService);
            submitObj.put("isReLaundry", isReLaundry);
            submitObj.put("storeMessage", "");
            submitObj.put("confirmPrice", totalPrice);
            submitObj.put("qty", 1);
            submitObj.put("itemColor", mActivity.colorPickValue);
            submitObj.put("laundry", laundry);

            JSONArray submitRepairArray = new JSONArray();
            for(int i = 0 ; i < mActivity.pickedRepairArray.length(); i++){
                JSONObject repairObj = new JSONObject();
                repairObj.put("itemId", mActivity.pickedRepairArray.getJSONObject(i).getInt("itemId"));
                repairObj.put("itemTitle", mActivity.pickedRepairArray.getJSONObject(i).getString("itemTitle"));
                repairObj.put("itemSP", sp);
                switch(sp){
                    case 1:
                        repairObj.put("itemPrice", mActivity.pickedRepairArray.getJSONObject(i).getInt("storePrice11"));
                        break;
                    case 2:
                        repairObj.put("itemPrice", mActivity.pickedRepairArray.getJSONObject(i).getInt("storePrice12"));
                        break;
                    case 3:
                        repairObj.put("itemPrice", mActivity.pickedRepairArray.getJSONObject(i).getInt("storePrice13"));
                        break;
                }
                //repairObj.put("storePrice11", mActivity.pickedRepairArray.getJSONObject(i).getInt("storePrice11"));
                //repairObj.put("storePrice12", mActivity.pickedRepairArray.getJSONObject(i).getInt("storePrice12"));
                //repairObj.put("storePrice13", mActivity.pickedRepairArray.getJSONObject(i).getInt("storePrice13"));
                submitRepairArray.put(repairObj);
            }
            submitObj.put("repair", submitRepairArray);

            JSONArray submitAdditionalArray = new JSONArray();
            for(int i = 0 ; i < mActivity.pickedAdditionalArray.length(); i++){
                JSONObject addObj = new JSONObject();
                addObj.put("itemId", mActivity.pickedAdditionalArray.getJSONObject(i).getInt("itemId"));
                addObj.put("itemTitle", mActivity.pickedAdditionalArray.getJSONObject(i).getString("itemTitle"));
                addObj.put("itemSP", sp);
                switch(sp){
                    case 1:
                        addObj.put("itemPrice", mActivity.pickedAdditionalArray.getJSONObject(i).getInt("storePrice11"));
                        break;
                    case 2:
                        addObj.put("itemPrice", mActivity.pickedAdditionalArray.getJSONObject(i).getInt("storePrice12"));
                        break;
                    case 3:
                        addObj.put("itemPrice", mActivity.pickedAdditionalArray.getJSONObject(i).getInt("storePrice13"));
                        break;
                }
                //addObj.put("storePrice11", mActivity.pickedAdditionalArray.getJSONObject(i).getInt("storePrice11"));
                //addObj.put("storePrice12", mActivity.pickedAdditionalArray.getJSONObject(i).getInt("storePrice12"));
                //addObj.put("storePrice13", mActivity.pickedAdditionalArray.getJSONObject(i).getInt("storePrice13"));
                submitAdditionalArray.put(addObj);
            }
            submitObj.put("additional", submitAdditionalArray);

            JSONArray submitPartsArray = new JSONArray();
            for(int i = 0 ; i < mActivity.pickedPartsArray.length(); i++){
                JSONObject partObj = new JSONObject();
                partObj.put("partId", mActivity.pickedPartsArray.getJSONObject(i).getInt("partId"));
                partObj.put("partTitle", mActivity.pickedPartsArray.getJSONObject(i).getString("partTitle"));
                submitPartsArray.put(partObj);
            }
            submitObj.put("part", submitPartsArray);
            if(mActivity.imagePickValue == null){
                mActivity.imagePickValue = new JSONArray();
            }
            submitObj.put("photo", mActivity.imagePickValue);

            mActivity.submitElement(submitObj, mActivity.tagNo+currentBucketIndex, currentBucketIndex);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void setImagePicsValue(String _addImageURL){
        try {
            JSONObject obj = new JSONObject();
            obj.put("photoUrl", _addImageURL);

            if(mActivity.imagePickValue == null){
                mActivity.imagePickValue = new JSONArray();
            }
            mActivity.imagePickValue.put(obj);

            aq.id(R.id.pui_pics_options_area).visible();

            for(int i =0; i < mActivity.imagePickValue.length(); i++){
                String url = mActivity.imagePickValue.getJSONObject(i).getString("photoUrl");
                int resId = getResources().getIdentifier("pics_0"+(i+1), "id", "me.rewhite.delivery");
                ImageView iv = aq.id(resId).getImageView();
                aq.id(iv).image(url);

                int resRemoveId = getResources().getIdentifier("btn_remove_0"+(i+1), "id", "me.rewhite.delivery");
                ImageButton btn = (ImageButton)mView.findViewById(resRemoveId);
                aq.id(btn).image(R.mipmap.pui_btn_image_delete_normal);
            }
            aq.id(R.id.btn_remove_01).clicked(this, "removeImage").tag(1);
            aq.id(R.id.btn_remove_02).clicked(this, "removeImage").tag(2);
            aq.id(R.id.btn_remove_03).clicked(this, "removeImage").tag(3);

//            final ScrollView sv = (ScrollView) getActivity().findViewById(R.id.scrollView3);
//
//            Handler h = new Handler();
//
//            h.postDelayed(new Runnable() {
//
//                @Override
//                public void run() {
//                    sv.fullScroll(View.FOCUS_DOWN);
//                }
//            }, 250); // 250 ms delay

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("imagePicks added", mActivity.imagePickValue.toString());
    }

    private JSONArray removePosition(int position, JSONArray data) throws JSONException {
        JSONArray temp = new JSONArray();
        for(int i = 0; i < data.length(); i++){
            if(position != i){
                temp.put(data.getJSONObject(i));
            }
        }
        return temp;
    }

    public void removeImage(View button){
        int tag = (int)button.getTag();
        try {

            switch (tag){
                case 1:
                    if(mActivity.imagePickValue.length() > 0){
                        mActivity.imagePickValue = removePosition(0, mActivity.imagePickValue);
                    }
                    break;
                case 2:
                    if(mActivity.imagePickValue.length() > 1){
                        mActivity.imagePickValue = removePosition(1, mActivity.imagePickValue);
                    }
                    break;
                case 3:
                    if(mActivity.imagePickValue.length() > 2){
                        mActivity.imagePickValue = removePosition(2, mActivity.imagePickValue);
                    }
                    break;
            }

            if(mActivity.imagePickValue.length() == 0){
                aq.id(R.id.pui_pics_options_area).gone();
            }else{
                aq.id(R.id.pui_pics_options_area).visible();
                aq.id(R.id.pics_01).image(R.mipmap.pui_image_background);
                aq.id(R.id.pics_02).image(R.mipmap.pui_image_background);
                aq.id(R.id.pics_03).image(R.mipmap.pui_image_background);

                aq.id(R.id.btn_remove_01).image(R.mipmap.pui_btn_image_delete_disabled);
                aq.id(R.id.btn_remove_02).image(R.mipmap.pui_btn_image_delete_disabled);
                aq.id(R.id.btn_remove_03).image(R.mipmap.pui_btn_image_delete_disabled);

                for(int i =0; i < mActivity.imagePickValue.length(); i++){
                    String url = mActivity.imagePickValue.getJSONObject(i).getString("photoUrl");
                    int resId = getResources().getIdentifier("pics_0"+(i+1), "id", "me.rewhite.delivery");
                    ImageView iv = aq.id(resId).getImageView();
                    aq.id(iv).image(url);

                    int resRemoveId = getResources().getIdentifier("btn_remove_0"+(i+1), "id", "me.rewhite.delivery");
                    ImageButton btn = (ImageButton)mView.findViewById(resRemoveId);
                    aq.id(btn).image(R.mipmap.pui_btn_image_delete_normal);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public void picsAddAction(View button){

        if(mActivity.imagePickValue != null){
            if(mActivity.imagePickValue.length() < 3){
                try{
                    if ( Build.VERSION.SDK_INT >= 23){
                        if (!mActivity.canAccessStorage()) {
                            requestPermissions(mActivity.STORAGE_PERMS, mActivity.STORAGE_REQUEST);
                        }else{
                            mActivity.addIntentCall();
                        }
                    }else{
                        mActivity.addIntentCall();
                    }
                }
                catch(NullPointerException ex){
                    mActivity.addIntentCall();
                }
            }else{
                DUtil.alertShow(getActivity(), "사진 3장만 첨부 가능합니다.\n" +
                        "등록하신 사진 삭제 후에 \n" +
                        "다시 추가해주세요.");
            }
        }else{
            try{
                if ( Build.VERSION.SDK_INT >= 23){
                    if (!mActivity.canAccessStorage()) {
                        requestPermissions(mActivity.STORAGE_PERMS, mActivity.STORAGE_REQUEST);
                    }else{
                        mActivity.addIntentCall();
                    }
                }else{
                    mActivity.addIntentCall();
                }
            }
            catch(NullPointerException ex){
                mActivity.addIntentCall();
            }
        }


    }


    public void setColorPickValue(String colorValue){
        mActivity.colorPickValue = colorValue;
        aq.id(R.id.before_color_area).gone();
        aq.id(R.id.after_color_area).visible();

        Resources res = getResources();
        Bitmap src = BitmapFactory.decodeResource(res, getResources().getIdentifier("pui_item_color_"+colorValue, "mipmap", "me.rewhite.delivery" ));
        RoundedBitmapDrawable dr = RoundedBitmapDrawableFactory.create(res, src);
        dr.setCornerRadius(Math.max(src.getWidth(), src.getHeight()) / 10.0f);
        aq.id(R.id.pui_btn_color_picked).getImageView().setImageDrawable(dr);
    }

    public void setPricePickValue(int price){
        totalPrice = price;
        mActivity.pickedPrice = price;
        aq.id(R.id.total_price_label).text(MZValidator.toNumFormat(totalPrice));
    }

    public void setPartsPickValue(JSONArray partsValue){
        mActivity.pickedPartsArray = partsValue;

        if(partsValue.length() > 0){
            aq.id(R.id.options_element_label_area).visible();
            String partsAppendString = "";
            try {
                for(int i =0 ; i < partsValue.length(); i++){
                    if(i == 0){
                        partsAppendString = partsValue.getJSONObject(i).getString("partTitle");
                    }else{
                        partsAppendString = partsAppendString + ", " + partsValue.getJSONObject(i).getString("partTitle");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("partsPickValue", partsAppendString);
            aq.id(R.id.pui_element_options).text(partsAppendString);
            aq.id(R.id.pui_detail_btn_options).getButton().setText("추가 / 변경하기");
        }else{
            aq.id(R.id.options_element_label_area).gone();
            aq.id(R.id.pui_detail_btn_options).getButton().setText("선택하기");
        }

    }

    public void setAdditionalPickValue(JSONArray additionalValue){
        mActivity.pickedAdditionalArray = additionalValue;

        if(additionalValue.length() > 0){
            aq.id(R.id.additional_element_label_area).visible();
            String addAppendString = "";
            try {
                for(int i =0 ; i < additionalValue.length(); i++){
                    if(i == 0){
                        addAppendString = additionalValue.getJSONObject(i).getString("itemTitle");
                    }else{
                        addAppendString = addAppendString + ", " + additionalValue.getJSONObject(i).getString("itemTitle");
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("additionalPickValue", addAppendString);

            aq.id(R.id.pui_additional_element).text(addAppendString);
            calculatePrice(false);

            aq.id(R.id.pui_detail_btn_additional).getButton().setText("추가 / 변경하기");
        }else{
            calculatePrice(false);
            aq.id(R.id.additional_element_label_area).gone();
            aq.id(R.id.pui_detail_btn_additional).getButton().setText("선택하기");
        }
    }

    public void setRepairPickValue(JSONArray repairValue){
        mActivity.pickedRepairArray = repairValue;

        if(repairValue.length() > 0){
            aq.id(R.id.repair_element_label_area).visible();
            String addAppendString = "";
            try {
                for(int i =0 ; i < repairValue.length(); i++){
                    if(i == 0){
                        addAppendString = repairValue.getJSONObject(i).getString("itemTitle");
                    }else{
                        addAppendString = addAppendString + ", " + repairValue.getJSONObject(i).getString("itemTitle");
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("repairPickValue", addAppendString);

            aq.id(R.id.pui_detail_repair_element_label).text(addAppendString);
            calculatePrice(false);

            aq.id(R.id.pui_detail_btn_step4_01).getButton().setText("추가 / 변경하기");

            aq.id(R.id.icon_step4).image(R.drawable.pui_circle);
            showOptionsMenu();


        }else{
            calculatePrice(false);
            aq.id(R.id.repair_element_label_area).gone();
            aq.id(R.id.pui_detail_btn_step4_01).getButton().setText("선택하기");
        }


    }

    int isStep1ChoiceItem = 0;
    int isStep2ChoiceItem = 0;
    int isStep3ChoiceItem = 0;

    //
    public void selectRepairAction(View button){
        mActivity.selectRepairPicker();


    }

    public void selectColorPick(View button){
        mActivity.selectColorPicker();
    }

    public void optionsSelectAction(View button){
        mActivity.selectPartsPicker();
    }

    public void additionalSelectAction(View button){
        // 추가항목 추가시 선택항목 추가
        mActivity.selectAdditionalPicker();
    }

    public void step1SelectAction(View button){
        int tag = (int)button.getTag();
        isStep1ChoiceItem = tag;

        aq.id(R.id.icon_step1).image(R.drawable.pui_circle);


        aq.id(R.id.pui_detail_btn_step1_01).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step1_02).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step1_03).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);

        switch(tag){
            case 1:
                aq.id(R.id.icon_step2).image(R.drawable.pui_circle_yellow);
                aq.id(R.id.clean_type_area).visible();
                aq.id(R.id.price_type_area).gone();
                aq.id(R.id.pui_detail_btn_step1_01).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                aq.id(R.id.element_must_repair).gone();
                break;
            case 2:
                aq.id(R.id.icon_step3).image(R.drawable.pui_circle_yellow);
                aq.id(R.id.icon_step4).image(R.drawable.pui_circle_yellow);
                aq.id(R.id.clean_type_area).gone();
                aq.id(R.id.price_type_area).visible();
                aq.id(R.id.element_must_repair).visible();
                aq.id(R.id.pui_detail_btn_step1_02).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                break;
            case 3:
                aq.id(R.id.icon_step2).image(R.drawable.pui_circle_yellow);
                aq.id(R.id.clean_type_area).visible();
                aq.id(R.id.price_type_area).gone();
                aq.id(R.id.element_must_repair).gone();
                aq.id(R.id.pui_detail_btn_step1_03).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                break;
        }

        aq.id(R.id.different_price_type_area).gone();


        isStep2ChoiceItem = 0;
        isStep3ChoiceItem = 0;
        laundry = "";

        aq.id(R.id.pui_detail_btn_step2_01).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step2_02).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step2_03).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);

        aq.id(R.id.pui_detail_btn_step25_01).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step25_02).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);

        sp = 0;
        calculatedPrice = 0;
        totalPrice = 0;
        additionalPrice = 0;
        aq.id(R.id.pui_detail_btn_step3_01).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step3_02).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step3_03).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);

        mActivity.pickedRepairArray = new JSONArray();
        repairPrice = 0;

        calculatePrice(false);

        hideOptionsMenu();
    }

    public void step2SelectAction(View button){
        int tag = (int)button.getTag();
        isStep2ChoiceItem = tag;

        aq.id(R.id.icon_step2).image(R.drawable.pui_circle);

        aq.id(R.id.pui_detail_btn_step2_01).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step2_02).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step2_03).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);

        aq.id(R.id.pui_detail_btn_step25_01).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step25_02).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);

        isStep3ChoiceItem = 0;
        aq.id(R.id.pui_detail_btn_step3_01).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step3_02).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step3_03).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.price_type_area).gone();

        switch(tag){
            case 1:
                laundry = "드라이";
                aq.id(R.id.pui_detail_btn_step2_01).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                break;
            case 2:
                laundry = "물세탁";
                aq.id(R.id.pui_detail_btn_step2_02).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                break;
            case 3:
                laundry = "다림질";
                aq.id(R.id.pui_detail_btn_step2_03).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                break;
        }

        Log.e("IS_VISIT_PRICE bool", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.IS_VISIT_PRICE));

        if("Y".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.IS_VISIT_PRICE))){
            aq.id(R.id.different_price_type_area).visible();
            aq.id(R.id.icon_step25).image(R.drawable.pui_circle_yellow);

        }else{
            aq.id(R.id.icon_step3).image(R.drawable.pui_circle_yellow);
            aq.id(R.id.price_type_area).visible();
        }


        hideOptionsMenu();

    }

    boolean isVisitOrder = false;

    public void step25SelectAction(View button){
        int tag = (int)button.getTag();

        aq.id(R.id.icon_step25).image(R.drawable.pui_circle);
        aq.id(R.id.icon_step3).image(R.drawable.pui_circle_yellow);

        try {
            switch (tag){
                case 1:
                    isVisitOrder = true;
                    aq.id(R.id.pui_detail_btn_step25_01).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                    aq.id(R.id.pui_detail_btn_step25_02).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);

                    aq.id(R.id.pui_detail_btn_step3_01).text("일반\n" + itemData.getInt("storePrice4") + "");
                    aq.id(R.id.pui_detail_btn_step3_02).text("명품\n" + itemData.getInt("storePrice2") + "");
                    aq.id(R.id.pui_detail_btn_step3_03).text("아동\n" + itemData.getInt("storePrice3") + "");
                    break;
                case 2:
                    isVisitOrder = false;
                    aq.id(R.id.pui_detail_btn_step25_02).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                    aq.id(R.id.pui_detail_btn_step25_01).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);

                    aq.id(R.id.pui_detail_btn_step3_01).text("일반\n" + itemData.getInt("storePrice1") + "");
                    aq.id(R.id.pui_detail_btn_step3_02).text("명품\n" + itemData.getInt("storePrice2") + "");
                    aq.id(R.id.pui_detail_btn_step3_03).text("아동\n" + itemData.getInt("storePrice3") + "");
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        aq.id(R.id.price_type_area).visible();
        hideOptionsMenu();
    }

    public void step3SelectAction(View button){
        int tag = (int)button.getTag();
        isStep3ChoiceItem = tag;
        if(isStep1ChoiceItem == 3 || isStep1ChoiceItem == 2){
            aq.id(R.id.icon_step4).image(R.drawable.pui_circle_yellow);
            aq.id(R.id.element_must_repair).visible();
        }else{
            showOptionsMenu();
        }

        aq.id(R.id.icon_step3).image(R.drawable.pui_circle);

        aq.id(R.id.pui_detail_btn_step3_01).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step3_02).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_step3_03).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);

        try {

            switch(isStep3ChoiceItem){
                case 1:
                    sp = 1;
                    if(isVisitOrder){
                        calculatedPrice = itemData.getInt("storePrice4");
                    }else{
                        calculatedPrice = itemData.getInt("storePrice1");
                    }

                    aq.id(R.id.pui_detail_btn_step3_01).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                    break;
                case 2:
                    sp = 2;
                    calculatedPrice = itemData.getInt("storePrice2");
                    aq.id(R.id.pui_detail_btn_step3_02).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                    break;
                case 3:
                    sp = 3;
                    calculatedPrice = itemData.getInt("storePrice3");
                    aq.id(R.id.pui_detail_btn_step3_03).background(R.drawable.pui_button_drawable_pressed).textColor(0xFFFFFFFF);
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        modPrice = calculatedPrice;
        calculatePrice(false);

    }

    private void calculatePrice(boolean isModfied){


        // 추가요금
        if(mActivity.pickedAdditionalArray.length() > 0) {

            int addAppendPrice = 0;
            try {
                for (int i = 0; i < mActivity.pickedAdditionalArray.length(); i++) {
                    if (sp == 1) {
                        if(isVisitOrder){
                            addAppendPrice += mActivity.pickedAdditionalArray.getJSONObject(i).getInt("storePrice14");
                        }else{
                            addAppendPrice += mActivity.pickedAdditionalArray.getJSONObject(i).getInt("storePrice11");
                        }
                    } else if (sp == 2) {
                        addAppendPrice += mActivity.pickedAdditionalArray.getJSONObject(i).getInt("storePrice12");
                    } else if (sp == 3) {
                        addAppendPrice += mActivity.pickedAdditionalArray.getJSONObject(i).getInt("storePrice13");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            additionalPrice = addAppendPrice;
        }else{
            additionalPrice = 0;
        }

        // 수선요금
        if(mActivity.pickedRepairArray.length() > 0) {

            int addRepairPrice = 0;
            try {
                for (int i = 0; i < mActivity.pickedRepairArray.length(); i++) {
                    if (sp == 1) {
                        if(isVisitOrder){
                            addRepairPrice += mActivity.pickedRepairArray.getJSONObject(i).getInt("storePrice14");
                        }else{
                            addRepairPrice += mActivity.pickedRepairArray.getJSONObject(i).getInt("storePrice11");
                        }

                    } else if (sp == 2) {
                        addRepairPrice += mActivity.pickedRepairArray.getJSONObject(i).getInt("storePrice12");
                    } else if (sp == 3) {
                        addRepairPrice += mActivity.pickedRepairArray.getJSONObject(i).getInt("storePrice13");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            repairPrice = addRepairPrice;
        }else{
            repairPrice = 0;
        }

        if(!isModfied){
            if(modPrice == calculatedPrice){
                if(isFree || isRetry){
                    totalPrice = 0;
                }else{

                    if (isStep1ChoiceItem == 1) {
                        totalPrice = calculatedPrice + additionalPrice;
                    } else if (isStep1ChoiceItem == 2) {
                        totalPrice = additionalPrice + repairPrice;
                    } else if (isStep1ChoiceItem == 3) {
                        totalPrice = calculatedPrice + additionalPrice + repairPrice;
                    }
                }
            }else{
                if(isFree || isRetry){
                    totalPrice = 0;
                }else{
                    //totalPrice =  + additionalPrice + repairPrice;
                    if (isStep1ChoiceItem == 1) {
                        totalPrice = modPrice + additionalPrice;
                    } else if (isStep1ChoiceItem == 2) {
                        totalPrice = additionalPrice + repairPrice;
                    } else if (isStep1ChoiceItem == 3) {
                        totalPrice = modPrice + additionalPrice + repairPrice;
                    }
                }

            }
        }

        aq.id(R.id.pui_additional_price).text(MZValidator.toNumFormat(additionalPrice));
        aq.id(R.id.pui_repair_price).text(MZValidator.toNumFormat(repairPrice));
        aq.id(R.id.total_price_label).text(MZValidator.toNumFormat(totalPrice));
    }

    boolean isFree = false;
    boolean isRetry = false;

    public void stepEtcAction(View button){
        int tag = (int)button.getTag();
        aq.id(R.id.pui_detail_btn_etc_free).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        aq.id(R.id.pui_detail_btn_etc_retry).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
        switch(tag) {
            case 1:
                isFree = !isFree;
                if (isFree) {
                    isService = "Y";
                    isReLaundry = "N";
                    isRetry = false;
                    aq.id(R.id.pui_detail_btn_etc_free).background(R.drawable.pui_button_drawable_pressed_gray).textColor(0xFFFFFFFF);

                    calculatePrice(false);
                } else {
                    isService = "N";
                    isReLaundry = "N";
                    aq.id(R.id.pui_detail_btn_etc_free).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
                    calculatePrice(false);
                }

                break;
            case 2:
                isRetry = !isRetry;
                if (isRetry) {
                    isService = "N";
                    isReLaundry = "Y";
                    isFree = false;
                    aq.id(R.id.pui_detail_btn_etc_retry).background(R.drawable.pui_button_drawable_pressed_gray).textColor(0xFFFFFFFF);

                    calculatePrice(false);
                } else {
                    isService = "N";
                    isReLaundry = "N";
                    aq.id(R.id.pui_detail_btn_etc_retry).background(R.drawable.pui_button_drawable_normal).textColor(0xFF777777);
                    calculatePrice(false);
                }

                break;
        }
    }

    private void hideOptionsMenu(){
        aq.id(R.id.sep_line).gone();
        aq.id(R.id.option_area).gone();
        aq.id(R.id.total_price_area).gone();
        aq.id(R.id.blank_area).gone();
    }

    private void showOptionsMenu(){
        aq.id(R.id.sep_line).visible();
        aq.id(R.id.option_area).visible();
        aq.id(R.id.total_price_area).visible();
        aq.id(R.id.blank_area).visible();
/*
        final ScrollView sv = (ScrollView) getActivity().findViewById(R.id.scrollView3);

        Handler h = new Handler();

        h.postDelayed(new Runnable() {

            @Override
            public void run() {
                if(sv != null){
                    if(sv.getMaxScrollAmount() >= 500){
                        sv.scrollTo(0, 500);
                    }else{
                        sv.scrollTo(0, sv.getMeasuredHeight());
                    }
                }

            }
        }, 100); // 250 ms delay*/
    }

    public void categoryMod(View button){
        getActivity().onBackPressed();
    }

    public void closeAction(View button){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setCancelable(true).setMessage("세탁물 등록이 완료되지 않았습니다.\n중단하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mActivity.showFragment(LocalOrderInputActivity.FRAGMENT_BLANK);
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }


}
