package me.rewhite.delivery.TabletFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.delivery.R;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.MZValidator;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabletStoreInfoFragment extends Fragment {

    private ViewGroup mView = null;
    AQuery aq;
    //TabletStoreInfoActivity mActivity;
    JSONObject data;

    public TabletStoreInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_tablet_store_info, container, false);

        if(getArguments() != null){
            String storeInfoString = getArguments().getString("storeInfo");
            try {
                data = new JSONObject(storeInfoString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //mActivity = (TabletStoreInfoActivity)getActivity();

        aq = new AQuery(mView);
        initialize();

        return mView;
    }

    private void initialize(){

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.title_01).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.title_02).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.title_03).getTextView());

        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_01).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_02).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_03).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_04).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_05).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_06).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_07).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_08).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_09).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_10).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_11).getTextView());
        CommonUtility.setTypefaceBoldSetup(aq.id(R.id.ttl_12).getTextView());

        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_store_name).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_phone).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_address).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_address_detail).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_zipcode).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_owner_name).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_bizno).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_email).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_account_no).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_operator_name).getTextView());
        CommonUtility.setTypefaceNormalSetup(aq.id(R.id.text_operator_phone).getTextView());

        try {
            aq.id(R.id.text_store_name).text(data.getString("storeName"));
            aq.id(R.id.text_phone).text(MZValidator.validTelNumber(data.getString("storeTelephone")));
            aq.id(R.id.text_address).text(data.getString("storeAddress1"));
            aq.id(R.id.text_address_detail).text(data.getString("storeAddress2"));
            aq.id(R.id.text_zipcode).text(data.getString("storeZipcode"));

            aq.id(R.id.text_owner_name).text(data.getString("masterName"));
            aq.id(R.id.text_bizno).text(data.getString("businessRegistrationNumber"));
            aq.id(R.id.text_email).text(data.getString("masterEmail"));
            aq.id(R.id.text_account_no).text(data.getString("accountBankNumber"));
            aq.id(R.id.text_operator_name).text(data.getString("managerName"));
            aq.id(R.id.text_operator_phone).text(MZValidator.validTelNumber(data.getString("managerMobile")));

            String paymentMethod = data.getString("storePaymentMethod");
            String payMethod01 = Character.toString(paymentMethod.charAt(0));
            String payMethod02 = Character.toString(paymentMethod.charAt(1));
            String payMethod03 = Character.toString(paymentMethod.charAt(2));
            if("1".equals(payMethod01)){
                aq.id(R.id.icon_pay_01).image(R.mipmap.text_payment_01_enabled);
            }
            if("1".equals(payMethod02)){
                aq.id(R.id.icon_pay_02).image(R.mipmap.text_payment_02_enabled);
            }
            if("1".equals(payMethod03)){
                aq.id(R.id.icon_pay_03).image(R.mipmap.text_payment_03_enabled);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
