package me.rewhite.delivery.TabletFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import me.rewhite.delivery.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabletUserCouponFragment extends Fragment {

    private ViewGroup mView = null;
    AQuery aq;

    public TabletUserCouponFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup)inflater.inflate(R.layout.fragment_tablet_coupon, container, false);
        aq = new AQuery(mView);
        //aq.id(R.id.result).text(mActivity.storeInfo.toString());


        return mView;
    }


}
