package me.rewhite.delivery;

/**
 * Created by marines on 2015. 10. 14..
 */

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.telephony.PhoneNumberUtils;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import java.io.BufferedInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import me.rewhite.delivery.activity.AlarmActivity;
import me.rewhite.delivery.activity.IntroActivity;
import me.rewhite.delivery.activity.MainActivity;
import me.rewhite.delivery.common.logic.PushWakeLock;
import me.rewhite.delivery.service.CallingService;
import me.rewhite.delivery.util.CommonUtility;
import me.rewhite.delivery.util.SharedPreferencesUtility;


/**
 * IntentService responsible for handling GCM messages.
 */
public class MyFcmListenerService extends GcmListenerService {

    private static final String TAG = "MyFcmListenerService";
    private static int mNotiId;
    private static String mLastState;


    public Context mCtx = this;
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String msg = data.getString("message");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + msg);

        Bundle extras = data;

        if (!extras.isEmpty()) {

            String c2dm_msg = (String)extras.get("message");
            //c2dm_msg = extras.get("message");
            String c2dm_parameter = null;
            c2dm_parameter = (String)extras.get("parameter");

            Log.i(TAG, "new message= " + c2dm_msg);
            Log.i(TAG, "new parameter= " + c2dm_parameter);


            //
            if (c2dm_parameter == null) return;

            // query = synchronize?memo_sync=Y
            Uri u = Uri.parse(c2dm_parameter);

            String queryHost = c2dm_parameter.split("\\?")[0];


            if (queryHost.equals("order")) {
                // silent push : for payment
                // 결제요청에 의한 PUSH처리
                Uri uri = Uri.parse(c2dm_parameter);
                String orderId = uri.getQueryParameter("orderId");
                String pushId = uri.getQueryParameter("pushId");

                Log.i(TAG, "new message= " + c2dm_msg);
                Log.i(TAG, "new type = " + queryHost);
                Log.i(TAG, "new parameter= " + uri.toString());
                Log.i(TAG, "new orderId= " + orderId);

                generateNotification( c2dm_msg);
            } else if (queryHost.equals("rCall")) {

                Uri uri = Uri.parse(c2dm_parameter);
                String userId = uri.getQueryParameter("storeId");
                String sender = uri.getQueryParameter("sender");
                String receiver = uri.getQueryParameter("receiver");
                String kind = uri.getQueryParameter("kind");
                String inner_num = uri.getQueryParameter("inner_num");
                String message = uri.getQueryParameter("message");

                String storeId = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_ID);

                // Tablet 여부
                boolean isTablet = CommonUtility.isTablet(this);
                if(storeId.equals(userId) && isTablet){

                    Log.i(TAG, "tablet rCall= " + userId);

                    String incomingNumber = sender;//intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                    final String phone_number = PhoneNumberUtils.formatNumber(incomingNumber);

                    Intent serviceIntent = new Intent(mCtx, CallingService.class);
                    serviceIntent.putExtra(CallingService.EXTRA_CALL_NUMBER, phone_number);
                    mCtx.startService(serviceIntent);
                }else{

                }

            } else if (queryHost.equals("event")) {
                // 이벤트 알
                // Rich Push Notification for Event Alert
                Uri uri = Uri.parse(c2dm_parameter);
                String eventId = uri.getQueryParameter("eventId");
                String imagePath = uri.getQueryParameter("image");
                String eventTitle = uri.getQueryParameter("title");
                String message = uri.getQueryParameter("message");

                Log.i(TAG, "new c2dm_parameter= " + c2dm_parameter);
                Log.i(TAG, "imagePath = " + imagePath);

                try {
                    URL url = new URL(imagePath.toString());
                    URLConnection conn = url.openConnection();
                    conn.connect();
                    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
                    Bitmap imgBitmap = BitmapFactory.decodeStream(bis);
                    bis.close();
                    loadedImage = imgBitmap;
                }catch (MalformedURLException e) {
                    System.out.println("The URL is not valid.");
                    System.out.println(e.getMessage());
                }catch (Exception e) {
                    Log.e("DEBUGTAG", "Remote Image Exception", e);
                }
                //generateNotification(context, c2dm_msg);
                generateImageNotification( c2dm_msg, eventId, loadedImage, eventTitle, message);

            }else if (queryHost.equals("store")) {
                // 이벤트 알림
                // Rich Push Notification for Event Alert
                Log.i(TAG, "new c2dm_parameter= " + c2dm_parameter);

                generateNotification( c2dm_msg);

            }else if (queryHost.equals("alarm")) {
                // 수거배송 알림

                Uri uri = Uri.parse(c2dm_parameter);
                String deliveryType = uri.getQueryParameter("deliveryType");
                String orderId = uri.getQueryParameter("orderId");
                String userId = uri.getQueryParameter("userId");

                String storeId = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.STORE_ID);

                // Tablet 여부
                boolean isTablet = CommonUtility.isTablet(this);

                if(storeId.equals(userId) && !isTablet){
                    Intent alarmIntent = new Intent(this, AlarmActivity.class);
                    alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    alarmIntent.putExtra("deliveryType", deliveryType);
                    alarmIntent.putExtra("orderId", orderId);
                    startActivity(alarmIntent);

                    PushWakeLock.acquireCpuWakeLock(this);
                }

                // Rich Push Notification for Event Alert
                Log.i(TAG, "new c2dm_parameter= " + c2dm_parameter);

            } else {
                // 정의되지않은 나머지 Push로 앱실행
                Log.i(TAG, "new c2dm_parameter= " + c2dm_parameter);
                generateNotification( "" + c2dm_msg );
            }

            //displayMessage( c2dm_msg);

            // for Push Callback Rate
            Uri uri = Uri.parse(c2dm_parameter);
            String push_id = uri.getQueryParameter("push_id");
            Log.i(TAG, "new push_id= " + push_id);
        }

    }

    protected Bitmap loadedImage;

    private void generateImageNotification(final String msg, final String eventId, final Bitmap _banner, final String title, final String message){

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.icon_line)
                .setLargeIcon(_banner)
                .setContentText(msg);

        NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle(mBuilder);
        bigPicStyle.bigPicture(_banner);
        bigPicStyle.setBigContentTitle(title);
        bigPicStyle.setSummaryText(message);
        mBuilder.setStyle(bigPicStyle);
        mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        Intent resultIntent = new Intent(this, IntroActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(IntroActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(100, mBuilder.build());

    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private void generateNotification(String message) {
        if (!"true".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Command.IS_NOTIFICATION))) {
            return;
        }

        Intent notificationIntent = new Intent(this, IntroActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        // 작은 아이콘 이미지.
        builder.setSmallIcon(R.mipmap.icon_line);
        // 알림이 출력될 때 상단에 나오는 문구.
        //builder.setTicker("미리보기 입니다.");
        // 알림 출력 시간.
        builder.setWhen(System.currentTimeMillis());
        // 알림 제목.
        builder.setContentTitle("리:화이트 Shop+");
        // 프로그래스 바.
        //builder.setProgress(100, 50, false);
        // 알림 내용.
        builder.setContentText(message);
        // 알림시 사운드, 진동, 불빛을 설정 가능.
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
        // 알림 터치시 반응.
        builder.setContentIntent(pendingIntent);
        // 알림 터치시 반응 후 알림 삭제 여부.
        builder.setAutoCancel(true);
        // 우선순위.
        builder.setPriority(Notification.PRIORITY_MAX);

        // 고유ID로 알림을 생성.
        NotificationManager nm = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(123456, builder.build());

        PushWakeLock.acquireCpuWakeLock(this);
    }


    private int checkActivity() {
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> infos = am.getRunningTasks(50);
        for (int i = 0; i < infos.size(); i++) {
            ActivityManager.RunningTaskInfo info = infos.get(i);
            String classname = info.topActivity.getClassName().toString();

            if (classname.equals(MainActivity.class.getName())) {
                return i;
            }
        }
        return -1;
    }
}
