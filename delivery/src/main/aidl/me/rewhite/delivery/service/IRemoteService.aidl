// IRemoteService.aidl
package me.rewhite.delivery.service;

import me.rewhite.delivery.service.IRemoteServiceCallback;
// Declare any non-default types here with import statements

interface IRemoteService {

    boolean registerCallback(IRemoteServiceCallback callback);
    boolean unregisterCallback(IRemoteServiceCallback callback);
}
