// IRemoteServiceCallback.aidl
package me.rewhite.delivery.service;

interface IRemoteServiceCallback {
    oneway void valueChanged(long value);
}
