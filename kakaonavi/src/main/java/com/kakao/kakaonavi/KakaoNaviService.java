package com.kakao.kakaonavi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.kakao.network.ServerProtocol;
import com.kakao.util.helper.CommonProtocol;
import com.kakao.util.helper.SystemInfo;
import com.kakao.util.helper.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 카카오내비 앱을 호출하여 목적지 공유/길찾기를 하기 위한 API.
 * @author kevin.kang
 * Created by kevin.kang on 2016. 8. 26..
 */

public class KakaoNaviService {

    /**
     * 카카오내비 앱의 목적지 공유를 하기 위한 API.
     * @param context 현재 목적지 공유 API를 호출하는 컨텍스트
     * @param params 카카오내비 파라미터
     */
    public static void shareDestination(final Context context, final KakaoNaviParams params) {
        SystemInfo.initialize(context.getApplicationContext());
        Uri uri = buildIntentUri(context, ServerProtocol.NAVI_SHARE_PATH, params);

        Intent intent;
        if (KakaoNaviService.isKakaoNaviInstalled(context.getApplicationContext())) {
            intent = new Intent(Intent.ACTION_VIEW, uri);
            context.startActivity(new Intent(Intent.ACTION_VIEW, buildIntentUri(context, ServerProtocol.NAVI_SHARE_PATH, params)));
        } else {
            intent = new Intent(context, KakaoNaviWebViewActivity.class);
            intent.putExtra(KakaoNaviProtocol.PROPERTY_WEB_URL, uri.toString());
        }
        context.startActivity(intent);
    }

    /**
     * 카카오내비 앱의 길 안내를 실행하기 위한 API.
     * @param context 현재 길 안내 API를 호출하는 컨텍스트
     * @param params 카카오내비 파라미터
     */
    public static void navigate(final Context context, final KakaoNaviParams params) {
        SystemInfo.initialize(context.getApplicationContext());
        Uri uri = buildIntentUri(context.getApplicationContext(), ServerProtocol.NAVI_GUIDE_PATH, params);

        Intent intent;
        if (KakaoNaviService.isKakaoNaviInstalled(context.getApplicationContext())) {
            intent = new Intent(Intent.ACTION_VIEW, uri);
        } else {
            intent = new Intent(context, KakaoNaviWebViewActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(KakaoNaviProtocol.PROPERTY_WEB_URL, uri.toString());
        }
        context.startActivity(intent);
    }

    /**
     * 카카오내비 앱이 설치되어 있는지 확인해 주는 메소드.
     * @param context 앱의 컨텍스트
     * @return true if installed, false otherwise.
     */
    @SuppressWarnings("WeakerAccess")
    public static boolean isKakaoNaviInstalled(final Context context) {
        return Utility.isPackageInstalled(context, KakaoNaviProtocol.NAVI_PACKAGE);
    }


    private static Uri buildIntentUri(final Context context, final String endpoint, final KakaoNaviParams params) {
        String appKey = Utility.getMetadata(context, CommonProtocol.APP_KEY_PROPERTY);
        if (appKey == null) {
            throw new IllegalStateException("Native app key is not defined in AndroidManifest.xml.");
        }
        if (params == null) {
            throw new NullPointerException("KakaoNaviParams is a required parameter and cannot be null.");
        }

        Uri.Builder builder = new Uri.Builder();
        boolean isNative = KakaoNaviService.isKakaoNaviInstalled(context);
        if (isNative) {
            builder.scheme(ServerProtocol.NAVI_SCHEME);
            builder.authority(endpoint);
        } else {
            builder.scheme(ServerProtocol.NAVI_WEB_SCHEME);
            builder.authority(ServerProtocol.NAVI_AUTHORITY);
            builder.path(ServerProtocol.NAVI_GUIDE_PATH + ".html");
        }

        try {
            JSONObject paramJson = params.toJson();
            builder.appendQueryParameter("param", paramJson.toString());
        } catch (JSONException e) {
            throw new IllegalArgumentException("JSON parsing error. Malformed parameters were provided to KakaoNavi API. detailed error message: " + e.toString());
        }

        builder.appendQueryParameter("apiver", "1.0");
        builder.appendQueryParameter("appkey", appKey);
        builder.appendQueryParameter("extras", getExtras(context));
        return builder.build();
    }

    private static String getExtras(Context context) {
        JSONObject extras = new JSONObject();
        String keyHash = Utility.getKeyHash(context);

        if (keyHash == null)
            throw new IllegalStateException("Android key hash is a required parameter for KakaoNavi API.");

        try {
            extras.put("appPkg", context.getPackageName());
            extras.put("KA", SystemInfo.getKAHeader());
            extras.put("keyHash", keyHash);
            return extras.toString();
        } catch (JSONException e) {
            throw new IllegalArgumentException("JSON parsing error. Malformed parameters wer provided to KakaoNavi API. Detailed error message: " + e.toString());
        }
    }
}
