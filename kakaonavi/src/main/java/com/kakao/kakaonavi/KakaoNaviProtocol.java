package com.kakao.kakaonavi;

/**
 * @author kevin.kang
 * Created by kevin.kang on 2016. 11. 15..
 */

class KakaoNaviProtocol {
    /**
     * 카카오내비 패키지 이름. 앱이 설치되어 있는지 확인할 때 사용한다.
     */
    static final String NAVI_PACKAGE = "com.locnall.KimGiSa";
    static final String NAVI_MARKET_URL = "market://details?id=" + NAVI_PACKAGE;
    static final String PROPERTY_WEB_URL = "com.kakao.kakaonavi.web_url";
}
