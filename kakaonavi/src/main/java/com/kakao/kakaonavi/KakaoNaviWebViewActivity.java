package com.kakao.kakaonavi;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class KakaoNaviWebViewActivity extends Activity {
    public class KakaoNaviWebViewClient extends WebViewClient {
        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url != null && KakaoNaviProtocol.NAVI_MARKET_URL.equals(url)) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                finish();
                return true;
            }
            return super.shouldOverrideUrlLoading(view, url);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            Uri uri = request.getUrl();
            if (uri != null && KakaoNaviProtocol.NAVI_MARKET_URL.equals(request.getUrl().toString())) {
                startActivity(new Intent(Intent.ACTION_VIEW, uri));
                finish();
                return true;
            }
            return super.shouldOverrideUrlLoading(view, request);
        }
    }

    public class KakaoNaviWebChromeClient extends WebChromeClient {
        @Override
        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
            callback.invoke(origin, true, false);
        }
    }

    @Override
    @SuppressLint("SetJavaScriptEnabled")
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kakao_navi_web_view);

        Intent intent = getIntent();
        String url = intent.getStringExtra(KakaoNaviProtocol.PROPERTY_WEB_URL);
        if (url == null) {
            finish();
        }
        WebView webView = (WebView) findViewById(R.id.navi_web_view);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setGeolocationEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(true);

        webView.loadUrl(url);

        webView.setWebViewClient(new KakaoNaviWebViewClient());
        webView.setWebChromeClient(new KakaoNaviWebChromeClient());
    }
}
