package net.daum.android.map.openapi.search;

import java.util.List;

public interface OnFinishSearchListener {
    void onSuccess(List<Item> itemList);

    void onFail();
}
