package me.rewhite.users.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;

/**
 * Created by marines on 2017. 5. 3..
 */

public abstract class ProfileNewBaseFragment extends Fragment {
    protected Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.context = context;
    }

    public abstract void initialize();
}
