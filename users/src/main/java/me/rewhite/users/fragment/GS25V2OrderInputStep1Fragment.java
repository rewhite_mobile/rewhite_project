package me.rewhite.users.fragment;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.gsretail.GS25V2OrderInputActivity;
import me.rewhite.users.util.DUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class GS25V2OrderInputStep1Fragment extends Fragment implements NumberPicker.OnValueChangeListener, IFragment{

    private static final String TAG = "GS25V2OrderInputStep1Fragment";
    View rootView;
    AQuery aq;
    Context mContext;
    int count_unit = 0;
    static Dialog unitDialog;

    public GS25V2OrderInputStep1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_gs25_v2_order_input_01, container, false);

        aq = new AQuery(rootView);

        Tracker t = ((Bootstrap) getActivity().getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        mContext = getActivity();
        aq.id(R.id.unit_count_layout).clicked(this, "unitCountModClicked");

        aq.id(R.id.btn_01).clicked(this, "countClicked").tag(1);
        aq.id(R.id.btn_02).clicked(this, "countClicked").tag(2);
        aq.id(R.id.btn_03).clicked(this, "countClicked").tag(3);
        aq.id(R.id.btn_04).clicked(this, "countClicked").tag(4);
        aq.id(R.id.btn_05).clicked(this, "countClicked").tag(5);
        aq.id(R.id.btn_06).clicked(this, "countClicked").tag(6);
        aq.id(R.id.btn_07).clicked(this, "countClicked").tag(7);
        aq.id(R.id.btn_08).clicked(this, "countClicked").tag(8);

        aq.id(R.id.btn_submit).clicked(this, "nextProcessClicked");



        return rootView;
    }

    @Override
    public void onResume() {
        Log.d(this.getClass().getSimpleName(), "onResume()");
        super.onResume();

        if(getActivity() instanceof GS25V2OrderInputActivity) {
            final GS25V2OrderInputActivity ra = (GS25V2OrderInputActivity) getActivity();
            selectButton(ra.count_unit);
        }
    }

    public void selectButton(int index){

            count_unit = index;
            if(index > 8){
                selectedIndex = 9;
            }else{
                selectedIndex = index;
            }

            allClearButtonStatus();

            aq.id(R.id.btn_submit).text(count_unit + "개의 세탁물을 맡깁니다.");

            if(count_unit>0){
                aq.id(R.id.btn_submit).backgroundColor(0xff252525).textColor(0xffffffff);
            }

            switch (selectedIndex){
                case 1:
                    aq.id(R.id.btn_01).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                    break;
                case 2:
                    aq.id(R.id.btn_02).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                    break;
                case 3:
                    aq.id(R.id.btn_03).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                    break;
                case 4:
                    aq.id(R.id.btn_04).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                    break;
                case 5:
                    aq.id(R.id.btn_05).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                    break;
                case 6:
                    aq.id(R.id.btn_06).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                    break;
                case 7:
                    aq.id(R.id.btn_07).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                    break;
                case 8:
                    aq.id(R.id.btn_08).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                    break;
                case 9:
                    aq.id(R.id.unit_count_layout).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                    //aq.id(R.id.text_unit_count).text(String.valueOf(count_unit) + " 개").textSize(18.f);

                    submitButtonInitialize();
                    break;
            }

    }

    int selectedIndex = 0;
    public void countClicked(View button){
        int tag = (int)button.getTag();
        count_unit = tag;
        selectedIndex = tag;
        allClearButtonStatus();

        aq.id(R.id.btn_submit).text(count_unit + "개의 세탁물을 맡깁니다.");

        if(count_unit>0){
            aq.id(R.id.btn_submit).backgroundColor(0xff252525).textColor(0xffffffff);
        }

        switch (tag){
            case 1:
                aq.id(R.id.btn_01).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                break;
            case 2:
                aq.id(R.id.btn_02).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                break;
            case 3:
                aq.id(R.id.btn_03).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                break;
            case 4:
                aq.id(R.id.btn_04).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                break;
            case 5:
                aq.id(R.id.btn_05).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                break;
            case 6:
                aq.id(R.id.btn_06).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                break;
            case 7:
                aq.id(R.id.btn_07).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                break;
            case 8:
                aq.id(R.id.btn_08).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                break;
        }
    }

    private void allClearButtonStatus(){

        aq.id(R.id.btn_01).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);
        aq.id(R.id.btn_02).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);
        aq.id(R.id.btn_03).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);
        aq.id(R.id.btn_04).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);
        aq.id(R.id.btn_05).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);
        aq.id(R.id.btn_06).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);
        aq.id(R.id.btn_07).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);
        aq.id(R.id.btn_08).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);
        aq.id(R.id.unit_count_layout).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);
    }

    public void unitCountModClicked(View button){
        unitNumberPickerShow();
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

        Log.i("value is",""+newVal);

    }

    private void submitButtonInitialize(){
        if(count_unit > 0){
            aq.id(R.id.btn_submit).image(R.mipmap.btn_gs_order_start).enabled(true);
        }else{
            aq.id(R.id.btn_submit).image(R.mipmap.btn_gs_order_input).enabled(false);
        }
    }

    private void unitNumberPickerShow(){
        final Dialog d = new Dialog(getActivity());
        //d.setTitle("맡기실 세탁물 수량을 입력해주세요.");
        d.setContentView(R.layout.dialog_unit_picker);
        Button b1 = (Button) d.findViewById(R.id.btn_unit_cancel);
        Button b2 = (Button) d.findViewById(R.id.btn_unit_ok);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.unitPicker);
        np.setMaxValue(30);
        np.setMinValue(9);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        b2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                //tv.setText(String.valueOf(np.getValue()));
                Log.e("unitPick", String.valueOf(np.getValue()));
                count_unit = np.getValue();
                if(count_unit >= 9){
                    aq.id(R.id.btn_submit).backgroundColor(0xff252525).textColor(0xffffffff).text(count_unit + "개의 세탁물을 맡깁니다.");;
                    allClearButtonStatus();
                    aq.id(R.id.unit_count_layout).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                    //aq.id(R.id.text_unit_count).text(String.valueOf(count_unit) + " 개").textSize(18.f);

                    submitButtonInitialize();
                }else{
                    //aq.id(R.id.text_unit_count).text("수량을 정확하게 입력해주세요").textSize(13.f);
                }

                d.dismiss();
            }
        });
        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    public void nextProcessClicked(View button){
        if(count_unit > 0){
            if(getActivity() instanceof GS25V2OrderInputActivity){
                final GS25V2OrderInputActivity ra = (GS25V2OrderInputActivity)getActivity();
                ra.count_unit = count_unit;
                ra.showFragment(GS25V2OrderInputActivity.FRAGMENT_STEP2);
            }
        }else{
            DUtil.alertShow(getActivity(), "수량을 선택해주세요.");
        }


    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }
}
