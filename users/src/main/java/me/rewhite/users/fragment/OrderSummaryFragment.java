package me.rewhite.users.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.OrderActivity;
import me.rewhite.users.activity.PaymentNoticeScreen;
import me.rewhite.users.activity.PolicyNoticeActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.PickupType;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.TimeUtil;


public class OrderSummaryFragment extends Fragment implements IFragment {

    private static final String TAG = "OrderSummaryFragment";
    AQuery aq;
    private OrderActivity mActivity = null;
    private Context ctx;
    boolean isChecked = false;

    public OrderSummaryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {

        Log.d(TAG, "[onResume]");
        mActivity.setTouchDisable(false);

        super.onResume();
        mActivity.setTitle("주문 확인");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (OrderActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_order_summary, container, false);
        ctx = getActivity();
        aq = new AQuery(getActivity(), view);

        JSONObject storeInfo = mActivity.storeInfo;
        long pTime = mActivity.pickupReqTime;
        long dTime = mActivity.deliveryReqTime;
        String sServiceItem = mActivity.orderRequestItem;
        String rMemo = mActivity.orderRequestString;

        String addressSeq = mActivity.addressSeq;
        String addressDetail = mActivity.addressDetailName;
        String address = mActivity.addressName;

        aq.id(R.id.pickup_date).text(TimeUtil.convertTimestampToStringDate(pTime)).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.pickup_time).text(TimeUtil.convertTimestampToStringTime(pTime, mActivity.storeDeliveryDuration)).typeface(CommonUtility.getNanumBarunTypeface());

        aq.id(R.id.delivery_date).text(TimeUtil.convertTimestampToStringDate(dTime)).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.delivery_time).text(TimeUtil.convertTimestampToStringTime(dTime, mActivity.storeDeliveryDuration)).typeface(CommonUtility.getNanumBarunTypeface());

        aq.id(R.id.address_detail).text(addressDetail).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.address).text(address).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.req_memo).text(rMemo).typeface(CommonUtility.getNanumBarunTypeface());

        aq.id(R.id.btn_comp).clicked(this, "orderCompleted");
        aq.id(R.id.btn_check).clicked(this, "orderPreCheckAction");
        aq.id(R.id.btn_alert).clicked(this, "noticeAlertAction");

        Log.i("ReqDate", TimeUtil.convertTimestampToString(pTime) + " / " + TimeUtil.convertTimestampToString(dTime));

        return view;
    }

    public void orderPreCheckAction(View button){
        isChecked = !isChecked;
        if(isChecked){
            aq.id(R.id.btn_check).image(R.mipmap.check_order_desc_on);
        }else{
            aq.id(R.id.btn_check).image(R.mipmap.check_order_desc_off);
        }
    }
    public void noticeAlertAction(View button){
        Intent intent = new Intent(getActivity(), PolicyNoticeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        getActivity().startActivity(intent);
    }

    public void orderCompleted(View button) {

        if(!isChecked){
            DUtil.alertShow(getActivity(), "주문시 유의사항을 확인해주세요.");
            return;
        }

        try {
            RequestParams params = new RequestParams();
            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            params.put("addressSeq", mActivity.addressSeq);
            params.put("storeId", mActivity.storeInfo.getString("storeId"));
            params.put("pickupRequestTime", mActivity.pickupReqTime);
            params.put("deliveryRequestTime", mActivity.deliveryReqTime);
            params.put("pickupRequestMessage", mActivity.orderRequestString);
            params.put("orderRequestItem", mActivity.orderRequestItem);
            params.put("orderType", "1");

            if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
                params.put("orderSubType", 105);
            }else if(mActivity.pickupMode == PickupType.PICKUPTYPE_TOMORROW){
                params.put("orderSubType", 104);
            }else{
                params.put("orderSubType", 101);
            }

            Log.e("param", params.toString());

            params.put("k", 1);
            NetworkClient.post(Constants.ADD_ORDER, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                    // TODO Auto-generated method stub
                    DUtil.Log(Constants.ADD_ORDER, error.getMessage());

                    Toast.makeText(getActivity(), "오류가 발생했습니다.\n"+error.getMessage(), Toast.LENGTH_LONG).show();
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                    String result;
                    try {
                        result = new String(data, "UTF-8");
                        DUtil.Log(Constants.ADD_ORDER, result);

                        JSONObject jsondata = new JSONObject(result);

                        if ("S0000".equals(jsondata.getString("resultCode"))) {

                            try{
                                Tracker t = ((Bootstrap) getActivity().getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                                t.send(new HitBuilders.EventBuilder()
                                        .setCategory("주문")
                                        .setAction("주문완료")
                                        //.setLabel("Order")
                                        .build());
                            }catch (NullPointerException e){
                                e.printStackTrace();
                            }

                            Toast.makeText(getActivity().getApplicationContext(), "신청이 접수되었습니다", Toast.LENGTH_LONG).show();

                            if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
                                AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
                                logger.logEvent("Today Order Completed");

                                getActivity().finish();

                            }else if(mActivity.pickupMode == PickupType.PICKUPTYPE_TOMORROW){
                                AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
                                logger.logEvent("Tomorrow Order Completed");

                                getActivity().finish();
                            }else{
                                AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
                                logger.logEvent("Normal order completed");

                                Intent payNoticeIntent = new Intent(getActivity(), PaymentNoticeScreen.class);
                                payNoticeIntent.putExtra("content", mActivity.storeInfo.toString());
                                payNoticeIntent.putExtra("deliveryRequestTime", mActivity.deliveryReqTime);
                                payNoticeIntent.putExtra("type", "01");
                                payNoticeIntent.putExtra("pickupMode", mActivity.pickupMode);
                                startActivity(payNoticeIntent);
                                getActivity().finish();
                            }

                        } else {
                            try{
                                Tracker t = ((Bootstrap) getActivity().getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                                t.send(new HitBuilders.EventBuilder()
                                        .setCategory("주문")
                                        .setAction("주문오류:"+jsondata.getString("resultCode"))
                                        //.setLabel("Order")
                                        .build());
                            }catch (NullPointerException e){
                                e.printStackTrace();
                            }


                            Toast.makeText(getActivity(), jsondata.getString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }


}
