package me.rewhite.users.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.EventAlertScreen;
import me.rewhite.users.activity.gsretail.GS25OrderActivity;
import me.rewhite.users.activity.HtmlActivity;
import me.rewhite.users.activity.LocationActivity;
import me.rewhite.users.activity.LocationAlertScreen;
import me.rewhite.users.activity.LocationChoiceScreen;
import me.rewhite.users.activity.MainActivity;
import me.rewhite.users.activity.OrderActivity;
import me.rewhite.users.activity.SpecialOrderActivity;
import me.rewhite.users.activity.StoreChoiceScreen;
import me.rewhite.users.activity.theplace.ThePlaceOrderActivity;
import me.rewhite.users.adapter.OrderListItem;
import me.rewhite.users.adapter.OrderTimelineListViewAdapter;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.session.RewhiteSession;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.StringUtil;
import me.rewhite.users.util.TimeUtil;
import me.rewhite.users.util.ToastUtility;


public class MainFragment extends Fragment implements IFragment {

    private static final String TAG = "MainFragment";
    AQuery aq;
    private MainActivity mMainActivity = null;
    private Context ctx;
    private String currentLocationAddress = "";
    private int currentStatusCode = 1;
    String currentLat;
    String currentLon;
    String currentAddress;
    String currentDetailAddress;
    String currentAddressSeq;

    int storeId;
    String currentStoreId;
    String storeName;
    JSONObject currentStoreInfo;
    JSONArray storeInfo;
    Tracker mTracker;
    int latestLocationStoreId = -1;

    private View mView;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {

        Log.d(TAG, "[onResume]");
        mMainActivity.setTouchDisable(false);
        ctx = getActivity();

        getBannerInfo();
        /*
        if(second != null){
            second.run();
        }*/

        super.onResume();
        //initialize();
        if(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN) != null){
            switch(currentStatusCode){
                case 1:
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            //getBannerInfo();
                            refreshMyLocationInfo();
                            getTimelineData();
                        }
                    }, 200);
                    break;
                case 2:
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            //getBannerInfo();
                            initialize(currentStatusCode);
                            //refreshMyLocationInfo();
                            getTimelineData();
                        }
                    }, 200);
                    break;
                case 3:
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            //getBannerInfo();
                            initialize(currentStatusCode);
                            //refreshMyLocationInfo();
                            getTimelineData();
                        }
                    }, 200);
                    break;
                case 4:

                    break;
                case 5:

                    break;
            }
        }

        // 세탁소 선택창 무한루프 현상으로 initialize호출 제거

    }

    @Override
    public void onPause() {

        Log.d(TAG, "[onPause]");
        mMainActivity.setTouchDisable(true);
        if(second != null){
            second.cancel();
        }

        super.onPause();
        //initialize();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTracker = ((Bootstrap)getActivity().getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        mMainActivity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mView = inflater.inflate(R.layout.fragment_main, container, false);
        ctx = getActivity();
        aq = new AQuery(getActivity(), mView);
        aq.id(R.id.top_area).clicked(this, "topmenuClicked");
        aq.id(R.id.btn_status).clicked(this, "orderClicked");

        aq.id(R.id.btn_location_set).clicked(this, "locationChangeClicked");
        aq.id(R.id.btn_invalid).clicked(this, "locationChangeClicked").enabled(false);

        aq.id(R.id.btn_banner).clicked(this, "bannerClicked");
        aq.id(R.id.btn_order_special).clicked(this, "theplaceClicked");
        aq.id(R.id.btn_order_gs).clicked(this, "orderGSClicked");

        aq.id(R.id.promotion_area).gone();

        currentStoreId = SharedPreferencesUtility.get(SharedPreferencesUtility.LocationInfo.CURRENT_SELECTED_STORE_ID);
        if(currentStoreId != null){
            latestLocationStoreId = Integer.parseInt(currentStoreId);
        }else{
            latestLocationStoreId = -1;
        }


        //
        initialize();
        //initialize(currentStatusCode);

        // 바코드스캔 개발중
        //aq.id(R.id.btn_barcode).clicked(this, "clickBarcode").gone();

        return mView;
    }

    public void theplaceClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("주문")
                        .setAction("리화이트플레이스 주문")
                        //.setLabel("")
                        .build());

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(ctx, ThePlaceOrderActivity.class);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);

            }
        }, 100);
    }

    public void specialClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("주문")
                        .setAction("시즌세탁주문")
                        //.setLabel("")
                        .build());

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(ctx, SpecialOrderActivity.class);
                getActivity().startActivity(intent);

            }
        }, 100);


    }

    public void bannerClicked(View button){
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("이벤트")
                .setAction("메인배너터치")
                //.setLabel("")
                .build());
        AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
        logger.logEvent("Touch Main Banner");

        String url = null;
        try {
            url = bannerInfo.getJSONObject(timer_sec%bannerInfo.length()).getString("url");
            Uri uri = Uri.parse(url);
            String queryHost = uri.getHost();

            if("http".equals(uri.getScheme()) || "https".equals(uri.getScheme())){
                Intent intent = new Intent(ctx, HtmlActivity.class);
                intent.putExtra("TERMS_URI", url);
                intent.putExtra("TITLE_NAME", "이벤트");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
            }else{
                //String url ="selphone://post_detail?post_id=10";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//
//                Intent intent = new Intent();
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                intent.setClass(ctx, ShareActivity.class);
//                startActivity(intent);
//                getActivity().overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
//
//            }
//        }, 100);
    }

    public void locationChangeClicked(View button) {

        JSONArray locations = SharedPreferencesUtility.getLocationData();
        if (locations != null) {
            if (locations.length() > 0) {
                latestLocationStoreId = -1;

                Intent phoneInputIntent = new Intent(ctx, LocationChoiceScreen.class);
                phoneInputIntent.putExtra("content", SharedPreferencesUtility.getLocationData().toString());

                phoneInputIntent.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(phoneInputIntent, MainActivity.CHOICE_LOCATION);
            }else{
                openLocationScreen();
            }
        }else{
            openLocationScreen();
        }
    }

    private void openLocationScreen(){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(ctx, LocationActivity.class);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
            }
        }, 100);
    }

    private int getDefaultAddressIndex() {
        try {
            JSONArray locations = SharedPreferencesUtility.getLocationData();
            if (locations != null) {
                if (locations.length() > 0) {
                    for (int i = 0; i < locations.length(); i++) {
                        JSONObject tempLoc = locations.getJSONObject(i);
                        String isDefault = tempLoc.getString("isDefault");
                        if ("Y".equals(isDefault)) {
                            return i;
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void refreshMyLocationInfo(){
        if (SharedPreferencesUtility.getLocationData() != null) {
            JSONArray locations = SharedPreferencesUtility.getLocationData();
            if (locations != null) {
                if (locations.length() > 0) {
                    //aq.id(R.id.btn_location_set).enabled(true);
                    if (locations.length() > 1) {
                        aq.id(R.id.btn_invalid).enabled(true);
                    }
                    // 주변가맹점여부 체크
                    try {
                        JSONObject myDefaultLocation = locations.getJSONObject(getDefaultAddressIndex());
                        Log.e("locationInfo", myDefaultLocation.toString());

                        currentAddressSeq = myDefaultLocation.getString("addressSeq");

                        String label = myDefaultLocation.getString("label");
                        currentAddress = myDefaultLocation.getString("address1");
                        currentDetailAddress = myDefaultLocation.getString("address2");
                        currentLon = myDefaultLocation.getString("longitude");
                        currentLat = myDefaultLocation.getString("latitude");
                        String isDefault = myDefaultLocation.getString("isDefault");

                        if (StringUtil.isNullOrEmpty(label) && StringUtil.isNullOrEmpty(currentDetailAddress)) {
                            currentLocationAddress = currentAddress;
                        } else {
                            if (StringUtil.isNullOrEmpty(label) || "null".equals(label)) {
                                if (StringUtil.isNullOrEmpty(currentDetailAddress)) {
                                    currentLocationAddress = currentAddress;
                                } else {
                                    currentLocationAddress = currentDetailAddress;
                                }
                            } else {
                                if (currentDetailAddress.contains(label)) {
                                    currentLocationAddress = currentDetailAddress;
                                } else {
                                    if (StringUtil.isNullOrEmpty(currentDetailAddress)) {
                                        currentLocationAddress = currentAddress;
                                    } else {
                                        currentLocationAddress = currentDetailAddress + " (" + label + ")";
                                    }

                                }
                            }
                        }

                        aq.id(R.id.location_title).text(currentLocationAddress).typeface(CommonUtility.getNanumBarunTypeface());

                        findStoreCheck(currentLon, currentLat);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void initialize() {

        if (SharedPreferencesUtility.getLocationData() != null) {
            JSONArray locations = SharedPreferencesUtility.getLocationData();
            if (locations != null) {
                if (locations.length() > 0) {
                    //aq.id(R.id.btn_location_set).enabled(true);
                    if (locations.length() > 1) {
                        aq.id(R.id.btn_invalid).enabled(true);
                    }
                    // 주변가맹점여부 체크
                    try {
                        JSONObject myDefaultLocation = locations.getJSONObject(getDefaultAddressIndex());
                        Log.e("locationInfo", myDefaultLocation.toString());

                        currentAddressSeq = myDefaultLocation.getString("addressSeq");

                        String label = myDefaultLocation.getString("label");
                        currentAddress = myDefaultLocation.getString("address1");
                        currentDetailAddress = myDefaultLocation.getString("address2");
                        currentLon = myDefaultLocation.getString("longitude");
                        currentLat = myDefaultLocation.getString("latitude");
                        String isDefault = myDefaultLocation.getString("isDefault");

                        if (StringUtil.isNullOrEmpty(label) && StringUtil.isNullOrEmpty(currentDetailAddress)) {
                            currentLocationAddress = currentAddress;
                        } else {
                            if (StringUtil.isNullOrEmpty(label) || "null".equals(label)) {
                                if (StringUtil.isNullOrEmpty(currentDetailAddress)) {
                                    currentLocationAddress = currentAddress;
                                } else {
                                    currentLocationAddress = currentDetailAddress;
                                }
                            } else {
                                if (currentDetailAddress.contains(label)) {
                                    currentLocationAddress = currentDetailAddress;
                                } else {
                                    if (StringUtil.isNullOrEmpty(currentDetailAddress)) {
                                        currentLocationAddress = currentAddress;
                                    } else {
                                        currentLocationAddress = currentDetailAddress + " (" + label + ")";
                                    }

                                }
                            }
                        }

                        aq.id(R.id.location_title).text(currentLocationAddress).typeface(CommonUtility.getNanumBarunTypeface());

                        findStoreCheck(currentLon, currentLat);



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    // STATUS 0
                    currentStatusCode = 1;
                    initialize(currentStatusCode);
                }
            } else {
                // STATUS 0
                currentStatusCode = 1;
                initialize(currentStatusCode);
            }

        } else {
            // STATUS 0
            currentStatusCode = 1;
            initialize(currentStatusCode);
        }
    }

    private synchronized void initialize(int statusCode) {
        Log.e("INITIALIZE STATUS CODE", "STATUS CODE : " + statusCode + "");
        switch (statusCode) {
            case 1:
                //aq.id(R.id.btn_location_set).enabled(false);
                aq.id(R.id.location_title).text("주소를 등록해주세요.").typeface(CommonUtility.getNanumBarunTypeface());
                aq.id(R.id.location_area).visible();
                aq.id(R.id.invalid_area).gone();
                aq.id(R.id.status_image).image(R.mipmap.main_back_01);
                aq.id(R.id.btn_status).image(R.drawable.btn_status_01);

                break;
            case 2:
                currentStoreInfo = null;
                aq.id(R.id.location_area).gone();
                aq.id(R.id.invalid_area).visible();
                currentAddress = currentAddress.replace("대한민국 ", "");
                currentAddress = currentAddress.replace("서울특별시", "서울시");
                aq.id(R.id.invalid_address).text(currentAddress).typeface(CommonUtility.getNanumBarunTypeface());
                aq.id(R.id.status_image).image(R.mipmap.main_back_02);
                aq.id(R.id.btn_status).image(R.drawable.btn_status_02);

                break;
            case 3:
                currentLocationAddress = currentLocationAddress.replace("대한민국 ", "");
                currentLocationAddress = currentLocationAddress.replace("서울특별시", "서울시");
                aq.id(R.id.location_title).text(currentLocationAddress).typeface(CommonUtility.getNanumBarunTypeface());
                aq.id(R.id.location_area).visible();
                aq.id(R.id.invalid_area).gone();
                aq.id(R.id.status_image).image(R.mipmap.main_back_03);
                aq.id(R.id.btn_status).image(R.drawable.btn_status_03);

                break;
            case 4:
                aq.id(R.id.invalid_area).gone();
                aq.id(R.id.status_image).image(R.mipmap.main_back_04);
                aq.id(R.id.btn_status).image(R.drawable.btn_status_04);
                break;
            case 5:
                aq.id(R.id.invalid_area).gone();
                aq.id(R.id.status_image).image(R.mipmap.main_back_05);
                aq.id(R.id.btn_status).image(R.drawable.btn_status_05);
                break;
            default:
                break;
        }
    }

    private void showPopup(String _showType){
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setClass(ctx, EventAlertScreen.class);
        intent.putExtra("type", _showType);

        if(getActivity() != null){
            getActivity().startActivity(intent);
        }

    }

    private void showPopup(String _imageLink, String _eventLink){
        Date today = new Date();
        if (TimeUtil.convertTimestampToStringDate2(today.getTime()).equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Command.EVENT_LATEST_DATE))) {

        }else{
            try{
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(ctx, EventAlertScreen.class);
                intent.putExtra("type", "event");

                intent.putExtra("image", _imageLink);
                intent.putExtra("link", _eventLink);
                if(getActivity() != null){
                    getActivity().startActivity(intent);
                }
            }catch(NullPointerException e){
                e.printStackTrace();
            }

        }
    }

    private void nonEventPopupCheck(){
        Date today = new Date();
        if (TimeUtil.convertTimestampToStringDate2(today.getTime()).equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Command.EVENT_LATEST_DATE))) {

        } else {
            if (isExistCoupon) {
                showPopup("coupon");
            } else {
                showPopup("refer");
            }
        }
    }


    boolean isExistCoupon;
    private synchronized void getBannerInfo() {
        if(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.SHARE_CODE) == null || "".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.SHARE_CODE))){
            return;
        }

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.EVENT_CHECK, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.EVENT_CHECK, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.EVENT_CHECK, result);

                    JSONObject jsondata = new JSONObject(result);
                    isExistCoupon = false;

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {

                        } else {
                            bannerInfo = jsondata.getJSONObject("data").getJSONArray("banner");
                            popupInfo = jsondata.getJSONObject("data").getJSONArray("popup");

                            if(bannerInfo.length() > 0){
                                activateBanner();
                            }

                            if(popupInfo.length() > 0){
                                activatePopup();
                            }
                        }
                    }

                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    JSONArray bannerInfo;
    JSONArray popupInfo;
    Timer timer;
    private TimerTask second;
    private final Handler handler = new Handler();
    int timer_sec = 0;

    private void activateBanner(){
        timer_sec = 0;

        second = new TimerTask() {

            @Override
            public void run() {
                //Log.i("Test", "Timer start");
                UpdateBanner();
                timer_sec++;
            }
        };
        timer = new Timer();
        timer.schedule(second, 0, 4500);

    }

    protected void UpdateBanner() {
        Runnable updater = new Runnable() {
            public void run() {
                try {
                    aq.id(R.id.btn_banner).image(bannerInfo.getJSONObject(timer_sec%bannerInfo.length()).getString("image"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        handler.post(updater);
    }

    private void activatePopup(){
        Date today = new Date();
        if (TimeUtil.convertTimestampToStringDate2(today.getTime()).equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Command.EVENT_LATEST_DATE))) {
            Log.e("popup info", "오늘 닫았어");
//            try {
//                if(popupInfo.getJSONObject(0).getString("bannerId").equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Command.EVENT_NEVER_SHOW))){
//                    Log.e("popup info", "배너아이디 겹쳐서 안나오기도 해");
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
        }else{
            try {
                if(popupInfo.getJSONObject(0).getString("bannerId").equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Command.EVENT_NEVER_SHOW))){
                    Log.e("popup info", "배너아이디 겹쳐서 안나옴");
                    return;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try{
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(ctx, EventAlertScreen.class);
                intent.putExtra("type", "event");
                try {
                    intent.putExtra("bannerId", popupInfo.getJSONObject(0).getString("bannerId"));
                    intent.putExtra("image", popupInfo.getJSONObject(0).getString("image"));
                    intent.putExtra("link", popupInfo.getJSONObject(0).getString("url"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(getActivity() != null){
                    getActivity().startActivity(intent);
                }
            }catch(NullPointerException e){
                e.printStackTrace();
            }

        }
    }


    // 주변 유효상점 검색
    private synchronized void findStoreCheck(String lon, String lat) {

        aq.id(R.id.promotion_area).gone();
        //Constants.FIND_NEAR_STORE;
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("longitude", lon);
        params.put("latitude", lat);
        params.put("serviceType", 1);
        params.put("k", 1);
        NetworkClient.post(Constants.FIND_NEAR_STORE, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.FIND_NEAR_STORE, error.getMessage());
                currentStatusCode = 4;
                initialize(currentStatusCode);
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {

                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.FIND_NEAR_STORE, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            currentStatusCode = 2;
                            initialize(currentStatusCode);
                            DUtil.Log(Constants.FIND_NEAR_STORE, "STORE NULL");
                        } else {
                            if(jsondata.getJSONArray("data") == null){
                                currentStatusCode = 2;
                                initialize(currentStatusCode);
                                DUtil.Log(Constants.FIND_NEAR_STORE, "STORE NULL");
                            }else{
                                JSONArray storeData = jsondata.getJSONArray("data");
                                if (storeData.length() > 0) {
                                    currentStatusCode = 3;
                                    initialize(currentStatusCode);
                                    storeInfo = storeData;

                                    if(storeData.length() == 1){
                                        /*
                                        for(int i =0; i < storeInfo.length(); i++){
                                            if(storeInfo.getJSONObject(i).getInt("storeId") == 1015){
                                                String availableOrder = storeInfo.getJSONObject(i).getString("availableOrder");
                                                if(availableOrder.length() > 3) {
                                                    if ("1".equals(String.valueOf(availableOrder.charAt(3)))) {
                                                        aq.id(R.id.btn_order_quick).visible();

                                                    } else {

                                                    }
                                                }
                                            }
                                        }*/
                                        // 주변 세탁소 1개일경우
                                        currentStoreInfo = storeData.getJSONObject(0);
                                        storeId = currentStoreInfo.getInt("storeId");
                                        // storeId, pickupRequestTime, deliveryRequestTime, pickupRequestMessage

                                        String availableOrder = storeData.getJSONObject(0).getString("availableOrder");

                                        /*
                                        빠른세탁 기능 제거
                                         */
//                                        if(availableOrder.length() > 3){
//                                            if ("1".equals(String.valueOf(availableOrder.charAt(3)))) {
//                                                aq.id(R.id.btn_order_quick).visible();
//
//                                            }else{
//                                                if(storeId == 1005){
//                                                    aq.id(R.id.btn_order_quick).visible();
//                                                    //aq.id(R.id.btn_order_quick).clicked(this, "orderQuickClicked");
//                                                }
//                                            }
//                                        }

                                        if(currentStoreInfo.getJSONArray("promotions").length() > 0){
                                            aq.id(R.id.promotion_area).visible();
                                            String promoTitle = currentStoreInfo.getJSONArray("promotions").getJSONObject(0).getString("title");
                                            String promoDescription = currentStoreInfo.getJSONArray("promotions").getJSONObject(0).getString("description");
                                            aq.id(R.id.promotion_title).text(promoTitle + "");
                                            aq.id(R.id.promotion_content).text(promoDescription + "");
                                        }else{
                                            aq.id(R.id.promotion_area).gone();
                                        }
                                    }else{
                                        //currentStoreInfo = null;
                                        //storeId = -1;
                                        aq.id(R.id.promotion_area).gone();

                                        boolean isCurrentSelected = false;

                                        for(int i = 0; i < storeData.length(); i++){
                                            if(storeData.getJSONObject(i).getInt("storeId") == latestLocationStoreId){
                                                isCurrentSelected = true;
                                            }
                                        }

                                        if(!isCurrentSelected){
                                            if(!isStorePopupOpened){
                                                choiceStorePopup(storeInfo);
                                            }

                                        }


                                    }

                                } else {
                                    currentStatusCode = 2;
                                    storeInfo = new JSONArray();
                                    currentStoreInfo = new JSONObject();
                                    initialize(currentStatusCode);
                                }
                            }

                        }
                    } else {
                        currentStatusCode = 4;
                        initialize(currentStatusCode);
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    boolean isStorePopupOpened = false;

    private void choiceStorePopup(JSONArray _storeInfo){
        if(_storeInfo != null && ctx != null){
            if(_storeInfo.length() > 0){
                AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
                logger.logEvent("Select Laundry Popup");

                Intent storeChoiceIntent = new Intent(ctx, StoreChoiceScreen.class);
                storeChoiceIntent.putExtra("storeInfo", _storeInfo.toString());

                Log.e("storeInfo", _storeInfo.toString());

                mMainActivity.startActivityForResult(storeChoiceIntent, MainActivity.CHOICE_STORE);
                mMainActivity.overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                isStorePopupOpened = true;

            }else{
                ToastUtility.show(getActivity(), "세탁소를 선택할수 없습니다.", Toast.LENGTH_LONG);
            }
        }else{
            ToastUtility.show(getActivity(), "시스템오류로 인해 일시적으로 세탁소를 선택할수 없습니다.", Toast.LENGTH_LONG);
        }

    }

    public void orderGSClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                JSONObject quickStoreInfo = new JSONObject();
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(ctx, GS25OrderActivity.class);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);

            }
        }, 100);
    }

    private void registerAlarmList() {
        AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
        logger.logEvent("Try Received Alarm");

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("phone", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.PHONE_NO));
        params.put("longitude", currentLon);
        params.put("latitude", currentLat);
        params.put("address1", currentAddress);
        params.put("address2", currentDetailAddress);
        params.put("k", 1);
        NetworkClient.post(Constants.ADD_ALARM_NEW, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ADD_ALARM_NEW, error.getMessage());
                currentStatusCode = 4;
                initialize(currentStatusCode);
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {

                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ADD_ALARM_NEW, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        ToastUtility.show(ctx, jsondata.getString("message"), Toast.LENGTH_SHORT);
                        AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
                        logger.logEvent("Received Alarm");
                    } else if ("S9006".equals(jsondata.getString("resultCode"))) {
                        // 중복 등록
                        ToastUtility.show(ctx, jsondata.getString("message"), Toast.LENGTH_SHORT);
                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void changeStore(final JSONObject obj){
        isStorePopupOpened = false;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                try {
                    if(obj == null){
                        currentStoreInfo = null;
                        SharedPreferencesUtility.set(SharedPreferencesUtility.LocationInfo.CURRENT_SELECTED_STORE_ID, "-1");
                        SharedPreferencesUtility.set(SharedPreferencesUtility.LocationInfo.CURRENT_SELECTED_ADDRESS_SEQ, "-1");
                    }else{
                        currentStoreInfo = obj;
                        storeId = currentStoreInfo.getInt("storeId");
                        SharedPreferencesUtility.set(SharedPreferencesUtility.LocationInfo.CURRENT_SELECTED_STORE_ID, storeId + "");
                        SharedPreferencesUtility.set(SharedPreferencesUtility.LocationInfo.CURRENT_SELECTED_ADDRESS_SEQ, currentAddressSeq + "");

                        currentStoreId = SharedPreferencesUtility.get(SharedPreferencesUtility.LocationInfo.CURRENT_SELECTED_STORE_ID);
                        latestLocationStoreId = Integer.parseInt(currentStoreId);

                        if(currentStoreInfo.getJSONArray("promotions").length() > 0){
                            aq.id(R.id.promotion_area).visible();
                            String promoTitle = currentStoreInfo.getJSONArray("promotions").getJSONObject(0).getString("title");
                            String promoDescription = currentStoreInfo.getJSONArray("promotions").getJSONObject(0).getString("description");
                            aq.id(R.id.promotion_title).text(promoTitle + "");
                            aq.id(R.id.promotion_content).text(promoDescription + "");
                        }else{
                            aq.id(R.id.promotion_area).gone();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(ctx, OrderActivity.class);
                intent.putExtra("storeInfo", obj.toString());
                intent.putExtra("addressSeq", currentAddressSeq);
                intent.putExtra("addressDetailName", currentDetailAddress);
                intent.putExtra("addressName", currentAddress);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);*/
            }
        }, 100);
    }

    public void orderClicked(View button) {
        if (currentStatusCode == 1) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("가입및탈퇴")
                            .setAction("주소등록시도")
                            //.setLabel("")
                            .build());

                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setClass(ctx, LocationActivity.class);
                    getActivity().startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                }
            }, 100);
        } else if (currentStatusCode == 2) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("가입및탈퇴")
                    .setAction("알림받기")
                    //.setLabel("")
                    .build());

            Intent alertIntent = new Intent(ctx, LocationAlertScreen.class);
            alertIntent.putExtra("longitude", currentLon);
            alertIntent.putExtra("latitude", currentLat);
            alertIntent.putExtra("address1", currentAddress);
            alertIntent.putExtra("address2", currentDetailAddress);
            getActivity().startActivityForResult(alertIntent, MainActivity.INVALID_LOCATION);
            /*
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setCancelable(false).setTitle("현재는 강남구,마포구 등 일부지역에서 서비스 제공중입니다.").setMessage("선택하신 지역의 가맹점 오픈 소식을 SMS문자로 알려드릴까요?")
                    .setPositiveButton("네", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            registerAlarmList();
                        }
                    }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            // 가맹점 오픈소식 등록
            */
        } else if (currentStatusCode == 3) {
            if(storeId != -1 && currentStoreInfo != null){
                DUtil.Log("storeInfo", currentStoreInfo.toString() + "");

                try {
                    if("Y".equals(currentStoreInfo.getString("isSuspend"))){
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
                        alertDialogBuilder.setCancelable(false).setMessage("세탁소 사정으로 일시적으로 주문이 불가능합니다.\n빠른 시일내에 정상화될 예정이니 양해부탁드립니다.")
                                .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }else{
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                mTracker.send(new HitBuilders.EventBuilder()
                                        .setCategory("주문")
                                        .setAction("일반세탁주문")
                                        //.setLabel("")
                                        .build());
                                AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
                                logger.logEvent("Select Normal order");


                                DUtil.Log("addressSeq", currentAddressSeq + "");

                                Intent intent = new Intent();
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                intent.setClass(ctx, OrderActivity.class);
                                intent.putExtra("storeInfo", currentStoreInfo.toString());
                                intent.putExtra("addressSeq", currentAddressSeq);
                                intent.putExtra("addressDetailName", currentDetailAddress);
                                intent.putExtra("addressName", currentAddress);
                                getActivity().startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                            }
                        }, 100);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else{
                // 상점이 2개 이상인 경우 선택팝업
                if(!isStorePopupOpened){
                    choiceStorePopup(storeInfo);
                }

            }

        } else if (currentStatusCode == 4) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
            alertDialogBuilder.setCancelable(false).setMessage("로그인정보가 유효하지 않습니다. 다시 로그인해주세요.")
                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().finish();
                            RewhiteSession.getCurrentSession().close();
                        }
                    });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else if (currentStatusCode == 5) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=me.rewhite.users"));
            startActivity(intent);
            getActivity().finish();
        }

    }

    private ArrayList<OrderListItem> orderData;
    private OrderTimelineListViewAdapter orderAdapter;

    private synchronized void getTimelineData() {
        DUtil.Log(Constants.ORDER_LIST, "REQUEST");

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("mode", "Y");
        //params.put("mode", "X");
        params.put("page", 1);
        params.put("block", 100);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_LIST, error.getMessage());
                if(currentStoreInfo == null){
                    aq.id(R.id.blank_message).visible();
                }else{
                    aq.id(R.id.blank_message).gone();
                }

                aq.id(R.id.listView).gone();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {

                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_LIST, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (!jsondata.isNull("data")) {
                            ListView orderListView = (ListView) mView.findViewById(R.id.listView);

                            orderData = new ArrayList<OrderListItem>();
                            JSONArray orderInfo = jsondata.getJSONArray("data");
                            DUtil.Log(Constants.ORDER_LIST, orderInfo.toString());

                            if (orderInfo.length() > 0) {
                                aq.id(R.id.blank_message).gone();
                                aq.id(R.id.listView).visible();
                                for (int i = 0; i < orderInfo.length(); i++) {

                                    String orderId = orderInfo.getJSONObject(i).getString("orderId");
                                    String orderStatus = orderInfo.getJSONObject(i).getString("orderStatus");
                                    String addressSeq = orderInfo.getJSONObject(i).getString("addressSeq");
                                    String storeId = orderInfo.getJSONObject(i).getString("storeId");
                                    String deliveryPrice = orderInfo.getJSONObject(i).getString("deliveryPrice");
                                    String pickupRequestTimeApp = orderInfo.getJSONObject(i).getString("pickupRequestTimeApp");
                                    String deliveryRequestTimeApp = orderInfo.getJSONObject(i).getString("deliveryRequestTimeApp");
                                    String registerDateApp = orderInfo.getJSONObject(i).getString("registerDateApp");
                                    String orderRequest = orderInfo.getJSONObject(i).getString("orderRequest");
                                    String orderPickupItemMessage = orderInfo.getJSONObject(i).getString("orderPickupItemMessage");
                                    String statusTimeApp = orderInfo.getJSONObject(i).getString("statusTimeApp");
                                    String storeName = orderInfo.getJSONObject(i).getString("storeName");
                                    String storeDeliveryTime = orderInfo.getJSONObject(i).getString("storeDeliveryTime");
                                    String isPayment = orderInfo.getJSONObject(i).getString("isPayment");
                                    int orderSubType = orderInfo.getJSONObject(i).getInt("orderSubType");

                                    if(orderSubType == 151 || orderSubType == 152){
                                        Date now = new Date();
                                        Log.e("getOrderSubType", orderId + " / " + now.toString());
                                    }

                                    int rateScore = orderInfo.getJSONObject(i).getInt("rateScore");

                                    if(rateScore == 0){
                                        if("23".equals(orderStatus) || "24".equals(orderStatus)){
                                            // 배송완료후 15일간 리뷰작성 가능
                                            long statusTime = Long.parseLong(statusTimeApp);
                                            Date comp = TimeUtil.convertTimestampToDate(statusTime);
                                            Calendar c = Calendar.getInstance();
                                            c.setTime(comp);
                                            c.add(Calendar.DATE, 15);
                                            comp = c.getTime();

                                            Date now = new Date();
                                            if((comp.getTime() - now.getTime()) < 0){
                                                OrderListItem aItem = new OrderListItem(orderId, orderStatus, addressSeq, storeId, storeName, deliveryPrice, pickupRequestTimeApp, deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage, storeDeliveryTime, isPayment, orderSubType);
                                                orderData.add(aItem);
                                            }
                                        }else{
                                            OrderListItem aItem = new OrderListItem(orderId, orderStatus, addressSeq, storeId, storeName, deliveryPrice, pickupRequestTimeApp, deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage, storeDeliveryTime, isPayment, orderSubType);
                                            orderData.add(aItem);
                                        }
                                    }
                                }

                                orderAdapter = new OrderTimelineListViewAdapter(ctx, R.layout.order_timeline_item_v2, orderData);
                                orderAdapter.notifyDataSetChanged();

                                orderListView.setAdapter(orderAdapter);
                            } else {
                                // 주문내역 없음
                                if(currentStoreInfo == null){
                                    aq.id(R.id.blank_message).visible();
                                }else{
                                    Log.e("currentStoreInfo", currentStoreInfo.toString() + " ");
                                    aq.id(R.id.blank_message).gone();
                                }

                                //aq.id(R.id.blank_message).visible();
                                aq.id(R.id.listView).gone();
                            }
                        } else {
                            // 주문내역 없음

                            if(currentStoreInfo == null){
                                aq.id(R.id.blank_message).visible();
                            }else{
                                Log.e("currentStoreInfo", currentStoreInfo.toString() + " ");
                                aq.id(R.id.blank_message).gone();
                            }

                            aq.id(R.id.listView).gone();
                        }

                    } else if ("S8000".equals(jsondata.getString("resultCode"))) {
                        // AccessToken Invalid
                        if("".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN)) || SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN) == null){

                        }else{
                            ToastUtility.show(ctx, jsondata.getString("message"), Toast.LENGTH_SHORT);
                        }

                    }else {
                        ToastUtility.show(ctx, jsondata.getString("message"), Toast.LENGTH_SHORT);
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }

    public void topmenuClicked(View button) {
        mMainActivity.getSlidingMenu().toggle();
    }

}
