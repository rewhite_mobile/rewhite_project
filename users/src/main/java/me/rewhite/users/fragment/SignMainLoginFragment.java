package me.rewhite.users.fragment;

//import com.actionbarsherlock.app.SherlockFragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.kakao.auth.AuthType;
import com.kakao.auth.KakaoSDK;
import com.kakao.auth.Session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.FindPasswordActivity;
import me.rewhite.users.activity.SignActivity;
import me.rewhite.users.common.RewhiteLoginType;
import me.rewhite.users.util.CommonUtility;


public class SignMainLoginFragment extends Fragment {

    private static final String TAG = "SignMainLoginFragment";
    private static final String KEY_CONTENT = "SignMainLoginFragment";
    private String mContent = "???";
    private SignActivity mSignActivity = null;
    private ViewGroup mView;
    private AQuery aq;
    private TextView input_email_text;
    private TextView input_password_text;
    private Context ctx;
    private InputMethodManager imm;
    // KAKAO
    //private LoginButtonCustom loginButton;


    final int DRAWABLE_LEFT = 0;
    final int DRAWABLE_TOP = 1;
    final int DRAWABLE_RIGHT = 2;
    final int DRAWABLE_BOTTOM = 3;

    public static SignMainLoginFragment newInstance(String content) {

        SignMainLoginFragment fragment = new SignMainLoginFragment();
        Bundle bdl = new Bundle();
        bdl.putString(KEY_CONTENT, content);
        fragment.setArguments(bdl);

        return fragment;
    }



    public void inputClearClicked(View button) {
        ImageButton v = (ImageButton) button;
        switch ((int) v.getTag()) {
            case 0:
                aq.id(R.id.input_email_text).text("");
                break;
            case 1:
                aq.id(R.id.input_password_text).text("");
                break;
            default:
                break;
        }
    }

    private List<AuthType> getAuthTypes() {
        final List<AuthType> availableAuthTypes = new ArrayList<AuthType>();
        if (Session.getCurrentSession().getAuthCodeManager().isTalkLoginAvailable()) {
            availableAuthTypes.add(AuthType.KAKAO_TALK);
        }
        if (Session.getCurrentSession().getAuthCodeManager().isStoryLoginAvailable()) {
            availableAuthTypes.add(AuthType.KAKAO_STORY);
        }
        availableAuthTypes.add(AuthType.KAKAO_ACCOUNT);

        AuthType[] authTypes = KakaoSDK.getAdapter().getSessionConfig().getAuthTypes();
        if (authTypes == null || authTypes.length == 0 || (authTypes.length == 1 && authTypes[0] == AuthType.KAKAO_LOGIN_ALL)) {
            authTypes = AuthType.values();
        }
        availableAuthTypes.retainAll(Arrays.asList(authTypes));

        // 개발자가 설정한 것과 available 한 타입이 없다면 직접계정 입력이 뜨도록 한다.
        if(availableAuthTypes.size() == 0){
            availableAuthTypes.add(AuthType.KAKAO_ACCOUNT);
        }
        return availableAuthTypes;
    }

    public void kakaoLoginClicked(View button) {
        final List<AuthType> authTypes = getAuthTypes();
        if (authTypes.size() == 1) {
            mSignActivity.requestKakaoSessionOpen(authTypes.get(0), (Activity) ctx);
        } else {
            final Item[] authItems = createAuthItemArray(authTypes);
            ListAdapter adapter = createLoginAdapter(authItems);
            final Dialog dialog = createLoginDialog(authItems, adapter);
            dialog.show();
        }
    }

    /**
     * 가능한 AuhType들이 담겨 있는 리스트를 인자로 받아 로그인 어댑터의 data source로 사용될 Item array를 반환한다.
     * @param authTypes 가능한 AuthType들을 담고 있는 리스트
     * @return 실제로 로그인 방법 리스트에 사용될 Item array
     */
    private Item[] createAuthItemArray(final List<AuthType> authTypes) {
        final List<Item> itemList = new ArrayList<Item>();
        if(authTypes.contains(AuthType.KAKAO_TALK)) {
            itemList.add(new Item(R.string.com_kakao_kakaotalk_account, R.drawable.talk, R.string.com_kakao_kakaotalk_account_tts, AuthType.KAKAO_TALK));
        }
        if(authTypes.contains(AuthType.KAKAO_STORY)) {
            itemList.add(new Item(R.string.com_kakao_kakaostory_account, R.drawable.story, R.string.com_kakao_kakaostory_account_tts, AuthType.KAKAO_STORY));
        }
        if(authTypes.contains(AuthType.KAKAO_ACCOUNT)){
            itemList.add(new Item(R.string.com_kakao_other_kakaoaccount, R.drawable.account, R.string.com_kakao_other_kakaoaccount_tts, AuthType.KAKAO_ACCOUNT));
        }

        return itemList.toArray(new Item[itemList.size()]);
    }

    @SuppressWarnings("deprecation")
    private ListAdapter createLoginAdapter(final Item[] authItems) {
        /**
         * 가능한 auth type들을 유저에게 보여주기 위한 준비.
         */
        return new ArrayAdapter<Item>(
                getContext(),
                android.R.layout.select_dialog_item,
                android.R.id.text1, authItems){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater) getContext()
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.layout_login_item, parent, false);
                }
                ImageView imageView = (ImageView) convertView.findViewById(R.id.login_method_icon);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    imageView.setImageDrawable(getResources().getDrawable(authItems[position].icon, getContext().getTheme()));
                } else {
                    imageView.setImageDrawable(getResources().getDrawable(authItems[position].icon));
                }
                TextView textView = (TextView) convertView.findViewById(R.id.login_method_text);
                textView.setText(authItems[position].textId);
                return convertView;
            }
        };
    }

    /**
     * 실제로 유저에게 보여질 dialog 객체를 생성한다.
     * @param authItems 가능한 AuthType들의 정보를 담고 있는 Item array
     * @param adapter Dialog의 list view에 쓰일 adapter
     * @return 로그인 방법들을 팝업으로 보여줄 dialog
     */
    private Dialog createLoginDialog(final Item[] authItems, final ListAdapter adapter) {
        final Dialog dialog = new Dialog(getContext(), R.style.LoginDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_login_dialog);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setGravity(Gravity.CENTER);
        }

//        TextView textView = (TextView) dialog.findViewById(R.id.login_title_text);
//        Typeface customFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/KakaoOTFRegular.otf");
//        if (customFont != null) {
//            textView.setTypeface(customFont);
//        }

        ListView listView = (ListView) dialog.findViewById(R.id.login_list_view);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final AuthType authType = authItems[position].authType;
                if (authType != null) {
                    mSignActivity.requestKakaoSessionOpen(authType, (Activity) ctx);
                    //openSession(authType);
                }
                dialog.dismiss();
            }
        });

        Button closeButton = (Button) dialog.findViewById(R.id.login_close_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

    private void onClickLoginButton(final List<AuthType> authTypes) {
        if (authTypes.size() == 1) {
            mSignActivity.requestKakaoSessionOpen(authTypes.get(0), (Activity) ctx);
        } else {
            final Item[] authItems = createAuthItemArray(authTypes);
            ListAdapter adapter = createLoginAdapter(authItems);
            final Dialog dialog = createLoginDialog(authItems, adapter);
            dialog.show();
        }

    }


    public void facebookLoginClicked(View button) {
        mSignActivity.requestFacebookSessionOpen();
    }
    public void googleLoginClicked(View button) {
        mSignActivity.requestGoogleSessionOpen();
    }
    public void paycoLoginClicked(View button) {
        mSignActivity.requestPaycoSessionOpen();
    }

    public void emailLoginClose(View button) {

        final Animation animationFade = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein);
        animationFade.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                aq.id(R.id.title_layout).text("로그인").typeface(CommonUtility.getNanumBarunTypeface());
                aq.id(R.id.button_area).visible();


            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //aq.id(R.id.email_form).gone();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        aq.id(R.id.button_area).getView().startAnimation(animationFade);

        Animation internalAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.fadeout);
        internalAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                aq.id(R.id.btn_close).gone();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                aq.id(R.id.email_form).gone();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        aq.id(R.id.email_form).getView().startAnimation(internalAnim);
        hideKeyboard(aq.id(R.id.input_email_text).getEditText());
    }

    public void emailLoginClicked(View button) {

        final Animation animationFade = AnimationUtils.loadAnimation(getActivity(), R.anim.fadein);
        animationFade.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                aq.id(R.id.title_layout).text("이메일로 로그인").typeface(CommonUtility.getNanumBarunTypeface());
                aq.id(R.id.email_form).visible();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        aq.id(R.id.email_form).getView().startAnimation(animationFade);

        Animation internalAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.fadeout);
        internalAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                aq.id(R.id.btn_close).visible();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                aq.id(R.id.button_area).gone();

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        aq.id(R.id.button_area).getView().startAnimation(internalAnim);
    }


    public void loginClicked(View button) {
        Log.d(TAG, "[loginClicked] ");

        if (mSignActivity.isLoginProcessing) {
            return;
        }

        String emailAddressInput = aq.id(R.id.input_email_text).getText().toString();
        String passwordInput = aq.id(R.id.input_password_text).getText().toString();

        //if (ValidateUtil.checkEmail(emailAddressInput)) {
        //if (ValidateUtil.validPassword(passwordInput) == 1) {
        mSignActivity.requestSessionOpen(getActivity(), emailAddressInput, passwordInput, RewhiteLoginType.TYPE_EMAIL);
        //} else {
        //DUtil.alertShow(mSignActivity, "사용할수 없는 비밀번호입니다.");
        //DialogUtility.showOneButtonAlertDialog(getActivity(), null, "사용할수 없는 비밀번호입니다.", "확인", null);
        //}
        //} else {
        //DUtil.alertShow(mSignActivity, "이메일형식이 유효하지않습니다.");

        //}

    }

    Tracker mTracker;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }

        ctx = getActivity();

        mTracker = ((Bootstrap) getActivity().getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        mTracker.setScreenName(TAG);
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        String message = getArguments().getString(KEY_CONTENT);
        mSignActivity = (SignActivity) getActivity();
        mView = (ViewGroup) inflater.inflate(R.layout.fragment_signmain_login, container, false);
        aq = new AQuery(mSignActivity, mView);


        Animation riseupAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.riseup);

        aq.id(R.id.email_form).gone();
        aq.id(R.id.btn_close).gone().clicked(this, "emailLoginClose");

        aq.id(R.id.btn_kakao).clicked(this, "kakaoLoginClicked").animate(riseupAnim);
        aq.id(R.id.btn_facebook).clicked(this, "facebookLoginClicked").animate(riseupAnim);
        aq.id(R.id.btn_google).clicked(this, "googleLoginClicked").animate(riseupAnim);
        aq.id(R.id.btn_payco).clicked(this, "paycoLoginClicked").animate(riseupAnim);
        aq.id(R.id.btn_email).clicked(this, "emailLoginClicked").animate(riseupAnim);
        aq.id(R.id.btn_login).clicked(this, "loginClicked");

        aq.id(R.id.btn_clear_01).tag(0).clicked(this, "inputClearClicked").gone();
        aq.id(R.id.btn_clear_02).tag(1).clicked(this, "inputClearClicked").gone();

        aq.id(R.id.btn_find_password).clicked(this, "findPasswordClicked");
        aq.id(R.id.layout_arrow).clicked(this, "transitionLoginClicked");

        input_email_text = (TextView) mView.findViewById(R.id.input_email_text);
        input_password_text = (TextView) mView.findViewById(R.id.input_password_text);

        aq.id(R.id.title_layout).typeface(CommonUtility.getNanumBarunLightTypeface());
        aq.id(input_email_text).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(input_password_text).typeface(CommonUtility.getNanumBarunTypeface());

        Animation dropdownAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.dropdown);
        aq.id(R.id.title_layout).animate(dropdownAnim);
        aq.id(R.id.down_arrow_join).animate(dropdownAnim);

        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        input_email_text.setOnFocusChangeListener(new OnFocusChangeListener() {

            public void onFocusChange(View view, boolean hasfocus) {

                if (hasfocus) {
                    if (input_email_text.getText().length() > 0) {
                        aq.id(R.id.btn_clear_01).visible();
                    }
                } else {
                    aq.id(R.id.btn_clear_01).gone();
                }
            }
        });
        input_password_text.setOnFocusChangeListener(new OnFocusChangeListener() {

            public void onFocusChange(View view, boolean hasfocus) {
                if (hasfocus) {
                    if (input_password_text.getText().length() > 0) {
                        aq.id(R.id.btn_clear_02).visible();
                    }
                    aq.id(R.id.btn_find_password).gone();
                } else {
                    aq.id(R.id.btn_clear_02).gone();
                    aq.id(R.id.btn_find_password).visible();
                }
            }
        });

        input_email_text.addTextChangedListener(emailTextWatcher);
        input_password_text.addTextChangedListener(passwordTextWatcher);

        return mView;
    }

    private final TextWatcher emailTextWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable edit) {
            if (edit.length() == 0) {
                //mButtonDone.setEnabled(true);
                aq.id(R.id.btn_clear_01).gone();
            } else {
                aq.id(R.id.btn_clear_01).visible();
                //mButtonDone.setEnabled(false);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
        }
    };
    private final TextWatcher passwordTextWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable edit) {
            if (edit.length() == 0) {
                //mButtonDone.setEnabled(true);
                aq.id(R.id.btn_clear_02).gone();
            } else {
                aq.id(R.id.btn_clear_02).visible();
                //mButtonDone.setEnabled(false);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
        }
    };

    public void hideKeyboard(EditText editText) {
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public void transitionLoginClicked(View button) {
        Log.d(TAG, "[transitionLoginClicked] ");
        mSignActivity.buttonTransitionClicked(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }

    public void findPasswordClicked(View button) {

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("가입및탈퇴")
                .setAction("비번찾기")
                //.setLabel("")
                .build());

        Log.i(TAG, "[findPasswordClicked] ");
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(getActivity(), FindPasswordActivity.class);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }, 500);
    }


    /**
     * 각 로그인 방법들의 text, icon, 실제 AuthTYpe들을 담고 있는 container class.
     */
    private static class Item {
        final int textId;
        public final int icon;
        final int contentDescId;
        final AuthType authType;
        Item(final int textId, final Integer icon, final int contentDescId, final AuthType authType) {
            this.textId = textId;
            this.icon = icon;
            this.contentDescId = contentDescId;
            this.authType = authType;
        }
    }


}
