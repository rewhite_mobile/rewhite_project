package me.rewhite.users.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.AlarmSettingActivity;
import me.rewhite.users.activity.CouponActivityV2;
import me.rewhite.users.activity.CustomerActivity;
import me.rewhite.users.activity.EventListActivity;
import me.rewhite.users.activity.LocationActivity;
import me.rewhite.users.activity.MainActivity;
import me.rewhite.users.activity.OrderHistoryActivity;
import me.rewhite.users.activity.ProfileActivity;
import me.rewhite.users.activity.ShareActivity;
import me.rewhite.users.activity.SpecialOrderActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.MZValidator;
import me.rewhite.users.util.SharedPreferencesUtility;

//import com.pkmmte.view.CircularImageView;

/**
 * Fragment used for managing interactions for and presentation of a navigation
 * drawer. See the <a href=
 * "https://developer.android.com/design/patterns/navigation-drawer.html#Interaction"
 * > design guidelines</a> for a complete explanation of the behaviors
 * implemented here.
 */
public class LeftMenuFragment extends Fragment {

    public final static String ITEM_TITLE = "title";
    public final static String ITEM_ICON = "icon";
    SharedPreferences preferences;
    private AQuery aq;
    private Handler mHandler;
    private float ratio;
    Tracker mTracker;

    public LeftMenuFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();

        setData();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTracker = ((Bootstrap) getActivity().getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
    }

    private void setData() {
/*
        CircularImageView circularImageView = (CircularImageView) getActivity()
                .findViewById(R.id.profile_image);
        circularImageView.addShadow();*/

        String userName = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME);
        String useAvailablePoint = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USE_AVAILABLE_POINT);
        String imageThumbPath = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.PROFILE_THUMB);

        ImageOptions options = new ImageOptions();
        options.round = 60;
        options.ratio = 1.0f;
        options.memCache = true;
        options.targetWidth = 100;
        options.animation = AQuery.FADE_IN_NETWORK;


        aq.id(R.id.profile_image).image(imageThumbPath, options);
        aq.id(R.id.profile_name).text(userName)
                .typeface(CommonUtility.getNanumBarunTypeface());

        if(useAvailablePoint == null || useAvailablePoint == ""){
            useAvailablePoint = "0";
        }
        int profilePoint  = Integer.parseInt(useAvailablePoint);
        aq.id(R.id.profile_point).text(MZValidator.toNumFormat(profilePoint) + " P").typeface(
                CommonUtility.getNanumBarunLightTypeface());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of
        // actions in the action bar.
        setHasOptionsMenu(true);

        preferences = getActivity().getSharedPreferences(Constants.PREF_NAME,
                Context.MODE_PRIVATE);

        aq = new AQuery(getActivity());
        // Enable hardware acceleration if the device has API 11 or above
        aq.hardwareAccelerated11();

        setData();

        PackageManager m = getActivity().getPackageManager();
        // 설치된 패키지의 버전이름 추출 : 1.0.0 스타일
        String app_ver = "";
        try {
            app_ver = m.getPackageInfo(getActivity().getPackageName(), 0).versionName;
            aq.id(R.id.text_version).text("버전정보 v" + app_ver);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        //aq.id(R.id.btn_cs_call).clicked(this, "csCallAction");

        aq.id(R.id.menu_title_01).typeface(
                CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.menu_title_02).typeface(
                CommonUtility.getNanumBarunTypeface());

        aq.id(R.id.btn_menu_01).clicked(this, "menuClick").tag(1);
        aq.id(R.id.btn_menu_02).clicked(this, "menuClick").tag(2);
        aq.id(R.id.btn_menu_03).clicked(this, "menuClick").tag(3);

        // recommend
        aq.id(R.id.menu_01).clicked(this, "menuClick").tag(4);
        // event
        aq.id(R.id.menu_03).clicked(this, "menuClick").tag(6);
        // CS
        aq.id(R.id.menu_02).clicked(this, "menuClick").tag(5);


        aq.id(R.id.myinfo_area).clicked(this, "menuClick").tag(0);
        //aq.id(R.id.profile_area).clicked(this, "menuClick").tag(0);
        //aq.id(R.id.point_area).clicked(this, "menuClick").tag(6);

        aq.id(R.id.btn_season).clicked(this, "seasonClick");


    }

    public void seasonClick(View button){
        if (getActivity() instanceof MainActivity) {
            final MainActivity ra = (MainActivity) getActivity();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("주문")
                            .setAction("시즌세탁주문")
                            //.setLabel("")
                            .build());

                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setClass(ra, SpecialOrderActivity.class);
                    ra.startActivity(intent);
                    ra.overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);

                }
            }, 100);
        }

    }

    public void alarmClicked(View button){

        Log.e("alarmClicked", "alarm button clicked");

        if (getActivity() instanceof MainActivity) {
            final MainActivity ra = (MainActivity) getActivity();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setClass(ra, AlarmSettingActivity.class);
                    ra.startActivity(intent);
                    ra.overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                }
            }, 100);
        }
    }

    public void csCallAction(View button) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setCancelable(true).setMessage("리화이트 고객센터와 전화통화를 원하십니까?")
                .setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("메뉴이동")
                                .setAction("고객센터전화걸기")
                                //.setLabel("")
                                .build());
                        call();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void call() {
        Intent callIntent = new Intent();
        callIntent.setAction("android.intent.action.DIAL");
        callIntent.setData(Uri.parse("tel:15442951"));
        startActivity(callIntent);
    }

    public void menuClick(View button) {
        int tag = (int) button.getTag();

        if (getActivity() instanceof MainActivity) {
            final MainActivity ra = (MainActivity) getActivity();
            switch (tag) {
                case 0:
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("메뉴이동")
                                    .setAction("프로필수정")
                                    //.setLabel("프로필수정")
                                    .build());

                            Intent intent = new Intent();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.setClass(ra, ProfileActivity.class);
                            ra.startActivity(intent);
                            ra.overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                        }
                    }, 100);
                    break;
                case 1:
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("메뉴이동")
                                    .setAction("배송지관리")
                                    //.setLabel("")
                                    .build());
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("가입및탈퇴")
                                    .setAction("주소등록시도")
                                    //.setLabel("프로필수정")
                                    .build());

                            Intent intent = new Intent();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.setClass(ra, LocationActivity.class);
                            ra.startActivity(intent);
                            ra.overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                        }
                    }, 100);
                    break;
                case 2:
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("메뉴이동")
                                    .setAction("주문내역")
                                    //.setLabel("")
                                    .build());
                            Intent intent = new Intent();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.setClass(ra, OrderHistoryActivity.class);
                            ra.startActivity(intent);
                            ra.overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                        }
                    }, 100);

                    break;
                case 3:
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("메뉴이동")
                                    .setAction("내지갑")
                                    //.setLabel("")
                                    .build());
                            Intent intent = new Intent();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.setClass(ra, CouponActivityV2.class);
                            ra.startActivity(intent);
                            ra.overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                        }
                    }, 100);
                    break;
                case 4:
                    // 공유하기
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("메뉴이동")
                                    .setAction("추천하기")
                                    //.setLabel("")
                                    .build());
                            Intent intent = new Intent();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.setClass(ra, ShareActivity.class);
                            ra.startActivity(intent);
                            ra.overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                        }
                    }, 100);
                    break;
                case 5:
                    // 고객지원
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("메뉴이동")
                                    .setAction("고객센터")
                                    //.setLabel("")
                                    .build());
                            Intent intent = new Intent();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.setClass(ra, CustomerActivity.class);
                            ra.startActivity(intent);
                            ra.overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                        }
                    }, 100);
                    break;
                case 6:
                    // 이벤트
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("메뉴이동")
                                    .setAction("이벤트")
                                    //.setLabel("")
                                    .build());
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("이벤트")
                                    .setAction("이벤트터치")
                                    //.setLabel("")
                                    .build());
                            AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
                            logger.logEvent("Touch Event");

                            Intent intent = new Intent();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.setClass(ra, EventListActivity.class);
                            ra.startActivity(intent);
                            ra.overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                        }
                    }, 100);
                    break;

                default:
                    break;
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_navigation_drawer, null);
    }

}
