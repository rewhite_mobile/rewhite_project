package me.rewhite.users.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.androidquery.AQuery;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.rewhite.users.R;
import me.rewhite.users.activity.ProfileNewActivityV2;
import me.rewhite.users.util.DUtil;


public class ProfileNewNicknameFragment extends ProfileNewBaseFragment {

    AQuery aq;
    EditText e1;
    ProfileNewActivityV2 parent;

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable edit) {
            String s1 = e1.getText().toString();
            if (s1.length() >= 1) {
                aq.id(R.id.btn_next).visible();
            } else {
                aq.id(R.id.btn_next).gone();
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    /** 이모티콘이 있을경우 "" 리턴, 그렇지 않을 경우 null 리턴 **/
    InputFilter specialCharacterFilter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                // 이모티콘 패턴
                Pattern unicodeOutliers = Pattern.compile("[\\uD83C-\\uDBFF\\uDC00-\\uDFFF]+");
                // '-' 입력 받고 싶을 경우 : unicodeOutliers.matcher(source).matches() && !source.toString().matches(".*-.*")
                if(unicodeOutliers.matcher(source).matches()) {
                    return "";
                }
            }
            return null;
        }
    };


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(context).inflate(R.layout.fragment_profile_new_nickname, null);

        parent = (ProfileNewActivityV2)getActivity();
        aq = new AQuery(view);
        aq.id(R.id.btn_next).gone();
        aq.id(R.id.btn_next).clicked(this, "nicknameSubmitAction");

        e1 = (EditText) view.findViewById(R.id.input_nickname);

        if(parent.inputedNickname != null){
            e1.setText(parent.inputedNickname);
        }
        // 이모티콘 필터링
        e1.setFilters(new InputFilter[]{specialCharacterFilter});
        if (e1 != null) {
            e1.addTextChangedListener(textWatcher);
            String s1 = e1.getText().toString();
            if (s1.length() >= 1) {
                aq.id(R.id.btn_next).visible();
            } else {
                aq.id(R.id.btn_next).gone();
            }
        }

        return view;
    }

    private Boolean validNickname(String _value){
        if(e1.getText().toString().length() >= 3){
            boolean flag = Pattern.matches("^[a-zA-Zㄱ-ㅎㅏ-ㅣ가-힣0-9!@.#$%^&*?_~\\s\\s+]*$", _value);
            return flag;
        }else{
            return false;
        }
    }

    public String removeTag(String str){
        Matcher mat;

// script 처리
        Pattern script = Pattern.compile("<(no)?script[^>]*>.*?</(no)?script>",Pattern.DOTALL);
        mat = script.matcher(str);
        str = mat.replaceAll("");

// style 처리
        Pattern style = Pattern.compile("<style[^>]*>.*</style>",Pattern.DOTALL);
        mat = style.matcher(str);
        str = mat.replaceAll("");

// tag 처리
        Pattern tag = Pattern.compile("<(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>");
        mat = tag.matcher(str);
        str = mat.replaceAll("");

// ntag 처리
        Pattern ntag = Pattern.compile("<\\w+\\s+[^<]*\\s*>");
        mat = ntag.matcher(str);
        str = mat.replaceAll("");

// entity ref 처리
        Pattern Eentity = Pattern.compile("&[^;]+;");
        mat = Eentity.matcher(str);
        str = mat.replaceAll("");

// whitespace 처리
//        Pattern wspace = Pattern.compile("\\s\\s+");
//        mat = wspace.matcher(str);
//        str = mat.replaceAll("");

        return str ;

    }

    public void nicknameSubmitAction(View button){
        String nicknameValue = removeTag(e1.getText().toString());
        if(validNickname(nicknameValue)){
            parent.inputedNickname = nicknameValue;
            parent.processNext();
        }else{
            DUtil.alertShow(getActivity(), "특수문자가 포함되지않은 이름으로 3자이상 입력해주세요.");
        }
    }

    @Override
    public void initialize() {


    }

}
