package me.rewhite.users.fragment;

//import com.actionbarsherlock.app.SherlockFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.EmailJoinActivity;
import me.rewhite.users.activity.SignActivity;
import me.rewhite.users.activity.TermsDetailView;
import me.rewhite.users.common.Constants;


public class SignMainJoinFragment extends Fragment {

    private static final String TAG = "SignMainJoinFragment";
    private static final String KEY_CONTENT = "SignMainJoinFragment";
    private SignActivity mSignActivity = null;
    private ViewGroup mView;
    private AQuery aq;
    private Context ctx;
    static SignActivity parent;
    Tracker mTracker;

    public SignMainJoinFragment() {
    }

    public static SignMainJoinFragment newInstance(String content) {
        SignMainJoinFragment fragment = new SignMainJoinFragment();
        Bundle bdl = new Bundle();
        bdl.putString(KEY_CONTENT, content);

        fragment.setArguments(bdl);

        return fragment;
    }

    private String mContent = "???";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }

        mTracker = ((Bootstrap) getActivity().getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        mTracker.setScreenName(TAG);
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        ctx = getActivity();
        mSignActivity = (SignActivity) getActivity();
        mView = (ViewGroup) inflater.inflate(R.layout.fragment_signmain_top, container, false);
        aq = new AQuery(mSignActivity, mView);


        aq.id(R.id.layout_arrow).clicked(this, "transitionLoginClicked");
        //aq.id(R.id.btn_register).clicked(this, "joinClicked");

        aq.id(R.id.button_area).clicked(this, "termsAgreeClicked");

        aq.id(R.id.checkbox_01).clicked(this, "checkboxClick").tag("check01");
        aq.id(R.id.checkbox_02).clicked(this, "checkboxClick").tag("check02");
        aq.id(R.id.checkbox_03).clicked(this, "checkboxClick").tag("check03");
        aq.id(R.id.checkbox_04).clicked(this, "checkboxClick").tag("check04");
        aq.id(R.id.checkbox_05).clicked(this, "checkboxClick").tag("check05");

        aq.id(R.id.terms_detail_1).clicked(this, "detailClick").tag("check01");
        aq.id(R.id.terms_detail_2).clicked(this, "detailClick").tag("check02");
        aq.id(R.id.terms_detail_3).clicked(this, "detailClick").tag("check03");
        aq.id(R.id.terms_detail_4).clicked(this, "detailClick").tag("check04");
        aq.id(R.id.terms_detail_5).clicked(this, "detailClick").tag("check05");

        return mView;
    }

    public void transitionLoginClicked(View button) {
        Log.d(TAG, "[transitionLoginClicked] ");
        mSignActivity.buttonTransitionClicked(true);
    }

    public void detailClick(View Button) {

        Log.i("checkbox", Button.getTag().toString());
        Intent intent;
        if ("check01".equals(Button.getTag().toString())) {
            intent = new Intent(getActivity(), TermsDetailView.class);
            intent.putExtra("TERMS_TITLE", "이용약관");
            intent.putExtra("TERMS_URI", Constants.TERMS_USE);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if ("check02".equals(Button.getTag().toString())) {
            intent = new Intent(getActivity(), TermsDetailView.class);
            intent.putExtra("TERMS_TITLE", "개인정보 취급방침");
            intent.putExtra("TERMS_URI", Constants.TERMS_PRIVACY);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if ("check03".equals(Button.getTag().toString())) {
            intent = new Intent(getActivity(), TermsDetailView.class);
            intent.putExtra("TERMS_TITLE", "개인정보 제3자 제공동의");
            intent.putExtra("TERMS_URI", Constants.TERMS_PRIVATE_SUPPORT);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if ("check04".equals(Button.getTag().toString())) {
            intent = new Intent(getActivity(), TermsDetailView.class);
            intent.putExtra("TERMS_TITLE", "위치기반 서비스 이용약관");
            intent.putExtra("TERMS_URI", Constants.TERMS_LOCATION);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if ("check05".equals(Button.getTag().toString())) {
            intent = new Intent(getActivity(), TermsDetailView.class);
            intent.putExtra("TERMS_TITLE", "개인정보 제3자 제공동의 (선택)");
            intent.putExtra("TERMS_URI", Constants.TERMS_PRIVATE_THIRDS_SUPPORT);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    private boolean isChecked01 = false;
    private boolean isChecked02 = false;
    private boolean isChecked03 = false;
    private boolean isChecked04 = false;
    private boolean isChecked05 = false;

    public void checkboxClick(View Button) {

        Log.i("checkbox", Button.getTag().toString());

        if ("check01".equals(Button.getTag().toString())) {

            if (isChecked01) {
                isChecked01 = false;
                aq.id(R.id.checkbox_01).image(R.drawable.checkbox_off_term);
                aq.id(R.id.checkbox_01).getView().setAlpha(0.5f);
            } else {
                isChecked01 = true;
                aq.id(R.id.checkbox_01).image(R.drawable.checkbox_term);
                aq.id(R.id.checkbox_01).getView().setAlpha(1.0f);
            }

        } else if ("check02".equals(Button.getTag().toString())) {
            if (isChecked02) {
                isChecked02 = false;
                aq.id(R.id.checkbox_02).image(R.drawable.checkbox_off_term);
                aq.id(R.id.checkbox_02).getView().setAlpha(0.5f);
            } else {
                isChecked02 = true;
                aq.id(R.id.checkbox_02).image(R.drawable.checkbox_term);
                aq.id(R.id.checkbox_02).getView().setAlpha(1.0f);
            }
        } else if ("check03".equals(Button.getTag().toString())) {
            if (isChecked03) {
                isChecked03 = false;
                aq.id(R.id.checkbox_03).image(R.drawable.checkbox_off_term);
                aq.id(R.id.checkbox_03).getView().setAlpha(0.5f);
            } else {
                isChecked03 = true;
                aq.id(R.id.checkbox_03).image(R.drawable.checkbox_term);
                aq.id(R.id.checkbox_03).getView().setAlpha(1.0f);
            }
        } else if ("check04".equals(Button.getTag().toString())) {
            if (isChecked04) {
                isChecked04 = false;
                aq.id(R.id.checkbox_04).image(R.drawable.checkbox_off_term);
                aq.id(R.id.checkbox_04).getView().setAlpha(0.5f);
            } else {
                isChecked04 = true;
                aq.id(R.id.checkbox_04).image(R.drawable.checkbox_term);
                aq.id(R.id.checkbox_04).getView().setAlpha(1.0f);
            }
        } else if ("check05".equals(Button.getTag().toString())) {
            if (isChecked05) {
                isChecked05 = false;
                aq.id(R.id.checkbox_05).image(R.drawable.checkbox_off_term);
                aq.id(R.id.checkbox_05).getView().setAlpha(0.5f);
            } else {
                isChecked05 = true;
                aq.id(R.id.checkbox_05).image(R.drawable.checkbox_term);
                aq.id(R.id.checkbox_05).getView().setAlpha(1.0f);
            }
        }

        if (checkAllAgreed()) {
            allAgreed();
        }

    }

    public boolean checkAllAgreed() {
        return isChecked01 && isChecked02 && isChecked03 && isChecked04;
        /*if (isChecked01 && isChecked02 && isChecked03 && isChecked04) {
            return true;
        } else {
            //MZError.alertWithErrorMessage(this, "약관에 동의해주세요");
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    ctx);
            alertDialogBuilder.setMessage(
                    "약관에 동의해주세요.")
                    .setCancelable(true)
                    .setNegativeButton("확인", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            return false;
        }*/
    }

    public void termsAgreeClicked(View Button) {

        isChecked01 = true;
        aq.id(R.id.checkbox_01).image(R.drawable.checkbox_term);
        isChecked02 = true;
        aq.id(R.id.checkbox_02).image(R.drawable.checkbox_term);
        isChecked03 = true;
        aq.id(R.id.checkbox_03).image(R.drawable.checkbox_term);
        isChecked04 = true;
        aq.id(R.id.checkbox_04).image(R.drawable.checkbox_term);
        isChecked05 = true;
        aq.id(R.id.checkbox_05).image(R.drawable.checkbox_term);

        aq.id(R.id.checkbox_01).getView().setAlpha(1.0f);
        aq.id(R.id.checkbox_02).getView().setAlpha(1.0f);
        aq.id(R.id.checkbox_03).getView().setAlpha(1.0f);
        aq.id(R.id.checkbox_04).getView().setAlpha(1.0f);
        aq.id(R.id.checkbox_05).getView().setAlpha(1.0f);

        if (checkAllAgreed()) {
            allAgreed();
        }
    }


    public void allAgreed() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mSignActivity, EmailJoinActivity.class);
                if(isChecked05){
                    intent.putExtra("isAgreeThirdOptinal", "Y");
                }else{
                    intent.putExtra("isAgreeThirdOptinal", "N");
                }

                mSignActivity.startActivity(intent);
                mSignActivity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("가입및탈퇴")
                        .setAction("가입시도")
                        .setLabel("이메일")
                        .build());

            }
        }, 500);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }

}
