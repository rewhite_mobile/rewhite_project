package me.rewhite.users.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.androidquery.AQuery;

import me.rewhite.users.R;
import me.rewhite.users.activity.gsretail.GS25V2OrderInputActivity;
import me.rewhite.users.activity.gsretail.GSOrderRequestContentActivity;
import me.rewhite.users.util.DUtil;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class GS25V2OrderInputStep2Fragment extends Fragment implements IFragment{

    private static final String TAG = "GS25V2OrderInputStep2Fragment";
    View rootView;
    AQuery aq;
    Context mContext;
    String final_request_content_string = "";
    String request_content_string = "";
    boolean isNoRequired = false;

    public GS25V2OrderInputStep2Fragment() {
        // Required empty public constructor
    }

//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_gs25_v2_order_input_02, container, false);
        aq = new AQuery(rootView);
        aq.id(R.id.request_content_layout).clicked(this, "requestContentClicked");
        aq.id(R.id.btn_no_req).clicked(this, "noRequestContentClicked");
        aq.id(R.id.btn_submit).clicked(this, "nextProcessClicked");
        return rootView;
    }

    public void noRequestContentClicked(View button){
        isNoRequired = !isNoRequired;
        if(isNoRequired){
            final_request_content_string = "";
            aq.id(R.id.btn_no_req).background(R.drawable.back_gs25_input_step01_blue_selected).textColor(0xffffffff);
            aq.id(R.id.btn_submit).backgroundColor(0xff252525).textColor(0xffffffff).enabled(true);

            aq.id(R.id.request_content_layout).background(R.drawable.back_gs25_input_step01_gray);
        }else{
            aq.id(R.id.btn_no_req).background(R.drawable.back_gs25_input_step01_gray).textColor(0xff909090);
            aq.id(R.id.request_content_layout).background(R.drawable.back_gs25_input_step01);

            if(final_request_content_string.length() == 0){
                aq.id(R.id.btn_submit).backgroundColor(0xffa0a0a0).textColor(0xffd3d3d3).enabled(false);
            }else{
                final_request_content_string = request_content_string;
                aq.id(R.id.btn_no_req).background(R.drawable.back_gs25_input_step01_blue_selected).textColor(0xffffffff);
                aq.id(R.id.btn_submit).backgroundColor(0xff252525).textColor(0xffffffff).enabled(true);
            }

        }

    }

    public void requestContentClicked(View button){
        aq.id(R.id.btn_no_req).background(R.drawable.back_gs25_input_step01_gray).textColor(0xff909090);
        aq.id(R.id.request_content_layout).background(R.drawable.back_gs25_input_step01);
        isNoRequired = false;

        Intent intent = new Intent(getActivity(), GSOrderRequestContentActivity.class);
        intent.putExtra("content", request_content_string);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivityForResult(intent, GS25V2OrderInputActivity.REQUEST_CONTENT_INPUT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GS25V2OrderInputActivity.REQUEST_CONTENT_INPUT:
                    String result = data.getStringExtra("INPUT_CONTENT");
                    request_content_string = result;

                    if(request_content_string.length() == 0){
                        final_request_content_string = "";
                        aq.id(R.id.text_req_content).text("입력하신 내용이 없습니다.");
                        aq.id(R.id.btn_submit).backgroundColor(0xffa0a0a0).textColor(0xffd3d3d3).enabled(false);
                    }else{
                        final_request_content_string = request_content_string;

                        if(getActivity() instanceof GS25V2OrderInputActivity) {
                            final GS25V2OrderInputActivity ra = (GS25V2OrderInputActivity) getActivity();
                            ra.request_content_string = request_content_string;
                        }
                        aq.id(R.id.btn_submit).backgroundColor(0xff252525).textColor(0xffffffff).enabled(true);
                        aq.id(R.id.btn_no_req).background(R.drawable.back_gs25_input_step01_gray).textColor(0xff909090);
                        aq.id(R.id.text_req_content).text(result);
                    }

                    aq.id(R.id.text_content_count).text(final_request_content_string.length() + "/100");

                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onResume() {
        Log.d(this.getClass().getSimpleName(), "onResume()");
        super.onResume();

        if(getActivity() instanceof GS25V2OrderInputActivity) {
            final GS25V2OrderInputActivity ra = (GS25V2OrderInputActivity) getActivity();
            request_content_string = ra.request_content_string;
            final_request_content_string = request_content_string;

            if(request_content_string.length() == 0){
                final_request_content_string = "";
                aq.id(R.id.text_req_content).text("입력하신 내용이 없습니다.");
                aq.id(R.id.btn_submit).backgroundColor(0xffa0a0a0).textColor(0xffd3d3d3).enabled(false);
            }else{
                final_request_content_string = request_content_string;
                aq.id(R.id.btn_submit).backgroundColor(0xff252525).textColor(0xffffffff).enabled(true);
                aq.id(R.id.btn_no_req).background(R.drawable.back_gs25_input_step01_gray).textColor(0xff909090);
                aq.id(R.id.text_req_content).text(final_request_content_string);
            }

            aq.id(R.id.text_content_count).text(final_request_content_string.length() + "/100");
        }
    }

    public void nextProcessClicked(View button){
        if((final_request_content_string.length() == 0 && isNoRequired) || final_request_content_string.length() > 0){
            if(getActivity() instanceof GS25V2OrderInputActivity){
                final GS25V2OrderInputActivity ra = (GS25V2OrderInputActivity)getActivity();
                ra.request_content_string = final_request_content_string;
                ra.showFragment(GS25V2OrderInputActivity.FRAGMENT_STEP3);
            }
        }else{
            DUtil.alertShow(getActivity(), "요청사항을 입력해주세요.");
        }


    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }
}
