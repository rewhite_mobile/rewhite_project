package me.rewhite.users.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.multiviewpager.MultiViewPager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.OrderActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.PickupType;
import me.rewhite.users.layout.WheelView;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.TimeUtil;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PickupReqFragmentV2.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PickupReqFragmentV2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PickupReqFragmentV2 extends Fragment implements IFragment {

    private final static String TAG = "PickupReqFragmentV2";
    private OrderActivity mActivity;

    private static final String STORE_INFO = "storeInfo";
    private JSONObject storeInfo;

    private OnFragmentInteractionListener mListener;

    private ViewGroup mView = null;
    ArrayList<String> TIME_TABLE;
    private WheelView wva;

    Calendar today;
    public AQuery aq;

    ProgressDialog dialog;

    public static PickupReqFragmentV2 newInstance(String storeInfo) {
        PickupReqFragmentV2 fragment = new PickupReqFragmentV2();
        Bundle args = new Bundle();
        args.putString(STORE_INFO, storeInfo);
        fragment.setArguments(args);
        return fragment;
    }

    public PickupReqFragmentV2() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {

        Log.d(TAG, "[onResume]");
        mActivity.setTouchDisable(false);

        super.onResume();

        String titleStr = "";
        if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
            titleStr = "수거 신청 (당일배송)";
        }else if(mActivity.pickupMode == PickupType.PICKUPTYPE_TOMORROW){
            titleStr = "수거 신청 (익일배송)";
        }else{
            titleStr = "수거 신청";
        }
        mActivity.setTitle(titleStr);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = (OrderActivity) getActivity();

        if (getArguments() != null) {
            try {
                storeInfo = new JSONObject(getArguments().getString(STORE_INFO));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        mView = (ViewGroup) inflater.inflate(R.layout.fragment_pickup_req, container, false);

        aq = new AQuery(mView);


        aq.id(R.id.btn_next).clicked(this, "pickupSetupComplete");

        aq.id(R.id.btn_service_01).clicked(this, "serviceSelected").tag(1);
        aq.id(R.id.btn_service_02).clicked(this, "serviceSelected").tag(2);
        if(mActivity.isRepairAvailable){
            aq.id(R.id.btn_service_03).clicked(this, "serviceSelected").tag(3);
        }else{
            aq.id(R.id.btn_service_03).gone();//.enabled(false).image(R.mipmap.btn_order_service_03_disabled);
        }

        dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.loading_comment));

        initTimetableData(0);

        return mView;
    }


    long selectedTimestamp = -1;
    int selectedHour = 0;

    boolean isService01_on = false;
    boolean isService02_on = false;
    boolean isService03_on = false;

    public void serviceSelected(View button) {
        int tag = (int) button.getTag();
        switch (tag) {
            case 1:
                isService01_on = !isService01_on;
                if (isService01_on) {
                    aq.id(R.id.btn_service_01).image(R.mipmap.btn_order_service_01_pres);
                } else {
                    aq.id(R.id.btn_service_01).image(R.drawable.btn_service_sel_01);
                }
                break;
            case 2:
                isService02_on = !isService02_on;
                if (isService02_on) {
                    aq.id(R.id.btn_service_02).image(R.mipmap.btn_order_service_02_pres);
                } else {
                    aq.id(R.id.btn_service_02).image(R.drawable.btn_service_sel_02);
                }
                break;
            case 3:
                isService03_on = !isService03_on;
                if (isService03_on) {
                    aq.id(R.id.btn_service_03).image(R.mipmap.btn_order_service_03_pres);
                } else {
                    aq.id(R.id.btn_service_03).image(R.drawable.btn_service_sel_03);
                }
                break;
            default:
                break;
        }
    }

    JSONArray finalDatePickerContainer;

    private void initTimetableData(long timestamp){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("storeId", mActivity.storeId);
        params.put("addressSeq", mActivity.addressSeq);
        params.put("pickupRequestTime", timestamp);
        params.put("k", 1);
        DUtil.Log("initTimetableData", params.toString());

        NetworkClient.post(Constants.ORDER_TIMETABLE, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_TIMETABLE, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //

                        } else {
                            finalDatePickerContainer = jsondata.getJSONArray("data");
                            setDatePicker();
                            if(finalDatePickerContainer.length() > 0){
                                String dateString = finalDatePickerContainer.getJSONObject(0).getString("date");
                                int day_of_week = Integer.parseInt(dateString.substring(8,9));
                                setYoilText(day_of_week);
                                setTimePicker(0);
                            }

                            DUtil.Log(Constants.ORDER_TIMETABLE, finalDatePickerContainer.toString());
                        }

                    } else {
                        DUtil.Log(Constants.ORDER_TIMETABLE, jsondata.toString());
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void pickupSetupComplete(View button) {
        AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
        logger.logEvent("Normal order Step1");

        if (selectedTimestamp != -1) {
            if (selectedHour != 0) {
                if (isService01_on || isService02_on || isService03_on) {
                    //Date pickupDate = TimeUtil.getDate(selectedYear, selectedMonth, selectedDay, selectedHour);
                    Log.e("pickup date", selectedTimestamp + " / " + TimeUtil.convertTimestampToString(selectedTimestamp));
                    mActivity.pickupReqTime = selectedTimestamp;
                    String orderReqItem = "000";
                    orderReqItem = String.format("%s%s%s", (isService01_on) ? "1" : "0", (isService02_on) ? "1" : "0", (isService03_on) ? "1" : "0");
                    mActivity.orderRequestItem = orderReqItem;
                    Log.i("PickupReqTime", mActivity.pickupReqTime + "");

                    Tracker t = ((Bootstrap) getActivity().getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("주문")
                            .setAction("수거일자선택")
                            //.setLabel("Order")
                            .build());

                    mActivity.showFragment(OrderActivity.FRAGMENT_DELIVERY);

                } else {
                    DUtil.alertShow(getActivity(), "원하시는 서비스를 선택해주세요");
                }

            } else {
                DUtil.alertShow(getActivity(), "시간을 선택해주세요");
            }
        } else {
            DUtil.alertShow(getActivity(), "날짜를 선택해주세요");
        }
    }

    private void setDatePicker() {
        MultiViewPager pager = (MultiViewPager) mView.findViewById(R.id.pager);

        FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(getChildFragmentManager()) {

            @Override
            public int getCount() {
                return finalDatePickerContainer.length();
            }

            @Override
            public DatePickFragment getItem(int position) {

                try {
                    String dateString = finalDatePickerContainer.getJSONObject(position).getString("date");
                    return DatePickFragment.create(Integer.parseInt(dateString.substring(6,8)));
                } catch (JSONException e) {
                    e.printStackTrace();
                    return DatePickFragment.create(0);
                }
            }
        };

        pager.setAdapter(adapter);
        pager.setCurrentItem(selectedCurrentPositionDate);
        pager.setOnPageChangeListener(new MultiViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selectedCurrentPositionDate = position;
                String dateString = null;
                try {
                    if(finalDatePickerContainer.length() > position){
                        dateString = finalDatePickerContainer.getJSONObject(position).getString("date");
                        int day_of_week = Integer.parseInt(dateString.substring(8));
                        setYoilText(day_of_week);
                        setTimePicker(position);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setYoilText(int value) {
        String m_week = "";

        if (value == 0 || value == 7)
            m_week = "일요일";
        else if (value == 1)
            m_week = "월요일";
        else if (value == 2)
            m_week = "화요일";
        else if (value == 3)
            m_week = "수요일";
        else if (value == 4)
            m_week = "목요일";
        else if (value == 5)
            m_week = "금요일";
        else if (value == 6)
            m_week = "토요일";


        aq.id(R.id.yoil_text).text(m_week).typeface(CommonUtility.getNanumBarunTypeface());
    }


    int selectedCurrentPositionDate = 0;
    int wheelSelectedIndex = 0;

    private String getDisplayTimeString(String _timeString){
        int timeTemp = Integer.parseInt(_timeString.substring(0,2));
        String temp = "";

        if(timeTemp < 12){
            temp = "오전 " + _timeString.substring(0,2) + "시~" +  _timeString.substring(3,5) + "시";
        }else if(timeTemp == 12){
            temp = "낮 " + _timeString.substring(0,2) + "시~" +  _timeString.substring(3,5) + "시";
        }else{
            temp = "오후 " + _timeString.substring(0,2) + "시~" +  _timeString.substring(3,5) + "시";
        }

        return temp;
    }

    private void initTimestamp(final int position) throws JSONException {
        selectedHour = Integer.parseInt(finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").getString(wheelSelectedIndex).substring(0,2));
        String selDate = finalDatePickerContainer.getJSONObject(position).getString("date").substring(0,8);
        selectedTimestamp = TimeUtil.getOrderDateTimestamp(selDate + finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").getString(wheelSelectedIndex).substring(0,2)).getTime();
    }

    private void setTimePicker(final int position) throws JSONException {

        wheelSelectedIndex = 0;
        TIME_TABLE = new ArrayList<String>();
        for(int i = 0 ; i < finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").length(); i++){
            String displayHour = getDisplayTimeString(finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").getString(i));
            TIME_TABLE.add(i,displayHour);
        }

        initTimestamp(position);

        wva = (WheelView) mView.findViewById(R.id.wheel_view_wv);
        wva.setItems(TIME_TABLE);
        wva.setOffset(wheelSelectedIndex+1);
        //wva.setSeletion(wheelSelectedIndex+1);
        wva.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                // selectedIndex 1부터 시작
                if (selectedIndex > TIME_TABLE.size()) {
                    selectedIndex = TIME_TABLE.size();
                }
                if(selectedIndex < 1){
                    selectedIndex = 1;
                }
                wheelSelectedIndex = selectedIndex;

                try {
                    Log.i(TAG, "selectedIndex: " + finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").getString(selectedIndex-1).substring(0,2) + "시, item: " + item);
                    selectedHour = Integer.parseInt(finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").getString(selectedIndex-1).substring(0,2));
                    String selDate = finalDatePickerContainer.getJSONObject(position).getString("date").substring(0,8);
                    selectedTimestamp = TimeUtil.getOrderDateTimestamp(selDate + finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").getString(selectedIndex-1).substring(0,2)).getTime();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        //Log.e("pickup date", selectedTimestamp + "/" + selectedHour);

        if(dialog != null){
            dialog.dismiss();
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
