package me.rewhite.users.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.R;
import me.rewhite.users.activity.gsretail.GS25V2OrderInputClientActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class GS25V2OrderInputStep5Fragment extends Fragment implements IFragment{

    private static final String TAG = "GS25V2OrderInputStep5Fragment";
    View rootView;
    String finalOrderId;
    JSONObject orderInfo;
    AQuery aq;
    Context mContext;
    GS25V2OrderInputClientActivity ra;

    String partnerOrderNo;
    int count_pack;

    String partnerId;
    int internalStoreId;

    public GS25V2OrderInputStep5Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_gs25_v2_order_input_05, container, false);
        aq = new AQuery(rootView);

        ra = (GS25V2OrderInputClientActivity)getActivity();
        if(getActivity() instanceof GS25V2OrderInputClientActivity){
            final GS25V2OrderInputClientActivity ra = (GS25V2OrderInputClientActivity)getActivity();
            partnerId = ra.partnerId;
            internalStoreId = ra.internalStoreId;
        }

        Log.e("GS25V2OrderInputStep5Fragment", internalStoreId + " , " + partnerId);
        if(internalStoreId >= 20000){
            aq.id(R.id.text_bottom_desc).text("· 주문번호 라벨지는 플레이스에 비치되어 있어요.");
        }else{
            aq.id(R.id.text_bottom_desc).text("· 주문번호 라벨지는 편의점에 비치되어 있어요.");
        }


        finalOrderId = ra.finalOrderId;
        getOrderInfo(finalOrderId);

        aq.id(R.id.btn_submit).clicked(this, "nextProcessClicked");

        return rootView;
    }

    public void nextProcessClicked(View button){
        if(getActivity() instanceof GS25V2OrderInputClientActivity){
            ra.showEndPopup();
        }
    }


    private void getOrderInfo(String orderId){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_DETAIL, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            orderInfo = jsondata.getJSONObject("data");
                            partnerOrderNo = orderInfo.getString("partnerOrderNo");
                            count_pack = orderInfo.getInt("packQuantity");

                            initializeLayout(partnerOrderNo, count_pack);
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });


    }



    private void initializeLayout(String orderId, int count){

        LinearLayout linearLayout = (LinearLayout)getActivity().findViewById(R.id.label_container);

        for(int i = 1; i < count+1; i++){
            String text = "<font color=#000000><b>"+orderId.substring(0,5)+"</b></font><font color=#21bae4><b>"+orderId.substring(5,10)+"_"+i+"</b></font>";
            //textView.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
            TextView tv = new TextView(getActivity());
            tv.setText(Html.fromHtml(text));
            tv.setTextSize((float) 32);
            tv.setTextScaleX(0.95f);
            tv.setGravity(Gravity.CENTER_HORIZONTAL);
            linearLayout.addView(tv);
        }

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }
}
