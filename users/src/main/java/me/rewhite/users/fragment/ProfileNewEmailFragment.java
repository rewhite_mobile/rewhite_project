package me.rewhite.users.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.androidquery.AQuery;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.rewhite.users.R;
import me.rewhite.users.activity.ProfileNewActivityV2;
import me.rewhite.users.util.DUtil;


public class ProfileNewEmailFragment extends ProfileNewBaseFragment {

    AQuery aq;
    EditText e1;
    ProfileNewActivityV2 parent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(context).inflate(R.layout.fragment_profile_new_email, null);
        parent = (ProfileNewActivityV2)getActivity();
        aq = new AQuery(view);

        aq.id(R.id.btn_next).clicked(this, "emailSubmitAction");

        e1 = (EditText) view.findViewById(R.id.input_email);

        if(parent.inputedEmail != null){
            e1.setText(parent.inputedEmail);
        }

        return view;
    }

    private Boolean validEmail(String _value){
        String mail = "^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$";
        Pattern p = Pattern.compile(mail);
        Matcher m = p.matcher(_value);

        return m.matches();
    }

    public void emailSubmitAction(View button){
        if(validEmail(e1.getText().toString())){
            parent.inputedEmail = e1.getText().toString();
            parent.processNext();
        }else{
            DUtil.alertShow(getActivity(), "이메일주소 형식이 잘못되었습니다.");
        }
    }

    @Override
    public void initialize() {


    }

}
