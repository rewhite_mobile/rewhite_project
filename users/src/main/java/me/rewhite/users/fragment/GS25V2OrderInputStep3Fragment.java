package me.rewhite.users.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import me.rewhite.users.R;
import me.rewhite.users.activity.gsretail.GS25V2OrderInputActivity;
import me.rewhite.users.util.DUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class GS25V2OrderInputStep3Fragment extends Fragment implements IFragment{

    private static final String TAG = "GS25V2OrderInputStep3Fragment";
    View rootView;
    AQuery aq;
    Context mContext;
    String partnerId;
    int internalStoreId;

    boolean isAgreed01 = false;
    boolean isAgreed02 = false;


    public GS25V2OrderInputStep3Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_gs25_v2_order_input_03, container, false);
        aq = new AQuery(rootView);

        if(getActivity() instanceof GS25V2OrderInputActivity){
            final GS25V2OrderInputActivity ra = (GS25V2OrderInputActivity)getActivity();
            partnerId = ra.partnerId;
            internalStoreId = Integer.parseInt(ra.internalStoreId);
        }

        if(internalStoreId >= 20000){
            aq.id(R.id.btn_agree_01).image(R.mipmap.btn_theplace_order_input_step3_01_normal);
            aq.id(R.id.btn_agree_02).image(R.mipmap.btn_theplace_order_input_step3_02_normal);
        }else{

        }

        aq.id(R.id.btn_agree_01).clicked(this, "elementAgreed").tag(1);
        aq.id(R.id.btn_agree_02).clicked(this, "elementAgreed").tag(2);
        aq.id(R.id.btn_submit).clicked(this, "nextProcessClicked");

        return rootView;
    }

    public void nextProcessClicked(View button){
        if(isAgreed01 && isAgreed02){
            if(getActivity() instanceof GS25V2OrderInputActivity){
                final GS25V2OrderInputActivity ra = (GS25V2OrderInputActivity)getActivity();
                ra.showEndPopup();
            }
        }else{
            DUtil.alertShow(getActivity(), "유의사항을 동의해주세요.");
        }


    }

    public void elementAgreed(View button){
        int tag = (int)button.getTag();



        switch (tag){
            case 1:
                isAgreed01 = !isAgreed01;
                if(isAgreed01){
                    if(internalStoreId >= 20000){
                        aq.id(R.id.btn_agree_01).image(R.mipmap.btn_theplace_order_input_step3_01_selected);
                    }else{
                        aq.id(R.id.btn_agree_01).image(R.mipmap.btn_gs25_order_input_step3_01_selected);
                    }

                }else{
                    if(internalStoreId >= 20000){
                        aq.id(R.id.btn_agree_01).image(R.mipmap.btn_theplace_order_input_step3_01_normal);
                    }else{
                        aq.id(R.id.btn_agree_01).image(R.mipmap.btn_gs25_order_input_step3_01_normal);
                    }

                }
                break;
            case 2:
                isAgreed02 = !isAgreed02;
                if(isAgreed02){
                    if(internalStoreId >= 20000){
                        aq.id(R.id.btn_agree_02).image(R.mipmap.btn_theplace_order_input_step3_02_selected);
                    }else{
                        aq.id(R.id.btn_agree_02).image(R.mipmap.btn_gs25_order_input_step3_02_selected);
                    }

                }else{
                    if(internalStoreId >= 20000){
                        aq.id(R.id.btn_agree_02).image(R.mipmap.btn_theplace_order_input_step3_02_normal);
                    }else{
                        aq.id(R.id.btn_agree_02).image(R.mipmap.btn_gs25_order_input_step3_02_normal);
                    }

                }
                break;
            default:
                break;
        }

        if(isAgreed01 && isAgreed02){
            aq.id(R.id.btn_submit).backgroundColor(0xff252525).textColor(0xffffffff).enabled(true);
        }else{
            aq.id(R.id.btn_submit).backgroundColor(0xffa0a0a0).textColor(0xffd3d3d3).enabled(false);
        }
    }



    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }
//
//    public void submitClicked(View button){
//

//    }
}
