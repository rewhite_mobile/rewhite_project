package me.rewhite.users.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import me.rewhite.users.R;
import me.rewhite.users.activity.ProfileNewActivityV2;
import me.rewhite.users.common.Constants;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.MZValidator;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.ValidateUtil;

public class ProfileNewPhoneFragment extends ProfileNewBaseFragment {

    private final static String TAG = ProfileNewActivityV2.class.getSimpleName();
    AQuery aq;
    public final Context mCtx = getActivity();
    EditText e1;
    ProfileNewActivityV2 parent;
    private TelephonyManager telephonyManager;
    ProgressDialog dialog;

    public String VARS_MOBILE_NUMBER = "";
    public String VARS_AUTH_NUMBER = "";
    String fNumber;

    int authProcessStatus = 0; // Ready
    private boolean isReceiveMode = false;
    final public int countdownSec = 180;
    private static final int SEND_THREAD_INFOMATION = 0;
    private static final int SEND_THREAD_STOP_MESSAGE = 1;

    private SendMassgeHandler mMainHandler = null;
    private CountThread mCountThread = null;

    protected static final String LOG_TAG = "SMSReceiver";
    private static final String ACTION = "android.provider.Telephony.SMS_RECEIVED";
    SmsMessage currentMessage;
    String message = "", sms_message = "";
    SharedPreferences preferences;
    private String myPhoneNumber = "";

    BroadcastReceiver mReceiver;
    IntentFilter filter;
    private Intent smsIntent;

    String number_1;
    private TextView tv_Count;

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.READ_SMS,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final String[] INITIAL_OREO_PERMS = {
            Manifest.permission.READ_SMS,
            Manifest.permission.READ_PHONE_NUMBERS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final int INITIAL_REQUEST = 1337;

    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case INITIAL_REQUEST:
                if (canAccessLocation()) {
                    number_1 = telephonyManager.getLine1Number();
                    String phoneNumber = number_1.replace("+82", "0");
                    Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                    fNumber = MZValidator.validTelNumber(phoneNumber);
                    aq.id(R.id.tf_phone_no).text(fNumber).typeface(CommonUtility.getNanumBarunTypeface());
                } else {
                    Toast.makeText(getActivity(), "핸드폰번호를 알수 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean canAccessLocation() {
        if (Build.VERSION.SDK_INT >= 26) {
            return (hasPermission(Manifest.permission.READ_PHONE_NUMBERS));
        }else{
            return (hasPermission(Manifest.permission.READ_SMS));
        }

    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == getActivity().checkSelfPermission(perm));
    }

    public void setLayout() {
        tv_Count = aq.id(R.id.validtime).getTextView();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(context).inflate(R.layout.fragment_profile_new_phone, null);

        parent = (ProfileNewActivityV2) getActivity();
        aq = new AQuery(view);

        e1 = (EditText) view.findViewById(R.id.input_nickname);

        parent.mTracker.setScreenName(TAG);
        // Send a screen view.
        parent.mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
        logger.logEvent("Profile input");

        dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.loading_comment));

        telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        number_1 = "";
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (!canAccessLocation()) {
                    if (Build.VERSION.SDK_INT >= 26) {
                        requestPermissions(INITIAL_OREO_PERMS, INITIAL_REQUEST);
                    }else{
                        requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
                    }

                } else {

                    number_1 = telephonyManager.getLine1Number();
                    String phoneNumber = number_1.replace("+82", "0");
                    Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                    fNumber = MZValidator.validTelNumber(phoneNumber);
                    aq.id(R.id.tf_phone_no).text(fNumber).typeface(CommonUtility.getNanumBarunTypeface());
                }
            }else{
                number_1 = telephonyManager.getLine1Number();
                String phoneNumber = number_1.replace("+82", "0");
                Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                fNumber = MZValidator.validTelNumber(phoneNumber);
                aq.id(R.id.tf_phone_no).text(fNumber).typeface(CommonUtility.getNanumBarunTypeface());
            }

        }
        catch(NullPointerException ex)
        {
            fNumber = "";
            aq.id(R.id.tf_phone_no).text(fNumber).typeface(CommonUtility.getNanumBarunTypeface());
        }

        //aq.id(R.id.text_auth).text("인증번호 요청");
        aq.id(R.id.auth_area).gone();
        aq.id(R.id.btn_auth).clicked(this, "phoneAuthRequest").enabled(false).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.btn_req).clicked(this, "authNoRequest").typeface(CommonUtility.getNanumBarunTypeface());

        setReceiver();
        // 메인 핸들러 생성
        mMainHandler = new SendMassgeHandler();
        setLayout();

        return view;
    }

    // Handler 클래스
    class SendMassgeHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case SEND_THREAD_INFOMATION:
                    tv_Count.setText("" + msg.obj);
                    // aq.id(R.id.validtime).text(""+msg.obj);
                    break;

                case SEND_THREAD_STOP_MESSAGE:
                    mCountThread.stopThread();
                    tv_Count.setText("입력시간초과");

                    break;

                default:
                    break;
            }
        }

    }

    // Thread 클래스
    class CountThread extends Thread implements Runnable {

        private boolean isPlay = false;
        private int i = countdownSec;

        public CountThread() {
            this.isPlay = true;
        }

        public void isThreadState(boolean isPlay) {
            this.isPlay = isPlay;
        }

        public void resetThread() {
            i = countdownSec;
            this.isPlay = true;
        }

        public void stopThread() {
            this.isPlay = false;
        }

        @Override
        public void run() {
            super.run();

            while (isPlay) {

                String outputSec = (i % 60 < 10) ? "0" + i % 60 : "" + i % 60;
                int outputMin = i / 60;
                String output = outputMin + ":" + outputSec;
                // Log.i("CountThread RUN" , i + "");

                // 메시지 얻어오기
                Message msg = mMainHandler.obtainMessage();
                // 메시지 ID 설정
                msg.what = SEND_THREAD_INFOMATION;

                // 메시지 정보 설정3 (Object 형식)
                msg.obj = output;
                mMainHandler.sendMessage(msg);

                i--;
                if (i <= 0) {
                    mMainHandler.sendEmptyMessage(SEND_THREAD_STOP_MESSAGE);
                    isPlay = false;
                    i = countdownSec;
                }

                // 1초 딜레이
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    //Mint.logException(e);
                }
            }
        }
    }

    private void setReceiver() {
        if (filter == null) {
            filter = new IntentFilter();
            filter.addAction(ACTION);
            mReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    //do something based on the intent's action
                    DUtil.Log("Smsintent recieved: ", intent.getAction());

                    //preferences = context.getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);

                    Log.e(LOG_TAG, "[inside onReceive]");
                    String fromCS = "";

                    if (intent.getAction().equals(ACTION)) {
                        // 받은 SMS의 내용을 가져오기 위해 StringBuilder생성
                        // 수신받은 Intent의 data를 가져오기 위해 getExtra로 bundle로 집어넣음
                        Bundle bundle = intent.getExtras();

                        if (bundle != null) {
                            // bundle에서 'pdus'를 key로 하는 값들을 가져옴
                            Object[] pdusObj = (Object[]) bundle.get("pdus");

                            // 가져온 object의 갯수만큼 class 생성
                            SmsMessage[] messages = new SmsMessage[pdusObj.length];

                            // object의 내용을 생성된 smsmessage에 할당
                            for (int i = 0; i < pdusObj.length; i++) {
                                messages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                            }

                            // 추출된 smsmessage들을 toast로 보여주기 위해 조립
                            for (SmsMessage currentMessage : messages) {
                                fromCS = currentMessage.getDisplayOriginatingAddress();
                                message = currentMessage.getDisplayMessageBody();
                                Log.i(LOG_TAG, "" + message);
                            }
                        }

                        DUtil.Log("fromCS", fromCS);
                        DUtil.Log("message", message);

                        message = message.replace("[Web발신]", "");

						/*
                         * 본인전화번호로 핸드폰인증번호 전송 02-1599-4704, 1599-4704 등록됨 LGT의
						 * 경우 상당수 "019-151-3540"으로 수신되는 경우때문에 추가로 수신체크함.
						 */
                        if (Constants.SMS_SENDER_NO.equals(fromCS) || fNumber.equals(fromCS)) {

                            if (message.contains(context.getResources().getString(R.string.app_sms_pattern))) {
                                if (message.contains("[")) {
                                    // 모바일앱에서의 가입요청시 () 이 아닌 <> 으로
                                    // 숫자가 둘러쌓임.

                                    //StringTokenizer tokens = new StringTokenizer(message, " : ");
                                    //tokens.nextToken();
                                    String temp1 = message.split("\\[")[1];//tokens.nextToken();
                                    String temp2 = temp1.split("\\]")[0];//tokens.nextToken();

                                    sms_message = temp2;
                                    //
                                    Toast.makeText(context, context.getResources().getString(R.string.sep_sms_received), Toast.LENGTH_SHORT).show();

                                    Log.i(LOG_TAG, "(sms)" + sms_message);
                                    setAuthCodeArea(sms_message);
                                    // 다른 App에서 사용하지 못하도록 함
                                    // Android 4.4+ 부터 해당기능 작동불가와
                                    // 기기별 문제발생여지를 고려해 abort처리는 안함.
                                    // abortBroadcast();
                                }

                            } else {

                            }
                        }

                    }
                }
            };
            getActivity().registerReceiver(mReceiver, filter);

            Log.d("onCreate()", "브로드캐스트리시버 등록됨");
        }
    }

    @Override
    public void initialize() {
        setReceiver();
    }



    private void requestAuthCode(String _ctn) {
        if (_ctn != null && _ctn.length() > 9) {
            isReceiveMode = true;
            //aq.id(R.id.input_ctn_text).enabled(false);
            aq.id(R.id.btn_auth).text("입력").typeface(CommonUtility.getNanumBarunTypeface());
            requestPhoneAuth(_ctn);
            aq.id(R.id.btn_clear_01).gone();

            aq.id(R.id.tf_phone_no).enabled(false);

            Toast.makeText(getActivity(), "인증코드가 발송되었습니다.", Toast.LENGTH_SHORT).show();

        } else {
            DUtil.alertShow(getActivity(), "정상적인 번호가 아닙니다.");
            //DUtil.Log(TAG, "정상적인 번호가 아닙니다.");
            aq.id(R.id.btn_clear_01).visible();

        }
    }

    public void authNoRequest(View button){


        authProcessStatus = 1; // Requested
        tv_Count.setText("");
        Log.d(TAG, "[phoneAuthRequest] ");
        String ctn = aq.id(R.id.tf_phone_no).getText().toString();
        String validCtn = ValidateUtil.validTelNumber(ctn);
        aq.id(R.id.btn_req).text("인증번호 다시받기").typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.input_code_text).text("");

        requestAuthCode(validCtn);
    }

    public void phoneAuthRequest(View button) {
        // 인증코드 검증
        String ctn = aq.id(R.id.tf_phone_no).getText().toString();
        String validCtn = ValidateUtil.validTelNumber(ctn);
        String authCode = aq.id(R.id.input_code_text).getText().toString();
        if (authCode.length() == 6) {
            authorizePhoneAuth(validCtn, authCode);
        } else {
            DUtil.alertShow(getActivity(), "인증번호는 6자리입니다.");
            DUtil.Log(TAG, "인증번호는 6자리입니다.");
            phoneInputEnable();
        }
    }

    public void setAuthCodeArea(String _code) {
        aq.id(R.id.input_code_text).text(_code);

        String ctn = aq.id(R.id.tf_phone_no).getText().toString();
        String validCtn = ValidateUtil.validTelNumber(ctn);
        String authCode = aq.id(R.id.input_code_text).getText().toString();

        if(!ValidateUtil.isValidPhoneNumber(validCtn)){
            DUtil.alertShow(getActivity(), "정상적인 휴대폰번호가 아닙니다.");
            phoneInputEnable();
            return;
        }

        if (_code.length() == 6) {
            authorizePhoneAuth(validCtn, authCode);
        } else {
            DUtil.alertShow(getActivity(), "인증번호는 6자리입니다.");
            DUtil.Log(TAG, "인증번호는 6자리입니다.");
            phoneInputEnable();
        }
    }

    private void authorizePhoneAuth(final String _ctn, final String _authCode) {
        Map<String, Object> params = new HashMap<String, Object>();
        //params.put("phone", _ctn);
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("verifyCode", _authCode);
        VARS_MOBILE_NUMBER = _ctn;
        VARS_AUTH_NUMBER = _authCode;

        AjaxCallback.setNetworkLimit(1);
        aq.progress(dialog).ajax(CommonUtility.SERVER_URL + Constants.CONFIRM_CTN_AUTHCODE, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                if (status.getCode() >= 400) {
                    // IF ERROR
                    Log.i("authorizePhoneAuth StatusCode", status.getCode() + "");
                    Log.i("authorizePhoneAuth StatusError", status.getError());
                } else {
                    // IF NORMAL
                    if (json == null) {
                        Log.i("RETURN", "" + status.getMessage());
                        // 네트워크 오류페이지로 이동
                        //goNetworkErrorPage();
                        phoneInputEnable();
                        return;
                    }

                    Log.i("authorizePhoneAuth RETURN", "" + json.toString());

                    try {
                        if ("S0000".equals(json.getString("resultCode"))) {
                            parent.mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("가입및탈퇴")
                                    .setAction("프로필입력")
                                    .setLabel("휴대폰인증완료")
                                    .build());

                            Toast.makeText(getActivity(), json.getString("message"), Toast.LENGTH_SHORT).show();
                            //aq.id(R.id.layout_codeauth).visible();
                            //aq.id(R.id.btn_auth).text("인증완료").enabled(false);
                            aq.id(R.id.tf_phone_no).clickable(false).enabled(false);
                            //aq.id(R.id.btn_req).gone();
                            mCountThread.stopThread();

                            authProcessStatus = 2; // authorized

                            parent.inputedPhone = VARS_MOBILE_NUMBER;
                            parent.processNext();

                        } else {
                            VARS_AUTH_NUMBER = "";
                            aq.id(R.id.input_code_text).text("");

                            phoneInputEnable();
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        phoneInputEnable();
                        e.printStackTrace();
                    }
                    Log.i("authorizePhoneAuth RESULT", json.toString());
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        destroyReceiver();
        Log.d(this.getClass().getSimpleName(), "onDestroyView()");
        super.onDestroyView();
    }
//
//    @Override
//    public void onDestroy() {
//
//        Log.d(this.getClass().getSimpleName(), "onDestroy()");
//        super.onDestroy();
//    }

    private void destroyReceiver(){
        if (mReceiver != null) {
            getActivity().unregisterReceiver(mReceiver);
            mReceiver = null;
        }
    }

    private void requestPhoneAuth(String _ctn) {
        Map<String, Object> params = new HashMap<String, Object>();
        _ctn = _ctn.replace("-", "");
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("phone", _ctn);

        Log.i("requestPhoneAuth PARAM", params.toString());

        AjaxCallback.setNetworkLimit(1);
        aq.progress(dialog).ajax(CommonUtility.SERVER_URL + Constants.REQUEST_CTN_AUTH, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                if (status.getCode() >= 400) {
                    // IF ERROR
                    Log.i("requestPhoneAuth StatusCode", status.getCode() + "");
                    Log.i("requestPhoneAuth StatusError", status.getError());
                } else {
                    // IF NORMAL
                    if (json == null) {
                        Log.i("RETURN", "" + status.getMessage());
                        // 네트워크 오류페이지로 이동
                        //goNetworkErrorPage();
                        phoneInputEnable();
                        return;
                    }

                    Log.i("requestPhoneAuth RETURN", "" + json.toString());

                    try {
                        if ("S0000".equals(json.getString("resultCode"))) {
                            Toast.makeText(getActivity(), json.getString("message"), Toast.LENGTH_SHORT).show();
                            aq.id(R.id.auth_area).visible();
                            //aq.id(R.id.auth_area).background(R.drawable.profile_new_input_gray_back);
                            aq.id(R.id.btn_auth).enabled(true);//.backgroundColor(0x00b4ff);
                            authValidTimerStart();
                            //aq.id(R.id.input_code_text).getEditText().requestFocus();
                        } else {
                            aq.id(R.id.auth_area).gone();
                            //aq.id(R.id.layout_codeauth).gone();
                            //aq.id(R.id.input_code_text).getEditText().requestFocus();
                            phoneInputEnable();
                            aq.id(R.id.btn_auth).enabled(false);//.backgroundColor(0xcfcfcf);
                            //DUtil.alertShow(mActivity, json.getString("result_msg"));
                            // Error Exception
                            //alert(json.getString("result_msg"), DURATION_SHORT);
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        phoneInputEnable();
                        e.printStackTrace();
                    }
                    //Log.i("requestPhoneAuth RESULT", json.toString());
                }
            }
        });
    }

    private void phoneInputEnable() {
        isReceiveMode = false;
        aq.id(R.id.tf_phone_no).enabled(true);
    }

    private void authValidTimerStart() {

        aq.id(R.id.validtime).visible();

        if (mCountThread == null) {
            mCountThread = new CountThread();
            mCountThread.start();
        } else {
            Log.i("CountThread ALIVE", mCountThread.isAlive() + "");

            mCountThread.interrupt();
            tv_Count.setText("");
            // mCountThread = new CountThread();
            if (mCountThread.isAlive()) {
                mCountThread.resetThread();
            } else {
                mCountThread = new CountThread();
                mCountThread.start();
            }
        }
    }


}
