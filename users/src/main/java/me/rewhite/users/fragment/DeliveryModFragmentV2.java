package me.rewhite.users.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.multiviewpager.MultiViewPager;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;

import me.rewhite.users.R;
import me.rewhite.users.activity.ModDeliveryInfoActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.layout.WheelView;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.TimeUtil;

public class DeliveryModFragmentV2 extends Fragment implements IFragment {

    private final static String TAG = "DeliveryReqFragmentV2";
    private ModDeliveryInfoActivity mActivity;

    private static final String STORE_INFO = "storeInfo";
    private JSONObject storeInfo;

    private ViewGroup mView = null;
    int[] timetable;
    ArrayList<String> TIME_TABLE;
    private WheelView wva;

    Calendar today;
    public AQuery aq;

    public DeliveryModFragmentV2() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {

        Log.d(TAG, "[onResume]");
        mActivity.setTouchDisable(false);

        super.onResume();

        mActivity.setTitle("배송요청일 변경");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ModDeliveryInfoActivity) getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup) inflater.inflate(R.layout.fragment_delivery_mod, container, false);

        aq = new AQuery(mView);

        aq.id(R.id.btn_next).clicked(this, "deliverySetupComplete");
        initTimetableData(mActivity.pickupReqTime);
        Log.i("mod-pickupReqTime", mActivity.storeId + " / " + mActivity.addressSeq + " / " + TimeUtil.convertTimestampToString(mActivity.pickupReqTime) );
        return mView;
    }

    int DATE_RANGE = 15;

    long selectedTimestamp = -1;
    int selectedHour = 0;

    public void deliverySetupComplete(View button) {
        if (selectedTimestamp != -1) {
            if (selectedHour != 0) {

                mActivity.deliveryReqTime = selectedTimestamp;
                Log.i("mod-DeliveryReqTime", TimeUtil.convertTimestampToString(mActivity.pickupReqTime) + " / " + TimeUtil.convertTimestampToString(mActivity.deliveryReqTime));

                mActivity.modInfo();

            } else {
                DUtil.alertShow(getActivity(), "시간을 선택해주세요");
            }
        } else {
            DUtil.alertShow(getActivity(), "날짜를 선택해주세요");
        }
    }

    private void initTimetableData(long timestamp){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("storeId", mActivity.storeId);
        params.put("addressSeq", mActivity.addressSeq);
        params.put("pickupRequestTime", timestamp);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_TIMETABLE, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_TIMETABLE, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //

                        } else {
                            finalDatePickerContainer = jsondata.getJSONArray("data");
                            setDatePicker();
                            if(finalDatePickerContainer.length() > 0){
                                String dateString = finalDatePickerContainer.getJSONObject(0).getString("date");
                                int day_of_week = Integer.parseInt(dateString.substring(8,9));
                                setYoilText(day_of_week);
                                setTimePicker(0);
                            }

                            DUtil.Log(Constants.ORDER_TIMETABLE, finalDatePickerContainer.toString());
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private void setDatePicker() {
        final MultiViewPager pager = (MultiViewPager) mView.findViewById(R.id.pager);


        final FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(getChildFragmentManager()) {
            //ArrayList<DatePickFragment> container = new ArrayList<DatePickFragment>();

            @Override
            public int getCount() {
                return finalDatePickerContainer.length();
            }

            @Override
            public DatePickFragment getItem(int position) {
                try {
                    String dateString = finalDatePickerContainer.getJSONObject(position).getString("date");
                    return DatePickFragment.create(Integer.parseInt(dateString.substring(6,8)));
                } catch (JSONException e) {
                    e.printStackTrace();
                    return DatePickFragment.create(0);
                }
            }

        };
        pager.setAdapter(adapter);
        pager.setCurrentItem(selectedCurrentPositionDate);
        pager.setOnPageChangeListener(new MultiViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selectedCurrentPositionDate = position;
                String dateString = null;
                try {
                    if(finalDatePickerContainer.length() > position){
                        dateString = finalDatePickerContainer.getJSONObject(position).getString("date");
                        int day_of_week = Integer.parseInt(dateString.substring(8));
                        setYoilText(day_of_week);
                        setTimePicker(position);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setYoilText(int value) {
        String m_week = "";

        if (value == 0 || value == 7)
            m_week = "일요일";
        else if (value == 1)
            m_week = "월요일";
        else if (value == 2)
            m_week = "화요일";
        else if (value == 3)
            m_week = "수요일";
        else if (value == 4)
            m_week = "목요일";
        else if (value == 5)
            m_week = "금요일";
        else if (value == 6)
            m_week = "토요일";

        aq.id(R.id.yoil_text).text(m_week).typeface(CommonUtility.getNanumBarunTypeface());
    }

    JSONArray finalDatePickerContainer;

    private String getDisplayTimeString(String _timeString){
        int timeTemp = Integer.parseInt(_timeString.substring(0,2));
        String temp = "";

        if(timeTemp < 12){
            temp = "오전 " + _timeString.substring(0,2) + "시~" +  _timeString.substring(3,5) + "시";
        }else if(timeTemp == 12){
            temp = "낮 " + _timeString.substring(0,2) + "시~" +  _timeString.substring(3,5) + "시";
        }else{
            temp = "오후 " + _timeString.substring(0,2) + "시~" +  _timeString.substring(3,5) + "시";
        }

        return temp;
    }

    private void initTimestamp(final int position) throws JSONException {
        selectedHour = Integer.parseInt(finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").getString(wheelSelectedIndex).substring(0,2));
        String selDate = finalDatePickerContainer.getJSONObject(position).getString("date").substring(0,8);
        selectedTimestamp = TimeUtil.getOrderDateTimestamp(selDate + finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").getString(wheelSelectedIndex).substring(0,2)).getTime();
    }

    int selectedCurrentPositionDate = 0;
    int wheelSelectedIndex = 0;

    private void setTimePicker(final int position) throws JSONException{

        wheelSelectedIndex = 0;
        TIME_TABLE = new ArrayList<String>();
        for(int i = 0 ; i < finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").length(); i++){
            String displayHour = getDisplayTimeString(finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").getString(i));
            TIME_TABLE.add(i,displayHour);
        }
        initTimestamp(position);

        wva = (WheelView) mView.findViewById(R.id.wheel_view_wv);
        //wva.invalidate();
        wva.setItems(TIME_TABLE);
        //wva.setOffset(selectedCurrentPositionHour);
        wva.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                // selectedIndex 1부터 시작
                // selectedIndex 1부터 시작
                if (selectedIndex > TIME_TABLE.size()) {
                    selectedIndex = TIME_TABLE.size();
                }
                if(selectedIndex < 1){
                    selectedIndex = 1;
                }
                wheelSelectedIndex = selectedIndex;

                try {
                    Log.i(TAG, "selectedIndex: " + finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").getString(selectedIndex-1).substring(0,2) + "시, item: " + item);
                    selectedHour = Integer.parseInt(finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").getString(selectedIndex-1).substring(0,2));
                    String selDate = finalDatePickerContainer.getJSONObject(position).getString("date").substring(0,8);
                    selectedTimestamp = TimeUtil.getOrderDateTimestamp(selDate + finalDatePickerContainer.getJSONObject(position).getJSONArray("timeList").getString(selectedIndex-1).substring(0,2)).getTime();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        Log.e("delivery date", selectedTimestamp + "/" + selectedHour);
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }
}
