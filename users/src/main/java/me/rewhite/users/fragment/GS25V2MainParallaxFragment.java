package me.rewhite.users.fragment;

import android.graphics.Color;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.balysv.materialripple.MaterialRippleLayout;

import me.rewhite.users.R;
import me.rewhite.users.activity.gsretail.GS25OrderActivity;
import me.rewhite.users.adapter.GSParallaxAdapter;

/**
 * Created by marines on 2017. 8. 28..
 */

public class GS25V2MainParallaxFragment extends Fragment {

    private GSParallaxAdapter mAdapter;
    int pos = -1;
    AQuery aq;
    GS25OrderActivity mActivity;

    public void setInstance(GS25OrderActivity _activity){
        mActivity = _activity;
    }

    public void infoAction(View button){
        mActivity.infoMove(pos);
    }

    private void setMaterialRippleLayout(View _target){
        MaterialRippleLayout.on(_target)
                .rippleColor(Color.parseColor("#FFffff"))
                .rippleAlpha(0.2f)
                .rippleHover(true)
                .rippleOverlay(true)
                .create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.gs25_main_fragment_parallax, container, false);
        aq = new AQuery(v);

        final ImageView image = (ImageView) v.findViewById(R.id.image);
        final ImageView subimage = (ImageView) v.findViewById(R.id.subimage);

        image.setImageResource(getArguments().getInt("image"));
        subimage.setImageResource(getArguments().getInt("subimage"));
        pos = getArguments().getInt("position");

        if(pos == 2 || pos == 5 || pos == 6){
            setMaterialRippleLayout((View)v.findViewById(R.id.subimage));
            aq.id(R.id.subimage).clicked(this, "infoAction");
        }

        image.post(new Runnable() {
            @Override
            public void run() {
                Matrix matrix = new Matrix();
                matrix.reset();

                float wv = image.getWidth();
                float hv = image.getHeight();

                float wi = image.getDrawable().getIntrinsicWidth();
                float hi = image.getDrawable().getIntrinsicHeight();

                float width = wv;
                float height = hv;

                if (wi / wv > hi / hv) {
                    matrix.setScale(hv / hi, hv / hi);
                    width = wi * hv / hi;
                } else {
                    matrix.setScale(wv / wi, wv / wi);
                    height= hi * wv / wi;
                }

                matrix.preTranslate((wv - width) / 2, (hv - height) / 2);
                image.setScaleType(ImageView.ScaleType.MATRIX);
                image.setImageMatrix(matrix);
            }
        });

        subimage.post(new Runnable() {
            @Override
            public void run () {
                Matrix matrix = new Matrix();
                matrix.reset();

                float wv = subimage.getWidth();
                float hv = subimage.getHeight();

                float wi = subimage.getDrawable().getIntrinsicWidth();
                float hi = subimage.getDrawable().getIntrinsicHeight();

                float width = wv;
                float height = hv;

                if (wi / wv > hi / hv) {
                    matrix.setScale(hv / hi, hv / hi);
                    width = wi * hv / hi;
                } else {
                    matrix.setScale(wv / wi, wv / wi);
                    height = hi * wv / wi;
                }

                matrix.preTranslate((wv - width) / 2, (hv - height) / 2);
                subimage.setScaleType(ImageView.ScaleType.MATRIX);
                subimage.setImageMatrix(matrix);
            }
        });

        return v;
    }

    public void setAdapter(GSParallaxAdapter _mAdapter) {
        mAdapter = _mAdapter;
    }
}
