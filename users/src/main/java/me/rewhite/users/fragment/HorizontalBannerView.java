package me.rewhite.users.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;

import me.rewhite.users.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HorizontalBannerView#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HorizontalBannerView extends Fragment {

    private static final String ARG_DATA = "data";
    private static final String ARG_INDEX = "index";
    private Context mCtx;
    private JSONArray adData;
    private int index;

    public HorizontalBannerView() {
        // Required empty public constructor
    }

    public static HorizontalBannerView newInstance(int _index, String data) {
        HorizontalBannerView fragment = new HorizontalBannerView();
        Bundle args = new Bundle();
        args.putString(ARG_DATA, data);
        args.putInt(ARG_INDEX, _index);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            try {
                adData = new JSONArray(getArguments().getString(ARG_DATA));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            index = getArguments().getInt(ARG_INDEX);
            mCtx = getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_horizontal_banner_view, container, false);
    }

}
