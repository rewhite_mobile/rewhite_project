package me.rewhite.users.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.multiviewpager.MultiViewPager;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.OrderActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.PickupType;
import me.rewhite.users.layout.WheelView;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.StringUtil;
import me.rewhite.users.util.TimeUtil;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PickupReqFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PickupReqFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PickupReqFragment extends Fragment implements IFragment {

    private final static String TAG = "PickupReqFragment";
    private OrderActivity mActivity;

    private static final String STORE_INFO = "storeInfo";
    private JSONObject storeInfo;

    private OnFragmentInteractionListener mListener;

    private ViewGroup mView = null;
    int[] timetable;
    //String[] TIME_TABLE;
    ArrayList<String> TIME_TABLE;
    private WheelView wva;

    Calendar today;
    public AQuery aq;

    ProgressDialog dialog;

    public static PickupReqFragment newInstance(String storeInfo) {
        PickupReqFragment fragment = new PickupReqFragment();
        Bundle args = new Bundle();
        args.putString(STORE_INFO, storeInfo);
        fragment.setArguments(args);
        return fragment;
    }

    public PickupReqFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {

        Log.d(TAG, "[onResume]");
        mActivity.setTouchDisable(false);

        super.onResume();

        today = Calendar.getInstance(Locale.KOREA);
        Calendar pickupReqTimeCal = Calendar.getInstance(Locale.KOREA);
        pickupReqTimeCal.setTimeInMillis(mActivity.pickupReqTime);
        //pickupReqTimeCal.add(Calendar.DATE, mActivity.washingDays);
        Calendar current = Calendar.getInstance();

        int pickupValue = pickupReqTimeCal.get(Calendar.YEAR)*10000 + pickupReqTimeCal.get(Calendar.MONTH) * 100 + pickupReqTimeCal.get(Calendar.DATE);
        int currentValue = current.get(Calendar.YEAR)*10000 + current.get(Calendar.MONTH) * 100 + current.get(Calendar.DATE);

        if(pickupValue < currentValue){
            today = current;
            //today.add(Calendar.DATE, -1*mActivity.washingDays);
        }else{
            today.setTimeInMillis(mActivity.pickupReqTime);
        }

        initPreTimeData();
        initDateData();

        String titleStr = "";
        if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
            titleStr = "수거 신청 (당일배송)";
        }else if(mActivity.pickupMode == PickupType.PICKUPTYPE_TOMORROW){
            titleStr = "수거 신청 (익일배송)";
        }else{
            titleStr = "수거 신청";
        }
        mActivity.setTitle(titleStr);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = (OrderActivity) getActivity();

        if (getArguments() != null) {
            try {
                storeInfo = new JSONObject(getArguments().getString(STORE_INFO));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        mView = (ViewGroup) inflater.inflate(R.layout.fragment_pickup_req, container, false);

        aq = new AQuery(mView);


        today = Calendar.getInstance(Locale.KOREA);

        Calendar pickupReqTimeCal = Calendar.getInstance(Locale.KOREA);
        pickupReqTimeCal.setTimeInMillis(mActivity.pickupReqTime);
        //pickupReqTimeCal.add(Calendar.DATE, mActivity.washingDays);
        Calendar current = Calendar.getInstance();

        int pickupValue = pickupReqTimeCal.get(Calendar.YEAR)*10000 + pickupReqTimeCal.get(Calendar.MONTH) * 100 + pickupReqTimeCal.get(Calendar.DATE);
        int currentValue = current.get(Calendar.YEAR)*10000 + current.get(Calendar.MONTH) * 100 + current.get(Calendar.DATE);

        if(pickupValue < currentValue){
            today = current;
            //today.add(Calendar.DATE, -1*mActivity.washingDays);
        }else{
            today.setTimeInMillis(mActivity.pickupReqTime);
        }

        aq.id(R.id.btn_next).clicked(this, "pickupSetupComplete");

        aq.id(R.id.btn_service_01).clicked(this, "serviceSelected").tag(1);
        aq.id(R.id.btn_service_02).clicked(this, "serviceSelected").tag(2);
        if(mActivity.isRepairAvailable){
            aq.id(R.id.btn_service_03).clicked(this, "serviceSelected").tag(3);
        }else{
            aq.id(R.id.btn_service_03).gone();//.enabled(false).image(R.mipmap.btn_order_service_03_disabled);
        }

        dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.loading_comment));


        initPreTimeData();
        initDateData();

        return mView;
    }

    int DATE_RANGE = 15;

    int selectedYear = -1;
    int selectedMonth = -1;
    int selectedDay = -1;
    int selectedHour = 0;

    boolean isService01_on = false;
    boolean isService02_on = false;
    boolean isService03_on = false;

    public void serviceSelected(View button) {
        int tag = (int) button.getTag();
        switch (tag) {
            case 1:
                isService01_on = !isService01_on;
                if (isService01_on) {
                    aq.id(R.id.btn_service_01).image(R.mipmap.btn_order_service_01_pres);
                } else {
                    aq.id(R.id.btn_service_01).image(R.drawable.btn_service_sel_01);
                }
                break;
            case 2:
                isService02_on = !isService02_on;
                if (isService02_on) {
                    aq.id(R.id.btn_service_02).image(R.mipmap.btn_order_service_02_pres);
                } else {
                    aq.id(R.id.btn_service_02).image(R.drawable.btn_service_sel_02);
                }
                break;
            case 3:
                isService03_on = !isService03_on;
                if (isService03_on) {
                    aq.id(R.id.btn_service_03).image(R.mipmap.btn_order_service_03_pres);
                } else {
                    aq.id(R.id.btn_service_03).image(R.drawable.btn_service_sel_03);
                }
                break;
            default:
                break;
        }
    }

    public void pickupSetupComplete(View button) {
        AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
        logger.logEvent("Normal order Step1");

        if (selectedYear != -1 && selectedMonth != -1 && selectedDay != -1) {
            if (selectedHour != 0) {
                if (isService01_on || isService02_on || isService03_on) {
                    Date pickupDate = TimeUtil.getDate(selectedYear, selectedMonth, selectedDay, selectedHour);
                    Log.e("pickup date", selectedYear + "/" + selectedMonth + "/" +selectedDay + "/" +selectedHour);
                    mActivity.pickupReqTime = pickupDate.getTime();
                    String orderReqItem = "000";
                    orderReqItem = String.format("%s%s%s", (isService01_on) ? "1" : "0", (isService02_on) ? "1" : "0", (isService03_on) ? "1" : "0");
                    mActivity.orderRequestItem = orderReqItem;
                    Log.i("PickupReqTime", mActivity.pickupReqTime + "");
                    //ORDER_AVAILABLE_CHECK
                    try {
                        RequestParams params = new RequestParams();
                        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                        params.put("storeId", mActivity.storeInfo.getString("storeId"));
                        Date orderDate = new Date();
                        orderDate.setTime(mActivity.pickupReqTime);
                        params.put("orderDate", TimeUtil.getOrderDateFormat(orderDate));
                        params.put("k", 1);
                        NetworkClient.post(Constants.ORDER_AVAILABLE_CHECK, params, new AsyncHttpResponseHandler() {

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                                // TODO Auto-generated method stub
                                DUtil.Log(Constants.ORDER_AVAILABLE_CHECK, error.getMessage());
                                Toast.makeText(getActivity().getApplicationContext(), "오류가 발생했습니다.\n" + error.getMessage(), Toast.LENGTH_LONG);
                            }

                            @Override
                            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                                String result;
                                try {
                                    result = new String(data, "UTF-8");
                                    DUtil.Log(Constants.ORDER_AVAILABLE_CHECK, result);

                                    JSONObject jsondata = new JSONObject(result);

                                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                                        Tracker t = ((Bootstrap) getActivity().getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                                        t.send(new HitBuilders.EventBuilder()
                                                .setCategory("주문")
                                                .setAction("수거일자선택")
                                                //.setLabel("Order")
                                                .build());
                                        int startTime = mActivity.storeSaleStartTime;
                                        Date oDate = new Date();
                                        oDate.setTime(mActivity.pickupReqTime);
                                        SimpleDateFormat objFormatter = new SimpleDateFormat("HH", Locale.KOREA);
                                        String startHour = objFormatter.format(oDate);

                                        int index = (Integer.parseInt(startHour) - startTime) / Integer.parseInt(deliveryDuration);

                                        String deliveryData = jsondata.getString("data");
                                        String deliveryDatum = deliveryData.split("\\|")[2];

                                        if (deliveryDatum.length() < (index + 1)) {
                                            index = deliveryDatum.length() - 1;
                                        }
                                        if ("X".equals(String.valueOf(deliveryDatum.charAt(wheelSelectedIndex)))) {
                                            // 해당시간에 주문량이 너무 많습니다.
                                            //Toast.makeText(getActivity(), "선택하신 시간대에 접수량이 많습니다. 죄송하지만 다른 시간을 선택해주시겠어요?", Toast.LENGTH_LONG).show();
                                            DUtil.alertShow(getActivity(), "선택하신 시간대에 접수량이 많습니다. 죄송하지만 다른 시간을 선택해주시겠어요?");
                                        } else {
                                            mActivity.showFragment(OrderActivity.FRAGMENT_DELIVERY);
                                        }

                                    } else {
                                        Toast.makeText(getActivity().getApplicationContext(), jsondata.getString("message"), Toast.LENGTH_LONG).show();
                                    }
                                } catch (UnsupportedEncodingException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                } catch (JSONException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    DUtil.alertShow(getActivity(), "원하시는 서비스를 선택해주세요");
                }

            } else {
                DUtil.alertShow(getActivity(), "시간을 선택해주세요");
            }
        } else {
            DUtil.alertShow(getActivity(), "날짜를 선택해주세요");
        }
    }

    private void setDatePicker() {
        MultiViewPager pager = (MultiViewPager) mView.findViewById(R.id.pager);

        FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(getChildFragmentManager()) {
            //ArrayList<DatePickFragment> container = new ArrayList<DatePickFragment>();

            @Override
            public int getCount() {
                return finalDatePickerContainer.size();
            }

            @Override
            public DatePickFragment getItem(int position) {
                return DatePickFragment.create(finalDatePickerContainer.get(position).get(Calendar.DAY_OF_MONTH));
            }

        };
        pager.setAdapter(adapter);
        pager.setCurrentItem(selectedCurrentPositionDate);
        pager.setOnPageChangeListener(new MultiViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


                int day_of_week = finalDatePickerContainer.get(position).get(Calendar.DAY_OF_WEEK);
                setYoilText(day_of_week);

                selectedYear = finalDatePickerContainer.get(position).get(Calendar.YEAR);
                selectedMonth = finalDatePickerContainer.get(position).get(Calendar.MONTH);
                selectedDay = finalDatePickerContainer.get(position).get(Calendar.DAY_OF_MONTH);

                //mActivity.pickupReqTime = finalDatePickerContainer.get(position).getTimeInMillis();
                Log.e("Date onPageSelected", "selected days : " + selectedMonth + "/" + selectedDay);

                initTimeData(finalDatePickerContainer.get(position).getTime());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setYoilText(int value) {
        String m_week = "";

        if (value == 1)
            m_week = "일요일";
        else if (value == 2)
            m_week = "월요일";
        else if (value == 3)
            m_week = "화요일";
        else if (value == 4)
            m_week = "수요일";
        else if (value == 5)
            m_week = "목요일";
        else if (value == 6)
            m_week = "금요일";
        else if (value == 7)
            m_week = "토요일";


        aq.id(R.id.yoil_text).text(m_week).typeface(CommonUtility.getNanumBarunTypeface());
    }

    String deliveryInfo;
    String deliveryDuration;
    String deliveryTime;
    int saleStartTime = 6;

    ArrayList<Calendar> datePickerContainer;
    ArrayList<Calendar> finalDatePickerContainer;


    private void initDateData() {
        if(dialog != null && !dialog.isShowing()){
            dialog.show();
        }

        Calendar today = Calendar.getInstance();
        //Log.e("today compare", pickupValue + "::" + currentValue);
        boolean isAvailablePickup = false;
        //int washingDays = mActivity.washingDays;
        //int addDays = getAddDays();
        // 배달일 이전에 휴무일 계산
        //washingDays += addDays;

        isAvailablePickup = checkTodayAvailable();
        Log.e("isTodayAvailable", isAvailablePickup + "");

        datePickerContainer = new ArrayList<Calendar>();

        for (int d = 0; d < DATE_RANGE; d++) {
            Calendar forTom = Calendar.getInstance();
            forTom.setTimeInMillis(today.getTimeInMillis());

            if(isAvailablePickup){
                forTom.add(Calendar.DATE,  d);
            }else{
                forTom.add(Calendar.DATE,  (d+1));
            }

            datePickerContainer.add(forTom);
        }

        removeHoliday();

        // 현재선택값 요일값 세팅
        //setYoilText(today.get(Calendar.DAY_OF_WEEK));

        if(finalDatePickerContainer.size() > 0){
            setYoilText(finalDatePickerContainer.get(0).get(Calendar.DAY_OF_WEEK));
            initTimeData(finalDatePickerContainer.get(0).getTime());
        }


        setDatePicker();

    }

    private void initPreTimeData(){
        initTimeData(null);
    }

    private void initTimeData(Date _selectedDate) {
        Date selectedDate = _selectedDate;

        JSONObject storeInfo = mActivity.storeInfo;
        int size = 0;
        int todayLimit = 0;
        boolean isToday = false;

        try {
            if(_selectedDate == null){
                Calendar base = Calendar.getInstance(Locale.KOREA);
                base.setTimeInMillis(today.getTimeInMillis());
                int washingDays = mActivity.washingDays;
                int addDays = getAddDays();
                // 배달일 이전에 휴무일 계산
                washingDays += addDays;
                base.add(Calendar.DATE, washingDays);

                selectedDate = base.getTime();
            }

            Log.e("selected initTimeData", selectedDate.toString() + "/" + selectedDay);

            Calendar selectedCal = Calendar.getInstance(Locale.KOREA);
            selectedCal.setTime(selectedDate);
            Calendar current = Calendar.getInstance(Locale.KOREA);
            current.setTimeInMillis((new Date()).getTime());

            int pickupValue = selectedCal.get(Calendar.YEAR)*10000 + selectedCal.get(Calendar.MONTH) * 100 + selectedCal.get(Calendar.DATE);
            int currentValue = current.get(Calendar.YEAR)*10000 + current.get(Calendar.MONTH) * 100 + current.get(Calendar.DATE);
            if(pickupValue == currentValue){
                isToday = true;
                todayLimit = current.get(Calendar.HOUR_OF_DAY)*60 + current.get(Calendar.MINUTE);
                Log.e("today", "TODAY is true / " + todayLimit);
            }

            if(TimeUtil.getWeekDayFromDate(selectedDate) == 5){
                // 토요일
                deliveryInfo = storeInfo.getString("storeDeliveryTimeSat");
            }else if(TimeUtil.getWeekDayFromDate(selectedDate) == 6){
                // 일요일
                deliveryInfo = storeInfo.getString("storeDeliveryTimeSun");
            }else{
                deliveryInfo = storeInfo.getString("storeDeliveryTime");
            }

            deliveryDuration = deliveryInfo.split("\\|")[0];
            mActivity.storeDeliveryDuration = Integer.parseInt(deliveryDuration);
            saleStartTime = 6 + Integer.parseInt(deliveryInfo.split("\\|")[1]);
            deliveryTime = deliveryInfo.split("\\|")[2];
            //String deliveryTime = storeInfo.getString("storeDeliveryTime");
            Log.i("deliveryTime : ", deliveryInfo + " / " + deliveryTime);

            String avPos = (Integer.parseInt(deliveryDuration)*2-1)+"";
            for (int i = 0; i < deliveryTime.length(); i++) {
                if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
                    if(i*Integer.parseInt(deliveryDuration) + saleStartTime < 14){
                        if (avPos.equals(String.valueOf(deliveryTime.charAt(i)))) {
                            size++;
                        }
                    }
                }else{
                    if (avPos.equals(String.valueOf(deliveryTime.charAt(i)))) {
                        size++;
                    }
                }
            }
            Log.i("Delivery Timetable", "Delivery size : " + size);

            timetable = new int[size];

            timeSelectedIndex = 0;
            int timeCount = 0;

            for (int i = 0; i < deliveryTime.length(); i++) {

                if (avPos.equals(String.valueOf(deliveryTime.charAt(i)))) {
                    if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
                        if(i*Integer.parseInt(deliveryDuration) + saleStartTime <= 14 - Integer.parseInt(deliveryDuration)){
                            timetable[timeCount] = i*Integer.parseInt(deliveryDuration) + saleStartTime;//i+6;
                            Log.i("Delivery Timetable", size + " // " + timetable[timeCount] + "");
                            timeCount++;
                        }
                    }else{
                        timetable[timeCount] = i*Integer.parseInt(deliveryDuration) + saleStartTime;//i+6;
                        Log.i("Delivery Timetable", size + " // " + timetable[timeCount] + "");
                        timeCount++;
                    }

                }
            }

            Log.i("timeCount + size", timeCount + ":::" + size);
            //06:00 ~ 23:00
            //{"06:00 ~ 07:00 오전", "07:00 ~ 08:00 오전", "08:00 ~ 09:00 오전", "09:00 ~ 10:00 오전", "16:00 ~ 17:00 오전", "06:00 ~ 07:00 오전", "06:00 ~ 07:00 오전", "06:00 ~ 07:00 오전"};
            TIME_TABLE = new ArrayList<String>();
            finalTimeTable = new ArrayList<Integer>();

            for (int i = 0; i < size; i++) {
                int startHour = timetable[i];

                int posHour = startHour;
                int calHour = (posHour % 12 == 0) ? 12 : posHour % 12;

                String apm = "";
                if (posHour > 12 && posHour != 24) {
                    apm = "오후";
                } else if (posHour == 24) {
                    apm = "밤";
                } else if (posHour == 12) {
                    apm = "낮";
                } else {
                    apm = "오전";
                }

                if(isToday){
                    if(todayLimit+30 < startHour*60){
                        Log.e("init today limit", todayLimit + "::" + startHour*60);
                        // 수거배송 선택가능 시간대 : 현재기준 30분후
                        TIME_TABLE.add(" " + apm + " " + calHour + "시~" + (calHour + Integer.parseInt(deliveryDuration)) + "시");
                        finalTimeTable.add(startHour);
                    }else{
                        timeSelectedIndex++;
                    }
                }else{
                    TIME_TABLE.add(" " + apm + " " + calHour + "시~" + (calHour + Integer.parseInt(deliveryDuration)) + "시");
                    finalTimeTable.add(startHour);
                }
                //apm + " " + (startHour % 12) + "시 ~ " + (startHour % 12 + 1) + "시";
            }

            Log.i("finalTimeTable size", finalTimeTable.size() + "");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        if(_selectedDate != null){
            savedRestore();
        }
    }

    int timeSelectedIndex = 0;
    ArrayList<Integer> finalTimeTable;

    private void savedRestore(){

        if(mActivity.pickupReqTime == 0){
            if(selectedYear == -1){
                selectedYear = finalDatePickerContainer.get(0).get(Calendar.YEAR);
                selectedMonth = finalDatePickerContainer.get(0).get(Calendar.MONTH);
                selectedDay = finalDatePickerContainer.get(0).get(Calendar.DAY_OF_MONTH);
            }

            if (finalTimeTable.size() > 0) {
                int startHour = finalTimeTable.get(0);//.get(Calendar.HOUR_OF_DAY);
                int posHour = ((startHour-saleStartTime)*Integer.parseInt(deliveryDuration)+saleStartTime);
                selectedHour = posHour;
            } else {
                android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
                alertDialogBuilder.setCancelable(true).setMessage("오늘 주문가능한 시간이 지났습니다.")
                        .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().finish();
                            }
                        });
                android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }

        }else{
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(mActivity.pickupReqTime);
            //selectedYear = c.get(Calendar.YEAR);
            //selectedMonth = c.get(Calendar.MONTH);
            //selectedDay = c.get(Calendar.DAY_OF_MONTH);
            //selectedHour = c.get(Calendar.HOUR_OF_DAY);

            for (int i = 0; i < finalDatePickerContainer.size(); i++) {
                if (finalDatePickerContainer.get(i).get(Calendar.DAY_OF_MONTH) == c.get(Calendar.DAY_OF_MONTH)) {
                    selectedCurrentPositionDate = i;
                }
            }

            for (int i = 0; i < finalTimeTable.size(); i++) {
                //int posHour = ((c.get(Calendar.HOUR)-6)*Integer.parseInt(deliveryDuration)+6);
                Log.e("finalTimeTable : " + i, finalTimeTable.get(i) + " / " + c.get(Calendar.HOUR_OF_DAY));
                if (finalTimeTable.get(i) == c.get(Calendar.HOUR_OF_DAY)) {
                    wheelSelectedIndex = i;
                }
            }

            //final MultiViewPager pager = (MultiViewPager) mView.findViewById(R.id.pager);
            //pager.setCurrentItem(selectedCurrentPositionDate);

            String ori = mActivity.orderRequestItem;
            if ("1".equals(ori.substring(0, 1))) {
                aq.id(R.id.btn_service_01).image(R.mipmap.btn_order_service_01_pres);
            } else {
                aq.id(R.id.btn_service_01).image(R.drawable.btn_service_sel_01);
            }

            if ("1".equals(ori.substring(1, 2))) {
                aq.id(R.id.btn_service_02).image(R.mipmap.btn_order_service_02_pres);
            } else {
                aq.id(R.id.btn_service_02).image(R.drawable.btn_service_sel_02);
            }

            if ("1".equals(ori.substring(2, 3))) {
                aq.id(R.id.btn_service_03).image(R.mipmap.btn_order_service_03_pres);
            } else {
                aq.id(R.id.btn_service_03).image(R.drawable.btn_service_sel_03);
            }
        }

        setTimePicker();
    }

    private boolean checkTodayAvailable(){

        boolean isAvailable = false;
        try {

            JSONObject storeInfo = mActivity.storeInfo;
            Calendar current = Calendar.getInstance();
            String deliveryInfoX;
            String deliveryDurationX;
            int saleStartTimeX;
            String deliveryTimeX;
            int size = 0;

            if(TimeUtil.getWeekDayFromDate(current.getTime()) == 5){
                // 토요일
                deliveryInfoX = storeInfo.getString("storeDeliveryTimeSat");
            }else if(TimeUtil.getWeekDayFromDate(current.getTime()) == 6){
                // 일요일
                deliveryInfoX = storeInfo.getString("storeDeliveryTimeSun");
            }else{
                deliveryInfoX = storeInfo.getString("storeDeliveryTime");
            }
            deliveryDurationX = deliveryInfoX.split("\\|")[0];
            saleStartTimeX = 6 + Integer.parseInt(deliveryInfoX.split("\\|")[1]);
            deliveryTimeX = deliveryInfoX.split("\\|")[2];

            String avPos = (Integer.parseInt(deliveryDurationX)*2-1)+"";
            for (int i = 0; i < deliveryTimeX.length(); i++) {
                if (avPos.equals(String.valueOf(deliveryTimeX.charAt(i)))) {
                    size++;
                }
            }
            Log.i("Today Timetable", "Delivery size : " + size);

            int[] timetable = new int[size];
            int timeCount = 0;

            for (int i = 0; i < deliveryTimeX.length(); i++) {

                if (avPos.equals(String.valueOf(deliveryTimeX.charAt(i)))) {
                    timetable[timeCount] = i*Integer.parseInt(deliveryDurationX) + saleStartTimeX;//i+6;
                    Log.i("Today Timetable", size + " // " + timetable[timeCount] + "");
                    timeCount++;
                }
            }

            int todayLimit = current.get(Calendar.HOUR_OF_DAY)*60 + current.get(Calendar.MINUTE);

            if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
                if(todayLimit < 14*60 - Integer.parseInt(deliveryDurationX)*60){
                    return true;
                }else{
                    return false;
                }
            }else{
                for (int i = 0; i < size; i++) {
                    int startHour = timetable[i]*60;
                    Log.e("today limit", todayLimit + "::" + startHour);
                    if(todayLimit+30 < startHour){
                        return true;
                    }
                }
            }

        }catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    private int getAddDays(){
        int added = 0;

        String storeClosingDay = mActivity.storeClosingDay;
        String storeDayoff = mActivity.storeDayoff;

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(today.getTimeInMillis());
        int addSelectedYear = c.get(Calendar.YEAR);
        int addSelectedMonth = c.get(Calendar.MONTH);
        int addSelectedDay = c.get(Calendar.DATE);

        String[] splitClose = storeClosingDay.split("\\|");
        String[] splitDayOff = storeDayoff.split(",");
        if(StringUtil.isNullOrEmpty(storeDayoff)){
            splitDayOff = new String[0];
        }

        int wasdays = mActivity.washingDays;
        Log.i("getAddDays", added + " // " + mActivity.washingDays);

        for(int i = 0 ; i < wasdays; i++){
            // 정기휴무일 처리
            Calendar selDay = Calendar.getInstance();
            selDay.set(addSelectedYear, addSelectedMonth, addSelectedDay);
            selDay.add(Calendar.DATE, (i+1));

            if(selDay.get(Calendar.DAY_OF_WEEK) == 1){
                // 일요일인경우
                wasdays++;
                added += 1;
                Log.i("selDay : ", selDay.get(Calendar.YEAR) + "-" + (selDay.get(Calendar.MONTH) + 1) + "-" + selDay.get(Calendar.DATE));
                Log.i("getAddDays sunday", added + "");
            }else{
                // 임시휴무일 처리
                for(int m = 0; m < splitDayOff.length; m++){
                    String dateStr = splitDayOff[m];
                    String[] dateSplit = dateStr.split("-");
                    int ySplit = Integer.parseInt(dateSplit[0]);
                    int mSplit = Integer.parseInt(dateSplit[1]);
                    int dSplit = Integer.parseInt(dateSplit[2]);

                    Log.i("===========", "============");
                    Log.i("tempHoliday : ", ySplit + "-" + mSplit + "-" + dSplit);

                    if(ySplit == selDay.get(Calendar.YEAR) && mSplit == (selDay.get(Calendar.MONTH)+1) && dSplit == selDay.get(Calendar.DATE)){
                        added += 1;
                        wasdays++;
                        Log.i("selDay : ", selDay.get(Calendar.YEAR) + "-" + (selDay.get(Calendar.MONTH) + 1) + "-" + selDay.get(Calendar.DATE));
                        Log.i("getAddDays holiday", added + "");
                    }
                }
            }

        }

        return added;
    }

    private void removeHoliday(){

        String storeClosingDay = mActivity.storeClosingDay;
        String storeDayoff = mActivity.storeDayoff;

        String[] splitClose = storeClosingDay.split("\\|");
        String[] splitDayOff = storeDayoff.split(",");
        if(StringUtil.isNullOrEmpty(storeDayoff)){
            splitDayOff = new String[0];
        }

        String gweek = splitClose[0];
        String holidayWeekValue = splitClose[1];

        finalDatePickerContainer = new ArrayList<Calendar>();

        Log.i("gweek", gweek + "");
        for(int i = 0 ; i < datePickerContainer.size(); i++){
            boolean isHoliday = false;
            // 정기휴무일 처리
            if("every".equals(gweek)) {
                for (int k = 0; k < holidayWeekValue.length(); k++) {
                    if ("1".equals(String.valueOf(holidayWeekValue.charAt(k)))) {
                        if(TimeUtil.convertWeekValueToInt(datePickerContainer.get(i).get(Calendar.DAY_OF_WEEK)) == k){
                            // 휴무일 제거
                            //Log.i("holidayWeekValue", "Remove holidayWeekValue : " + datePickerContainer.get(i).get(Calendar.DAY_OF_WEEK));
                            isHoliday = true;
                        }
                    }
                }
            }

            // 임시휴무일 처리
            for(int m = 0; m < splitDayOff.length; m++){
                String dateStr = splitDayOff[m];
                String[] dateSplit = dateStr.split("-");
                int ySplit = Integer.parseInt(dateSplit[0]);
                int mSplit = Integer.parseInt(dateSplit[1]);
                int dSplit = Integer.parseInt(dateSplit[2]);
                //Log.i("tempHoliday : ", ySplit + "-" + mSplit + "-" + dSplit);

                if(ySplit == datePickerContainer.get(i).get(Calendar.YEAR) && mSplit == (datePickerContainer.get(i).get(Calendar.MONTH)+1) && dSplit == datePickerContainer.get(i).get(Calendar.DAY_OF_MONTH)){
                    isHoliday = true;
                }
            }

            if(!isHoliday){
                if(finalDatePickerContainer.size() < 10){
                    //Date now = new Date();
                    //if(now.getHours()*60+now.getMinutes() >
                    finalDatePickerContainer.add(datePickerContainer.get(i));
                }else{
                    Log.i("Date Array Count","Date Element Count rebalanced to 10");
                }
            }
        }

    }

    int selectedCurrentPositionDate = 0;
    int selectedCurrentPositionHour = 0;
    int wheelSelectedIndex = 0;

    private void setTimePicker() {
/*
        if(wva == null){

        }else{
            wva = new WheelView(getActivity());
        }*/
        wheelSelectedIndex = 0;

        if(finalTimeTable == null || finalTimeTable.size() == 0){
            Log.e("fuinalTimeTable", "0");
        }else{
            Log.e("fuinalTimeTable", finalTimeTable.size() +"");
            selectedHour = finalTimeTable.get(wheelSelectedIndex);

            Log.e("pickup date", selectedMonth + "/" + selectedDay);
            Log.e("setTimePicker Timetable", finalTimeTable.size() +"/"+ finalTimeTable.get(0).toString() + "/" + selectedCurrentPositionHour);
            wva = (WheelView) mView.findViewById(R.id.wheel_view_wv);
            wva.setItems(TIME_TABLE);
            wva.setOffset(wheelSelectedIndex+1);
            //wva.setSeletion(wheelSelectedIndex+1);
            wva.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                @Override
                public void onSelected(int selectedIndex, String item) {
                    // selectedIndex 1부터 시작
                    if (selectedIndex > finalTimeTable.size()) {
                        selectedIndex = finalTimeTable.size();
                    }
                    if(selectedIndex < 1){
                        selectedIndex = 1;
                    }
                    wheelSelectedIndex = selectedIndex;

                    Log.i(TAG, "selectedIndex: " + finalTimeTable.get(selectedIndex-1) + "시, item: " + item);
                    selectedHour = finalTimeTable.get(selectedIndex-1);
                }
            });

            Log.e("pickup date", selectedYear + "/" + selectedMonth + "/" + selectedDay + "/" + selectedHour);

            if(dialog != null){
                dialog.dismiss();
            }
        }


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
