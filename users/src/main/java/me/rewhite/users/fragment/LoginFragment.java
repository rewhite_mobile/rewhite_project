package me.rewhite.users.fragment;

//import com.actionbarsherlock.app.SherlockFragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import fr.castorflex.android.verticalviewpager.VerticalViewPager;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.SignActivity;
import me.rewhite.users.adapter.SignMainFragmentAdapter;


@SuppressLint("NewApi")
public class LoginFragment extends Fragment implements IFragment {

    private static String TAG = "LoginFragment";
    private boolean mIsUpState = true;

    @Override
    public void dismiss() {

    }

    private SignActivity mSignActivity = null;

    private ViewGroup mView;
    private AQuery aq;

    private VerticalViewPager verticalViewPager;
    private SignMainFragmentAdapter pageAdapter;

    protected static final String[] CONTENT = new String[]{"SignMainTop", "SignMainBottom"};
    private static final float MIN_SCALE = 0.75f;
    private static final float MIN_ALPHA = 0.75f;
    //private List<Fragment> fragments;

    private String mContent = "???";

	/*
     * @Override
	 * public void onAttach(Activity activity) {
	 * super.onAttach(activity);
	 * mSignActivity = (SignActivity) activity;
	 * mSignActivity.TAG = getClass().getSimpleName();
	 * Log.d(TAG, "[mSignActivity.TAG]" + mSignActivity.TAG);
	 * }
	 */

    public void screenToggle(boolean _isTop) {
        if (_isTop) {
            verticalViewPager.setCurrentItem(1);
        } else {
            verticalViewPager.setCurrentItem(0);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Tracker t = ((Bootstrap) getActivity().getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        mView = (ViewGroup) inflater.inflate(R.layout.fragment_login, container, false);
        mSignActivity = (SignActivity) getActivity();
        pageAdapter = new SignMainFragmentAdapter(mSignActivity);

        verticalViewPager = (VerticalViewPager) mView.findViewById(R.id.pager);
        verticalViewPager.setAdapter(pageAdapter);
        verticalViewPager.setPageMargin(0);

        verticalViewPager.setPageTransformer(true, new ViewPager.PageTransformer() {

            @Override
            public void transformPage(View view, float position) {
                int pageWidth = view.getWidth();
                int pageHeight = view.getHeight();

                if (position < -1) { // [-Infinity,-1)
                    // This page is way off-screen to the left.
                    view.setAlpha(0);

                } else if (position <= 1) { // [-1,1]
                    // Modify the default slide transition to shrink the page as well
                    float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                    float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                    float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                    if (position < 0) {
                        view.setTranslationY(vertMargin - horzMargin / 2);
                    } else {
                        view.setTranslationY(-vertMargin + horzMargin / 2);
                    }

                    // Scale the page down (between MIN_SCALE and 1)
                    view.setScaleX(scaleFactor);
                    view.setScaleY(scaleFactor);

                    // Fade the page relative to its size.
                    view.setAlpha(MIN_ALPHA + (scaleFactor - MIN_SCALE) / (1 - MIN_SCALE) * (1 - MIN_ALPHA));

                } else { // (1,+Infinity]
                    // This page is way off-screen to the right.
                    view.setAlpha(0);
                }
            }
        });

        verticalViewPager.invalidate();
        verticalViewPager.requestLayout();

        Log.d(TAG, "[onCreateView] ");

        return mView;
    }

    @Override
    public void onResume() {
        Log.d(TAG, "[onResume]");
        super.onResume();

        verticalViewPager.invalidate();
        verticalViewPager.requestLayout();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {
        // TODO Auto-generated method stub

    }
}
