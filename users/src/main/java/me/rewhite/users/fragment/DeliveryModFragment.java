package me.rewhite.users.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.pixplicity.multiviewpager.MultiViewPager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import me.rewhite.users.R;
import me.rewhite.users.activity.ModDeliveryInfoActivity;
import me.rewhite.users.layout.WheelView;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.StringUtil;
import me.rewhite.users.util.TimeUtil;

public class DeliveryModFragment extends Fragment implements IFragment {

    private final static String TAG = "DeliveryReqFragment";
    private ModDeliveryInfoActivity mActivity;

    private static final String STORE_INFO = "storeInfo";
    private JSONObject storeInfo;

    private ViewGroup mView = null;
    int[] timetable;
    ArrayList<String> TIME_TABLE;
    private WheelView wva;

    Calendar today;
    public AQuery aq;

    public DeliveryModFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {

        Log.d(TAG, "[onResume]");
        mActivity.setTouchDisable(false);

        super.onResume();

        today = Calendar.getInstance(Locale.KOREA);
        today.setTimeInMillis(mActivity.pickupReqTime);

        Calendar pickupReqTimeCal = Calendar.getInstance(Locale.KOREA);
        pickupReqTimeCal.setTimeInMillis(mActivity.pickupReqTime);
        pickupReqTimeCal.add(Calendar.DATE, mActivity.washingDays + getAddDays());
        Calendar current = Calendar.getInstance(Locale.KOREA);

        int pickupValue = pickupReqTimeCal.get(Calendar.YEAR)*10000 + pickupReqTimeCal.get(Calendar.MONTH) * 100 + pickupReqTimeCal.get(Calendar.DATE);
        int currentValue = current.get(Calendar.YEAR)*10000 + current.get(Calendar.MONTH) * 100 + current.get(Calendar.DATE);

        if(pickupValue < currentValue){
            today = current;
            today.add(Calendar.DATE, -1*(mActivity.washingDays+ getAddDays()));
        }else{
            today.setTimeInMillis(mActivity.pickupReqTime);
        }

        initPreTimeData();
        initDateData();
        mActivity.setTitle("배송요청일 변경");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (ModDeliveryInfoActivity) getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup) inflater.inflate(R.layout.fragment_delivery_mod, container, false);

        aq = new AQuery(mView);
        today = Calendar.getInstance(Locale.KOREA);
        today.setTimeInMillis(mActivity.pickupReqTime);

        Calendar pickupReqTimeCal = Calendar.getInstance(Locale.KOREA);
        pickupReqTimeCal.setTimeInMillis(mActivity.pickupReqTime);
        pickupReqTimeCal.add(Calendar.DATE, mActivity.washingDays+ getAddDays());
        Calendar current = Calendar.getInstance(Locale.KOREA);

        int pickupValue = pickupReqTimeCal.get(Calendar.YEAR)*10000 + pickupReqTimeCal.get(Calendar.MONTH) * 100 + pickupReqTimeCal.get(Calendar.DATE);
        int currentValue = current.get(Calendar.YEAR)*10000 + current.get(Calendar.MONTH) * 100 + current.get(Calendar.DATE);

        if(pickupValue < currentValue){
            today = current;
            today.add(Calendar.DATE, -1*(mActivity.washingDays+ getAddDays()));
        }else{
            today.setTimeInMillis(mActivity.pickupReqTime);
        }

        initPreTimeData();
        initDateData();
        aq.id(R.id.btn_next).clicked(this, "deliverySetupComplete");

        return mView;
    }

    int DATE_RANGE = 15;

    int selectedYear = -1;
    int selectedMonth = -1;
    int selectedDay = -1;
    int selectedHour = 0;

    public void deliverySetupComplete(View button) {
        if (selectedYear != -1 && selectedMonth != -1 && selectedDay != -1) {
            if (selectedHour != 0) {
                Date pickupDate = TimeUtil.getDate(selectedYear, selectedMonth, selectedDay, selectedHour);
                Calendar pickDays = Calendar.getInstance();
                pickDays.setTimeInMillis(pickupDate.getTime());
                int selectedYoil = pickDays.get(Calendar.DAY_OF_WEEK);

                Log.e("selected yoil", selectedYoil + "");
                boolean isSaturday = false;
                int limitSaturPickReqTime = 0;
                if(selectedYoil == 7){
                    isSaturday = true;
                    String storeDeliveryTimeSat = "";
                    try{
                        storeDeliveryTimeSat = mActivity.storeInfo.getString("storeDeliveryTimeSat");
                    }catch (JSONException e){

                    }
                    String[] satSet = storeDeliveryTimeSat.split("\\|");
                    int satDuration = Integer.parseInt(satSet[0]);
                    int satStartPos = Integer.parseInt(satSet[1]);
                    String satTimetable = satSet[2];

                    int endIndex = 0;
                    for(int i =0; i < satTimetable.length(); i++){
                        if(Integer.parseInt(String.valueOf(satTimetable.charAt(i)))%2 == 1){
                            endIndex = i;
                        }
                    }
                    limitSaturPickReqTime = satDuration*endIndex + (satStartPos+6);
                    Log.e("limitSaturPickReqTime", limitSaturPickReqTime + " / " + pickDays.get(Calendar.HOUR_OF_DAY));
                }

                mActivity.deliveryReqTime = pickupDate.getTime();
                mActivity.modInfo();


            } else {
                DUtil.alertShow(getActivity(), "시간을 선택해주세요");
            }
        } else {
            DUtil.alertShow(getActivity(), "날짜를 선택해주세요");
        }
    }

    private void setDatePicker() {
        final MultiViewPager pager = (MultiViewPager) mView.findViewById(R.id.pager);


        final FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(getChildFragmentManager()) {
            //ArrayList<DatePickFragment> container = new ArrayList<DatePickFragment>();

            @Override
            public int getCount() {
                return finalDatePickerContainer.size();
            }

            @Override
            public DatePickFragment getItem(int position) {
                return DatePickFragment.create(finalDatePickerContainer.get(position).get(Calendar.DAY_OF_MONTH));
            }

        };
        pager.setAdapter(adapter);
        pager.setCurrentItem(selectedCurrentPositionDate);
        pager.setOnPageChangeListener(new MultiViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.i("Date onPageSelected", "selected days : " + finalDatePickerContainer.get(position).get(Calendar.DAY_OF_MONTH));

                int day_of_week = finalDatePickerContainer.get(position).get(Calendar.DAY_OF_WEEK);
                setYoilText(day_of_week);

                selectedYear = finalDatePickerContainer.get(position).get(Calendar.YEAR);
                selectedMonth = finalDatePickerContainer.get(position).get(Calendar.MONTH);
                selectedDay = finalDatePickerContainer.get(position).get(Calendar.DAY_OF_MONTH);

                initTimeData(finalDatePickerContainer.get(position).getTime());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setYoilText(int value) {
        String m_week = "";

        if (value == 1)
            m_week = "일요일";
        else if (value == 2)
            m_week = "월요일";
        else if (value == 3)
            m_week = "화요일";
        else if (value == 4)
            m_week = "수요일";
        else if (value == 5)
            m_week = "목요일";
        else if (value == 6)
            m_week = "금요일";
        else if (value == 7)
            m_week = "토요일";

        aq.id(R.id.yoil_text).text(m_week).typeface(CommonUtility.getNanumBarunTypeface());
    }

    ArrayList<Calendar> datePickerContainer;
    ArrayList<Calendar> finalDatePickerContainer;



    private void initDateData(){
        Calendar base = Calendar.getInstance();
        base.setTimeInMillis(today.getTimeInMillis());
        int washingDays = mActivity.washingDays;
        int addDays = getAddDays();
        // 배달일 이전에 휴무일 계산
        washingDays += addDays;
        base.add(Calendar.DATE, washingDays);

        Calendar current = Calendar.getInstance();
        int pickupValue = base.get(Calendar.YEAR)*10000 + base.get(Calendar.MONTH) * 100 + base.get(Calendar.DATE);
        int currentValue = current.get(Calendar.YEAR)*10000 + current.get(Calendar.MONTH) * 100 + current.get(Calendar.DATE);
        Log.e("initDateData today compare", pickupValue + "::" + currentValue);
        boolean isAvailablePickup = false;
        isAvailablePickup = checkTodayAvailable();

        datePickerContainer = new ArrayList<Calendar>();

        for (int d = 0; d < DATE_RANGE; d++) {
            Calendar dr = Calendar.getInstance();
            dr.setTimeInMillis(today.getTimeInMillis());

            if(pickupValue == currentValue){
                // 오늘일때 주문가능여부 체크
                if(isAvailablePickup){
                    dr.add(Calendar.DATE, washingDays + d);
                }else{
                    dr.add(Calendar.DATE, washingDays + (d+1));
                }
            }else{
                dr.add(Calendar.DATE, washingDays + (d+1));
            }

            datePickerContainer.add(dr);
        }

        removeHoliday();
        // 현재선택값 요일값 세팅
        setYoilText(finalDatePickerContainer.get(0).get(Calendar.DAY_OF_WEEK));
        initTimeData(finalDatePickerContainer.get(0).getTime());
        setDatePicker();
    }

    private boolean checkTodayAvailable(){

        boolean isAvailable = false;
        try {

            JSONObject storeInfo = mActivity.storeInfo;
            Calendar current = Calendar.getInstance();
            String deliveryInfoX;
            String deliveryDurationX;
            int saleStartTimeX;
            String deliveryTimeX;
            int size = 0;

            if(TimeUtil.getWeekDayFromDate(current.getTime()) == 5){
                // 토요일
                deliveryInfoX = storeInfo.getString("storeDeliveryTimeSat");
            }else if(TimeUtil.getWeekDayFromDate(current.getTime()) == 6){
                // 일요일
                deliveryInfoX = storeInfo.getString("storeDeliveryTimeSun");
            }else{
                deliveryInfoX = storeInfo.getString("storeDeliveryTime");
            }
            deliveryDurationX = deliveryInfoX.split("\\|")[0];
            saleStartTimeX = 6 + Integer.parseInt(deliveryInfoX.split("\\|")[1]);
            deliveryTimeX = deliveryInfoX.split("\\|")[2];

            String avPos = (Integer.parseInt(deliveryDurationX)*2-1)+"";
            for (int i = 0; i < deliveryTimeX.length(); i++) {
                if (avPos.equals(String.valueOf(deliveryTimeX.charAt(i)))) {
                    size++;
                }
            }
            Log.i("Today Timetable", "Delivery size : " + size);

            int[] timetable = new int[size];
            int timeCount = 0;

            for (int i = 0; i < deliveryTimeX.length(); i++) {

                if (avPos.equals(String.valueOf(deliveryTimeX.charAt(i)))) {
                    timetable[timeCount] = i*Integer.parseInt(deliveryDurationX) + saleStartTimeX;//i+6;
                    Log.i("Today Timetable", size + " // " + timetable[timeCount] + "");
                    timeCount++;
                }
            }

            int todayLimit = current.get(Calendar.HOUR_OF_DAY)*60 + current.get(Calendar.MINUTE);

            for (int i = 0; i < size; i++) {
                int startHour = timetable[i]*60;
                Log.e("today limit", todayLimit + "::" + startHour);
                if(todayLimit + 30 < startHour){
                    // 수거배송 선택가능 시간대 : 현재기준 30분후
                    //isAvailable = true;
                    return true;
                }
            }

        }catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void initPreTimeData(){
        initTimeData(null);
    }

    String deliveryInfo;
    String deliveryDuration;
    String deliveryTime;
    int saleStartTime;
    ArrayList<Integer> finalTimeTable;

    private void initTimeData(Date _selectedDate) {
        Date selectedDate = _selectedDate;
        JSONObject storeInfo = mActivity.storeInfo;
        int size = 0;
        int todayLimit = 0;
        boolean isToday = false;

        try {
            if(selectedDate == null){
                Calendar base = Calendar.getInstance(Locale.KOREA);
                base.setTimeInMillis(today.getTimeInMillis());
                int washingDays = mActivity.washingDays;
                int addDays = getAddDays();
                // 배달일 이전에 휴무일 계산
                washingDays += addDays;
                base.add(Calendar.DATE, washingDays);

                selectedDate = base.getTime();
            }

            Calendar selectedCal = Calendar.getInstance(Locale.KOREA);
            selectedCal.setTime(selectedDate);
            Calendar current = Calendar.getInstance(Locale.KOREA);
            current.setTimeInMillis((new Date()).getTime());

            int pickupValue = selectedCal.get(Calendar.YEAR)*10000 + selectedCal.get(Calendar.MONTH) * 100 + selectedCal.get(Calendar.DATE);
            int currentValue = current.get(Calendar.YEAR)*10000 + current.get(Calendar.MONTH) * 100 + current.get(Calendar.DATE);
            if(pickupValue == currentValue){
                isToday = true;
                todayLimit = current.get(Calendar.HOUR_OF_DAY)*60 + current.get(Calendar.MINUTE);
                Log.e("today", "TODAY is true / " + todayLimit);
            }

            if(TimeUtil.getWeekDayFromDate(selectedDate) == 5){
                // 토요일
                deliveryInfo = storeInfo.getString("storeDeliveryTimeSat");
            }else if(TimeUtil.getWeekDayFromDate(selectedDate) == 6){
                // 일요일
                deliveryInfo = storeInfo.getString("storeDeliveryTimeSun");
            }else{
                deliveryInfo = storeInfo.getString("storeDeliveryTime");
            }

            deliveryDuration = deliveryInfo.split("\\|")[0];
            saleStartTime = 6 + Integer.parseInt(deliveryInfo.split("\\|")[1]);
            deliveryTime = deliveryInfo.split("\\|")[2];
            //String deliveryTime = storeInfo.getString("storeDeliveryTime");
            Log.i("deliveryTime : ", deliveryTime);

            String avPos = (Integer.parseInt(deliveryDuration)*2-1)+"";
            for (int i = 0; i < deliveryTime.length(); i++) {
                if (avPos.equals(String.valueOf(deliveryTime.charAt(i)))) {
                    size++;
                }
            }
            Log.i("Delivery Timetable", "Delivery size : " + size);

            timetable = new int[size];
            int timeCount = 0;

            for (int i = 0; i < deliveryTime.length(); i++) {

                if (avPos.equals(String.valueOf(deliveryTime.charAt(i)))) {
                    timetable[timeCount] = i*Integer.parseInt(deliveryDuration) + saleStartTime;//i+6;
                    Log.i("Delivery Timetable", size + " // " + timetable[timeCount] + "");
                    timeCount++;
                }
            }

            //06:00 ~ 23:00
            //{"06:00 ~ 07:00 오전", "07:00 ~ 08:00 오전", "08:00 ~ 09:00 오전", "09:00 ~ 10:00 오전", "16:00 ~ 17:00 오전", "06:00 ~ 07:00 오전", "06:00 ~ 07:00 오전", "06:00 ~ 07:00 오전"};
            TIME_TABLE = new ArrayList<String>();
            finalTimeTable = new ArrayList<Integer>();

            for (int i = 0; i < size; i++) {
                int startHour = timetable[i];

                int posHour = startHour;
                int calHour = (posHour % 12 == 0) ? 12 : posHour % 12;

                String apm = "";
                if (posHour > 12 && posHour != 24) {
                    apm = "오후";
                } else if (posHour == 24) {
                    apm = "밤";
                } else if (posHour == 12) {
                    apm = "낮";
                } else {
                    apm = "오전";
                }

                if(isToday){
                    if(todayLimit+30 < startHour*60){
                        Log.e("init today limit", todayLimit + "::" + startHour*60);
                        // 수거배송 선택가능 시간대 : 현재기준 30분후
                        TIME_TABLE.add(" " + apm + " " + calHour + "시~" + (calHour + Integer.parseInt(deliveryDuration)) + "시");
                        finalTimeTable.add(startHour);
                    }
                }else{
                    TIME_TABLE.add(" " + apm + " " + calHour + "시~" + (calHour + Integer.parseInt(deliveryDuration)) + "시");
                    finalTimeTable.add(startHour);
                }
                //apm + " " + (startHour % 12) + "시 ~ " + (startHour % 12 + 1) + "시";
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(_selectedDate != null){
            savedRestore();
        }
    }

    private void savedRestore(){
        if(timetable.length == 0){
            android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setCancelable(true).setMessage("영업시간이 설정되지않았습니다.")
                    .setPositiveButton("네", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getActivity().finish();
                        }
                    }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else{
            if(selectedYear == -1){
                selectedYear = finalDatePickerContainer.get(0).get(Calendar.YEAR);
                selectedMonth = finalDatePickerContainer.get(0).get(Calendar.MONTH);
                selectedDay = finalDatePickerContainer.get(0).get(Calendar.DAY_OF_MONTH);
                selectedHour = finalTimeTable.get(0);
            }
        }

        setTimePicker();
    }

    private int getAddDays(){
        int added = 0;

        String storeClosingDay = mActivity.storeClosingDay;
        String storeDayoff = mActivity.storeDayoff;

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(today.getTimeInMillis());
        int addSelectedYear = c.get(Calendar.YEAR);
        int addSelectedMonth = c.get(Calendar.MONTH);
        int addSelectedDay = c.get(Calendar.DATE);

        String[] splitClose = storeClosingDay.split("\\|");
        String[] splitDayOff = storeDayoff.split(",");
        if(StringUtil.isNullOrEmpty(storeDayoff)){
            splitDayOff = new String[0];
        }

        int wasdays = mActivity.washingDays;
        Log.e("getAddDays", added + " // " + mActivity.washingDays);

        for(int i = 0 ; i < wasdays; i++){
            // 정기휴무일 처리
            Calendar selDay = Calendar.getInstance();
            selDay.set(addSelectedYear, addSelectedMonth, addSelectedDay);
            selDay.add(Calendar.DATE, (i+1));

            if(selDay.get(Calendar.DAY_OF_WEEK) == 1){
                // 일요일인경우
                wasdays++;
                added += 1;
                Log.e("selDay : ", selDay.get(Calendar.YEAR) + "-" + (selDay.get(Calendar.MONTH)+1) + "-" + selDay.get(Calendar.DATE));
                Log.e("getAddDays sunday", added + "");
            }else{
                // 임시휴무일 처리
                for(int m = 0; m < splitDayOff.length; m++){
                    String dateStr = splitDayOff[m];
                    String[] dateSplit = dateStr.split("-");
                    int ySplit = Integer.parseInt(dateSplit[0]);
                    int mSplit = Integer.parseInt(dateSplit[1]);
                    int dSplit = Integer.parseInt(dateSplit[2]);

                    Log.i("=========================", "=========================");
                    Log.i("tempHoliday : ", ySplit + "-" + mSplit + "-" + dSplit);

                    if(ySplit == selDay.get(Calendar.YEAR) && mSplit == (selDay.get(Calendar.MONTH)+1) && dSplit == selDay.get(Calendar.DATE)){
                        added += 1;
                        wasdays++;
                        Log.e("selDay : ", selDay.get(Calendar.YEAR) + "-" + (selDay.get(Calendar.MONTH)+1) + "-" + selDay.get(Calendar.DATE));
                        Log.e("getAddDays holiday", added + "");
                    }
                }
            }

        }

        return added;
    }

    private void removeHoliday(){

        String storeClosingDay = mActivity.storeClosingDay;
        String storeDayoff = mActivity.storeDayoff;

        String[] splitClose = storeClosingDay.split("\\|");
        String[] splitDayOff = storeDayoff.split(",");
        if(StringUtil.isNullOrEmpty(storeDayoff)){
            splitDayOff = new String[0];
        }

        String gweek = splitClose[0];
        String holidayWeekValue = splitClose[1];

        finalDatePickerContainer = new ArrayList<Calendar>();

        Log.i("gweek", gweek + "");
        for(int i = 0 ; i < datePickerContainer.size(); i++){
            boolean isHoliday = false;
            // 정기휴무일 처리
            if("every".equals(gweek)) {
                for (int k = 0; k < holidayWeekValue.length(); k++) {
                    if ("1".equals(String.valueOf(holidayWeekValue.charAt(k)))) {
                        if(TimeUtil.convertWeekValueToInt(datePickerContainer.get(i).get(Calendar.DAY_OF_WEEK)) == k){
                            // 휴무일 제거
                            Log.i("holidayWeekValue", "Remove holidayWeekValue : " + datePickerContainer.get(i).get(Calendar.DAY_OF_WEEK));
                            isHoliday = true;
                        }
                    }
                }
            }

            // 임시휴무일 처리
            for(int m = 0; m < splitDayOff.length; m++){
                String dateStr = splitDayOff[m];
                String[] dateSplit = dateStr.split("-");
                int ySplit = Integer.parseInt(dateSplit[0]);
                int mSplit = Integer.parseInt(dateSplit[1]);
                int dSplit = Integer.parseInt(dateSplit[2]);
                Log.i("tempHoliday : ", ySplit + "-" + mSplit + "-" + dSplit);

                if(ySplit == datePickerContainer.get(i).get(Calendar.YEAR) && mSplit == (datePickerContainer.get(i).get(Calendar.MONTH)+1) && dSplit == datePickerContainer.get(i).get(Calendar.DAY_OF_MONTH)){
                    isHoliday = true;
                }
            }

            if(!isHoliday){
                if(finalDatePickerContainer.size() < 10){
                    finalDatePickerContainer.add(datePickerContainer.get(i));
                }else{
                    Log.i("Date Array Count","Date Element Count rebalanced to 10");
                }
            }
        }

    }

    int selectedCurrentPositionDate = 0;
    int selectedCurrentPositionHour = 1;
    int wheelSelectedIndex = 0;

    private void setTimePicker() {

        if(wva == null){

        }else{
            wva = new WheelView(getActivity());
        }
        wva = (WheelView) mView.findViewById(R.id.wheel_view_wv);
        wva.invalidate();
        wva.setItems(TIME_TABLE);
        //wva.setOffset(selectedCurrentPositionHour);
        wva.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                // selectedIndex 1부터 시작
                if(selectedIndex > finalTimeTable.size()){
                    selectedIndex = finalTimeTable.size();
                }
                wheelSelectedIndex = selectedIndex;
                Log.i(TAG, "selectedIndex: " + finalTimeTable.get(selectedIndex - 1) + "시, item: " + item);
                selectedHour =  finalTimeTable.get(selectedIndex - 1);
            }
        });
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }
}
