package me.rewhite.users.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pixplicity.multiviewpager.MultiViewPager;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.OrderActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.PickupType;
import me.rewhite.users.layout.WheelView;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.StringUtil;
import me.rewhite.users.util.TimeUtil;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DeliveryReqFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DeliveryReqFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DeliveryReqFragment extends Fragment implements IFragment {

    private final static String TAG = "DeliveryReqFragment";
    private OrderActivity mActivity;

    private static final String STORE_INFO = "storeInfo";
    private JSONObject storeInfo;

    private ViewGroup mView = null;
    int[] timetable;
    ArrayList<String> TIME_TABLE;
    private WheelView wva;

    Calendar today;
    public AQuery aq;
    InputMethodManager imm;

    public static DeliveryReqFragment newInstance(String storeInfo) {
        DeliveryReqFragment fragment = new DeliveryReqFragment();
        Bundle args = new Bundle();
        args.putString(STORE_INFO, storeInfo);
        fragment.setArguments(args);
        return fragment;
    }

    public DeliveryReqFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {

        Log.d(TAG, "[onResume]");
        mActivity.setTouchDisable(false);

        super.onResume();

        today = Calendar.getInstance(Locale.KOREA);
        today.setTimeInMillis(mActivity.pickupReqTime);

        initPreTimeData();
        initDateData();

        String titleStr = "";
        if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
            titleStr = "배송 신청 (당일배송)";
        }else if(mActivity.pickupMode == PickupType.PICKUPTYPE_TOMORROW){
            titleStr = "배송 신청 (익일배송)";
        }else{
            titleStr = "배송 신청";
        }

        mActivity.setTitle(titleStr);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = (OrderActivity) getActivity();
        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        if (getArguments() != null) {
            try {
                storeInfo = new JSONObject(getArguments().getString(STORE_INFO));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = (ViewGroup) inflater.inflate(R.layout.fragment_delivery_req, container, false);

        aq = new AQuery(mView);


        today = Calendar.getInstance(Locale.KOREA);
        today.setTimeInMillis(mActivity.pickupReqTime);

        initPreTimeData();
        initDateData();

        aq.id(R.id.btn_next).clicked(this, "deliverySetupComplete");
        aq.id(R.id.btn_req).clicked(this, "deliveryRequest");
        aq.id(R.id.input_box).gone();

        return mView;
    }

    public void hideKeyboard(EditText editText) {
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }


    int DATE_RANGE = 15;
    int selectedYear = -1;
    int selectedMonth = -1;
    int selectedDay = -1;
    int selectedHour = 0;

    final String items[] = {"수거/배송전 전화연락바랍니다", "부재시 연락주세요", "직접 입력"};
    int selectedIndex = 0;
    String messageString = "";

    public void deliveryRequest(View button) {
        AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
        ab.setTitle("수거/배송시 요청사항 선택");
        ab.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedIndex = which;
            }
        }).setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (selectedIndex == 2) {
                    aq.id(R.id.input_box).visible();
                } else {

                    messageString = items[selectedIndex];
                    aq.id(R.id.text_message).text(messageString).typeface(CommonUtility.getNanumBarunTypeface());

                    aq.id(R.id.input_box).gone();
                }

            }
        }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        ab.show();
    }

    public void deliverySetupComplete(View button) {
        Log.e("deliveryDate : ", selectedYear + "/" + selectedMonth + "/" + selectedDay);
        AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
        logger.logEvent("Normal order Step2");

        if (selectedYear != -1 && selectedMonth != -1 && selectedDay != -1) {
            if (selectedHour != 0) {
                if (StringUtil.isNullOrEmpty(aq.id(R.id.edit_req).getText().toString()) && selectedIndex == 2) {
                    DUtil.alertShow(getActivity(), "요청사항을 적어주세요");
                } else {
                    if (selectedIndex == 2) {
                        messageString = aq.id(R.id.edit_req).getText().toString();
                    }

                    EditText ib = aq.id(R.id.edit_req).getEditText();
                    hideKeyboard(ib);

                    Date pickupDate = TimeUtil.getDate(selectedYear, selectedMonth, selectedDay, selectedHour);
                    Log.e("delivery date", selectedYear + "/" + selectedMonth + "/" +selectedDay + "/" +selectedHour);

                    mActivity.deliveryReqTime = pickupDate.getTime();
                    mActivity.orderRequestString = messageString;
                    Log.i("PickupReqTime", mActivity.pickupReqTime + " / " + mActivity.deliveryReqTime);



                    if (StringUtil.isNullOrEmpty(messageString)) {
                        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
                        alertDialogBuilder.setCancelable(false).setMessage("요청사항을 적어주세요.")
                                .setPositiveButton("네", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                }).setCancelable(true);
                        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else {

                        //ORDER_AVAILABLE_CHECK
                        try {
                            RequestParams params = new RequestParams();
                            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                            params.put("storeId", mActivity.storeInfo.getString("storeId"));
                            Date orderDate = new Date();
                            orderDate.setTime(mActivity.deliveryReqTime);
                            params.put("orderDate", TimeUtil.getOrderDateFormat(orderDate));
                            params.put("k", 1);
                            NetworkClient.post(Constants.ORDER_AVAILABLE_CHECK, params, new AsyncHttpResponseHandler() {

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                                    // TODO Auto-generated method stub
                                    DUtil.Log(Constants.ORDER_AVAILABLE_CHECK, error.getMessage());
                                    Toast.makeText(getActivity().getApplicationContext(), "오류가 발생했습니다.\n" + error.getMessage(), Toast.LENGTH_LONG);
                                }

                                @Override
                                public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                                    String result;
                                    try {
                                        result = new String(data, "UTF-8");
                                        DUtil.Log(Constants.ORDER_AVAILABLE_CHECK, result);

                                        JSONObject jsondata = new JSONObject(result);

                                        if ("S0000".equals(jsondata.getString("resultCode"))) {
                                            Tracker t = ((Bootstrap) getActivity().getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                                            t.send(new HitBuilders.EventBuilder()
                                                    .setCategory("주문")
                                                    .setAction("배송일자선택")
                                                    //.setLabel("Order")
                                                    .build());
                                            int duration = mActivity.storeDeliveryDuration;
                                            int startTime = mActivity.storeSaleStartTime;
                                            Date oDate = new Date();
                                            oDate.setTime(mActivity.deliveryReqTime);
                                            SimpleDateFormat objFormatter = new SimpleDateFormat("HH");
                                            String startHour = objFormatter.format(oDate);

                                            int index = (Integer.parseInt(startHour) - startTime) / Integer.parseInt(deliveryDuration);


                                            String deliveryData = jsondata.getString("data");
                                            String deliveryDatum = deliveryData.split("\\|")[2];

                                            if(deliveryDatum.length() < (index+1)){
                                                index = deliveryDatum.length()-1;
                                            }
                                            if ("X".equals(String.valueOf(deliveryDatum.charAt(wheelSelectedIndex)))) {
                                                // 해당시간에 주문량이 너무 많습니다.
                                                //Toast.makeText(getActivity(), "선택하신 시간대에 접수량이 많습니다. 죄송하지만 다른 시간을 선택해주시겠어요?", Toast.LENGTH_LONG).show();
                                                DUtil.alertShow(getActivity(), "선택하신 시간대에 접수량이 많습니다. 죄송하지만 다른 시간을 선택해주시겠어요?");
                                            } else {
                                                mActivity.showFragment(OrderActivity.FRAGMENT_SUMMARY);
                                            }

                                        } else {
                                            Toast.makeText(getActivity().getApplicationContext(), jsondata.getString("message"), Toast.LENGTH_LONG).show();
                                        }
                                    } catch (UnsupportedEncodingException e1) {
                                        // TODO Auto-generated catch block
                                        e1.printStackTrace();
                                    } catch (JSONException e1) {
                                        // TODO Auto-generated catch block
                                        e1.printStackTrace();
                                    }

                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                }

            } else {
                DUtil.alertShow(getActivity(), "시간을 선택해주세요");
            }
        } else {
            DUtil.alertShow(getActivity(), "날짜를 선택해주세요");
        }
    }

    private void setDatePicker() {
        final MultiViewPager pager = (MultiViewPager) mView.findViewById(R.id.pager);


        final FragmentStatePagerAdapter adapter = new FragmentStatePagerAdapter(getChildFragmentManager()) {
            //ArrayList<DatePickFragment> container = new ArrayList<DatePickFragment>();

            @Override
            public int getCount() {
                return finalDatePickerContainer.size();
            }

            @Override
            public DatePickFragment getItem(int position) {
                return DatePickFragment.create(finalDatePickerContainer.get(position).get(Calendar.DAY_OF_MONTH));
            }

        };
        pager.setAdapter(adapter);
        pager.setCurrentItem(selectedCurrentPositionDate);
        pager.setOnPageChangeListener(new MultiViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.i("Date onPageSelected", "selected days : " + finalDatePickerContainer.get(position).get(Calendar.DAY_OF_MONTH));

                int day_of_week = finalDatePickerContainer.get(position).get(Calendar.DAY_OF_WEEK);
                setYoilText(day_of_week);

                selectedYear = finalDatePickerContainer.get(position).get(Calendar.YEAR);
                selectedMonth = finalDatePickerContainer.get(position).get(Calendar.MONTH);
                selectedDay = finalDatePickerContainer.get(position).get(Calendar.DAY_OF_MONTH);

                initTimeData(finalDatePickerContainer.get(position).getTime());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setYoilText(int value) {
        String m_week = "";

        if (value == 1)
            m_week = "일요일";
        else if (value == 2)
            m_week = "월요일";
        else if (value == 3)
            m_week = "화요일";
        else if (value == 4)
            m_week = "수요일";
        else if (value == 5)
            m_week = "목요일";
        else if (value == 6)
            m_week = "금요일";
        else if (value == 7)
            m_week = "토요일";

        aq.id(R.id.yoil_text).text(m_week).typeface(CommonUtility.getNanumBarunTypeface());
    }

    ArrayList<Calendar> datePickerContainer;
    ArrayList<Calendar> finalDatePickerContainer;

    private void initDateData(){
        Calendar base = Calendar.getInstance();
        base.setTimeInMillis(today.getTimeInMillis());
        int washingDays = mActivity.washingDays;
        int addDays = getAddDays();
        // 배달일 이전에 휴무일 계산
        washingDays += addDays;

        if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
            base.add(Calendar.DATE, washingDays);
            base.add(Calendar.HOUR_OF_DAY, mActivity.washingTimes);
        }else if(mActivity.pickupMode == PickupType.PICKUPTYPE_TOMORROW){
            base.add(Calendar.DATE, washingDays);
            base.add(Calendar.HOUR_OF_DAY, mActivity.washingTimes);
        }else{
            base.add(Calendar.DATE, washingDays);
        }

        Calendar current = Calendar.getInstance();
        if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
            current.setTimeInMillis(mActivity.pickupReqTime);
        }else if(mActivity.pickupMode == PickupType.PICKUPTYPE_TOMORROW){
            current.setTimeInMillis(mActivity.pickupReqTime);
        }else{
            current.setTimeInMillis((new Date()).getTime());
        }

        int pickupValue = base.get(Calendar.YEAR)*10000 + (base.get(Calendar.MONTH)+1) * 100 + base.get(Calendar.DATE);
        int currentValue = current.get(Calendar.YEAR)*10000 + (current.get(Calendar.MONTH)+1) * 100 + current.get(Calendar.DATE);
        Log.e("today compare", pickupValue + "::" + currentValue);
        boolean isAvailablePickup = false;
        isAvailablePickup = checkTodayAvailable();

        datePickerContainer = new ArrayList<Calendar>();

        // 당일세탁의 경우 배송일은 1일만 보여줘도 됨
        if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY || mActivity.pickupMode == PickupType.PICKUPTYPE_TOMORROW){
            Calendar dr = Calendar.getInstance();
            dr.setTimeInMillis(today.getTimeInMillis());
            dr.add(Calendar.DATE, washingDays + 0);
            datePickerContainer.add(dr);
        }else{
            for (int d = 0; d < DATE_RANGE; d++) {
                Calendar dr = Calendar.getInstance();
                dr.setTimeInMillis(today.getTimeInMillis());
                dr.add(Calendar.DATE, washingDays + d);

            /*if(pickupValue == currentValue){
                // 오늘일때 주문가능여부 체크
                if(isAvailablePickup){
                    dr.add(Calendar.DATE, washingDays + d);
                }else{
                    dr.add(Calendar.DATE, washingDays + (d+1));
                }
            }else{
                dr.add(Calendar.DATE, washingDays + (d+1));
            }*/

                datePickerContainer.add(dr);
            }
        }


        removeHoliday();
        // 현재선택값 요일값 세팅
        setYoilText(finalDatePickerContainer.get(0).get(Calendar.DAY_OF_WEEK));
        initTimeData(finalDatePickerContainer.get(0).getTime());
        setDatePicker();
    }

    private boolean checkTodayAvailable(){

        boolean isAvailable = false;
        try {

            JSONObject storeInfo = mActivity.storeInfo;
            Calendar current = Calendar.getInstance();
            String deliveryInfoX;
            String deliveryDurationX;
            int saleStartTimeX;
            String deliveryTimeX;
            int size = 0;

            if(TimeUtil.getWeekDayFromDate(current.getTime()) == 5){
                // 토요일
                deliveryInfoX = storeInfo.getString("storeDeliveryTimeSat");
            }else if(TimeUtil.getWeekDayFromDate(current.getTime()) == 6){
                // 일요일
                deliveryInfoX = storeInfo.getString("storeDeliveryTimeSun");
            }else{
                deliveryInfoX = storeInfo.getString("storeDeliveryTime");
            }
            deliveryDurationX = deliveryInfoX.split("\\|")[0];
            saleStartTimeX = 6 + Integer.parseInt(deliveryInfoX.split("\\|")[1]);
            deliveryTimeX = deliveryInfoX.split("\\|")[2];

            String avPos = (Integer.parseInt(deliveryDurationX)*2-1)+"";
            for (int i = 0; i < deliveryTimeX.length(); i++) {
                if (avPos.equals(String.valueOf(deliveryTimeX.charAt(i)))) {
                    size++;
                }
            }
            Log.i("Today Timetable", "Delivery size : " + size);

            int[] timetable = new int[size];
            int timeCount = 0;

            for (int i = 0; i < deliveryTimeX.length(); i++) {

                if (avPos.equals(String.valueOf(deliveryTimeX.charAt(i)))) {
                    timetable[timeCount] = i*Integer.parseInt(deliveryDurationX) + saleStartTimeX;//i+6;
                    Log.i("Today Timetable", size + " // " + timetable[timeCount] + "");
                    timeCount++;
                }
            }

            int todayLimit = current.get(Calendar.HOUR_OF_DAY)*60 + current.get(Calendar.MINUTE);
            int gTodayLimit;
            Calendar pickReq = Calendar.getInstance(Locale.KOREA);
            pickReq.setTimeInMillis(mActivity.pickupReqTime);

            if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
                gTodayLimit = (pickReq.get(Calendar.HOUR_OF_DAY) + mActivity.washingTimes)*60;
            }else if(mActivity.pickupMode == PickupType.PICKUPTYPE_TOMORROW){
                gTodayLimit = (pickReq.get(Calendar.HOUR_OF_DAY) + mActivity.washingTimes)*60;
            }else{
                gTodayLimit = todayLimit+30;
            }

            for (int i = 0; i < size; i++) {
                int startHour = timetable[i]*60;
                Log.e("today limit", todayLimit + "::" + startHour);
                if(gTodayLimit < startHour){
                    // 수거배송 선택가능 시간대 : 현재기준 30분후
                    //isAvailable = true;
                    return true;
                }
            }

        }catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void initPreTimeData(){
        initTimeData(null);
    }

    String deliveryInfo;
    String deliveryDuration;
    String deliveryTime;
    int saleStartTime;

    private void initTimeData(Date _selectedDate) {
        Date selectedDate = _selectedDate;
        JSONObject storeInfo = mActivity.storeInfo;
        int size = 0;
        int todayLimit = 0;
        boolean isToday = false;

        try {
            if(selectedDate == null){
                Calendar base = Calendar.getInstance(Locale.KOREA);
                base.setTimeInMillis(today.getTimeInMillis());

                int washingDays = mActivity.washingDays;
                int addDays = getAddDays();
                // 배달일 이전에 휴무일 계산
                washingDays += addDays;

                if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
                    base.add(Calendar.DATE, washingDays);
                    base.add(Calendar.HOUR_OF_DAY, mActivity.washingTimes);
                }else if(mActivity.pickupMode == PickupType.PICKUPTYPE_TOMORROW){
                    base.add(Calendar.DATE, washingDays);
                    base.add(Calendar.HOUR_OF_DAY, mActivity.washingTimes);
                }else{
                    base.add(Calendar.DATE, washingDays);
                }

                selectedDate = base.getTime();
            }

            Calendar selectedCal = Calendar.getInstance(Locale.KOREA);
            selectedCal.setTime(selectedDate);
            Calendar current = Calendar.getInstance(Locale.KOREA);

            if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
                current.setTimeInMillis(mActivity.pickupReqTime);
            }else if(mActivity.pickupMode == PickupType.PICKUPTYPE_TOMORROW){
                current.setTimeInMillis(mActivity.pickupReqTime);
            }else{
                current.setTimeInMillis((new Date()).getTime());
            }

            int pickupValue = selectedCal.get(Calendar.YEAR)*10000 + selectedCal.get(Calendar.MONTH) * 100 + selectedCal.get(Calendar.DATE);
            int currentValue = current.get(Calendar.YEAR)*10000 + current.get(Calendar.MONTH) * 100 + current.get(Calendar.DATE);
            if(pickupValue == currentValue){
                isToday = true;
                todayLimit = current.get(Calendar.HOUR_OF_DAY)*60 + current.get(Calendar.MINUTE);
                Log.e("today", "TODAY is true / " + todayLimit);
            }

            if(TimeUtil.getWeekDayFromDate(selectedDate) == 5){
                // 토요일
                deliveryInfo = storeInfo.getString("storeDeliveryTimeSat");
            }else if(TimeUtil.getWeekDayFromDate(selectedDate) == 6){
                // 일요일
                deliveryInfo = storeInfo.getString("storeDeliveryTimeSun");
            }else{
                deliveryInfo = storeInfo.getString("storeDeliveryTime");
            }

            deliveryDuration = deliveryInfo.split("\\|")[0];
            saleStartTime = 6 + Integer.parseInt(deliveryInfo.split("\\|")[1]);
            deliveryTime = deliveryInfo.split("\\|")[2];
            //String deliveryTime = storeInfo.getString("storeDeliveryTime");
            Log.i("deliveryTime : ", deliveryTime);

            int gTodayLimit;
            Calendar pickReq = Calendar.getInstance(Locale.KOREA);
            pickReq.setTimeInMillis(selectedDate.getTime());

            if(mActivity.pickupMode == PickupType.PICKUPTYPE_TODAY){
                gTodayLimit = (pickReq.get(Calendar.HOUR_OF_DAY) + mActivity.washingTimes)*60;
            }else if(mActivity.pickupMode == PickupType.PICKUPTYPE_TOMORROW){
                int lmt = 14;
                if(pickReq.get(Calendar.HOUR_OF_DAY) >= 14){
                    lmt = 12;
                }else{
                    lmt = 0;
                }
                gTodayLimit = (lmt)*60;
            }else{
                gTodayLimit = todayLimit+30;
            }

            String avPos = (Integer.parseInt(deliveryDuration)*2-1)+"";
            for (int i = 0; i < deliveryTime.length(); i++) {
                if (gTodayLimit < (i * Integer.parseInt(deliveryDuration) + saleStartTime) * 60) {
                    if (avPos.equals(String.valueOf(deliveryTime.charAt(i)))) {
                        size++;
                    }
                }
            }
            Log.i("Delivery Timetable", "Delivery size : " + size);

            timetable = new int[size];
            int timeCount = 0;
            for (int i = 0; i < deliveryTime.length(); i++) {
                if (avPos.equals(String.valueOf(deliveryTime.charAt(i)))) {
                    if(gTodayLimit < (i*Integer.parseInt(deliveryDuration) + saleStartTime)*60){
                        timetable[timeCount] = i*Integer.parseInt(deliveryDuration) + saleStartTime;//i+6;
                        //timetable[timeCount] = i + 6;
                        Log.i("Delivery Timetable", size + " // " + timetable[timeCount] + "");
                        timeCount++;
                    }
                }
            }

            //06:00 ~ 23:00
            //{"06:00 ~ 07:00 오전", "07:00 ~ 08:00 오전", "08:00 ~ 09:00 오전", "09:00 ~ 10:00 오전", "16:00 ~ 17:00 오전", "06:00 ~ 07:00 오전", "06:00 ~ 07:00 오전", "06:00 ~ 07:00 오전"};
            TIME_TABLE = new ArrayList<String>();


            for (int i = 0; i < size; i++) {
                int startHour = timetable[i];
                //String s1 = (startHour%12>10)?startHour%12+"":"0"+startHour%12;
                //String s2 = (startHour%12+1>10)?startHour%12+1+"":"0"+startHour%12+1;
                int posHour = startHour;//((startHour-6)*Integer.parseInt(deliveryDuration)+6);
                int calHour = (posHour % 12 == 0) ? 12 : posHour % 12;

                String apm = "";
                if (posHour > 12 && posHour != 24) {
                    apm = "오후";
                } else if (posHour == 24) {
                    apm = "밤";
                } else if (posHour == 12) {
                    apm = "낮";
                } else {
                    apm = "오전";
                }

                // 당일세탁 배송가능시간 계산부분
                if(isToday){
                    if(gTodayLimit < startHour*60){
                        Log.e("init today limit", todayLimit + "::" + startHour*60);
                        // 수거배송 선택가능 시간대 : 현재기준 30분후
                        TIME_TABLE.add(" " + apm + " " + calHour + "시~" + (calHour + Integer.parseInt(deliveryDuration)) + "시");
                    }
                }else{
                    TIME_TABLE.add(" " + apm + " " + calHour + "시~" + (calHour + Integer.parseInt(deliveryDuration)) + "시");
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(_selectedDate != null){
            savedRestore();
        }

    }

    private void savedRestore(){
        if(timetable.length == 0){
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(mActivity.deliveryReqTime);
            selectedYear = c.get(Calendar.YEAR);
            selectedMonth = c.get(Calendar.MONTH);
            selectedDay = c.get(Calendar.DATE);
            selectedHour = c.get(Calendar.HOUR_OF_DAY);
            for (int i = 0; i < finalDatePickerContainer.size(); i++) {
                if (finalDatePickerContainer.get(i).get(Calendar.DATE) == c.get(Calendar.DATE)) {
                    selectedCurrentPositionDate = i;
                }
            }
            for (int i = 0; i < timetable.length; i++) {
                //int posHour = ((c.get(Calendar.HOUR)-6)*Integer.parseInt(deliveryDuration)+6);
                if (timetable[i] == c.get(Calendar.HOUR_OF_DAY)) {
                    selectedCurrentPositionHour = i + 1;
                }
            }

            aq.id(R.id.text_message).text(mActivity.orderRequestString).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.input_box).gone();
        }else{
            if(selectedYear == -1){
                selectedYear = finalDatePickerContainer.get(0).get(Calendar.YEAR);
                selectedMonth = finalDatePickerContainer.get(0).get(Calendar.MONTH);
                selectedDay = finalDatePickerContainer.get(0).get(Calendar.DAY_OF_MONTH);
                selectedHour = timetable[0];
            }
        }

        setTimePicker();
    }

    private int getAddDays(){
        int added = 0;

        String storeClosingDay = mActivity.storeClosingDay;
        String storeDayoff = mActivity.storeDayoff;

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(mActivity.pickupReqTime);
        int addSelectedYear = c.get(Calendar.YEAR);
        int addSelectedMonth = c.get(Calendar.MONTH);
        int addSelectedDay = c.get(Calendar.DATE);

        String[] splitClose = storeClosingDay.split("\\|");
        String[] splitDayOff = storeDayoff.split(",");
        if(StringUtil.isNullOrEmpty(storeDayoff)){
            splitDayOff = new String[0];
        }

        int wasdays = mActivity.washingDays;
        Log.e("getAddDays", added + " // " + mActivity.washingDays);

        for(int i = 0 ; i < wasdays; i++){
            // 정기휴무일 처리
            Calendar selDay = Calendar.getInstance();
            selDay.set(addSelectedYear, addSelectedMonth, addSelectedDay);
            selDay.add(Calendar.DATE, (i+1));

            if(selDay.get(Calendar.DAY_OF_WEEK) == 1){
                // 일요일인경우
                wasdays++;
                added += 1;
                Log.e("selDay : ", selDay.get(Calendar.YEAR) + "-" + (selDay.get(Calendar.MONTH)+1) + "-" + selDay.get(Calendar.DATE));
                Log.e("getAddDays sunday", added + "");
            }else{
                // 임시휴무일 처리
                for(int m = 0; m < splitDayOff.length; m++){
                    String dateStr = splitDayOff[m];
                    String[] dateSplit = dateStr.split("-");
                    int ySplit = Integer.parseInt(dateSplit[0]);
                    int mSplit = Integer.parseInt(dateSplit[1]);
                    int dSplit = Integer.parseInt(dateSplit[2]);

                    Log.i("=========================", "=========================");
                    Log.i("tempHoliday : ", ySplit + "-" + mSplit + "-" + dSplit);

                    if(ySplit == selDay.get(Calendar.YEAR) && mSplit == (selDay.get(Calendar.MONTH)+1) && dSplit == selDay.get(Calendar.DATE)){
                        added += 1;
                        wasdays++;
                        Log.e("selDay : ", selDay.get(Calendar.YEAR) + "-" + (selDay.get(Calendar.MONTH)+1) + "-" + selDay.get(Calendar.DATE));
                        Log.e("getAddDays holiday", added + "");
                    }
                }
            }

        }

        return added;
    }

    private void removeHoliday(){

        String storeClosingDay = mActivity.storeClosingDay;
        String storeDayoff = mActivity.storeDayoff;

        String[] splitClose = storeClosingDay.split("\\|");
        String[] splitDayOff = storeDayoff.split(",");
        if(StringUtil.isNullOrEmpty(storeDayoff)){
            splitDayOff = new String[0];
        }

        String gweek = splitClose[0];
        String holidayWeekValue = splitClose[1];

        finalDatePickerContainer = new ArrayList<Calendar>();

        Log.i("gweek", gweek + "");
        for(int i = 0 ; i < datePickerContainer.size(); i++){
            boolean isHoliday = false;
            // 정기휴무일 처리
            if("every".equals(gweek)) {
                for (int k = 0; k < holidayWeekValue.length(); k++) {
                    if ("1".equals(String.valueOf(holidayWeekValue.charAt(k)))) {
                        if(TimeUtil.convertWeekValueToInt(datePickerContainer.get(i).get(Calendar.DAY_OF_WEEK)) == k){
                            // 휴무일 제거
                            Log.i("holidayWeekValue", "Remove holidayWeekValue : " + datePickerContainer.get(i).get(Calendar.DAY_OF_WEEK));
                            isHoliday = true;
                        }
                    }
                }
            }

            // 임시휴무일 처리
            for(int m = 0; m < splitDayOff.length; m++){
                String dateStr = splitDayOff[m];
                String[] dateSplit = dateStr.split("-");
                int ySplit = Integer.parseInt(dateSplit[0]);
                int mSplit = Integer.parseInt(dateSplit[1]);
                int dSplit = Integer.parseInt(dateSplit[2]);
                Log.i("tempHoliday : ", ySplit + "-" + mSplit + "-" + dSplit);

                if(ySplit == datePickerContainer.get(i).get(Calendar.YEAR) && mSplit == (datePickerContainer.get(i).get(Calendar.MONTH)+1) && dSplit == datePickerContainer.get(i).get(Calendar.DAY_OF_MONTH)){
                    isHoliday = true;
                }
            }

            if(!isHoliday){
                if(finalDatePickerContainer.size() < 10){
                    finalDatePickerContainer.add(datePickerContainer.get(i));
                }else{
                    Log.i("Date Array Count","Date Element Count rebalanced to 10");
                }
            }
        }

    }

    int selectedCurrentPositionDate = 0;
    int selectedCurrentPositionHour = 1;
    int wheelSelectedIndex = 0;

    private void setTimePicker() {

        if(wva == null){

        }else{
            wva = new WheelView(getActivity());
        }
        wva = (WheelView) mView.findViewById(R.id.wheel_view_wv);
        //wva.invalidate();
        wva.setItems(TIME_TABLE);
        //wva.setOffset(selectedCurrentPositionHour);
        wva.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(int selectedIndex, String item) {
                // selectedIndex 1부터 시작
                if(selectedIndex > timetable.length){
                    selectedIndex = timetable.length;
                }
                wheelSelectedIndex = selectedIndex;
                Log.i(TAG, "selectedIndex: " + timetable[selectedIndex - 1] + "시, item: " + item);
                selectedHour = timetable[selectedIndex - 1];
            }
        });

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }
}
