package me.rewhite.users.fragment;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.gsretail.GS25V2OrderInputClientActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class GS25V2OrderInputStep4Fragment extends Fragment implements NumberPicker.OnValueChangeListener, IFragment{

    private static final String TAG = "GS25V2OrderInputStep4Fragment";
    int count_pack = 0;
    View rootView;
    AQuery aq;
    Context mContext;

    GS25V2OrderInputClientActivity ra;

    public GS25V2OrderInputStep4Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_gs25_v2_order_input_04, container, false);
        aq = new AQuery(rootView);

        ra = (GS25V2OrderInputClientActivity)getActivity();

        aq.id(R.id.text_unit_count).text("고객님의 세탁물 수량은 "+ra.count_unit+"개 입니다.");

        aq.id(R.id.btn_01).clicked(this, "unitCountModClicked");
        aq.id(R.id.btn_02).clicked(this, "acceptUnitCount");
        aq.id(R.id.btn_03).clicked(this, "countClicked").tag(1);
        aq.id(R.id.btn_04).clicked(this, "countClicked").tag(2);
        aq.id(R.id.btn_05).clicked(this, "countClicked").tag(3);
        aq.id(R.id.btn_06).clicked(this, "countClicked").tag(4);

        aq.id(R.id.btn_submit).clicked(this, "nextProcessClicked");

        return rootView;
    }

    public void acceptUnitCount(View button){
        aq.id(R.id.area_02).visible();
        aq.id(R.id.btn_02).background(R.drawable.img_gs25_client_button_selected).textColor(0xffffffff);
    }

    int selectedIndex = 0;
    public void countClicked(View button){
        int tag = (int)button.getTag();
        count_pack = tag;
        ra.count_pack = count_pack;
        selectedIndex = tag;
        allClearButtonStatus();

        aq.id(R.id.btn_submit).text(count_pack + "개의 수거봉투를 접수합니다.");

        if(count_pack>0){
            aq.id(R.id.btn_submit).backgroundColor(0xff252525).textColor(0xffffffff);
        }

        switch (tag){
            case 1:
                aq.id(R.id.btn_03).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                break;
            case 2:
                aq.id(R.id.btn_04).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                break;
            case 3:
                aq.id(R.id.btn_05).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                break;
            case 4:
                aq.id(R.id.btn_06).background(R.drawable.img_gs25_count_button_selected).textColor(0xffffffff);
                break;
        }
    }

    private void allClearButtonStatus(){

        aq.id(R.id.btn_03).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);
        aq.id(R.id.btn_04).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);
        aq.id(R.id.btn_05).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);
        aq.id(R.id.btn_06).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);

        aq.id(R.id.unit_count_layout).background(R.drawable.img_gs25_count_button_normal).textColor(0xff909090);
    }

    public void unitCountModClicked(View button){
        unitNumberPickerShow();
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

        Log.i("value is",""+newVal);

    }

    private void submitButtonInitialize(){
        if(count_pack > 0){
            aq.id(R.id.btn_submit).image(R.mipmap.btn_gs_order_start).enabled(true);
        }else{
            aq.id(R.id.btn_submit).image(R.mipmap.btn_gs_order_input).enabled(false);
        }
    }

    private void unitNumberPickerShow(){
        final Dialog d = new Dialog(getActivity());
        //d.setTitle("맡기실 세탁물 수량을 입력해주세요.");
        d.setContentView(R.layout.dialog_unit_picker);
        Button b1 = (Button) d.findViewById(R.id.btn_unit_cancel);
        Button b2 = (Button) d.findViewById(R.id.btn_unit_ok);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.unitPicker);
        np.setMaxValue(30);
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        b2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                //tv.setText(String.valueOf(np.getValue()));
                Log.e("unitPick", String.valueOf(np.getValue()));
                ra.count_unit = np.getValue();

                aq.id(R.id.text_unit_count).text("고객님의 세탁물 수량은 "+ra.count_unit+"개 입니다.");
                aq.id(R.id.btn_02).background(R.drawable.img_gs25_client_button_normal).textColor(0xff909090);
                aq.id(R.id.area_02).gone();

                d.dismiss();
            }
        });
        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    private void packNumberPickerShow(){
        final Dialog d = new Dialog(getActivity());
        //d.setTitle("포장한 수거봉투 수량을 입력해주세요.");
        d.setContentView(R.layout.dialog_pack_picker);
        Button b1 = (Button) d.findViewById(R.id.btn_pack_cancel);
        Button b2 = (Button) d.findViewById(R.id.btn_pack_ok);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.packPicker);
        np.setMaxValue(3);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        b2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                //tv.setText(String.valueOf(np.getValue()));
                Log.e("unitPick", String.valueOf(np.getValue()));
                count_pack = np.getValue();
                if(count_pack > 0){
                    aq.id(R.id.text_pack_count).text(String.valueOf(count_pack) + " 개").textSize(18.f);
                }else{
                    aq.id(R.id.text_pack_count).text("수량을 정확하게 입력해주세요").textSize(13.f);
                }

                aq.id(R.id.btn_02).background(R.drawable.img_gs25_client_button_normal).textColor(0xff909090);
                aq.id(R.id.area_02).gone();
                //submitButtonInitialize();
                d.dismiss();
            }
        });
        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    public void nextProcessClicked(View button){

        if(ra.partnerId != null && ra.partnerStoreId != null && ra.count_unit > 0 && ra.count_pack > 0 && ra.request_content_string != null){

        }else{
            DUtil.alertShow(getActivity(), "수량을 다시 확인해주세요.");
            return;
        }

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("partnerId", ra.partnerId);
        params.put("partnerStoreId", ra.partnerStoreId);
        params.put("pickupQuantity", ra.count_unit);
        params.put("packQuantity", count_pack);
        params.put("pickupRequestMessage", ra.request_content_string);

        params.put("k", 1);

        Log.e("nextProcessClicked", params.toString());

        NetworkClient.post(Constants.GS_ORDER_CREATE, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GS_ORDER_CREATE, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GS_ORDER_CREATE, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        try{
                            Tracker t = ((Bootstrap) getActivity().getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                            t.send(new HitBuilders.EventBuilder()
                                    .setCategory("GS주문")
                                    .setAction("주문완료")
                                    //.setLabel("Order")
                                    .build());
                        }catch (NullPointerException e){
                            e.printStackTrace();
                        }

                        if(getActivity() instanceof GS25V2OrderInputClientActivity){
                            final GS25V2OrderInputClientActivity ra = (GS25V2OrderInputClientActivity)getActivity();
                            ra.finalOrderId = jsondata.getString("data");
                            ra.showFragment(GS25V2OrderInputClientActivity.FRAGMENT_STEP5);
                        }

                    } else {
                        Toast.makeText(mContext, jsondata.getString("message"), Toast.LENGTH_LONG).show();
                        //DUtil.alertShow(GSOrderNoticeScreen.this, "이용가능한 GS25에서 QR코드를 촬영해주세요.");
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }
}
