package me.rewhite.users.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import me.rewhite.users.R;
import me.rewhite.users.util.CommonUtility;

public class CustomDialogFragment extends DialogFragment {
    public static final int TYPE_ERR_DELETE_DEFAULT_FOLDER = 1;
    public static final int TYPE_ERR_FOLDER_COUNT_LIMIT = 2;
    public static final int TYPE_DELETE_MEMO = 3;
    public static final int TYPE_DELETE_FOLDER = 5;
    public static final int TYPE_DELETE_ACCOUNT = 6;
    public static final int TYPE_LOGOUT = 7;
    public static final int TYPE_RESET_TIME_MEMO = 8;
    public static final int TYPE_DELETE_TIME_MEMO = 9;
    public static final int TYPE_UPDATE_APP = 10;
    public static final int TYPE_CHECK_SERVER = 11;
    public static final int TYPE_ERR_NETWORK = 12;

    public CustomDialogListener mListener;

    private int mMode = -1;
    private String mParam;
    private static final String TAG = "CustomDialogFragment";

    private static final String KEY_MODE = "mode";
    private static final String KEY_PARAM = "param";

    private View mView;

    public static CustomDialogFragment newInstance(int mode, String param) {
        CustomDialogFragment f = new CustomDialogFragment();

        Bundle args = new Bundle();
        args.putInt(KEY_MODE, mode);
        args.putString(KEY_PARAM, param);
        f.setArguments(args);

        return f;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (CustomDialogListener) activity;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mMode = args.getInt(KEY_MODE);
        mParam = args.getString(KEY_PARAM);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "[onCreateView]");
        Dialog dialog = getDialog();
        dialog.setCanceledOnTouchOutside(false);

        Window window = dialog.getWindow();
        window.requestFeature(Window.FEATURE_NO_TITLE);
        window.setBackgroundDrawable(new ColorDrawable(getResources().getColor(
                R.color.dialog_bg)));

        // View
        mView = inflater.inflate(R.layout.dialog_fragment_custom, container);

        setView();

        return mView;
    }

    private void setView() {
        String title = "";
        String message = "";
        boolean isTwoBtn = false;
        String btnPositiveName = null;
        String btnNegativeName = null;

        switch (mMode) {

            case TYPE_LOGOUT:
                isTwoBtn = true;
                message = getString(R.string.dialog_setting_logout);
                btnPositiveName = getString(R.string.dialog_btn_cancel);
                btnNegativeName = getString(R.string.dialog_btn_ok);
                break;

            case TYPE_UPDATE_APP:
                isTwoBtn = true;
                message = getString(R.string.dialog_update_app_msg);
                btnPositiveName = getString(R.string.dialog_btn_app_exit);
                btnNegativeName = getString(R.string.dialog_btn_update);
                break;

            case TYPE_ERR_NETWORK:
                isTwoBtn = false;
                message = getString(R.string.dialog_err_network_msg);
                btnNegativeName = getString(R.string.dialog_btn_ok);
                break;
        }

        LinearLayout layout = (LinearLayout) mView
                .findViewById(R.id.custom_dialog_btn_layout);

        TextView titleView = (TextView) mView
                .findViewById(R.id.custom_dialog_title_textview);
        titleView.setPaintFlags(titleView.getPaintFlags()
                | Paint.FAKE_BOLD_TEXT_FLAG);
        titleView.setTypeface(CommonUtility.getTypeface());

        TextView msgView = (TextView) mView
                .findViewById(R.id.custom_dialog_textview);
        msgView.setTypeface(CommonUtility.getTypeface());
        msgView.setText(message);

        if (title.equals("")) {
            titleView.setVisibility(View.GONE);
        } else {
            titleView.setVisibility(View.VISIBLE);
            titleView.setText(title);
        }

        if (isTwoBtn) {
            for (int i = 0; i < 2; i++) {
                TextView textView = new TextView(getActivity());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
                params.weight = (float) 0.5;
                textView.setLayoutParams(params);
                textView.setGravity(Gravity.CENTER);
                //textView.setTextAppearance(getActivity(),						R.style.DialogButtonText);
                textView.setBackgroundDrawable(getResources().getDrawable(
                        R.drawable.bg_dialog_button));
                textView.setTypeface(CommonUtility.getTypeface());
                textView.setText((i == 0) ? btnPositiveName : btnNegativeName);
                textView.setOnClickListener((i == 0) ? mClickPositiveListener
                        : mClickNegativeListener);
                layout.addView(textView);
            }
        } else {
            TextView textView = new TextView(getActivity());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            textView.setLayoutParams(params);
            textView.setGravity(Gravity.CENTER);
            //textView.setTextAppearance(getActivity(), R.style.DialogButtonText);
            textView.setBackgroundDrawable(getResources().getDrawable(
                    R.drawable.bg_dialog_button));
            textView.setTypeface(CommonUtility.getTypeface());
            textView.setText(btnNegativeName);
            textView.setOnClickListener(mClickNegativeListener);
            layout.addView(textView);
        }
    }

    private View.OnClickListener mClickPositiveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mMode == TYPE_UPDATE_APP) {
                if (mListener != null)
                    mListener.onDialogPositiveClick(CustomDialogFragment.this,
                            mMode, mParam);
            } else {
            }

            dismiss();
        }
    };

    private View.OnClickListener mClickNegativeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            // if(mMode == TYPE_DELETE_MEMO || mMode ==
            // TYPE_ERR_DELETE_DEFAULT_FOLDER || mMode ==
            // TYPE_ERR_FOLDER_COUNT_LIMIT) {
            if (mListener != null)
                mListener.onDialogNegativeClick(CustomDialogFragment.this,
                        mMode, mParam);
            // } else {
            // }

            dismiss();
        }
    };

    public interface CustomDialogListener {
        void onDialogPositiveClick(DialogFragment dialog, int mode,
                                   String param);

        void onDialogNegativeClick(DialogFragment dialog, int mode,
                                   String param);
    }
}
