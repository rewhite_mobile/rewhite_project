package me.rewhite.users.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.androidquery.AQuery;

import java.util.regex.Pattern;

import me.rewhite.users.R;
import me.rewhite.users.activity.ProfileNewActivityV2;


public class ProfileNewSharecodeFragment extends ProfileNewBaseFragment {

    AQuery aq;
    EditText e1;
    ProfileNewActivityV2 parent;

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable edit) {
            String s1 = e1.getText().toString();
            if (s1.length() >= 1) {
                aq.id(R.id.btn_submit_input).backgroundColor(0xff00b4ff);
                aq.id(R.id.btn_submit_input).textColor(0xffffffff);
            } else {
                aq.id(R.id.btn_submit_input).backgroundColor(0xffcdd4da);
                aq.id(R.id.btn_submit_input).textColor(0xff303030);
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    /** 이모티콘이 있을경우 "" 리턴, 그렇지 않을 경우 null 리턴 **/
    InputFilter specialCharacterFilter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                // 이모티콘 패턴
                Pattern unicodeOutliers = Pattern.compile("[\\uD83C-\\uDBFF\\uDC00-\\uDFFF]+");
                // '-' 입력 받고 싶을 경우 : unicodeOutliers.matcher(source).matches() && !source.toString().matches(".*-.*")
                if(unicodeOutliers.matcher(source).matches()) {
                    return "";
                }
            }
            return null;
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(context).inflate(R.layout.fragment_profile_new_sharecode, null);
        parent = (ProfileNewActivityV2)getActivity();
        aq = new AQuery(view);

        aq.id(R.id.btn_submit).clicked(this, "shareCodeSubmitAction");
        aq.id(R.id.btn_submit_input).clicked(this, "shareCodeSubmitAction");

        e1 = (EditText) view.findViewById(R.id.input_code);

        // 이모티콘 필터링
        e1.setFilters(new InputFilter[]{specialCharacterFilter});
        if (e1 != null) {
            e1.addTextChangedListener(textWatcher);
            String s1 = e1.getText().toString();
            if (s1.length() >= 1) {
                aq.id(R.id.btn_submit_input).backgroundColor(0xff00b4ff);
                aq.id(R.id.btn_submit_input).textColor(0xffffffff);
            } else {
                aq.id(R.id.btn_submit_input).backgroundColor(0xffcdd4da);
                aq.id(R.id.btn_submit_input).textColor(0xff303030);
            }
        }

        return view;
    }

    public void shareCodeSubmitAction(View button){
        parent.inputedSharecode = e1.getText().toString();
        parent.processNext();
    }

    @Override
    public void initialize() {


    }

}
