package me.rewhite.users.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;

import me.rewhite.users.R;
import me.rewhite.users.activity.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingFragment extends Fragment implements IFragment {

    private static final String TAG = "MainFragment";
    AQuery aq;
    private MainActivity mMainActivity = null;
    private Context ctx;
    private String currentLocationAddress = "";

    private int mPos = -1;

    public SettingFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public SettingFragment(int pos) {
        mPos = pos;
    }

    @Override
    public void onResume() {

        Log.d(TAG, "[onResume]");
        mMainActivity.setTouchDisable(false);

        super.onResume();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mMainActivity = (MainActivity) getActivity();
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ctx = getActivity();
        aq = new AQuery(getActivity(), view);
        aq.id(R.id.top_area).clicked(this, "topmenuClicked");

        aq.id(R.id.payment_layout).clicked(this, "paymentClicked");

        return view;
    }

    public void topmenuClicked(View button) {
        mMainActivity.getSlidingMenu().toggle();
    }

    public void paymentClicked(View button) {

    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onResult(String command, String result) {

    }

    @Override
    public void onDetectorKeyboard(boolean isShow) {

    }

    @Override
    public void dismiss() {

    }


}
