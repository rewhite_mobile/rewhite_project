package me.rewhite.users.fragment;

import android.net.Uri;

public interface IFragment {

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /**
     * onBackPressed시 호출
     *
     * @return return true if was consumed.
     */
    boolean onBackPressed();

    /**
     * 해당 fragment에 커맨드와 결과 값 전달
     *
     * @param command
     * @param result
     */
    void onResult(String command, String result);

    /**
     * 키보드 show/hide 상태 전달
     *
     * @param isShow show/hide 여부
     */
    void onDetectorKeyboard(boolean isShow);

    /**
     * DialogFragment의 dismiss를 위한 함수
     */
    void dismiss();
}
