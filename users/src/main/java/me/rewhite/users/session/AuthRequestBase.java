package me.rewhite.users.session;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


public class AuthRequestBase implements Parcelable {

    private String loginId;
    private String secretKey;
    private String loginType;
    private String agreeThirdOpt;
    private String deviceToken;
    private Bundle extras;

    protected AuthRequestBase(final String loginId, final String secretKey, final String loginType, final String deviceToken) {
        this.loginId = loginId;
        this.secretKey = secretKey;
        this.loginType = loginType;
        this.deviceToken = deviceToken;
    }

    protected AuthRequestBase(final String loginId, final String secretKey, final String loginType,final String isAgreeThirdOptinal, final String deviceToken) {
        this.loginId = loginId;
        this.secretKey = secretKey;
        this.loginType = loginType;
        this.agreeThirdOpt = isAgreeThirdOptinal;
        this.deviceToken = deviceToken;
    }

    protected AuthRequestBase() {
    }

    protected AuthRequestBase(Parcel in) {
        loginId = in.readString();
        secretKey = in.readString();
        loginType = in.readString();
        agreeThirdOpt = in.readString();
        deviceToken = in.readString();
        extras = in.readBundle();
    }

    public static final Creator<AuthRequestBase> CREATOR = new Creator<AuthRequestBase>() {
        @Override
        public AuthRequestBase createFromParcel(Parcel in) {
            return new AuthRequestBase(in);
        }

        @Override
        public AuthRequestBase[] newArray(int size) {
            return new AuthRequestBase[size];
        }
    };

    protected void addExtraString(final String key, final String value) {
        if (extras == null) {
            extras = new Bundle();
        }
        extras.putString(key, value);
    }

    public int getRequestCode() {
        return RewhiteSession.AUTHORIZATION_CODE_REQUEST;
    }

    public String getLoginId() {
        return loginId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String getLoginType() {
        return loginType;
    }

    public String getAgreeThirdOpt() {
        return agreeThirdOpt;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public Bundle getExtras() {
        return extras;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(loginId);
        parcel.writeString(secretKey);
        parcel.writeString(loginType);
        parcel.writeString(agreeThirdOpt);
        parcel.writeString(deviceToken);
        parcel.writeBundle(extras);
    }

    public void readFromParcel(final Parcel parcel) {
        loginId = parcel.readString();
        secretKey = parcel.readString();
        loginType = parcel.readString();
        agreeThirdOpt = parcel.readString();
        deviceToken = parcel.readString();
        extras = parcel.readBundle();
    }

}
