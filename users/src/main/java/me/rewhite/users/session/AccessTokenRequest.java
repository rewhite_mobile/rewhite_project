package me.rewhite.users.session;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;


public class AccessTokenRequest extends AuthRequestBase {

    private String keyHash;
    private String authorizationCode;
    private String deviceToken;
    private Context ctx;

    public static AccessTokenRequest createRequestWithAuthorizationCode(final Context context, final String loginId, final String secretKey, final String loginType,
                                                                        final String authorizationCode) {
        return new AccessTokenRequest(context, loginId, secretKey, loginType).setAuthorizationCode(authorizationCode);
    }

    public static AccessTokenRequest createRequestWithDeviceToken(final Context context, final String loginId, final String secretKey, final String loginType,
                                                                  final String deviceToken) {

        return new AccessTokenRequest(context, loginId, secretKey, loginType).setDeviceToken(deviceToken);
    }

    private AccessTokenRequest(final Context context, final String loginId, final String secretKey, final String loginType) {

        super(loginId, secretKey, loginType, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));
        this.ctx = context;
        this.keyHash = DUtil.getKeyHash(context);
    }

    private AccessTokenRequest(final Parcel parcel) {
        readFromParcel(parcel);
    }

    public Context getContext() {
        return ctx;
    }

    public boolean isAccessTokenRequestWithAuthCode() {
        return authorizationCode != null;
    }

    public String getKeyHash() {
        return keyHash;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    private AccessTokenRequest setAuthorizationCode(final String authorizationCode) {
        this.authorizationCode = authorizationCode;
        return this;
    }

    private AccessTokenRequest setDeviceToken(final String deviceToken) {
        this.deviceToken = deviceToken;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel, final int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(keyHash);
        parcel.writeString(authorizationCode);
        parcel.writeString(deviceToken);
    }

    public void readFromParcel(final Parcel parcel) {
        super.readFromParcel(parcel);
        keyHash = parcel.readString();
        authorizationCode = parcel.readString();
        deviceToken = parcel.readString();
    }

    public static final Parcelable.Creator<AccessTokenRequest> CREATOR = new Parcelable.Creator<AccessTokenRequest>() {

        public AccessTokenRequest createFromParcel(Parcel in) {
            return new AccessTokenRequest(in);
        }

        public AccessTokenRequest[] newArray(int size) {
            return new AccessTokenRequest[size];
        }
    };
}
