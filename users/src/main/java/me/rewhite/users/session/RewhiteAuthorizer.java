package me.rewhite.users.session;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;


public abstract class RewhiteAuthorizer {

    private transient boolean hasInternetPermission;
    protected transient OnAuthorizationListener onAuthorizationListener;

    public interface OnAuthorizationListener {

        void onAuthorizationCompletion(AuthorizationResult result);
    }

    protected boolean checkInternetPermission() {
        if (hasInternetPermission) {
            return true;
        }

        int permissionCheck = getApplicationContext().checkCallingOrSelfPermission(Manifest.permission.INTERNET);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            doneOnOAuthError("This Operation needs INTERNET permission.");
            return false;
        } else {
            hasInternetPermission = true;
            return true;
        }
    }

    public Context getApplicationContext() {
        return RewhiteSession.getCurrentSession().getContext();
    }

    protected void done(final AuthorizationResult result) {
        if (onAuthorizationListener != null) {
            onAuthorizationListener.onAuthorizationCompletion(result);
        }
    }

    protected abstract void doneOnOAuthError(final String errorMessage);

    public void setOnAuthorizationListener(OnAuthorizationListener onAuthorizationListener) {
        this.onAuthorizationListener = onAuthorizationListener;
    }
}
