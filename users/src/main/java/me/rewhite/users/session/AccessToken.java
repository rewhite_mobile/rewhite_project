package me.rewhite.users.session;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.StringUtil;


public class AccessToken implements Parcelable {

    private static final String ACCESS_TOKEN_KEY = "accessToken";
    private static final String USER_KEY = "userId";
    private static final String EMAIL_AUTH_CHECK = "isEmailConfirm";

    private static final String CACHE_ACCESS_TOKEN = "me.rewhite.users.token.AccessToken";
    private static final Date MIN_DATE = new Date(Long.MIN_VALUE);
    private static final Date MAX_DATE = new Date(Long.MAX_VALUE);
    private static final Date DEFAULT_EXPIRATION_TIME = MAX_DATE;
    private static final Date ALREADY_EXPIRED_EXPIRATION_TIME = MIN_DATE;

    private String accessTokenString;
    private String user_key;
    private String email_auth_check;

    public static AccessToken createEmptyToken() {
        DUtil.Log("AccessToken", "createEmptyToken");
        return new AccessToken("", "", "N");
    }

    public static AccessToken createFromCache() {
        DUtil.Log("AccessToken", "createFromCache");
        final String accessToken = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN);
        final String userKey = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_ID);
        final String emailAuthCheck = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL_AUTH_CHECK);
        //
        return new AccessToken(accessToken, userKey, emailAuthCheck);
    }

    public static void clearAccessTokenFromCache() {
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN, "");
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.USER_ID, "");
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.EMAIL_AUTH_CHECK, "N");
    }

    public static void saveAccessTokenToCache(final String accessTokenString, final String user_key, final String email_auth_check) {
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN, accessTokenString);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.USER_ID, user_key);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.EMAIL_AUTH_CHECK, email_auth_check);
    }

    public static AccessToken createFromResponse(final JSONObject resultObj) {
        String accessToken;
        String userKey;
        String emailAuthCheck;

        try {
            JSONObject data = resultObj.getJSONObject("data");
            accessToken = data.getString(ACCESS_TOKEN_KEY);
            userKey = data.getInt(USER_KEY) + "";
            emailAuthCheck = data.getString(EMAIL_AUTH_CHECK);

            if (accessToken == null) return null;

            saveAccessTokenToCache(accessToken, userKey, emailAuthCheck);
            // 일단 refresh token의 expires_in은 나중에
            return new AccessToken(accessToken, userKey, emailAuthCheck);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }

    private AccessToken(final String accessTokenString, final String userKey, final String emailAuthCheck) {
        this.accessTokenString = accessTokenString;
        this.user_key = userKey;
        this.email_auth_check = emailAuthCheck;

        DUtil.Log("AccessToken userKey", userKey);
    }

    // access token 갱신시에는 refresh token이 내려오지 않을 수도 있다.
    public void updateAccessToken(final AccessToken newAccessToken) {
        this.accessTokenString = newAccessToken.accessTokenString;
        this.user_key = newAccessToken.user_key;
        this.email_auth_check = newAccessToken.email_auth_check;

        saveAccessTokenToCache(this.accessTokenString, this.user_key, this.email_auth_check);
    }

    public String getAccessTokenString() {
        return this.accessTokenString;
    }

    public String getUserKey() {
        return this.user_key;
    }

    public String getEmailAuthCheck() {
        return this.email_auth_check;
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    public boolean hasToken() {
        return !StringUtil.isNullOrEmpty(this.accessTokenString);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        // TODO Auto-generated method stub
        dest.writeString(this.accessTokenString);
        dest.writeString(this.user_key);
        dest.writeString(this.email_auth_check);
    }

    public AccessToken(Parcel in) {
        this.accessTokenString = in.readString();
        this.user_key = in.readString();
        this.email_auth_check = in.readString();
    }

    public static final Parcelable.Creator<AccessToken> CREATOR = new Parcelable.Creator<AccessToken>() {

        public AccessToken createFromParcel(Parcel in) {
            return new AccessToken(in);
        }

        public AccessToken[] newArray(int size) {
            return new AccessToken[size];
        }
    };
}
