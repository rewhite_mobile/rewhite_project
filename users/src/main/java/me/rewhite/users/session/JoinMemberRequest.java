package me.rewhite.users.session;


import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;


public class JoinMemberRequest extends AuthRequestBase {

    private String keyHash;
    private String mobileNumber;
    private String authNumber;
    private String agreeThirdOpt;
    private Context ctx;

    public static JoinMemberRequest createRequest(final Context context, final String loginId, final String secretKey, final String loginType, final String isAgreeThirdOptinal) {
        DUtil.Log("JoinMemberRequest createRequest", loginId + " / " + secretKey + " / isAgreeThirdOptinal : " + isAgreeThirdOptinal);
        //String ctn = mobileNumber.replace("-", "");
        return new JoinMemberRequest(context, loginId, secretKey, loginType, isAgreeThirdOptinal);
    }

    private JoinMemberRequest(final Context context, final String loginId, final String secretKey, final String loginType, final String isAgreeThirdOptinal) {
        super(loginId, secretKey, loginType, isAgreeThirdOptinal, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));
        ctx = context;
        this.keyHash = DUtil.getKeyHash(context);
    }

    private JoinMemberRequest(final Parcel parcel) {
        readFromParcel(parcel);
    }

    public boolean isAccessTokenRequestWithAuthNumber() {
        return authNumber != null;
    }

    public Context getContext() {
        return ctx;
    }

    public String getKeyHash() {
        return keyHash;
    }

    public String getAuthNumber() {
        return authNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getAgreeThirdOpt() {
        return agreeThirdOpt;
    }

    private JoinMemberRequest setAuthNumber(final String authNumber) {
        this.authNumber = authNumber;
        return this;
    }

    private JoinMemberRequest setMobileNumber(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
        return this;
    }

    private JoinMemberRequest setAgreeThirdOpt(final String agreeThirdOpt) {
        this.agreeThirdOpt = agreeThirdOpt;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel parcel, final int i) {
        super.writeToParcel(parcel, i);
        parcel.writeString(keyHash);
        parcel.writeString(mobileNumber);
        parcel.writeString(authNumber);
        parcel.writeString(agreeThirdOpt);
    }

    public void readFromParcel(final Parcel parcel) {
        super.readFromParcel(parcel);
        keyHash = parcel.readString();
        mobileNumber = parcel.readString();
        authNumber = parcel.readString();
        agreeThirdOpt = parcel.readString();
    }

    public static final Parcelable.Creator<JoinMemberRequest> CREATOR = new Parcelable.Creator<JoinMemberRequest>() {

        public JoinMemberRequest createFromParcel(Parcel in) {
            return new JoinMemberRequest(in);
        }

        public JoinMemberRequest[] newArray(int size) {
            return new JoinMemberRequest[size];
        }
    };
}
