package me.rewhite.users.session;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.rewhite.users.common.Constants;
import me.rewhite.users.fragment.CustomDialogFragment;
import me.rewhite.users.session.RewhiteException.ErrorType;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.SharedPreferencesUtility.UserInfo;
import me.rewhite.users.util.StringUtil;


public class RewhiteSession implements RewhiteAuthorizer.OnAuthorizationListener, CustomDialogFragment.CustomDialogListener {

    private static RewhiteSession currentSession;
    private static final String REDIRECT_URL_PREFIX = "rewhite.users";
    //private static final String REDIRECT_URL_POSTFIX = "://oauth";
    private static final String COOKIE_SEPERATOR = "Rewhite;";
    private static final String COOKIE_NAME_VALUE_DELIMITER = "=";
    private static final int DEFAULT_TOKEN_REQUEST_TIME_MILLIS = 3 * 60 * 60 * 1000; // 3 hours
    private static final int RETRY_TOKEN_REQUEST_TIME_MILLIS = 5 * 60 * 1000; // 5 minutes
    private static final String EXPIRES_IN_MILLIS = "expiresInMillis";
    public static final int AUTHORIZATION_CODE_REQUEST = 1;
    public static final int ACCESS_TOKEN_REQUEST = 2;

    private final Object INSTANCE_LOCK = new Object();

    private Context context;
    private AQuery aq;
    private final List<RewhiteSessionCallback> rewhiteSessionCallbacks;
    private final Handler sessionCallbackHandler;
    // 아래 값들은 변경되는 값으로 INSTANCE_LOCK의 보호를 받는다.
    private SessionState state;
    private AccessToken accessToken;

    //private GetterAccessToken getterAccessToken;

    public static synchronized RewhiteSession getInstance(final Context context) {
        if (currentSession != null) {
            return currentSession;
        } else {
            return initialize(context);
        }
    }

    public static synchronized RewhiteSession initialize(final Context context) {
        if (currentSession != null) {
            currentSession.clearCallbacks();
            currentSession.close();
        }

        currentSession = new RewhiteSession(context);
        return currentSession;
    }

    private RewhiteSession(final Context context) {
        if (context == null)
            throw new RewhiteException(ErrorType.ILLEGAL_ARGUMENT, "cannot create Session without Context.");

        this.context = context;

        this.rewhiteSessionCallbacks = new ArrayList<RewhiteSessionCallback>();
        this.sessionCallbackHandler = new Handler(Looper.getMainLooper()); //세션 callback은 main thread에서 호출되도록 한다.

        synchronized (INSTANCE_LOCK) {
            accessToken = AccessToken.createFromCache();
            if (accessToken.hasToken()) {
                this.state = SessionState.OPENED;
            } else {
                this.state = SessionState.CLOSED;
                close();
            }

        }
    }

    /**
     * 세션 오픈을 진행한다. <br/>
     * {@link SessionState#OPENED} 상태이면 바로 종료. <br/>
     * {@link SessionState#CLOSED} 상태이면 authorization code 요청. 에러/취소시 {@link SessionState#CLOSED} <br/>
     * {@link SessionState#OPENABLE} 상태이면 code 또는 refresh token 이용하여 access token 을 받아온다. 에러/취소시
     * {@link SessionState#CLOSED}, refresh 취소시에만 {@link SessionState#OPENABLE} 유지. <br/>
     * param으로 받은 콜백으로 그 결과를 전달한다. <br/>
     *
     * @param authType       인증받을 타입.
     * @param callerActivity 세션오픈을 호출한 activity
     */
    public void open(final AccessTokenRequest _request) {
        final SessionState currentState = getState();
        //state = currentState;
        context = _request.getContext();

//        if (currentState.isOpened()) {
//            // 이미 open이 되어 있다.
//            DUtil.Log("currentState.isOpened()", "이미 open이 되어 있다");
//            notifySessionState(currentState, null);
//            return;
//        }

        if (currentState.isOpened()) {
            DUtil.Log("INSTANCE_LOCK OPENABLE", "isOpened");
            //    String accessToken = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN);
            if (!StringUtil.isNullOrEmpty(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN))) {
                // 이미 open이 되어 있다.
                DUtil.Log("currentState.isOpened()", "이미 open이 되어 있다");
                notifySessionState(currentState, null);
                return;
            } else {
                // 재로그인
                requestAccessToken(_request);
            }
        }

        try {
            synchronized (INSTANCE_LOCK) {
                switch (state) {
                    case CLOSED:
                        DUtil.Log("INSTANCE_LOCK CLOSED", "CLOSED");
                        requestAccessToken(_request);
                        break;
                    case OPENABLE:
                        DUtil.Log("INSTANCE_LOCK OPENABLE", "OPENABLE");
                        requestAccessToken(_request);
                        /*
                        if (!accessToken.hasToken()) {
                            DUtil.Log("INSTANCE_LOCK OPENABLE", "requestAccessToken");
                            requestAccessToken(_request);
                        } else {
                            //accessToken.createFromResponse(json);
                            final SessionState previous = state;
                            state = SessionState.OPENED;
                            RewhiteException sException = new RewhiteException(RewhiteException.ErrorType.AUTHORIZATION_FAILED, "SESSION AUTHORIZE");
                            onStateChange(previous, state, null, sException, true);

                            //notifySessionState(state, null);
                        }*/
                        break;
                    default:
                        DUtil.Log("INSTANCE_LOCK OPENABLE", "AUTHORIZATION_FAILED");
                        throw new RewhiteException(ErrorType.AUTHORIZATION_FAILED, "current session state is not possible to open. state = " + state);
                }
            }
        } catch (RewhiteException e) {
            close(e, false);
        }
    }

    public void registerMember(final JoinMemberRequest _request) {
        aq = new AQuery(_request.getContext());

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("loginId", _request.getLoginId());
        params.put("secretKey", _request.getSecretKey());
        params.put("loginType", _request.getLoginType());
        params.put("isAgreeThirdOptinal", _request.getAgreeThirdOpt());

        DUtil.Log("registerMember params", params.toString());

        AjaxCallback.setNetworkLimit(1);
        aq.ajax(CommonUtility.SERVER_URL + Constants.MEMBER_REGISTER, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                if (status.getCode() >= 400) {
                    // IF ERROR
                    Log.i("MEMBER_REGISTER StatusCode", status.getCode() + "");
                    Log.i("MEMBER_REGISTER StatusError", status.getError());
                } else {
                    // IF NORMAL
                    if (json == null) {
                        Log.i("RETURN", "" + status.getMessage());
                        return;
                    }

                    try {
                        if ("S0000".equals(json.getString("resultCode"))) {
                            //Toast.makeText(mActivity, json.getString("result_msg"), Toast.LENGTH_SHORT);
                            AccessTokenRequest aRequest = AccessTokenRequest.createRequestWithDeviceToken(context, _request.getLoginId(),
                                    _request.getSecretKey(), _request.getLoginType(), _request.getDeviceToken());
                            requestAccessToken(aRequest);
                        } else {
                            Toast.makeText(aq.getContext(), json.getString("message"), Toast.LENGTH_SHORT).show();
                            close();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.i("requestPhoneAuth RESULT", json.toString());
                }
            }
        });
    }

    private void requestAccessToken(final AccessTokenRequest _request) {

        aq = new AQuery(_request.getContext());

        String url = CommonUtility.SERVER_URL + Constants.LOGIN;
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("loginId", _request.getLoginId());
        params.put("secretKey", _request.getSecretKey());
        params.put("loginType", _request.getLoginType());
        params.put("deviceType", "207");
        params.put("deviceToken", _request.getDeviceToken());

        //SharedPreferencesUtility.set(UserInfo.GCM_ID, _request.getDeviceToken());

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
        cb.url(url).type(JSONObject.class).params(params).weakHandler(this, "loginProcessCallback");
        //cb.header("Authorization", "device_token='" + _request.getDeviceToken() + "'");

        Log.i("getDeviceToken() ", _request.getDeviceToken() + ":::");

        aq.ajax(cb);
    }

    public void userInfoCallback(String url, JSONObject json, AjaxStatus status) {
        if (status.getCode() >= 400) {
            // IF ERROR
            Log.i("userInfoCallback StatusCode", status.getCode() + "");
            Log.i("userInfoCallback StatusError", status.getError());
        } else {
            // IF NORMAL
            if (json == null) {
                Log.i("RETURN", "" + status.getMessage());
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("서버와의 통신이 실패했습니다.")
                        .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                // 네트워크 오류페이지로 이동
                //goNetworkErrorPage();
                return;
            }

            try {
                Log.i("userInfoCallback >>", "" + json.toString());
                if ("S0000".equals(json.getString("resultCode")) || "S0100".equals(json.getString("resultCode")) || "S0101".equals(json.getString("resultCode"))) {

                    JSONObject userInfo = json.getJSONObject("data");
                    String accessToken = userInfo.getString("accessToken");
                    String loginType = userInfo.getString("loginType");
                    String loginId = userInfo.getString("loginId");
                    String userId = userInfo.getInt("userId") + "";
                    String userName = userInfo.getString("userName");
                    String shareCode = userInfo.getString("shareCode");
                    String phoneno = userInfo.getString("phone");
                    String email = userInfo.getString("email");
                    String isEmailConfirm = userInfo.getString("isEmailConfirm");
                    String birth = userInfo.getString("birth");
                    String gender = userInfo.getString("gender");
                    String imageBigPath = userInfo.getString("imageBigPath");
                    String imageThumbPath = userInfo.getString("imageThumbPath");
                    String useAvailablePoint = userInfo.getString("useAvailablePoint");
                    String totalEarnedPoint = userInfo.getString("totalEarnedPoint");
                    String expectPoint = userInfo.getString("expectPoint");
                    String useAvailablePrepaymentMoney = userInfo.getString("useAvailablePrepaymentMoney");
                    JSONArray agreeThirdList = userInfo.getJSONArray("agreeThird");

                    SharedPreferencesUtility.set(UserInfo.ACCESS_TOKEN, accessToken);
                    SharedPreferencesUtility.set(UserInfo.LOGIN_TYPE, loginType);
                    SharedPreferencesUtility.set(UserInfo.LOGIN_ID, loginId);
                    SharedPreferencesUtility.set(UserInfo.USER_ID, userId);
                    SharedPreferencesUtility.set(UserInfo.USER_NAME, userName);
                    SharedPreferencesUtility.set(UserInfo.SHARE_CODE, shareCode);
                    SharedPreferencesUtility.set(UserInfo.EMAIL, email);
                    SharedPreferencesUtility.set(UserInfo.EMAIL_AUTH_CHECK, isEmailConfirm);
                    SharedPreferencesUtility.set(UserInfo.PHONE_NO, phoneno);
                    SharedPreferencesUtility.set(UserInfo.BIRTH, birth);
                    SharedPreferencesUtility.set(UserInfo.GENDER, gender);
                    SharedPreferencesUtility.set(UserInfo.PROFILE_IMAGE, imageBigPath);
                    SharedPreferencesUtility.set(UserInfo.PROFILE_THUMB, imageThumbPath);
                    SharedPreferencesUtility.set(UserInfo.USE_AVAILABLE_POINT, useAvailablePoint);
                    SharedPreferencesUtility.set(UserInfo.TOTAL_EARNED_POINT, totalEarnedPoint);
                    SharedPreferencesUtility.set(UserInfo.EXPECT_POINT, expectPoint);
                    SharedPreferencesUtility.set(UserInfo.USE_AVAILABLE_COIN, useAvailablePrepaymentMoney);
                    SharedPreferencesUtility.set(UserInfo.TERMS_AGREE_THIRD, agreeThirdList.toString());
                    SharedPreferencesUtility.set(UserInfo.LOGIN_RESULT_CODE, json.getString("resultCode"));

                    SharedPreferencesUtility.setLocationData(userInfo.getJSONArray("geoInfo"));

                    final SessionState previous = state;
                    state = SessionState.OPENED;
                    RewhiteException sException = new RewhiteException(RewhiteException.ErrorType.AUTHORIZATION_FAILED, "SESSION AUTHORIZE");
                    onStateChange(previous, state, null, sException, true);
                } else {
                    //Toast.makeText(context.getApplicationContext(), json.getString("message"), Toast.LENGTH_SHORT).show();
                    AccessToken.createEmptyToken();
                    close();
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void loginProcessCallback(String url, JSONObject json, AjaxStatus status) {
        if (status.getCode() >= 400) {
            // IF ERROR
            Log.i("loginProcessCallback StatusCode", status.getCode() + "");
            Log.i("loginProcessCallback StatusError", status.getError());


        } else {
            // IF NORMAL
            if (json == null) {
                Log.i("RETURN", "" + status.getMessage());

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setMessage("서버와의 통신이 실패했습니다.")
                        .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                // 네트워크 오류페이지로 이동
                //goNetworkErrorPage();
                return;
            }

            try {
                Log.i("loginProcessCallback >>", "" + json.toString());
                if ("S0000".equals(json.getString("resultCode")) || "S0100".equals(json.getString("resultCode")) || "S0101".equals(json.getString("resultCode"))) {
                    AccessToken.createFromResponse(json);

                    String gcmId = SharedPreferencesUtility.get(UserInfo.GCM_ID);

                    JSONObject userInfo = json.getJSONObject("data");
                    String accessToken = userInfo.getString("accessToken");
                    String loginType = userInfo.getString("loginType");
                    String loginId = userInfo.getString("loginId");
                    String userId = userInfo.getInt("userId") + "";
                    String userName = userInfo.getString("userName");
                    String shareCode = userInfo.getString("shareCode");
                    String phoneno = userInfo.getString("phone");
                    String email = userInfo.getString("email");
                    String isEmailConfirm = userInfo.getString("isEmailConfirm");
                    String birth = userInfo.getString("birth");
                    String gender = userInfo.getString("gender");
                    String imageBigPath = userInfo.getString("imageBigPath");
                    String imageThumbPath = userInfo.getString("imageThumbPath");
                    String useAvailablePoint = userInfo.getString("useAvailablePoint");
                    String totalEarnedPoint = userInfo.getString("totalEarnedPoint");
                    String expectPoint = userInfo.getString("expectPoint");
                    String useAvailablePrepaymentMoney = userInfo.getString("useAvailablePrepaymentMoney");
                    JSONArray agreeThirdList = userInfo.getJSONArray("agreeThird");

                    SharedPreferencesUtility.set(UserInfo.ACCESS_TOKEN, accessToken);
                    SharedPreferencesUtility.set(UserInfo.LOGIN_TYPE, loginType);
                    SharedPreferencesUtility.set(UserInfo.LOGIN_ID, loginId);
                    SharedPreferencesUtility.set(UserInfo.USER_ID, userId);
                    SharedPreferencesUtility.set(UserInfo.USER_NAME, userName);
                    SharedPreferencesUtility.set(UserInfo.SHARE_CODE, shareCode);
                    SharedPreferencesUtility.set(UserInfo.EMAIL, email);
                    SharedPreferencesUtility.set(UserInfo.EMAIL_AUTH_CHECK, isEmailConfirm);
                    SharedPreferencesUtility.set(UserInfo.PHONE_NO, phoneno);
                    SharedPreferencesUtility.set(UserInfo.BIRTH, birth);
                    SharedPreferencesUtility.set(UserInfo.GENDER, gender);
                    SharedPreferencesUtility.set(UserInfo.PROFILE_IMAGE, imageBigPath);
                    SharedPreferencesUtility.set(UserInfo.PROFILE_THUMB, imageThumbPath);
                    SharedPreferencesUtility.set(UserInfo.USE_AVAILABLE_POINT, useAvailablePoint);
                    SharedPreferencesUtility.set(UserInfo.TOTAL_EARNED_POINT, totalEarnedPoint);
                    SharedPreferencesUtility.set(UserInfo.EXPECT_POINT, expectPoint);
                    SharedPreferencesUtility.set(UserInfo.USE_AVAILABLE_COIN, useAvailablePrepaymentMoney);
                    SharedPreferencesUtility.set(UserInfo.TERMS_AGREE_THIRD, agreeThirdList.toString());
                    SharedPreferencesUtility.set(UserInfo.LOGIN_RESULT_CODE, json.getString("resultCode"));

                    SharedPreferencesUtility.setLocationData(userInfo.getJSONArray("geoInfo"));

                    final SessionState previous = state;
                    state = SessionState.OPENED;
                    RewhiteException sException = new RewhiteException(RewhiteException.ErrorType.AUTHORIZATION_FAILED, "SESSION AUTHORIZE");
                    onStateChange(previous, state, null, sException, true);
					/*
					AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>();
					cb.url(userInfoUrl).type(JSONObject.class).params(params).weakHandler(this, "userInfoCallback");
					//cb.header("Authorization", "device_token='" + gcmId + "',access_token='" + json.getString("access_token") + "'");
					Log.i("getDeviceToken", gcmId + " / " + accessToken.getAccessTokenString());
					aq.ajax(cb);*/
                } else if ("S9003".equals(json.getString("resultCode"))) {
                    // 페이스북 가입필요
                    Log.i("loginProcess", "Invalid Facebook User : Join Required");
                    RewhiteException sException = new RewhiteException(RewhiteException.ErrorType.AUTHORIZATION_FAILED, "S9003");
                    close(sException, true);
                } else if ("S9004".equals(json.getString("resultCode"))) {
                    // 카카오톡 가입필요
                    Log.i("loginProcess", "Invalid Kakao User : Join Required");
                    RewhiteException sException = new RewhiteException(RewhiteException.ErrorType.AUTHORIZATION_FAILED, "S9004");
                    close(sException, true);
                } else if ("S9005".equals(json.getString("resultCode"))) {
                    // GOOGLE 가입필요
                    Log.i("loginProcess", "Invalid Kakao User : Join Required");
                    RewhiteException sException = new RewhiteException(RewhiteException.ErrorType.AUTHORIZATION_FAILED, "S9005");
                    close(sException, true);
                }else if ("S9006".equals(json.getString("resultCode"))) {
                    // PAYCO 가입필요
                    Log.i("loginProcess", "Invalid Kakao User : Join Required");
                    RewhiteException sException = new RewhiteException(RewhiteException.ErrorType.AUTHORIZATION_FAILED, "S9006");
                    close(sException, true);
                }else {
                    Toast.makeText(context.getApplicationContext(), json.getString("message"), Toast.LENGTH_SHORT).show();
                    AccessToken.createEmptyToken();
                    close();
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Log.i("loginProcess", json.toString());
        }
    }

    /**
     * 토큰 갱신이 가능한지 여부를 반환한다.
     * 토큰 갱신은 background로 사용자가 모르도록 진행한다.
     *
     * @return 토큰 갱신을 진행할 때는 true, 토큰 갱신을 하지 못할때는 false를 return 한다.
     */
    public boolean implicitOpen() {
        if (isOpened() || (isOpenable() && accessToken.hasToken())) {
            open(null);
            return true;
        } else {
            return false;
        }
    }

    /**
     * 명시적 강제 close(로그아웃/탈퇴). request중 인 것들은 모두 실패를 받게 된다.
     * token을 삭제하기 때문에 authorization code부터(로그인 버튼) 다시 받아서 세션을 open 해야한다.
     */
    public void close(final RewhiteException rewhiteException, final boolean forced) {
        synchronized (INSTANCE_LOCK) {
            final SessionState previous = state;
            state = SessionState.CLOSED;
            accessToken = AccessToken.createEmptyToken();
            SharedPreferencesUtility.logoutClear();
            onStateChange(previous, state, null, rewhiteException, forced);
        }
    }

    public void close() {
        RewhiteException sException = new RewhiteException(RewhiteException.ErrorType.AUTHORIZATION_FAILED, "SESSION AUTHORIZE");
        close(sException, true);
    }

    private void onStateChange(final SessionState previousState, final SessionState newState, final RequestType requestType,
                               final RewhiteException exception, boolean forced) {
        if (!forced && (previousState == newState) && exception == null) {
            return;
        }

        DUtil.Log(
                "Rewhite Session",
                String.format("Session State changed : %s -> %s \n ex = %s, request_type = %s", previousState, newState, (exception != null ? ", ex="
                        + exception.getMessage() : ""), requestType));

        // 사용자에게 opening을 state를 알려줄 필요는 없는듯.
        if (newState.isOpenable()) return;

        notifySessionState(newState, exception);
    }

    private void notifySessionState(final SessionState newState, final RewhiteException exception) {
        final List<RewhiteSessionCallback> dumpRewhiteSessionCallbacks = new ArrayList<RewhiteSessionCallback>(rewhiteSessionCallbacks);
        Runnable runCallbacks = new Runnable() {

            public void run() {
                if (newState.isOpened()) {
                    //registerTokenManger(DEFAULT_TOKEN_REQUEST_TIME_MILLIS);
                } else if (newState.isClosed()) {
                    //deregisterTokenManger();
                }

                for (RewhiteSessionCallback callback : dumpRewhiteSessionCallbacks) {
                    if (newState.isOpened()) {
                        callback.onSessionOpened();
                    } else if (newState.isClosed()) {
                        callback.onSessionClosed(exception);
                    }

                }
            }
        };
        //세션 callback은 main thread에서 호출되도록 한다.
        sessionCallbackHandler.post(runCallbacks);
    }

    /**
     * 현재 세션이 가지고 있는 access token이 유효한지를 검사후 세션의 상태를 반환한다.
     * 만료되었다면 opened 상태가 아닌 opening상태가 반환된다.
     *
     * @return 세션의 상태
     */
    public final SessionState checkState() {
        synchronized (INSTANCE_LOCK) {
            if (state.isOpened()) {
                synchronized (INSTANCE_LOCK) {
                    state = SessionState.OPENABLE;
                    //requestType = null;
                    //authorizationCode = AuthorizationCode.createEmptyCode();
                }
            }
            return state;
        }
    }

    /**
     * 현재 세션의 상태
     *
     * @return 세션의 상태
     */
    public SessionState getState() {
        synchronized (INSTANCE_LOCK) {
            return state;
        }
    }

    public static synchronized RewhiteSession getCurrentSession() {
        if (currentSession == null) {
            throw new IllegalStateException("Session is not initialized. Call Session#initializeSession first.");
        }

        return currentSession;
    }

    /**
     * 현재 세션이 열린 상태인지 여부를 반환한다.
     *
     * @return 세션이 열린 상태라면 true, 그외의 경우 false를 반환한다.
     */
    public final boolean isOpened() {
        final SessionState state = checkState();
        return state == SessionState.OPENED;
    }

    /**
     * 현재 세션이 오픈중(갱신 포함) 상태인지 여부를 반환한다.
     *
     * @return 세션 오픈 진행 중이면 true, 그외 경우는 false를 반환한다.
     */
    public boolean isOpenable() {
        final SessionState state = checkState();
        return state == SessionState.OPENABLE;
    }

    /**
     * 현재 세션이 닫힌 상태인지 여부를 반환한다.
     *
     * @return 세션이 닫힌 상태라면 true, 그외의 경우 false를 반환한다.
     */
    public final boolean isClosed() {
        final SessionState state = checkState();
        return state == SessionState.CLOSED;
    }

    /**
     * 현재 세션이 가지고 있는 access token을 반환한다.
     *
     * @return access token
     */
    public final String getAccessToken() {
        synchronized (INSTANCE_LOCK) {
            if (accessToken == null) {
                accessToken = AccessToken.createFromCache();
                return (accessToken.getAccessTokenString() == null) ? null : accessToken.getAccessTokenString();
            } else {
                return accessToken.getAccessTokenString();
            }

        }
    }

    /**
     * 로그인 activity를 이용하여 sdk에서 필요로 하는 activity를 띄운다.
     * 따라서 해당 activity의 결과를 로그인 activity가 받게 된다.
     * 해당 결과를 세션이 받아서 다음 처리를 할 수 있도록 로그인 activity의 onActivityResult에서 해당 method를 호출한다.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     * @return
     */
    public boolean handleActivityResult(int requestCode, int resultCode, Intent data) {
        return requestCode == AUTHORIZATION_CODE_REQUEST;
    }

    /**
     * 세션 상태 변화 콜백을 받고자 할때 콜백을 등록한다.
     *
     * @param callback 추가할 세션 콜백
     */
    public void addCallback(final RewhiteSessionCallback callback) {
        synchronized (rewhiteSessionCallbacks) {
            if (callback != null && !rewhiteSessionCallbacks.contains(callback)) {
                rewhiteSessionCallbacks.add(callback);
            }
        }
    }

    /**
     * 더이상 세션 상태 변화 콜백을 받고 싶지 않을 때 삭제한다.
     *
     * @param callback 삭제할 콜백
     */
    public void removeCallback(final RewhiteSessionCallback callback) {
        synchronized (rewhiteSessionCallbacks) {
            if (callback != null) {
                rewhiteSessionCallbacks.remove(callback);
            }
        }
    }

    private void clearCallbacks() {
        synchronized (rewhiteSessionCallbacks) {
            rewhiteSessionCallbacks.clear();
        }
    }

    public Context getContext() {
        return context;
    }

    /**
     * @author Marines
     * @Minwise
     */
    private enum SessionState {
        /**
         * memory와 cache에 session 정보가 없는 전혀 상태.
         * 처음 session에 접근할 때 또는 session을 close(예를 들어 로그아웃, 탈퇴)한 상태.
         * open({@link me.rewhite.users.session.RequestType#GETTING_AUTHORIZATION_CODE}) : 성공 -
         * {@link #OPENABLE}, 실패 - 그대로 CLOSED
         * close(명시적 close) : 그대로 CLOSED
         */
        CLOSED,
        /**
         * {@link #CLOSED}상태에서 token을 발급 받기 위해 authorization code를 발급 받아 valid한 authorization code를
         * 가지고 있는 상태.
         * 또는 토큰이 만료되었으나 refresh token을 가지고 있는 상태.
         * open({@link me.rewhite.users.session.RequestType#GETTING_ACCESS_TOKEN} 또는
         * {@link me.rewhite.users.session.RequestType#REFRESHING_ACCESS_TOKEN}) : 성공 -
         * {@link #OPENED}, 실패 - {@link #CLOSED} close(명시적 close) : {@link #CLOSED}
         */
        OPENABLE,

        /**
         * access token을 성공적으로 발급 받아 valid access token을 가지고 있는 상태.
         * 토큰 만료 : {@link #OPENABLE} close(명시적 close) : {@link #CLOSED}
         */
        OPENED,

        /**
         * {@link #CLOSED} 상태에서 authcode를 요청하는 경우, code나 refresh token으로 token을 요청하는 경우 중간 단계.
         */
        OPENING;

        private boolean isClosed() {
            return this == SessionState.CLOSED;
        }

        private boolean isOpenable() {
            return this == SessionState.OPENABLE;
        }

        private boolean isOpened() {
            return this == SessionState.OPENED;
        }

        private boolean isOpening() {
            return this == SessionState.OPENING;
        }
    }

    private enum RequestType {
        GETTING_AUTHORIZATION_CODE, GETTING_ACCESS_TOKEN, REFRESHING_ACCESS_TOKEN;

        private boolean isAuthorizationCodeRequest() {
            return this == RequestType.GETTING_AUTHORIZATION_CODE;
        }

        private boolean isAccessTokenRequest() {
            return this == RequestType.GETTING_ACCESS_TOKEN;
        }

        private boolean isRefreshingTokenRequest() {
            return this == RequestType.REFRESHING_ACCESS_TOKEN;
        }
    }

    @Override
    public void onAuthorizationCompletion(AuthorizationResult result) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, int mode, String param) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog, int mode, String param) {
        // TODO Auto-generated method stub

    }
}
