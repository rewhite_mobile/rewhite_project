package me.rewhite.users.util;

import android.util.Log;

import java.text.DecimalFormat;
import java.util.regex.Pattern;


public class MZValidator {

    // ValidatorPattern
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
            + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");

    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-zA-Z]).{6,14})";
    public static final Pattern pattern = Pattern.compile(PASSWORD_PATTERN);

    public static int validPassword(final String _origin) {

        boolean valid = pattern.matcher(_origin).matches();

        if (_origin == null || _origin.length() < 6) {
            return 0;
        } else {
            if (valid) {
                return 1;
            } else {
                return 0;
            }
        }

    }

    public static String toNumFormat(int num) {
        //NumberFormat fmt1 = NumberFormat.getCurrencyInstance();
        DecimalFormat df = new DecimalFormat("#,###,###");
        return df.format(num);
    }

    public static boolean validPartys(String _origin) {

        if (_origin.contains("_")) {
            return false;
        } else if (_origin.contains("$")) {
            return false;
        } else if (_origin.contains("\\")) {
            return false;
        } else if (_origin.contains("'")) {
            return false;
        } else if (_origin.contains(",")) {
            return false;
        } else if (_origin.contains("<")) {
            return false;
        } else if (_origin.contains(">")) {
            return false;
        } else if (_origin.contains("[")) {
            return false;
        } else if (_origin.contains("]")) {
            return false;
        } else if (_origin.contains(";")) {
            return false;
        } else if (_origin.contains("{")) {
            return false;
        } else return !_origin.contains("}");

    }

    public static boolean validNickname(String _origin) {

        if (_origin.contains(" ")) {
            return false;
        } else if (_origin.contains("$")) {
            return false;
        } else if (_origin.contains("\\")) {
            return false;
        } else if (_origin.contains("'")) {
            return false;
        } else if (_origin.contains(",")) {
            return false;
        } else if (_origin.contains("<")) {
            return false;
        } else if (_origin.contains(">")) {
            return false;
        } else if (_origin.contains("[")) {
            return false;
        } else if (_origin.contains("]")) {
            return false;
        } else if (_origin.contains(";")) {
            return false;
        } else if (_origin.contains("{")) {
            return false;
        } else return !_origin.contains("}");

    }

    public static boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public static String validTelNumber(String _origin) {
        String temp = null;
        String origin;

        Log.i("telNum", _origin);

        origin = _origin.replace("-", "");

        if (origin.length() == 10 || origin.length() == 11) {
            String fn = origin.substring(0, 3);

            if ("010".equals(fn) || "011".equals(fn) || "016".equals(fn) || "017".equals(fn) || "018".equals(fn) || "019".equals(fn)
                    || "070".equals(fn)) {
                if (origin.length() == 10) {
                    temp = fn + "-" + origin.substring(3, 6) + "-" + origin.substring(6, 10);
                } else {
                    temp = fn + "-" + origin.substring(3, 7) + "-" + origin.substring(7, 11);
                }
            }

        }

        Log.i("final telNum", temp + "");
        return temp;
    }

    public static boolean validString(String _origin) {
        if ("null".equals(_origin)) {
            return false;
        }

        String newString = _origin.replace(" ", "");
        return newString.length() > 0;
    }

    public static int convertVersionStringToInteger(String verString) {
        int temp = 0;
        String[] verArray;
        verArray = verString.split("\\.");

        int majorValue = Integer.parseInt(verArray[0]);
        int minorValue = Integer.parseInt(verArray[1]);
        int customValue = 0;
        if (verArray.length > 2) {
            customValue = Integer.parseInt(verArray[2]);
        }
        int customValue2 = 0;
        if (verArray.length > 3) {
            customValue2 = Integer.parseInt(verArray[3]);
        }

        temp = majorValue * 100000000 + minorValue * 1000000 + customValue*1000 + customValue2;

        return temp;
    }

}
