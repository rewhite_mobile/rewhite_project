package me.rewhite.users.util;

import java.util.regex.Pattern;


public class ValidateUtil {

    // ValidatorPattern
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@"
            + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+");

    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-zA-Z]).{6,14})";
    public static final Pattern pattern = Pattern.compile(PASSWORD_PATTERN);

    public static int validPassword(final String _origin) {

        boolean valid = pattern.matcher(_origin).matches();

        if (_origin == null || _origin.length() < 8) {
            return 0;
        } else {
            // 다른조건 없이 8자리 이상의 비번이면 모두 Valid.
            if (valid) {
                return 2;
            } else {
                return 1;
            }
        }

    }

    public static boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public static boolean isValidPhoneNumber(String _origin){
        String temp = null;
        String origin;

        DUtil.Log("telNum", _origin);

        temp = _origin.replace("-", "");
        if (_origin.length() == 10 || _origin.length() == 11) {
            String fn = _origin.substring(0, 3);

            if ("010".equals(fn) || "011".equals(fn) || "016".equals(fn) || "017".equals(fn) || "018".equals(fn) || "019".equals(fn)) {
                return true;
            }else if("070".equals(fn)){
                return false;
            }else{
                return false;
            }

        }else{
            return false;
        }
    }

    public static String validTelNumber(String _origin) {
        String temp = null;
        String origin;

        DUtil.Log("telNum", _origin);

        temp = _origin.replace("-", "");
        /*
        if (origin.length() == 10 || origin.length() == 11) {
			String fn = origin.substring(0, 3);

			if ("010".equals(fn) || "011".equals(fn) || "016".equals(fn) || "017".equals(fn) || "018".equals(fn) || "019".equals(fn)
					|| "070".equals(fn)) {
				if (origin.length() == 10) {
					temp = fn + "-" + origin.substring(3, 6) + "-" + origin.substring(6, 10);
				} else {
					temp = fn + "-" + origin.substring(3, 7) + "-" + origin.substring(7, 11);
				}
			}

		}
*/
        DUtil.Log("final telNum", temp + "");
        return temp;
    }

    /**
     * 모바일 전화번호 검증
     *
     * @param phoneNum 구분자 포함된 번호
     * @param separate 구분자
     * @return
     */
    public static boolean isMobilePhoneNumber(String phoneNum, String separate) {
        String regEx = "^01(?:[0-9])" + separate + "(?:\\d{3}|\\d{4})" + separate + "\\d{4}$";
        return Pattern.matches(regEx, phoneNum);
    }

    /**
     * 모바일 전화번호 검증
     *
     * @param phoneNum
     * @return
     */
    public static boolean isMobilePhoneNumber(String phoneNum) {
        String regEx = "^01(?:[0-9])(?:\\d{3}|\\d{4})\\d{4}$";
        return Pattern.matches(regEx, phoneNum);
    }
}
