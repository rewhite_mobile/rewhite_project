package me.rewhite.users.util;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;


public class SharedPreferencesUtility {

    public enum Command {
        IS_AUTO_LOGIN, // 자동 로그인 여부
        IS_TUTORIAL, // tutorial 확인 여부
        IS_FIRST_STARTED,
        FONT_STYLE, // 글꼴 타입
        FONT_SIZE, // 글자 크기
        SHORTCUT,
        IS_NOTIFICATION,
        EVENT_LATEST_DATE,
        EVENT_NEVER_SHOW
    }

    public enum UserInfo {
        GCM_ID, LOGIN_TYPE, REFER, LOGIN_ID, USER_ID, EMAIL, USER_NAME, SHARE_CODE, ACCESS_TOKEN,
        EMAIL_AUTH_CHECK, PHONE_NO, TEMP_IMAGE, PROFILE_IMAGE, PROFILE_THUMB, GENDER, BIRTH, USE_AVAILABLE_POINT, TOTAL_EARNED_POINT, EXPECT_POINT, USE_AVAILABLE_COIN, TERMS_AGREE_THIRD,
        TERMS_THIRDS_TRIED_TPHONE, LOGIN_RESULT_CODE
    }

    public enum TempInfo {
        TEMP_IMAGE, TEMP_THUMB, TEMP_USER_NAME, TEMP_EMAIL, TEMP_LOGIN_TYPE
    }

    public enum LocationInfo {
        LIST, CURRENT_SELECTED_STORE_ID, CURRENT_SELECTED_ADDRESS_SEQ
    }

    private static SharedPreferences mPref = null;

    public static void initialize(Context context) {
        mPref = context.getSharedPreferences("me.rewhite.users", Context.MODE_PRIVATE);

        if (mPref != null && mPref.getAll().isEmpty()) {
            SharedPreferences.Editor edit = mPref.edit();

            // Oriented Device Value
            edit.putString(Command.IS_TUTORIAL.toString(), "false");
            edit.putString(Command.IS_FIRST_STARTED.toString(), "false");
            edit.putString(UserInfo.GCM_ID.toString(), "");
            edit.putString(Command.EVENT_LATEST_DATE.toString(), "");
            edit.putString(UserInfo.TERMS_AGREE_THIRD.toString(), "");
            edit.putString(UserInfo.TERMS_THIRDS_TRIED_TPHONE.toString(), "N");
            edit.putString(UserInfo.LOGIN_RESULT_CODE.toString(), "");

            edit.putString(Command.IS_AUTO_LOGIN.toString(), "false");
            edit.putString(UserInfo.LOGIN_ID.toString(), "");
            edit.putString(UserInfo.REFER.toString(), "");
            edit.putString(UserInfo.LOGIN_TYPE.toString(), "");
            edit.putString(UserInfo.USER_ID.toString(), "");
            edit.putString(UserInfo.USER_NAME.toString(), "");
            edit.putString(UserInfo.SHARE_CODE.toString(), "");
            edit.putString(UserInfo.EMAIL.toString(), "");
            edit.putString(UserInfo.ACCESS_TOKEN.toString(), "");
            edit.putString(UserInfo.EMAIL_AUTH_CHECK.toString(), "N");
            edit.putString(UserInfo.PROFILE_IMAGE.toString(), "");
            edit.putString(UserInfo.PROFILE_THUMB.toString(), "");
            edit.putString(UserInfo.PHONE_NO.toString(), "");
            edit.putString(UserInfo.GENDER.toString(), "");
            edit.putString(UserInfo.BIRTH.toString(), "");
            edit.putString(UserInfo.USE_AVAILABLE_POINT.toString(), "0");
            edit.putString(UserInfo.TOTAL_EARNED_POINT.toString(), "0");
            edit.putString(UserInfo.EXPECT_POINT.toString(), "0");
            edit.putString(UserInfo.USE_AVAILABLE_COIN.toString(), "0");

            edit.putString(TempInfo.TEMP_IMAGE.toString(), "");
            edit.putString(TempInfo.TEMP_THUMB.toString(), "");
            edit.putString(TempInfo.TEMP_USER_NAME.toString(), "");
            edit.putString(TempInfo.TEMP_EMAIL.toString(), "");
            edit.putString(TempInfo.TEMP_LOGIN_TYPE.toString(), "");
            edit.putString(Command.EVENT_NEVER_SHOW.toString(), "");

            edit.putString(Command.IS_NOTIFICATION.toString(), "true");

            edit.putString(LocationInfo.LIST.toString(), "[]");
            edit.putString(LocationInfo.CURRENT_SELECTED_STORE_ID.toString(), "0");

            edit.commit();
        }
    }

    public static void logoutClear() {
        if (mPref != null && !mPref.getAll().isEmpty()) {
            SharedPreferences.Editor edit = mPref.edit();

            edit.putString(Command.IS_AUTO_LOGIN.toString(), "false");
            edit.putString(Command.EVENT_LATEST_DATE.toString(), "");
            edit.putString(UserInfo.TERMS_AGREE_THIRD.toString(), "");
            edit.putString(UserInfo.TERMS_THIRDS_TRIED_TPHONE.toString(), "N");
            edit.putString(UserInfo.LOGIN_RESULT_CODE.toString(), "");

            edit.putString(UserInfo.LOGIN_ID.toString(), "");
            edit.putString(UserInfo.LOGIN_TYPE.toString(), "");
            edit.putString(UserInfo.USER_ID.toString(), "");
            edit.putString(UserInfo.USER_NAME.toString(), "");
            edit.putString(UserInfo.SHARE_CODE.toString(), "");
            edit.putString(UserInfo.EMAIL.toString(), "");
            edit.putString(UserInfo.ACCESS_TOKEN.toString(), "");
            edit.putString(UserInfo.EMAIL_AUTH_CHECK.toString(), "N");
            edit.putString(UserInfo.PROFILE_IMAGE.toString(), "");
            edit.putString(UserInfo.PROFILE_THUMB.toString(), "");
            edit.putString(UserInfo.PHONE_NO.toString(), "");
            edit.putString(UserInfo.GENDER.toString(), "");
            edit.putString(UserInfo.BIRTH.toString(), "");
            edit.putString(UserInfo.USE_AVAILABLE_POINT.toString(), "0");
            edit.putString(UserInfo.TOTAL_EARNED_POINT.toString(), "0");
            edit.putString(UserInfo.EXPECT_POINT.toString(), "0");
            edit.putString(UserInfo.USE_AVAILABLE_COIN.toString(), "0");
            edit.putString(Command.IS_NOTIFICATION.toString(), "true");

            edit.putString(TempInfo.TEMP_IMAGE.toString(), "");
            edit.putString(TempInfo.TEMP_THUMB.toString(), "");
            edit.putString(TempInfo.TEMP_USER_NAME.toString(), "");
            edit.putString(TempInfo.TEMP_EMAIL.toString(), "");
            edit.putString(TempInfo.TEMP_LOGIN_TYPE.toString(), "");
            edit.putString(Command.EVENT_NEVER_SHOW.toString(), "");

            edit.putString(LocationInfo.LIST.toString(), "[]");
            edit.putString(LocationInfo.CURRENT_SELECTED_STORE_ID.toString(), "0");

            edit.commit();
        }
    }

    public static void clear() {
        if (mPref != null && !mPref.getAll().isEmpty()) {
            SharedPreferences.Editor edit = mPref.edit();

            edit.putString(Command.FONT_STYLE.toString(), "");
            edit.putString(Command.FONT_SIZE.toString(), "");

            edit.putString(Command.IS_AUTO_LOGIN.toString(), "false");
            edit.putString(Command.EVENT_LATEST_DATE.toString(), "");
            edit.putString(UserInfo.TERMS_AGREE_THIRD.toString(), "");
            edit.putString(UserInfo.TERMS_THIRDS_TRIED_TPHONE.toString(), "N");
            edit.putString(UserInfo.LOGIN_RESULT_CODE.toString(), "");

            edit.putString(UserInfo.LOGIN_ID.toString(), "");
            edit.putString(UserInfo.LOGIN_TYPE.toString(), "");
            edit.putString(UserInfo.USER_ID.toString(), "");
            edit.putString(UserInfo.USER_NAME.toString(), "");
            edit.putString(UserInfo.SHARE_CODE.toString(), "");
            edit.putString(UserInfo.EMAIL.toString(), "");
            edit.putString(UserInfo.ACCESS_TOKEN.toString(), "");
            edit.putString(UserInfo.EMAIL_AUTH_CHECK.toString(), "N");
            edit.putString(UserInfo.PROFILE_IMAGE.toString(), "");
            edit.putString(UserInfo.PROFILE_THUMB.toString(), "");
            edit.putString(UserInfo.PHONE_NO.toString(), "");
            edit.putString(UserInfo.GENDER.toString(), "");
            edit.putString(UserInfo.BIRTH.toString(), "");
            edit.putString(UserInfo.USE_AVAILABLE_POINT.toString(), "0");
            edit.putString(UserInfo.TOTAL_EARNED_POINT.toString(), "0");
            edit.putString(UserInfo.EXPECT_POINT.toString(), "0");
            edit.putString(UserInfo.USE_AVAILABLE_COIN.toString(), "0");
            edit.putString(Command.IS_NOTIFICATION.toString(), "true");

            edit.putString(TempInfo.TEMP_IMAGE.toString(), "");
            edit.putString(TempInfo.TEMP_THUMB.toString(), "");
            edit.putString(TempInfo.TEMP_USER_NAME.toString(), "");
            edit.putString(TempInfo.TEMP_EMAIL.toString(), "");
            edit.putString(TempInfo.TEMP_LOGIN_TYPE.toString(), "");
            edit.putString(Command.EVENT_NEVER_SHOW.toString(), "");

            edit.putString(LocationInfo.LIST.toString(), "[]");
            edit.putString(LocationInfo.CURRENT_SELECTED_STORE_ID.toString(), "0");

            edit.commit();
        }
    }

    public static String get(Object cmd) {
        if (mPref == null) return null;
        return mPref.getString(cmd.toString(), null);
    }

    public static boolean set(Object cmd, String value) {
        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();

            edit.putString(cmd.toString(), value);
            return edit.commit();
        }
        return false;
    }

    public static boolean setLocationData(JSONArray datas) {
        if (mPref != null) {
            SharedPreferences.Editor edit = mPref.edit();

            edit.putString(LocationInfo.LIST.toString(), datas.toString());
            return edit.commit();
        }
        return false;
    }

    public static JSONArray getLocationData() {
        if (mPref == null) return null;
        String data = mPref.getString(LocationInfo.LIST.toString(), null);
        if (data == null) {
            return null;
        }
        JSONArray arr = null;
        try {
            arr = new JSONArray(data);
            return arr;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }
}