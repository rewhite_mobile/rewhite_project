package me.rewhite.users.util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.users.common.RewhiteCMD;

public class RewhiteServerInterface {
    private static final String TAG = "RewhiteServerInterface";

    Context mContext;
    SMemoASyncTask mRewhiteAsync;
    RewhiteServiceInterfaceListener mListener = null;

    public RewhiteServerInterface(Context context) {
        mContext = context;
    }

    public void setInterface(RewhiteServiceInterfaceListener listener) {
        mListener = listener;
    }

    public void checkPassword(String password) {
        final String CMD = RewhiteCMD.PASSWORD_CHECK;

        mRewhiteAsync = new SMemoASyncTask();
        JSONObject json = new JSONObject();

        try {
            json.put(RewhiteCMD.JSON_PASSWORD, password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mRewhiteAsync.execute(CMD, json.toString());
    }

    public void terms(String type) {
        final String CMD = RewhiteCMD.TERMS;

        mRewhiteAsync = new SMemoASyncTask();
        mRewhiteAsync.execute(CMD, type);
    }

    public void appVersion() {
        final String CMD = RewhiteCMD.VERSION;

        mRewhiteAsync = new SMemoASyncTask();
        mRewhiteAsync.execute(CMD);
    }

    public int typeNotice(String type) {
        final String CMD = RewhiteCMD.TYPE_BOARD;
        int ret = 0;

        mRewhiteAsync = new SMemoASyncTask();
        mRewhiteAsync.execute(CMD, type);
        return ret;
    }

    public int notice() {
        final String CMD = RewhiteCMD.NOTICE;
        int ret = 0;

        mRewhiteAsync = new SMemoASyncTask();
        mRewhiteAsync.execute(CMD);
        return ret;
    }

    public int faq(String type) {
        final String CMD = RewhiteCMD.FAQ;
        int ret = 0;

        mRewhiteAsync = new SMemoASyncTask();
        mRewhiteAsync.execute(CMD, type);
        return ret;
    }

    public int inquire(String type, String name, String email, String title, String content) {
        final String CMD = RewhiteCMD.INQUIRE;
        int ret = 0;
        JSONObject json = new JSONObject();

        try {
            json.put(RewhiteCMD.JSON_INQUIRE_TYPE, type);
            json.put(RewhiteCMD.JSON_INQUIRE_NAME, name);
            json.put(RewhiteCMD.JSON_INQUIRE_EMAIL, email);
            json.put(RewhiteCMD.JSON_INQUIRE_TITLE, title);
            json.put(RewhiteCMD.JSON_INQUIRE_CONTENT, content);
        } catch (JSONException e) {
            e.printStackTrace();
            ret = -1;
        }

        mRewhiteAsync = new SMemoASyncTask();
        mRewhiteAsync.execute(CMD, json.toString());
        return ret;
    }

    public class SMemoASyncTask extends AsyncTask<String, Integer, String> {
        ServerUtility mServer = new ServerUtility();
        private final String BASE_URL = CommonUtility.SERVER_URL;

        @Override
        protected String doInBackground(String... strings) {
            String retValue = null;
            String result = null;

            if (strings[0].equals(RewhiteCMD.TYPE_BOARD) || strings[0].equals(RewhiteCMD.FAQ) || strings[0].equals(RewhiteCMD.TERMS)) {
                result = mServer.getHttpJson(BASE_URL + CommonUtility.APIS_SUPPORT + strings[0] + "/" + strings[1]);
            } else if (strings[0].equals(RewhiteCMD.NOTICE) || strings[0].equals(RewhiteCMD.VERSION)) {
                result = mServer.getHttpJson(BASE_URL + CommonUtility.APIS_SUPPORT + strings[0]);
            } else if (strings[0].equals(RewhiteCMD.PASSWORD_CHECK)) {
                JSONObject json;
                try {
                    json = new JSONObject(strings[1]);
                    result = mServer.sendHttpJson(BASE_URL + CommonUtility.APIS_USER + strings[0], json, true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else if (strings[0].equals(RewhiteCMD.INQUIRE)) {
                JSONObject json;
                try {
                    json = new JSONObject(strings[1]);
                    result = mServer.sendHttpJson(BASE_URL + CommonUtility.APIS_SUPPORT + strings[0], json, false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            retValue = strings[0] + "^" + result;

            return retValue;
        }

        @Override
        protected void onCancelled() {

            super.onCancelled();
        }

        @Override
        protected void onPostExecute(String result) {
            String command = null;
            String value = null;
            Log.d("debug", mListener + "onPostExecute::" + result);
            if (mListener != null) {
                if (result != null) {
                    int pos = result.indexOf("^");
                    command = result.substring(0, pos);
                    value = result.substring(pos + 1, result.length());
                }
                mListener.onResult(command, value);
            }
            super.onPostExecute(result);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

            super.onProgressUpdate(values);
        }

    }

    public interface RewhiteServiceInterfaceListener {
        void onResult(String command, String result);
    }
}
