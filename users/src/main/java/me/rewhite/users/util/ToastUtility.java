package me.rewhite.users.util;

import android.content.Context;

/**
 * Created by marines on 15. 6. 23..
 */
public class ToastUtility {
    private static android.widget.Toast mToast;

    public static void show(Context context, int resourceId) {
        show(context, resourceId, android.widget.Toast.LENGTH_SHORT);
    }

    public static void show(Context context, int resourceId, int length) {
        if (mToast == null) {
            mToast = android.widget.Toast.makeText(context, resourceId, length);
        } else {
            mToast.setText(resourceId);
        }

        mToast.show();
    }

    public static void show(Context context, CharSequence text) {
        show(context, text, android.widget.Toast.LENGTH_SHORT);
    }

    public static void show(Context context, CharSequence text, int length) {
        if (mToast == null) {
            mToast = android.widget.Toast.makeText(context, text, length);
        } else {
            mToast.setText(text);
        }

        mToast.show();
    }
}
