package me.rewhite.users.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class TimeUtil {

    static private final String ORDER_DATE_FORMAT = "yyyy-MM-dd";

    static private final String FORMAT_DATE_TIME_TYPE1 = "yyyyMMdd";
    static private final String FORMAT_DATE_TIME_TYPE2 = "yyyy.MM.dd HH:mm";
    static private final String FORMAT_DATE_ONLY_TYPE1 = "yyyyMMdd";
    static private final String FORMAT_DATE_ONLY_TYPE2 = "yyyy.MM.dd";
    static private final String FORMAT_DATE_ONLY_TYPE3 = "yy.MM.dd";
    static private final String FORMAT_DATE_ONLY_TYPE4 = "yy/MM/dd";
    static private final String FORMAT_DISPLAY_DATE_ONLY = "MM/dd";
    static private final String FORMAT_TIME_ONLY = "HH:mm";
    static private final String FORMAT_SMEMO_TIMESTAMP = "yyMMddHHmmSSS";
    static private final String FORMAT_ORDER_TIMESTAMP = "yyyyMMddHH";

    static private SimpleDateFormat mDateformatOrderDateType = new SimpleDateFormat(ORDER_DATE_FORMAT);

    static private SimpleDateFormat mDateformatDateTimeType1 = new SimpleDateFormat(FORMAT_DATE_TIME_TYPE1);
    static private SimpleDateFormat mDateformatDateTimeType2 = new SimpleDateFormat(FORMAT_DATE_TIME_TYPE2);
    static private SimpleDateFormat mDateformatDateOrderTimeType = new SimpleDateFormat(FORMAT_ORDER_TIMESTAMP);
    static private SimpleDateFormat mDateformatDateType1 = new SimpleDateFormat(FORMAT_DATE_ONLY_TYPE1);
    static private SimpleDateFormat mDateformatDateType2 = new SimpleDateFormat(FORMAT_DATE_ONLY_TYPE2);
    static private SimpleDateFormat mDateformatDateType3 = new SimpleDateFormat(FORMAT_DATE_ONLY_TYPE3);
    static private SimpleDateFormat mDateformatDateType4 = new SimpleDateFormat(FORMAT_DATE_ONLY_TYPE4);
    static private SimpleDateFormat mDateformatDisplayDate = new SimpleDateFormat(FORMAT_DISPLAY_DATE_ONLY);
    static private SimpleDateFormat mDateformatTime = new SimpleDateFormat(FORMAT_TIME_ONLY);


    public static String getToday() {
        return mDateformatDateType1.format(new Date());
    }

    public static Date getDate(long time) {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(new Date(time));
        Date date = new Date();
        try {
            date = sdf.parse(localTime);//get local date
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date getDateTimeSimpleDateFormat(String str) {
        Date date = null;
        if (str == null) return null;

        try {
            date = mDateformatDateTimeType1.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date getOrderDateTimestamp(String str) {
        Date date = null;
        if (str == null) return null;

        try {
            date = mDateformatDateOrderTimeType.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date simpleDateFormat(String str) {
        Date date = null;
        if (str == null) return null;

        try {
            date = mDateformatDateType1.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getOrderDateFormat(Date date) {
        if (date == null) return null;

        return mDateformatOrderDateType.format(date);
    }

    public static String getSimpleDisplayDateFormat(Date date) {
        if (date == null) return null;

        return mDateformatDateType2.format(date);
    }

    public static String getDisplayDateFormat(Date date) {
        if (date == null) return null;

        return mDateformatDateType1.format(date);
    }

    public static String getTimeFormat(Date date) {
        if (date == null) return null;
        return mDateformatTime.format(date);
    }

    public static String getCurrentDate() {
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String updateTimeStamp = s.format(new Date());

        return updateTimeStamp;
    }

    public static Date convertTimestampToDate(Long timestamp) {
        return new Date(timestamp);
    }

    public static String getTimelineDateString(Long timestamp) {
//        Date temp = convertTimestampToDate(timestamp);
//        SimpleDateFormat objDateFormatter = new SimpleDateFormat("M월 d일");
//        SimpleDateFormat objTimeFormatter = new SimpleDateFormat("HH:mm");
        String result = "";

        Date nowTime = new Date();
        long second = (nowTime.getTime() - timestamp) / 1000;
        long minute = second / 60;

        String compareTime = "";
        if (minute / 60 / 24 > 0) { // Day
            compareTime = minute / 60 / 24 + "";
            result = compareTime + "일 전";
        } else {
            if (minute / 60 % 24 > 0) { // Hour
                compareTime = minute / 60 % 24 + "";
                result = compareTime + "시간 전";
            } else {
                if (minute % 60 > 0) { // Minute
                    compareTime = minute % 60 + "";
                    result = compareTime + "분 전";
                } else {
                    compareTime = second % 60 + "";
                    result = compareTime + "초 전";
                }
            }
        }

        return result;
    }


    public static String convertTimestampToString(Long timestamp) {
        Date temp = convertTimestampToDate(timestamp);
        SimpleDateFormat objFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String result = objFormatter.format(temp);
        return result;
    }

    public static String convertTimestampToGSString(Long timestamp) {
        Date temp = convertTimestampToDate(timestamp);
        SimpleDateFormat objFormatter = new SimpleDateFormat("yyyy년 MMM dd일 (EEE)", Locale.KOREAN);
        String result = objFormatter.format(temp);
        return result;
    }

    public static String convertTimestampToString2(Long timestamp) {
        Date temp = convertTimestampToDate(timestamp);
        SimpleDateFormat objFormatter = new SimpleDateFormat("yyyy.MM.dd");
        String result = objFormatter.format(temp);
        return result;
    }

    public static String convertTimestampToStringDate(Long timestamp) {
        Date temp = convertTimestampToDate(timestamp);
        SimpleDateFormat objFormatter = new SimpleDateFormat("M월 d일");
        String result = objFormatter.format(temp);
        String m_week = "";
        int d = temp.getDay();
        if (d == 0)
            m_week = "일요일";
        else if (d == 1)
            m_week = "월요일";
        else if (d == 2)
            m_week = "화요일";
        else if (d == 3)
            m_week = "수요일";
        else if (d == 4)
            m_week = "목요일";
        else if (d == 5)
            m_week = "금요일";
        else if (d == 6)
            m_week = "토요일";
        result = result + " " + m_week;

        return result;
    }

    public static int convertWeekValueToInt(int weekValue) {
        int d = weekValue;
        if (d == 1)
            return 6;
        else if (d == 2)
            return 0;
        else if (d == 3)
            return 1;
        else if (d == 4)
            return 2;
        else if (d == 5)
            return 3;
        else if (d == 6)
            return 4;
        else if (d == 7)
            return 5;
        else return 0;
    }

    public static int getWeekDayFromDate(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        return convertWeekValueToInt(dayOfWeek);
    }

    public static String convertTimestampToStringDate2(Long timestamp) {
        Date temp = convertTimestampToDate(timestamp);
        SimpleDateFormat objFormatter = new SimpleDateFormat("M월 d일");
        String result = objFormatter.format(temp);

        return result;
    }

    public static String getDateByMDSH(Long timestamp, String storeDeliveryTime) {
        Date temp = convertTimestampToDate(timestamp);
        SimpleDateFormat objFormatter = new SimpleDateFormat("M월 d일");
        String result = objFormatter.format(temp);
        int gap = Integer.parseInt(storeDeliveryTime);

        int startHour = temp.getHours();
        int calHour = (startHour % 12 == 0) ? 12 : startHour % 12;
        String apm = "";
        if (startHour > 12 && startHour != 24) {
            apm = "오후";
        } else if (startHour == 24) {
            apm = "밤";
        } else if (startHour == 12) {
            apm = "낮";
        } else {
            apm = "오전";
        }
        result += " " + apm + " " + calHour + "시~" + (calHour + gap) + "시";
        //result += " " + apm + " " + calHour + "시 경";

        return result;
    }

    public static String getDateByMDSH_onlyTime(Long timestamp, String storeDeliveryTime) {
        Date temp = convertTimestampToDate(timestamp);
        //SimpleDateFormat objFormatter = new SimpleDateFormat("M월 d일");
        String result = "";//objFormatter.format(temp);
        int gap = Integer.parseInt(storeDeliveryTime);

        int startHour = temp.getHours();
        int calHour = (startHour % 12 == 0) ? 12 : startHour % 12;
        String apm = "";
        if (startHour > 12 && startHour != 24) {
            apm = "오후";
        } else if (startHour == 24) {
            apm = "밤";
        } else if (startHour == 12) {
            apm = "낮";
        } else {
            apm = "오전";
        }
        result = apm + " " + calHour + "시~" + (calHour + gap) + "시";
        //result += " " + apm + " " + calHour + "시 경";

        return result;
    }

    public static String convertTimestampToStringTime(Long timestamp, int duration) {
        Date temp = convertTimestampToDate(timestamp);
        int startHour = temp.getHours();
        int calHour = (startHour % 12 == 0) ? 12 : startHour % 12;
        //SimpleDateFormat objFormatter = new SimpleDateFormat("HH");
        String result = "";//objFormatter.format(temp);

        String apm = "";
        if (startHour > 12 && startHour != 24) {
            apm = "오후";
        } else if (startHour == 24) {
            apm = "밤";
        } else if (startHour == 12) {
            apm = "낮";
        } else {
            apm = "오전";
        }
        result = apm + " " + calHour + "시 ~ " + (calHour + 1*duration) + "시";

        return result;
    }

    public static String convertTimestampToStringTime2(Long timestamp) {
        Date temp = convertTimestampToDate(timestamp);
        int startHour = temp.getHours();
        int calHour = (startHour % 12 == 0) ? 12 : startHour % 12;
        int startMinute = temp.getMinutes();
        //SimpleDateFormat objFormatter = new SimpleDateFormat("HH");
        String result = "";//objFormatter.format(temp);

        String apm = "";
        if (startHour > 12 && startHour != 24) {
            apm = "오후";
        } else if (startHour == 24) {
            apm = "밤";
        } else if (startHour == 12) {
            apm = "낮";
        } else {
            apm = "오전";
        }
        result = apm + " " + calHour + ":" + ((startMinute > 10) ? startMinute : "0" + startMinute);

        return result;
    }

    public static String getComparePastTimeFromNow(long _timeStamp) {
        Date nowTime = new Date();
        long second = (_timeStamp - nowTime.getTime()) / 1000;
        long minute = second / 60;

        String compareTime = "";
        if (minute / 60 / 24 > 0) { // Day
            compareTime = minute / 60 / 24 + "d";
        } else {
            if (minute / 60 % 24 > 0) { // Hour
                compareTime = minute / 60 % 24 + "h";
            } else {
                if (minute % 60 > 0) { // Minute
                    compareTime = minute % 60 + "m";
                }
//				else {
//					compareTime = second % 60 + "초";
//				}
            }
        }

        return compareTime;
    }

    public static long getMinuteComparePastTimeFromNow(long _timeStamp) {
        Date nowTime = new Date();
        return (_timeStamp - nowTime.getTime()) / 1000 / 60;
    }

    public static Date getDate(int year, int month, int date, int hour) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, date, hour, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
