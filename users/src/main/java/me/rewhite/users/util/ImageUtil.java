package me.rewhite.users.util;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;

import java.util.Arrays;

import me.rewhite.users.zxing.BarcodeFormat;
import me.rewhite.users.zxing.MultiFormatWriter;
import me.rewhite.users.zxing.WriterException;
import me.rewhite.users.zxing.common.BitMatrix;

/**
 * Created by marines on 2017. 3. 23..
 */

public class ImageUtil {
    public static Bitmap createBarcodeBitmap(String data, int width, int height) throws WriterException {
        MultiFormatWriter writer = new MultiFormatWriter();
        String finalData = Uri.encode(data);

        Log.e("ImageUtil create", data + "");
        // Use 1 as the height of the matrix as this is a 1D Barcode.
        //BitMatrix bm = writer.encode(finalData, BarcodeFormat.CODE_128, width, 1);
        BitMatrix bm = writer.encode(finalData, BarcodeFormat.CODE_128, width, 1);
        int bmWidth = bm.getWidth();

        Bitmap imageBitmap = Bitmap.createBitmap(bmWidth, height, Bitmap.Config.ARGB_8888);

        for (int i = 0; i < bmWidth; i++) {
            // Paint columns of width 1
            int[] column = new int[height];
            Arrays.fill(column, bm.get(i, 0) ? Color.BLACK : Color.WHITE);
            imageBitmap.setPixels(column, 0, 1, i, 0, 1, height);
        }

        return imageBitmap;
    }
}
