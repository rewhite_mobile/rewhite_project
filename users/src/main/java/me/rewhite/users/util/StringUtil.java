package me.rewhite.users.util;

public class StringUtil {

	protected static final byte[] Hexhars = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

	public static boolean isEmpty(String paramString) {
		if (paramString == null) ;
		while ((paramString.trim().length() == 0) || ("null".equals(paramString.trim())))
			return true;
		return false;
	}

	public static boolean isNotEmpty(String paramString) {
		return !isEmpty(paramString);
	}

	public static boolean isNullOrEmpty(final String paramString) {
		//return (s == null) || (s.length() == 0);
		if (paramString == null){
			return true;
		}else if((paramString.trim().length() == 0) || ("null".equals(paramString.trim())) || ("".equals(paramString.trim()))){
			return true;
		}
		return false;
	}

	public static boolean isBlank(final CharSequence cs) {
		int strLen;
		if (cs == null || (strLen = cs.length()) == 0) {
			return true;
		}
		for (int i = 0; i < strLen; i++) {
			if (Character.isWhitespace(cs.charAt(i)) == false) {
				return false;
			}
		}
		return true;
	}

	public static boolean isNotBlank(final CharSequence cs) {
		return !isBlank(cs);
	}
/*
	public static List<String> splitForList(String paramString) {
		if ((paramString == null) || (isEmpty(paramString))) return new ArrayList();
		return Arrays.asList(paramString.split(","));
	}

	public static String substringForLast(String paramString, int paramInt) {
		if ((paramString != null) && (paramString.length() > 0)) paramString = paramString.substring(0, paramInt + paramString.length());
		return paramString;
	}

	public static String toHexString(byte[] paramArrayOfByte) {
		StringBuilder localStringBuilder = new StringBuilder(2 * paramArrayOfByte.length);
		for (int i = 0;; i++) {
			if (i >= paramArrayOfByte.length) return localStringBuilder.toString();
			int j = 0xFF & paramArrayOfByte[i];
			localStringBuilder.append((char) Hexhars[(j >> 4)]);
			localStringBuilder.append((char) Hexhars[(j & 0xF)]);
		}
	}

	public static String trimToEmpty(String paramString) {
		if (paramString == null) return "";
		return paramString.trim();
	}*/
}
