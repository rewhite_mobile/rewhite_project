package me.rewhite.users.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.androidquery.AQuery;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.util.SharedPreferencesUtility;

public class ReviewFacebookActivity extends BaseActivity {

    AQuery aq;
    Context ctx = this;
    CallbackManager callbackManager;
    ShareDialog shareDialog;

    String shareString = "스마트폰으로 예약하면\n" +
            "엄선된 세탁 장인이 직접 수거배달을!\n" +
            "추천인코드 : %s";
    private final String weblink = "http://smarturl.it/rewhiteapp_referral";//https://www.rewhite.me?referral="+ SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.SHARE_CODE);

    String nickName = "";
    String shareCode = "";
    String webLinkString = "http://www.rewhite.me";
    String imageUrlString = "https://img.rewhite.me/www/assets/images/invite_friend.png";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_facebook);

        aq = new AQuery(this);

        shareCode = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.SHARE_CODE);
        nickName = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        // this part is optional
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {
                Log.e("facebook share", "on success : " + result.getPostId());

                closeActivity();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        aq.id(R.id.btn_facebook).clicked(this, "shareViaFacebook");
        aq.id(R.id.btn_close).clicked(this, "closeAction");
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void closeActivity(){
        finish();
    }

    public void closeAction(View button){
        finish();
    }


    public void shareViaFacebook(View button) {
        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("기타")
                .setAction("추천하기")
                .setLabel("페이스북포스팅")
                .build());
        AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
        logger.logEvent("Recommend");

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentDescription(
                            String.format(shareString, shareCode))
                    .setImageUrl(Uri.parse(imageUrlString))
                    .setContentUrl(Uri.parse(weblink))
                    .setShareHashtag(new ShareHashtag.Builder()
                            .setHashtag("#리화이트").build())
                    .build();

            shareDialog.show(linkContent);
        }

    }
}
