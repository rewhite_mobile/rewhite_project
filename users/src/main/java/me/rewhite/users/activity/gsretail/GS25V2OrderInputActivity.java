package me.rewhite.users.activity.gsretail;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.PickupType;
import me.rewhite.users.fragment.GS25V2OrderInputStep1Fragment;
import me.rewhite.users.fragment.GS25V2OrderInputStep2Fragment;
import me.rewhite.users.fragment.GS25V2OrderInputStep3Fragment;
import me.rewhite.users.fragment.IFragment;
import me.rewhite.users.util.DUtil;

public class GS25V2OrderInputActivity extends BaseActivity {

    private static final String TAG = "GS25V2OrderInput";
    public static final String FRAGMENT_STEP1 = "gs_order_step1";
    public static final String FRAGMENT_STEP2 = "gs_order_step2";
    public static final String FRAGMENT_STEP3 = "gs_order_step3";

    private Fragment mContent;
    private boolean mIsTouchDisable; // 연속 터치로 생기는 이슈 막기 위함
    private String currentFragname;

    private Context mContext;
    private AQuery aq;
    ImageButton button = null;

    JSONObject storeInfo;
    public String partnerId = "7";
    public String internalStoreId = "";

    String partnerStoreId = "1002";
    String partnerStoreName = "";
    String priceGroupId = "";
    String storeId = "1002";
    int pickupMode = PickupType.PICKUPTYPE_GS25;

    public int count_unit = 0;

    public static final int REQUEST_CONTENT_INPUT = 1;
    public static final int REQUEST_ORDER_CONFIRM = 2;
    public String request_content_string = "";

    static Dialog unitDialog;
    static Dialog packDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gs25_v2_order_input);

        //DUtil.alertShow(this, "편의점 점원에게 접수봉투를 받아서 수량을 입력해주세요.");

        aq = new AQuery(this);

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        mContext = this;

        Intent intent = getIntent();
        if(intent != null){
            try {
                storeInfo = new JSONObject(intent.getStringExtra("storeInfo"));
                internalStoreId = intent.getStringExtra("internalStoreId");
                partnerId = intent.getStringExtra("partnerId");

                Log.w("OrderInput v2", internalStoreId + " / " + partnerId);

                storeId = storeInfo.getString("storeId");
                partnerStoreId = storeInfo.getString("partnerStoreId");
                partnerStoreName = storeInfo.getString("partnerStoreName");
                priceGroupId = storeInfo.getString("priceGroupId");
                getConvStoreInfo(partnerStoreId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        //convStoreId
//        int washingDays = 0;
//        try {
//            washingDays = storeInfo.getJSONObject("storeInformation").getInt("partnerWashingDays");
//            aq.id(R.id.text_complete_date).text("수거접수 후 " + washingDays + "일 이내");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        int intStoreId = Integer.parseInt(internalStoreId);
        if(intStoreId >= 20000){
            aq.id(R.id.btn_next_act).text("담당 직원이면 터치해주세요");
        }else{
            aq.id(R.id.btn_next_act).text("편의점 직원이면 터치해주세요");
        }

        aq.id(R.id.btn_submit).clicked(this, "submitClicked").enabled(false);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
        aq.id(R.id.btn_close).clicked(this, "closePermanentClicked");
        aq.id(R.id.btn_next_act).clicked(this, "nextActProcess");

        initilize();
    }

    public void closePermanentClicked(View button) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setCancelable(true).setMessage("주문접수 중인데, 중단하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void closeClicked(View button) {
        onBackPressed();
    }

    private void getConvStoreInfo(String convStoreId){
        // 해당 편의점ID로 매칭된 세탁소정보 조회

        // 완료예정일
        aq.id(R.id.text_complete_date).text("");
        aq.id(R.id.conv_store_name).text(partnerStoreName);
    }

//    public void clickPriceTable(View button){
//        Intent intent = new Intent(this, PriceActivity.class);
//        intent.putExtra("partnerId", "7");
//        intent.putExtra("priceGroupId", priceGroupId);
//        intent.putExtra("pickupMode", pickupMode);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//                | Intent.FLAG_ACTIVITY_SINGLE_TOP
//                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//
//        startActivity(intent);
//    }



    private void initilize() {
        showFragment(FRAGMENT_STEP1);
    }

    public void closeEndPopup(View button){
        aq.id(R.id.comp_layout).gone();
        compLayoutVisible = false;
    }

    public void nextActProcess(View button){
        if(count_unit > 0 ){
            Intent intent = new Intent(this, GS25V2OrderInputClientActivity.class);
            // DEBUG

            try {
                JSONObject temp = new JSONObject();
                temp.put("partnerId", partnerId);
                temp.put("partnerStoreId", partnerStoreId);
                temp.put("pickupQuantity", count_unit);
                temp.put("internalStoreId", internalStoreId);
                //temp.put("packQuantity", count_pack);
                temp.put("pickupRequestMessage", request_content_string);
                intent.putExtra("orderInfo", temp.toString());

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
                this.finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    boolean compLayoutVisible = false;
    public void showEndPopup(){
        aq.id(R.id.comp_layout).visible();
        compLayoutVisible = true;
    }

    public void showFragment(String fragmentName) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);
        showFragment(fragmentName, null);
    }



    public void showFragment(String fragmentName, Bundle args) {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(fragmentName);

        if (FRAGMENT_STEP1.equals(fragmentName)) {
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new GS25V2OrderInputStep1Fragment();
            aq.id(R.id.gs25_top_image).image(R.mipmap.img_gs25_step2_top_01);
            aq.id(R.id.text_title).text("세탁물 접수" );
        } else if (FRAGMENT_STEP2.equals(fragmentName)) {
            fragment = new GS25V2OrderInputStep2Fragment();
            aq.id(R.id.gs25_top_image).image(R.mipmap.img_gs25_step2_top_02);
            aq.id(R.id.text_title).text("요청사항 입력");
        } else if (FRAGMENT_STEP3.equals(fragmentName)) {
            fragment = new GS25V2OrderInputStep3Fragment();
            aq.id(R.id.gs25_top_image).image(R.mipmap.img_gs25_step2_top_03);
            aq.id(R.id.text_title).text("유의사항 확인 및 약관동의");

        }

        Log.e(TAG, "[GS25V2 OrderInput] Create : " + fragmentName);

        if (null != args) {
            fragment.setArguments(args);
        }
        ft.setCustomAnimations(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        ft.replace(R.id.content_frame, fragment, fragmentName);
        ft.addToBackStack(fragmentName);
        ft.commitAllowingStateLoss();
        //ft.commit();
    }


    public boolean getTouchDisable() {
        Log.d(TAG, "[getTouchDisable]");
        return mIsTouchDisable;
    }

    public void setTouchDisable(boolean isTouchDisable) {
        Log.d(TAG, "[setTouchDisable]" + isTouchDisable);
        mIsTouchDisable = isTouchDisable;
    }

    @Override
    public void onBackPressed() {
        if(compLayoutVisible){
            aq.id(R.id.comp_layout).gone();
            compLayoutVisible = false;
        }else{
            FragmentManager fm = getSupportFragmentManager();
            DUtil.Log(TAG, "[onBackPressed] Back Stack Entry Count : " + fm.getBackStackEntryCount());

            if (fm.getBackStackEntryCount() >= 1) {

                String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
                IFragment fragment = (IFragment) fm.findFragmentByTag(tag);
                if (false == fragment.onBackPressed()) {
                    currentFragname = null;

                    if (fm.getBackStackEntryCount() == 1) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                        alertDialogBuilder.setCancelable(true).setMessage("주문접수 중인데, 중단하시겠어요?")
                                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else {
                        setTouchDisable(true);
                        //clearCaptureView();

                        fm.popBackStack();
                    }

                    if (fm.getBackStackEntryCount() == 2) {
                        aq.id(R.id.gs25_top_image).image(R.mipmap.img_gs25_step2_top_01);
                        aq.id(R.id.text_title).text("세탁물 접수" );

                    }else if (fm.getBackStackEntryCount() == 3) {
                        aq.id(R.id.gs25_top_image).image(R.mipmap.img_gs25_step2_top_02);
                        aq.id(R.id.text_title).text("요청사항 입력");
                    }else if (fm.getBackStackEntryCount() == 4) {
                        aq.id(R.id.gs25_top_image).image(R.mipmap.img_gs25_step2_top_03);
                        aq.id(R.id.text_title).text("유의사항 확인 및 약관동의");
                    }
                }
            } else {
                super.onBackPressed();
            }
        }


    }

}
