package me.rewhite.users.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ListView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.adapter.StoreChoiceListViewAdapter;
import me.rewhite.users.adapter.StoreListItem;

public class StoreChoiceScreen extends AppCompatActivity {

    private AQuery aq;
    JSONArray storeInfo;
    private ArrayList<StoreListItem> myStoreData;
    private StoreChoiceListViewAdapter myStoreAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        setContentView(R.layout.fragment_store_choice);

        aq = new AQuery(this);

        Intent intent = getIntent();
        if (intent != null) {

            try {
                storeInfo = new JSONArray(intent.getStringExtra("storeInfo"));
                Log.e("Store Data : ", storeInfo.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        aq.id(R.id.btn_close).clicked(this, "noClicked");

        try {
            initialize();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initialize() throws JSONException {
        // locationListView
        ListView storeListView = (ListView) findViewById(R.id.listView);
        myStoreData = new ArrayList<>();

        //JSONArray locationItems = jsondata.getJSONArray("data");
        //SharedPreferencesUtility.setLocationData(data);

        for (int i = 0; i < storeInfo.length(); i++) {
            int storeId = storeInfo.getJSONObject(i).getInt("storeId");
            String storeName = storeInfo.getJSONObject(i).getString("storeDisplayName");
            String storeImage1 = storeInfo.getJSONObject(i).getString("storeImage1");
            String storeAddress1 = storeInfo.getJSONObject(i).getString("storeAddress1");
            String storeAddress2 = storeInfo.getJSONObject(i).getString("storeAddress2");
            String storeClosingDay = storeInfo.getJSONObject(i).getString("storeClosingDay");
            String storeBusinessHours = storeInfo.getJSONObject(i).getString("storeBusinessHours");
            double distance = storeInfo.getJSONObject(i).getDouble("distance");
            int storeRateScore = storeInfo.getJSONObject(i).getInt("storeRateScore");
            int orderCountForUser = storeInfo.getJSONObject(i).getInt("orderCountForUser");
            String storeAvailableService = storeInfo.getJSONObject(i).getString("storeAvailableService");

            if(storeName.length() > 8){
                storeName = storeName.substring(0,7) + "...";
            }

            StoreListItem aItem = new StoreListItem(storeId, storeName, storeImage1, storeAddress1, storeAddress2, storeClosingDay, storeBusinessHours, distance, storeRateScore, orderCountForUser, storeAvailableService);
            myStoreData.add(aItem);
        }

        myStoreAdapter = new StoreChoiceListViewAdapter(this, R.layout.store_choice_list_item, myStoreData);
        myStoreAdapter.notifyDataSetChanged();

        storeListView.setAdapter(myStoreAdapter);
    }

    public void noClicked(View button) {
        Log.e("okCilcked", "NO");
        setResult(Activity.RESULT_CANCELED);
        finish();
    }


    public void changeStore(int position) {
        try {
            Intent resultData = new Intent();
            resultData.putExtra("currentStoreInfo", storeInfo.getJSONObject(position).toString());
            resultData.putExtra("result", true);
            setResult(Activity.RESULT_OK, resultData);
            finish();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
