package me.rewhite.users.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.RatingBar;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.ToastUtility;

public class ReviewActivity extends BaseActivity implements RatingBar.OnRatingBarChangeListener{

    String orderId;
    String storeId;
    AQuery aq;
    RatingBar reviewRatingBar;
    int ratingValue = 0;
    String ratingMessage = "";
    Context ctx = this;
    JSONObject orderInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        Intent intent = getIntent();
        if (intent != null) {
            orderId = intent.getStringExtra("orderId");
            storeId = intent.getStringExtra("storeId");
            if("".equals(storeId)){
                loadOrderInfo(orderId);
            }
        }
        aq = new AQuery(this);

        reviewRatingBar = (RatingBar)findViewById(R.id.ratingBar);
        if (reviewRatingBar != null) {
            reviewRatingBar.setOnRatingBarChangeListener(ReviewActivity.this);
        }

        aq.id(R.id.btn_submit).clicked(this, "submitAction");

        aq.id(R.id.btn_message_01).clicked(this, "selectMessage").tag(1);
        aq.id(R.id.btn_message_02).clicked(this, "selectMessage").tag(2);
        aq.id(R.id.btn_message_03).clicked(this, "selectMessage").tag(3);
        aq.id(R.id.btn_message_04).clicked(this, "selectMessage").tag(4);
        aq.id(R.id.btn_message_05).clicked(this, "selectMessage").tag(5);
        aq.id(R.id.btn_message_06).clicked(this, "selectMessage").tag(6);
        aq.id(R.id.btn_message_07).clicked(this, "selectMessage").tag(7);
        aq.id(R.id.btn_message_08).clicked(this, "selectMessage").tag(8);
    }


    private void loadOrderInfo(String orderId){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_DETAIL, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            orderInfo = jsondata.getJSONObject("data");
                            storeId = orderInfo.getString("storeId");
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        final int numStarts = ratingBar.getNumStars();

        if(rating == 0){
            aq.id(R.id.rating_image).image(R.mipmap.review_pick_image_00);
        }else if(rating == 1){
            aq.id(R.id.rating_image).image(R.mipmap.review_pick_image_01);
        }else if(rating == 2){
            aq.id(R.id.rating_image).image(R.mipmap.review_pick_image_02);
        }else if(rating == 3){
            aq.id(R.id.rating_image).image(R.mipmap.review_pick_image_03);
        }else if(rating == 4){
            aq.id(R.id.rating_image).image(R.mipmap.review_pick_image_04);
        }else if(rating == 5){
            aq.id(R.id.rating_image).image(R.mipmap.review_pick_image_05);
        }

        ratingValue = (int)rating;
    }

    int selectionIdx = 0;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void selectMessage(View button){
        int tag = (int)button.getTag();
        selectionIdx = tag;

        aq.id(R.id.btn_message_01).background(R.drawable.blue_button);//.setBackgroundTintList(ColorStateList.valueOf(0xffebebeb));
        aq.id(R.id.btn_message_02).background(R.drawable.blue_button);//.setBackgroundTintList(ColorStateList.valueOf(0xffebebeb));
        aq.id(R.id.btn_message_03).background(R.drawable.blue_button);//.setBackgroundTintList(ColorStateList.valueOf(0xffebebeb));
        aq.id(R.id.btn_message_04).background(R.drawable.blue_button);//.setBackgroundTintList(ColorStateList.valueOf(0xffebebeb));
        aq.id(R.id.btn_message_05).background(R.drawable.blue_button);//.setBackgroundTintList(ColorStateList.valueOf(0xffebebeb));
        aq.id(R.id.btn_message_06).background(R.drawable.blue_button);//.setBackgroundTintList(ColorStateList.valueOf(0xffebebeb));
        aq.id(R.id.btn_message_07).background(R.drawable.blue_button);//.setBackgroundTintList(ColorStateList.valueOf(0xffebebeb));
        aq.id(R.id.btn_message_08).background(R.drawable.blue_button);//.setBackgroundTintList(ColorStateList.valueOf(0xffebebeb));
        aq.id(R.id.input_area).gone();

        switch(tag){
            case 1:
                //aq.id(R.id.btn_message_01).getButton().setBackground(R.drawable.blue_button_selected).setSelected(true); //.getButton().setBackgroundTintList(ColorStateList.valueOf(0xff2b8aea));
                //aq.id(R.id.btn_message_01).getButton()
                aq.id(R.id.btn_message_01).background(R.drawable.blue_button_selected);
                ratingMessage = "수거/배달 시간약속";
                break;
            case 2:
                aq.id(R.id.btn_message_02).background(R.drawable.blue_button_selected); //.setBackgroundTintList(ColorStateList.valueOf(0xff2b8aea));
                ratingMessage = "세탁 불만족";
                break;
            case 3:
                aq.id(R.id.btn_message_03).background(R.drawable.blue_button_selected); //.setBackgroundTintList(ColorStateList.valueOf(0xff2b8aea));
                ratingMessage = "배달원 불친절";
                break;
            case 4:
                aq.id(R.id.btn_message_04).background(R.drawable.blue_button_selected);//.setBackgroundTintList(ColorStateList.valueOf(0xff2b8aea));
                ratingMessage = "요금이 비쌈";
                break;
            case 5:
                aq.id(R.id.btn_message_05).background(R.drawable.blue_button_selected); //.setBackgroundTintList(ColorStateList.valueOf(0xff2b8aea));
                ratingMessage = "세탁기간 오래걸림";
                break;
            case 6:
                aq.id(R.id.btn_message_06).background(R.drawable.blue_button_selected); //.setBackgroundTintList(ColorStateList.valueOf(0xff2b8aea));
                ratingMessage = "앱사용이 불편함";
                break;
            case 7:
                aq.id(R.id.btn_message_07).background(R.drawable.blue_button_selected); //.setBackgroundTintList(ColorStateList.valueOf(0xff2b8aea));
                ratingMessage = "개선사항 없음";
                break;
            case 8:
                aq.id(R.id.btn_message_08).background(R.drawable.blue_button_selected); //.setBackgroundTintList(ColorStateList.valueOf(0xff2b8aea));
                aq.id(R.id.input_area).visible();
                break;
        }

    }

    public void submitAction(View button){
        if(ratingValue == 0){
            DUtil.alertShow(this, "별점을 선택해주세요.");
            return;
        }else if(selectionIdx == 0){
            DUtil.alertShow(this, "개선사항을 선택해주세요.");
            return;
        }

        if(selectionIdx == 8){
            ratingMessage = aq.id(R.id.input_text).getText().toString();
            if(ratingMessage.length() < 3){
                DUtil.alertShow(this, "3글자 이상 입력해주세요.");
                return;
            }
        }
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("storeId", storeId);
        params.put("rateScore", ratingValue);
        params.put("rateMessage", ratingMessage);
        params.put("k", 1);

        Log.i("param", params.toString());

        NetworkClient.post(Constants.ORDER_REVIEW, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_REVIEW, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_REVIEW, result);

                    JSONObject jsondata = new JSONObject(result);
                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("주문")
                                .setAction("리뷰쓰기")
                                //.setLabel("")
                                .build());
                        AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
                        logger.logEvent("Order Review");

                        if(ratingValue <= 3){
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
                            alertDialogBuilder.setCancelable(false).setMessage("소중한 의견 주셔서 대단히 감사합니다.\n항상 별 5개를 받을 수 있도록 노력하겠습니다.\n(*포인트는 익일 일괄 적립될 예정)")
                                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent resultData = new Intent();
                                            resultData.putExtra("RESULT", "N");
                                            setResult(Activity.RESULT_OK, resultData);
                                            finish();
                                        }
                                    });
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }else{
                            Intent resultData = new Intent();
                            resultData.putExtra("RESULT", "Y");
                            setResult(Activity.RESULT_OK, resultData);
                            finish();
                        }





                    } else {
                        ToastUtility.show(ctx,  jsondata.getString("message"), Toast.LENGTH_LONG);
                    }

                } catch (UnsupportedEncodingException | JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }
}
