package me.rewhite.users.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.PickupType;
import me.rewhite.users.fragment.DeliveryReqFragmentV2;
import me.rewhite.users.fragment.IFragment;
import me.rewhite.users.fragment.OrderSummaryFragment;
import me.rewhite.users.fragment.PickupReqFragmentV2;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;

public class OrderActivity extends BaseActivity implements PickupReqFragmentV2.OnFragmentInteractionListener {

    private final static String TAG = OrderActivity.class.getSimpleName();
    public static final String FRAGMENT_PICKUP = "order_pickup";
    public static final String FRAGMENT_DELIVERY = "order_delivery";
    public static final String FRAGMENT_SUMMARY = "order_summary";

    public final Context mCtx = this;
    AQuery aq;
    public JSONObject storeInfo;
    public String storeId;
    public long pickupReqTime = 0;
    public long deliveryReqTime = 0;
    public String orderRequestItem;
    public String orderRequestString;

    public int pickupMode;
    public String addressSeq;
    public String addressDetailName;
    public String addressName;
    public String storeClosingDay;
    public String storeDayoff;
    public String storeAvailableService;
    public boolean isRepairAvailable = false;
    public int storeDeliveryDuration;
    public int storeSaleStartTime = 6;
    public int washingDays = 3;
    public int washingTimes = 5;
    private Fragment mContent;


    private boolean mIsTouchDisable; // 연속 터치로 생기는 이슈 막기 위함
    private boolean mFlagFinish;
    private Handler mHandler;
    private Runnable mTask;
    private String currentFragname;

    private void checkAvailableService(String value){
        if(checkAvailableBool(value)){

        }else{
            Intent intent = new Intent(this, DisabledScreen.class);
            intent.putExtra("content", value);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    private boolean checkAvailableBool(String value){
        String[] sas = value.split("\\,");
        boolean isAvailable1 = false;
        boolean isAvailable2 = false;
        boolean isAvailable3 = false;
        boolean isAvailable4 = false;

        for(int i = 0; i < sas.length; i++){
            if("197".equals(sas[i])){
                // 운동화 취급안함
                isAvailable1 = true;
            }else if("199".equals(sas[i])){
                // 이불 취급안함
                isAvailable2 = true;
            }else if("200".equals(sas[i])){
                // 카페트 취급안함
                isAvailable3 = true;
            }else if("203".equals(sas[i])){
                // 커텐 취급안함
                isAvailable4 = true;
            }
        }

        if(isAvailable1 && isAvailable2 && isAvailable3 && isAvailable4){
            return true;
        }else{
            return false;
        }
    }

    String deliveryInfo;
    String deliveryDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");


        aq.id(R.id.title_text).typeface(CommonUtility.getNanumBarunTypeface());

        Intent intent = getIntent();
        if (intent != null) {
            try {
                storeInfo = new JSONObject(intent.getStringExtra("storeInfo"));

                if(storeInfo == null){
                    finish();
                }

                storeId = storeInfo.getString("storeId");
                addressSeq = intent.getStringExtra("addressSeq");
                addressDetailName = intent.getStringExtra("addressDetailName");
                addressName = intent.getStringExtra("addressName");
                pickupMode = intent.getIntExtra("pickupMode", 0);
                storeClosingDay = storeInfo.getString("storeClosingDay");
                storeDayoff = storeInfo.getString("storeDayoff");
                washingDays = storeInfo.getInt("washingDays");
                storeAvailableService = storeInfo.getString("storeAvailableService");

                deliveryInfo = storeInfo.getString("storeDeliveryTime");
                deliveryDuration = deliveryInfo.split("\\|")[0];
                storeDeliveryDuration = Integer.parseInt(deliveryDuration);

                if(pickupMode == PickupType.PICKUPTYPE_TODAY || pickupMode == PickupType.PICKUPTYPE_TOMORROW){

                }else{
                    checkAvailableService(storeAvailableService);
                }


                String storeAvailableItem = storeInfo.getString("availableOrder");
                try{
                    if(pickupMode == PickupType.PICKUPTYPE_TODAY){
                        washingDays = 0;
                        washingTimes = 5;
                        //aq.id(R.id.btn_repair_price).gone();
                        aq.id(R.id.btn_price).clicked(this, "priceTableClicked").typeface(CommonUtility.getNanumBarunTypeface()).tag(PickupType.PICKUPTYPE_TODAY);
                    }else if(pickupMode == PickupType.PICKUPTYPE_TOMORROW){
                        washingDays = 1;
                        washingTimes = 10;
                        //aq.id(R.id.btn_repair_price).gone();
                        aq.id(R.id.btn_price).clicked(this, "priceTableClicked").typeface(CommonUtility.getNanumBarunTypeface()).tag(PickupType.PICKUPTYPE_TOMORROW);
                    }else{
                        aq.id(R.id.btn_price).clicked(this, "priceTableClicked").typeface(CommonUtility.getNanumBarunTypeface()).tag(PickupType.PICKUPTYPE_CLEAN);
                        // 수선가능여부 판단
                        if('1' == storeAvailableItem.charAt(2)){
                            isRepairAvailable = true;
                            //aq.id(R.id.btn_repair_price).clicked(this, "priceTableClicked").typeface(CommonUtility.getNanumBarunTypeface()).tag(1);
                        }else{
                            isRepairAvailable = false;
                            //aq.id(R.id.btn_repair_price).gone();
                        }
                    }

                }catch (NullPointerException e){
                    Log.e("NullPointerException", e.getMessage());
                    isRepairAvailable = false;
                    aq.id(R.id.btn_repair_price).gone();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        mHandler = new Handler();
        mTask = new Runnable() {

            @Override
            public void run() {
                mFlagFinish = false;
            }
        };

        initilize();
    }

    public void priceTableClicked(View button) {
        int tag = (int)button.getTag();

        Intent intent = new Intent(this, PriceActivity.class);
        intent.putExtra("storeId", storeId);
        intent.putExtra("pickupMode", tag);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);

    }

    public void setTitle(String _title) {
        aq.id(R.id.title_text).text(_title);
    }

    public void closeClicked(View button) {
        onBackPressed();
    }

    private void initilize() {
        showFragment(FRAGMENT_PICKUP);
    }

    public void showFragment(String fragmentName) {
        //Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);
        showFragment(fragmentName, null);
    }

    public void showFragment(String fragmentName, Bundle args) {
        //Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(fragmentName);

        if (FRAGMENT_PICKUP.equals(fragmentName)) {
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new PickupReqFragmentV2();
            String titleStr = "";
            if(pickupMode == PickupType.PICKUPTYPE_TODAY){
                titleStr = "수거 신청 (당일배송)";
            }else if(pickupMode == PickupType.PICKUPTYPE_TOMORROW){
                titleStr = "수거 신청 (익일배송)";
            }else{
                titleStr = "수거 신청";
            }
            aq.id(R.id.title_text).text(titleStr);
        } else if (FRAGMENT_DELIVERY.equals(fragmentName)) {
            fragment = new DeliveryReqFragmentV2();
            String titleStr = "";
            if(pickupMode == PickupType.PICKUPTYPE_TODAY){
                titleStr = "배송 신청 (당일배송)";
            }else if(pickupMode == PickupType.PICKUPTYPE_TOMORROW){
                titleStr = "배송 신청 (익일배송)";
            }else{
                titleStr = "배송 신청";
            }
            aq.id(R.id.title_text).text(titleStr);
        } else if (FRAGMENT_SUMMARY.equals(fragmentName)) {
            fragment = new OrderSummaryFragment();
            aq.id(R.id.title_text).text("주문 확인");
            //aq.id(R.id.btn_price).gone();
        }

        Log.e(TAG, "[PickupReqFragment] Create : " + fragmentName);

        if (null != args) {
            fragment.setArguments(args);
        }
        ft.setCustomAnimations(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        ft.replace(R.id.content_frame, fragment, fragmentName);
        ft.addToBackStack(fragmentName);
        ft.commitAllowingStateLoss();
        //ft.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public boolean getTouchDisable() {
        //Log.d(TAG, "[getTouchDisable]");
        return mIsTouchDisable;
    }

    public void setTouchDisable(boolean isTouchDisable) {
        //Log.d(TAG, "[setTouchDisable]" + isTouchDisable);
        mIsTouchDisable = isTouchDisable;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        DUtil.Log(TAG, "[onBackPressed] Back Stack Entry Count : " + fm.getBackStackEntryCount());

        if (fm.getBackStackEntryCount() >= 1) {

            String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
            IFragment fragment = (IFragment) fm.findFragmentByTag(tag);
            if (false == fragment.onBackPressed()) {
                currentFragname = null;

                if (fm.getBackStackEntryCount() == 1) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                    alertDialogBuilder.setCancelable(true).setMessage("신청이 완료되지 않았어요.\n중단하시겠어요?")
                            .setPositiveButton("네", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {
                    setTouchDisable(true);
                    //clearCaptureView();

                    fm.popBackStack();

                }
            }
        } else {
            super.onBackPressed();
        }
        //finish();
        //super.onBackPressed();
    }
}
