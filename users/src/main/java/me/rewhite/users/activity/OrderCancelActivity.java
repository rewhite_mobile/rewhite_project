package me.rewhite.users.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.ToastUtility;

public class OrderCancelActivity extends BaseActivity {

    AQuery aq;
    int selectedIndex = 0;
    String selectedString = "";
    String orderId;
    Context mCtx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_cancel);

        Intent intent = getIntent();
        if (intent != null) {
            orderId = intent.getStringExtra("orderId");
        }

        aq = new AQuery(this);
        aq.id(R.id.btn_01).clicked(this, "choiceClicked").tag(1);
        aq.id(R.id.btn_02).clicked(this, "choiceClicked").tag(2);
        aq.id(R.id.btn_03).clicked(this, "choiceClicked").tag(3);
        aq.id(R.id.btn_04).clicked(this, "choiceClicked").tag(4);

        aq.id(R.id.btn_submit).enabled(false);
        aq.id(R.id.btn_close).clicked(this, "closeClicked");
    }

    public void closeClicked(View button){
        Intent resultData = new Intent();
        //resultData.putExtra("content", orderInfo.toString());
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }

    public void choiceClicked(View button){
        int tag = (int)button.getTag();
        selectedIndex = tag;
        aq.id(R.id.btn_01).image(R.mipmap.btn_order_cancel_normal);
        aq.id(R.id.btn_02).image(R.mipmap.btn_order_cancel_normal);
        aq.id(R.id.btn_03).image(R.mipmap.btn_order_cancel_normal);
        aq.id(R.id.btn_04).image(R.mipmap.btn_order_cancel_normal);

        aq.id(R.id.inputbox).gone();

        switch(tag){
            case 1:
                aq.id(R.id.btn_01).image(R.mipmap.btn_order_cancel_pressed);
                break;
            case 2:
                aq.id(R.id.btn_02).image(R.mipmap.btn_order_cancel_pressed);
                break;
            case 3:
                aq.id(R.id.btn_03).image(R.mipmap.btn_order_cancel_pressed);
                break;
            case 4:
                aq.id(R.id.btn_04).image(R.mipmap.btn_order_cancel_pressed);
                aq.id(R.id.inputbox).visible();
                break;
        }

        aq.id(R.id.btn_submit).image(R.mipmap.btn_ordercancel_submit_enabled).clicked(this, "submitClicked").enabled(true);
    }

    public void submitClicked(View button){
        switch(selectedIndex){
            case 1:
                selectedString = "예약시간을 지킬 수 없어요";
                break;
            case 2:
                selectedString = "요금이 비싸요";
                break;
            case 3:
                selectedString = "앱 사용이 불편해요";
                break;
            case 4:
                selectedString = aq.id(R.id.input_text).getText().toString();
                break;
        }

        cancelAction();
    }


    public void cancelAction(){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("pickupRequestMessage", selectedString);
        params.put("orderStatus", "91");
        params.put("k", 1);

        NetworkClient.post(Constants.ORDER_CANCEL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_CANCEL, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_CANCEL, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("주문")
                                    .setAction("주문취소")
                                    //.setLabel("")
                                    .build());
                            AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                            logger.logEvent("Order Cancel");

                            Intent resultData = new Intent();
                            //resultData.putExtra("content", orderInfo.toString());
                            setResult(Activity.RESULT_OK, resultData);
                            finish();
                        }

                    } else {
                        ToastUtility.show(mCtx, jsondata.getString("message"));
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }
}
