package me.rewhite.users.activity;

import android.os.Bundle;
import android.view.View;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class AlarmSettingActivity extends BaseActivity {

    AQuery aq;
    boolean isAlarm01 = true;
    boolean isAlarm02 = true;
    boolean isAlarm03 = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_setting);

        aq = new AQuery(this);

        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        aq.id(R.id.alarm_01).clicked(this, "alarmClicked").tag(1);
        aq.id(R.id.alarm_02).clicked(this, "alarmClicked").tag(2);
        aq.id(R.id.alarm_03).clicked(this, "alarmClicked").tag(3);

        getNotificationSetting();
    }

    public void closeClicked(View button) {
        finish();
    }

    public void alarmClicked(View button){
        int tag = (int) button.getTag();
        String setValue = "";

        switch (tag) {
            case 1:
                isAlarm01 = !isAlarm01;
                break;
            case 2:
                isAlarm02 = !isAlarm02;
                break;
            case 3:
                isAlarm03 = !isAlarm03;
                break;
        }

        if(isAlarm01){
            aq.id(R.id.icon_check_01).image(R.mipmap.box_sel_black);
            setValue = "1";
        }else{
            aq.id(R.id.icon_check_01).image(R.mipmap.box_def);
            setValue = "0";
        }
        if(isAlarm02){
            aq.id(R.id.icon_check_02).image(R.mipmap.box_sel_black);
            setValue += "1";
        }else{
            aq.id(R.id.icon_check_02).image(R.mipmap.box_def);
            setValue += "0";
        }
        if(isAlarm03){
            aq.id(R.id.icon_check_03).image(R.mipmap.box_sel_black);
            setValue += "1";
        }else{
            aq.id(R.id.icon_check_03).image(R.mipmap.box_def);
            setValue += "0";
        }
        setNotificationSetting(setValue);
    }

    String pushElement = "";
    private void getNotificationSetting(){

        isAlarm01 = true;
        isAlarm02 = true;
        isAlarm03 = true;

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.GET_PUSH_SETTING, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GET_PUSH_SETTING, error.getMessage());
                //dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_PUSH_SETTING, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        pushElement = jsondata.getString("data");
                        for(int i = 0 ; i < 3; i++){
                            if("0".equals(pushElement.substring(i,i+1))){
                                switch(i){
                                    case 0:
                                        isAlarm01 = false;
                                        break;
                                    case 1:
                                        isAlarm02 = false;
                                        break;
                                    case 2:
                                        isAlarm03 = false;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        if(isAlarm01){
                            aq.id(R.id.icon_check_01).image(R.mipmap.box_sel_black);
                        }else{
                            aq.id(R.id.icon_check_01).image(R.mipmap.box_def);
                        }
                        if(isAlarm02){
                            aq.id(R.id.icon_check_02).image(R.mipmap.box_sel_black);
                        }else{
                            aq.id(R.id.icon_check_02).image(R.mipmap.box_def);
                        }
                        if(isAlarm03){
                            aq.id(R.id.icon_check_03).image(R.mipmap.box_sel_black);
                        }else{
                            aq.id(R.id.icon_check_03).image(R.mipmap.box_def);
                        }
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    private void setNotificationSetting(String _value){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("pushSettings", _value);
        params.put("k", 1);
        NetworkClient.post(Constants.SET_PUSH_SETTING, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SET_PUSH_SETTING, error.getMessage());
                //dismissDialog();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.SET_PUSH_SETTING, result);
                    //dismissDialog();

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }
}
