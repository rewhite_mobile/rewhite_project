package me.rewhite.users.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import me.rewhite.users.R;
import me.rewhite.users.adapter.LocationChoiceListViewAdapter;
import me.rewhite.users.adapter.MyLocationListItem;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.PickupType;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.TimeUtil;

public class PaymentNoticeScreen extends BaseActivity {

    private AQuery aq;
    JSONObject content;
    String type;
    long deliveryRequestTime;
    String storeDeliveryTime;
    int pickupMode;

    JSONArray data;
    private ArrayList<MyLocationListItem> myLocationData;
    private LocationChoiceListViewAdapter myLocationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        setContentView(R.layout.fragment_payment_notice);

        aq = new AQuery(this);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                content = new JSONObject(intent.getStringExtra("content"));
                deliveryRequestTime = intent.getLongExtra("deliveryRequestTime", 0);
                type = intent.getStringExtra("type");
                pickupMode = intent.getIntExtra("pickupMode", 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        aq.id(R.id.text_title).text(Html.fromHtml("<B>결제 안내</B>"));
        aq.id(R.id.btn_close).clicked(this, "noClicked");

        try {
            initialize();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initialize() throws JSONException {

        long dTime;
        if("01".equals(type)){
            dTime = deliveryRequestTime;
        }else{
            dTime = content.getLong("deliveryRequestTimeApp");
        }

        String dueDate = TimeUtil.convertTimestampToStringDate2(dTime);


        Calendar dr = Calendar.getInstance();
        dr.setTimeInMillis(dTime);
        if(pickupMode == PickupType.PICKUPTYPE_TODAY){
            dr.add(Calendar.DATE, 0);
        }else if(pickupMode == PickupType.PICKUPTYPE_TOMORROW){
            dr.add(Calendar.DATE, -1);
        }else if(pickupMode == PickupType.PICKUPTYPE_PARCEL){
            dr.add(Calendar.DATE, -4);
        }else{
            dr.add(Calendar.DATE, -1);
        }

        int month = dr.get(Calendar.MONTH);
        int day = dr.get(Calendar.DATE);

        aq.id(R.id.impact_text).text((month + 1) + "월 " + day + "일까지").typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.step03_text).text((month + 1) + "월 " + day + "일\n까지 결제");
        aq.id(R.id.step04_text).text(dueDate+"\n배달 가능");

        aq.id(R.id.date_text).text((month+1)+"월 "+day+"일까지 결제가 완료되지 않으면 요청하신\n날짜에 세탁물을 받으실 수 없으니 꼭 기억해주세요.");

        //payinfo_memo_bg_0
        if("01".equals(type)){
            aq.id(R.id.step01_text).textColorId(R.color.navigationBarColor);
            aq.id(R.id.step_icon_01).image(R.mipmap.top_payinfo_icon_01_enabled);
            aq.id(R.id.step_icon_03).image(R.mipmap.top_payinfo_icon_03_disabled);
            aq.id(R.id.memo_bg).image(R.mipmap.payinfo_memo_bg_0);
        }else{
            aq.id(R.id.step03_text).textColorId(R.color.navigationBarColor);
            aq.id(R.id.step_icon_01).image(R.mipmap.top_payinfo_icon_01_disabled);
            aq.id(R.id.step_icon_03).image(R.mipmap.top_payinfo_icon_03_enabled);
            aq.id(R.id.memo_bg).image(R.mipmap.payinfo_memo_bg);
        }


    }

    public void noClicked(View button) {
        Log.e("okCilcked", "NO");
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

}
