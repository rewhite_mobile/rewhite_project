package me.rewhite.users.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;

import org.json.JSONArray;

import java.util.Date;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.TimeUtil;

public class EventAlertScreen extends BaseActivity {

    private AQuery aq;
    String type;
    String imageLink;
    String eventLink;
    String bannerId;
    JSONArray data;
    public Context ctx = this;
    boolean isLatestSelected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        setContentView(R.layout.fragment_event_popup);
        aq = new AQuery(this);

        Intent intent = getIntent();
        if (intent != null) {
            type = intent.getStringExtra("type");

            if("coupon".equals(type)){
                aq.id(R.id.content).image(R.mipmap.coupon);
            }else if("refer".equals(type)){
                aq.id(R.id.content).image(R.mipmap.refer);
            }else if("event".equals(type)){
                imageLink = intent.getStringExtra("image");
                aq.id(R.id.content).image(imageLink);
                eventLink = intent.getStringExtra("link");
                bannerId = intent.getStringExtra("bannerId");
            }
        }

        aq.id(R.id.content).clicked(this, "contentClicked");
        aq.id(R.id.btn_latest).clicked(this, "latestClicked");
        aq.id(R.id.btn_close).clicked(this, "closeClicked");

    }

    public void latestClicked(View button){
        isLatestSelected = !isLatestSelected;
        if(isLatestSelected){
            aq.id(R.id.btn_latest).image(R.mipmap.event_btn_dontshow_sel);
            SharedPreferencesUtility.set(SharedPreferencesUtility.Command.EVENT_LATEST_DATE, "");
            SharedPreferencesUtility.set(SharedPreferencesUtility.Command.EVENT_NEVER_SHOW, bannerId);
        }else{
            aq.id(R.id.btn_latest).image(R.mipmap.event_btn_dontshow_def);
            Date today = new Date();
            SharedPreferencesUtility.set(SharedPreferencesUtility.Command.EVENT_LATEST_DATE, TimeUtil.convertTimestampToStringDate2(today.getTime()));
            SharedPreferencesUtility.set(SharedPreferencesUtility.Command.EVENT_NEVER_SHOW, "");
        }
    }

    public void contentClicked(View button){

        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("이벤트")
                .setAction("팝업터치")
                //.setLabel("")
                .build());
        AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
        logger.logEvent("Touch Popup Banner");

        String url = null;
        url = eventLink;
        Uri uri = Uri.parse(url);
        String queryHost = uri.getHost();

        if("http".equals(uri.getScheme()) || "https".equals(uri.getScheme())){
            Intent intent = new Intent(ctx, HtmlActivity.class);
            intent.putExtra("TERMS_URI", url);
            intent.putExtra("TITLE_NAME", "이벤트");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(intent);

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Date today = new Date();
                    SharedPreferencesUtility.set(SharedPreferencesUtility.Command.EVENT_LATEST_DATE, TimeUtil.convertTimestampToStringDate2(today.getTime()));
                    Log.i("EVENT_LATEST_DATE", SharedPreferencesUtility.get(SharedPreferencesUtility.Command.EVENT_LATEST_DATE));
                    finish();
                }
            }, 100);
        }else{
            //String url ="selphone://post_detail?post_id=10";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        }

    }

    public void closeClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Date today = new Date();
                SharedPreferencesUtility.set(SharedPreferencesUtility.Command.EVENT_LATEST_DATE, TimeUtil.convertTimestampToStringDate2(today.getTime()));
                Log.i("EVENT_LATEST_DATE", SharedPreferencesUtility.get(SharedPreferencesUtility.Command.EVENT_LATEST_DATE));
                finish();
            }
        }, 100);
    }

    @Override
    public void onBackPressed() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Date today = new Date();
                SharedPreferencesUtility.set(SharedPreferencesUtility.Command.EVENT_LATEST_DATE, TimeUtil.convertTimestampToStringDate2(today.getTime()));
                Log.i("EVENT_LATEST_DATE", SharedPreferencesUtility.get(SharedPreferencesUtility.Command.EVENT_LATEST_DATE));

            }
        }, 100);
        super.onBackPressed();

    }

}

