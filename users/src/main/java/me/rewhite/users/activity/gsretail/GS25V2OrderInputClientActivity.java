package me.rewhite.users.activity.gsretail;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.PickupType;
import me.rewhite.users.fragment.GS25V2OrderInputStep4Fragment;
import me.rewhite.users.fragment.GS25V2OrderInputStep5Fragment;
import me.rewhite.users.fragment.IFragment;
import me.rewhite.users.util.DUtil;

public class GS25V2OrderInputClientActivity extends BaseActivity {

    private static final String TAG = "GS25V2OrderInputClient";
    public static final String FRAGMENT_STEP4 = "gs_order_step4";
    public static final String FRAGMENT_STEP5 = "gs_order_step5";

    private Fragment mContent;
    private boolean mIsTouchDisable; // 연속 터치로 생기는 이슈 막기 위함
    private String currentFragname;

    public boolean isEndPopupOpened = false;

    private Context mContext;
    private AQuery aq;
    ImageButton button = null;

    JSONObject orderInfo;
    public String partnerStoreId = "1002";
    public String partnerStoreName = "";
    public String priceGroupId = "";
    public String partnerId = "7";
    public int internalStoreId;
    int orderSubType = 151;

    int pickupMode = PickupType.PICKUPTYPE_GS25;

    public int count_unit = 0;
    public int count_pack = 0;

    public String request_content_string = "";
    public String finalOrderId = "";
    public String finalOrderDataInfo = "";

    static Dialog unitDialog;
    static Dialog packDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gs25_v2_order_input_client);

        aq = new AQuery(this);

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        mContext = this;

        Intent intent = getIntent();
        if(intent != null){
            try {
                orderInfo = new JSONObject(intent.getStringExtra("orderInfo"));
                partnerId = orderInfo.getString("partnerId");
                partnerStoreId = orderInfo.getString("partnerStoreId");
                count_unit = orderInfo.getInt("pickupQuantity");
                request_content_string = orderInfo.getString("pickupRequestMessage");
                internalStoreId = Integer.parseInt(orderInfo.getString("internalStoreId"));
                if(internalStoreId >= 20000){
                    orderSubType = 152;
                }else{
                    orderSubType = 151;
                }

                Log.e("inputClient", internalStoreId + " , " + partnerId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        initilize();

        aq.id(R.id.btn_close).clicked(this, "closeClicked");
        aq.id(R.id.btn_orderinfo).clicked(this, "orderDetailClicked");
    }

    public void orderDetailClicked(View button){
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setClass(mContext, GS25OrderDetailActivity.class);
        intent.putExtra("orderId", finalOrderId);
        intent.putExtra("orderSubType", orderSubType);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        finish();
    }

    public void closeClicked(View button) {
        finish();
    }

    public void showEndPopup(){
        aq.id(R.id.comp_layout).visible();
        isEndPopupOpened = true;
    }

    private void initilize() {
        showFragment(FRAGMENT_STEP4);
    }

    public void showFragment(String fragmentName) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);
        showFragment(fragmentName, null);
    }

    public void showFragment(String fragmentName, Bundle args) {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(fragmentName);

        if (FRAGMENT_STEP4.equals(fragmentName)) {
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragment = new GS25V2OrderInputStep4Fragment();
            aq.id(R.id.gs25_top_image).image(R.mipmap.img_gs25_step4_top_01);
            aq.id(R.id.text_title).text("주문수량 확인");
        } else if (FRAGMENT_STEP5.equals(fragmentName)) {
            aq.id(R.id.gs25_top_image).image(R.mipmap.img_gs25_step5_top_02);
            fragment = new GS25V2OrderInputStep5Fragment();
            aq.id(R.id.text_title).text("라벨지 부착 확인");
        }

        Log.e(TAG, "[GS25V2 OrderInput] Create : " + fragmentName);

        if (null != args) {
            fragment.setArguments(args);
        }
        ft.setCustomAnimations(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        ft.replace(R.id.content_frame, fragment, fragmentName);
        ft.addToBackStack(fragmentName);
        ft.commitAllowingStateLoss();
        //ft.commit();
    }


    public boolean getTouchDisable() {
        Log.d(TAG, "[getTouchDisable]");
        return mIsTouchDisable;
    }

    public void setTouchDisable(boolean isTouchDisable) {
        Log.d(TAG, "[setTouchDisable]" + isTouchDisable);
        mIsTouchDisable = isTouchDisable;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        DUtil.Log(TAG, "[onBackPressed] Back Stack Entry Count : " + fm.getBackStackEntryCount());

        if (fm.getBackStackEntryCount() >= 1) {

            String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
            IFragment fragment = (IFragment) fm.findFragmentByTag(tag);
            if (false == fragment.onBackPressed()) {
                currentFragname = null;

                if (fm.getBackStackEntryCount() == 1) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                    alertDialogBuilder.setCancelable(true).setMessage("주문접수 중인데, 중단하시겠어요?")
                            .setPositiveButton("네", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }else if (fm.getBackStackEntryCount() == 2) {
                    if(!isEndPopupOpened){
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                        alertDialogBuilder.setCancelable(true).setMessage("주문번호 부착을 완료해주세요.")
                                .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }else{
                        finish();
                    }

                } else {
                    setTouchDisable(true);
                    //clearCaptureView();

                    fm.popBackStack();

                }
            }
        } else {
            super.onBackPressed();
        }

    }

}
