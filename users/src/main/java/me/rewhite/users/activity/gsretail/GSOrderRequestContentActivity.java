package me.rewhite.users.activity.gsretail;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.androidquery.AQuery;

import me.rewhite.users.R;
import me.rewhite.users.common.BackPressEditText;
import me.rewhite.users.common.BaseActivity;

public class GSOrderRequestContentActivity extends BaseActivity {

    // 세탁물 접수시 요청사항 입력화면
    private AQuery aq;
    private String preContent = "";
    private BackPressEditText editText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gsorder_request_content);

        aq = new AQuery(this);

        Intent intent = getIntent();
        if(intent != null){
            preContent = intent.getStringExtra("content");
            aq.id(R.id.input_content).text(preContent);
        }

        editText = (BackPressEditText) findViewById(R.id.input_content);
        editText.requestFocus();
        editText.addTextChangedListener(new TextWatcher() {
            String strCur;
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if(s.length() > 100){
                    editText.setText(strCur);
                    editText.setSelection(start);
                }else{
                    aq.id(R.id.text_length).text(s.length() + "/100");
                }

            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                strCur = s.toString();
            }
            @Override
            public void afterTextChanged(Editable edit) {
            }
        });
        editText.setOnBackPressListener(onBackPressListener);


        //키보드 보이게 하는 부분
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.SHOW_IMPLICIT);

        aq.id(R.id.btn_submit).clicked(this, "submitAction");
        aq.id(R.id.btn_confirm).clicked(this, "submitAction");
        aq.id(R.id.btn_drawer).clicked(this, "closeAction");
    }

    private void didBackPressOnEditText()
    {
        hideKeyboard();

        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }

    private void hideKeyboard(){
        editText = (BackPressEditText) findViewById(R.id.input_content);
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    private BackPressEditText.OnBackPressListener onBackPressListener = new BackPressEditText.OnBackPressListener()
    {
        @Override
        public void onBackPress()
        {
            didBackPressOnEditText();
        }
    };

    public void closeAction(View button){

        hideKeyboard();

        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }

    public void submitAction(View button){
        String content = aq.id(R.id.input_content).getText().toString();

        Intent resultData = new Intent();
        resultData.putExtra("INPUT_CONTENT", content);
        setResult(Activity.RESULT_OK, resultData);
        finish();
    }

    @Override
    public void onBackPressed() {
        hideKeyboard();

        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }
}
