package me.rewhite.users.activity;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.share.Sharer;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.AppInviteDialog;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.kakao.kakaolink.AppActionBuilder;
import com.kakao.kakaolink.AppActionInfoBuilder;
import com.kakao.kakaolink.KakaoLink;
import com.kakao.kakaolink.KakaoTalkLinkMessageBuilder;
import com.kakao.util.KakaoParameterException;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.ToastUtility;

public class ShareActivity extends BaseActivity {

    AQuery aq;
    Context ctx = this;

    CallbackManager callbackManager;
    ShareDialog shareDialog;
    private KakaoLink kakaoLink;
    private CheckBox forwardable;
    private KakaoTalkLinkMessageBuilder kakaoTalkLinkMessageBuilder;

    private final String imageSrc = "http://img.rewhite.me/www/assets/images/invite_friend.png";

    //"http://marines.co.kr/rewhite/invite_friend.png";
    //"http://img.rewhite.me/www/assets/images/invite_friend.png";
    //https://img.rewhite.me/www/assets/images/logo-100x100.jpg
    //private final String weblink = "https://fb.me/521608421360937?referral="+ SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.SHARE_CODE);
    private final String weblink = "https://www.rewhite.me?referral="+ SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.SHARE_CODE);
    private final String inWeblink = "https://fb.me/521608421360937?referral="+ SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.SHARE_CODE);

    String shareTitle = "우리동네 배달세탁소 '리:화이트'";
    String shareSubject = "우리 동네 배달 세탁소 '리:화이트' 설치하면 3,000원 이용권 드려요~";
    String shareContent = "자주 이용하는 우리 동네 세탁소\n이제 직접 가지말고 리화이트 하세요.\n\n10초면 세탁물 주문 완료 끝!\n그리고 깨끗한 옷은 집에서 편하게 배송받기!!\n\n지금 다운받기 http://www.rewhite.me";
    //String shareFormatString = "우리 동네 배달 세탁소 “리화이트”\n\n회원가입 시 친구 추천 코드 <B><font color=\"#5eaeff\">%s</font></B>을 \n입력하면 3,000 포인트를 드려요~";
    //*  추천 받은 친구가 추천코드 ( UK220924B ) 를 입력 후    첫 주문하면  Clara  님에게  3,000 포인트를 드려요~
    String descString = "* 추천 받은 친구가 추천코드( <B><font color=\"#5eaeff\">%s</font></B> )를 입력 후<br/>첫 주문하면 <B><font color=\"#4d4d4d\">%s</font></B> 님에게 3,000 포인트를 드려요~";

    String kakaoShareString = "[우리동네 배달 세탁소, 리:화이트]\n%s님이 세탁비 3,000원 이용권을 보냈어요!\n" +
            "초대코드 : %s";


    String nickName = "";
    String shareCode = "";
    String webLinkString = "http://www.rewhite.me";
    String imageUrlString = "https://img.rewhite.me/www/assets/images/invite_friend.png";
    private final String execParam = "referrer=kakaostory&referral=" + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.SHARE_CODE);
    private final String marketParam = "referrer=" + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.SHARE_CODE);
    private View decorView;
    TextView codeLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
        codeLabel = aq.id(R.id.text_code).getTextView();

        shareCode = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.SHARE_CODE);
        nickName = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME);
        aq.id(R.id.text_code).text(shareCode);

        codeLabel.setLongClickable(true);
        codeLabel.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                TextView tv = (TextView)v;
                final String m = tv.getText().toString();

                AlertDialog.Builder dial = new AlertDialog.Builder(ctx);
                dial.setTitle("클립보드에 복사");

                final CharSequence[] items ={"내 추천코드 복사하기"};

                dial.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ClipboardManager clip = (ClipboardManager)ctx.getSystemService(Context.CLIPBOARD_SERVICE);
                        clip.setText(m);
                        dialog.dismiss();
                    }
                });

                dial.show();

                return false;
            }
        });

        try {
            kakaoLink = KakaoLink.getKakaoLink(getApplicationContext());
            kakaoTalkLinkMessageBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();
        } catch (KakaoParameterException e) {
            e.printStackTrace();
            //alert(e.getMessage());
        }
        try {
        kakaoTalkLinkMessageBuilder.addText(String.format(kakaoShareString, nickName, shareCode))
                .addImage(imageSrc, 600, 406)
                .addAppButton("앱으로 이동",
                        new AppActionBuilder()
                                .addActionInfo(AppActionInfoBuilder
                                        .createAndroidActionInfoBuilder()
                                        .setExecuteParam("referral=" + shareCode)
                                        .setMarketParam("referrer=kakaotalklink")
                                        .build())
                                .addActionInfo(AppActionInfoBuilder
                                        .createiOSActionInfoBuilder()
                                        .setExecuteParam("referral=" + shareCode)
                                        .build())
                                .setUrl("http://www.rewhite.me").build()); // PC 카카오톡 에서 사용하게 될 웹사이트 주소
        } catch (KakaoParameterException e) {
            DUtil.alertShow(this, e.getMessage());
        }

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        // this part is optional
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        aq.id(R.id.btn_kakaotalk).clicked(this, "shareViaKakaoTalk");
        aq.id(R.id.btn_fb_invite).clicked(this, "shareViaInvite");
        //aq.id(R.id.btn_kakaostory).clicked(this, "shareViaKakaoStory");
        aq.id(R.id.btn_facebook).clicked(this, "shareViaFacebook");
        aq.id(R.id.btn_etc).clicked(this, "shareViaEtc");
    }

    public void shareViaEmail(View button) {

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("기타")
                .setAction("추천하기")
                .setLabel("이메일")
                .build());

        Intent it = new Intent(Intent.ACTION_SEND);
        it.putExtra(Intent.EXTRA_TEXT, shareContent);
        it.putExtra(Intent.EXTRA_SUBJECT, shareSubject);
        it.setType("message/rfc822");
        startActivity(Intent.createChooser(it, "Choose Email Client"));
    }

    public void closeClicked(View button) {
        finish();
    }

    public void shareViaKakaoTalk(View button) {
        try {
            Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
            t.send(new HitBuilders.EventBuilder()
                    .setCategory("기타")
                    .setAction("추천하기")
                    .setLabel("카카오톡")
                    .build());
            AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
            logger.logEvent("Recommend");
            kakaoLink.sendMessage(kakaoTalkLinkMessageBuilder, this);

        } catch (KakaoParameterException e) {
            DUtil.alertShow(this, e.getMessage());
        }
    }

    public void shareViaFacebook(View button) {
        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("기타")
                .setAction("추천하기")
                .setLabel("페이스북포스팅")
                .build());
        AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
        logger.logEvent("Recommend");

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(shareSubject)
                    .setContentDescription(
                            String.format(kakaoShareString, nickName, shareCode))
                    .setImageUrl(Uri.parse(imageUrlString))
                    .setContentUrl(Uri.parse(weblink))
                    .setShareHashtag(new ShareHashtag.Builder()
                            .setHashtag("#리화이트").build())
                    .build();

            shareDialog.show(linkContent);
        }

    }

    public void shareViaInvite(View button) {
        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("기타")
                .setAction("추천하기")
                .setLabel("페이스북앱초대")
                .build());
        AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
        logger.logEvent("Recommend");

        String appLinkUrl, previewImageUrl;

        appLinkUrl = "https://fb.me/521608421360937?referral="+shareCode;
        previewImageUrl = imageUrlString;//imageSrc;//"https://img.rewhite.me/www/assets/images/10sec_slogan.jpg";

        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .setPreviewImageUrl(previewImageUrl)
                    .build();
            AppInviteDialog.show(this, content);
        }
    }

    public void shareViaEtc(View button) {
        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("기타")
                .setAction("추천하기")
                .setLabel("클립보드복사")
                .build());
        AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
        logger.logEvent("Recommend");

//        Intent sendIntent = new Intent();
//        sendIntent.setAction(Intent.ACTION_SEND);
//        sendIntent.putExtra(Intent.EXTRA_SUBJECT, shareSubject);
//        sendIntent.putExtra(Intent.EXTRA_TEXT, String.format(kakaoShareString, nickName, shareCode));
//        sendIntent.setType("text/plain");
//
//        startActivity(Intent.createChooser(sendIntent, "공유하기"));

        ClipboardManager clip = (ClipboardManager)ctx.getSystemService(Context.CLIPBOARD_SERVICE);
        clip.setText(String.format(kakaoShareString, nickName, shareCode) + "\n\nhttp://smarturl.it/rewhite_friend");

        ToastUtility.show(ctx, "클립보드에 복사되었습니다.");

        //startActivity(sendIntent);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
