package me.rewhite.users.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.AppInviteDialog;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.kakao.kakaolink.AppActionBuilder;
import com.kakao.kakaolink.AppActionInfoBuilder;
import com.kakao.kakaolink.KakaoLink;
import com.kakao.kakaolink.KakaoTalkLinkMessageBuilder;
import com.kakao.util.KakaoParameterException;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.util.SharedPreferencesUtility;

public class RewhiteSchemeListener extends BaseActivity {

    Tracker t;
    public String referCode;
    public Context mContext = this;

    CallbackManager callbackManager;
    ShareDialog shareDialog;

    FacebookCallback<Sharer.Result> fbCallback = new FacebookCallback<Sharer.Result>(){

        @Override
        public void onSuccess(Sharer.Result result) {

        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException error) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewhite_scheme_listener);

        t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        // this part is optional
        shareDialog.registerCallback(callbackManager, fbCallback);

        Log.e("RewhiteSchemeListener", "excuted");

        Intent pIntent = getIntent();
        if (Intent.ACTION_VIEW.equals(pIntent.getAction())) {
            Uri uri = pIntent.getData();
            if(uri != null){

                String queryHost = uri.getHost();

                Intent intent = new Intent();

                if("tphoneSn2Kg4R4OMOPqHE1rn1H3gIwU1ve50d28NU0cKgA2FA=".equals(uri.getScheme())){
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("APP_REFER_Android")
                            .setAction("Referral")
                            .setLabel("TPhone")
                            .build());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setClass(mContext, IntroActivity.class);
                }else if("kakao60a4496af5250131ee65bb41d09ebedc=".equals(uri.getScheme())){
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("APP_REFER_Android")
                            .setAction("Referral")
                            .setLabel("KakaoLink")
                            .build());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setClass(mContext, IntroActivity.class);
                }else{
                    if("excute".equals(queryHost)){
                        if(uri.getQueryParameter("referCode") != null){
                            referCode = uri.getQueryParameter("referCode");
                            Log.e("RewhiteSchemeListener", queryHost + " / " + referCode);
                            t.send(new HitBuilders.EventBuilder()
                                    .setCategory("APP_REFER_Android")
                                    .setAction("Referral")
                                    .setLabel(referCode)
                                    .build());
                        }else if(uri.getQueryParameter("referral") != null){
                            SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.REFER, uri.getQueryParameter("referral"));
                        }
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.setClass(mContext, IntroActivity.class);

                    }else if("auth".equals(queryHost)){
                        if(uri.getQueryParameter("referCode") != null){
                            referCode = uri.getQueryParameter("referCode");
                            Log.e("RewhiteSchemeListener", queryHost + " / " + referCode);
                            t.send(new HitBuilders.EventBuilder()
                                    .setCategory("APP_Auth_Android")
                                    .setAction("Referral")
                                    .setLabel(referCode)
                                    .build());

                            if("SHINHAN".equals(referCode.toUpperCase())){
                                /* ==========================================================================================
                                    TODO 신한카드 로그인 연동
                                    1. 로그인처리 앱실행
                                    2. 로그인처리 주문하기
                                    3. 로그인처리 주문내역목록
                                    4. 로그인처리 배송지관리
                                    5. 로그인처리 내지갑
                                    6. 로그인처리 이벤트
                                ========================================================================================== */
                                // 신한카드 로그인 연동
                                // 신한카드 FAN통해 접근하는 로그인

                                String authToken = uri.getQueryParameter("token");
                                String menuIndex = uri.getQueryParameter("menuIndex");
                                processSchemeForSHINHAN(authToken, menuIndex);

                                return;
                            }else{

                            }

                        }else if(uri.getQueryParameter("referral") != null){
                            SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.REFER, uri.getQueryParameter("referral"));
                        }
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.setClass(mContext, IntroActivity.class);

                    }else if("app".equals(queryHost)){
                        // 로그인여부 확인
                        if("".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN))){
                            Log.e("RewhiteSchemeListener ACCESS_TOKEN", "empty token");
                            // 로그인안한 상태
                            if(uri.getQueryParameter("referCode") != null){
                                referCode = uri.getQueryParameter("referCode");
                                Log.e("RewhiteSchemeListener", queryHost + " / " + referCode);
                                t.send(new HitBuilders.EventBuilder()
                                        .setCategory("APP_MENU_MOVE")
                                        .setAction("Referral")
                                        .setLabel(referCode)
                                        .build());
                            }else if(uri.getQueryParameter("referral") != null){
                                SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.REFER, uri.getQueryParameter("referral"));
                            }
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.setClass(mContext, IntroActivity.class);
                        }else{
                            // 로그인이 되어있을때
                            String menuIndex = uri.getQueryParameter("menuIndex");

                            // 주문상세인 경우와 아닐때
                            if(!"6".equals(menuIndex)){

                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                                if("1".equals(menuIndex)){
                                    // 주문하기
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.setClass(mContext, MainActivity.class);
                                }else if("2".equals(menuIndex)){
                                    // 배송지 등록
                                    intent.setClass(mContext, LocationActivity.class);
                                }else if("3".equals(menuIndex)){
                                    // 프로필 수정
                                    intent.setClass(mContext, ProfileActivity.class);
                                }else if("4".equals(menuIndex)){
                                    // 내 지갑
                                    String subMenuIndex = uri.getQueryParameter("subMenuIndex");
                                    if("0".equals(subMenuIndex)){
                                        intent.setClass(mContext, CouponActivityV2.class);
                                    }else if("1".equals(subMenuIndex)){
                                        intent.setClass(mContext, AddCoinActivity.class);
                                        intent.putExtra("section", 0);
                                    }else if("2".equals(subMenuIndex)){
                                        intent.setClass(mContext, AddCouponActivity.class);
                                        intent.putExtra("section", 1);
                                    }
                                }else if("5".equals(menuIndex)){
                                    // 공유하기
                                    intent.setClass(mContext, ShareActivity.class);
                                }else if("7".equals(menuIndex)){
                                    intent.setClass(mContext, EventListActivity.class);
                                }else if("8".equals(menuIndex)){
                                    // 약관
                                    String subMenuIndex = uri.getQueryParameter("subMenuIndex");
                                    if("1".equals(subMenuIndex)){
                                        // 이용약관
                                        intent.setClass(mContext, TermsDetailView.class);
                                        intent.putExtra("TERMS_TITLE", "이용약관");
                                        intent.putExtra("TERMS_URI", Constants.TERMS_USE);

                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);


                                    }else if("2".equals(subMenuIndex)){
                                        // 개인정보취급방침
                                        intent.setClass(mContext, TermsDetailView.class);
                                        intent.putExtra("TERMS_TITLE", "개인정보 취급방침");
                                        intent.putExtra("TERMS_URI", Constants.TERMS_PRIVACY);

                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                                    }
                                }else if("9".equals(menuIndex)){
                                    // 주문내역목록
                                    intent.setClass(mContext, OrderHistoryActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                }else if("10".equals(menuIndex)){
                                    // 주문내역목록
                                    intent.setClass(mContext, HtmlActivity.class);
                                    intent.putExtra("TERMS_URI", Constants.HTML_NOTICE);
                                    intent.putExtra("TITLE_NAME", getString(R.string.title_notice));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                            | Intent.FLAG_ACTIVITY_SINGLE_TOP
                                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                }
                            }else{
                                // 주문상세일때는 필요한 파라미터가 orderId
                                String orderId = uri.getQueryParameter("orderId");
                                String orderLink = "rewhiteMeUserOrder://orderDetail?orderId="+orderId;
                                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(orderLink));
                            }
                        }
                    }else if("appEx".equals(queryHost)){
                        if(uri.getQueryParameter("referCode") != null){
                            referCode = uri.getQueryParameter("referCode");
                            Log.e("RewhiteSchemeListener", queryHost + " / " + referCode);
                            t.send(new HitBuilders.EventBuilder()
                                    .setCategory("WEB_REFER")
                                    .setAction("Referral")
                                    .setLabel(referCode)
                                    .build());
                        }else if(uri.getQueryParameter("referral") != null){
                            SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.REFER, uri.getQueryParameter("referral"));
                        }

                        intent.setClass(mContext, HtmlActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        String type = uri.getQueryParameter("type");

                        if("web".equals(type)){
                            String linkUrl = uri.getQueryParameter("value");
                            String titleString = uri.getQueryParameter("title");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                            intent.putExtra("TERMS_URI", linkUrl);
                            intent.putExtra("TITLE_NAME", titleString);

                        }else if("yellowid".equals(type)){
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                            intent.putExtra("TERMS_URI", Constants.HTML_YELLOW);
                            intent.putExtra("TITLE_NAME", "옐로우아이디");
                        }else if("shareFacebook".equals(type)){
                            String shareString = uri.getQueryParameter("value");
                            String titleString = uri.getQueryParameter("title");
                            if (ShareDialog.canShow(ShareLinkContent.class)) {
                                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                        .setContentTitle(titleString)
                                        .setContentDescription(
                                                shareString)
                                        .setContentUrl(Uri.parse("https://www.rewhite.me?referral="+referCode))
                                        .setQuote("리화이트 - 깨끗한 세탁 편리한 신청")
                                        .setShareHashtag(new ShareHashtag.Builder()
                                                .setHashtag("#리화이트")
                                                .build())
                                        .build();

                                shareDialog.show(linkContent);
                            }
                            return;
                        }else if("inviteFacebook".equals(type)){
                            String appLinkUrl, previewImageUrl;

                            appLinkUrl = "https://fb.me/521608421360937?referral="+referCode;
                            previewImageUrl = "https://img.rewhite.me/www/assets/images/10sec_slogan.jpg";

                            if (AppInviteDialog.canShow()) {
                                AppInviteContent content = new AppInviteContent.Builder()
                                        .setApplinkUrl(appLinkUrl)
                                        .setPreviewImageUrl(previewImageUrl)
                                        .build();
                                AppInviteDialog.show(this, content);
                            }
                            finish();
                            return;
                        }else if("shareKakaotalk".equals(type)){
                            KakaoLink kakaoLink;
                            KakaoTalkLinkMessageBuilder kakaoTalkLinkMessageBuilder;
                            String shareString = uri.getQueryParameter("value");
                            String previewImageUrl = "http://img.rewhite.me/www/assets/images/10sec_slogan.jpg";

                            try {
                                kakaoLink = KakaoLink.getKakaoLink(getApplicationContext());
                                kakaoTalkLinkMessageBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();
                                kakaoTalkLinkMessageBuilder.addText(shareString)
                                        .addImage(previewImageUrl, 660, 380)
                                        .addAppButton("앱으로 이동",
                                                new AppActionBuilder()
                                                        .addActionInfo(AppActionInfoBuilder
                                                                .createAndroidActionInfoBuilder()
                                                                .setExecuteParam("referral=" + referCode)
                                                                .setMarketParam("referrer=kakaotalklink")
                                                                .build())
                                                        .addActionInfo(AppActionInfoBuilder
                                                                .createiOSActionInfoBuilder()
                                                                .setExecuteParam("referral=" + referCode)
                                                                .build())
                                                        .setUrl("http://www.rewhite.me").build()); // PC 카카오톡 에서 사용하게 될 웹사이트 주소

                                t.send(new HitBuilders.EventBuilder()
                                        .setCategory("SHARE")
                                        .setAction(referCode + "_Kakaotalk_Refer")
                                        .setLabel("공유하기")
                                        .build());
                                kakaoLink.sendMessage(kakaoTalkLinkMessageBuilder, this);
                            } catch (KakaoParameterException e) {
                                e.printStackTrace();
                                //alert(e.getMessage());
                            }
                            finish();
                            return;
                        }

                    }else{
                        // 정의되지않은 Scheme 호출시 앱실행만
                        intent.setClass(mContext, IntroActivity.class);
                    }
                }

                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                Log.e("RewhiteSchemeListener ", intent.toString());
                //finish();
            }else{
                finish();
            }

        }
    }

    private void processSchemeForSHINHAN(String _token, String _menuIndex){
        Intent intent = new Intent();
        intent.setClass(mContext, IntroActivity.class);
        intent.putExtra("excuteType", "auth");
        intent.putExtra("referCode", referCode.toUpperCase());
        intent.putExtra("token", _token);
        intent.putExtra("menuIndex", _menuIndex);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        Log.e("processSchemeForSHINHAN ", intent.toString());
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        finish();
    }
}
