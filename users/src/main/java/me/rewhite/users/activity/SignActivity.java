package me.rewhite.users.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.kakao.auth.AuthType;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeResponseCallback;
import com.kakao.usermgmt.response.model.UserProfile;
import com.kakao.util.exception.KakaoException;
import com.kakao.util.helper.log.Logger;
import com.toast.android.paycologin.OnLoginListener;
import com.toast.android.paycologin.PaycoLoginError;
import com.toast.android.paycologin.PaycoLoginExtraResult;
import com.toast.android.paycologin.PaycoLoginManager;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseFragmentActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.RewhiteLoginType;
import me.rewhite.users.fragment.IFragment;
import me.rewhite.users.fragment.LoginFragment;
import me.rewhite.users.session.AccessTokenRequest;
import me.rewhite.users.session.RewhiteException;
import me.rewhite.users.session.RewhiteSession;
import me.rewhite.users.session.RewhiteSessionCallback;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.StringUtil;

/*
import com.kakao.APIErrorResult;
import com.kakao.AuthType;
import com.kakao.MeResponseCallback;
import com.kakao.Session;
import com.kakao.SessionCallback;
import com.kakao.UserProfile;
import com.kakao.exception.KakaoException;
import com.kakao.helper.Logger;*/

//import com.actionbarsherlock.app.SherlockFragmentActivity;


public class SignActivity extends BaseFragmentActivity implements
        GoogleApiClient.OnConnectionFailedListener{

    private final static String TAG = SignActivity.class.getSimpleName();
    public static final String FRAGMENT_LOGIN = "sign_login";
    public static final String FRAGMENT_JOIN = "sign_join";

    public AQuery aq;
    public final Context mCtx = this;
    private final RewhiteSessionCallback rewhiteSessionCallback = new RewhiteSessionStatusCallback();
    private RewhiteSession session;
    Bundle bundle;

    // KAKAO Session
    private Session kakaoSession;
    private KakaoSessionCallback kakaoCallback;

    // Facebook Session
    CallbackManager callbackManager;

    public boolean isLoginProcessing = false;
    ProgressDialog mProgressDialog;
    View decorView;
    Tracker mTracker;

    private static final int RC_SIGN_IN = 9001;
    private GoogleApiClient mGoogleApiClient;

    @Override
    public void onDestroy() {
        super.onDestroy();

        session.removeCallback(rewhiteSessionCallback);
        //kakaoSession.removeCallback(kakaoSessionCallback);
        kakaoSession.removeCallback(kakaoCallback);
    }

    @Override
    public void onStart() {
        super.onStart();
        /*
        // Google Cached Login 처리부분
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);

        if (getActionBar() != null) {
            getActionBar().hide();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR);//.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.navigationBarColor));
        }

        decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_LAYOUT_FLAGS);

        mTracker = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        mTracker.setScreenName(TAG);
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        aq = new AQuery(this);

        initilize();

        String serverClientId = getString(R.string.server_client_id);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(serverClientId)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {

                        if(Profile.getCurrentProfile() != null && AccessToken.getCurrentAccessToken() != null) {
                            Log.i(TAG, Profile.getCurrentProfile().getName() + ", " + Profile.getCurrentProfile().getId() + ", " + Profile.getCurrentProfile().getLinkUri());

                            Profile.fetchProfileForCurrentAccessToken(); //manually force profile fetching from current token
                            if(Profile.getCurrentProfile() != null) { //if it available
                                Log.i(TAG, Profile.getCurrentProfile().getName() + ", " + Profile.getCurrentProfile().getId() + ", " + Profile.getCurrentProfile().getLinkUri());

                                if(AccessToken.getCurrentAccessToken() != null){
                                    // App code
                                    DUtil.Log(TAG, "onSuccess = " + AccessToken.getCurrentAccessToken().getUserId());
                                    Log.i("userProfile ID", AccessToken.getCurrentAccessToken().getUserId() + "");
                                    // login ok get access token
                                    Log.i("userProfile NickName", Profile.getCurrentProfile().getName());

                                    bundle = new Bundle();
                                    bundle.putString("loginId", Profile.getCurrentProfile().getId() + "");
                                    bundle.putString("secretKey", AccessToken.getCurrentAccessToken().getToken());
                                    bundle.putString("nickName", Profile.getCurrentProfile().getName());
                                    bundle.putString("imagePath", Profile.getCurrentProfile().getProfilePictureUri(200, 200).toString());
                                    bundle.putString("imageThumbPath", Profile.getCurrentProfile().getProfilePictureUri(100, 100).toString());
                                    authorize(bundle, RewhiteLoginType.TYPE_FACEBOOK);
                                }

                            } else {
                                Log.i(TAG, "Profile is null");
                                //showLoginActivity(); //no profile - login again
                            }
                        } else {
                            if(AccessToken.getCurrentAccessToken() != null){
                                AccessToken.getCurrentAccessToken();
                                Profile.fetchProfileForCurrentAccessToken();
                                if(Profile.getCurrentProfile() != null) { //if it available
                                    Log.i(TAG, Profile.getCurrentProfile().getName() + ", " + Profile.getCurrentProfile().getId() + ", " + Profile.getCurrentProfile().getLinkUri());
                                }
                            }else{

                            }

                        }
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        DUtil.Log(TAG, "onError = " + exception.getLocalizedMessage());
                        DUtil.alertShow(SignActivity.this, exception.getLocalizedMessage());
                    }
                });

    }


    public void requestMe() {
        UserManagement.getInstance().requestMe(new MeResponseCallback() {
            @Override
            public void onSuccess(final UserProfile userProfile) {
//                Toast.makeText(getApplicationContext(),
//                        "succeeded to get user profile", Toast.LENGTH_SHORT)
//                        .show();
                if (userProfile != null) {

                    Log.i("userProfile ID", userProfile.getId() + "");
                    Log.i("userProfile NickName", userProfile.getNickname() + "");
                    bundle = new Bundle();
                    bundle.putString("loginId", userProfile.getId() + "");
                    bundle.putString("secretKey", kakaoSession.getAccessToken());
                    bundle.putString("nickName", userProfile.getNickname() + "");
                    bundle.putString("imagePath", userProfile.getProfileImagePath());
                    bundle.putString("imageThumbPath", userProfile.getThumbnailImagePath());

                    SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_IMAGE, userProfile.getProfileImagePath());
                    SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_THUMB, userProfile.getThumbnailImagePath());
                    SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_USER_NAME, userProfile.getNickname());
                    SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_LOGIN_TYPE, RewhiteLoginType.TYPE_KAKAO);

                    authorize(bundle, RewhiteLoginType.TYPE_KAKAO);
                }
            }

            @Override
            public void onNotSignedUp() {
                // redirectLoginActivity();
            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                //redirectLoginActivity();
            }

            @Override
            public void onFailure(ErrorResult errorResult) {
                String message = "failed to get user info. msg=" + errorResult;
                //Logger.getInstance().d(message);


            }
        });
    }

    ProfileTracker profileTracker;
    private void initilize() {

        session = RewhiteSession.getInstance(mCtx);
        session.addCallback(rewhiteSessionCallback);

        kakaoSession = Session.getCurrentSession();
        //kakaoSession.addCallback(kakaoSessionCallback);
        kakaoCallback = new KakaoSessionCallback();
        kakaoSession.addCallback(kakaoCallback);
        //Session.getCurrentSession().checkAndImplicitOpen();

        profileTracker = new ProfileTracker() { //initializing profile tracker, so it can respond to it's state changes
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) { //if new profile is logged
                Profile.fetchProfileForCurrentAccessToken(); //manually force profile fetching from current token
                if(Profile.getCurrentProfile() != null) { //if it available
                    Log.i(TAG, Profile.getCurrentProfile().getName() + ", " + Profile.getCurrentProfile().getId() + ", " + Profile.getCurrentProfile().getLinkUri());

                    if(AccessToken.getCurrentAccessToken() != null){
                        // App code
                        DUtil.Log(TAG, "onSuccess = " + AccessToken.getCurrentAccessToken().getUserId());
                        Log.i("userProfile ID", AccessToken.getCurrentAccessToken().getUserId() + "");
                        // login ok get access token
                        Log.i("userProfile NickName", Profile.getCurrentProfile().getName());

                        bundle = new Bundle();
                        bundle.putString("loginId", Profile.getCurrentProfile().getId() + "");
                        bundle.putString("secretKey", AccessToken.getCurrentAccessToken().getToken());
                        bundle.putString("nickName", Profile.getCurrentProfile().getName());
                        bundle.putString("imagePath", Profile.getCurrentProfile().getProfilePictureUri(200, 200).toString());
                        bundle.putString("imageThumbPath", Profile.getCurrentProfile().getProfilePictureUri(100, 100).toString());

                        SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_IMAGE, Profile.getCurrentProfile().getProfilePictureUri(200, 200).toString());
                        SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_THUMB, Profile.getCurrentProfile().getProfilePictureUri(200, 200).toString());
                        SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_USER_NAME, Profile.getCurrentProfile().getName());
                        SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_LOGIN_TYPE, RewhiteLoginType.TYPE_FACEBOOK);

                        authorize(bundle, RewhiteLoginType.TYPE_FACEBOOK);
                    }

                } else {
                    Log.i(TAG, "Profile is null");
                    //showLoginActivity(); //no profile - login again
                }
            }
        };

        showFragment(FRAGMENT_LOGIN);

    }

    private static final int SLEEPY_ACCOUNT_WAKED = 199;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (kakaoSession.handleActivityResult(requestCode,
                resultCode, data)) {
            Log.d(TAG, "onActivityResult : KakaoSession");
            return;
        } else if (session.handleActivityResult(requestCode,
                resultCode, data)) {
            Log.d(TAG, "onActivityResult : RewhiteSession");
            return;
        }

        switch (requestCode) {
            case SLEEPY_ACCOUNT_WAKED:
                Log.i("onActivityResult", "SLEEPY_ACCOUNT_WAKED");
                onSessionOpenedAfter();
                break;
            case RC_SIGN_IN:
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
                break;
            default:
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        PaycoLoginManager.getInstance().onActivityResult(this, requestCode, resultCode, data);
    }

    public void showFragment(String fragmentName) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);
        showFragment(fragmentName, null);
    }

    public void showFragment(String fragmentName, Bundle args) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(fragmentName);

        if (FRAGMENT_LOGIN.equals(fragmentName)) {
            fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        fragment = new LoginFragment();
        Log.e(TAG, "[LoginFragment] Create : " + fragmentName);

        if (null != args) {
            fragment.setArguments(args);
        }

        ft.replace(R.id.main_fragment_container, fragment, fragmentName);
        ft.addToBackStack(fragmentName);
        ft.commitAllowingStateLoss();
        //ft.commit();
    }

    private class RewhiteSessionStatusCallback implements RewhiteSessionCallback {

        /**
         * 세션이 오픈되었으면 가입페이지로 이동 한다.
         */
        @Override
        public void onSessionOpened() {
            // 뺑글이 종료

            // 프로그레스바를 보이고 있었다면 중지하고 세션 오픈후 보일 페이지로 이동
            SignActivity.this.onSessionOpened();
            DUtil.Log(TAG, "onSessionOpened");

        }

        @Override
        public void onSessionOpening() {
            // 뺑글이 시작
            isLoginProcessing = true;
            DUtil.Log(TAG, "onSessionOpening");
        }

        /**
         * 세션이 삭제되었으니 로그인 화면이 보여야 한다.
         *
         * @param exception 에러가 발생하여 close가 된 경우 해당 exception
         */
        @Override
        public void onSessionClosed(final RewhiteException exception) {
            // TODO Auto-generated method stub
            SignActivity.this.onSessionClosed();

            DUtil.Log(TAG, "onSessionClosed = " + exception.getLocalizedMessage());

            if ("S9003".equals(exception.getErrorCode()) || "S9004".equals(exception.getErrorCode()) || "S9005".equals(exception.getErrorCode()) || "S9006".equals(exception.getErrorCode())) {
                DUtil.Log(TAG, "회원가입 되지않은 Social로그인 요청");
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        isLoginProcessing = false;
                        if (mProgressDialog != null) mProgressDialog.dismiss();

                        Intent intent = new Intent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.setClass(mCtx, TermsAgreeActivity.class);
                        intent.putExtra("bundle", bundle);

                        if ("S9003".equals(exception.getErrorCode())) {
                            intent.putExtra("loginType", RewhiteLoginType.TYPE_FACEBOOK);

                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("가입및탈퇴")
                                    .setAction("가입시도")
                                    .setLabel("페이스북")
                                    .build());
                            AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                            logger.logEvent("Try join a member");
                        } else if ("S9004".equals(exception.getErrorCode())) {
                            intent.putExtra("loginType", RewhiteLoginType.TYPE_KAKAO);

                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("가입및탈퇴")
                                    .setAction("가입시도")
                                    .setLabel("카카오톡")
                                    .build());
                            AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                            logger.logEvent("Try join a member");
                        }else if ("S9005".equals(exception.getErrorCode())) {
                            intent.putExtra("loginType", RewhiteLoginType.TYPE_GOOGLE);

                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("가입및탈퇴")
                                    .setAction("가입시도")
                                    .setLabel("구글")
                                    .build());
                            AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                            logger.logEvent("Try join a member");
                        }else if ("S9006".equals(exception.getErrorCode())) {
                            intent.putExtra("loginType", RewhiteLoginType.TYPE_PAYCO);

                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("가입및탈퇴")
                                    .setAction("가입시도")
                                    .setLabel("페이코")
                                    .build());
                            AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                            logger.logEvent("Try join a member");
                        }

                        mCtx.startActivity(intent);
                        ((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        //((Activity) mCtx).finish();
                    }
                }, 500);
            } else {
                isLoginProcessing = false;
                if (mProgressDialog != null) mProgressDialog.dismiss();
            }

        }
    }

    public void requestFacebookSessionOpen() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.e("handleSignInResult getDisplayName", acct.getDisplayName());
            Log.e("handleSignInResult getEmail", acct.getEmail());
            Log.e("handleSignInResult getId", acct.getId());
            Log.e("handleSignInResult getIdToken", acct.getIdToken() + "");
            Log.e("handleSignInResult getAccount", acct.getAccount().toString());
            Log.e("handleSignInResult getPhotoUrl", acct.getPhotoUrl().toString());

            bundle = new Bundle();
            bundle.putString("loginId", acct.getId() + "");
            bundle.putString("secretKey", acct.getIdToken());
            bundle.putString("nickName", acct.getDisplayName() + "");
            bundle.putString("imagePath", acct.getPhotoUrl().toString());
            bundle.putString("imageThumbPath", acct.getPhotoUrl().toString());
            bundle.putString("email", acct.getEmail());

            SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_IMAGE, acct.getPhotoUrl().toString());
            SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_THUMB, acct.getPhotoUrl().toString());
            SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_USER_NAME, acct.getDisplayName());
            SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_LOGIN_TYPE, RewhiteLoginType.TYPE_GOOGLE);

            authorize(bundle, RewhiteLoginType.TYPE_GOOGLE);

        } else {
            Toast.makeText(this, "로그인에 실패했습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    public void requestGoogleSessionOpen(){
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void getPaycoProfile(String _clientId, String _accessToken){

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                if (status.getCode() >= 400) {
                    // IF ERROR
                    Log.i("PAYCO_GET_PROFILE StatusCode", status.getCode() + "");
                    Log.i("PAYCO_GET_PROFILE StatusError", status.getError());
                } else {
                    // IF NORMAL
                    if (json == null) {
                        Log.i("RETURN", "" + status.getMessage());
                        return;
                    }

                    Log.i("PAYCO_GET_PROFILE", "" + json.toString());

                    try {
                        if (json.getJSONObject("header").getInt("resultCode") == 0) {

                            String uuid = json.getJSONObject("memberProfile").getString("idNo");
                            String id = json.getJSONObject("memberProfile").getString("id");
                            String name = json.getJSONObject("memberProfile").getString("name");
                            String gender = json.getJSONObject("memberProfile").getString("sexCode");
                            //Toast.makeText(mActivity, json.getString("result_msg"), Toast.LENGTH_SHORT);
                            bundle = new Bundle();
                            bundle.putString("loginId", uuid);
                            bundle.putString("secretKey", PaycoLoginManager.getInstance().getAccessToken());
                            bundle.putString("nickName", name);

                            SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_IMAGE, "");
                            SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_THUMB, "");
                            SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_USER_NAME, name);
                            SharedPreferencesUtility.set(SharedPreferencesUtility.TempInfo.TEMP_LOGIN_TYPE, RewhiteLoginType.TYPE_PAYCO);

                            authorize(bundle, RewhiteLoginType.TYPE_PAYCO);
                        } else {
                            Toast.makeText(aq.getContext(), json.getJSONObject("header").getString("resultMessage"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.i("PAYCO_GET_PROFILE RESULT", json.toString());
                }
            }
        };

        cb.header("client_id", _clientId);
        cb.header("access_token", _accessToken);
        cb.header("Content-Type", "application/json; charset=utf-8");

        Map<String, Object> params = new HashMap<String, Object>();
        JSONObject reqJsonObj = new JSONObject();
        StringEntity se = null;

        try {
            reqJsonObj.put("client_id", _clientId);
            reqJsonObj.put("access_token", _accessToken);
            se = new StringEntity(reqJsonObj.toString());
        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        params.put(AQuery.POST_ENTITY, se);

        DUtil.Log("PAYCO_GET_PROFILE params", params.toString());
        cb.params(params);

        aq.ajax(Constants.PAYCO_GET_PROFILE, JSONObject.class, cb);
    }

    public void requestPaycoSessionOpen(){
        OnLoginListener onLoginListener = new OnLoginListener() {
            @Override
            public void onLogin(PaycoLoginExtraResult paycoLoginExtraResult) {
                Log.d(TAG, "[PaycoLoginManager]login onLogin() call");

                // TODO : 회원서버와 협의된 서비스에서 추가적으로 요구한 데이터 정보를 조회한다.
                // ex) servicePromotionReceiveYn : 서비스용 홍보성 정보 수신 동의(선택적)
                // 추가적인 정보 관련해서 안쓰는 서비스는 해당 정보를 파싱해서 사용하지 않는다.
                String servicePromotionReceiveYn = "";
                Hashtable<String, String> extraInfo = paycoLoginExtraResult.getExtraInfo();
                if (extraInfo.size() > 0) {
                    if (extraInfo.containsKey("servicePromotionReceiveYn")) {
                        servicePromotionReceiveYn = extraInfo.get("servicePromotionReceiveYn");
                    }
                    Log.e("extraInfo", extraInfo.get("idNo"));
                    Log.e("extraInfo", extraInfo.get("id"));
                    Log.e("extraInfo", extraInfo.get("name"));
                }

                Enumeration e = extraInfo.keys();
                while (e.hasMoreElements()) {
                    Integer i = (Integer) e.nextElement();
                    Log.e("extraInfo enum", extraInfo.get(i));
                }

                Log.d(TAG, "servicePromotionReceiveYn:" + servicePromotionReceiveYn);

                Log.e("Payco ID", PaycoLoginManager.getInstance().getLoginId());
                Log.e("Payco AccessToken", PaycoLoginManager.getInstance().getAccessToken());

                if(PaycoLoginManager.getInstance().getAccessToken() != null){
                    // login ok get access token
                    getPaycoProfile(Bootstrap.CLIENT_ID, PaycoLoginManager.getInstance().getAccessToken());
                }
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "[PaycoLoginManager]login onCancel()");

                //showMsg(R.string.cancel_msg);
            }

            @Override
            public void onFail(PaycoLoginError error) {
                Log.d(TAG, "[PaycoLoginManager]login onFail()");

                //showMsg(error.getDisplayMessage());
            }
        };

        PaycoLoginManager.getInstance().login(this, onLoginListener);
    }

    public void requestKakaoSessionOpen(AuthType authType, Activity callerActivity) {
        kakaoSession.open(authType, callerActivity);
    }

    public void requestSessionOpen(Context ctx, String loginId, String secretKey, String loginType) {
        DUtil.Log(TAG, "requestSessionOpen Start :::: " + loginId + " / " + secretKey + " / " + loginType);

        /*
        if(mProgressDialog == null){
            mProgressDialog = new ProgressDialog(Bootstrap.getCurrentActivity());
            mProgressDialog.setMessage("로그인 진행중...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }*/

        isLoginProcessing = true;

        AccessTokenRequest aRequest = AccessTokenRequest.createRequestWithDeviceToken(ctx, loginId, secretKey, loginType, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));
        session.open(aRequest);
    }

    protected void onSessionClosed() {
        Log.e("onSessionClosed", "SignActivity onSessionClosed");
        if ("KK".equals(loginType)) {
            kakaoSession.close();
        } else if ("FB".equals(loginType)) {
            LoginManager.getInstance().logOut();
        }
    }

    protected void onSessionOpened() {

        String accessToken = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN);
        if (StringUtil.isNullOrEmpty(accessToken)) {
            isLoginProcessing = false;
            if (mProgressDialog != null) mProgressDialog.dismiss();

            Log.e("onSessionOpened ", "AccessToken Empty");
            return;
        }else{
            Log.e("onSessionOpened ", "AccessToken existed");
        }

        SharedPreferencesUtility.set(SharedPreferencesUtility.Command.IS_AUTO_LOGIN, "true");

        try {
            if (bundle.containsKey("imagePath")) {
                SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.TEMP_IMAGE, bundle.getString("imagePath"));
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        Log.e("onSessionOpened LOGIN_RESULT_CODE", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_RESULT_CODE));

        if("S0100".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_RESULT_CODE))){
            // 휴면예정
            // SLEEPY_ACCOUNT_WAKED
            Intent alertIntent = new Intent(this, SleepyAccountWakedScreen.class);
            startActivityForResult(alertIntent, SLEEPY_ACCOUNT_WAKED);
        }else if("S0101".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_RESULT_CODE))){
            // 휴면처리 -> 정상 전환
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
            alertDialogBuilder.setCancelable(true).setMessage("장기 미사용으로 인한 휴면계정 전환 대상 고객이십니다.\n" +
                    "[확인]을 누르면 이전과 동일하게 사용하실 수 있어요.")
                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            onSessionOpenedAfter();
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else{
            onSessionOpenedAfter();
        }

    }

    private void onSessionOpenedAfter(){
        String userName = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME);
        String email = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL);
        String mobile = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.PHONE_NO);
        if (StringUtil.isNullOrEmpty(userName) || StringUtil.isNullOrEmpty(mobile) || StringUtil.isNullOrEmpty(email)) {
            // 필수정보 미입력시
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    isLoginProcessing = false;
                    if (mProgressDialog != null) mProgressDialog.dismiss();

                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setClass(mCtx, ProfileNewActivityV2.class);
                    intent.putExtra("bundle", bundle);
                    intent.putExtra("loginType", loginType);
                    mCtx.startActivity(intent);
                    ((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    ((Activity) mCtx).finish();
                }
            }, 200);
        } else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    isLoginProcessing = false;
                    if (mProgressDialog != null) mProgressDialog.dismiss();

                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setClass(mCtx, MainActivity.class);
                    mCtx.startActivity(intent);
                    ((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    ((Activity) mCtx).finish();
                }
            }, 200);
        }
    }

    @Override
    public void onBackPressed() {

        FragmentManager fm = getSupportFragmentManager();
        DUtil.Log(TAG, "[onBackPressed] Back Stack Entry Count : " + fm.getBackStackEntryCount());

        if (fm.getBackStackEntryCount() >= 1) {
            String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
            DUtil.Log(TAG, "[FragmentManager] tag : " + tag + "\nfm.getBackStackEntryCount() : " + fm.getBackStackEntryCount());

            IFragment fragment = (IFragment) fm.findFragmentByTag(tag);
            if (false == fragment.onBackPressed()) {
                fm.popBackStack();
                if (fm.getBackStackEntryCount() == 1) super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    public void buttonTransitionClicked(boolean _isTop) {
        DUtil.Log(TAG, "[buttonLoginBottomClicked] : ");

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        LoginFragment fragment = (LoginFragment) fm.findFragmentByTag(FRAGMENT_LOGIN);

        fragment.screenToggle(_isTop);
    }

    private class KakaoSessionCallback implements ISessionCallback {
        /**
         * 세션이 오픈되었으면 가입페이지로 이동 한다.
         */
        @Override
        public void onSessionOpened() {
            // 뺑글이 종료
            DUtil.Log(TAG, "onSessionOpened");
            requestMe();
        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            if (exception != null) {
                Logger.e(exception);
            }
        }
    }

    // [START revokeAccess]
    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        // [START_EXCLUDE]
                        //updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END revokeAccess]

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    public String loginType;
    private void authorize(final Bundle _bundle, String signType) {
        this.bundle = _bundle;
        this.loginType = signType;
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_TYPE, signType);
        requestSessionOpen(mCtx, _bundle.getString("loginId"), _bundle.getString("secretKey"), signType);
    }
}
