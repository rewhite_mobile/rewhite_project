package me.rewhite.users.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.adapter.PickupItemAdapter;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.PickupType;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;


public class PriceActivity extends BaseActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    public final Context ctx = this;
    private JSONArray pickupItems;
    private AQuery aq;
    private int pickupMode = PickupType.PICKUPTYPE_CLEAN;
    private String priceGroupId;
    private String partnerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price);

        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        Intent intent = getIntent();
        if (intent != null) {
            pickupMode = intent.getIntExtra("pickupMode", PickupType.PICKUPTYPE_CLEAN);
            if(pickupMode == PickupType.PICKUPTYPE_CLEAN){
                aq.id(R.id.title_text).text("세탁 요금표"); // 일반/기본
                Tracker t = ((Bootstrap)getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("기타")
                        .setAction("요금표보기")
                        //.setLabel("Order")
                        .build());
                AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
                logger.logEvent("Show Normal PriceTable");
            }else if(pickupMode == PickupType.PICKUPTYPE_REPAIR){
                aq.id(R.id.title_text).text("수선 요금표"); // 수선
            }else if(pickupMode == PickupType.PICKUPTYPE_TODAY){
                aq.id(R.id.title_text).text("세탁 요금표(당일)"); // 당일
                Tracker t = ((Bootstrap)getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("기타")
                        .setAction("당일세탁요금표")
                        //.setLabel("Order")
                        .build());
                AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
                logger.logEvent("Show Today PriceTable");

            }else if(pickupMode == PickupType.PICKUPTYPE_TOMORROW){
                aq.id(R.id.title_text).text("세탁 요금표(익일)"); // 익일
                Tracker t = ((Bootstrap)getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("기타")
                        .setAction("익일세탁요금표")
                        //.setLabel("Order")
                        .build());
                AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
                logger.logEvent("Show Tomorrow PriceTable");
            }else if(pickupMode == PickupType.PICKUPTYPE_GS25){
                priceGroupId = intent.getStringExtra("priceGroupId");
                partnerId = intent.getStringExtra("partnerId");
                aq.id(R.id.title_text).text("세탁 요금표"); // 익일
                Tracker t = ((Bootstrap)getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("기타")
                        .setAction("GS25세탁요금표")
                        //.setLabel("Order")
                        .build());
                AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
                logger.logEvent("Show GS25 PriceTable");
            }

        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        initialize();
    }

    public void closeClicked(View button) {
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
        //super.onBackPressed();
    }

    private int getTotalPrice(JSONArray data) throws JSONException {
        int price = 0;
        for (int i = 0; i < data.length(); i++) {
            int p = data.getJSONObject(i).getInt("itemPrice");
            int c = data.getJSONObject(i).getInt("itemQuantity");
            price += p * c;
        }
        return price;
    }

    JSONArray itemInfo;

    private void initialize() {

        if(pickupMode == PickupType.PICKUPTYPE_GS25){
            RequestParams params = new RequestParams();
            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            params.put("partnerId", partnerId);
            params.put("priceGroupId", priceGroupId);
            params.put("k", 1);

            NetworkClient.post(Constants.ORDER_PRICE_USER_V3, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                    // TODO Auto-generated method stub
                    DUtil.Log(Constants.ORDER_PRICE_USER_V3, error.getMessage());
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                    String result;
                    try {
                        result = new String(data, "UTF-8");
                        DUtil.Log(Constants.ORDER_PRICE_USER_V3, result);

                        JSONObject jsondata = new JSONObject(result);

                        if ("S0000".equals(jsondata.getString("resultCode"))) {
                            if (jsondata.isNull("data")) {
                                //

                            } else {
                                itemInfo = jsondata.getJSONArray("data");
                                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
                                // Set up the ViewPager with the sections adapter.
                                mViewPager = (ViewPager) findViewById(R.id.container);
                                mViewPager.setAdapter(mSectionsPagerAdapter);

                                TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
                                tabLayout.setupWithViewPager(mViewPager);
                            }
                        } else {

                        }
                    } catch (UnsupportedEncodingException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            });
        }else{
            Intent myIntent = getIntent();
            String storeId = myIntent.getExtras().getString("storeId");

            RequestParams params = new RequestParams();
            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            String pickupModeParam = "G";
            if(pickupMode == PickupType.PICKUPTYPE_CLEAN){
                pickupModeParam = "G"; // 일반/기본
            }else if(pickupMode == PickupType.PICKUPTYPE_REPAIR){
                pickupModeParam = "R"; // 수선
            }else if(pickupMode == PickupType.PICKUPTYPE_TODAY){
                pickupModeParam = "T"; // 당일
            }else if(pickupMode == PickupType.PICKUPTYPE_TOMORROW){
                pickupModeParam = "N"; // 익일
            }
            params.put("mode", pickupModeParam);
            params.put("storeId", storeId);
            params.put("k", 1);

            Log.e("price initialize", params.toString());

            NetworkClient.post(Constants.ORDER_PRICE_USER_V2, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                    // TODO Auto-generated method stub
                    DUtil.Log(Constants.ORDER_PRICE_USER_V2, error.getMessage());
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                    String result;
                    try {
                        result = new String(data, "UTF-8");
                        DUtil.Log(Constants.ORDER_PRICE_USER_V2, result);

                        JSONObject jsondata = new JSONObject(result);

                        if ("S0000".equals(jsondata.getString("resultCode"))) {
                            if (jsondata.isNull("data")) {
                                //

                            } else {
                                itemInfo = jsondata.getJSONArray("data");
                                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
                                // Set up the ViewPager with the sections adapter.
                                mViewPager = (ViewPager) findViewById(R.id.container);
                                mViewPager.setAdapter(mSectionsPagerAdapter);

                                TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
                                tabLayout.setupWithViewPager(mViewPager);
                            }
                        } else {

                        }
                    } catch (UnsupportedEncodingException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            });
        }


    }

    //private String[] itemCategory = {"상의", "하의", "원피스", "아우터", "기타", "악세사리", "이불", "카페트", "커텐"};

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(ctx, position);
        }

        @Override
        public int getCount() {
            return itemInfo.length();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = "";
            try {
                title = itemInfo.getJSONObject(position).getString("itemTitle");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return title;
        }

    }

    /**
     * A placeholder fragment containing a simple view.
     */
    @SuppressLint("ValidFragment")
    public static class PlaceholderFragment extends Fragment {
        private RecyclerView mRecyclerView;
        private RecyclerView.Adapter mAdapter;
        private RecyclerView.LayoutManager mLayoutManager;
        public Context parent;
        private AQuery bq;
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_POSITION = "section_number";
//        private static final String ARG_DATA = "section_data";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(Context ctx, int position) {

            PlaceholderFragment fragment = new PlaceholderFragment(ctx);
            Bundle args = new Bundle();
            args.putInt(ARG_POSITION, position);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        public PlaceholderFragment(Context ctx) {
            parent = ctx;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_price, container, false);
            mRecyclerView = (RecyclerView) rootView.findViewById(R.id.item_list);
            bq = new AQuery(rootView);
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            mRecyclerView.setHasFixedSize(true);

            // use a linear layout manager
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);

            // specify an adapter (see also next example)
            try {

                JSONArray subArr = ((PriceActivity) getActivity()).itemInfo.getJSONObject(getArguments().getInt(ARG_POSITION)).getJSONArray("items");
                if (subArr.length() == 0) {
                    bq.id(R.id.screen_no).visible();
                } else {
                    bq.id(R.id.screen_no).gone();
                }
                mAdapter = new PickupItemAdapter(parent, getArguments().getInt(ARG_POSITION), subArr);
                mRecyclerView.setAdapter(mAdapter);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return rootView;
        }
    }

    private JSONArray sendDataSet = new JSONArray();

    public void addRemoveAction(int category, int position, boolean isAddType) throws JSONException {
        JSONObject item = itemInfo.getJSONObject(category).getJSONArray("items").getJSONObject(position);
        if (isAddType) {
            // 수량추가
            if (item.isNull("itemQuantity")) {
                item.put("itemQuantity", 1);
            } else {
                int quantity = item.getInt("itemQuantity");
                item.put("itemQuantity", quantity + 1);
            }
        } else {
            if (item.isNull("itemQuantity")) {
                //item.put("itemQuantity", 1);
            } else {
                int quantity = item.getInt("itemQuantity");
                if (quantity <= 1) {
                    item.put("itemQuantity", 0);
                } else {
                    item.put("itemQuantity", quantity - 1);
                }
            }
        }

        sendDataSet = createFinalData();
        Log.i("SEND DATA SET", sendDataSet.toString());
        mSectionsPagerAdapter.notifyDataSetChanged();
        mViewPager.invalidate();
    }

    private JSONArray createFinalData() throws JSONException {
        JSONArray tempArr = new JSONArray();

        for (int i = 0; i < itemInfo.length(); i++) {
            JSONArray t = itemInfo.getJSONObject(i).getJSONArray("items");
            for (int j = 0; j < t.length(); j++) {
                if (!t.getJSONObject(j).isNull("itemQuantity")) {
                    int itemId = t.getJSONObject(j).getInt("itemId");
                    int itemPrice = t.getJSONObject(j).getInt("storePrice");
                    int itemQuantity = t.getJSONObject(j).getInt("itemQuantity");
                    String itemTitle = t.getJSONObject(j).getString("itemTitle");

                    if (itemQuantity != 0) {
                        JSONObject data = new JSONObject();
                        data.put("itemId", itemId);
                        data.put("itemPrice", itemPrice);
                        data.put("itemQuantity", itemQuantity);
                        data.put("itemTitle", itemTitle);

                        tempArr.put(data);
                    }
                }
            }
        }

        return tempArr;
    }
}
