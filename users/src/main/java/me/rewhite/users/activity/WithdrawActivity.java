package me.rewhite.users.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.session.RewhiteSession;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class WithdrawActivity extends BaseActivity {

    private AQuery aq;
    private Context mCtx = this;
    private boolean isChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);

        aq = new AQuery(this);

        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
        logger.logEvent("Try leave");

        aq.id(R.id.btn_confirm).clicked(this, "checkAction");
        aq.id(R.id.btn_withdraw).clicked(this, "withdrawAction");
        aq.id(R.id.btn_back).clicked(this, "backAction");
    }

    public void backAction(View button) {
        finish();
    }

    public void checkAction(View button) {
        isChecked = !isChecked;
        if (isChecked) {
            aq.id(R.id.btn_confirm).image(R.mipmap.check_withdraw_desc_on);
        } else {
            aq.id(R.id.btn_confirm).image(R.mipmap.check_withdraw_desc_off);
        }
    }

    Tracker t;
    public void withdrawAction(View button) {
        if (isChecked) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
            alertDialogBuilder.setCancelable(false).setMessage("확인을 누르시면 즉시 탈퇴처리가 됩니다. 진행하시겠습니까?")
                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                            t.send(new HitBuilders.EventBuilder()
                                    .setCategory("가입및탈퇴")
                                    .setAction("탈퇴시도")
                                    //.setLabel("회원탈퇴")
                                    .build());


                            withDrawStart();
                        }
                    }).setNegativeButton("취소", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        } else {
            DUtil.alertShow(this, "체크상자를 선택해주세요.");
        }
    }

    private void withDrawStart() {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);

        NetworkClient.post(Constants.WITHDRAW, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.WITHDRAW, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.WITHDRAW, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        t.send(new HitBuilders.EventBuilder()
                                .setCategory("가입및탈퇴")
                                .setAction("탈퇴완료")
                                //.setLabel("회원탈퇴")
                                .build());
                        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                        logger.logEvent("Leave completed");
                        /*
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                        alertDialogBuilder.setCancelable(false).setMessage("리화이트 앱이 종료됩니다.")
                                .setPositiveButton("네", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        RewhiteSession.getCurrentSession().close();
                                        finish();
                                        moveTaskToBack(true);
                                        finish();
                                        android.os.Process.killProcess(android.os.Process.myPid());
                                    }
                                });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                        */
                        RewhiteSession.getCurrentSession().close();
                        finish();
                    } else {
                        Toast.makeText(mCtx, jsondata.getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }
}
