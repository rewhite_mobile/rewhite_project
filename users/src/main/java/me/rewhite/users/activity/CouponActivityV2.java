package me.rewhite.users.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.R;
import me.rewhite.users.adapter.CouponListItem;
import me.rewhite.users.adapter.CouponListViewAdapter;
import me.rewhite.users.adapter.PointListItem;
import me.rewhite.users.adapter.PointListViewAdapter;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.MZValidator;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.TimeUtil;


public class CouponActivityV2 extends BaseActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private AQuery aq;
    final public Context context = this;
    ProgressDialog dialog;
    String currentMode;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    View decorView;

    @Override
    protected void onResume() {
        super.onResume();
        DUtil.Log("onResume", "==========onResume============");

        getProfileInfo();
        aq.id(R.id.btn_tab_01).image(R.mipmap.tab_l_sel);
        aq.id(R.id.btn_tab_02).image(R.mipmap.tab_r_nor);
        aq.id(R.id.coupon_view).gone();
        aq.id(R.id.point_view).visible();
        aq.id(R.id.title_label).text("포인트 & 코인 내역");
        currentMode = "M";
        getData(currentMode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_v2);


        aq = new AQuery(this);
        aq.id(R.id.title_text).text(getString(R.string.title_coupon_point));
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked").image(R.mipmap.btn_menu_back_black);
        aq.id(R.id.btn_add_coupon).clicked(this, "addCouponClicked");
        aq.id(R.id.btn_add_coin).clicked(this, "addCoinClicked");

        aq.id(R.id.btn_tab_01).clicked(this, "tabClicked").tag(0);
        aq.id(R.id.btn_tab_02).clicked(this, "tabClicked").tag(1);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        if(dialog == null){
            // Progress 처리 진행상황을 보기위해
            dialog = new ProgressDialog(this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.setInverseBackgroundForced(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setMessage(getString(R.string.web_loading_comment));
        }

        getProfileInfo();

        currentMode = "M";
        getData(currentMode);
    }

    public void tabClicked(View button){
        int tag = (int)button.getTag();
        switch(tag){
            case 0:
                aq.id(R.id.btn_tab_01).image(R.mipmap.tab_l_sel);
                aq.id(R.id.btn_tab_02).image(R.mipmap.tab_r_nor);
                aq.id(R.id.coupon_view).gone();
                aq.id(R.id.point_view).visible();
                aq.id(R.id.title_label).text("포인트 & 코인 내역");
                currentMode = "M";
                getData(currentMode);
                break;
            case 1:
                aq.id(R.id.btn_tab_01).image(R.mipmap.tab_l_nor);
                aq.id(R.id.btn_tab_02).image(R.mipmap.tab_r_sel);
                aq.id(R.id.coupon_view).visible();
                aq.id(R.id.point_view).gone();
                aq.id(R.id.title_label).text("MY 쿠폰함");
                currentMode = "C";
                getData(currentMode);
                break;
            default:
                break;
        }
    }

    private void initialize(){
        String availPoint = MZValidator.toNumFormat(Integer.parseInt(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USE_AVAILABLE_POINT)));
        aq.id(R.id.point_label).text(availPoint);//.typeface(CommonUtility.getNanumBarunLightTypeface());
        String presavePoint = MZValidator.toNumFormat(Integer.parseInt(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EXPECT_POINT)));
        aq.id(R.id.pre_point_label).text("익일 적립 예정 " + presavePoint  + "P");//.typeface(CommonUtility.getNanumBarunLightTypeface());

        String coinPoint = MZValidator.toNumFormat(Integer.parseInt(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USE_AVAILABLE_COIN)));
        aq.id(R.id.coin_label).text(coinPoint);//.typeface(CommonUtility.getNanumBarunLightTypeface());


    }

    private void getProfileInfo(){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("deviceToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));
        params.put("k", 1);
        NetworkClient.post(Constants.USER_INFO, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.USER_INFO, error.getMessage());
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.USER_INFO, result);

                    JSONObject jsondata = new JSONObject(result);

                    if (dialog != null) {
                        dialog.dismiss();
                    }

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        JSONObject userInfo = jsondata.getJSONObject("data");
                        String loginType = userInfo.getString("loginType");
                        String loginId = userInfo.getString("loginId");
                        String userId = userInfo.getInt("userId") + "";
                        String userName = userInfo.getString("userName");
                        String shareCode = userInfo.getString("shareCode");
                        String phoneno = userInfo.getString("phone");
                        String email = userInfo.getString("email");
                        String isEmailConfirm = userInfo.getString("isEmailConfirm");
                        String birth = userInfo.getString("birth");
                        String gender = userInfo.getString("gender");
                        String imageBigPath = userInfo.getString("imageBigPath");
                        String imageThumbPath = userInfo.getString("imageThumbPath");
                        String useAvailablePoint = userInfo.getString("useAvailablePoint");
                        String totalEarnedPoint = userInfo.getString("totalEarnedPoint");
                        String expectPoint = userInfo.getString("expectPoint");
                        String useAvailablePrepaymentMoney = userInfo.getString("useAvailablePrepaymentMoney");

                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_TYPE, loginType);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_ID, loginId);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.USER_ID, userId);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.USER_NAME, userName);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.SHARE_CODE, shareCode);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.EMAIL, email);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.EMAIL_AUTH_CHECK, isEmailConfirm);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.PHONE_NO, phoneno);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.BIRTH, birth);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.GENDER, gender);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.PROFILE_IMAGE, imageBigPath);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.PROFILE_THUMB, imageThumbPath);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.USE_AVAILABLE_POINT, useAvailablePoint);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.TOTAL_EARNED_POINT, totalEarnedPoint);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.EXPECT_POINT, expectPoint);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.USE_AVAILABLE_COIN, useAvailablePrepaymentMoney);

                        SharedPreferencesUtility.setLocationData(userInfo.getJSONArray("geoInfo"));

                        initialize();
                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    public void addCouponClicked(View button){

        Intent intent = new Intent(this, AddCouponActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);

    }

    public void addCoinClicked(View button){

        Intent intent = new Intent(this, AddCoinActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);

    }

    public void closeClicked(View button) {
        finish();
    }

    private void getData(String _type) {
        if ("M".equals(_type)) {
            getMileageData();
        } else if ("C".equals(_type)) {
            getCouponData();
        }
    }

    JSONArray pointInfo;
    JSONArray coinInfo;
    JSONArray pointFinalInfo;
    JSONArray couponInfo;

    private void getMileageData() {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("page", 1);
        params.put("block", 100);
        params.put("k", 1);
        NetworkClient.post(Constants.MILEAGE_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.MILEAGE_LIST, error.getMessage());
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.MILEAGE_LIST, result);

                    JSONObject jsondata = new JSONObject(result);

                    if (dialog != null) {
                        dialog.dismiss();
                    }

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            aq.id(R.id.point_label).text("0");
                            //aq.id(R.id.screen_no_point).visible();
                            pointInfo = new JSONArray();
                        } else {
                            pointInfo = jsondata.getJSONArray("data");
                        }
                        getCoinData();
                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    private JSONArray multipleSortArray() throws JSONException {
        JSONArray sortedJsonArray = new JSONArray();
        List<JSONObject> jsonValues = new ArrayList<JSONObject>();
        for (int i = 0; i < pointInfo.length(); i++) {
            jsonValues.add(pointInfo.getJSONObject(i));
        }
        for (int j = 0; j < coinInfo.length(); j++) {
            jsonValues.add(coinInfo.getJSONObject(j));
        }

        Collections.sort( jsonValues, new Comparator<JSONObject>() {
            //You can change "Name" with "ID" if you want to sort by ID
            private static final String KEY_NAME = "registerDateApp";

            @Override
            public int compare(JSONObject a, JSONObject b) {
                long valA = 0;
                long valB = 0;

                try {
                    valA = (long) a.get(KEY_NAME);
                    valB = (long) b.get(KEY_NAME);
                }
                catch (JSONException e) {
                    //do something
                }

                return valB<valA?-1:
                        valB>valA?1:0;
                //if you want to change the sort order, simply use the following:
                //return -valA.compareTo(valB);
            }
        });

        for (int i = 0; i < pointInfo.length() + coinInfo.length(); i++) {
            sortedJsonArray.put(jsonValues.get(i));
        }
        return sortedJsonArray;
    }

    private void getCoinData() {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("page", 1);
        params.put("block", 100);
        params.put("k", 1);
        NetworkClient.post(Constants.COIN_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.COIN_LIST, error.getMessage());
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.COIN_LIST, result);

                    JSONObject jsondata = new JSONObject(result);

                    if (dialog != null) {
                        dialog.dismiss();
                    }

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            aq.id(R.id.coin_label).text("0");
                            coinInfo = new JSONArray();
                        } else {
                            coinInfo = jsondata.getJSONArray("data");
                        }

                        pointFinalInfo = multipleSortArray();

                        ListView pointListView = (ListView)findViewById(R.id.listView);
                        ArrayList<PointListItem> pointData = new ArrayList<>();

                        if (pointFinalInfo.length() > 0) {
                            aq.id(R.id.listView).visible();
                            aq.id(R.id.screen_no).gone();
                            //aq.id(R.id.empty_screen).gone();
                            for (int i = 0; i < pointFinalInfo.length(); i++) {

                                PointListItem aItem;
                                if(pointFinalInfo.getJSONObject(i).isNull("mileage")){
                                    String isSaving = pointFinalInfo.getJSONObject(i).getString("isSaving");
                                    boolean isSavingBool = ("Y".equals(isSaving)) ? true : false;
                                    int money = pointFinalInfo.getJSONObject(i).getInt("money");
                                    String prepaymentValue = pointFinalInfo.getJSONObject(i).getString("prepaymentValue");
                                    long regDate = pointFinalInfo.getJSONObject(i).getLong("registerDateApp");
                                    int prepaymentType = pointFinalInfo.getJSONObject(i).getInt("prepaymentType");
                                    aItem = new PointListItem("C", isSavingBool, money, prepaymentValue, 0, regDate, "", prepaymentType);
                                }else{
                                    String isSaving = pointFinalInfo.getJSONObject(i).getString("isSaving");
                                    boolean isSavingBool = ("Y".equals(isSaving)) ? true : false;
                                    int mileage = pointFinalInfo.getJSONObject(i).getInt("mileage");
                                    String referrer = pointFinalInfo.getJSONObject(i).getString("mileageValue");
                                    long availableDateApp = pointFinalInfo.getJSONObject(i).getLong("availableDateApp");
                                    long regDate = pointFinalInfo.getJSONObject(i).getLong("registerDateApp");
                                    String mileageTypeApp = pointFinalInfo.getJSONObject(i).getString("mileageTypeApp");
                                    int mileageType = pointFinalInfo.getJSONObject(i).getInt("mileageType");
                                    aItem = new PointListItem("M", isSavingBool, mileage, referrer, availableDateApp, regDate, mileageTypeApp, mileageType);
                                }
                                pointData.add(aItem);
                            }

                            PointListViewAdapter pointAdapter = new PointListViewAdapter(context, R.layout.point_list_item, pointData);
                            pointAdapter.notifyDataSetChanged();

                            pointListView.setAdapter(pointAdapter);
                        }else{
                            aq.id(R.id.screen_no).visible();
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    private void getCouponData() {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("applyType", "A");
        //params.put("applyValue", "A");
        params.put("page", 1);
        params.put("block", 100);
        params.put("k", 1);
        NetworkClient.post(Constants.COUPON_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.COUPON_LIST, error.getMessage());
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.COUPON_LIST, result);

                    JSONObject jsondata = new JSONObject(result);

                    if (dialog != null) {
                        dialog.dismiss();
                    }

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        ListView couponListView = (ListView)findViewById(R.id.listView);

                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                            aq.id(R.id.screen_no).visible();
                            aq.id(R.id.listView).gone();
                            aq.id(R.id.usable_coupon_label).text("0");
                            aq.id(R.id.hurry_coupon_label).text("0");
                        } else {
                            couponInfo = jsondata.getJSONArray("data");
                            String count = MZValidator.toNumFormat(couponInfo.length());
                            aq.id(R.id.usable_coupon_label).text(count);

                            ArrayList<CouponListItem> couponData = new ArrayList<>();

                            if (couponInfo.length() > 0) {
                                aq.id(R.id.screen_no).gone();
                                aq.id(R.id.listView).visible();

                                int hurryCount = 0;
                                for (int i = 0; i < couponInfo.length(); i++) {

                                    int couponId = couponInfo.getJSONObject(i).getInt("couponId");
                                    String couponTitle = couponInfo.getJSONObject(i).getString("couponTitle");
                                    String couponType = couponInfo.getJSONObject(i).getString("couponType");
                                    int couponValue = couponInfo.getJSONObject(i).getInt("couponValue");
                                    int couponBaseMoney = couponInfo.getJSONObject(i).getInt("couponBaseMoney");
                                    int couponMaxMoney = couponInfo.getJSONObject(i).getInt("couponMaxMoney");

                                    long useStartDateApp = couponInfo.getJSONObject(i).getLong("useStartDateApp");
                                    long useFinishDateApp = couponInfo.getJSONObject(i).getLong("useFinishDateApp");
                                    long registerDateApp = couponInfo.getJSONObject(i).getLong("registerDateApp");
                                    String isUse = couponInfo.getJSONObject(i).getString("isUse");

                                    CouponListItem aItem = new CouponListItem(couponId, couponTitle, couponType, couponValue, couponBaseMoney, couponMaxMoney, useStartDateApp, useFinishDateApp, registerDateApp, isUse);
                                    couponData.add(aItem);

                                    long finishDateValue = useFinishDateApp;
                                    long leftTime = TimeUtil.getMinuteComparePastTimeFromNow(finishDateValue);
                                    Log.e("lefttime", leftTime + " / " + finishDateValue);
                                    if("Y".equals(isUse)){

                                    }else{
                                        // 173035
                                        if(leftTime > 0 && leftTime < 1*15*24*60){
                                            hurryCount++;
                                        }else{

                                        }
                                    }
                                }
                                aq.id(R.id.hurry_coupon_label).text(hurryCount + "");

                                CouponListViewAdapter pointAdapter = new CouponListViewAdapter(context, R.layout.coupon_list_item, couponData);
                                pointAdapter.notifyDataSetChanged();

                                couponListView.setAdapter(pointAdapter);
                                couponListView.setOnItemClickListener(itemClickListener);
                            } else {
                                aq.id(R.id.listView).gone();
                                aq.id(R.id.screen_no).visible();
                            }
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    private AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener()
    {
        public void onItemClick(AdapterView<?> adapterView, View clickedView, int pos, long id)
        {
            if("C".equals(currentMode)){
                try {
                    Intent intent = new Intent(CouponActivityV2.this, CouponDetailActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("data", couponInfo.getJSONObject(pos).toString());
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{

            }
        }
    };

}
