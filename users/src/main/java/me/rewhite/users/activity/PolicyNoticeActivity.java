package me.rewhite.users.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import com.androidquery.AQuery;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;

public class PolicyNoticeActivity extends BaseActivity {

    private WebView webView;
    AQuery aq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy_notice);

        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
        aq.id(R.id.title_text).text("주문시 유의사항"); // 일반/기본

        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);

        aq.id(R.id.webView).webImage("http://img.rewhite.me/www/img/appdata/policy_alert_02.png");
    }

    public void closeClicked(View button) {
        finish();
    }
}
