package me.rewhite.users.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.applinks.AppLinkData;
import com.google.ads.conversiontracking.AdWordsConversionReporter;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import bolts.AppLinks;
import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.QuickstartPreferences;
import me.rewhite.users.R;
import me.rewhite.users.RegistrationIntentService;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.LoginType;
import me.rewhite.users.common.RewhiteCMD;
import me.rewhite.users.fragment.CustomDialogFragment;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.session.AccessToken;
import me.rewhite.users.session.AccessTokenRequest;
import me.rewhite.users.session.RewhiteException;
import me.rewhite.users.session.RewhiteSession;
import me.rewhite.users.session.RewhiteSessionCallback;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.MZValidator;
import me.rewhite.users.util.RewhiteServerInterface;
import me.rewhite.users.util.RewhiteServerInterface.RewhiteServiceInterfaceListener;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.SharedPreferencesUtility.Command;
import me.rewhite.users.util.StringUtil;

import static me.rewhite.users.util.CommonUtility.SENDER_ID;


public class IntroActivity extends FragmentActivity implements CustomDialogFragment.CustomDialogListener, RewhiteServiceInterfaceListener {

    private static final String TAG = "IntroActivity";

    private final static String EXTRA_KEY = "me.rewhite.users";
    private final static String EXTRA_SHOTCUT = "Rewhite Shortcut";

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private AQuery aq;
    private Context mContext;

    GoogleCloudMessaging mGcm;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;

    private RewhiteServerInterface mServerInterface;

    private final RewhiteSessionCallback mySessionCallback = new RewhiteSessionStatusCallback();
    private RewhiteSession session;

    //IInAppBillingService mService;

    // Push Message Receiver 정의
    //private BroadcastReceiver mHandleMessageReceiver;
    View decorView;
/*
    ServiceConnection mServiceConn = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
        }
    };*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (session != null) session.removeCallback(mySessionCallback);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        /*if (mService != null) {
            unbindService(mServiceConn);
        }*/
        /*
        if (mHandleMessageReceiver != null) {
            unregisterReceiver(mHandleMessageReceiver);
            mHandleMessageReceiver = null;
        }*/

    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        Log.e("outside", "onUserLeaveHint");
        // super.onUserLeaveHint();
        isStopNeed = true;
        // impriortyPackage();
        aq.ajaxCancel();
        AjaxCallback.cancel();

        if (session != null) session.removeCallback(mySessionCallback);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        /*if (mService != null) {
            unbindService(mServiceConn);
        }*/
/*
        if (mHandleMessageReceiver != null) {
            unregisterReceiver(mHandleMessageReceiver);
            mHandleMessageReceiver = null;
        }*/


    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);
        try{
            Bootstrap.getGlobalApplicationContext();
        }catch (IllegalStateException e){
            e.printStackTrace();
        }


        registerReceiver();
        DUtil.Log(TAG, "onResume");

        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            if (checkPlayServices()) {
                if (session != null) {
                    if (session.isOpened()) {
                        IntroActivity.this.onSessionOpened();
                    } else {
                        initialize();
                    }
                } else {
                    initialize();
                }
            }
        } else {
            if (session != null) {
                if (session.isOpened()) {
                    IntroActivity.this.onSessionOpened();
                } else {
                    initialize();
                }
            } else {
                initialize();
            }
        }

    }
    private static final String LAST_RECORDED_VERSION_KEY = "last_recorded_app_version";

    String queryHost = null;
    String destinationType;
    String destParam;
    int orderSubType;
    String viewType;
    String title;
    String comment;
    String image;
    public String referCode;
    String excuteType;
    String authToken;
    String menuIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        //t.enableAdvertisingIdCollection(true);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        AdWordsConversionReporter.reportWithConversionId(this.getApplicationContext(),
                "939419543", "IhfHCKPo12oQl8_5vwM", "0.00", true);

        Log.e("IntroActivity", "onCreate");

        // GCM Received Handle Event
        Intent pIntent = getIntent();
        if (pIntent != null) {
            queryHost = pIntent.getStringExtra("queryHost");
            destinationType = pIntent.getStringExtra("destinationType");
            destParam = pIntent.getStringExtra("destParam");
            orderSubType = pIntent.getIntExtra("orderSubType", 151);
            viewType = pIntent.getStringExtra("viewType");
            title = pIntent.getStringExtra("title");
            comment = pIntent.getStringExtra("comment");
            image = pIntent.getStringExtra("image");
            Log.e("IntroActivity gcm param", queryHost + " / " + destinationType + " / " + destParam + " / " + orderSubType + " / "  + viewType + " / " + title + " / " + comment + " / " + image);
        }

        // gcm param: default / start / 12345 / null /  / null / null / null

        if(pIntent != null){
            excuteType = pIntent.getStringExtra("excuteType");
            if("auth".equals(excuteType)){
                referCode = pIntent.getStringExtra("referCode");
                authToken = pIntent.getStringExtra("token");
                menuIndex = pIntent.getStringExtra("menuIndex");
                SharedPreferencesUtility.set(Command.IS_AUTO_LOGIN, "false");
                SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN, "");
                SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_TYPE, LoginType.SHINHANCARD);
                // 신한카드로부터 앱실행
            }
        }

        /*
        UPDATE SharedPreference Value
         */
        if(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USE_AVAILABLE_COIN) == null){
            SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.USE_AVAILABLE_COIN, "0");
        }
        /*

        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && Intent.ACTION_MAIN.equals(intent.getAction())) {
                Log.w(TAG, "Main Activity is not the root.  Finishing Main Activity instead of launching.");
                finish();
                return;
            }
        }*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR);//.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.navigationBarColor));
        }

        decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_LAYOUT_FLAGS);

        aq = new AQuery(this);

//        Animation anim = AnimationUtils.loadAnimation(this, R.anim.riseup);
//        aq.id(R.id.logo_image).animate(anim);

        mServerInterface = new RewhiteServerInterface(this);
        mServerInterface.setInterface(this);

        DUtil.Log(TAG, "onCreate");

        mContext = this;
        session = RewhiteSession.getInstance(mContext);


        CommonUtility.loadTypeface(this);
/*
        GCMRegistrar.checkDevice(mContext);
        GCMRegistrar.checkManifest(mContext);

        mHandleMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
                Log.i("GCM onReceive", newMessage);
            }
        };*/

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    //mInformationTextView.setText(getString(R.string.gcm_send_message));
                } else {
                   // mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };
        // Registering BroadcastReceiver
        registerReceiver();

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);

            mGcm = GoogleCloudMessaging.getInstance(this);
            getGCMID();
        }
        /*
        registerReceiver(mHandleMessageReceiver, new IntentFilter(
                DISPLAY_MESSAGE_ACTION));
*/

		/*
		 * 앱실행시 Local Storage(SQLite) 점검 (강제삭제의 경우 대비)
		 */
        // 계정정보DB 생성여부 체크
        boolean memo2Result = isCheckDB(this, Constants.DB_NAME);

        DUtil.Log("REWHITE DB Check", "DB2 Check=" + memo2Result);
        if (!memo2Result) { // DB가 없으면 복사
            DUtil.Log("REWHITE DB Check", "REWHITE DB NO EXIST");
            copyDB(this, Constants.DB_NAME);
        }

        Uri targetUrl =
                AppLinks.getTargetUrlFromInboundIntent(this, getIntent());
        if (targetUrl != null) {
            Log.i("Activity", "App Link Target URL: " + targetUrl.toString());
            String refCode = targetUrl.getQueryParameter("referral");
            Log.i("Activity", "Referral Code: " + refCode);
            if(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.REFER) == null || "".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.REFER))){
                SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.REFER, refCode);
            }else{
                Log.e("Refer already", "이미 추천인코드가 등록되어있습니다.");
            }

        } else {
            AppLinkData.fetchDeferredAppLinkData(
                    this,
                    new AppLinkData.CompletionHandler() {
                        @Override
                        public void onDeferredAppLinkDataFetched(AppLinkData appLinkData) {
                            //process applink data
                        }
                    });
        }

    }


    private long splashDelay = 1000;
    boolean isStopNeed = false;

    public void requestSessionOpen(Context ctx, String loginId, String secretKey, String loginType) {
        DUtil.Log(TAG, "requestSessionOpen Start :::: " + loginId + " / " + secretKey + " / " + loginType);

        AccessTokenRequest aRequest = AccessTokenRequest.createRequestWithDeviceToken(ctx, loginId, secretKey, loginType, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));
        session.open(aRequest);
    }

    private void checkExcuted() {
        if("auth".equals(excuteType) && "SHINHAN".equals(referCode)){
            /*
            TODO 신한카드 전달받은 변수값 전달
             */
            session.addCallback(mySessionCallback);
            DUtil.Log(Constants.SHINHAN_AUTH_ACCESSTOKEN, excuteType + " / " + referCode);

            RequestParams params = new RequestParams();
            params.put("token", authToken);
            NetworkClient.post(Constants.SHINHAN_AUTH_ACCESSTOKEN, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                    // TODO Auto-generated method stub
                    DUtil.Log(Constants.SHINHAN_AUTH_ACCESSTOKEN, error.getMessage());
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                    String result;
                    try {
                        result = new String(data, "UTF-8");
                        JSONObject jsondata = new JSONObject(result);
                        DUtil.Log(Constants.SHINHAN_AUTH_ACCESSTOKEN, jsondata.toString());

                        if ("S0000".equals(jsondata.getString("resultCode"))) {
                            JSONObject json = jsondata.getJSONObject("data");;

                            // Login Response값 Authorize 세팅처리
                            SharedPreferencesUtility.set(SharedPreferencesUtility.Command.IS_AUTO_LOGIN, "true");
                            SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_TYPE, LoginType.SHINHANCARD);

                            String userId = json.getString("userId");
                            requestSessionOpen(mContext, userId, authToken, LoginType.SHINHANCARD);

                        }else{
                            // 신한카드 로그인 실패
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                            alertDialogBuilder.setCancelable(false).setMessage("신한 FAN앱을 통한 로그인처리가 실패했습니다. 로그인화면으로 이동합니다.")
                                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            defaultLoginProcess();
                                        }
                                    });
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                    } catch (UnsupportedEncodingException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            });

        }else{
            defaultLoginProcess();
        }
    }

    private void defaultLoginProcess(){
        Log.e("defaultLoginProcess", "in");


        if (!"true".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Command.IS_FIRST_STARTED))) {
            Log.e("defaultLoginProcess", "IS_FIRST_STARTED");
            Intent tutorialIntent = new Intent(IntroActivity.this, GetStartedActivity.class);
            tutorialIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(tutorialIntent);
            overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        } else {
            if (session == null) {
                session = RewhiteSession.getInstance(mContext);
            }
            String accessToken = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN);
            DUtil.Log(TAG, "ACCESS_TOKEN : " + accessToken);

            if (StringUtil.isNullOrEmpty(accessToken)) {
                IntroActivity.this.onSessionClosed();
                session.addCallback(mySessionCallback);
            } else {
                IntroActivity.this.onSessionOpened();
            }

        }

        //Log.e("IS_FIRST_STARTED", SharedPreferencesUtility.get(SharedPreferencesUtility.Command.IS_FIRST_STARTED));
    }

    private void appVersionCheck() {
        RequestParams params = new RequestParams();
        params.put("k", 1);
        NetworkClient.post(Constants.VERSION_CHECK, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.VERSION_CHECK, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {

                    result = new String(data, "UTF-8");
                    JSONObject jsondata = new JSONObject(result);
                    JSONObject json = jsondata.getJSONObject("data");

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        String version = json.getString("au");

                        int myVerNum = 0;
                        PackageManager m = mContext.getPackageManager();

                        // 설치된 패키지의 버전이름 추출 : A.B.C.D 스타일
                        String app_ver = m.getPackageInfo(mContext.getPackageName(), 0).versionName;
                        Log.i("appver", version + " / " + app_ver);

                        // 구분자 '.'를 기반으로 5자리 Integer값으로 변환.
                        // 각 Middle, Minor 버전값은 99를 넘지 못한다.
                        myVerNum = MZValidator.convertVersionStringToInteger(app_ver);

                        // 추출된 버전값을 Preferences값에 저장한다.
                        //SharedPreferencesUtility.set(cmd, value);

                        // 서버에서 최신앱 버전정보를 조회하여 5자리 값으로 변환.
                        int currentVerNum = MZValidator.convertVersionStringToInteger(version);
                        Log.i("AppVersionNum", currentVerNum + "/" + myVerNum);

                        boolean isStrict = false;


                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                        alertDialogBuilder.setCancelable(false).setMessage(getResources().getString(R.string.appupdate_minor))
                                .setPositiveButton("업데이트", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setData(Uri.parse("market://details?id=" + DUtil.getAppPackageName(mContext)));
                                        startActivity(intent);
                                        //finish();
                                    }
                                });

                        if (currentVerNum - myVerNum <= 0) {
                            checkExcuted();
                        } else {
                            if (currentVerNum - myVerNum >= 1000) {
                                // 강제업데이트
                                alertDialogBuilder.setCancelable(false);
                            } else {
                                alertDialogBuilder.setCancelable(true).setNegativeButton("확인", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // 설치된 버전값이 서버값보다 작을 경우 update알림 :
                                        // Minor업데이트의 경우 필수업데이트가 아님.
                                        checkExcuted();
                                    }
                                });
                            }

                            if(!IntroActivity.this.isFinishing()) {
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }

                        }

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    checkExcuted();
                } catch (NameNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    //Mint.logException(e);
                }

            }
        });

    }

    private void getGCMID() {
//        String gcmId = "";
//
//        final String regId = GCMRegistrar.getRegistrationId(this);
//        if (StringUtil.isNullOrEmpty(regId)) {
//            // Automatically registers application on startup.
//            GCMRegistrar.register(this, SENDER_ID);
//        } else {
//            // Device is already registered on GCM, check server.
//            if (GCMRegistrar.isRegisteredOnServer(this)) {
//                // Skips registration.
//                Log.i("GCM", "GCM already_registered" + "\n");
//                Log.e("GCM ID", regId);
//            } else {
//                Log.e("GCM ID", regId);
//            }
//            SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.GCM_ID, regId);
//        }

        new AsyncTask<Void, Void, String>(){
            @Override
            protected String doInBackground(Void... params) {
                String msg;
                try{
                    if(mGcm == null){
                        mGcm = GoogleCloudMessaging.getInstance(getApplicationContext());

                    }
                    msg = mGcm.register(SENDER_ID);
                }catch (IOException e){
                    msg = "";
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg){
                if(!msg.equals("")){
                    Log.e("GCM ID onPostExecute", msg);
                    SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.GCM_ID, msg);
                }
            }
        }.execute(null, null, null);

    }

    private void initialize() {

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("deviceToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));
        params.put("k", 1);
        NetworkClient.post(Constants.AUTH_ACCESS_TOKEN, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.AUTH_ACCESS_TOKEN, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.AUTH_ACCESS_TOKEN, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        //getGCMID();
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                appVersionCheck();
                            }
                        }, 1000);
                    } else {
                        SharedPreferencesUtility.set(Command.IS_AUTO_LOGIN, "false");
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN, "");
                        //getGCMID();
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                appVersionCheck();
                            }
                        }, 1000);
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (RewhiteSession.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) {
                return;
            }
        } catch (IllegalStateException e) {
            RewhiteSession.initialize(this);
            return;
        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    private class RewhiteSessionStatusCallback implements RewhiteSessionCallback {

        /**
         * 세션이 오픈되었으면 가입페이지로 이동 한다.
         */
        @Override
        public void onSessionOpened() {
            // 뺑글이 종료

            // 프로그레스바를 보이고 있었다면 중지하고 세션 오픈후 보일 페이지로 이동
            IntroActivity.this.onSessionOpened();
            DUtil.Log(TAG, "onSessionOpened");
        }

        @Override
        public void onSessionOpening() {
            // 뺑글이 시작
        }

        /**
         * 세션이 삭제되었으니 로그인 화면이 보여야 한다.
         *
         * @param exception 에러가 발생하여 close가 된 경우 해당 exception
         */
        @Override
        public void onSessionClosed(RewhiteException exception) {
            // TODO Auto-generated method stub
            IntroActivity.this.onSessionClosed();
            DUtil.Log(TAG, "onSessionClosed = " + exception.getLocalizedMessage());
        }
    }

    protected void onSessionClosed() {

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mContext, SignActivity.class);
                mContext.startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                //((Activity) mContext).finish();
            }
        }, 1000);

    }

    Bundle _bundle;

    protected void onSessionOpened() {

        if (!"true".equals(SharedPreferencesUtility.get(Command.IS_AUTO_LOGIN))) {
            AccessToken.clearAccessTokenFromCache();
            onSessionClosed();
        } else {

            String userName = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME);
            String mobile = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.PHONE_NO);
            String email = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL);
            String imagePath = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.TEMP_IMAGE);

            _bundle = new Bundle();
            _bundle.putString("nickName", userName);
            _bundle.putString("imagePath", imagePath);

            if (StringUtil.isNullOrEmpty(userName) || StringUtil.isNullOrEmpty(mobile) || StringUtil.isNullOrEmpty(email)) {
                // 필수정보 미입력시
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        Intent intent = new Intent();
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setClass(mContext, ProfileNewActivityV2.class);
                        intent.putExtra("bundle", _bundle);
                        startActivity(intent);
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        //((Activity) mContext).finish();
                    }
                }, 500);
            } else {
                if("auth".equals(excuteType) && "SHINHAN".equals(referCode) && menuIndex != null){
                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    if("1".equals(menuIndex)){
                        // 주문하기
                        intent.setClass(mContext, MainActivity.class);
                    }else if("2".equals(menuIndex)){
                        // 배송지 등록
                        intent.setClass(mContext, LocationActivity.class);
                    }else if("3".equals(menuIndex)){
                        // 프로필 수정
                        intent.setClass(mContext, ProfileActivity.class);
                    }else if("4".equals(menuIndex)){
                        // 내 지갑
                        intent.setClass(mContext, CouponActivityV2.class);
                    }else if("5".equals(menuIndex)){
                        // 공유하기
                        intent.setClass(mContext, ShareActivity.class);
                    }else if("7".equals(menuIndex)){
                        // 이벤트
                        intent.setClass(mContext, EventListActivity.class);
                    }else if("8".equals(menuIndex)){
                        // 이용약관
                        intent.setClass(mContext, TermsDetailView.class);
                        intent.putExtra("TERMS_TITLE", "이용약관");
                        intent.putExtra("TERMS_URI", Constants.TERMS_USE);
                    }else if("9".equals(menuIndex)){
                        // 주문내역목록
                        intent.setClass(mContext, OrderHistoryActivity.class);
                    }else if("10".equals(menuIndex)){
                        // 공지사항
                        intent.setClass(mContext, HtmlActivity.class);
                        intent.putExtra("TERMS_URI", Constants.HTML_NOTICE);
                        intent.putExtra("TITLE_NAME", getString(R.string.title_notice));
                    }
                    startActivity(intent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }else{
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {

                            Intent intent = new Intent();
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.setClass(mContext, MainActivity.class);

                            if (queryHost != null) {
                                DUtil.Log(TAG, "MainActivity params call");
                                intent.putExtra("queryHost", queryHost);
                                intent.putExtra("destinationType", destinationType);
                                intent.putExtra("destParam", destParam);
                                intent.putExtra("orderSubType", orderSubType);
                                intent.putExtra("viewType", viewType);
                                intent.putExtra("title", title);
                                DUtil.Log(TAG, queryHost + " / " + destinationType);
                            }

                            startActivity(intent);
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                            //((Activity) mContext).finish();
                        }
                    }, 500);
                }

            }

        }

    }

    private void registerReceiver(){
        if(!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }

    // google play service가 사용가능한가
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    // DB가 있나 체크하기
    public boolean isCheckDB(Context mContext, String _dbname) {

        String filePath = this.getApplicationContext().getFilesDir() + Constants.PACKAGE_NAME + "/databases/" + _dbname;
        File file = new File(filePath);

        return file.exists();

    }

    // DB를 복사하기
    // assets의 /db/xxxx.db 파일을 설치된 프로그램의 내부 DB공간으로 복사하기
    public void copyDB(Context mContext, String _dbname) {
        DUtil.Log("MiniApp", "copyDB");
        AssetManager manager = this.getApplicationContext().getAssets();
        String folderPath = this.getApplicationContext().getFilesDir() + Constants.PACKAGE_NAME + "/databases";
        String filePath = this.getApplicationContext().getFilesDir() + Constants.PACKAGE_NAME + "/databases/" + _dbname;
        File folder = new File(folderPath);
        File file = new File(filePath);

        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            InputStream is = manager.open(_dbname);
            BufferedInputStream bis = new BufferedInputStream(is);

            if (folder.exists()) {
            } else {
                folder.mkdirs();
            }

            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }

            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            int read = -1;
            byte[] buffer = new byte[1024];
            while ((read = bis.read(buffer, 0, 1024)) != -1) {
                bos.write(buffer, 0, read);
            }

            bos.flush();

            bos.close();
            fos.close();
            bis.close();
            is.close();

        } catch (IOException e) {
            DUtil.Log("ErrorMessage : ", e.getMessage());
        }

    }

    @Override
    public void onResult(String command, String result) {
        Log.d(TAG, "[onResult] : command : " + command + " , result : " + result);

        if (command.equals(RewhiteCMD.VERSION)) {
            if (result == null) {
                // TODO : 서버 정기 정검 팝업
                // 네트워크 상태 체크 해서 네트워크가 끊어져 있는 상태가 아니라면 팝업 띄움.

            } else {
                JSONObject o = null;
                try {
                    o = new JSONObject(result);
                    if (o.getBoolean(RewhiteCMD.JSON_RESULT)) {
                        String version = o.getString(RewhiteCMD.JSON_VERSION);
                        Log.d(TAG, "get version : " + version + ", app ver : " + CommonUtility.getAppVersion(this));

                        String[] versionArr = version.split(".");
                        String[] aVersionArr = CommonUtility.getAppVersion(this).split(".");

                        boolean isUpdate = false;
                        for (int i = 0; i < aVersionArr.length; i++) {
                            int sVersion = Integer.valueOf(versionArr[i]);
                            int aVersion = Integer.valueOf(aVersionArr[i]);

                            if (aVersion < sVersion) {
                                isUpdate = true;
                                break;
                            }
                        }

                        if (isUpdate) {
                            CustomDialogFragment dialog = CustomDialogFragment.newInstance(CustomDialogFragment.TYPE_UPDATE_APP, null);
                            dialog.show(getSupportFragmentManager(), String.valueOf(CustomDialogFragment.TYPE_UPDATE_APP));
                        } else {
                            // 다음 단계로
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, int mode, String param) {
        if (mode == CustomDialogFragment.TYPE_UPDATE_APP) {
            // 앱 종료
            finish();
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog, int mode, String param) {
        if (mode == CustomDialogFragment.TYPE_UPDATE_APP) {
            // 업데이트
            //            Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
            //            marketLaunch.setData(Uri.parse("market://details?id="));
            //            startActivity(marketLaunch);
            //            finish();
        }
    }
}
