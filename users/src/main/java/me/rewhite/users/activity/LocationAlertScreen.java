package me.rewhite.users.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.users.R;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.ToastUtility;

public class LocationAlertScreen extends AppCompatActivity {

    private AQuery aq;
    String title;
    String message;
    public Context ctx = this;

    String currentLat;
    String currentLon;
    String currentAddress;
    String currentDetailAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        setContentView(R.layout.fragment_location_invalid);

        Intent intent = getIntent();
        if (intent != null) {
            currentLon = intent.getStringExtra("longitude");
            currentLat = intent.getStringExtra("latitude");
            currentAddress = intent.getStringExtra("address1");
            currentDetailAddress = intent.getStringExtra("address2");
        }

        aq = new AQuery(this);

        aq.id(R.id.btn_no).clicked(this, "okClicked");
        aq.id(R.id.btn_ok).clicked(this, "noClicked");

    }

    public void noClicked(View button) {
        Log.e("okCilcked", "NO");
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    public void okClicked(View button) {
        Log.e("okCilcked", "OK");
        registerAlarmList();
    }

    private void registerAlarmList() {
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("phone", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.PHONE_NO));
        params.put("longitude", currentLon);
        params.put("latitude", currentLat);
        params.put("address1", currentAddress);
        params.put("address2", currentDetailAddress);
        params.put("k", 1);
        NetworkClient.post(Constants.ADD_ALARM_NEW, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ADD_ALARM_NEW, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {

                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ADD_ALARM_NEW, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        ToastUtility.show(ctx, jsondata.getString("message"), Toast.LENGTH_SHORT);
                        Intent resultData = new Intent();
                        resultData.putExtra("result", true);
                        setResult(Activity.RESULT_OK, resultData);
                        finish();
                    } else if ("S9006".equals(jsondata.getString("resultCode"))) {
                        // 중복 등록
                        ToastUtility.show(ctx, jsondata.getString("message"), Toast.LENGTH_SHORT);
                        Intent resultData = new Intent();
                        resultData.putExtra("result", true);
                        setResult(Activity.RESULT_OK, resultData);
                        finish();
                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

}
