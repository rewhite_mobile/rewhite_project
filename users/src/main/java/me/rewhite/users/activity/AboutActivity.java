package me.rewhite.users.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.androidquery.AQuery;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;

public class AboutActivity extends BaseActivity {

    AQuery aq;
    Context ctx = this;

    String FacebookPageId = "293162404141109";
    String wwwAddress = "http://www.rewhite.me";
    String BlogAddress = "http://blog.naver.com/rewhiteme";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
        aq.id(R.id.btn_www).clicked(this, "buttonClicked").tag(1);
        aq.id(R.id.btn_blog).clicked(this, "buttonClicked").tag(2);
        aq.id(R.id.btn_facebook).clicked(this, "buttonClicked").tag(3);

        aq.id(R.id.text_term).clicked(this, "termsClicked");
        aq.id(R.id.text_opensource).clicked(this, "opensourceClicked");
    }

    public void closeClicked(View button) {
        finish();
    }

    public Intent getOpenFacebookIntent(Context context) {

        String facebookUrl = "https://www.facebook.com/rewhite.page";

        try {
            int versionCode = getPackageManager().getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) {
                Uri uri = Uri.parse("fb://facewebmodal/f?href=" + facebookUrl);
                return new Intent(Intent.ACTION_VIEW, uri);
            } else {
                // open the Facebook app using the old method (fb://profile/id or fb://pro
                return new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/" + FacebookPageId));
            }

        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl));
        }
    }

    public void buttonClicked(View button) {
        int tag = (int) button.getTag();
        switch (tag) {
            case 1:
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(wwwAddress));
                startActivity(i);
                break;
            case 2:
                Intent j = new Intent(Intent.ACTION_VIEW, Uri.parse(BlogAddress));
                startActivity(j);
                break;
            case 3:
                startActivity(getOpenFacebookIntent(this));
                break;
            default:
                break;
        }
    }

    public void termsClicked(View button) {
        Intent termsIntent = new Intent().setClass(AboutActivity.this, TermsActivity.class);
        termsIntent.putExtra("section", 0);
        startActivity(termsIntent);
    }

    public void opensourceClicked(View button) {
        Intent intent = new Intent().setClass(AboutActivity.this, TermsDetailView.class);
        intent.putExtra("TERMS_TITLE", getString(R.string.title_opensource_license));
        intent.putExtra("TERMS_URI", Constants.TERMS_OSS);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
