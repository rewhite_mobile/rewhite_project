package me.rewhite.users.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidquery.AQuery;

import org.apache.http.util.EncodingUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.layout.INIP2PWebView;
import me.rewhite.users.layout.NicepayWebView;
import me.rewhite.users.layout.UPlusWebView;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.Utility;


@SuppressLint({"NewApi", "JavascriptInterface", "SetJavaScriptEnabled"})
public class PaymentBridgeActivity extends BaseActivity {

    public static final String TAG = "PaymentBridgeActivity";
    private AQuery aq;
    WebView webView;
    ProgressDialog dialog;
    public Context ctx = this;

    Toast toast = null;
    String pamId;
    private Handler handler = new Handler();

    private static final int LAUNCHED_ACTIVITY = 0;

    private int CURRENT_PAYMENT = 4;
    private static final int PAYMENT_UPLUS = 1;
    private static final int PAYMENT_KAKAOPAY = 2;
    private static final int PAYMENT_INICIS = 3;
    private static final int PAYMENT_NICEPAY = 4;
    private static final int PAYMENT_PAYCO = 5;

    public static final String INTENT_PROTOCOL_START = "intent:";
    public static final String INTENT_PROTOCOL_INTENT = "#Intent;";
    public static final String INTENT_PROTOCOL_END = ";end";
    public static final String PACKAGE_NAME_START = "package=";
    public static final String GOOGLE_PLAY_STORE_PREFIX = "market://details?id=";
    public static final String KAKAOTALK_CUSTOM_URL = "kakaotalk:";
    public static final String SCHEME_NAME = "scheme=";
    public static final String KAKAOTALK_PACKAGE_NAME = "com.kakao.talk";

    private static final int DIALOG_PROGRESS_WEBVIEW = 0;
    private static final int DIALOG_PROGRESS_MESSAGE = 1;
    private static final int DIALOG_ISP = 2;
    private static final int DIALOG_CARDAPP = 3;
    private static String DIALOG_CARDNM = "";
    private AlertDialog alertIsp;
    private boolean isPaymentSuccess = false;

    final String ISP_LINK =  "market://details?id=kvp.jjy.MispAndroid320";				// ISP 설치 링크
    final String KFTC_LINK = "market://details?id=com.kftc.bankpay.android";			//	금융결제원 설치 링크

    final String MERCHANT_URL = "https://web.nicepay.co.kr/smart/mainPay.jsp";	// 가맹점의 결제 요청 페이지 URL

    private String NICE_BANK_URL = "";	// 계좌이체  인증후 거래 요청 URL

    private WebView mWebView;			// 웹뷰 인스턴스

    // ISP앱에서 결제후 리턴받을 스키마 이름을 설정합니다.
    // AndroidManaifest.xml에 명시된 값과 동일한 값을 설정하십시요.
    // 스키마 뒤에 ://를 붙여주십시요.
    private String WAP_URL = "RewhitePay"+"://";

    private String BANK_TID = "";		// 계좌이체 거래시 인증ID

    protected void onResume() { // Custom url scheme
        if (getIntent() != null) {
            Uri uri = getIntent().getData();
            if (uri != null) {
                //DLP창이 있는 화면으로 재진입시 스키마가 있으면 진입
                if (uri.toString().contains("ok")) {
                    //return_url설정에 TestMerchant://ok로 설정
                    webView.loadUrl("javascript:kakaopayDlp.getAuthInfo()");
                } else {
                    //cancel_url설정에 TestMerchant://cancel로 설정
                    webView.loadUrl("javascript:kakaopayDlp.closeServerDlp()");
                }
            }
        }
        super.onResume();
    }

    private void initKeyBoardListener() {
        // Минимальное значение клавиатуры. Threshold for minimal keyboard height.
        final int MIN_KEYBOARD_HEIGHT_PX = 150;
        // Окно верхнего уровня view. Top-level window decor view.
        final View decorView = getWindow().getDecorView();
        // Регистрируем глобальный слушатель. Register global layout listener.
        decorView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            // Видимый прямоугольник внутри окна. Retrieve visible rectangle inside window.
            private final Rect windowVisibleDisplayFrame = new Rect();
            private int lastVisibleDecorViewHeight;

            @Override
            public void onGlobalLayout() {
                decorView.getWindowVisibleDisplayFrame(windowVisibleDisplayFrame);
                final int visibleDecorViewHeight = windowVisibleDisplayFrame.height();

                if (lastVisibleDecorViewHeight != 0) {
                    if (lastVisibleDecorViewHeight > visibleDecorViewHeight + MIN_KEYBOARD_HEIGHT_PX) {
                        Log.d("Pasha", "SHOW");
                    } else if (lastVisibleDecorViewHeight + MIN_KEYBOARD_HEIGHT_PX < visibleDecorViewHeight) {
                        Log.d("Pasha", "HIDE");
                        webView.clearFocus();
                    }
                }
                // Сохраняем текущую высоту view до следующего вызова.
                // Save current decor view height for the next call.
                lastVisibleDecorViewHeight = visibleDecorViewHeight;
            }
        });
    }

    public void loadURL(String _url){
        webView.loadUrl(_url);
    }

    /**
     *
     * 	계좌이체 결과값을 받아와 오류시 해당 메세지를, 성공시에는 결과 페이지를 호출한다.
     *
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        String resVal = data.getExtras().getString("bankpay_value");
        String resCode = data.getExtras().getString("bankpay_code");
        Log.i("NICE","resCode : "+ resCode);
        Log.i("NICE","resVal : "+ resVal);

        String postData1 = String.format("accessToken=%s&pg=%d&pamId=%s", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN), CURRENT_PAYMENT, pamId);

        if("091".equals(resCode)){//계좌이체 결제를 취소한 경우
            Utility.AlertDialog("인증 오류", "계좌이체 결제를 취소하였습니다.",  PaymentBridgeActivity.this);
            webView.postUrl(Constants.CHECK_ORDER, EncodingUtils.getBytes(postData1, "BASE64"));
        }else if("060".equals(resCode)){
            Utility.AlertDialog("인증 오류", "타임아웃",  PaymentBridgeActivity.this);
            webView.postUrl(Constants.CHECK_ORDER, EncodingUtils.getBytes(postData1, "BASE64"));
        }else if("050".equals(resCode)){
            Utility.AlertDialog("인증 오류", "전자서명 실패",  PaymentBridgeActivity.this);
            webView.postUrl(Constants.CHECK_ORDER, EncodingUtils.getBytes(postData1, "BASE64"));
        }else if("040".equals(resCode)){
            Utility.AlertDialog("인증 오류", "OTP/보안카드 처리 실패",  PaymentBridgeActivity.this);
            webView.postUrl(Constants.CHECK_ORDER, EncodingUtils.getBytes(postData1, "BASE64"));
        }else if("030".equals(resCode)){
            Utility.AlertDialog("인증 오류", "인증모듈 초기화 오류",  PaymentBridgeActivity.this);
            webView.postUrl(Constants.CHECK_ORDER, EncodingUtils.getBytes(postData1, "BASE64"));
        }else if("000".equals(resCode)){	// 성공일 경우
            String postData = "callbackparam2="+BANK_TID+"&bankpay_code="+resCode+"&bankpay_value="+resVal;
            webView.postUrl(NICE_BANK_URL,EncodingUtils.getBytes(postData,"euc-kr"));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_html);


        //CURRENT_PAYMENT = PAYMENT_UPLUS;
        CURRENT_PAYMENT = PAYMENT_NICEPAY;

        aq = new AQuery(this);
        aq.id(R.id.title_text).text("결제하기");
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        webView = (WebView) findViewById(R.id.webview_area);
        webView.setHorizontalScrollBarEnabled(false);

        webView.clearCache(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);

        // KAKAO Related
        webView.getSettings().getUserAgentString();
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setDatabaseEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webView.getSettings().setSupportMultipleWindows(false);
        webView.getSettings().setSavePassword(false);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            CookieManager cookieManager = android.webkit.CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.setAcceptThirdPartyCookies(webView, true);
        }

        if (CURRENT_PAYMENT == PAYMENT_UPLUS) {
            webView.setWebViewClient(new UPlusWebView());
            webView.addJavascriptInterface(new AndroidBridge(this), "RewhiteInterface");
        } else if (CURRENT_PAYMENT == PAYMENT_INICIS) {
            webView.setWebViewClient(new INIP2PWebView());
            webView.addJavascriptInterface(new AndroidBridge(this), "RewhiteInterface");
        }else if (CURRENT_PAYMENT == PAYMENT_NICEPAY) {
            webView.setWebViewClient(new NicepayWebView());
            webView.addJavascriptInterface(new AndroidBridge(this), "RewhiteInterface");
        }

        webView.setWebChromeClient(new MyWebChromeClient());
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        initKeyBoardListener();

        Intent myIntent = getIntent();
        pamId = myIntent.getExtras().getString("PAM_ID");

        Log.e("PAM ID ::::::::::", pamId);

        String urlString = null;
        Uri uri = myIntent.getData();
        if (uri != null){
            String url = uri.toString(); Log.i("NICE","NicePay onCreate url :" + url); if (url.startsWith(WAP_URL)) {
                // 결제결과 url을 설정한다.
                urlString = url;
                urlString = url.substring(WAP_URL.length()); mWebView.loadUrl(urlString);
            }
        }else{
            // 1:U+, 3:Inicis
            String postData = String.format("accessToken=%s&pg=%d&pamId=%s", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN), CURRENT_PAYMENT, pamId);
            webView.postUrl(Constants.CHECK_ORDER, EncodingUtils.getBytes(postData, "BASE64"));
        }

        // Progress 처리 진행상황을 보기위해
        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.web_loading_comment));
    }

    public void closeClicked(View button) {
        if (isPaymentSuccess) {
            Intent resultData = new Intent();
            resultData.putExtra("RESULT", "Y");
            setResult(Activity.RESULT_OK, resultData);
            finish();
        } else {
            finish();
        }
    }

    public void processCancel(){
        Intent resultData = new Intent();
        resultData.putExtra("RESULT", "N");
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (isPaymentSuccess) {
            Intent resultData = new Intent();
            resultData.putExtra("RESULT", "Y");
            setResult(Activity.RESULT_OK, resultData);
            finish();
        } else {
            Intent resultData = new Intent();
            resultData.putExtra("RESULT", "N");
            setResult(Activity.RESULT_CANCELED, resultData);
            finish();
            //super.onBackPressed();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN){
            switch(keyCode)
            {
                case KeyEvent.KEYCODE_BACK:
                    if (isPaymentSuccess) {
                        Intent resultData = new Intent();
                        resultData.putExtra("RESULT", "Y");
                        setResult(Activity.RESULT_OK, resultData);
                        finish();
                    } else {
                        finish();//webView.goBack();
                    }
                    return true;
                case KeyEvent.KEYCODE_ENTER:
                    mWebView.goBack();
                    return true;

            }

        }else{
            if (isPaymentSuccess) {
                Intent resultData = new Intent();
                resultData.putExtra("RESULT", "Y");
                setResult(Activity.RESULT_OK, resultData);
                finish();
            } else {
                finish();//webView.goBack();
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    public void showAlert(String message, String positiveButton, DialogInterface.OnClickListener positiveListener, String negativeButton, DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage(message);
        alert.setPositiveButton(positiveButton, positiveListener);
        alert.setNegativeButton(negativeButton, negativeListener);
        alert.setCancelable(true);
        alert.show();
    }

    private class MyWebChromeClient extends WebChromeClient {

        ProgressBar pb_item01 = (ProgressBar) findViewById(R.id.pb_item01);

        public void onProgressChanged(WebView view, int progress) {
            pb_item01.setProgress(progress); // ProgressBar값 설정
            PaymentBridgeActivity.this.setProgress(progress * 1000);

            if (progress == 100) { // 모두 로딩시 Progressbar를 숨김
                pb_item01.setVisibility(View.GONE);
            } else {
                pb_item01.setVisibility(View.VISIBLE);
            }
        }

        //javascript의 alert과 confirm을 인식하기 위해서는 onJsAlert과 onJsConfirm 메서드가 있어야 합니다.
        @Override
        public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result) {

            final WebView wv = view;
            final String msg = message;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(wv.getContext())
                            .setMessage(msg)
                            .setPositiveButton(android.R.string.ok,
                                    new AlertDialog.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            result.confirm();
                                        }
                                    })
                            .setCancelable(true)
                            .create()
                            .show();
                }
            });


            return true;
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, final android.webkit.JsResult result) {
            final WebView wv = view;
            final String msg = message;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(wv.getContext())
                            .setMessage(msg)
                            .setPositiveButton(android.R.string.ok,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            result.confirm();
                                        }
                                    })
                            .setNegativeButton(android.R.string.cancel,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            result.cancel();
                                        }
                                    })
                            .setCancelable(true)
                            .create()
                            .show();
                }
            });

            return true;
        }

    }


    //Javascript Interface에 정의되는 클래스

    public class AndroidBridge {
        private Context mContext;

        AndroidBridge(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void setSuccess(final String arg) {
            handler.post(new Runnable() {
                public void run() {
                    if ("Y".equals(arg)) {
                        Log.d(TAG, "setSuccess(" + arg + ")");
                        isPaymentSuccess = true;
                    }
                }
            });
        }

        //웹페이지에서 호출될  임의의 함수를 정의합니다.
        public void callAndroid(final String arg) {
            handler.post(new Runnable() {
                public void run() {
                    Log.d(TAG, "callAndroid(" + arg + ")");
                    Toast.makeText(PaymentBridgeActivity.this, "자바스크립트에 의해 호출된 함수에서 찍는 값:" + arg, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    // 외부 앱 호출
    public boolean callApp(String url) {
        Intent intent = null;
        // 인텐트 정합성 체크 : 2014 .01추가
        Log.e("Browser", "callApp URI : " + url );

        try {
            intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
            Log.e("getScheme     +++===>", intent.getScheme());
            Log.e("getDataString +++===>", intent.getDataString());
        } catch (URISyntaxException ex) {
            Log.e("Browser", "Bad URI " + url + ":" + ex.getMessage());
            return false;
        }
        try {
            boolean retval = true;
            //chrome 버젼 방식 : 2014.01 추가
            if (url.startsWith("intent")) { // chrome 버젼 방식
                // 앱설치 체크를 합니다.
                if (getPackageManager().resolveActivity(intent, 0) == null) {
                    String packagename = intent.getPackage();
                    if (packagename != null) {
                        Uri uri = Uri.parse("market://search?q=pname:" + packagename);
                        intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                        retval = true;
                    }
                } else {
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setComponent(null);
                    try {
                        if (startActivityIfNeeded(intent, -1)) {
                            retval = true;
                        }
                    } catch (ActivityNotFoundException ex) {
                        retval = false;
                    }
                }
            } else { // 구 방식
                Uri uri = Uri.parse(url);
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                retval = true;
            }
            return retval;
        } catch (ActivityNotFoundException e) {
            Log.e("error ===>", e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
        int keyCode = event.getKeyCode();
        if ((keyCode == KeyEvent.KEYCODE_DPAD_LEFT) && view.canGoBack()) {

            if (isPaymentSuccess) {
                Intent resultData = new Intent();
                resultData.putExtra("RESULT", "Y");
                setResult(Activity.RESULT_OK, resultData);
                finish();
                return true;
            } else {
                view.goBack();//finish();
                return true;
            }

        } else if ((keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) && view.canGoForward()) {
            view.goForward();
            return true;
        }
        return false;
    }

    // DownloadFileTask생성 및 실행
    public void downloadFile(String mUrl) {
        new DownloadFileTask().execute(mUrl);
    }

    // AsyncTask<Params,Progress,Result>
    private class DownloadFileTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            URL myFileUrl = null;
            try {
                myFileUrl = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream is = conn.getInputStream();

                // 다운 받는 파일의 경로는 sdcard/ 에 저장되며 sdcard에 접근하려면 uses-permission에
                // android.permission.WRITE_EXTERNAL_STORAGE을 추가해야만 가능.
                String mPath = "sdcard/v3mobile.apk";
                FileOutputStream fos;
                File f = new File(mPath);
                if (f.createNewFile()) {
                    fos = new FileOutputStream(mPath);
                    int read;
                    while ((read = is.read()) != -1) {
                        fos.write(read);
                    }
                    fos.close();
                }

                return "v3mobile.apk";
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }

        @Override
        protected void onPostExecute(String filename) {
            if (!"".equals(filename)) {
                Toast.makeText(getApplicationContext(), "download complete", Toast.LENGTH_SHORT).show();

                // 안드로이드 패키지 매니저를 사용한 어플리케이션 설치.
                File apkFile = new File(Environment.getExternalStorageDirectory() + "/" + filename);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
                startActivity(intent);
            }
        }
    }

    public void setPaymentSuccess(boolean _isSuccess) {
        if (_isSuccess) {
            isPaymentSuccess = _isSuccess;
            webView.clearHistory();
        }
    }


    // App 체크 메소드 // 존재:true, 존재하지않음:false
    public static boolean isPackageInstalled(Context ctx, String pkgName) {
        try {
            ctx.getPackageManager().getPackageInfo(pkgName, PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.e("===============", "onNewIntent!!");
        if (intent != null) {
            if (Intent.ACTION_VIEW.equals(intent.getAction())) {
                Uri uri = intent.getData();
                Log.e("================uri", uri.toString());
                if (String.valueOf(uri).startsWith("ISP 커스텀스키마를 넣어주세요")) { // ISP 커스텀스키마를 넣어주세요
                    String result = uri.getQueryParameter("result");
                    if ("success".equals(result)) {
                        webView.loadUrl("javascript:doPostProcess();");
                    } else if ("cancel".equals(result)) {
                        webView.loadUrl("javascript:doCancelProcess();");
                    } else {
                        webView.loadUrl("javascript:doNoteProcess();");
                    }
                } else if (String.valueOf(uri).startsWith("계좌이체 커스텀스키마를 넣어주세요")) { // 계좌이체 커스텀스키마를 넣어주세요
                    // 계좌이체는 WebView가 아무일을 하지 않아도 됨
                } else if (String.valueOf(uri).startsWith("paypin 커스텀스키마를 넣어주세요")) { // paypin 커스텀스키마를 넣어주세요
                    webView.loadUrl("javascript:doPostProcess();");
                } else if (String.valueOf(uri).startsWith("paynow 커스텀스키마를 넣어주세요")) { // paynow 커스텀스키마를 넣어주세요
                    // paynow는 WebView가 아무일을 하지 않아도 됨
                }
            }
        }

        super.onNewIntent(intent);
        setIntent(intent);
    }

    private void showToast(String message) {
        if (toast == null) {
            toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
            toast.show();
        } else {
            toast.setText(message);
            toast.show();
        }
    }

    public void goToPlayStore(String url) {
        Log.d(TAG, "goToPlayStore()");
        Intent intent = null;
        if (url.startsWith(KAKAOTALK_CUSTOM_URL)) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_STORE_PREFIX + KAKAOTALK_PACKAGE_NAME));
        } else if (url.startsWith(INTENT_PROTOCOL_START)) {
            final int packageStartIndex =
                    url.indexOf(PACKAGE_NAME_START) + PACKAGE_NAME_START.length();
            final int packageEndIndex =
                    url.indexOf(INTENT_PROTOCOL_END);
            Log.d(TAG, "packageStartIndex : " + packageStartIndex
                    + ", packageEndIndex : " + packageEndIndex);
            final String packageName =
                    url.substring(packageStartIndex, packageEndIndex < 0 ? url.length() : packageEndIndex);
            Log.d(TAG, "packageName : " + packageName);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(GOOGLE_PLAY_STORE_PREFIX + packageName));
        }
        startActivity(intent);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    @SuppressWarnings("unused")
    private AlertDialog getCardInstallAlertDialog(final String coCardNm) {

        final Hashtable<String, String> cardNm = new Hashtable<String, String>();
        cardNm.put("HYUNDAE", "현대 앱카드");
        cardNm.put("SAMSUNG", "삼성 앱카드");
        cardNm.put("LOTTE", "롯데 앱카드");
        cardNm.put("SHINHAN", "신한 앱카드");
        cardNm.put("KB", "국민 앱카드");
        cardNm.put("HANASK", "하나SK 통합안심클릭");
        cardNm.put("SHINHAN_SMART",  "Smart 신한앱");

        final Hashtable<String, String> cardInstallUrl = new Hashtable<String, String>();
        cardInstallUrl.put("HYUNDAE", "market://details?id=com.hyundaicard.appcard");
        cardInstallUrl.put("SAMSUNG", "market://details?id=kr.co.samsungcard.mpocket");
        cardInstallUrl.put("LOTTE", "market://details?id=com.lotte.lottesmartpay");
        cardInstallUrl.put("LOTTEAPPCARD", "market://details?id=com.lcacApp");
        cardInstallUrl.put("SHINHAN", "market://details?id=com.shcard.smartpay");
        cardInstallUrl.put("KB", "market://details?id=com.kbcard.cxh.appcard");
        cardInstallUrl.put("HANASK", "market://details?id=com.ilk.visa3d");
        cardInstallUrl.put("SHINHAN_SMART",  "market://details?id=com.shcard.smartpay");//여기 수정 필요!!2014.04.01

        AlertDialog alertCardApp = new AlertDialog.Builder(PaymentBridgeActivity.this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("알림")
                .setMessage(cardNm.get(coCardNm) + " 어플리케이션이 설치되어 있지 않습니다. \n설치를 눌러 진행 해 주십시요.\n취소를 누르면 결제가 취소 됩니다.")
                .setPositiveButton("설치", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String installUrl = cardInstallUrl.get(coCardNm);
                        Uri uri = Uri.parse(installUrl);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        Log.d("<INIPAYMOBILE>", "Call : " + uri.toString());
                        try {
                            startActivity(intent);
                        } catch (ActivityNotFoundException anfe) {
                            Toast.makeText(PaymentBridgeActivity.this, cardNm.get(coCardNm) + "설치 url이 올바르지 않습니다", Toast.LENGTH_SHORT).show();
                        }
                        //finish();
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(PaymentBridgeActivity.this, "(-1)결제를 취소 하셨습니다.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                })
                .create();

        return alertCardApp;

    }//end getCardInstallAlertDialog

    protected Dialog onCreateDialog(int id) {//ShowDialog


        switch (id) {

            case DIALOG_PROGRESS_WEBVIEW:
                ProgressDialog dialog = new ProgressDialog(PaymentBridgeActivity.this);
                dialog.setMessage("로딩중입니다. \n잠시만 기다려주세요.");
                dialog.setIndeterminate(true);
                dialog.setCancelable(true);
                return dialog;

            case DIALOG_PROGRESS_MESSAGE:
                break;


            case DIALOG_ISP:

                alertIsp = new AlertDialog.Builder(PaymentBridgeActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("알림")
                        .setMessage("모바일 ISP 어플리케이션이 설치되어 있지 않습니다. \n설치를 눌러 진행 해 주십시요.\n취소를 누르면 결제가 취소 됩니다.")
                        .setPositiveButton("설치", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                String ispUrl = "http://mobile.vpay.co.kr/jsp/MISP/andown.jsp";
                                webView.loadUrl(ispUrl);
                                finish();
                            }
                        })
                        .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Toast.makeText(PaymentBridgeActivity.this, "(-1)결제를 취소 하셨습니다.", Toast.LENGTH_SHORT).show();
                                finish();
                            }

                        })
                        .create();

                return alertIsp;

            case DIALOG_CARDAPP:
                return getCardInstallAlertDialog(DIALOG_CARDNM);

        }//end switch

        return super.onCreateDialog(id);

    }//end onCreateDialog


    // NICEPAY
    /**
     *
     * 계좌이체 데이터를 파싱한다.
     *
     * @param str
     * @return
     */
    public String makeBankPayData(String str)
    {
        String[] arr = str.split("&");
        String[] parse_temp;
        HashMap<String, String> tempMap = new HashMap<String,String>();

        for(int i=0;i<arr.length;i++){
            try{
                parse_temp = arr[i].split("=");
                tempMap.put(parse_temp[0], parse_temp[1]);
            }catch(Exception e){

            }
        }

        BANK_TID = tempMap.get("user_key");
        NICE_BANK_URL = tempMap.get("callbackparam1");
        return str;
    }



    /**
     * 	ISP가 설치되지 않았을때 처리를 진행한다.
     *
     *
     */
    public void installISP()
    {
        AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setMessage("ISP결제를 하기 위해서는 ISP앱이 필요합니다.\n설치 페이지로  진행하시겠습니까?");
        d.setTitle( "ISP 설치" );
        d.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(ISP_LINK));
                startActivity(intent);
            }
        });
        d.setNegativeButton("아니요", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                //결제 초기 화면을 요청합니다.
                String postData = String.format("accessToken=%s&pg=%d&pamId=%s", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN), CURRENT_PAYMENT, pamId);
                webView.postUrl(Constants.CHECK_ORDER, EncodingUtils.getBytes(postData, "BASE64"));

            }
        });
        d.show();
    }
    /**
     * 	계좌이체 BANKPAY 설치 진행 안내
     *
     *
     */
    public void installKFTC()
    {
        AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setMessage("계좌이체 결제를 하기 위해서는 BANKPAY 앱이 필요합니다.\n설치 페이지로  진행하시겠습니까?");
        d.setTitle( "계좌이체 BANKPAY 설치" );
        d.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(KFTC_LINK));
                startActivity(intent);
            }
        });
        d.setNegativeButton("아니요", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                String postData = String.format("accessToken=%s&pg=%d&pamId=%s", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN), CURRENT_PAYMENT, pamId);
                webView.postUrl(Constants.CHECK_ORDER, EncodingUtils.getBytes(postData, "BASE64"));
            }
        });
        d.show();
    }

}
