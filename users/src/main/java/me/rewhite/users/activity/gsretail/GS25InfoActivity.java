package me.rewhite.users.activity.gsretail;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.PriceActivity;
import me.rewhite.users.adapter.GSStoreRecyclerViewAdapter;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.PickupType;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.thread.GpsInfo;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class GS25InfoActivity extends BaseActivity {

    private static final String TAG = "GS25InfoActivity";
    private Context mContext;
    private AQuery aq;
    int orderSubType;

    int pickupMode = PickupType.PICKUPTYPE_GS25;

    private GpsInfo gps;
    JSONArray storeList;

    double lat;
    double lng;

    private boolean hasLocation;
    private static final String[] INITIAL_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final String[] LOCATION_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int INITIAL_REQUEST=1337;
    private static final int LOCATION_REQUEST=INITIAL_REQUEST+3;
    boolean isGpsGet = false;

    @Override
    public void onDestroy() {
        super.onDestroy();
        DUtil.Log("LocationActivity onDestroy", "==========onDestroy============");
        if (gps != null) {
            gps.stopUsingGPS();
        }
    }
    /**
     * 다른 화면으로 넘어갈 때, 일시정지 처리
     */
    @Override
    public void onPause() {
        //Activity LifrCycle 관련 메서드는 무조건 상위 메서드 호출 필요
        super.onPause();
        DUtil.Log("LocationActivity onPause", "==========onPause============");
        if (gps != null) {
            gps.stopUsingGPS();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gs25_info);

        aq = new AQuery(this);

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        Intent intent = getIntent();
        if(intent != null){
            orderSubType = intent.getIntExtra("orderSubType", 151);
        }

        if(orderSubType == 152){
            aq.id(R.id.text_title).text("리화이트 플레이스");
            aq.id(R.id.step2_image).image(R.mipmap.theplace_info_image_02);
            aq.id(R.id.step3_image).image(R.mipmap.theplace_info_image_03);
        }

        if ( Build.VERSION.SDK_INT >= 23){
            if (!canAccessLocation()) {
                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
            }else{
                isGpsGet = true;
                //
                getLocationInfo();
            }
        }else{
            isGpsGet = true;
            //
            getLocationInfo();
        }

        setMaterialRippleLayout((View)findViewById(R.id.btn_01));
        setMaterialRippleLayout((View)findViewById(R.id.btn_02));
        setMaterialRippleLayout((View)findViewById(R.id.btn_03));

        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        aq.id(R.id.btn_01).clicked(this, "step1Clicked");
        aq.id(R.id.btn_02).clicked(this, "step2Clicked");
        aq.id(R.id.btn_03).clicked(this, "step3Clicked");
    }
    public void closeClicked(View button) {
        finish();
    }

    private void setMaterialRippleLayout(View _target){
        MaterialRippleLayout.on(_target)
                .rippleColor(Color.parseColor("#FFffff"))
                .rippleAlpha(0.2f)
                .rippleHover(true)
                .rippleOverlay(true)
                .create();
    }

    public void step1Clicked(View button){

        aq.id(R.id.btn_01).background(R.mipmap.back_gs25_info_topmenu_selected);
        aq.id(R.id.btn_02).background(R.mipmap.back_gs25_info_topmenu_normal);
        aq.id(R.id.btn_03).background(R.mipmap.back_gs25_info_topmenu_normal);

        aq.id(R.id.recyclerview).visible();
        aq.id(R.id.step2).gone();
        aq.id(R.id.step3).gone();
    }

    public void step2Clicked(View button){

        aq.id(R.id.btn_01).background(R.mipmap.back_gs25_info_topmenu_normal);
        aq.id(R.id.btn_02).background(R.mipmap.back_gs25_info_topmenu_selected);
        aq.id(R.id.btn_03).background(R.mipmap.back_gs25_info_topmenu_normal);

        aq.id(R.id.recyclerview).gone();
        aq.id(R.id.step2).visible();
        aq.id(R.id.step3).gone();
    }

    public void step3Clicked(View button){

        aq.id(R.id.btn_01).background(R.mipmap.back_gs25_info_topmenu_normal);
        aq.id(R.id.btn_02).background(R.mipmap.back_gs25_info_topmenu_normal);
        aq.id(R.id.btn_03).background(R.mipmap.back_gs25_info_topmenu_selected);

        aq.id(R.id.recyclerview).gone();
        aq.id(R.id.step2).gone();
        aq.id(R.id.step3).visible();
    }

    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch(requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    isGpsGet = true;
                    getLocationInfo();
                }
                else {
                    Toast.makeText(this, "위치정보설정이 사용자에 의해 비활성화되어있습니다.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
    private boolean canAccessLocation() {
        return(hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
    }

    public void getLocationInfo() {
        gps = new GpsInfo(GS25InfoActivity.this);
        // GPS 사용유무 가져오기
        if (gps.isGetLocation()) {

            lat = gps.getLatitude();
            lng = gps.getLongitude();

//            Toast.makeText(
//                    getApplicationContext(),
//                    "당신의 위치 - \n위도: " + lat + "\n경도: " + lng,
//                    Toast.LENGTH_LONG).show();
            initialize();
        } else {
            // GPS 를 사용할수 없으므로
            gps.showSettingsAlert();
        }
    }

    private void initialize(){


        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        //params.put("partnerId", "7");
        if(orderSubType == 152){
            params.put("isGS", "N");
        }else{
            params.put("isGS", "Y");
        }
        params.put("longitude", lng);
        params.put("latitude", lat);
        params.put("k", 1);

        Log.e("storeList req", params.toString());

        NetworkClient.post(Constants.GS_STORE_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GS_STORE_LIST, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GS_STORE_LIST, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            storeList = jsondata.getJSONArray("data");

                            GSStoreRecyclerViewAdapter adapter = new GSStoreRecyclerViewAdapter(storeList, GS25InfoActivity.this);
                            RecyclerView myView =  (RecyclerView)findViewById(R.id.recyclerview);
                            myView.setHasFixedSize(true);
                            myView.setAdapter(adapter);
                            LinearLayoutManager llm = new LinearLayoutManager(GS25InfoActivity.this);
                            llm.setOrientation(LinearLayoutManager.VERTICAL);
                            myView.setLayoutManager(llm);
//                            myView.addOnItemTouchListener(
//                                    new RecyclerItemClickListener(mContext, myView, new RecyclerItemClickListener.OnItemClickListener() {
//                                        @Override
//                                        public void onItemClick(View view, int position) {
//
//
//                                        }
//
//                                        @Override
//                                        public void onLongItemClick(View view, int position) {
//                                            // do whatever
//                                        }
//                                    })
//                            );
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    public void showMap(int position){
        // do whatever
        Intent intent = new Intent(this, GSStoreDetailTMapActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("sourceType", "current");
        try {
            intent.putExtra("longitude", storeList.getJSONObject(position).getString("longitude") + "");
            intent.putExtra("latitude", storeList.getJSONObject(position).getString("latitude") + "");
            intent.putExtra("title", storeList.getJSONObject(position).getString("partnerStoreName"));
            intent.putExtra("addressName", storeList.getJSONObject(position).getString("partnerStoreAddress1"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        startActivity(intent);
    }

    public void showPriceTable(String priceGroupId, int partnerId){
        Intent intent = new Intent(this, PriceActivity.class);
        intent.putExtra("partnerId", partnerId+"");
        intent.putExtra("priceGroupId", priceGroupId);
        intent.putExtra("pickupMode", pickupMode);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }


}
