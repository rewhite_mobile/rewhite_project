package me.rewhite.users.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.session.AccessToken;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.SharedPreferencesUtility.Command;

//import com.actionbarsherlock.app.SherlockActivity;


public class GetStartedActivity extends AppCompatActivity {

    private static final String TAG = "GetStartedActivity";
    private Context mContext;
    private WalkthroughAdapter mPagerAdapter;
    private AQuery aq;
    ImageButton button = null;
    View decorView;

    private final int mPageCount = 5;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getstarted);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR);//.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.navigationBarColor));
        }

        decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_LAYOUT_FLAGS);

        aq = new AQuery(this);

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        aq.id(R.id.bottom_area).gone();

        mContext = this;
        mPagerAdapter = new WalkthroughAdapter();

        button = (ImageButton) findViewById(R.id.layout_walkthrough_btn);
        button.setOnClickListener(mOnClickStartBtn);
        button.setVisibility(View.GONE);

        ViewPager pager = (ViewPager) findViewById(R.id.activity_walkthrough_layout);
        pager.setAdapter(mPagerAdapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        //aq.id(R.id.indicator_image).image(R.mipmap.g_indicator_01).visible();
                        button.setVisibility(View.GONE);
                        aq.id(R.id.bottom_area).gone();
                        break;

                    case 1:
                        //aq.id(R.id.indicator_image).image(R.mipmap.g_indicator_02).visible();
                        button.setVisibility(View.GONE);
                        aq.id(R.id.bottom_area).gone();
                        break;

                    case 2:
                        //aq.id(R.id.indicator_image).image(R.mipmap.g_indicator_03).visible();
                        button.setVisibility(View.GONE);
                        aq.id(R.id.bottom_area).gone();
                        break;

                    case 3:
                        //aq.id(R.id.indicator_image).image(R.mipmap.g_indicator_04).visible();
                        button.setVisibility(View.GONE);
                        aq.id(R.id.bottom_area).gone();
                        break;

                    case 4:
                        //aq.id(R.id.indicator_image).image(R.mipmap.g_indicator_05).gone();
                        button.setVisibility(View.VISIBLE);
                        aq.id(R.id.bottom_area).visible();
                        break;

                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        PageIndicatorLayout indicator = (PageIndicatorLayout) findViewById(R.id.layout_menu_indicator);
//        indicator.setPager(pager);
//        indicator.initilize(mPageCount);
    }

    private class WalkthroughAdapter extends PagerAdapter {
        @Override
        public int getCount() {
            return mPageCount;
        }

        @Override
        public void destroyItem(View pager, int position, Object view) {
            ((ViewPager) pager).removeView((View) view);
        }

        @Override
        public Object instantiateItem(View pager, int position) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            RelativeLayout view = null;
            ImageView imageView = null;
            Button button = null;

            switch (position) {
                case 0:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.getstarted_bg01);
                    ((ViewPager) pager).addView(view, 0);
                    break;

                case 1:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.getstarted_bg02);
                    ((ViewPager) pager).addView(view, 0);
                    break;

                case 2:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.getstarted_bg03);
                    ((ViewPager) pager).addView(view, 0);
                    break;

                case 3:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.getstarted_bg04);
                    ((ViewPager) pager).addView(view, 0);
                    break;
                case 4:
                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough, null);
                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
                    imageView.setImageResource(R.mipmap.getstarted_bg05);
                    ((ViewPager) pager).addView(view, 0);


                    break;

                default:
                    break;
            }

            return view;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }
    }

    private View.OnClickListener mOnClickStartBtn = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SharedPreferencesUtility.set(Command.IS_FIRST_STARTED, "true");
            AccessToken.clearAccessTokenFromCache();

            Intent intent = new Intent(mContext, SignActivity.class);
            //intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_NEW_TASK);

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            //intent.setClass(mContext, SignActivity.class);

            startActivity(intent);
            finish();
        }
    };

}
