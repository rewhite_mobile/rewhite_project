package me.rewhite.users.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.splunk.mint.Mint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.gsretail.GS25OrderDetailActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.fragment.IFragment;
import me.rewhite.users.fragment.LeftMenuFragment;
import me.rewhite.users.fragment.MainFragment;
import me.rewhite.users.session.AccessToken;
import me.rewhite.users.session.RewhiteException;
import me.rewhite.users.session.RewhiteSession;
import me.rewhite.users.session.RewhiteSessionCallback;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.ToastUtility;

public class MainActivity extends SlidingFragmentActivity implements IFragment.OnFragmentInteractionListener {

    public static final String FRAGMENT_MAIN = "main";
    public static final String FRAGMENT_LIST = "itemList";
    public static final String FRAGMENT_SETTING = "settings";

    public static final int INVALID_LOCATION = 7;
    public static final int CHOICE_LOCATION = 8;
    public static final int ORDER_LIST_REFRESH = 9;
    public static final int CHOICE_STORE = 10;
    private static final int ORDER_REVIEWED = 5;
    //public static Activity MActivity;

    private static final String TAG = "MainActivity";
    public final Context mCtx = this;
    public SharedPreferences preferences;
    public String reservedFileId;
    private boolean mIsTouchDisable; // 연속 터치로 생기는 이슈 막기 위함
    private boolean mFlagFinish;
    private FrameLayout mFragmentLayout;
    private Handler mHandler;
    private Runnable mTask;
    private Fragment mContent;

    private final RewhiteSessionCallback rewhiteSessionCallback = new RewhiteSessionStatusCallback();
    private RewhiteSession session;

    private CharSequence mTitle;
    private String currentFragname;

    private View decorView;
    private int uiOption;

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("MainActivity", "onDestroy");
        session.removeCallback(rewhiteSessionCallback);
        mHandler.removeCallbacks(mTask);
    }



    //@TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("MainActivity", "onCreate");

        setContentView(R.layout.activity_main);

        //MActivity = MainActivity.this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR);//.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }

        decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_LAYOUT_FLAGS);

        preferences = getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        //t.setScreenName(TAG);
        t.set("&uid", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_ID));
        // Send a screen view.
        t.send(new HitBuilders.EventBuilder().setCategory("USER").setAction("User Sign In").build());

        mHandler = new Handler();
        mTask = new Runnable() {

            @Override
            public void run() {
                mFlagFinish = false;
            }
        };

        session = RewhiteSession.getInstance(this);
        session.addCallback(rewhiteSessionCallback);

        //showFragment(FRAGMENT_MAIN);
        setupSlideMenu(savedInstanceState);

        if (!"true".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Command.IS_TUTORIAL))) {
            Intent intent = new Intent(this, TutorialActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(intent);
        }

        // Push 메세지 수신 분기 제어부분
        Intent pIntent = getIntent();
        if (pIntent != null) {
            String queryHost = pIntent.getStringExtra("queryHost");
            String destinationType = pIntent.getStringExtra("destinationType");
            String destParam = pIntent.getStringExtra("destParam");
            int orderSubType =  pIntent.getIntExtra("orderSubType", 151);

            String viewType = pIntent.getStringExtra("viewType");
            String title = pIntent.getStringExtra("title");

            Log.e("MainActivity gcm param", queryHost + " / " + destinationType + " / " + destParam + " / " + viewType);

            if("system".equals(queryHost)) {

                if ("order".equals(destinationType)) {
                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    // GS주문인 경우 분기
                    if(orderSubType == 151 || orderSubType == 152){
                        intent.setClass(mCtx, GS25OrderDetailActivity.class);
                        intent.putExtra("orderSubType", orderSubType);
                    }else{
                        intent.setClass(mCtx, OrderDetailActivity.class);
                    }
                    intent.putExtra("orderId", destParam);
                    startActivityForResult(intent, ORDER_LIST_REFRESH);
                    overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                } else if ("review".equals(destinationType)) {
                    Intent reviewIntent = new Intent(mCtx, ReviewActivity.class);
                    reviewIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    //reviewIntent.setAction(Intent.ACTION_GET_CONTENT);
                    reviewIntent.putExtra("orderId", destParam);
                    reviewIntent.putExtra("storeId", "");
                    startActivityForResult(reviewIntent, ORDER_REVIEWED);
                    overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                } else if ("point".equals(destinationType)) {
                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setClass(mCtx, CouponActivityV2.class);
                    intent.putExtra("position", destParam);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
                } else {

                }
            }else if("web".equals(queryHost)){
                Intent intent = new Intent(this, HtmlActivity.class);
                intent.putExtra("TERMS_URI", destParam);
                intent.putExtra("TITLE_NAME", title);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
            }else if("event".equals(queryHost)){

            }
        }

        onSessionOpened();

        Mint.setUserIdentifier(AccessToken.createFromCache().getUserKey());

        if(Constants.TAPI_HOST.equals(Constants.API_HOST)){
            //ToastUtility.show(this, "실서버가 아닌 테스트서버를 바라보고있는 DEBUG 버전입니다.");
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
            alertDialogBuilder.setCancelable(true).setMessage("실서버가 아닌 테스트서버를 바라보고있는 DEBUG 버전입니다.")
                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    private void setupSlideMenu(Bundle savedInstanceState) {
        // check if the content frame contains the menu frame
        // 좌측 슬라이드메뉴를 세팅한다.
        if (findViewById(R.id.menu_frame) == null) {
            setBehindContentView(R.layout.menu_frame);
            getSlidingMenu().setSlidingEnabled(true);
            getSlidingMenu()
                    .setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
            // show home as up so we can toggle
            // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } else {
            // add a dummy view
            View v = new View(this);
            setBehindContentView(v);
            getSlidingMenu().setSlidingEnabled(false);
            getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        }

		/*
         * // set the Above View Fragment if (savedInstanceState != null){
		 * if(fragmentIndex != 1){ selectIndexFragment(1); mContent = new
		 * HomeFragment(1); if (mContent != null) { switchContent(mContent); }
		 * }else{ mContent =
		 * getSupportFragmentManager().getFragment(savedInstanceState,
		 * "mContent"); } }else if (mContent == null){ mContent = new
		 * HomeFragment(1); }
		 */
        selectIndexFragment(1);
        mContent = new MainFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, mContent).commit();
        // set the Behind View Fragment
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.menu_frame, new LeftMenuFragment()).commit();

        // customize the SlidingMenu
        SlidingMenu sm = getSlidingMenu();
        // 슬라이드메뉴가 열리는 정도를 dimen.slidingmenu_offset에 설정한다.
        sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        sm.setShadowWidthRes(R.dimen.shadow_width);
        sm.setShadowDrawable(R.drawable.shadow);
        sm.setBehindScrollScale(0.5f);
        sm.setFadeDegree(0.25f);

        getSlidingMenu().setSlidingEnabled(true);
        getSlidingMenu()
                .setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
    }

    @Override
    public void onResume() {
        super.onResume();

//        try{
//            Bootstrap.getGlobalApplicationContext();
//        }catch (IllegalStateException e){
//            e.printStackTrace();
//        }
        /*
        if (fragmentIndex == 1) {
            ((MainFragment) mContent).initialize();
        }*/

    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mIsTouchDisable) {
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    public boolean getTouchDisable() {
        Log.d(TAG, "[getTouchDisable]");
        return mIsTouchDisable;
    }

    public void setTouchDisable(boolean isTouchDisable) {
        Log.d(TAG, "[setTouchDisable]" + isTouchDisable);
        mIsTouchDisable = isTouchDisable;
    }

    public void setSoftInputMode(boolean isAdjustPan) {
        if (isAdjustPan) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        } else {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
    }

    public static int fragmentIndex = 1;

    // 현재 선택중인 Fragment Index 저장
    public void selectIndexFragment(int _fragmentIndex) {
        fragmentIndex = _fragmentIndex;
        Log.i("select fragmentIndex :", fragmentIndex + "");

        if (this.getCurrentFocus() != null) {
            InputMethodManager mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            mInputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus()
                    .getWindowToken(), 0);
        }
    }

    /*
     * Fragment간의 전환메서드
     */
    public void switchContent(Fragment fragment) {

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment).commitAllowingStateLoss();
        Handler h = new Handler();
        // 50ms의 오차를 주어 Transition Blocking현상 최소화
        h.postDelayed(new Runnable() {
            public void run() {
                getSlidingMenu().showContent();
            }
        }, 50);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        DUtil.Log(TAG, "[onBackPressed] Back Stack Entry Count : " + fm.getBackStackEntryCount());

        if (fm.getBackStackEntryCount() >= 1) {

            String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount()-1).getName();
            IFragment fragment = (IFragment) fm.findFragmentByTag(tag);
            if (false == fragment.onBackPressed()) {
                currentFragname = null;

                if (fm.getBackStackEntryCount() == 0) {

                    if (!mFlagFinish) {
                        ToastUtility.show(mCtx, getResources().getString(R.string.toast_back_app_finish));
                        mFlagFinish = true;
                        mHandler.postDelayed(mTask, 2000);
                        return;
                    }

                    finish();
                } else {
                    setTouchDisable(true);
                    //clearCaptureView();

                    fm.popBackStack();

                }
            }
        } else {

            if (!mFlagFinish) {
                ToastUtility.show(mCtx, getResources().getString(R.string.toast_back_app_finish));
                mFlagFinish = true;
                mHandler.postDelayed(mTask, 2000);
                return;
            }

            super.onBackPressed();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    private class RewhiteSessionStatusCallback implements RewhiteSessionCallback {

        /**
         * 세션이 오픈되었으면 가입페이지로 이동 한다.
         */
        @Override
        public void onSessionOpened() {
            // 프로그레스바를 보이고 있었다면 중지하고 세션 오픈후 보일 페이지로 이동
            MainActivity.this.onSessionOpened();
            DUtil.Log(TAG, "onSessionOpened");
        }

        @Override
        public void onSessionOpening() {
            // 뺑글이 시작
            DUtil.Log(TAG, "onSessionOpening");
        }

        /**
         * 세션이 삭제되었으니 로그인 화면이 보여야 한다.
         *
         * @param exception 에러가 발생하여 close가 된 경우 해당 exception
         */
        @Override
        public void onSessionClosed(final RewhiteException exception) {
            // TODO Auto-generated method stub
            MainActivity.this.onSessionClosed();
            DUtil.Log(TAG, "onSessionClosed = " + exception.getLocalizedMessage());
        }
    }

    protected void onSessionClosed() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, SignActivity.class);
                mCtx.startActivity(intent);
                ((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                ((Activity) mCtx).finish();
            }
        }, 200);
    }

    private void openThirdsAgreePopup(){
        if(!"Y".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.TERMS_THIRDS_TRIED_TPHONE))){
            // T전화 약관동의 팝업 오픈
            // T전화 약관동의 팝업 오픈
            Intent intent = new Intent(this, TermsAgreeThirdPartyScreen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }
    protected void onSessionOpened() {
        // 제3자 약관동의 체크
        DUtil.Log(TAG, "SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.TERMS_AGREE_THIRD) = " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.TERMS_AGREE_THIRD));
        DUtil.Log(TAG, "SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.TERMS_THIRDS_TRIED_TPHONE)) = " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.TERMS_THIRDS_TRIED_TPHONE));

        if (!"".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.TERMS_AGREE_THIRD)) && SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.TERMS_AGREE_THIRD) != null) {
            try {
                JSONArray thirdsAgreeList = new JSONArray(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.TERMS_AGREE_THIRD));
                if(thirdsAgreeList.length() > 0){
                    boolean isNotAgreed = false;
                    for(int i = 0 ; i < thirdsAgreeList.length(); i++){
                        // 제3자 제공동의 미동의건 체크
                        if("N".equals(thirdsAgreeList.getJSONObject(i).getString("isAgree"))){
                            isNotAgreed = true;
                            if("TPhone".equals(thirdsAgreeList.getJSONObject(i).getString("agreeThirdName")) ){
                                // T전화 약관동의 팝업 오픈
                                openThirdsAgreePopup();
                            }
                        }
                    }
                }else{
                    openThirdsAgreePopup();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            openThirdsAgreePopup();
        }
    }

    // ORDER DETAIL
    public void itemSelected(String orderId, String orderStatus, String storeId, int orderSubType) {
        if(orderSubType == 151 || orderSubType == 152){

            Intent intent = new Intent();
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.setClass(mCtx, GS25OrderDetailActivity.class);
            intent.putExtra("orderId", orderId);
            intent.putExtra("orderSubType", orderSubType);
            startActivityForResult(intent, ORDER_LIST_REFRESH);
            overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);

        }else{
            if("23".equals(orderStatus) || "24".equals(orderStatus)){
                Intent reviewIntent = new Intent(mCtx, ReviewActivity.class);
                reviewIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                //reviewIntent.setAction(Intent.ACTION_GET_CONTENT);
                reviewIntent.putExtra("orderId", orderId);
                reviewIntent.putExtra("storeId", storeId);
                startActivityForResult(reviewIntent, ORDER_REVIEWED);
                overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
            }else{
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, OrderDetailActivity.class);
                intent.putExtra("orderId", orderId);
                startActivityForResult(intent, ORDER_LIST_REFRESH);
                overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOICE_LOCATION:
                    if (fragmentIndex == 1) {
                        Log.e("CHOICE_LOCATION", "===================== CHOICE_LOCATION INITIALIZE ===================");
                        ((MainFragment) mContent).initialize();
                    }
                    break;
                case INVALID_LOCATION:
                    if (fragmentIndex == 1) {
                        Log.e("INVALID_LOCATION", "===================== INVALID_LOCATION INITIALIZE ===================");
                        ((MainFragment) mContent).initialize();
                    }
                    break;
                case ORDER_LIST_REFRESH:
                    if (fragmentIndex == 1) {
                        Log.e("ORDER_LIST_REFRESH", "===================== ORDER_LIST_REFRESH INITIALIZE ===================");
                        ((MainFragment) mContent).initialize();
                    }
                    break;
                case CHOICE_STORE:
                    if (fragmentIndex == 1) {
                        Log.e("CHOICE_LOCATION", "===================== CHOICE_STORE INITIALIZE ===================");
                        JSONObject currentStoreInfoObj = null;
                        try {
                            currentStoreInfoObj = new JSONObject(data.getStringExtra("currentStoreInfo"));
                            ((MainFragment) mContent).changeStore(currentStoreInfoObj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case ORDER_REVIEWED:
                    if (fragmentIndex == 1) {
                        Log.e("REVIEW COMPLETE", "===================== REVIEW COMPLETE INITIALIZE ===================");
                        Tracker t2 = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                        t2.send(new HitBuilders.EventBuilder()
                                .setCategory("USER")
                                .setAction("주문평가")
                                .setLabel("Order")
                                .build());
                        ToastUtility.show(mCtx, "정상적으로 평가되었습니다.", Toast.LENGTH_SHORT);
                        ((MainFragment) mContent).initialize();

                        String result = data.getStringExtra("RESULT");
                        if("Y".equals(result)){
                            Intent reviewFacebookIntent = new Intent(mCtx, ReviewFacebookActivity.class);
                            startActivity(reviewFacebookIntent);
                        }else{

                        }
                    }

                    break;
                default:
                    break;
            }
        }else if (resultCode == RESULT_CANCELED) {
            switch (requestCode) {
                case CHOICE_STORE:
                    if (fragmentIndex == 1) {
                        Log.e("CHOICE_LOCATION", "===================== CHOICE_STORE INITIALIZE ===================");
                        ((MainFragment) mContent).changeStore(null);
                    }
                    break;
            }

        }
    }
}
