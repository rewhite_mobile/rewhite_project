package me.rewhite.users.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.util.MZValidator;
import me.rewhite.users.util.TimeUtil;

public class CouponDetailActivity extends BaseActivity {

    AQuery aq;
    Context mCtx = this;
    JSONObject data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_coupon);

        aq = new AQuery(this);
        aq.id(R.id.title_text).text("쿠폰 상세정보");

        Intent intent = getIntent();
        if (intent != null) {
            try {
                Log.e("coupon info", intent.getStringExtra("data"));
                data = new JSONObject(intent.getStringExtra("data"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        try {
            aq.id(R.id.title_label).text(data.getString("couponTitle"));

            aq.id(R.id.title_label_2).text(data.getString("couponTitle"));


            String pointString;
            if ("F".equals(data.getString("couponType"))) {
                pointString = MZValidator.toNumFormat(data.getInt("couponValue"));
            } else {
                pointString = data.getInt("couponValue") + "%"; //String.format("<font color=\"#5eaeff\"><B>%d", listviewitem.getCouponValue()) + "%</B></font> 할인";
            }
            aq.id(R.id.value_label).text(pointString);


            aq.id(R.id.type_label).text(data.getString("applyOrderSubTypeTitle"));

            if("S".equals(data.getString("couponCalculateTarget"))){
                if(!data.isNull("informationMessage")){
                    aq.id(R.id.place_label).text(data.getString("informationMessage"));
                }else{
                    aq.id(R.id.place_label).text("표시할 내용이 없습니다.");
                }
            }else{
                aq.id(R.id.place_layout).gone();
            }


            int baseMoney = data.getInt("couponBaseMoney");
            int maxDiscountLimit = data.getInt("couponMaxMoney");// listviewitem.getCouponMaxMoney();
            String limitDesc = pointString + " 할인\n";
            if (baseMoney == 0 && maxDiscountLimit > 0) {
                limitDesc += "최대 "+MZValidator.toNumFormat(maxDiscountLimit)+"원 할인";
            } else if (baseMoney > 0 && maxDiscountLimit == 0) {
                limitDesc += MZValidator.toNumFormat(baseMoney)+"원 이상 결제시 사용가능";
            } else if (baseMoney > 0 && maxDiscountLimit > 0) {
                limitDesc += MZValidator.toNumFormat(baseMoney) + "원 이상 결제시 사용가능 /\n최대 "+ MZValidator.toNumFormat(maxDiscountLimit)+"원 할인";
            } else {

            }
            aq.id(R.id.content_label).text(limitDesc);

            long regdateValue = data.getLong("registerDateApp");
            String regDateString = TimeUtil.getSimpleDisplayDateFormat(TimeUtil.convertTimestampToDate(regdateValue));
            aq.id(R.id.regdate_label).text(regDateString);

            long availableDateValue = data.getLong("useFinishDateApp");
            if (availableDateValue == 0) {
                aq.id(R.id.valid_date_label).text("없음");
            } else {
                String availableString = TimeUtil.getSimpleDisplayDateFormat(TimeUtil.convertTimestampToDate(availableDateValue));
                aq.id(R.id.valid_date_label).text( availableString + " 까지");
            }

            aq.id(R.id.notice_label).text("∙ 리화이트 앱에서 바로결제를 하신 경우에만 사용 가능한 쿠폰입니다.\n∙ 포인트 및 타 쿠폰, 이용권과 중복 사용이 불가능합니다.\n∙ 쿠폰은 현금으로 환불되지 않습니다.");

        }catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void closeClicked(View button) {
        finish();
    }

}
