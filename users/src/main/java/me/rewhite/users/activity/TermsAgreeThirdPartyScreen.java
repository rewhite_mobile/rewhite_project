package me.rewhite.users.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class TermsAgreeThirdPartyScreen extends BaseActivity {

    private AQuery aq;
    JSONArray data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        setContentView(R.layout.fragment_third_terms_agree);

        aq = new AQuery(this);

        aq.id(R.id.btn_close).clicked(this, "noClicked");
        aq.id(R.id.btn_disagree).clicked(this, "disagreeClicked");
        aq.id(R.id.btn_agree).clicked(this, "agreeClicked");
    }

    public void noClicked(View button) {
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.TERMS_THIRDS_TRIED_TPHONE, "N");
        Log.e("okCilcked", "NO");
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    public void disagreeClicked(View button) {

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("agreeThirdName", "TPhone");
        params.put("isAgree", "N");
        params.put("k", 1);

        NetworkClient.post(Constants.SET_THIRDS_AGREE, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SET_THIRDS_AGREE, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {

                    result = new String(data, "UTF-8");
                    JSONObject jsondata = new JSONObject(result);
                    DUtil.Log(Constants.SET_THIRDS_AGREE, jsondata.toString());

                    if ("S0000".equals(jsondata.getString("resultCode"))) {

                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.TERMS_THIRDS_TRIED_TPHONE, "Y");
                        Log.e("agreeClicked", "NO");
                        setResult(Activity.RESULT_OK);
                        finish();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    public void agreeClicked(View button) {

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("agreeThirdName", "TPhone");
        params.put("isAgree", "Y");
        params.put("k", 1);

        NetworkClient.post(Constants.SET_THIRDS_AGREE, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SET_THIRDS_AGREE, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    JSONObject jsondata = new JSONObject(result);
                    DUtil.Log(Constants.SET_THIRDS_AGREE, jsondata.toString());

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.TERMS_THIRDS_TRIED_TPHONE, "Y");
                        Log.e("agreeClicked", "YES");
                        setResult(Activity.RESULT_OK);
                        finish();
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });

    }

}
