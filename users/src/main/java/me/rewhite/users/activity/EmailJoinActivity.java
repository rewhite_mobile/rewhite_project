package me.rewhite.users.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.StringTokenizer;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.LoginType;
import me.rewhite.users.common.RewhiteLoginType;
import me.rewhite.users.fragment.IFragment;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.session.AccessTokenRequest;
import me.rewhite.users.session.RewhiteException;
import me.rewhite.users.session.RewhiteSession;
import me.rewhite.users.session.RewhiteSessionCallback;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.StringUtil;
import me.rewhite.users.util.ValidateUtil;

public class EmailJoinActivity extends BaseActivity {

    private final static String TAG = EmailJoinActivity.class.getSimpleName();

    public AQuery aq;
    public final Context mCtx = this;

    private final RewhiteSessionCallback rewhiteSessionCallback = new RewhiteSessionStatusCallback();
    public RewhiteSession session;

    public boolean isLoginProcessing = false;
    ProgressDialog mProgressDialog;

    EditText input_email_text;
    EditText input_pw_text;
    EditText input_pwc_text;

    InputMethodManager imm;
    String isAgreeThirdOptinal = "N";
    Tracker mTracker;

    @Override
    public void onDestroy() {
        super.onDestroy();

        session.removeCallback(rewhiteSessionCallback);
        //kakaoSession.removeCallback(kakaoSessionCallback);
        //kakaoSession.removeCallback(kakaoCallback);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_join);

        if (getActionBar() != null) {
            getActionBar().hide();
        }

        Intent intent = getIntent();
        if (intent != null) {
            isAgreeThirdOptinal = intent.getStringExtra("isAgreeThirdOptinal");
        }

        mTracker = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        mTracker.setScreenName(TAG);
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        aq = new AQuery(this);

        try{
            session = RewhiteSession.getCurrentSession();
        }catch(IllegalStateException e){
            e.printStackTrace();
            session = RewhiteSession.getInstance(mCtx);
        }
        session.addCallback(rewhiteSessionCallback);

        input_email_text = (EditText) findViewById(R.id.tf_email);
        input_email_text.setTypeface(CommonUtility.getNanumBarunTypeface());
        input_pw_text = (EditText) findViewById(R.id.tf_password);
        input_pw_text.setTypeface(CommonUtility.getNanumBarunTypeface());
        input_pwc_text = (EditText) findViewById(R.id.tf_password_confirm);
        input_pwc_text.setTypeface(CommonUtility.getNanumBarunTypeface());

        aq.id(R.id.btn_next).clicked(this, "joinProcessClick");
        aq.id(R.id.btn_back).clicked(this, "backClick");

        aq.id(R.id.btn_clear_01).tag(0).clicked(this, "inputClearClicked").gone();
        aq.id(R.id.btn_clear_02).tag(1).clicked(this, "inputClearClicked").gone();
        aq.id(R.id.btn_clear_03).tag(2).clicked(this, "inputClearClicked").gone();

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        input_email_text.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean hasfocus) {
                if (hasfocus) {
                    if (input_email_text.getText().length() > 0) {
                        aq.id(R.id.btn_clear_01).visible();
                    }
                } else {
                    aq.id(R.id.btn_clear_01).gone();
                }
            }
        });
        input_email_text.addTextChangedListener(emailTextWatcher);
        input_pw_text.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean hasfocus) {
                if (hasfocus) {
                    if (input_pw_text.getText().length() > 0) {
                        aq.id(R.id.btn_clear_02).visible();
                    }
                } else {
                    aq.id(R.id.btn_clear_02).gone();
                }
            }
        });
        input_pw_text.addTextChangedListener(passwordTextWatcher);
        input_pwc_text.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean hasfocus) {
                if (hasfocus) {
                    if (input_pwc_text.getText().length() > 0) {
                        aq.id(R.id.btn_clear_03).visible();
                    }
                } else {
                    aq.id(R.id.btn_clear_03).gone();
                }
            }
        });
        input_pwc_text.addTextChangedListener(passwordConfirmTextWatcher);
    }

    private final TextWatcher emailTextWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable edit) {
            if (edit.length() == 0) {
                aq.id(R.id.btn_clear_01).gone();
            } else {
                aq.id(R.id.btn_clear_01).visible();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
        }
    };
    private final TextWatcher passwordTextWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable edit) {
            if (edit.length() == 0) {
                aq.id(R.id.btn_clear_02).gone();
            } else {
                aq.id(R.id.btn_clear_02).visible();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
        }
    };
    private final TextWatcher passwordConfirmTextWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable edit) {
            if (edit.length() == 0) {
                aq.id(R.id.btn_clear_03).gone();
            } else {
                aq.id(R.id.btn_clear_03).visible();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
        }
    };

    public void backClick(View button) {
        super.onBackPressed();
    }

    public void joinProcessClick(View button) {
        if (isLoginProcessing) {
            return;
        }

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("회원가입 진행중...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        String emailAddressInput = aq.id(R.id.tf_email).typeface(CommonUtility.getNanumBarunTypeface()).getText().toString();
        String passwordInput = aq.id(R.id.tf_password).typeface(CommonUtility.getNanumBarunTypeface()).getText().toString();
        String passwordConfirmInput = aq.id(R.id.tf_password_confirm).typeface(CommonUtility.getNanumBarunTypeface()).getText().toString();

        if (ValidateUtil.checkEmail(emailAddressInput)) {

            if (passwordInput.equals(passwordConfirmInput)) {
                if (ValidateUtil.validPassword(passwordInput) == 0) {
                    DUtil.alertShow(this, "비밀번호는 8자리 이상이어야 합니다.");
                    if (mProgressDialog != null) mProgressDialog.dismiss();
                } else if (ValidateUtil.validPassword(passwordInput) == 1) {
                    DUtil.alertShow(this, "비밀번호에 영문,숫자가 모두 포함되어야 합니다.");
                    if (mProgressDialog != null) mProgressDialog.dismiss();
                } else if (ValidateUtil.validPassword(passwordInput) == 2) {
                    //JoinMemberRequest jmr = JoinMemberRequest.createRequest(mCtx, emailAddressInput, passwordInput, LoginType.EMAIL, isAgreeThirdOptinal);
                    //session.registerMember(jmr);
                    registerMember(emailAddressInput, passwordInput, LoginType.EMAIL, isAgreeThirdOptinal);
                }

            } else {
                DUtil.alertShow(this, "입력하신 비밀번호가 다릅니다.");
                if (mProgressDialog != null) mProgressDialog.dismiss();
            }
        } else {
            DUtil.alertShow(this, "이메일 형식이 잘못되었습니다.");
            if (mProgressDialog != null) mProgressDialog.dismiss();
        }
    }

    public void inputClearClicked(View button) {
        ImageButton v = (ImageButton) button;
        switch ((int) v.getTag()) {
            case 0:
                aq.id(R.id.tf_email).text("").typeface(CommonUtility.getNanumBarunTypeface());
                break;
            case 1:
                aq.id(R.id.tf_password).text("").typeface(CommonUtility.getNanumBarunTypeface());
                break;
            case 2:
                aq.id(R.id.tf_password_confirm).text("").typeface(CommonUtility.getNanumBarunTypeface());
                break;
            default:
                break;
        }
    }

    private void registerMember(final String loginId, final String secretKey, final String loginType, final String isAgreeThirdOptinal){
        RequestParams params = new RequestParams();
        params.put("loginId", loginId);
        params.put("loginType", loginType);
        params.put("secretKey", secretKey);
        params.put("isAgreeThirdOptinal", isAgreeThirdOptinal);

        params.put("deviceToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));

        params.put("k", 1);
        NetworkClient.post(Constants.MEMBER_REGISTER, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                if (mProgressDialog != null) mProgressDialog.dismiss();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        //Toast.makeText(mActivity, json.getString("result_msg"), Toast.LENGTH_SHORT);
                        //AccessTokenRequest aRequest = AccessTokenRequest.createRequestWithDeviceToken(mCtx, loginId,
                        //        secretKey, loginType, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));
                        //session.requestAccessToken(aRequest);

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("가입및탈퇴")
                                .setAction("가입완료")
                                .setLabel("이메일")
                                .build());

                        requestSessionOpen(mCtx, loginId, secretKey, loginType);
                    } else {
                        Toast.makeText(aq.getContext(), jsondata.getString("message"), Toast.LENGTH_SHORT).show();
                        session.close();
                    }

                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();

                }
            }
        });
    }

    public void requestSessionOpen(Context ctx, String loginId, String secretKey, String loginType) {
        DUtil.Log(TAG, "requestSessionOpen Start :::: " + loginId + " / " + secretKey + " / " + loginType);
        /*
        if(mProgressDialog == null){
            mProgressDialog = new ProgressDialog(Bootstrap.getCurrentActivity());
            mProgressDialog.setMessage("회원가입 처리중...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }*/

        isLoginProcessing = true;

        AccessTokenRequest aRequest = AccessTokenRequest.createRequestWithDeviceToken(ctx, loginId, secretKey, loginType, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));
        session.open(aRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (session.handleActivityResult(requestCode,
                resultCode, data)) {
            Log.d(TAG, "onActivityResult : RewhiteSession");
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

//    public void requestSessionOpen(Context ctx, String loginId, String secretKey, String loginType) {
//        DUtil.Log(TAG, "requestSessionOpen Start :::: " + loginId + " / " + secretKey + " / " + loginType);
//
//        isLoginProcessing = true;
//
//        AccessTokenRequest aRequest = AccessTokenRequest.createRequestWithDeviceToken(ctx, loginId, secretKey, loginType, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));
//        session.open(aRequest);
//    }

    protected void onSessionClosed() {
    }

    protected void onSessionOpened() {

        String accessToken = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN);
        if (StringUtil.isNullOrEmpty(accessToken)) {
            isLoginProcessing = false;
            if (mProgressDialog != null) mProgressDialog.dismiss();
            return;
        }

        SharedPreferencesUtility.set(SharedPreferencesUtility.Command.IS_AUTO_LOGIN, "true");
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_TYPE, LoginType.EMAIL);
        String userName = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME);
        String mobile = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.PHONE_NO);
        final String loginId = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_ID);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.EMAIL, loginId);

        if (StringUtil.isNullOrEmpty(userName) || StringUtil.isNullOrEmpty(mobile) || StringUtil.isNullOrEmpty(loginId)) {
            // 필수정보 미입력시
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    Bundle bundle = new Bundle();
                    //bundle.putString("loginId", loginId);
                    StringTokenizer tokens = new StringTokenizer(loginId, "@");
                    String first = tokens.nextToken();

                    bundle.putString("nickName", first);
                    bundle.putString("imagePath", "");
                    bundle.putString("imageThumbPath", "");

                    Log.e("EMAILJOIN", RewhiteLoginType.TYPE_EMAIL);

                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setClass(mCtx, ProfileNewActivityV2.class);
                    intent.putExtra("bundle", bundle);
                    intent.putExtra("loginType", RewhiteLoginType.TYPE_EMAIL);
                    mCtx.startActivity(intent);
                    ((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    ((Activity) mCtx).finish();
                }
            }, 200);
        } else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setClass(mCtx, MainActivity.class);
                    mCtx.startActivity(intent);
                    ((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    ((Activity) mCtx).finish();
                }
            }, 200);
        }

    }

    @Override
    public void onBackPressed() {

        FragmentManager fm = getSupportFragmentManager();
        DUtil.Log(TAG, "[onBackPressed] Back Stack Entry Count : " + fm.getBackStackEntryCount());

        if (fm.getBackStackEntryCount() >= 1) {
            String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
            DUtil.Log(TAG, "[FragmentManager] tag : " + tag + "\nfm.getBackStackEntryCount() : " + fm.getBackStackEntryCount());

            IFragment fragment = (IFragment) fm.findFragmentByTag(tag);
            if (false == fragment.onBackPressed()) {
                fm.popBackStack();
                if (fm.getBackStackEntryCount() == 1) super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    private class RewhiteSessionStatusCallback implements RewhiteSessionCallback {

        /**
         * 세션이 오픈되었으면 가입페이지로 이동 한다.
         */
        @Override
        public void onSessionOpened() {
            // 뺑글이 종료
            if (mProgressDialog != null) mProgressDialog.dismiss();

            // 프로그레스바를 보이고 있었다면 중지하고 세션 오픈후 보일 페이지로 이동
            EmailJoinActivity.this.onSessionOpened();
            DUtil.Log(TAG, "onSessionOpened");

        }

        @Override
        public void onSessionOpening() {
            // 뺑글이 시작
            isLoginProcessing = true;
            DUtil.Log(TAG, "onSessionOpening");
        }

        /**
         * 세션이 삭제되었으니 로그인 화면이 보여야 한다.
         *
         * @param exception 에러가 발생하여 close가 된 경우 해당 exception
         */
        @Override
        public void onSessionClosed(final RewhiteException exception) {
            // TODO Auto-generated method stub
            EmailJoinActivity.this.onSessionClosed();

            DUtil.Log(TAG, "onSessionClosed = " + exception.getLocalizedMessage());

            isLoginProcessing = false;
            if (mProgressDialog != null) mProgressDialog.dismiss();

        }

    }
}
