package me.rewhite.users.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.skt.Tmap.TMapGpsManager;
import com.skt.Tmap.TMapMarkerItem;
import com.skt.Tmap.TMapPOIItem;
import com.skt.Tmap.TMapPoint;
import com.skt.Tmap.TMapView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.StringUtil;

public class LocationDetailTMapActivity extends BaseActivity implements TMapGpsManager.onLocationChangedCallback {

    @Override
    public void onLocationChange(Location location) {

        if(m_bTrackingMode) {
            tmapview.setLocationPoint(location.getLongitude(), location.getLatitude());
        }
    }

    private static final String LOG_TAG = "LocationDetailActivity";
    private static final int ALERT_SAVE = 1;
    private final Context mCtx = this;

    //private MapView mapView;
    private TMapView tmapview = null;
    TMapGpsManager gps = null;
    private 	int 		m_nCurrentZoomLevel = 0;
    private 	double 		m_Latitude  = 0;
    private     double  	m_Longitude = 0;
    private 	boolean 	m_bShowMapIcon = false;

    private 	boolean 	m_bTrafficeMode = false;
    private 	boolean 	m_bSightVisible = false;
    private 	boolean 	m_bTrackingMode = false;

    private 	boolean 	m_bOverlayMode = false;

    public double longitude;
    public double latitude;
    public String addressName;
    public String newAddressName;
    boolean isMapInitialized = false;
    private InputMethodManager imm;

    public String title;
    public String sourceType = "search";
    AQuery aq;
    private static final String HEADER_NAME_X_APPID = "x-appid";
    private static final String HEADER_NAME_X_PLATFORM = "x-platform";
    private static final String HEADER_VALUE_X_PLATFORM_ANDROID = "android";
    EditText inputAddressText;

    private View mView;

    private int mPos = -1;
    private boolean isTMapViewInitialized = false;

    private static final String[] INITIAL_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final String[] LOCATION_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int INITIAL_REQUEST=1337;
    private static final int LOCATION_REQUEST=INITIAL_REQUEST+3;
    boolean isGpsGet = false;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(gps != null){
            gps.CloseGps();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_detail);

        Log.e(LOG_TAG, "onCreate");

        Intent intent = getIntent();
        if (intent != null) {
            title = intent.getStringExtra("title");
            longitude = Double.parseDouble(intent.getStringExtra("longitude"));
            latitude = Double.parseDouble(intent.getStringExtra("latitude"));
            addressName = intent.getStringExtra("addressName");
            newAddressName = intent.getStringExtra("newAddressName");
            sourceType = intent.getStringExtra("sourceType");
        }
        aq = new AQuery(this);

        //aq.id(R.id.text_title).text(title).typeface(CommonUtility.getNanumBarunTypeface());

        //aq.id(R.id.text_new_address).text("[도로명] " + newAddressName).typeface(CommonUtility.getNanumBarunLightTypeface());

//        if (StringUtil.isNullOrEmpty(title)) {
//            aq.id(R.id.text_title).gone();
//        }
        if (StringUtil.isNullOrEmpty(newAddressName)) {
            aq.id(R.id.text_address).text(addressName).typeface(CommonUtility.getNanumBarunTypeface());
        }else{
            aq.id(R.id.text_address).text(newAddressName).typeface(CommonUtility.getNanumBarunTypeface());
        }

        Log.i("location position", longitude + "/" + latitude);

        aq.id(R.id.button_area).gone();

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        tmapview = new TMapView(this);
        configureMapView();
        initMapview();

        ViewGroup mapViewContainer = (ViewGroup)findViewById(R.id.map_view);
        mapViewContainer.addView(tmapview);
//
//        if ( Build.VERSION.SDK_INT >= 23){
//            if (!canAccessLocation()) {
//                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
//            }else{
//
//            }
//        }else{
//            tmapview = new TMapView(this);
//            configureMapView();
//            initMapview();
//
//            ViewGroup mapViewContainer = (ViewGroup)findViewById(R.id.map_view);
//            mapViewContainer.addView(tmapview);
//        }


        aq.id(R.id.btn_apart_popup).clicked(this, "apartPopupClicked");
        aq.id(R.id.btn_popup).clicked(this, "popupClicked");
        aq.id(R.id.btn_close).clicked(this, "popupCloseClicked");
        aq.id(R.id.btn_input_complete).clicked(this, "inputPopupCompleted");
    }

    private void configureMapView(){
        tmapview.setSKTMapApiKey(Constants.SK_APP_KEY);
        tmapview.setIconVisibility(true);
        tmapview.setZoomLevel(17);
        tmapview.setMapType(TMapView.MAPTYPE_STANDARD);
       // tmapview.setCompassMode(true);
        //tmapview.setTrackingMode(true);

    }

    private void initGpsManager(){
        gps = new TMapGpsManager(LocationDetailTMapActivity.this);
        gps.setMinTime(1000);
        gps.setMinDistance(5);
        gps.setProvider(gps.NETWORK_PROVIDER);
        gps.OpenGps();
    }

    private void initMapview(){
        if ( Build.VERSION.SDK_INT >= 23){
            if (!canAccessLocation()) {
                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
            }else{
                initGpsManager();
            }
        }else{
            initGpsManager();
        }


        isTMapViewInitialized = true;
        tmapview.setTMapLogoPosition(TMapView.TMapLogoPositon.POSITION_BOTTOMLEFT);

        TMapPoint point = new TMapPoint(latitude, longitude);
        tmapview.setCenterPoint(point.getLongitude(), point.getLatitude(), true);
        aq.id(R.id.button_area).visible();

        tmapview.setOnApiKeyListener(new TMapView.OnApiKeyListenerCallback() {
            @Override
            public void SKTMapApikeySucceed() {
                Log.e("TMAP LOG", "MainActivity SKTMapApikeySucceed");
            }

            @Override
            public void SKTMapApikeyFailed(String errorMsg) {
                Log.e("TMAP LOG", "MainActivity SKTMapApikeyFailed " + errorMsg);
            }
        });

        tmapview.setOnEnableScrollWithZoomLevelListener(new TMapView.OnEnableScrollWithZoomLevelCallback() {
            @Override
            public void onEnableScrollWithZoomLevelEvent(float zoom, TMapPoint centerPoint) {
                //Log.e("TMAP LOG", "MainActivity onEnableScrollWithZoomLevelEvent " + zoom + " " + centerPoint.getLatitude() + " " + centerPoint.getLongitude());
            }
        });

        tmapview.setOnDisableScrollWithZoomLevelListener(new TMapView.OnDisableScrollWithZoomLevelCallback() {
            @Override
            public void onDisableScrollWithZoomLevelEvent(float zoom, TMapPoint centerPoint) {
                //Log.e("TMAP LOG", "MainActivity onDisableScrollWithZoomLevelEvent " + zoom + " " + centerPoint.getLatitude() + " " + centerPoint.getLongitude());
            }
        });

        tmapview.setOnClickListenerCallBack(new TMapView.OnClickListenerCallback() {
            @Override
            public boolean onPressUpEvent(ArrayList<TMapMarkerItem> markerlist, ArrayList<TMapPOIItem> poilist, TMapPoint point, PointF pointf) {
                Log.e("TMAP LOG", "MainActivity onPressUpEvent " + markerlist.size());
                //MapPoint.GeoCoordinate mapPointGeo = mapView.getMapCenterPoint().getMapPointGeoCoord();
                TMapPoint cMapPoint = tmapview.getCenterPoint();

                transGeoCode(cMapPoint);

                aq.id(R.id.button_area).visible();
                Log.i(LOG_TAG, String.format("MapView onMapViewDragEnded (%f,%f)", cMapPoint.getLatitude(), cMapPoint.getLongitude()));

                return false;
            }

            @Override
            public boolean onPressEvent(ArrayList<TMapMarkerItem> markerlist,ArrayList<TMapPOIItem> poilist, TMapPoint point, PointF pointf) {
                Log.e("TMAP LOG", "MainActivity onPressEvent " + markerlist.size());

                aq.id(R.id.pin).image(R.mipmap.map_pin_move);

                aq.id(R.id.button_area).gone();
                aq.id(R.id.apart_balloon).gone();
//
//                for (int i = 0; i < markerlist.size(); i++) {
//                    TMapMarkerItem item = markerlist.get(i);
//                    Log.e("TMAP LOG", "MainActivity onPressEvent " + item.getName() + " " + item.getTMapPoint().getLatitude() + " " + item.getTMapPoint().getLongitude());
//                }
                return false;
            }
        });
/*
        tmapview.setOnLongClickListenerCallback(new TMapView.OnLongClickListenerCallback() {
            @Override
            public void onLongPressEvent(ArrayList<TMapMarkerItem> markerlist,ArrayList<TMapPOIItem> poilist, TMapPoint point) {
                Log.e("TMAP LOG", "MainActivity onLongPressEvent " + markerlist.size());
            }
        });*/

    }

    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch(requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    tmapview = new TMapView(this);
                    configureMapView();
                    initMapview();

                    ViewGroup mapViewContainer = (ViewGroup)findViewById(R.id.map_view);
                    mapViewContainer.addView(tmapview);
                }
                else {
                    Toast.makeText(this, "위치정보설정이 사용자에 의해 비활성화되어있습니다.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
    private boolean canAccessLocation() {
        return(hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
    }

    public void popupCloseClicked(View button){
        aq.id(R.id.popup_input).gone();
    }

    public void apartPopupClicked(View button){
        //showPopup("apart", apartAddressName, apartNewAddressName, apartTitle, apartLat, apartLon);
    }

    public void popupClicked(View button){
        showPopup("normal", addressName, newAddressName, title, latitude, longitude);
    }

    String popupType;
    private void showPopup(String _type, String _addressName, String _newAddressName,String _title,  Double _lat, Double _lon) {
        aq.id(R.id.popup_input).visible();
        popupType = _type;

        String addr;
        if (StringUtil.isNullOrEmpty(_newAddressName)) {
            addr = _addressName;
        } else {
            addr = _newAddressName;
        }

        if("apart".equals(_type)){
            addr = addr + " (" + _title + ")";
        }

        aq.id(R.id.popup_address_label).text(addr);

    }

    public void inputPopupCompleted(View button){


        String addr = "";
        String detailAddr = aq.id(R.id.popup_input_field).getText().toString();

        if(StringUtil.isNullOrEmpty(detailAddr)){
            DUtil.alertShow(this, "상세주소를 입력해주세요.");
            return;
        }

        if (StringUtil.isNullOrEmpty(newAddressName)) {
            addr = addressName;
        } else {
            addr = newAddressName;
        }

        // 저장부분 마무리 필요함
        if(title != null && !"".equals(title)){
            detailAddr = detailAddr + " (" + title + ")";
        }
        Log.e("infoSaveAction", title + " / " + addr + "/" + detailAddr);
        addLocation(title, longitude, latitude, addr, detailAddr);

/*
        if("apart".equals(popupType)){
            if (StringUtil.isNullOrEmpty(apartNewAddressName)) {
                addr = apartAddressName;
            } else {
                addr = apartNewAddressName;
            }
            detailAddr = detailAddr + " (" + apartTitle + ")";
            Log.e("infoSaveAction", addr + "/" + detailAddr);
            addLocation(apartTitle, apartLon, apartLat, addr, detailAddr);
        }else{
            if (StringUtil.isNullOrEmpty(newAddressName)) {
                addr = addressName;
            } else {
                addr = newAddressName;
            }
            Log.e("infoSaveAction", addr + "/" + detailAddr);
            addLocation(title, longitude, latitude, addr, detailAddr);
        }
*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.e("onActivityResult", "ALERT_SAVE");
        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case ALERT_SAVE:
                    Log.i("onActivityResult", "ALERT_SAVE");

                    String addr = "";
                    String detailAddr = aq.id(R.id.popup_input_field).getText().toString();
                    if (StringUtil.isNullOrEmpty(newAddressName)) {
                        addr = addressName;
                    } else {
                        addr = newAddressName;
                    }

                    // 저장부분 마무리 필요함
                    if(title != null && !"".equals(title)){
                        detailAddr = detailAddr + " (" + title + ")";
                    }
                    Log.e("infoSaveAction", title + " / " + addr + "/" + detailAddr);
                    addLocation(title, longitude, latitude, addr, detailAddr);

                    break;
                default:
                    break;
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    public void infoSaveAction(View button) {
        String detailAddress = aq.id(R.id.popup_input_field).getText().toString();
        final boolean isDefaultSet = false;
        if (StringUtil.isNullOrEmpty(detailAddress)) {

            Intent alertIntent = new Intent(this, AlertDialogScreen.class);
            alertIntent.putExtra("title", "상세주소를 입력하지 않았습니다");
            alertIntent.putExtra("message", "층,동,호 같은 정보가 없으면 확인을 눌러주세요.");
            alertIntent.putExtra("ok_text", "확인");
            alertIntent.putExtra("no_text", "상세정보 입력");
            startActivityForResult(alertIntent, ALERT_SAVE);

        } else {
            String addr = "";
            String detailAddr = aq.id(R.id.popup_input_field).getText().toString();
            if (StringUtil.isNullOrEmpty(newAddressName)) {
                addr = addressName;
            } else {
                addr = newAddressName;
            }
            // 저장부분 마무리 필요함
            if(title != null && !"".equals(title)){
                detailAddr = detailAddr + " (" + title + ")";
            }
            Log.e("infoSaveAction", title + " / " + addr + "/" + detailAddr);
            addLocation(title, longitude, latitude, addr, detailAddr);
        }

    }

    public final void addLocation(final String label, final Double longitude, final Double latitude, final String address, final String detailAddress) {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                RequestParams params = new RequestParams();
                params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                params.put("label", label);
                params.put("longitude", longitude);
                params.put("latitude", latitude);
                params.put("address1", address);
                params.put("address2", detailAddress);
                params.put("isDefault", "Y");
                params.put("k", 1);
                NetworkClient.post(Constants.ADD_LOCATION, params, new AsyncHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                        // TODO Auto-generated method stub
                        DUtil.Log(Constants.ADD_LOCATION, error.getMessage());
                    }

                    @Override
                    public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                        String result;
                        try {
                            result = new String(data, "UTF-8");
                            DUtil.Log(Constants.ADD_LOCATION, result);

                            JSONObject jsondata = new JSONObject(result);

                            if ("S0000".equals(jsondata.getString("resultCode"))) {
                                if (jsondata.isNull("data")) {
                                    //

                                } else {
                                    mTracker.send(new HitBuilders.EventBuilder()
                                            .setCategory("가입및탈퇴")
                                            .setAction("주소입력완료")
                                            //.setLabel("")
                                            .build());
                                    AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                                    logger.logEvent("Address inputed");

                                    JSONArray locationInfo = jsondata.getJSONArray("data");
                                    SharedPreferencesUtility.setLocationData(locationInfo);

                                    Intent resultData = new Intent();
                                    resultData.putExtra("result", true);
                                    setResult(Activity.RESULT_OK, resultData);
                                    finish();
                                }
                            }
                        } catch (UnsupportedEncodingException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        } catch (JSONException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                    }
                });
            }
        }, 200);
    }

    private void showToast(final String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    private void transGeoCode(final TMapPoint mapPointGeo) {
        final double _latitude = mapPointGeo.getLatitude(); // 위도
        final double _longitude = mapPointGeo.getLongitude(); // 경도
        String apikey = Constants.DAUM_API_KEY;

        String Host = Constants.TRANS_GEO_ADDRESS;
        String query = Host + String.format("?apikey=%s&x=%f&y=%f&inputCoordSystem=%s&output=%s", apikey, _longitude, _latitude, "WGS84", "json");

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                if (status.getCode() >= 400) {
                    // IF ERROR
                    Log.i("StatusCode", status.getCode() + "");
                    Log.i("StatusError", status.getError());
                } else {
                    if (status.getCode() == 200) {
                        // IF NORMAL
                        if (json == null) {
                            Log.i("RETURN", "" + status.getMessage());
                            // 네트워크 오류페이지로 이동
                            return;
                        } else {
                            Log.i("RETURN", "" + json.toString());


                            try {
                                String newName = json.getJSONObject("new").getString("name");
                                String oldName = json.getJSONObject("old").getString("name");

                                title = "";
                                longitude = _longitude;
                                latitude = _latitude;

                                addressName = oldName;
                                newAddressName = newName;

                                //aq.id(R.id.text_title).gone();
                                if(StringUtil.isNullOrEmpty(newName)){
                                    aq.id(R.id.text_address).text(oldName);
                                }else{
                                    aq.id(R.id.text_address).text(newName);
                                }

//                                aq.id(R.id.text_new_address).text("[도로명] " + newName);

//                                if (StringUtil.isNullOrEmpty(newName)) {
//                                    aq.id(R.id.text_new_address).gone();
//
//                                } else {
//                                    aq.id(R.id.text_new_address).visible();
//                                }
                                //searchNear(latitude, longitude);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }

                }
            }
        };
        cb.header("Accept", "application/json");
        cb.header("Content-Type", "application/json; charset=utf-8");
        cb.header(HEADER_NAME_X_APPID, "33199");
        cb.header(HEADER_NAME_X_PLATFORM, HEADER_VALUE_X_PLATFORM_ANDROID);

        aq.ajax(query, JSONObject.class, cb);

        aq.id(R.id.pin).image(R.mipmap.map_pin);
    }

    String apartAddressName;
    String apartTitle;
    String apartNewAddressName;
    Double apartLat;
    Double apartLon;

    private synchronized void confirmApart(final String _oldName, final String _newName, final String _title, final Double _lon, final Double _lat) {

        apartTitle = _title;
        apartAddressName = _oldName;
        apartNewAddressName = _newName;
        apartLat = _lat;
        apartLon = _lon;

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {

            @Override
            public void run() {
                aq.id(R.id.apart_balloon).visible();
                aq.id(R.id.apartname).text(apartTitle);

                //setLabel();
            }
        });

    }

    private void setLabel() {
        //aq.id(R.id.text_title).visible().text(title);

        if(StringUtil.isNullOrEmpty(newAddressName)){
            aq.id(R.id.text_address).text(addressName);
        }else{
            aq.id(R.id.text_address).text(newAddressName);
        }

    }

}
