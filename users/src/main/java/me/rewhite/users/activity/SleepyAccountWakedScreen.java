package me.rewhite.users.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;

import com.androidquery.AQuery;

import org.json.JSONArray;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.util.SharedPreferencesUtility;

public class SleepyAccountWakedScreen extends BaseActivity {

    private AQuery aq;
    String imageLink;
    JSONArray data;
    public Context ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        setContentView(R.layout.fragment_waked_popup);
        aq = new AQuery(this);
        aq.id(R.id.content).image(R.mipmap.sleepy_account_waked);
        aq.id(R.id.btn_close).clicked(this, "closeClicked");

    }

    @Override
    public void onBackPressed() {

        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_RESULT_CODE, "S0000");
        super.onBackPressed();

    }

    public void closeClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_RESULT_CODE, "S0000");
                Intent resultData = new Intent();
                resultData.putExtra("result", true);
                setResult(Activity.RESULT_OK, resultData);
                finish();
            }
        }, 100);
    }

}

