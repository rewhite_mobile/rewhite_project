package me.rewhite.users.activity.gsretail;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.skt.Tmap.TMapGpsManager;
import com.skt.Tmap.TMapMarkerItem;
import com.skt.Tmap.TMapPoint;
import com.skt.Tmap.TMapView;

import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.StringUtil;

public class GSStoreDetailTMapActivity extends BaseActivity implements TMapGpsManager.onLocationChangedCallback {

    @Override
    public void onLocationChange(Location location) {

        if(m_bTrackingMode) {
            tmapview.setLocationPoint(location.getLongitude(), location.getLatitude());
        }
    }

    private static final String LOG_TAG = "GSStoreDetailTMapAct";
    private static final int ALERT_SAVE = 1;
    private final Context mCtx = this;

    //private MapView mapView;
    private TMapView tmapview = null;
    TMapGpsManager gps = null;
    private 	int 		m_nCurrentZoomLevel = 0;
    private 	double 		m_Latitude  = 0;
    private     double  	m_Longitude = 0;
    private 	boolean 	m_bShowMapIcon = false;

    private 	boolean 	m_bTrafficeMode = false;
    private 	boolean 	m_bSightVisible = false;
    private 	boolean 	m_bTrackingMode = false;

    private 	boolean 	m_bOverlayMode = false;

    public double longitude;
    public double latitude;
    public String addressName;
    public String newAddressName;
    boolean isMapInitialized = false;
    private InputMethodManager imm;

    public String title;
    public String sourceType = "search";
    AQuery aq;
    private static final String HEADER_NAME_X_APPID = "x-appid";
    private static final String HEADER_NAME_X_PLATFORM = "x-platform";
    private static final String HEADER_VALUE_X_PLATFORM_ANDROID = "android";
    EditText inputAddressText;

    private View mView;

    private int mPos = -1;
    private boolean isTMapViewInitialized = false;

    private static final String[] INITIAL_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final String[] LOCATION_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int INITIAL_REQUEST=1337;
    private static final int LOCATION_REQUEST=INITIAL_REQUEST+3;
    boolean isGpsGet = false;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(gps != null){
            gps.CloseGps();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gs_store_detail);

        Log.e(LOG_TAG, "onCreate");

        Intent intent = getIntent();
        if (intent != null) {
            title = intent.getStringExtra("title");
            longitude = Double.parseDouble(intent.getStringExtra("longitude"));
            latitude = Double.parseDouble(intent.getStringExtra("latitude"));
            addressName = intent.getStringExtra("addressName");
            newAddressName = intent.getStringExtra("newAddressName");
            sourceType = intent.getStringExtra("sourceType");
        }
        aq = new AQuery(this);

        aq.id(R.id.text_address).text(addressName).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.text_title).text(title).typeface(CommonUtility.getNanumBarunTypeface());

        Log.i("location position", longitude + "/" + latitude);

        tmapview = new TMapView(this);
        configureMapView();
        initMapview();

        ViewGroup mapViewContainer = (ViewGroup)findViewById(R.id.map_view);
        mapViewContainer.addView(tmapview);

        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
    }
    public void closeClicked(View button) {
        finish();
    }

    private void configureMapView(){
        tmapview.setSKTMapApiKey(Constants.SK_APP_KEY);
        tmapview.setIconVisibility(true);
        tmapview.setZoomLevel(17);
        tmapview.setMapType(TMapView.MAPTYPE_STANDARD);
       // tmapview.setCompassMode(true);
        //tmapview.setTrackingMode(true);

    }

    private void initGpsManager(){
        gps = new TMapGpsManager(GSStoreDetailTMapActivity.this);
        gps.setMinTime(1000);
        gps.setMinDistance(5);
        gps.setProvider(gps.NETWORK_PROVIDER);
        gps.OpenGps();
    }

    private void initMapview(){
        if ( Build.VERSION.SDK_INT >= 23){
            if (!canAccessLocation()) {
                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
            }else{
                initGpsManager();
            }
        }else{
            initGpsManager();
        }


        isTMapViewInitialized = true;
        tmapview.setTMapLogoPosition(TMapView.TMapLogoPositon.POSITION_BOTTOMLEFT);

        TMapPoint point = new TMapPoint(latitude, longitude);
        tmapview.setCenterPoint(point.getLongitude(), point.getLatitude(), true);

        TMapMarkerItem tMapMarkerItem2 = new TMapMarkerItem();
        tMapMarkerItem2.setTMapPoint(point);
        Bitmap bitmap = BitmapFactory.decodeResource(mCtx.getResources(),R.mipmap.map_pin_gs25);
        tMapMarkerItem2.setIcon(bitmap);
        tMapMarkerItem2.setID(title);

        tmapview.addMarkerItem(title, tMapMarkerItem2);

        tmapview.setOnApiKeyListener(new TMapView.OnApiKeyListenerCallback() {
            @Override
            public void SKTMapApikeySucceed() {
                Log.e("TMAP LOG", "MainActivity SKPMapApikeySucceed");
            }

            @Override
            public void SKTMapApikeyFailed(String errorMsg) {
                Log.e("TMAP LOG", "MainActivity SKPMapApikeyFailed " + errorMsg);
            }
        });

        tmapview.setOnEnableScrollWithZoomLevelListener(new TMapView.OnEnableScrollWithZoomLevelCallback() {
            @Override
            public void onEnableScrollWithZoomLevelEvent(float zoom, TMapPoint centerPoint) {
                //Log.e("TMAP LOG", "MainActivity onEnableScrollWithZoomLevelEvent " + zoom + " " + centerPoint.getLatitude() + " " + centerPoint.getLongitude());
            }
        });

        tmapview.setOnDisableScrollWithZoomLevelListener(new TMapView.OnDisableScrollWithZoomLevelCallback() {
            @Override
            public void onDisableScrollWithZoomLevelEvent(float zoom, TMapPoint centerPoint) {
                //Log.e("TMAP LOG", "MainActivity onDisableScrollWithZoomLevelEvent " + zoom + " " + centerPoint.getLatitude() + " " + centerPoint.getLongitude());
            }
        });

/*
        tmapview.setOnLongClickListenerCallback(new TMapView.OnLongClickListenerCallback() {
            @Override
            public void onLongPressEvent(ArrayList<TMapMarkerItem> markerlist,ArrayList<TMapPOIItem> poilist, TMapPoint point) {
                Log.e("TMAP LOG", "MainActivity onLongPressEvent " + markerlist.size());
            }
        });*/

    }

    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch(requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    tmapview = new TMapView(this);
                    configureMapView();
                    initMapview();

                    ViewGroup mapViewContainer = (ViewGroup)findViewById(R.id.map_view);
                    mapViewContainer.addView(tmapview);
                }
                else {
                    Toast.makeText(this, "위치정보설정이 사용자에 의해 비활성화되어있습니다.", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }
    private boolean canAccessLocation() {
        return(hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
    }

    public void popupCloseClicked(View button){
        aq.id(R.id.popup_input).gone();
    }


    private void showToast(final String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    private void transGeoCode(final TMapPoint mapPointGeo) {
        final double _latitude = mapPointGeo.getLatitude(); // 위도
        final double _longitude = mapPointGeo.getLongitude(); // 경도
        String apikey = Constants.DAUM_API_KEY;

        String Host = Constants.TRANS_GEO_ADDRESS;
        String query = Host + String.format("?apikey=%s&x=%f&y=%f&inputCoordSystem=%s&output=%s", apikey, _longitude, _latitude, "WGS84", "json");

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                if (status.getCode() >= 400) {
                    // IF ERROR
                    Log.i("StatusCode", status.getCode() + "");
                    Log.i("StatusError", status.getError());
                } else {
                    if (status.getCode() == 200) {
                        // IF NORMAL
                        if (json == null) {
                            Log.i("RETURN", "" + status.getMessage());
                            // 네트워크 오류페이지로 이동
                            return;
                        } else {
                            Log.i("RETURN", "" + json.toString());


                            try {
                                String newName = json.getJSONObject("new").getString("name");
                                String oldName = json.getJSONObject("old").getString("name");

                                title = "";
                                longitude = _longitude;
                                latitude = _latitude;

                                addressName = oldName;
                                newAddressName = newName;

                                //aq.id(R.id.text_title).gone();
                                if(StringUtil.isNullOrEmpty(newName)){
                                    aq.id(R.id.text_address).text(oldName);
                                }else{
                                    aq.id(R.id.text_address).text(newName);
                                }

//                                aq.id(R.id.text_new_address).text("[도로명] " + newName);

//                                if (StringUtil.isNullOrEmpty(newName)) {
//                                    aq.id(R.id.text_new_address).gone();
//
//                                } else {
//                                    aq.id(R.id.text_new_address).visible();
//                                }
                                //searchNear(latitude, longitude);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }

                }
            }
        };
        cb.header("Accept", "application/json");
        cb.header("Content-Type", "application/json; charset=utf-8");
        cb.header(HEADER_NAME_X_APPID, "33199");
        cb.header(HEADER_NAME_X_PLATFORM, HEADER_VALUE_X_PLATFORM_ANDROID);

        aq.ajax(query, JSONObject.class, cb);

        aq.id(R.id.pin).image(R.mipmap.map_pin_gs25);
    }

    private void setLabel() {
        //aq.id(R.id.text_title).visible().text(title);

        if(StringUtil.isNullOrEmpty(newAddressName)){
            aq.id(R.id.text_address).text(addressName);
        }else{
            aq.id(R.id.text_address).text(newAddressName);
        }

    }

}
