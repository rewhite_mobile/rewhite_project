package me.rewhite.users.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.androidquery.AQuery;

import me.rewhite.users.R;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.StringUtil;

public class AlertDialogScreen extends AppCompatActivity {

    private AQuery aq;
    String title;
    String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        setContentView(R.layout.fragment_message_dialog);

        aq = new AQuery(this);

        Intent intent = getIntent();
        if (intent != null) {
            title = intent.getStringExtra("title");
            message = intent.getStringExtra("message");

            aq.id(R.id.text_title).text(title).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.text_message).text(message).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.text_ok).text(intent.getStringExtra("ok_text")).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.text_no).text(intent.getStringExtra("no_text")).typeface(CommonUtility.getNanumBarunTypeface());

            if (StringUtil.isNullOrEmpty(title)) {
                aq.id(R.id.text_title).gone();
            }

        }

        aq.id(R.id.btn_ok).clicked(this, "okClicked");
        aq.id(R.id.btn_no).clicked(this, "noClicked");
        aq.id(R.id.btn_close).clicked(this, "noClicked");
    }

    public void noClicked(View button) {
        Log.e("okCilcked", "NO");
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    public void okClicked(View button) {
        Log.e("okCilcked", "OK");
        Intent resultData = new Intent();
        resultData.putExtra("result", true);
        setResult(Activity.RESULT_OK, resultData);
        finish();
    }

}
