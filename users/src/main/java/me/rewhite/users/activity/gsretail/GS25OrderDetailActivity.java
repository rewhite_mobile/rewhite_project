package me.rewhite.users.activity.gsretail;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.ads.conversiontracking.AdWordsConversionReporter;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.PaymentBridgeActivity;
import me.rewhite.users.activity.ReceiptActivity;
import me.rewhite.users.activity.ReviewActivity;
import me.rewhite.users.activity.ReviewFacebookActivity;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.layout.HeaderView;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.MZValidator;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.TimeUtil;
import me.rewhite.users.util.ToastUtility;


public class GS25OrderDetailActivity extends BaseActivity implements AppBarLayout.OnOffsetChangedListener {

    private static final String TAG = "GS25OrderDetailActivity";
    private Context mContext;
    private AQuery aq;
    ImageButton button = null;

    JSONObject orderInfo;
    String convStoreId = "1002";
    String convStoreName = "";
    String storeId = "1002";
    String pickupMode = "GS";
    String isNewPOS = "N";
    String orderStatus;
    String orderId = "";
    int orderSubType = 151;
    String partnerOrderNo;
    int count_unit = 0;
    int count_pack = 0;
    String request_content_string = "";

    @BindView(R.id.toolbar_header_view)
    protected HeaderView toolbarHeaderView;

    @BindView(R.id.float_header_view)
    protected HeaderView floatHeaderView;

    @BindView(R.id.appbar)
    protected AppBarLayout appBarLayout;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    private boolean isHideToolbarView = false;

    int deliveryPay = 2000;

    private static final int PAYMENT_OK = 1;
    private static final int MOD_DELIVERY_OK = 2;
    private static final int VIEW_RECEIPT = 3;
    private static final int ORDER_OPERATION_CANCEL = 4;
    private static final int ORDER_REVIEWED = 5;
    private static final int ORDER_RECEIVED_POPUP = 6;
    private static final int REQUEST_CONTENT_INPUT = 7;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setTheme(R.style.AppTheme_StatusBarTransparent); // Set here
        setContentView(R.layout.activity_gs25_order_detail);
        ButterKnife.bind(this);
        aq = new AQuery(this);

        Intent intent = getIntent();
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();
            orderId = uri.getQueryParameter("orderId");
            orderSubType = Integer.parseInt(uri.getQueryParameter("orderSubType"));
        } else {
            if (intent != null) {
                orderId = intent.getStringExtra("orderId");
                orderSubType = intent.getIntExtra("orderSubType", 151);
            }
        }

        Log.e("orderSubType", "orderSubType : " + orderSubType);

        if (orderSubType == 151) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.setStatusBarColor(getResources().getColor(R.color.GsStatusbar));
            }
            aq.id(R.id.returning_info).gone();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.setStatusBarColor(getResources().getColor(R.color.theplace_statusbar));
            }

            aq.id(R.id.image).backgroundColorId(R.color.theplace_statusbar);
            //aq.id(R.id.appbar).backgroundColorId(R.color.theplace_statusbar);
            //aq.id(R.id.float_header_view).backgroundColorId(R.color.theplace_statusbar);
            aq.id(R.id.toolbar_header_view).backgroundColorId(R.color.theplace_statusbar);
            aq.id(R.id.btn_cscall).image(R.drawable.btn_theplace_order_detail_cscall);

            aq.id(R.id.btn_takeout).image(R.drawable.btn_theplace_order_takeout);

            aq.id(R.id.gs_icon).image(R.mipmap.icon_list_place);
            aq.id(R.id.returning_info).visible();
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");


        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        mContext = this;


        Log.e("orderId", orderId + "");
        try {
            getDataRefresh(orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        aq.id(R.id.payView).gone();
        aq.id(R.id.priceView).gone();
        aq.id(R.id.takeoutView).gone();

        aq.id(R.id.unit_info_view).gone();
        aq.id(R.id.reviewView).gone();

        aq.id(R.id.btn_cscall).clicked(this, "callAction");
        aq.id(R.id.btn_receipt_01).clicked(this, "showReceipt");
        aq.id(R.id.btn_receipt_02).clicked(this, "showReceipt");
        aq.id(R.id.btn_payment).clicked(this, "paymentAction");
        aq.id(R.id.btn_takeout).clicked(this, "barcodeEnlargeAction");
        aq.id(R.id.btn_order_cancel).clicked(this, "orderCancelAction");
        aq.id(R.id.btn_gs_order_review).clicked(this, "orderReviewAction");


    }

    public void reqMessageInputAction(View button){
        Intent intent = new Intent(this, GSOrderRequestContentActivity.class);
        intent.putExtra("content", request_content_string);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivityForResult(intent, REQUEST_CONTENT_INPUT);
    }

    private void showReviewSelector() {

        aq.id(R.id.payView).gone();
        aq.id(R.id.priceView).gone();
        aq.id(R.id.unit_info_view).gone();

        aq.id(R.id.reviewView).visible();

        if (orderInfo.isNull("rateScore")) {
            aq.id(R.id.review_before).visible();
            aq.id(R.id.review_after).gone();
        } else {
            //"rateScore":{"storeId":1002,"userId":1006,"orderId":0,"rateScore":3,"rateMessage":"세탁 불만족","registerDate":"\/Date(1460547248603)\/"}}
            //{"storeId":0,"userId":0,"orderId":0,"rateScore":3,"rateMessage":"세탁 불만족","registerDate":"\/Date(1460547248603)\/","registerDateTimeApp":1460547248603}}
            aq.id(R.id.review_before).gone();
            aq.id(R.id.review_after).visible();
            try {
                JSONObject rateInfo = orderInfo.getJSONObject("rateScore");
                Log.e("rate info", rateInfo.toString());

                int score = rateInfo.getInt("rateScore");

                switch (score) {
                    case 1:
                        aq.id(R.id.rate_score_text).image("1.0");
                        aq.id(R.id.rate_text).text("최악이에요..");
                        break;
                    case 2:
                        aq.id(R.id.rate_score_text).image("2.0");
                        aq.id(R.id.rate_text).text("별로에요.");
                        break;
                    case 3:
                        aq.id(R.id.rate_score_text).image("3.0");
                        aq.id(R.id.rate_text).text("보통이에요~");
                        break;
                    case 4:
                        aq.id(R.id.rate_score_text).image("4.0");
                        aq.id(R.id.rate_text).text("좋았어요!");
                        break;
                    case 5:
                        aq.id(R.id.rate_score_text).image("5.0");
                        aq.id(R.id.rate_text).text("너무 좋았어요!");
                        break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void orderReviewAction(View button) {
        try {
            Intent reviewIntent = new Intent(mContext, ReviewActivity.class);
            reviewIntent.setAction(Intent.ACTION_GET_CONTENT);
            reviewIntent.putExtra("orderId", orderInfo.getString("orderId"));
            reviewIntent.putExtra("storeId", orderInfo.getString("storeId"));
            startActivityForResult(reviewIntent, ORDER_REVIEWED);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void orderCancelAction(View button) {
        Intent receivePopupIntent = new Intent(mContext, GSOrderReceiveCancelPopup.class);
        receivePopupIntent.setAction(Intent.ACTION_GET_CONTENT);
        receivePopupIntent.putExtra("orderInfo", orderInfo.toString());
        receivePopupIntent.putExtra("orderStatus", currentOrderStatus);
        receivePopupIntent.putExtra("orderSubType", orderSubType);
        receivePopupIntent.putExtra("isNewPOS", isNewPOS);
        startActivityForResult(receivePopupIntent, ORDER_RECEIVED_POPUP);
    }

    public void barcodeEnlargeAction(View button) {
        if ("22".equals(orderStatus)) {
            Intent receivePopupIntent = new Intent(mContext, GSOrderReceiveCompletePopup.class);
            receivePopupIntent.setAction(Intent.ACTION_GET_CONTENT);
            receivePopupIntent.putExtra("orderInfo", orderInfo.toString());
            receivePopupIntent.putExtra("orderStatus", currentOrderStatus);
            receivePopupIntent.putExtra("orderSubType", orderSubType);
            receivePopupIntent.putExtra("isNewPOS", isNewPOS);
            startActivityForResult(receivePopupIntent, ORDER_RECEIVED_POPUP);
        } else {
            //aq.id(R.id.btn_enlarge).gone();
        }
    }

    public void paymentAction(View button) {
        // PG결제로 이동
        orderPaymentPG();
    }

    private void orderPaymentPG() {
        RequestParams params = new RequestParams();
        try {
            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            params.put("orderId", orderInfo.getString("orderId"));
            params.put("k", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkClient.post(Constants.PAYMENT_PRECODE_REQ, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.PAYMENT_PRECODE_REQ, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.PAYMENT_PRECODE_REQ, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            String pgReqCode = jsondata.getString("data");
                            orderCallRequest(pgReqCode);
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    private void orderCallRequest(final String pgReqCode) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent payBridgeIntent = new Intent(mContext, PaymentBridgeActivity.class);
                payBridgeIntent.setAction(Intent.ACTION_GET_CONTENT);
                payBridgeIntent.putExtra("PAM_ID", pgReqCode);
                startActivityForResult(payBridgeIntent, PAYMENT_OK);
            }
        }, 250);

    }

    private void initUi(String _statusText) {
        appBarLayout.addOnOffsetChangedListener(this);

        toolbarHeaderView.bindTo(_statusText, partnerOrderNo);
        floatHeaderView.bindTo(_statusText, partnerOrderNo);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        if (percentage == 1f && isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;
        } else if (percentage < 1f && !isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }
    }

    private void getDataRefresh(String _orderId) throws JSONException {

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_DETAIL, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            orderInfo = jsondata.getJSONObject("data");
                            orderStatus = orderInfo.getString("orderStatus");
                            isNewPOS = orderInfo.getString("isNewPOS");
                            if ("01".equals(orderStatus)) {
                                aq.id(R.id.btn_order_cancel).visible().enabled(true);
                            } else {
                                aq.id(R.id.btn_order_cancel).invisible().enabled(false);  //gone();
                            }

                            if (orderInfo.isNull("partnerOrderNo")) {
                                Log.e("partnerOrderNo", "is Null");
                                partnerOrderNo = orderId;
                            } else {
                                partnerOrderNo = orderInfo.getString("partnerOrderNo");
                            }
                            Log.e("partnerOrderNo", partnerOrderNo);

                            setStatus(orderInfo);
                            //initUi("조회중입니다.");
                        }
                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    aq.id(R.id.payView).gone();
                    aq.id(R.id.priceView).gone();
                    aq.id(R.id.unit_info_view).gone();
                    aq.id(R.id.takeoutView).gone();
                    //aq.id(R.id.btn_enlarge).gone();
                    aq.id(R.id.reviewView).gone();
                }

            }
        });
    }

    private void setStatusText(String _value) {
        appBarLayout.addOnOffsetChangedListener(this);

        //toolbarHeaderView.bindTo(_statusText, partnerOrderNo);
        //floatHeaderView.bindTo(_statusText, partnerOrderNo);

        toolbarHeaderView.bindTo(_value, partnerOrderNo);
        floatHeaderView.bindTo(_value, partnerOrderNo);
    }

    String currentOrderStatus = "01";

    private void setStatus(JSONObject _orderInfo) throws JSONException {

        final Animation fadein = AnimationUtils.loadAnimation(this, R.anim.fadein);
        final Animation fadeout = AnimationUtils.loadAnimation(this, R.anim.fadeout);

        int unitCount = _orderInfo.getInt("pickupQuantity");
        int packCount = _orderInfo.getInt("packQuantity");
        int boxCount = _orderInfo.getInt("boxQuantity");
        String convName = _orderInfo.getJSONObject("partnerStoreV3").getString("partnerStoreName");
        String storeDisplayName = _orderInfo.getString("storeDisplayName");
        String reqMessage = _orderInfo.getString("pickupRequestMessage");
        long pickupTimestamp = _orderInfo.getLong("pickupRequestTimeApp");
        long deliveryTimestamp = _orderInfo.getLong("deliveryRequestTimeApp");
        String orderStatus = _orderInfo.getString("orderStatus");
        currentOrderStatus = orderStatus;

        aq.id(R.id.text_unit_count).text(unitCount + "");
        aq.id(R.id.text_pack_count).text(packCount + "");
        aq.id(R.id.text_conv_store_name).text(convName);
        aq.id(R.id.text_store_name).text(storeDisplayName);
        if ("".equals(reqMessage) || reqMessage.length() == 0) {
            aq.id(R.id.text_req_message).text("주문시 요청사항 없음");
        } else {
            request_content_string = reqMessage;
            if (reqMessage.length() > 15) {
                aq.id(R.id.text_req_message).text(reqMessage.substring(0, 15) + "...");
            } else {
                aq.id(R.id.text_req_message).text(reqMessage);
            }

        }

        aq.id(R.id.text_pickup_time).text(TimeUtil.convertTimestampToGSString(pickupTimestamp));
        aq.id(R.id.text_complete_date).text(TimeUtil.convertTimestampToGSString(deliveryTimestamp));

        String prefixName = "";
        if(orderSubType == 151){
            prefixName = "gs";
        }else if(orderSubType == 152){
            prefixName = "theplace";
        }

//        if ("11".equals(currentOrderStatus) || "12".equals(currentOrderStatus) || "13".equals(currentOrderStatus) || "21".equals(currentOrderStatus) || "22".equals(currentOrderStatus)) {
//            String subInfoImage = null;
//            if ("Y".equals(_orderInfo.getString("isPayment"))) {
//                subInfoImage = "text_"+prefixName+"_order_detail_status_sub_" + currentOrderStatus + "_y";
//            } else {
//                subInfoImage = "text_"+prefixName+"_order_detail_status_sub_" + currentOrderStatus + "_n";
//            }
//            int gsSubId = mContext.getResources().getIdentifier(subInfoImage, "mipmap", mContext.getPackageName());
//            Log.i("setStatus", "Res Name: " + subInfoImage + "==> Res ID = " + gsSubId);
//            aq.id(R.id.image_sub_text).image(gsSubId);
//        } else {
//            String subInfoImage = "text_"+prefixName+"_order_detail_status_sub_" + orderStatus;
//            int gsSubId = mContext.getResources().getIdentifier(subInfoImage, "mipmap", mContext.getPackageName());
//            Log.i("setStatus", "Res Name: " + subInfoImage + "==> Res ID = " + gsSubId);
//            aq.id(R.id.image_sub_text).image(gsSubId);
//        }

        int intCurrentOrderStatus = Integer.parseInt(currentOrderStatus);
        switch (intCurrentOrderStatus){
            case 1:

            case 2:
                setStatusText("접수완료");
                aq.id(R.id.pick_date_text_title).text("수령예정일");
                aq.id(R.id.sub_text).text("편의점에서 보관 중(세탁소 이동 전)");
                aq.id(R.id.req_message_layout).clicked(this, "reqMessageInputAction");
                break;
            case 3:
                setStatusText("세탁소로 이동중");
                aq.id(R.id.pick_date_text_title).text("수령예정일");
                aq.id(R.id.sub_text).text("세탁물 검수 후 인수증 발급예정");
                aq.id(R.id.req_message_layout).clicked(this, "reqMessageInputAction");
                break;
            case 11:
                setStatusText("세탁 준비중");
                // 결제여부 구분
                if ("Y".equals(_orderInfo.getString("isPayment"))) {
                    aq.id(R.id.sub_text).text("세탁완료 후 편의점 입고 시 문자 발송예정");
                }else{
                    aq.id(R.id.sub_text).text("세탁물 인수증 확인, 결제는 리화이트 앱에서만 가능!");
                }
                aq.id(R.id.pick_date_text_title).text("수령예정일");
                break;
            case 12:
                setStatusText("세탁중");
                // 결제여부 구분
                if ("Y".equals(_orderInfo.getString("isPayment"))) {
                    aq.id(R.id.sub_text).text("세탁완료 후 편의점 입고 시 문자 발송예정");
                }else{
                    aq.id(R.id.sub_text).text("세탁물 인수증 확인, 결제는 리화이트 앱에서만 가능!");
                }
                aq.id(R.id.pick_date_text_title).text("수령예정일");
                break;
            case 13:
                setStatusText("세탁완료");
                // 결제여부 구분
                if ("Y".equals(_orderInfo.getString("isPayment"))) {
                    aq.id(R.id.sub_text).text("세탁완료 후 편의점 입고 시 문자 발송예정");
                }else{
                    aq.id(R.id.sub_text).text("세탁물 인수증 확인, 결제는 리화이트 앱에서만 가능!");
                }
                aq.id(R.id.pick_date_text_title).text("수령예정일");
                break;
            case 21:
                if(orderSubType == 151){
                    setStatusText("편의점 이동준비중");
                }else if(orderSubType == 152){
                    setStatusText("플레이스 이동준비중");
                }
                aq.id(R.id.pick_date_text_title).text("수령예정일");
                // 결제여부 구분
                if ("Y".equals(_orderInfo.getString("isPayment"))) {
                    aq.id(R.id.sub_text).text("편의점 입고 후 문자 발송예정");
                }else{
                    aq.id(R.id.sub_text).text("리화이트 앱에서 결제해야만 세탁물 수령가능!\n" +
                            "*현장결제가 불가능하니, 리화이트 앱에서 결제주세요.");
                }
                break;
            case 22:
                if(orderSubType == 151){
                    setStatusText("편의점 수령대기중");
                    aq.id(R.id.pick_date_text_title).text("편의점 입고일");
                }else if(orderSubType == 152){
                    setStatusText("플레이스 수령대기중");
                    aq.id(R.id.pick_date_text_title).text("플레이스 입고일");
                }

                // New POS 구분
                // 결제여부 구분
                if("Y".equals(_orderInfo.get("isNewPOS"))){
                    // New POS 지점
                    if ("Y".equals(_orderInfo.getString("isPayment"))) {
                        aq.id(R.id.sub_text).text("수령지연 시 세탁소로 이동 후 보관예정(보관료 발생예정)");
                    }else{
                        aq.id(R.id.sub_text).text("수령지연 시 세탁소로 이동 후 보관예정(보관료 발생예정)\n" +
                                "*현장결제가 불가능하니, 리화이트 앱에서 결제해주세요.");
                    }
                }else{
                    if ("Y".equals(_orderInfo.getString("isPayment"))) {
                        aq.id(R.id.sub_text).text("편의점 입고완료, 지금 편의점에 방문해주세요.");
                    }else{
                        aq.id(R.id.sub_text).text("편의점 입고완료, 결제 후 편의점에 방문해주세요.\n" +
                                " *현장결제가 불가능하니, 리화이트 앱에서 결제해주세요.");
                    }
                }

                break;
            case 23:
            case 24:
                setStatusText("수령완료");
                aq.id(R.id.pick_date_text_title).text("수령완료일");
                // 리뷰작성여부 체크
                if(_orderInfo.getJSONObject("rateScore").getInt("rateScore") == 0){
                    // 미작성시
                    aq.id(R.id.sub_text).text("서비스 리뷰 작성하고 포인트 받으세요.");
                }else{
                    // 작성완료
                    aq.id(R.id.sub_text).text("다음에 다시 이용해주세요~");
                }
                break;
            case 61:
                setStatusText("세탁진행 중단");
                aq.id(R.id.pick_date_text_title).text("수령예정일");
                aq.id(R.id.sub_text).text("세탁물을 곧 돌려드릴 예정입니다.");
                break;
            case 62:
                setStatusText("세탁진행 중단");
                aq.id(R.id.pick_date_text_title).text("편의점 입고일");
                aq.id(R.id.sub_text).text("세탁물을 곧 돌려드릴 예정입니다.");
                break;
            case 82:
                setStatusText("세탁소 보관중");
                aq.id(R.id.pick_date_text_title).text("유료보관 시작일");
                // 결제여부 구분
                if ("Y".equals(_orderInfo.getString("isPayment"))) {
                    aq.id(R.id.sub_text).text("세탁물 보관비용 발생 중! \n" +
                            "* 지금 고객센터로 연락주세요.");
                }else{
                    aq.id(R.id.sub_text).text("세탁물 보관비용 발생 중! \n" +
                            "* 지금 고객센터로 연락주세요.");
                }
                break;
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
            case 96:
                setStatusText("접수 취소");
                aq.id(R.id.pick_date_text_title).text("접수취소일");
                aq.id(R.id.sub_text).text("이미 취소한 내역입니다.");
                break;
            case 97:
                setStatusText("세탁진행 중단");
                aq.id(R.id.pick_date_text_title).text("수령완료일");
                aq.id(R.id.sub_text).text("다음에 다시 이용해주세요~");
                break;
            case 98:
            case 99:
                setStatusText("접수 취소");
                aq.id(R.id.pick_date_text_title).text("접수취소일");
                aq.id(R.id.sub_text).text("취소된 내역입니다.");
                break;
            default:
                setStatusText("접수 취소");
                aq.id(R.id.pick_date_text_title).text("접수취소일");
                aq.id(R.id.sub_text).text("이미 취소한 내역입니다.");
                break;
        }

        /*
         Init Visibility Setting
         */
        aq.id(R.id.payView).gone();
        aq.id(R.id.priceView).gone();
        aq.id(R.id.unit_info_view).gone();
        aq.id(R.id.takeoutView).gone();
        //aq.id(R.id.btn_enlarge).gone();
        aq.id(R.id.reviewView).gone();
        aq.id(R.id.desc_payenable).gone();
        aq.id(R.id.text_lefttime).gone();

        if ("01".equals(orderStatus) || "02".equals(orderStatus)) {

        } else if ("03".equals(orderStatus)) {

        } else {
            // 품목등록 이후
            aq.id(R.id.priceView).visible(); // 가격표시
            aq.id(R.id.unit_info_view).visible(); // 품목항목 표시
            aq.id(R.id.count_info_view).gone(); // 수량표시 숨김
            aq.id(R.id.takeoutView).gone();

            int price = Integer.parseInt(orderInfo.getString("deliveryPrice"));
            if (price < 20000) {
                price = price + deliveryPay;
            }
            String priceString = MZValidator.toNumFormat(price);
            aq.id(R.id.text_price_01).text(priceString);
            aq.id(R.id.text_price_02).text(priceString);
            aq.id(R.id.text_takeout_price).text(priceString);

            if ("N".equals(orderInfo.getString("isPayment"))) {
                // 결제 미완료 상태 : 결제유도장치 표시
                aq.id(R.id.payView).visible();
                aq.id(R.id.priceView).gone();
                if ("11".equals(orderStatus) || "12".equals(orderStatus) || "13".equals(orderStatus)) {

                }else if("82".equals(orderStatus)){
                    aq.id(R.id.desc_payenable).visible();
                    aq.id(R.id.desc_payenable).image(R.mipmap.gs25_detail_top_bottomtext_cs_info);
                }else if ("21".equals(orderStatus) || "22".equals(orderStatus) ) {
                    aq.id(R.id.desc_payenable).visible();
                    aq.id(R.id.desc_payenable).image(R.mipmap.gs25_detail_top_bottomtext_paynotice2);
                }

            } else {
                aq.id(R.id.payView).gone();
                aq.id(R.id.priceView).visible();
                aq.id(R.id.desc_payenable).gone();
            }

            if (orderInfo.getJSONArray("pickupItemsV2").length() > 0) {
                String orderRequest = orderInfo.getJSONArray("pickupItemsV2").getJSONObject(0).getString("itemTitle") + " 등 " + orderInfo.getJSONArray("pickupItemsV2").length() + "개";
                aq.id(R.id.text_unit_summary).text(orderRequest);
            } else {
                aq.id(R.id.text_unit_summary).text("입력품목이 없습니다.");
            }

            if ("22".equals(orderStatus)) {

                JSONArray history = orderInfo.getJSONArray("history");
                long deliveriedTimestamp = 0;
                for (int i = 0; i < history.length(); i++) {
                    if ("22".equals(history.getJSONObject(i).getString("orderStatus"))) {
                        deliveriedTimestamp = history.getJSONObject(i).getLong("statusDateApp");
                    }
                }
                Date startDate = new Date(deliveriedTimestamp);
                Date nowDate = new Date();
                long duration = nowDate.getTime() - startDate.getTime();
                long diffDay = duration / 1000 / (60 * 60 * 24);
                String dayResultString = "";
                if (diffDay <= 0) {
                    dayResultString = "오늘 도착했습니다.";
                } else {
                    dayResultString = diffDay + "일째 기다리는중";
                }
                // aq.id(R.id.text_lefttime).visible().text(dayResultString);
                //aq.id(R.id.btn_enlarge).visible();
                aq.id(R.id.priceView).gone();

                if ("N".equals(orderInfo.getString("isPayment"))) {
                    // 결제 미완료 상태 : 결제유도장치 표시
                    aq.id(R.id.payView).visible();
                    aq.id(R.id.takeoutView).gone();
                } else {
                    aq.id(R.id.payView).gone();
                    aq.id(R.id.takeoutView).visible();
                }


            } else if ("23".equals(orderStatus)) {
                showReviewSelector();
            } else if ("24".equals(orderStatus)) {
                showReviewSelector();
            } else if ("91".equals(orderStatus) || "95".equals(orderStatus) || "96".equals(orderStatus) || "99".equals(orderStatus)) {

                aq.id(R.id.payView).gone();
                aq.id(R.id.priceView).gone();
                aq.id(R.id.unit_info_view).gone();
                aq.id(R.id.reviewView).gone();
                aq.id(R.id.takeoutView).gone();
                aq.id(R.id.desc_payenable).gone();
            }
        }

    }

    /*
        고객센터로 전화걸기
     */

    public void callAction(View button) {

        Intent intent = new Intent(this, GS25CSPopupActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);


//
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (!canAccessCall()) {
//                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
//            } else {
//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//                alertDialogBuilder.setCancelable(true)
//                        .setMessage("리화이트 고객센터와 전화통화를 원하십니까? (상담가능시간 : 평일 오전10시~오후7시)")
//                        .setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                Intent callIntent = new Intent(Intent.ACTION_CALL);
//                                //callIntent.setAction("android.intent.action.DIAL");
//
//                                callIntent.setData(Uri.parse("tel:15442951"));
//                                if(callIntent.resolveActivity(getPackageManager()) != null){
//                                    startActivity(callIntent);
//                                }
//                            }})
//                        .setNegativeButton("아니오", new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//
//                            }
//                        });
//                AlertDialog alertDialog = alertDialogBuilder.create();
//                alertDialog.show();
//            }
//        } else {
//            call();
//        }
    }

    public void call() {
        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("GS주문")
                .setAction("고객센터 전화")
                //.setLabel("Order")
                .build());
        AppEventsLogger logger = AppEventsLogger.newLogger(mContext);
        logger.logEvent("Call CSCenter");

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:15442951"));
        if (callIntent.resolveActivity(getPackageManager()) != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

                return;
            }else{
                startActivity(callIntent);
            }

        }
    }

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.CALL_PHONE
    };

    private static final int INITIAL_REQUEST = 1337;


    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }
    private boolean canAccessCall() {
        return(hasPermission(Manifest.permission.CALL_PHONE));
    }
    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case INITIAL_REQUEST:
                if (canAccessCall()) {

                }
                else {
                    Toast.makeText(this, "전화걸기 권한을 거절하셔서 통화연결이 불가합니다.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    /*
        인수증 보기
     */
    public void showReceipt(View button) {

        try {
            Intent receiptIntent = new Intent(this, ReceiptActivity.class);
            receiptIntent.setAction(Intent.ACTION_GET_CONTENT);
            receiptIntent.putExtra("ORDER_ID", orderInfo.getString("orderId"));
            startActivityForResult(receiptIntent, VIEW_RECEIPT);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PAYMENT_OK:
                    try {
                        String orderId = orderInfo.getString("orderId");
                        String result = data.getStringExtra("RESULT");
                        if("Y".equals(result)){
                            Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                            t.send(new HitBuilders.EventBuilder()
                                    .setCategory("USER")
                                    .setAction("결제성공")
                                    .setLabel("Payment")
                                    .build());
                            int price = Integer.parseInt(orderInfo.getString("deliveryPrice"));
                            if(price < 20000){
                                price = price + deliveryPay;
                            }
                            AppEventsLogger logger = AppEventsLogger.newLogger(this);
                            logger.logPurchase(BigDecimal.valueOf(price), Currency.getInstance("KRW"));

                            Toast.makeText(this, "성공적으로 결제되었습니다.", Toast.LENGTH_LONG).show();

                            AdWordsConversionReporter.reportWithConversionId(this.getApplicationContext(),
                                    "939419543", "HK5tCMqTrWsQl8_5vwM", "0.00", true);

                            getDataRefresh(orderId);
                        }else if("N".equals(result)){
                            Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                            t.send(new HitBuilders.EventBuilder()
                                    .setCategory("USER")
                                    .setAction("결제실패")
                                    .setLabel("Payment")
                                    .build());

                            Toast.makeText(this, "결제에 실패했습니다.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case ORDER_REVIEWED:
                    Tracker t2 = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                    t2.send(new HitBuilders.EventBuilder()
                            .setCategory("USER")
                            .setAction("주문평가")
                            .setLabel("GSOrder")
                            .build());
                    ToastUtility.show(this, "정상적으로 평가되었습니다.", Toast.LENGTH_SHORT);
                    aq.id(R.id.review_before).gone();
                    aq.id(R.id.review_after).visible();
                    try {
                        String orderId = orderInfo.getString("orderId");
                        getDataRefresh(orderId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String result = data.getStringExtra("RESULT");
                    if("Y".equals(result)){
                        Intent reviewFacebookIntent = new Intent(this, ReviewFacebookActivity.class);
                        startActivity(reviewFacebookIntent);
                    }else{

                    }

                    break;
                case ORDER_RECEIVED_POPUP:
                    Toast.makeText(this, "정상적으로 처리되었습니다.", Toast.LENGTH_LONG).show();
                    //String orderId = data.getStringExtra("RESULT");
                    try {
                        String orderId = orderInfo.getString("orderId");
                        getDataRefresh(orderId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case REQUEST_CONTENT_INPUT:
                    // 요청사항 수정하기
                    Tracker t3 = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                    t3.send(new HitBuilders.EventBuilder()
                            .setCategory("USER")
                            .setAction("요청사항입력")
                            .setLabel("GSOrder")
                            .build());

                    //ORDER_ADD_REQ_MESSAGE
                    //
                    try {
                        String orderId = orderInfo.getString("orderId");
                        String pickupRequestMessage = data.getStringExtra("INPUT_CONTENT");

                        RequestParams params = new RequestParams();
                        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                        params.put("orderId", orderId);
                        params.put("pickupRequestMessage", pickupRequestMessage);
                        params.put("k", 1);
                        NetworkClient.post(Constants.ORDER_ADD_REQ_MESSAGE, params, new AsyncHttpResponseHandler() {

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                                // TODO Auto-generated method stub
                                DUtil.Log(Constants.ORDER_ADD_REQ_MESSAGE, error.getMessage());
                            }

                            @Override
                            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                                String result;
                                try {
                                    result = new String(data, "UTF-8");
                                    DUtil.Log(Constants.ORDER_ADD_REQ_MESSAGE, result);

                                    JSONObject jsondata = new JSONObject(result);

                                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                                        if (jsondata.isNull("data")) {
                                            // 주문내역이 없습니다
                                        } else {
                                            String orderId = orderInfo.getString("orderId");
                                            getDataRefresh(orderId);
                                        }
                                    } else {

                                    }
                                } catch (UnsupportedEncodingException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                } catch (JSONException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    break;
                default:
                    break;
            }
        }else if(resultCode == RESULT_CANCELED){
            switch (requestCode) {
                case PAYMENT_OK:
                    if (Build.VERSION.SDK_INT == 19) {

                    }else{
                        Toast.makeText(this, "사용자의 요청으로 결제가 취소되었습니다.", Toast.LENGTH_LONG).show();

                        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                        t.send(new HitBuilders.EventBuilder()
                                .setCategory("USER")
                                .setAction("주문취소")
                                .setLabel("Payment")
                                .build());
                    }

                    break;
                case REQUEST_CONTENT_INPUT:

                    break;
                default:

                    break;
            }
        }
    }
}
