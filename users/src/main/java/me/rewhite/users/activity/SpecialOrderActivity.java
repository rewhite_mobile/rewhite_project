package me.rewhite.users.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;

public class SpecialOrderActivity extends BaseActivity {

    private AQuery aq;
    private final Context mCtx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special_order);

        aq = new AQuery(this);

        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
        logger.logEvent("Season Order Try");

        aq.id(R.id.btn_send).clicked(this, "sendClicked");
        aq.id(R.id.btn_input).clicked(this, "inputClicked");

        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
    }

    public void closeClicked(View button) {
        finish();
    }

    public void sendClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, SPOrderSendActivity.class);
                startActivity(intent);

            }
        }, 100);
    }

    public void inputClicked(View button){
        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
        logger.logEvent("Season Order Inputed");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, SPOrderInputActivity.class);
                startActivity(intent);

            }
        }, 100);
    }
}
