package me.rewhite.users.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.R;
import me.rewhite.users.activity.gsretail.GS25OrderDetailActivity;
import me.rewhite.users.adapter.OrderListItem;
import me.rewhite.users.adapter.OrderListViewAdapter;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class OrderHistoryActivity extends BaseActivity {

    Context ctx = this;
    AQuery aq;
    private ArrayList<OrderListItem> orderData;
    private OrderListViewAdapter orderAdapter;

    @Override
    public void onResume() {
        super.onResume();
        initialize();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
        initialize();
    }

    public void closeClicked(View button) {
        finish();
    }

    private void initialize() {
        orderData = new ArrayList<>();

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("page", 1);
        params.put("block", 100);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_LIST, error.getMessage());


            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_LIST, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            // locationListView
                            ListView orderListView = (ListView) findViewById(R.id.listView);
                            orderData = new ArrayList<>();

                            JSONArray orderInfo = jsondata.getJSONArray("data");

                            if (orderInfo.length() > 0) {
                                aq.id(R.id.empty_screen).gone();
                                for (int i = 0; i < orderInfo.length(); i++) {

                                    String orderId = orderInfo.getJSONObject(i).getString("orderId");
                                    String orderStatus = orderInfo.getJSONObject(i).getString("orderStatus");
                                    String addressSeq = orderInfo.getJSONObject(i).getString("addressSeq");
                                    String storeId = orderInfo.getJSONObject(i).getString("storeId");
                                    String deliveryPrice = orderInfo.getJSONObject(i).getString("deliveryPrice");
                                    String pickupRequestTimeApp = orderInfo.getJSONObject(i).getString("pickupRequestTimeApp");
                                    String deliveryRequestTimeApp = orderInfo.getJSONObject(i).getString("deliveryRequestTimeApp");
                                    String registerDateApp = orderInfo.getJSONObject(i).getString("registerDateApp");
                                    String orderRequest = orderInfo.getJSONObject(i).getString("orderRequest");
                                    String orderPickupItemMessage = orderInfo.getJSONObject(i).getString("orderPickupItemMessage");
                                    String statusTimeApp = orderInfo.getJSONObject(i).getString("statusTimeApp");
                                    String storeName = null;
                                    String storeDeliveryTime = orderInfo.getJSONObject(i).getString("storeDeliveryTime");
                                    String isPayment = orderInfo.getJSONObject(i).getString("isPayment");
                                    int orderSubType = orderInfo.getJSONObject(i).getInt("orderSubType");

                                    if(orderSubType == 151 || orderSubType == 152){
                                        // 편의점 점포명
                                        if(orderInfo.getJSONObject(i).isNull("partnerStoreName")){
                                            storeName = orderInfo.getJSONObject(i).getString("storeDisplayName");
                                        }else{
                                            storeName = orderInfo.getJSONObject(i).getString("partnerStoreName");
                                        }
                                    }else{
                                        storeName = orderInfo.getJSONObject(i).getString("storeDisplayName");
                                    }

                                    OrderListItem aItem = new OrderListItem(orderId, orderStatus, addressSeq, storeId, storeName, deliveryPrice, pickupRequestTimeApp,
                                            deliveryRequestTimeApp, registerDateApp, orderRequest, statusTimeApp, orderPickupItemMessage,storeDeliveryTime, isPayment, orderSubType);
                                    orderData.add(aItem);
                                }

                                orderAdapter = new OrderListViewAdapter(ctx, R.layout.order_history_item, orderData);
                                orderAdapter.notifyDataSetChanged();

                                orderListView.setAdapter(orderAdapter);

                            } else {
                                aq.id(R.id.empty_screen).visible();
                            }
                            // orderId, orderStatus, storeId,
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    public static final int ORDER_LIST_REFRESH = 9;

    public void itemSelected(int position) {
        String orderId = orderData.get(position).getOrderId();
        int orderSubType = orderData.get(position).getOrderSubType();

        if(orderSubType == 151 || orderSubType == 152){

            Intent intent = new Intent();
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.setClass(ctx, GS25OrderDetailActivity.class);
            intent.putExtra("orderSubType", orderSubType);
            intent.putExtra("orderId", orderId);
            startActivityForResult(intent, ORDER_LIST_REFRESH);
            overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);

        }else{

            Intent intent = new Intent();

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.setClass(ctx, OrderDetailActivity.class);
            intent.putExtra("orderId", orderId);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        }

    }
}
