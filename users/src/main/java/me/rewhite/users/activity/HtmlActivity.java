package me.rewhite.users.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;


@SuppressLint({"NewApi", "JavascriptInterface", "SetJavaScriptEnabled"})
public class HtmlActivity extends BaseActivity {

    private AQuery aq;
    WebView webView;
    ProgressDialog dialog;
    public Context ctx = this;

    String currentTerms;
    String currentName;

    @Override
    protected void onResume() {
        super.onResume();
        if(dialog != null && dialog.isShowing()) dialog.dismiss();
/*
        if(webView != null){
            webView.clearHistory();
            //webView.loadUrl(currentTerms);
            redirect = true;
        }*/
    }

    public void shareClicked(View button){
        Log.e("htmlAct", currentTerms);
        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("기타")
                .setAction("이벤트공유")
                .setLabel(currentName)
                .build());

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "우리동네 배달세탁소 리화이트\n");
        sendIntent.putExtra(Intent.EXTRA_TEXT, "\n지금 가장 핫한 이벤트입니다. 꼭 참여해서 당첨되길~!\n" + currentTerms+ "\n");
        sendIntent.setType("text/plain");

        startActivity(Intent.createChooser(sendIntent, "이벤트 공유하기"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_html);


        Intent myIntent = getIntent();
        currentTerms = myIntent.getExtras().getString("TERMS_URI");
        currentName = myIntent.getExtras().getString("TITLE_NAME");


        aq = new AQuery(this);
        aq.id(R.id.title_text).text(currentName);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        webView = (WebView) findViewById(R.id.webview_area);

        if(currentTerms.startsWith("http://www.rewhite.me/app/event/") || currentTerms.startsWith("https://www.rewhite.me/app/event/")){
            aq.id(R.id.btn_share).clicked(this, "shareClicked").visible();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
            @Override
            public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
                return super.onJsConfirm(view, url, message, result);
            }

            @Override
            public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, final JsPromptResult result) {
                return super.onJsPrompt(view, url, message, defaultValue, result);
            }

        });
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON_DEMAND);
        webView.setWebViewClient(new MyWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);

        Log.e("webview", currentTerms);
        webView.loadUrl(currentTerms);
        //aq.id(R.id.top).gone();
        /*
		aq.id(R.id.title_name).text("FAQ");
		aq.id(R.id.btn_setting).clicked(this, "settingStart");
		aq.id(R.id.btn_info).clicked(this, "infoStart");*/

        // Progress 처리 진행상황을 보기위해
        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.web_loading_comment));
    }

    boolean loadingFinished = true;
    boolean redirect = true;
    public String lastUrlString = "";

    public void closeClicked(View button) {
        finish();
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
            Log.e("urlNewString", urlNewString);
            if (urlNewString != null && urlNewString.startsWith("market://")) {
                try {
                    view.getContext().startActivity(
                            new Intent(Intent.ACTION_VIEW, Uri.parse(urlNewString)));

                    if(dialog != null && dialog.isShowing()) dialog.dismiss();
                    //view.loadUrl(currentTerms);
                    finish();
                    return true;
                } catch (ActivityNotFoundException e) {
                    // Google Play app is not installed, you may want to open the app store link
                    Log.e("ActivityNotFoundException", e.getLocalizedMessage());
                    Uri uri = Uri.parse(urlNewString);
                    view.loadUrl("http://play.google.com/store/apps/" + uri.getHost() + "?" + uri.getQuery());
                    return false;
                }
            }else if( urlNewString.startsWith("http:") || urlNewString.startsWith("https:") ) {
                loadingFinished = false;
                view.loadUrl(urlNewString);
                return false;
            }else if (urlNewString.startsWith("tel:")) {
                Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse(urlNewString));
                startActivity(tel);
                return true;
            }else if (urlNewString.startsWith("intent:")) {
                try {
                    Intent intent = Intent.parseUri(urlNewString, Intent.URI_INTENT_SCHEME);
                    Intent existPackage = getPackageManager().getLaunchIntentForPackage(intent.getPackage());
                    if (existPackage != null) {
                        startActivity(intent);
                        return true;
                    } else {
                        try {
                            Activity host = (Activity) view.getContext();
                            Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                            marketIntent.setData(Uri.parse("market://details?id="+intent.getPackage()));
                            host.startActivity(marketIntent);
                            return true;
                        }catch (ActivityNotFoundException e) {
                            // Google Play app is not installed, you may want to open the app store link
                            Uri uri = Uri.parse(urlNewString);
                            view.loadUrl("http://play.google.com/store/apps/" + uri.getHost() + "?" + uri.getQuery());
                            return false;
                        }

                    }
                    //return true;
                }catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }

            return false;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap facIcon) {
            loadingFinished = false;
            // SHOW LOADING IF IT ISNT ALREADY VISIBLE

            Log.e("onPageStarted", url + " , " + currentTerms);
            lastUrlString = url;

            if(!HtmlActivity.this.isFinishing()) {
                if(dialog != null) dialog.show();
            }

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if(dialog != null && dialog.isShowing()) dialog.dismiss();

        }
    }

    @Override
    protected void onDestroy() {
        Log.d("onDestroy", "called onDestroy");
        if (dialog != null){
            dialog.dismiss();
        }
        super.onDestroy();
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }
    }
}
