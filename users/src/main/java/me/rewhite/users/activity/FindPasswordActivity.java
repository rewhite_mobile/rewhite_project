package me.rewhite.users.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.MZValidator;
import me.rewhite.users.util.StringUtil;

public class FindPasswordActivity extends BaseActivity {

    private AQuery aq;
    private Context mCtx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_password);

        aq = new AQuery(this);

        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
        logger.logEvent("Search password");

        aq.id(R.id.btn_send).clicked(this, "checkAction");
        aq.id(R.id.btn_back).clicked(this, "backAction");
    }

    public void backAction(View button) {
        finish();
    }

    public void checkAction(View button) {
        if (!StringUtil.isNullOrEmpty(aq.id(R.id.input_email).getText().toString())) {
            String loginId = aq.id(R.id.input_email).getText().toString();
            if (MZValidator.checkEmail(loginId)) {
                findStart(loginId);
            } else {
                DUtil.alertShow(this, "E-mail주소가 아닙니다.");
            }
        }
    }

    private void findStart(String loginId) {
        RequestParams params = new RequestParams();
        params.put("loginId", loginId);
        params.put("k", 1);

        NetworkClient.post(Constants.PASSWORD_RESET, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.PASSWORD_RESET, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.PASSWORD_RESET, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(FindPasswordActivity.this);
                        alertDialogBuilder.setCancelable(false).setMessage("E-mail 확인 후 비밀번호를 재설정하신 후에 사용해주세요.")
                                .setPositiveButton("네", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    } else if ("S9002".equals(jsondata.getString("resultCode"))) {
                        DUtil.alertShow(FindPasswordActivity.this, "가입하신 정보가 없거나 찾을 수 없어요. 입력하신 E-mail을 다시 확인해주세요.");
                    } else {
                        DUtil.alertShow(FindPasswordActivity.this, jsondata.getString("message"));
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }
}
