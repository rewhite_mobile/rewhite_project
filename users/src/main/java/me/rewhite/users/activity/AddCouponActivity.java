package me.rewhite.users.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class AddCouponActivity extends BaseActivity {

    AQuery aq;
    Context mCtx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_coupon);

        aq = new AQuery(this);
        aq.id(R.id.title_text).text(getString(R.string.title_add_coupon));
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        aq.id(R.id.btn_confirm).clicked(this, "couponAddClicked");
        e = (EditText)findViewById(R.id.text_input);
        //e.addTextChangedListener(textWatcher);
    }

    EditText e;

    public void closeClicked(View button) {
        finish();
    }
//
//    private void arrangeButton(boolean _isOK){
//
//        e.addTextChangedListener(textWatcher);
//        e.setSelection(e.getText().length());
//    }
//
//    EditText e;
//    TextWatcher textWatcher = new TextWatcher() {
//        @Override
//        public void afterTextChanged(Editable edit) {
//            e.removeTextChangedListener(textWatcher);
//            String s = edit.toString();
//            String changedS = validateCouponNumber(s);
//            e.setText(changedS);
//
//            if (changedS.length() == 19) {
//                arrangeButton(true);
//            }else{
//                arrangeButton(false);
//            }
//        }
//
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count,
//                                      int after) {
//
//        }
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before,
//                                  int count) {
//        }
//    };

    public String validateCouponNumber(String _value){
        String temp = _value.replace("-","").toUpperCase();

        if(temp.length() <= 4){
            return temp;
        }else if(temp.length() > 4 && temp.length() <= 8){
            return temp.substring(0,4) + "-" + temp.substring(4);
        }else if(temp.length() > 8 && temp.length() <= 12){
            return temp.substring(0,4) + "-" + temp.substring(4,8) + "-" + temp.substring(8);
        }else if(temp.length() > 12 && temp.length() <= 16){
            return temp.substring(0,4) + "-" + temp.substring(4,8) + "-" + temp.substring(8,12) + "-" + temp.substring(12);
        }
        return temp;
    }

    public void couponAddClicked(View button){
        Log.i("couponAdded", "Success");
//        if(e.getText().toString().length() != 19){
//            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
//            alertDialogBuilder.setCancelable(false).setMessage("쿠폰번호는 16자리입니다.")
//                    .setPositiveButton(getString(R.string.dialog_btn_ok), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//
//                        }
//                    });
//            AlertDialog alertDialog = alertDialogBuilder.create();
//            alertDialog.show();
//            return;
//        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                RequestParams params = new RequestParams();
                params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                params.put("couponNumber", e.getText().toString());
                params.put("k", 1);
                NetworkClient.post(Constants.ADD_COUPON, params, new AsyncHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                        // TODO Auto-generated method stub
                        DUtil.Log(Constants.ADD_COUPON, error.getMessage());
                    }

                    @Override
                    public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                        String result;
                        try {
                            result = new String(data, "UTF-8");
                            DUtil.Log(Constants.ADD_COUPON, result);

                            JSONObject jsondata = new JSONObject(result);


                            if ("S0000".equals(jsondata.getString("resultCode"))) {
                                if (jsondata.isNull("data")) {
                                    //
                                } else {
                                    Tracker t = ((Bootstrap)getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                                    t.send(new HitBuilders.EventBuilder()
                                            .setCategory("쿠폰및포인트")
                                            .setAction("쿠폰등록")
                                            //.setLabel("Order")
                                            .build());
                                    AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                                    logger.logEvent("Register Coupon Completed");



                                    int retCode = jsondata.getInt("data");
                                    if(retCode > 0){
                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                                        alertDialogBuilder.setCancelable(false).setMessage(getString(R.string.message_coupon_added_success))
                                                .setPositiveButton(getString(R.string.dialog_btn_ok), new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        finish();
                                                    }
                                                });
                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        alertDialog.show();

                                    }
                                }
                            }else{
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                                alertDialogBuilder.setCancelable(false).setMessage(jsondata.getString("message"))
                                        .setPositiveButton(getString(R.string.dialog_btn_ok), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            }
                        } catch (UnsupportedEncodingException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        } catch (JSONException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                    }
                });
            }
        }, 200);
    }
}
