package me.rewhite.users.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.fragment.DeliveryModFragmentV2;
import me.rewhite.users.fragment.IFragment;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class ModDeliveryInfoActivity extends BaseActivity {

    private final static String TAG = ModDeliveryInfoActivity.class.getSimpleName();
    public static final String FRAGMENT_DELIVERY = "order_delivery-mod";

    public final Context mCtx = this;
    AQuery aq;
    public JSONObject storeInfo;
    public long deliveryReqTime = 0;
    public long pickupReqTime = 0;
    public String storeClosingDay;
    public String storeDayoff;
    public String storeId;
    public String addressSeq;
    public int washingDays = 3;// 배달일기준 : 수거일기준 3일후


    private Fragment mContent;

    private boolean mIsTouchDisable; // 연속 터치로 생기는 이슈 막기 위함
    public JSONObject orderInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_delivery_info);

        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        aq.id(R.id.title_text).typeface(CommonUtility.getNanumBarunTypeface());

        Intent intent = getIntent();
        if (intent != null) {
            try {
                String info = intent.getStringExtra("INFO");
                orderInfo = new JSONObject(info);
                deliveryReqTime = orderInfo.getLong("deliveryRequestTimeApp");
                pickupReqTime = orderInfo.getLong("pickupRequestTimeApp");
                washingDays = orderInfo.getInt("washingDays");
                storeId = orderInfo.getString("storeId");
                addressSeq = orderInfo.getString("addressSeq");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        initilize();
    }

    public void setTitle(String _title) {
        aq.id(R.id.title_text).text(_title);
    }

    public void closeClicked(View button) {
        onBackPressed();
    }

    private void initilize() {

        RequestParams params = new RequestParams();
        try {
            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            params.put("storeId", orderInfo.getString("storeId"));
            params.put("k", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkClient.post(Constants.SHOW_STORE_INFO, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SHOW_STORE_INFO, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.SHOW_STORE_INFO, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            storeInfo = jsondata.getJSONObject("data");
                            storeClosingDay = storeInfo.getString("storeClosingDay");
                            storeDayoff = storeInfo.getString("storeDayoff");
                            showFragment(FRAGMENT_DELIVERY);
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    public void showFragment(String fragmentName) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);
        showFragment(fragmentName, null);
    }

    public void showFragment(String fragmentName, Bundle args) {
        Log.d(TAG, "[showFragment] fragmentName : " + fragmentName);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(fragmentName);

        fragment = new DeliveryModFragmentV2();
        aq.id(R.id.title_text).text("배송일 변경");

        Log.e(TAG, "[DeliveryModFragment] Create : " + fragmentName);

        if (null != args) {
            fragment.setArguments(args);
        }
        ft.setCustomAnimations(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
        ft.replace(R.id.content_frame, fragment, fragmentName);
        ft.addToBackStack(fragmentName);
        ft.commitAllowingStateLoss();
        //ft.commit();
    }

    public boolean getTouchDisable() {
        Log.d(TAG, "[getTouchDisable]");
        return mIsTouchDisable;
    }

    public void setTouchDisable(boolean isTouchDisable) {
        Log.d(TAG, "[setTouchDisable]" + isTouchDisable);
        mIsTouchDisable = isTouchDisable;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        DUtil.Log(TAG, "[onBackPressed] Back Stack Entry Count : " + fm.getBackStackEntryCount());

        if (fm.getBackStackEntryCount() >= 1) {

            String tag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
            IFragment fragment = (IFragment) fm.findFragmentByTag(tag);
            if (false == fragment.onBackPressed()) {

                if (fm.getBackStackEntryCount() == 1) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
                    alertDialogBuilder.setCancelable(true).setMessage("배송일 변경신청이 완료되지 않았어요.\n중단하시겠어요?")
                            .setPositiveButton("네", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                } else {
                    setTouchDisable(true);
                    //clearCaptureView();

                    fm.popBackStack();

                }
            }
        } else {
            super.onBackPressed();
        }
        //finish();
        //super.onBackPressed();
    }


    public void modInfo() {
        RequestParams params = new RequestParams();
        try {
            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            params.put("orderId", orderInfo.getString("orderId"));
            params.put("addressSeq", orderInfo.getString("addressSeq"));
            params.put("deliveryRequestTime", deliveryReqTime);
            params.put("pickupRequestMessage", orderInfo.getString("pickupRequestMessage"));
            params.put("k", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkClient.post(Constants.ORDER_MOD, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_MOD, error.getMessage());


            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_MOD, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            Tracker t = ((Bootstrap)getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                            t.send(new HitBuilders.EventBuilder()
                                    .setCategory("주문")
                                    .setAction("배송시간변경")
                                    //.setLabel("Order")
                                    .build());
                            AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                            logger.logEvent("Modify Delivery time");
                            //JSONArray orderInfo = jsondata.getJSONArray("data");

                            Intent resultData = new Intent();
                            //resultData.putExtra("content", orderInfo.toString());
                            setResult(Activity.RESULT_OK, resultData);
                            finish();
                        }

                    } else {
                        Toast.makeText(mCtx, jsondata.getString("message"), Toast.LENGTH_SHORT);
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }
}
