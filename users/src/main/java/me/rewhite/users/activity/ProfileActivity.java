package me.rewhite.users.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.session.RewhiteSession;
import me.rewhite.users.task.ImageUploadModTask;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.MZValidator;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.StringUtil;
import me.rewhite.users.util.TimeUtil;

public class ProfileActivity extends BaseActivity {

    private final static String TAG = ProfileActivity.class.getSimpleName();
    public AQuery aq;
    public final Context mCtx = this;
    private TelephonyManager telephonyManager;

    ProgressDialog dialog;

    public String VARS_MOBILE_NUMBER = "";
    public String VARS_AUTH_NUMBER = "";

    String nickName;
    String imagePath;
    String loginType;
    String emailAddress;
    String fNumber;
    SharedPreferences preferences;

    private boolean isProfileImageModified = false;

    int authProcessStatus = 0; // Ready
    private static final int INPUT_EMAIL = 8;
    private static final int AUTH_PHONE = 7;
    private static final int REQ_CODE_PICK_IMAGE = 0;
    private static final String TEMP_PHOTO_FILE = "upload.jpg"; // 임시 저장파일

    ImageOptions options;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        aq = new AQuery(this);

        if(dialog == null){
            dialog = new ProgressDialog(this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(false);
            dialog.setInverseBackgroundForced(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setMessage(getString(R.string.loading_comment));
            dialog.show();
        }


        aq.id(R.id.text_box).clicked(this, "nicknameEditMode");
        aq.id(R.id.btn_text_edit).clicked(this, "nicknameEditMode");

        aq.id(R.id.btn_clear).clicked(this, "nicknameClear");
        aq.id(R.id.btn_camera).clicked(this, "addPictureAction");
        aq.id(R.id.btn_confirm).clicked(this, "saveAction");

        aq.id(R.id.gender_area).clicked(this, "selectGender");
        aq.id(R.id.birth_area).clicked(this, "selectBirth");

        aq.id(R.id.btn_logout).clicked(this, "logoutAction");

        aq.id(R.id.phone_area).clicked(this, "modPhone");
        aq.id(R.id.password_area).clicked(this, "modPassword");

        aq.id(R.id.btn_back).clicked(this, "backAction");
        aq.id(R.id.btn_withdraw).clicked(this, "withdrawAction");

        options = new ImageOptions();
        options.round = 100;
        options.ratio = 1.0f;
        options.memCache = true;
        options.targetWidth = 100;
        options.animation = AQuery.FADE_IN_NETWORK;

        //aq.id(R.id.top_info).background(R.mipmap.)

        initialize();
    }

    public void nicknameClear(View button) {
        aq.id(R.id.edit_nickname).text("");
    }

    public void withdrawAction(View button) {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, WithdrawActivity.class);
                mCtx.startActivity(intent);
                ((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                ((Activity) mCtx).finish();
            }
        }, 200);
    }

    public void backAction(View button) {
        finish();
    }

    private void initialize() {

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.USER_INFO, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.USER_INFO, error.getMessage());
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.USER_INFO, result);

                    JSONObject jsondata = new JSONObject(result);

                    if (dialog != null) {
                        dialog.dismiss();
                    }

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            JSONObject userInfo = jsondata.getJSONObject("data");
                            setDataView(userInfo);
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    private void setDataView(JSONObject userInfo) throws JSONException {

        String loginType = userInfo.getString("loginType");
        if ("FB".equals(loginType) || "KK".equals(loginType)) {
            aq.id(R.id.password_area).gone();
            aq.id(R.id.email_area).visible();
            aq.id(R.id.email_selector).clicked(this, "emailSelect");
        } else {
            aq.id(R.id.password_area).visible();
            aq.id(R.id.email_area).visible().enabled(false);
            aq.id(R.id.text_email_desc).gone();
            aq.id(R.id.email_selector_icon).gone();
        }
        String loginId = userInfo.getString("loginId");
        String userId = userInfo.getInt("userId") + "";
        nickName = userInfo.getString("userName");
        String shareCode = userInfo.getString("shareCode");
        String phoneno = userInfo.getString("phone");
        String email = userInfo.getString("email");
        String isEmailConfirm = userInfo.getString("isEmailConfirm");
        selectedBirth = userInfo.getString("birth");
        if (selectedBirth.length() > 1) {
            Date dt = TimeUtil.simpleDateFormat(selectedBirth);
            selectedBirth = TimeUtil.getDisplayDateFormat(dt);
            aq.id(R.id.tf_birth).text(TimeUtil.getSimpleDisplayDateFormat(dt));
        }
        String gender = userInfo.getString("gender");
        if ("M".equals(gender)) {
            selectedGenderIndex = 1;
            aq.id(R.id.tf_gender).text("남성");
        } else if ("F".equals(gender)) {
            selectedGenderIndex = 0;
            aq.id(R.id.tf_gender).text("여성");
        }
        String imageBigPath = userInfo.getString("imageBigPath");
        imagePath = userInfo.getString("imageThumbPath");
        String useAvailablePoint = userInfo.getString("useAvailablePoint");
        String totalEarnedPoint = userInfo.getString("totalEarnedPoint");
        String expectPoint = userInfo.getString("expectPoint");

        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_TYPE, loginType);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_ID, loginId);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.USER_ID, userId);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.USER_NAME, nickName);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.SHARE_CODE, shareCode);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.EMAIL, email);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.EMAIL_AUTH_CHECK, isEmailConfirm);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.PHONE_NO, phoneno);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.BIRTH, selectedBirth);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.GENDER, gender);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.PROFILE_IMAGE, imageBigPath);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.PROFILE_THUMB, imagePath);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.USE_AVAILABLE_POINT, useAvailablePoint);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.TOTAL_EARNED_POINT, totalEarnedPoint);
        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.EXPECT_POINT, expectPoint);

        SharedPreferencesUtility.setLocationData(userInfo.getJSONArray("geoInfo"));

        aq.id(R.id.tf_nickname).text(nickName).typeface(CommonUtility.getNanumBarunTypeface());
        if (!StringUtil.isNullOrEmpty(imagePath)) {
            //Bitmap placeholder = aq.getCachedImage(R.mipmap.kakao_default_profile_image);
            //aq.id(R.id.profile_image).image(imagePath, true, true, 0, 0, null, AQuery.FADE_IN, 1.0f / 1.0f);
            aq.id(R.id.profile_image).image(imagePath, options);
        }

        String phoneNumber = phoneno;
        Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        fNumber = MZValidator.validTelNumber(phoneNumber);
        aq.id(R.id.tf_phone_no).text(fNumber).typeface(CommonUtility.getNanumBarunTypeface());

        if (StringUtil.isNullOrEmpty(nickName)) {
            aq.id(R.id.text_box).gone();
            aq.id(R.id.edit_box).visible();
        }

        aq.id(R.id.tf_email).text(email).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.tf_location_count).text(userInfo.getJSONArray("geoInfo").length() + "");
    }

    public void nicknameEditMode(View button) {
        aq.id(R.id.edit_nickname).text(nickName);

        aq.id(R.id.text_box).gone();
        aq.id(R.id.edit_box).visible();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AUTH_PHONE:
                    String pno = data.getStringExtra("content");
                    aq.id(R.id.tf_phone_no).text(MZValidator.validTelNumber(pno));
                    fNumber = pno;
                    break;
                case INPUT_EMAIL:
                    String contents = data.getStringExtra("content");
                    aq.id(R.id.tf_email).text(contents);
                    emailAddress = contents;
                    break;
                case REQ_CODE_PICK_IMAGE:
                    if (resultCode == RESULT_OK) {
                        if (data != null) {

                            String rPath = null;
                            Uri _uri = data.getData();
                            Log.d("", "URI = " + _uri);
                            if (_uri != null && "file".equals(_uri.getScheme())) {
                                rPath = _uri.getPath();
                            } else {
                                File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                                File f = new File(path, TEMP_PHOTO_FILE);
                                rPath = f.getAbsolutePath();
                            }
                            Log.i("imageDATA PATH :::::", "Chosen path = " + rPath);

                            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                            File f = new File(path, TEMP_PHOTO_FILE);
                            String filePath = f.getAbsolutePath();

                            // Get dimensions of image first (takes very little time)
                            BitmapFactory.Options bfo = new BitmapFactory.Options();
                            bfo.inJustDecodeBounds = true;
                            bfo.inDither = false;
                            bfo.inPreferredConfig = Bitmap.Config.RGB_565;
                            BitmapFactory.decodeFile(rPath, bfo);

                            // Calculate sample size to keep image under maxFileSize
                            int maxFileSize = 2572864; // in bytes
                            int sampleSize = 1;
                            long fileSize = 2 * (bfo.outWidth / sampleSize) * (bfo.outHeight / sampleSize);
                            while (fileSize > maxFileSize) {
                                sampleSize++;
                                fileSize = 2 * (bfo.outWidth / sampleSize) * (bfo.outHeight / sampleSize);
                            }
                            // Decode image using calculated sample size
                            bfo.inSampleSize = sampleSize;
                            bfo.inJustDecodeBounds = false;
                            Bitmap bmpAvator = BitmapFactory.decodeFile(rPath, bfo);
            /* Rotation */
                            int degrees = GetExifOrientation(rPath);
                            // 회전한 이미지 취득
                            bmpAvator = GetRotatedBitmap(bmpAvator, degrees);
                            File fileCacheItem = new File(filePath);
                            OutputStream out = null;
                            try {
                                fileCacheItem.createNewFile();
                                out = new FileOutputStream(fileCacheItem);
                                bmpAvator.compress(Bitmap.CompressFormat.JPEG, 90, out);
                            } catch (Exception e) {
                                e.printStackTrace();
                            } finally {
                                try {
                                    out.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            HashMap<String, Object> reqParams = new HashMap<String, Object>();
                            new ImageUploadModTask(this, preferences).execute(reqParams);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public void saveAction(View button) {

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        if (StringUtil.isNullOrEmpty(aq.id(R.id.edit_nickname).getText().toString())) {
            params.put("userName", aq.id(R.id.tf_nickname).getText().toString());
        } else {
            params.put("userName", aq.id(R.id.edit_nickname).getText().toString());
        }

        params.put("email", aq.id(R.id.tf_email).getText().toString());
        params.put("phone", fNumber.replace("-", ""));
        params.put("birth", selectedBirth);
        params.put("gender", genderFinalItems[selectedGenderIndex]);

        if (!isProfileImageModified) {
            params.put("imageBigPath", imagePath);
            params.put("imageThumbPath", imagePath);
        }
        params.put("k", 1);
        NetworkClient.post(Constants.USERINFO_MOD, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.USERINFO_MOD, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.USERINFO_MOD, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        JSONObject userInfo = jsondata.getJSONObject("data");
                        setDataView(userInfo);

                        Toast.makeText(mCtx, "내 정보가 수정되었어요.", Toast.LENGTH_SHORT);

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {

                                Intent intent = new Intent();
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                intent.setClass(mCtx, MainActivity.class);
                                mCtx.startActivity(intent);
                                ((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                ((Activity) mCtx).finish();
                            }
                        }, 200);
                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    public void modPhone(View button) {
        Intent phoneAuthIntent = new Intent(this, PhoneAuthActivity.class);
        phoneAuthIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(phoneAuthIntent, AUTH_PHONE);
    }

    public void modPassword(View button) {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, PasswordModActivity.class);
                mCtx.startActivity(intent);
                ((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }, 200);
    }

    public void emailSelect(View button) {
        Intent phoneInputIntent = new Intent(this, EmailInputActivity.class);
        phoneInputIntent.putExtra("content", aq.id(R.id.tf_email).getText().toString());

        phoneInputIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(phoneInputIntent, INPUT_EMAIL);
    }

    final String genderItems[] = {"여성", "남성"};
    final String genderFinalItems[] = {"F", "M"};
    int selectedGenderIndex = 0;

    public void selectGender(View button) {
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle("성별 선택");
        ab.setSingleChoiceItems(genderItems, selectedGenderIndex, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedGenderIndex = which;
            }
        }).setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                aq.id(R.id.tf_gender).text(genderItems[selectedGenderIndex]);
            }
        }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        ab.show();
    }

    private int year;
    private int month;
    private int day;
    String selectedBirth;

    public void selectBirth(View button) {
        final Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        showDialog(DATE_DIALOG_ID);
    }

    static final int DATE_DIALOG_ID = 100;

    @Override
    protected Dialog onCreateDialog(int id) {

        switch (id) {
            case DATE_DIALOG_ID:
                // set date picker as current date
                return new DatePickerDialog(this, datePickerListener, 1987, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            Date birth = calendar.getTime();

            selectedBirth = TimeUtil.getDisplayDateFormat(birth);
            aq.id(R.id.tf_birth).text(TimeUtil.getSimpleDisplayDateFormat(birth));

            // set selected date into Date Picker

        }
    };


    public void logoutAction(View button) {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false).setTitle("로그아웃").setMessage("정말 로그아웃 하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        RewhiteSession.getCurrentSession().close();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        android.app.AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private static final String[] INITIAL_PERMS={
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final String[] STORAGE_PERMS={
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final int INITIAL_REQUEST=1337;
    private static final int STORAGE_REQUEST=INITIAL_REQUEST+3;

    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch(requestCode) {

            case STORAGE_REQUEST:
                if (canAccessPermission()) {
                    addIntentCall();
                }
                else {
                    Toast.makeText(this, "사진첩에 접근할수 있는 권한이 없습니다.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
    private boolean canAccessPermission() {
        return(hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
    }
    /*
    ADD PICTURE
     */
    // Image Upload
    public void addPictureAction(View button) {
        //MZTrace.debug("add", "addbutton");

        try
        {
            if ( Build.VERSION.SDK_INT >= 23){
                if (!canAccessPermission()) {
                    requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
                }else{
                    addIntentCall();
                }
            }else{
                addIntentCall();
            }

        }
        catch(NullPointerException ex)
        {
            addIntentCall();
        }
    }

    private void addIntentCall(){
        Display display = getWindowManager().getDefaultDisplay();
        int mOutputX = display.getWidth();

        Intent intent = new Intent(Intent.ACTION_PICK
                , android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*"); // 모든 이미지
        intent.putExtra("crop", "true"); // Crop기능 활성화
        intent.putExtra("scale", true);
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri()); // 임시파일 생성
        intent.putExtra("outputFormat", // 포맷방식
                Bitmap.CompressFormat.JPEG.toString());

        startActivityForResult(intent, REQ_CODE_PICK_IMAGE);
    }

    private int GetExifOrientation(String filepath) {
        int degree = 0;
        ExifInterface exif = null;

        try {
            exif = new ExifInterface(filepath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (exif != null) {
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            if (orientation != -1) {
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        degree = 90;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        degree = 180;
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        degree = 270;
                        break;
                }
            }
        }

        return degree;
    }

    private Bitmap GetRotatedBitmap(Bitmap bitmap, int degrees) {
        if (degrees != 0 && bitmap != null) {
            Matrix m = new Matrix();
            m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);

            try {
                Bitmap b2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);

                if (bitmap != b2) {
                    bitmap.recycle();
                    bitmap = b2;
                }
            } catch (OutOfMemoryError e) {
                // 메모리 부족에러시, 원본을 반환
            }
        }

        return bitmap;
    }

    public void uploadCompleted(String response) {

        try {
            JSONObject json = new JSONObject(response);
            Log.i("uploadCompleted", response.toString());

            if ("S0000".equals(json.getString("resultCode"))) {

                JSONObject images = json.getJSONObject("data");
                String imageBig = images.getString("imageBigPath");
                String imageThumb = images.getString("imageThumbPath");
                isProfileImageModified = true;

                //SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.PROFILE_THUMB, url);

                Snackbar.make(aq.getView(), "업로드가 완료되었습니다.", Snackbar.LENGTH_SHORT);
                imageInitialize(imageThumb);

            } else {
                DUtil.alertShow(this, "이미지 업로드가 실패했습니다.");
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void imageInitialize(final String _url) {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                aq.id(R.id.profile_image).image(_url, options);
                /*
                aq.id(R.id.profile_image).image(_url, true, true, 200, 0, new BitmapAjaxCallback() {

                    @Override
                    public void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                        Log.i("uploadCompleted BitmapAjaxCallback", url + "\n" + bm.describeContents());
                        iv.setImageBitmap(bm);
                    }

                });*/
            }
        }, 500);
    }

    void createExternalStoragePublicPicture() {

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);

        try {
            // Make sure the Pictures directory exists.
            path.mkdirs();

            InputStream is = getAssets().open("kakao_default_profile_image.png");// getResources().openRawResource(R.mipmap.kakao_default_profile_image);
            OutputStream os = new FileOutputStream(file);
            byte[] data = new byte[is.available()];
            is.read(data);
            os.write(data);
            is.close();
            os.close();

            MediaScannerConnection.scanFile(this, new String[]{file.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });
        } catch (IOException e) {
            // Unable to create file, likely because external storage is
            // not currently mounted.
            Log.w("ExternalStorage", "Error writing " + file, e);
        }
    }

    void deleteExternalStoragePublicPicture() {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        file.delete();
    }

    boolean hasExternalStoragePublicPicture() {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, TEMP_PHOTO_FILE);
        return file.exists();
    }

    /**
     * 임시 저장 파일의 경로를 반환
     */
    private Uri getTempUri() {
        return Uri.fromFile(getTempFile());
    }

    /**
     * 외장메모리에 임시 이미지 파일을 생성하여 그 파일의 경로를 반환
     */
    private File getTempFile() {
        if (hasExternalStoragePublicPicture()) {
            deleteExternalStoragePublicPicture();
        }
        createExternalStoragePublicPicture();

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File f = new File(path, TEMP_PHOTO_FILE);

        try {
            f.createNewFile(); // 외장메모리에 temp.jpg 파일 생성
        } catch (IOException e) {
        }

        return f;
    }
}
