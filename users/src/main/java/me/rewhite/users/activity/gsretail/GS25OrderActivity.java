package me.rewhite.users.activity.gsretail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;

import com.androidquery.AQuery;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.xgc1986.parallaxPagerTransformer.ParallaxPagerTransformer;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.adapter.GSParallaxAdapter;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.fragment.GS25V2MainParallaxFragment;

public class GS25OrderActivity extends BaseActivity {

    private static final String TAG = "GS25OrderActivity";
    private Context mContext;
    private ViewPager pager;
    //private TopImageAdapter mPagerAdapter;
    GSParallaxAdapter mAdapter;
    private AQuery aq;
    ImageButton button = null;
    View decorView;

    private final int mPageCount = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gs25_order);

        aq = new AQuery(this);

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        mContext = this;
        //mPagerAdapter = new TopImageAdapter();


//        String userId = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_ID);
//
//        try {
//            Log.i("barcode pixels", aq.id(R.id.barcode_container).getView().getWidth() + " / " + aq.id(R.id.barcode_container).getView().getHeight());
//            aq.id(R.id.barcode_text).text(userId);
//            aq.id(R.id.barcode_image).image(ImageUtil.createBarcodeBitmap(userId, 1400, 200));
//        } catch (WriterException e) {
//            e.printStackTrace();
//        }
//
//        String nickname = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME);
//        aq.id(R.id.text_nickname).text(nickname);

        pager = (ViewPager) findViewById(R.id.view_pager);
        ParallaxPagerTransformer pt = new ParallaxPagerTransformer((R.id.image));
        ParallaxPagerTransformer ptsub = new ParallaxPagerTransformer((R.id.subimage));
        pager.setPageTransformer(false, ptsub);
        pager.setPageTransformer(true, pt);


        mAdapter = new GSParallaxAdapter(getSupportFragmentManager());
        mAdapter.setPager(pager);

        Bundle image01 = new Bundle();
        image01.putInt("image", R.mipmap.gs25_main_image_01);
        image01.putInt("subimage", R.mipmap.gs25_main_sub_image_01);
        image01.putInt("position", 0);
        GS25V2MainParallaxFragment image01Frag = new GS25V2MainParallaxFragment();
        image01Frag.setInstance(this);
        image01Frag.setArguments(image01);

        Bundle image02 = new Bundle();
        image02.putInt("image", R.mipmap.gs25_main_image_02);
        image02.putInt("subimage", R.mipmap.gs25_main_sub_image_02);
        image02.putInt("position", 1);
        GS25V2MainParallaxFragment image02Frag = new GS25V2MainParallaxFragment();
        image02Frag.setInstance(this);
        image02Frag.setArguments(image02);

        Bundle image03 = new Bundle();
        image03.putInt("image", R.mipmap.gs25_main_image_03);
        image03.putInt("subimage", R.mipmap.gs25_main_sub_image_03);
        image03.putInt("position", 2);
        GS25V2MainParallaxFragment image03Frag = new GS25V2MainParallaxFragment();
        image03Frag.setInstance(this);
        image03Frag.setArguments(image03);

        Bundle image04 = new Bundle();
        image04.putInt("image", R.mipmap.gs25_main_image_04);
        image04.putInt("subimage", R.mipmap.gs25_main_sub_image_04);
        image04.putInt("position", 3);
        GS25V2MainParallaxFragment image04Frag = new GS25V2MainParallaxFragment();
        image04Frag.setInstance(this);
        image04Frag.setArguments(image04);

        Bundle image05 = new Bundle();
        image05.putInt("image", R.mipmap.gs25_main_image_05);
        image05.putInt("subimage", R.mipmap.gs25_main_sub_image_05);
        image05.putInt("position", 4);
        GS25V2MainParallaxFragment image05Frag = new GS25V2MainParallaxFragment();
        image05Frag.setInstance(this);
        image05Frag.setArguments(image05);

        Bundle image06 = new Bundle();
        image06.putInt("image", R.mipmap.gs25_main_image_06);
        image06.putInt("subimage", R.mipmap.gs25_main_sub_image_06);
        image06.putInt("position", 5);
        GS25V2MainParallaxFragment image06Frag = new GS25V2MainParallaxFragment();
        image06Frag.setInstance(this);
        image06Frag.setArguments(image06);

        Bundle image07 = new Bundle();
        image07.putInt("image", R.mipmap.gs25_main_image_07);
        image07.putInt("subimage", R.mipmap.gs25_main_sub_image_07);
        image07.putInt("position", 6);
        GS25V2MainParallaxFragment image07Frag = new GS25V2MainParallaxFragment();
        image07Frag.setInstance(this);
        image07Frag.setArguments(image07);


        mAdapter.add(image01Frag);
        mAdapter.add(image02Frag);
        mAdapter.add(image03Frag);
        mAdapter.add(image04Frag);
        mAdapter.add(image05Frag);
        mAdapter.add(image06Frag);
        mAdapter.add(image07Frag);
        pager.setAdapter(mAdapter);

        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().show();
        }

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        aq.id(R.id.indicator_image).image(R.mipmap.gs_indicator_01).visible();
                        break;
                    case 1:
                        aq.id(R.id.indicator_image).image(R.mipmap.gs_indicator_02).visible();
                        break;
                    case 2:
                        aq.id(R.id.indicator_image).image(R.mipmap.gs_indicator_03).visible();
                        break;
                    case 3:
                        aq.id(R.id.indicator_image).image(R.mipmap.gs_indicator_04).visible();
                        break;
                    case 4:
                        aq.id(R.id.indicator_image).image(R.mipmap.gs_indicator_05).visible();
                        break;
                    case 5:
                        aq.id(R.id.indicator_image).image(R.mipmap.gs_indicator_06).visible();
                        break;
                    case 6:
                        aq.id(R.id.indicator_image).image(R.mipmap.gs_indicator_07).visible();
                        break;


                    default:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setMaterialRippleLayout((View)findViewById(R.id.btn_start));

        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
        aq.id(R.id.btn_start).clicked(this, "gsStartOrderAction");
        aq.id(R.id.btn_store_list).clicked(this, "gsStoreInfoAction");
    }
    public void closeClicked(View button) {
        finish();
    }

    private void setMaterialRippleLayout(View _target){
        MaterialRippleLayout.on(_target)
                .rippleColor(Color.parseColor("#FFffff"))
                .rippleAlpha(0.2f)
                .rippleHover(true)
                .rippleOverlay(true)
                .create();
    }

    public void infoMove(int pos){
        if(pos == 2){
            Intent inputIntent = new Intent();
            inputIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            inputIntent.setClass(mContext, GS25InfoActivity.class);
            inputIntent.putExtra("pos", 1);
            startActivity(inputIntent);
        }else if(pos == 5){
            Intent inputIntent = new Intent();
            inputIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            inputIntent.setClass(mContext, GS25InfoActivity.class);
            inputIntent.putExtra("pos", 1);
            startActivity(inputIntent);
        }else if(pos == 6){
            Intent inputIntent = new Intent();
            inputIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            inputIntent.setClass(mContext, GS25InfoActivity.class);
            inputIntent.putExtra("pos", 2);
            startActivity(inputIntent);
        }

    }

    public void gsStoreInfoAction(View button){
        Intent inputIntent = new Intent();
        inputIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        inputIntent.setClass(mContext, GS25InfoActivity.class);
        startActivity(inputIntent);

    }

    public void gsStartOrderAction(View button){
//        IntentIntegrator integrator = new IntentIntegrator(this);
//        integrator.initiateScan();
        Intent inputIntent = new Intent();
        inputIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        // GS 미연동 주문 시작
        //inputIntent.setClass(mContext, GS25V2OrderStartActivity.class);
        // GS 연동주문 시작
        inputIntent.setClass(mContext, GS25V3OrderAgreeActivity.class);
        startActivity(inputIntent);
    }

//    private class TopImageAdapter extends PagerAdapter {
//        @Override
//        public int getCount() {
//            return mPageCount;
//        }
//
//        @Override
//        public void destroyItem(View pager, int position, Object view) {
//            ((ViewPager) pager).removeView((View) view);
//        }
//
//        @Override
//        public Object instantiateItem(View pager, int position) {
//            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//            RelativeLayout view = null;
//            ImageView imageView = null;
//            Button button = null;
//
//            switch (position) {
//                case 0:
//                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough, null);
//                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
//                    imageView.setImageResource(R.mipmap.gs25_info_01);
//                    ((ViewPager) pager).addView(view, 0);
//                    break;
//
//                case 1:
//                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough, null);
//                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
//                    imageView.setImageResource(R.mipmap.gs25_info_01);
//                    ((ViewPager) pager).addView(view, 0);
//                    break;
//
//                case 2:
//                    view = (RelativeLayout) inflater.inflate(R.layout.layout_walkthrough, null);
//                    imageView = (ImageView) view.findViewById(R.id.layout_walkthrough_imageview);
//                    imageView.setImageResource(R.mipmap.gs25_info_01);
//                    ((ViewPager) pager).addView(view, 0);
//                    break;
//
//                default:
//                    break;
//            }
//
//            return view;
//        }
//
//        @Override
//        public boolean isViewFromObject(View view, Object obj) {
//            return view == obj;
//        }
//    }
}
