package me.rewhite.users.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ScrollView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.facebook.appevents.AppEventsLogger;
import com.google.ads.conversiontracking.AdWordsConversionReporter;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Currency;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.RewhitePaymentType;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.MZValidator;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.StringUtil;
import me.rewhite.users.util.TimeUtil;
import me.rewhite.users.util.ToastUtility;


public class OrderDetailActivity extends BaseActivity {

    public final Context mCtx = this;
    AQuery aq;
    public JSONObject orderInfo;
    public String orderId;
    ProgressDialog mProgressDialog;

    boolean isMobilePayment = false;
    String isFacePaymentType = "";
    ScrollView scrollView;

    String pickupItems;
    String pickupRepairItems;
    String pickupItemsV2;
    JSONArray photosArray;

    int deliveryPay = 2000;

    private static final int PAYMENT_OK = 1;
    private static final int MOD_DELIVERY_OK = 2;
    private static final int VIEW_RECEIPT = 3;
    private static final int ORDER_OPERATION_CANCEL = 4;
    private static final int ORDER_REVIEWED = 5;

    String orderType;
    String orderStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("주문상태를 조회중입니다.");
        mProgressDialog.setCancelable(true);
        mProgressDialog.show();

        Intent intent = getIntent();
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();
            orderId = uri.getQueryParameter("orderId");
        }else{
            if (intent != null) {
                orderId = intent.getStringExtra("orderId");
            }
        }

        Log.e("orderId", orderId + "");
        try {
            getDataRefresh(orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void closeClicked(View button) {
        finish();
    }

    private JSONArray getPickupItem(JSONArray originData){
        JSONArray result = new JSONArray();
        try {
            for(int i = 0; i < originData.length(); i++){
                if("G".equals(originData.getJSONObject(i).getString("pickupType"))){
                    result.put(originData.getJSONObject(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
    private JSONArray getRepairItem(JSONArray originData){
        JSONArray result = new JSONArray();
        try {
            for(int i = 0; i < originData.length(); i++){
                if("R".equals(originData.getJSONObject(i).getString("pickupType"))){
                    result.put(originData.getJSONObject(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void initialize() {
        try {
            orderStatus = orderInfo.getString("orderStatus");
            orderType = orderInfo.getString("orderType");
            setStatus(orderStatus);
            aq.id(R.id.text_location).text(orderInfo.getString("address1") + " " + orderInfo.getString("address2")).typeface(CommonUtility.getNanumBarunTypeface());
            String message = ("".equals(orderInfo.getString("pickupRequestMessage")) ? "요청사항 없음" : orderInfo.getString("pickupRequestMessage"));
            aq.id(R.id.text_memo).text(message).typeface(CommonUtility.getNanumBarunTypeface());
            photosArray = new JSONArray();

            if (orderInfo.getJSONArray("pickupItems").length() > 0) {
                //[{"orderId":10050,"itemId":54,"itemQuantity":1,"itemPrice":15000,"itemTitle":"어깨","isOption":"N","pickupType":"R"}]
                pickupItems = getPickupItem(orderInfo.getJSONArray("pickupItems")).toString();
                pickupRepairItems = getRepairItem(orderInfo.getJSONArray("pickupItems")).toString();
                JSONArray picks = new JSONArray(pickupItems);
                JSONArray repairs = new JSONArray(pickupRepairItems);
                int count = 0;
                for (int i = 0; i < picks.length(); i++) {
                    int q = picks.getJSONObject(i).getInt("itemQuantity");
                    count += q;
                }
                int count2 = 0;
                for (int i = 0; i < repairs.length(); i++) {
                    int q = repairs.getJSONObject(i).getInt("itemQuantity");
                    count2 += q;
                }
                if(picks.length() > 0){
                    String orderRequest = picks.getJSONObject(0).getString("itemTitle") + " 등 수량 " + count + "개";
                    aq.id(R.id.text_order_element).text(orderRequest).typeface(CommonUtility.getNanumBarunTypeface());
                    aq.id(R.id.order_req_area).visible();
                }else{
                    aq.id(R.id.order_req_area).gone();
                }

                if(repairs.length() > 0){
                    String orderRepairRequest = repairs.getJSONObject(0).getString("itemTitle") + " 등 " + count2 + "건";
                    aq.id(R.id.text_order_repair_element).text(orderRepairRequest).typeface(CommonUtility.getNanumBarunTypeface());
                    aq.id(R.id.order_req_repair_area).visible();
                }else{
                    aq.id(R.id.order_req_repair_area).gone();
                }

                if(picks.length() > 0 && repairs.length() > 0){
                    aq.id(R.id.btn_receipt).gone();
                    aq.id(R.id.btn_receipt2).visible();
                }

            }else if (orderInfo.getJSONArray("pickupItemsV2").length() > 0) {
                pickupItemsV2 = orderInfo.getJSONArray("pickupItemsV2").toString();
                String orderRequest = orderInfo.getJSONArray("pickupItemsV2").getJSONObject(0).getString("itemTitle") + " 등 수량 " + orderInfo.getJSONArray("pickupItemsV2").length() + "개";
                aq.id(R.id.text_order_element).text(orderRequest).typeface(CommonUtility.getNanumBarunTypeface());
                aq.id(R.id.order_req_area).visible();
                aq.id(R.id.order_req_repair_area).gone();

                for(int i = 0 ; i < orderInfo.getJSONArray("pickupItemsV2").length(); i++){
                    for(int j = 0; j < orderInfo.getJSONArray("pickupItemsV2").getJSONObject(i).getJSONArray("photo").length(); j++){
                        photosArray.put(orderInfo.getJSONArray("pickupItemsV2").getJSONObject(i).getJSONArray("photo").getJSONObject(j));
                    }
                }
                if(photosArray.length() > 0){
                    aq.id(R.id.photo_layout).visible();

                    ImageOptions op = new ImageOptions();
                    op.memCache = true;
                    op.fileCache = true;
                    op.round = 30;
                    op.ratio = 1.f;

                    if(photosArray.length() > 0){
                        aq.id(R.id.photo_01).image(photosArray.getJSONObject(0).getString("fileUrl"), op).visible().clicked(this, "showPhotos").tag(0);
                        if(photosArray.length() > 1){
                            aq.id(R.id.photo_02).image(photosArray.getJSONObject(1).getString("fileUrl"), op).visible().clicked(this, "showPhotos").tag(1);
                            if(photosArray.length() > 2){
                                aq.id(R.id.photo_03).image(photosArray.getJSONObject(2).getString("fileUrl"), op).visible().clicked(this, "showPhotos").tag(2);
                                if(photosArray.length() > 3){
                                    aq.id(R.id.photo_04).image(photosArray.getJSONObject(3).getString("fileUrl"), op).visible().clicked(this, "showPhotos").tag(3);
                                    if(photosArray.length() > 4){
                                        aq.id(R.id.photo_ext).visible().clicked(this, "showPhotos").tag(4);;
                                        aq.id(R.id.photo_ext_count).text("+"+(photosArray.length()-4));
                                    }else{
                                        aq.id(R.id.photo_ext).gone();
                                    }
                                }
                            }
                        }
                    }

                }else{
                    aq.id(R.id.photo_layout).gone();
                }

            } else{
                int pickCount = orderInfo.getInt("pickupQuantity");
                if(pickCount > 0){
                    aq.id(R.id.text_order_element).text("수량 "+pickCount+"개 (세탁소에 도착후 상세품목 확인이 가능합니다)").typeface(CommonUtility.getNanumBarunTypeface());
                }else{
                    aq.id(R.id.text_order_element).text("수거완료 후에 표시됩니다").typeface(CommonUtility.getNanumBarunTypeface());
                }

                aq.id(R.id.order_req_repair_area).gone();
            }

            String storeName = orderInfo.getString("storeName");
            String storeDisplayName = orderInfo.getString("storeDisplayName");
            if(storeName.equals(storeDisplayName)){
                aq.id(R.id.btn_storeinfo).image(R.mipmap.order_detail_storeinfo).enabled(true).clicked(this, "storeInfoAction");
                isStoreSecure = false;
            }else{
                aq.id(R.id.btn_storeinfo).image(R.mipmap.order_detail_storeinfo).enabled(true).clicked(this, "storeInfoAction");
                isStoreSecure = true;
            }

            aq.id(R.id.text_orderid).text("주문번호 : " + orderId).typeface(CommonUtility.getNanumBarunTypeface()).gone();
            //order_detail_storeinfo_no
            aq.id(R.id.text_storename).text(storeDisplayName).typeface(CommonUtility.getNanumBarunTypeface());
            //aq.id(R.id.text_storelocation).text(orderInfo.getString("storeAddress1")).typeface(CommonUtility.getNanumBarunTypeface());

            aq.id(R.id.btn_change).clicked(this, "deliveryChange");
            aq.id(R.id.btn_cancel).clicked(this, "orderCancel");
            aq.id(R.id.btn_receipt).clicked(this, "showReceipt");
            aq.id(R.id.btn_receipt2).clicked(this, "showReceipt");

            aq.id(R.id.btn_review).clicked(this, "reviewClicked");

            long pTime = orderInfo.getLong("pickupRequestTimeApp");
            long dTime = orderInfo.getLong("deliveryRequestTimeApp");
            String storeDeliveryTime = orderInfo.getString("storeDeliveryTime");
            String pStr = TimeUtil.getDateByMDSH_onlyTime(pTime, storeDeliveryTime.split("\\|")[0]);
            String dStr = TimeUtil.getDateByMDSH_onlyTime(dTime, storeDeliveryTime.split("\\|")[0]);

            aq.id(R.id.text_pickup_date).text(TimeUtil.convertTimestampToStringDate2(pTime)).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.text_pickup_time).text(pStr).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.text_delivery_date).text(TimeUtil.convertTimestampToStringDate2(dTime)).typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.text_delivery_time).text(dStr).typeface(CommonUtility.getNanumBarunTypeface());

            // PAYMENT
            aq.id(R.id.radio_01).clicked(this, "radioClicked").tag(1);
            aq.id(R.id.radio_02).clicked(this, "radioClicked").tag(2);

            if("2".equals(orderType)){
                aq.id(R.id.btn_pay_direct).gone();
            }else{
                aq.id(R.id.btn_pay_card).clicked(this, "facePayClicked").tag(1);
                aq.id(R.id.btn_pay_cash).clicked(this, "facePayClicked").tag(2);
            }

            aq.id(R.id.btn_next).clicked(this, "paymentAction");

            aq.id(R.id.btn_parcel).clicked(this, "parcelStatusClicked");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (mProgressDialog != null){
            if(mProgressDialog.isShowing()){
                mProgressDialog.dismiss();
            }
        }
    }

    public void showPhotos(View button){
        int pos = (int)button.getTag();

        Intent intent = new Intent(this, PhotoSlideShow.class);
        intent.putExtra("source", photosArray.toString());
        intent.putExtra("position", pos);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        startActivity(intent);
    }

    public void parcelStatusClicked(View button){
        // 상태값에 따라 IN/OUT 운송장번호 전달 Webview
        // https://www.rewhite.me/customer/deliveryredirect?orderId=orderId&s=I

        int orderStatusInt = Integer.parseInt(orderStatus);
        String parcelMode = "I";
        if(orderStatusInt < 11){
            parcelMode = "I";
        }else if(orderStatusInt > 20){
            parcelMode = "O";
        }


        String url = Constants.WWW_HOST + "/customer/deliveryredirect?orderId="+orderId+"&s="+parcelMode;

        Intent intent = new Intent(this, HtmlActivity.class);
        intent.putExtra("TERMS_URI", url);
        intent.putExtra("TITLE_NAME", "택배 배송 조회");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    boolean isStoreSecure = false;

    public void call(String telNo) {
        Tracker t = ((Bootstrap)getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("기타")
                .setAction("세탁소전화")
                //.setLabel("Order")
                .build());
        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
        logger.logEvent("Call Store");

//        RequestParams params = new RequestParams();
//        try {
//            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
//            params.put("callerMdn", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.PHONE_NO));
//
//            if(!orderInfo.getString("storeTelephone").equals(telNo)){
//                params.put("calleeMdn", orderInfo.getString("storeTelephone"));
//                params.put("originCallerNumber", telNo);
//            }else{
//                params.put("calleeMdn", telNo);
//            }
//            params.put("title", orderStatus + "title");
//            params.put("text", orderStatus + " text");
//            params.put("appUrl", "rewhiteMeUser://order?orderId="+orderId);
//            params.put("k", 1);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        NetworkClient.post(Constants.SEND_TPHONE, params, new AsyncHttpResponseHandler() {
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
//                // TODO Auto-generated method stub
//                DUtil.Log(Constants.SEND_TPHONE, error.getMessage());
//            }
//
//            @Override
//            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
//                String result;
//                try {
//                    result = new String(data, "UTF-8");
//                    DUtil.Log(Constants.SEND_TPHONE, result);
//
//                    JSONObject jsondata = new JSONObject(result);
//
//                    if ("S0000".equals(jsondata.getString("resultCode"))) {
//                        if (jsondata.isNull("data")) {
//                            // 주문내역이 없습니다
//                        } else {
//
//                        }
//
//                    } else {
//
//                    }
//                } catch (UnsupportedEncodingException e1) {
//                    // TODO Auto-generated catch block
//                    e1.printStackTrace();
//                } catch (JSONException e1) {
//                    // TODO Auto-generated catch block
//                    e1.printStackTrace();
//                }
//
//            }
//        });

        telNo = telNo.replace("-","");

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        //callIntent.setAction("android.intent.action.DIAL");
        callIntent.setData(Uri.parse("tel:"+telNo));
        if(callIntent.resolveActivity(getPackageManager()) != null){
            startActivity(callIntent);
        }
    }

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.CALL_PHONE
    };

    private static final int INITIAL_REQUEST = 1337;


    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }
    private boolean canAccessCall() {
        return(hasPermission(Manifest.permission.CALL_PHONE));
    }
    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case INITIAL_REQUEST:
                if (canAccessCall()) {

                }
                else {
                    Toast.makeText(this, "전화걸기 권한을 거절하셔서 통화연결이 불가합니다.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    public void storeInfoAction(View button){

        try {
            final String telNo = orderInfo.getString("storeVirtualPhone").replace("-","");
            String storeDisplayName = orderInfo.getString("storeDisplayName");

            if (Build.VERSION.SDK_INT >= 23) {
                if (!canAccessCall()) {
                    requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    alertDialogBuilder.setCancelable(true)
                            .setMessage(storeDisplayName+" 와 전화통화를 원하십니까?")
                            .setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                                    //callIntent.setAction("android.intent.action.DIAL");

                                    callIntent.setData(Uri.parse("tel:" + telNo));
                                    if(callIntent.resolveActivity(getPackageManager()) != null){
                                        startActivity(callIntent);
                                    }
                                }})
                            .setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            } else {
                call(telNo);
            }
        }catch(NullPointerException|JSONException e){
            e.printStackTrace();
        }

//        if(isStoreSecure){
//            try {
//                final String telNo = orderInfo.getString("storeVirtualPhone");
//                String storeDisplayName = orderInfo.getString("storeDisplayName");
//
//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//                alertDialogBuilder.setCancelable(true).setMessage(storeDisplayName + "에 전화통화를 원하십니까?")
//                        .setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
//                                t.send(new HitBuilders.EventBuilder()
//                                        .setCategory("USER")
//                                        .setAction("세탁소에 전화걸기")
//                                        .setLabel("Order")
//                                        .build());
//
//                                call(telNo);
//                            }
//                        }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
//                AlertDialog alertDialog = alertDialogBuilder.create();
//                alertDialog.show();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//        }else{
//            Intent intent = new Intent(this, TermsDetailView.class);
//            intent.putExtra("TERMS_TITLE", "상점정보");
//            try {
//                intent.putExtra("TERMS_URI", Constants.STORE_HOST + orderInfo.getString("storeId")+"?app=Y");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
//                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//        }

    }

    public void radioClicked(View button) {
        int tag = (int) button.getTag();

        isFacePaymentType = "";
        aq.id(R.id.pay_03).visible();

        switch (tag) {
            case 1:
                isMobilePayment = true;

                aq.id(R.id.pay_02).gone();
                aq.id(R.id.direct_select_area).gone();
                break;
            case 2:
                isMobilePayment = false;

                if(isRemoteCardEnabled){
                    aq.id(R.id.pay_02).visible();
                    aq.id(R.id.direct_select_area).visible();
                }else{
                    aq.id(R.id.pay_02).visible();
                    aq.id(R.id.direct_select_area).gone();
                    isFacePaymentType = RewhitePaymentType.FACE_CASH;
                    aq.id(R.id.btn_pay_cash_back).image(R.mipmap.paymethod_checked);
                    aq.id(R.id.btn_pay_card_back).gone();
                }
                break;
            default:
                break;
        }

        if (isMobilePayment) {
            aq.id(R.id.radio_01).image(R.mipmap.radio_on);
            aq.id(R.id.radio_02).image(R.mipmap.radio_off);
        } else {
            aq.id(R.id.radio_01).image(R.mipmap.radio_off);
            aq.id(R.id.radio_02).image(R.mipmap.radio_on);

            aq.id(R.id.btn_pay_card_back).image(R.mipmap.paymethod_normal);
            aq.id(R.id.btn_pay_cash_back).image(R.mipmap.paymethod_normal);

        }

        scrollToEnd();

    }


    public void scrollToEnd(){
        scrollView = (ScrollView)findViewById(R.id.scrollView);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }

        });

    }


    public void facePayClicked(View button) {
        int tag = (int) button.getTag();
        switch (tag) {
            case 1:
                isFacePaymentType = RewhitePaymentType.FACE_CARD;
                aq.id(R.id.btn_pay_card_back).image(R.mipmap.paymethod_checked);
                aq.id(R.id.btn_pay_cash_back).image(R.mipmap.paymethod_normal);
                break;
            case 2:
                isFacePaymentType = RewhitePaymentType.FACE_CASH;
                aq.id(R.id.btn_pay_card_back).image(R.mipmap.paymethod_normal);
                aq.id(R.id.btn_pay_cash_back).image(R.mipmap.paymethod_checked);
                break;
            default:
                break;
        }
    }

    public void paymentAction(View button) {
        if (isMobilePayment) {
            // PG결제로 이동
            orderPaymentPG();
        } else {
            if (!StringUtil.isNullOrEmpty(isFacePaymentType)) {
                orderPaymentFacetoFace();
            } else {
                // 현장결제 방식을 선택해주세요
                DUtil.alertShow(this, "현장결제 방식을 선택해주세요.");
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PAYMENT_OK:
                    try {
                        if(orderInfo == null || orderInfo.isNull("orderId")){
                            DUtil.alertShow(this, "죄송합니다. 화면조회중에 오류가 발생했습니다. 오류가 계속되면 고객센터로 문의부탁드립니다.");
                        }else{
                            String orderId = orderInfo.getString("orderId");
                            String result = data.getStringExtra("RESULT");
                            if("Y".equals(result)){
                                Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                                t.send(new HitBuilders.EventBuilder()
                                        .setCategory("USER")
                                        .setAction("결제성공")
                                        .setLabel("Payment")
                                        .build());
                                int price = Integer.parseInt(orderInfo.getString("deliveryPrice"));
                                if(price < 20000){
                                    price = price + deliveryPay;
                                }
                                AppEventsLogger logger = AppEventsLogger.newLogger(this);
                                logger.logPurchase(BigDecimal.valueOf(price), Currency.getInstance("KRW"));

                                Toast.makeText(this, "성공적으로 결제되었습니다.", Toast.LENGTH_LONG).show();

                                AdWordsConversionReporter.reportWithConversionId(this.getApplicationContext(),
                                        "939419543", "HK5tCMqTrWsQl8_5vwM", "0.00", true);

                                getDataRefresh(orderId);
                            }else if("N".equals(result)){
                                Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                                t.send(new HitBuilders.EventBuilder()
                                        .setCategory("USER")
                                        .setAction("결제실패")
                                        .setLabel("Payment")
                                        .build());

                                Toast.makeText(this, "결제에 실패했습니다.", Toast.LENGTH_LONG).show();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case MOD_DELIVERY_OK:
                    Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("USER")
                            .setAction("배송요청일 변경")
                            .setLabel("Order")
                            .build());

                    Toast.makeText(this, "배송요청정보가 변경되었습니다.", Toast.LENGTH_LONG).show();
                    try {
                        String orderId = orderInfo.getString("orderId");
                        getDataRefresh(orderId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case ORDER_OPERATION_CANCEL:
                    Tracker t1 = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                    t1.send(new HitBuilders.EventBuilder()
                            .setCategory("USER")
                            .setAction("주문취소")
                            .setLabel("Order")
                            .build());

                    ToastUtility.show(mCtx, "주문이 취소되었습니다", Toast.LENGTH_SHORT);

                    Intent resultData = new Intent();
                    //resultData.putExtra("content", orderInfo.toString());
                    setResult(Activity.RESULT_OK, resultData);
                    finish();

                case ORDER_REVIEWED:
                    Tracker t2 = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                    t2.send(new HitBuilders.EventBuilder()
                            .setCategory("USER")
                            .setAction("주문평가")
                            .setLabel("Order")
                            .build());
                    ToastUtility.show(mCtx, "정상적으로 평가되었습니다.", Toast.LENGTH_SHORT);
                    aq.id(R.id.review_before).gone();
                    aq.id(R.id.review_after).visible();
                    try {
                        String orderId = orderInfo.getString("orderId");
                        getDataRefresh(orderId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String result = data.getStringExtra("RESULT");
                    if("Y".equals(result)){
                        Intent reviewFacebookIntent = new Intent(mCtx, ReviewFacebookActivity.class);
                        startActivity(reviewFacebookIntent);
                    }else{

                    }


                    break;
                default:
                    break;
            }
        }else if(resultCode == RESULT_CANCELED){
            switch (requestCode) {
                case PAYMENT_OK:
                    if (Build.VERSION.SDK_INT == 19) {

                    }else{
                        Toast.makeText(this, "사용자의 요청으로 결제가 취소되었습니다.", Toast.LENGTH_LONG).show();

                        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                        t.send(new HitBuilders.EventBuilder()
                                .setCategory("USER")
                                .setAction("주문취소")
                                .setLabel("Payment")
                                .build());
                    }

                    break;
                default:

                    break;
            }
        }
    }

    private void orderCallRequest(final String pgReqCode) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent payBridgeIntent = new Intent(mCtx, PaymentBridgeActivity.class);
                payBridgeIntent.setAction(Intent.ACTION_GET_CONTENT);
                payBridgeIntent.putExtra("PAM_ID", pgReqCode);
                startActivityForResult(payBridgeIntent, PAYMENT_OK);
            }
        }, 250);

    }

    private void orderPaymentPG() {
        RequestParams params = new RequestParams();
        try {
            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            params.put("orderId", orderInfo.getString("orderId"));
            params.put("k", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkClient.post(Constants.PAYMENT_PRECODE_REQ, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.PAYMENT_PRECODE_REQ, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.PAYMENT_PRECODE_REQ, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            String pgReqCode = jsondata.getString("data");
                            orderCallRequest(pgReqCode);
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    private void orderPaymentFacetoFace() {

        if("2".equals(orderType)){
            DUtil.alertShow(this, "택배접수는 모바일결제만 선택가능합니다.");
            return;
        }

        RequestParams params = new RequestParams();
        try {
            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            params.put("orderId", orderInfo.getString("orderId"));
            params.put("paymentType", isFacePaymentType);
            params.put("k", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("orderPaymentFacetoFace", params.toString());

        //SetOrderPaymentDirect
        NetworkClient.post(Constants.PAYMENT_FACE, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.PAYMENT_FACE, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.PAYMENT_FACE, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            AdWordsConversionReporter.reportWithConversionId(getApplicationContext(),
                                    "939419543", "HK5tCMqTrWsQl8_5vwM", "0.00", true);
                            AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                            int price = Integer.parseInt(orderInfo.getString("deliveryPrice"));
                            if(price < 20000){
                                price = price + deliveryPay;
                            }
                            logger.logPurchase(BigDecimal.valueOf(price), Currency.getInstance("KRW"));

                            Toast.makeText(mCtx, "배송을 위해 배달원이 오면 결제해주세요.", Toast.LENGTH_LONG);
                            finish();
                            //JSONArray orderInfo = jsondata.getJSONArray("data");

                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });

    }

    private void setAlreadyPayment(boolean _isPayment) throws JSONException {
        final Animation fadein = AnimationUtils.loadAnimation(this, R.anim.fadein);

        if (!_isPayment) {
            aq.id(R.id.pay_select_area).visible().animate(fadein);
            //aq.id(R.id.pay_03).visible();
            // 결제안내창 노출
            Intent payNoticeIntent = new Intent(this, PaymentNoticeScreen.class);
            payNoticeIntent.putExtra("content", orderInfo.toString());
            payNoticeIntent.putExtra("deliveryRequestTime", orderInfo.getLong("deliveryRequestTimeApp"));
            payNoticeIntent.putExtra("type", "11");
            startActivity(payNoticeIntent);
        } else {
            if (orderInfo.getJSONArray("payments").length() > 0) {
                String payType = orderInfo.getJSONArray("payments").getJSONObject(0).getString("paymentType");
                if("P01".equals(payType)){
                    aq.id(R.id.payment_desc_text).visible().text("이미 결제했습니다.");
                }else if("P02".equals(payType)){
                    aq.id(R.id.payment_desc_text).visible().text("이미 결제했습니다.(핸드폰 결제)");
                }else if("P21".equals(payType)){
                    aq.id(R.id.payment_desc_text).visible().text("이미 결제했습니다.");
                }else if("P91".equals(payType)){
                    aq.id(R.id.payment_desc_text).visible().text("만나서결제(카드)를 선택했습니다.");
                }else if("P92".equals(payType)){
                    aq.id(R.id.payment_desc_text).visible().text("만나서결제(현금)를 선택했습니다.");
                }else{
                    aq.id(R.id.payment_desc_text).visible().text("이미 결제했습니다.");
                }
            }
            aq.id(R.id.pay_select_area).gone();
            aq.id(R.id.pay_03).gone();
        }

        // 미수금 처리부분 추가 2016-06-10
        if("Y".equals(orderInfo.getString("isReceivable"))){
            aq.id(R.id.pay_select_area).visible();
            aq.id(R.id.btn_pay_direct).gone();
            aq.id(R.id.pay_03).visible();
            aq.id(R.id.btn_change).gone();
        }
    }

    private void setStatus(String _status) throws JSONException {
        int price = Integer.parseInt(orderInfo.getString("deliveryPrice"));
        int orderType = Integer.parseInt(orderInfo.getString("orderType"));
        if(price < 20000){
            price = price + deliveryPay;
        }
        String priceString = MZValidator.toNumFormat(price);
        aq.id(R.id.text_price).text(priceString + "원");

        aq.id(R.id.pay_02).gone();
        aq.id(R.id.pay_select_area).gone();
        aq.id(R.id.direct_select_area).gone();
        aq.id(R.id.pay_03).gone();
        aq.id(R.id.btn_cancel).gone();
        aq.id(R.id.payment_desc_text).gone();

        final Animation fadein = AnimationUtils.loadAnimation(this, R.anim.fadein);
        final Animation fadeout = AnimationUtils.loadAnimation(this, R.anim.fadeout);

        if ("01".equals(_status)) {
            if(orderType == 1){
                aq.id(R.id.text_pickup_title).text("수거희망");
                aq.id(R.id.btn_cancel).visible().animate(fadein);
            }else if(orderType == 2){
                aq.id(R.id.text_pickup_title).text("택배접수일");
                aq.id(R.id.text_delivery_title).text("도착예정");
                aq.id(R.id.btn_change).gone();
            }

            aq.id(R.id.status_image).image(R.mipmap.order_detail_step_01);
            aq.id(R.id.order_req_area).gone();
            aq.id(R.id.order_req_repair_area).gone();

            aq.id(R.id.after_pickup_area).gone();
            aq.id(R.id.btn_receipt).gone();
        } else if ("02".equals(_status)) {
            if(orderType == 1){
                aq.id(R.id.text_pickup_title).text("수거희망");
            }else if(orderType == 2){
                aq.id(R.id.text_pickup_title).text("택배접수일");
                aq.id(R.id.text_delivery_title).text("도착예정");
                aq.id(R.id.btn_change).gone();
                aq.id(R.id.parcel_area).visible();
            }
            aq.id(R.id.status_image).image(R.mipmap.order_detail_step_01);
            aq.id(R.id.order_req_area).gone().animate(fadeout);
            aq.id(R.id.order_req_repair_area).gone().animate(fadeout);
            //aq.id(R.id.btn_cancel).visible().animate(fadein);
            aq.id(R.id.after_pickup_area).gone().animate(fadeout);
            aq.id(R.id.btn_receipt).gone().animate(fadeout);
        } else if ("03".equals(_status)) {
            if(orderType == 1){
                aq.id(R.id.text_pickup_title).text("수거완료");
            }else if(orderType == 2){
                aq.id(R.id.text_pickup_title).text("택배접수일");
                aq.id(R.id.text_delivery_title).text("도착예정");
                aq.id(R.id.btn_change).gone();
                aq.id(R.id.parcel_area).visible();
            }

            aq.id(R.id.status_image).image(R.mipmap.order_detail_step_02);
            aq.id(R.id.after_pickup_area).gone().animate(fadeout);
            aq.id(R.id.text_order_element).text("수거수량 : " + orderInfo.getInt("pickupQuantity") + "개").typeface(CommonUtility.getNanumBarunTypeface());
            aq.id(R.id.btn_receipt).gone();
        } else if ("11".equals(_status)) {
            if(orderType == 1){
                aq.id(R.id.text_pickup_title).text("수거완료");
            }else if(orderType == 2){
                aq.id(R.id.text_pickup_title).text("인수완료");
                aq.id(R.id.text_delivery_title).text("도착예정");
                aq.id(R.id.btn_change).gone();
            }

            aq.id(R.id.status_image).image(R.mipmap.order_detail_step_03);
            aq.id(R.id.after_pickup_area).visible();

            if ("N".equals(orderInfo.getString("isPayment"))) {
                setAlreadyPayment(false);
            } else {
                setAlreadyPayment(true);
            }

        } else if ("12".equals(_status)) {
            if(orderType == 1){
                aq.id(R.id.text_pickup_title).text("수거완료");
            }else if(orderType == 2){
                aq.id(R.id.text_pickup_title).text("인수완료");
                aq.id(R.id.text_delivery_title).text("도착예정");
                aq.id(R.id.btn_change).gone();
            }

            aq.id(R.id.status_image).image(R.mipmap.order_detail_step_04);
            aq.id(R.id.after_pickup_area).visible();

            if ("N".equals(orderInfo.getString("isPayment"))) {
                setAlreadyPayment(false);
            } else {
                setAlreadyPayment(true);
            }

        } else if ("13".equals(_status)) {
            if(orderType == 1){
                aq.id(R.id.text_pickup_title).text("수거완료");
            }else if(orderType == 2){
                aq.id(R.id.text_pickup_title).text("인수완료");
                aq.id(R.id.text_delivery_title).text("도착예정");
                aq.id(R.id.btn_change).gone();
            }
            aq.id(R.id.status_image).image(R.mipmap.order_detail_step_05);
            aq.id(R.id.after_pickup_area).visible();

            if ("N".equals(orderInfo.getString("isPayment"))) {
                setAlreadyPayment(false);
            } else {
                setAlreadyPayment(true);
            }
        } else if ("21".equals(_status)) {
            if(orderType == 1){
                aq.id(R.id.text_pickup_title).text("수거완료");
            }else if(orderType == 2){
                aq.id(R.id.text_pickup_title).text("인수완료");
                aq.id(R.id.text_delivery_title).text("도착예정");
                aq.id(R.id.parcel_area).visible();

            }
            aq.id(R.id.status_image).image(R.mipmap.order_detail_step_06);
            aq.id(R.id.after_pickup_area).visible();

            if ("N".equals(orderInfo.getString("isPayment"))) {
                setAlreadyPayment(false);
            } else {
                setAlreadyPayment(true);
            }

            aq.id(R.id.btn_change).gone().animate(fadeout);
        } else if ("22".equals(_status)) {
            if(orderType == 1){
                aq.id(R.id.text_pickup_title).text("수거완료");
            }else if(orderType == 2){
                aq.id(R.id.text_pickup_title).text("인수완료");
                aq.id(R.id.text_delivery_title).text("도착예정");
                aq.id(R.id.parcel_area).visible();
            }

            aq.id(R.id.status_image).image(R.mipmap.order_detail_step_07);
            aq.id(R.id.after_pickup_area).visible();

            if ("N".equals(orderInfo.getString("isPayment"))) {
                setAlreadyPayment(false);
            } else {
                setAlreadyPayment(true);
            }

            aq.id(R.id.btn_change).gone().animate(fadeout);
        } else if ("23".equals(_status)) {
            if(orderType == 1){
                aq.id(R.id.text_pickup_title).text("수거완료");
                aq.id(R.id.text_delivery_title).text("배송완료");
            }else if(orderType == 2){
                aq.id(R.id.text_pickup_title).text("인수완료");
                aq.id(R.id.text_delivery_title).text("배송완료");
            }

            aq.id(R.id.after_pickup_area).visible();
            aq.id(R.id.status_image).image(R.mipmap.order_detail_step_08);
            aq.id(R.id.btn_change).gone();

            showReviewSelector();
        } else if ("24".equals(_status)) {
            if(orderType == 1){
                aq.id(R.id.text_pickup_title).text("수거완료");
                aq.id(R.id.text_delivery_title).text("배송완료");
            }else if(orderType == 2){
                aq.id(R.id.text_pickup_title).text("인수완료");
                aq.id(R.id.text_delivery_title).text("배송완료");
            }
            aq.id(R.id.after_pickup_area).visible();
            aq.id(R.id.status_image).image(R.mipmap.order_detail_step_24);
            aq.id(R.id.btn_change).gone();

            showReviewSelector();
        } else if ("91".equals(_status)) {
            aq.id(R.id.after_pickup_area).gone();
            aq.id(R.id.status_image).image(R.mipmap.order_detail_step_91);
            aq.id(R.id.price_area).gone();
            aq.id(R.id.btn_change).gone();
            aq.id(R.id.btn_receipt).gone();
        }else if ("98".equals(_status)) {
            aq.id(R.id.after_pickup_area).gone();
            aq.id(R.id.status_image).image(R.mipmap.order_detail_step_91);
            aq.id(R.id.price_area).gone();
            aq.id(R.id.btn_change).gone();
            aq.id(R.id.btn_receipt).gone();
        } else {
            aq.id(R.id.after_pickup_area).gone();
            aq.id(R.id.status_image).image(R.mipmap.order_detail_step_99);
            aq.id(R.id.price_area).gone();
            if(orderType == 1){
                aq.id(R.id.text_pickup_title).text("수거완료");
                aq.id(R.id.text_delivery_title).text("배송완료");
            }else if(orderType == 2){
                aq.id(R.id.text_pickup_title).text("인수완료");
                aq.id(R.id.text_delivery_title).text("배송완료");
            }
            aq.id(R.id.btn_change).gone();
            aq.id(R.id.btn_receipt).gone();
        }

    }

    private void showReviewSelector(){
        /*
           TODO
           리뷰기능 OrderView json Data 평점정보,평점기록일시 필요함.
           배송완료시점으로 1주일간만 평점가능
        */
        aq.id(R.id.review_area).visible();

        if(orderInfo.isNull("rateScore")){
            aq.id(R.id.review_before).visible();
            aq.id(R.id.review_after).gone();
        }else{
            //"rateScore":{"storeId":1002,"userId":1006,"orderId":0,"rateScore":3,"rateMessage":"세탁 불만족","registerDate":"\/Date(1460547248603)\/"}}
            //{"storeId":0,"userId":0,"orderId":0,"rateScore":3,"rateMessage":"세탁 불만족","registerDate":"\/Date(1460547248603)\/","registerDateTimeApp":1460547248603}}
            aq.id(R.id.review_before).gone();
            aq.id(R.id.review_after).visible();
            try {
                JSONObject rateInfo = orderInfo.getJSONObject("rateScore");
                Log.e("rate info", rateInfo.toString());

                int score = rateInfo.getInt("rateScore");

                switch(score){
                    case 1:
                        aq.id(R.id.rate_image).image(R.mipmap.rate_image_01);
                        aq.id(R.id.rate_text).text("최악이에요..");
                        break;
                    case 2:
                        aq.id(R.id.rate_image).image(R.mipmap.rate_image_02);
                        aq.id(R.id.rate_text).text("별로에요.");
                        break;
                    case 3:
                        aq.id(R.id.rate_image).image(R.mipmap.rate_image_03);
                        aq.id(R.id.rate_text).text("보통이에요~");
                        break;
                    case 4:
                        aq.id(R.id.rate_image).image(R.mipmap.rate_image_04);
                        aq.id(R.id.rate_text).text("좋았어요!");
                        break;
                    case 5:
                        aq.id(R.id.rate_image).image(R.mipmap.rate_image_05);
                        aq.id(R.id.rate_text).text("너무 좋았어요!");
                        break;
                }

                long registerDateTimeApp = rateInfo.getLong("registerDateTimeApp");
                aq.id(R.id.rate_date).text(TimeUtil.getTimelineDateString(registerDateTimeApp));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void reviewClicked(View button){
        try {
            Intent reviewIntent = new Intent(mCtx, ReviewActivity.class);
            reviewIntent.setAction(Intent.ACTION_GET_CONTENT);
            reviewIntent.putExtra("orderId", orderInfo.getString("orderId"));
            reviewIntent.putExtra("storeId", orderInfo.getString("storeId"));
            startActivityForResult(reviewIntent, ORDER_REVIEWED);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void cancelAction() {
        RequestParams params = new RequestParams();
        try {
            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            params.put("orderId", orderInfo.getString("orderId"));
            params.put("orderStatus", "91");
            params.put("k", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkClient.post(Constants.ORDER_CANCEL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_CANCEL, error.getMessage());

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_CANCEL, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            ToastUtility.show(mCtx, "주문이 취소되었습니다", Toast.LENGTH_SHORT);
                            finish();
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    public void orderCancel(View button) {

        try {
            Intent cancelIntent = new Intent(mCtx, OrderCancelActivity.class);
            cancelIntent.setAction(Intent.ACTION_GET_CONTENT);
            cancelIntent.putExtra("orderId", orderInfo.getString("orderId"));
            startActivityForResult(cancelIntent, ORDER_OPERATION_CANCEL);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void deliveryChange(View button) {
        if(orderInfo != null){
            Log.e("deliveryChange", orderInfo.toString());
            Intent receiptIntent = new Intent(this, ModDeliveryInfoActivity.class);
            receiptIntent.setAction(Intent.ACTION_GET_CONTENT);
            receiptIntent.putExtra("INFO", orderInfo.toString());
            startActivityForResult(receiptIntent, MOD_DELIVERY_OK);
        }

    }

    public void showReceipt(View button) {

        try {
            Intent receiptIntent = new Intent(this, ReceiptActivity.class);
            receiptIntent.setAction(Intent.ACTION_GET_CONTENT);
            receiptIntent.putExtra("ORDER_ID", orderInfo.getString("orderId"));
            startActivityForResult(receiptIntent, VIEW_RECEIPT);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    boolean isRemoteCardEnabled = false;
    private void getDataRefresh(String _orderId) throws JSONException {

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_DETAIL, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            orderInfo = jsondata.getJSONObject("data");

                            if(orderInfo.getInt("orderSubType") == 104 || orderInfo.getInt("orderSubType") == 105){
                                aq.id(R.id.btn_change).gone();
                            }
                            // storePaymentMethod
                            // 0 : 현금
                            // 1 : 매장내카드결제
                            // 2 : 휴대용카드단말기
                            String storePaymentMethod = orderInfo.getString("storePaymentMethod");
                            if("1".equals(String.valueOf(storePaymentMethod.charAt(2)))){
                                isRemoteCardEnabled = true;
                            }else{
                                isRemoteCardEnabled = false;
                            }

                            initialize();
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }
}
