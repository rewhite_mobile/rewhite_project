package me.rewhite.users.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.adapter.LocationChoiceListViewAdapter;
import me.rewhite.users.adapter.MyLocationListItem;
import me.rewhite.users.common.BaseActivity;

public class DisabledScreen extends BaseActivity {

    private AQuery aq;
    String content;
    JSONArray data;
    private ArrayList<MyLocationListItem> myLocationData;
    private LocationChoiceListViewAdapter myLocationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        setContentView(R.layout.fragment_disabled_product);

        aq = new AQuery(this);

        Intent intent = getIntent();
        if (intent != null) {
            content = intent.getStringExtra("content");
            if(content == null){
                setResult(Activity.RESULT_CANCELED);
                finish();
            }else{
                String[] sas = content.split("\\,");
                for(int i = 0; i < sas.length; i++){
                    if("197".equals(sas[i])){
                        // 운동화 취급안함
                        aq.id(R.id.icon_layout_04).gone();
                    }else if("199".equals(sas[i])){
                        // 이불 취급안함
                        aq.id(R.id.icon_layout_01).gone();
                    }else if("200".equals(sas[i])){
                        // 카페트 취급안함
                        aq.id(R.id.icon_layout_03).gone();
                    }else if("203".equals(sas[i])){
                        // 커텐 취급안함
                        aq.id(R.id.icon_layout_02).gone();
                    }
                }
            }

        }

        aq.id(R.id.text_title).text(Html.fromHtml(getString(R.string.message_disabled_products)));
        aq.id(R.id.btn_close).clicked(this, "noClicked");

        try {
            initialize();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initialize() throws JSONException {

    }

    public void noClicked(View button) {
        Log.e("okCilcked", "NO");
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

}
