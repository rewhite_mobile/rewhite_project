package me.rewhite.users.activity.gsretail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.androidquery.AQuery;

import me.rewhite.users.R;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class GS25V3OrderAgreeActivity extends AppCompatActivity {

    AQuery aq;
    String userId;

    String partnerId;
    int internalStoreId;

    boolean isAgreed01 = false;
    boolean isAgreed02 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_gs25_v2_order_input_03);

        userId = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_ID);

        aq = new AQuery(this);

        //aq.id(R.id.btn_agree_01).image(R.mipmap.btn_gs25_order_input_step3_01_normal);
        //aq.id(R.id.btn_agree_02).image(R.mipmap.btn_gs25_order_input_step3_02_normal);
        aq.id(R.id.screen_title).visible();
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked").visible();
        aq.id(R.id.btn_agree_01).clicked(this, "elementAgreed").tag(1);
        aq.id(R.id.btn_agree_02).clicked(this, "elementAgreed").tag(2);
        aq.id(R.id.btn_submit).clicked(this, "nextProcessClicked");
    }

    public void closeAction(View button){
        finish();
    }

    public void nextProcessClicked(View button){
        if(isAgreed01 && isAgreed02){
            Intent inputIntent = new Intent();
            inputIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            //inputIntent.setClass(mContext, GS25V3OrderAgreeActivity.class);
            inputIntent.setClass(this, GS25V3OrderBarcodeActivity.class);

            startActivity(inputIntent);
            finish();
        }else{
            DUtil.alertShow(this, "유의사항을 동의해주세요.");
        }
    }



    public void elementAgreed(View button){
        int tag = (int)button.getTag();

        switch (tag){
            case 1:
                isAgreed01 = !isAgreed01;
                if(isAgreed01){
                    aq.id(R.id.btn_agree_01).image(R.mipmap.btn_gs25_order_input_step3_01_selected);
                }else{
                    aq.id(R.id.btn_agree_01).image(R.mipmap.btn_gs25_order_input_step3_01_normal);
                }
                break;
            case 2:
                isAgreed02 = !isAgreed02;
                if(isAgreed02){
                    aq.id(R.id.btn_agree_02).image(R.mipmap.btn_gs25_order_input_step3_02_selected);
                }else{
                    aq.id(R.id.btn_agree_02).image(R.mipmap.btn_gs25_order_input_step3_02_normal);
                }
                break;
            default:
                break;
        }

        if(isAgreed01 && isAgreed02){
            aq.id(R.id.btn_submit).backgroundColor(0xff252525).textColor(0xffffffff).enabled(true);
        }else{
            aq.id(R.id.btn_submit).backgroundColor(0xffa0a0a0).textColor(0xffd3d3d3).enabled(false);
        }
    }
}
