package me.rewhite.users.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.adapter.CouponListItem;
import me.rewhite.users.adapter.CouponListViewAdapter;
import me.rewhite.users.adapter.PointListItem;
import me.rewhite.users.adapter.PointListViewAdapter;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.MZValidator;
import me.rewhite.users.util.SharedPreferencesUtility;


public class CouponActivity extends BaseActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private AQuery aq;
    final public Context context = this;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    View decorView;

    @Override
    protected void onResume() {
        super.onResume();
        DUtil.Log("onResume", "==========onResume============");
        if(mViewPager != null){
            mSectionsPagerAdapter.notifyDataSetChanged();
            mViewPager.invalidate();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon);


        aq = new AQuery(this);
        aq.id(R.id.title_text).text(getString(R.string.title_coupon_point));
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked").image(R.mipmap.btn_menu_back_black);
        aq.id(R.id.btn_add_coupon).clicked(this, "addCouponClicked");
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }

    public void addCouponClicked(View button){

        Intent intent = new Intent(this, AddCouponActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    public void closeClicked(View button) {
        finish();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "포인트 & 코인";
                case 1:
                    return "쿠폰";
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private AQuery aq;
        WebView webView;
        ProgressDialog dialog;
        public Context ctx = getActivity();
        boolean loadingFinished = true;
        boolean redirect = false;
        private AQuery bq;
        View rootView;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            int section = getArguments().getInt(ARG_SECTION_NUMBER);
            switch (section) {
                case 0:
                    rootView = inflater.inflate(R.layout.fragment_coupon, container, false);
                    bq = new AQuery(rootView);
                    bq.id(R.id.text_nickname).typeface(CommonUtility.getNanumBarunTypeface());
                    bq.id(R.id.text_unit).typeface(CommonUtility.getNanumBarunTypeface());
                    bq.id(R.id.text_sub_info).typeface(CommonUtility.getNanumBarunLightTypeface());
                    bq.id(R.id.text_desc).typeface(CommonUtility.getNanumBarunLightTypeface());
                    //
                    String availPoint = MZValidator.toNumFormat(Integer.parseInt(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USE_AVAILABLE_POINT)));
                    bq.id(R.id.text_available).text(availPoint).typeface(CommonUtility.getNanumBarunLightTypeface());
                    String presavePoint = MZValidator.toNumFormat(Integer.parseInt(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EXPECT_POINT)));
                    bq.id(R.id.text_presave).text(presavePoint).typeface(CommonUtility.getNanumBarunLightTypeface());

                    getData("M");
                    break;
                case 1:
                    rootView = inflater.inflate(R.layout.fragment_coupon, container, false);
                    bq = new AQuery(rootView);
                    bq.id(R.id.text_nickname).text(getString(R.string.item_available_coupon_count)).typeface(CommonUtility.getNanumBarunTypeface());
                    bq.id(R.id.text_unit).text(getString(R.string.item_available_coupon_unit)).typeface(CommonUtility.getNanumBarunTypeface());
                    bq.id(R.id.text_sub_info).gone().typeface(CommonUtility.getNanumBarunLightTypeface());
                    bq.id(R.id.text_desc).gone().typeface(CommonUtility.getNanumBarunLightTypeface());

                    getData("C");
                    break;
            }

            if(dialog == null){
                // Progress 처리 진행상황을 보기위해
                dialog = new ProgressDialog(getActivity());
                dialog.setIndeterminate(true);
                dialog.setCancelable(true);
                dialog.setInverseBackgroundForced(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setMessage(getString(R.string.web_loading_comment));
            }


            return rootView;
        }

        private void getData(String _type) {
            if ("M".equals(_type)) {
                getMileageData();
            } else if ("C".equals(_type)) {
                getCouponData();
            }
        }

        private void getMileageData() {
            RequestParams params = new RequestParams();
            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            params.put("page", 1);
            params.put("block", 100);
            params.put("k", 1);
            NetworkClient.post(Constants.MILEAGE_LIST, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                    // TODO Auto-generated method stub
                    DUtil.Log(Constants.MILEAGE_LIST, error.getMessage());
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                    String result;
                    try {
                        result = new String(data, "UTF-8");
                        DUtil.Log(Constants.MILEAGE_LIST, result);

                        JSONObject jsondata = new JSONObject(result);

                        if (dialog != null) {
                            dialog.dismiss();
                        }

                        if ("S0000".equals(jsondata.getString("resultCode"))) {
                            if (jsondata.isNull("data")) {
                                // 주문내역이 없습니다
                                bq.id(R.id.screen_no_point).visible();
                            } else {
                                //bq.id(R.id.listView)
                                ListView pointListView = (ListView) rootView.findViewById(R.id.listView);
                                ArrayList<PointListItem> pointData = new ArrayList<>();

                                JSONArray pointInfo = jsondata.getJSONArray("data");

                                if (pointInfo.length() > 0) {
                                    bq.id(R.id.screen_no_point).gone();
                                    //aq.id(R.id.empty_screen).gone();
                                    for (int i = 0; i < pointInfo.length(); i++) {

                                        String isSaving = pointInfo.getJSONObject(i).getString("isSaving");
                                        boolean isSavingBool = ("Y".equals(isSaving)) ? true : false;
                                        int mileage = pointInfo.getJSONObject(i).getInt("mileage");
                                        String referrer = pointInfo.getJSONObject(i).getString("mileageValue");
                                        long availableDateApp = pointInfo.getJSONObject(i).getLong("availableDateApp");
                                        long regDate = pointInfo.getJSONObject(i).getLong("registerDateApp");
                                        String mileageTypeApp = pointInfo.getJSONObject(i).getString("mileageTypeApp");
                                        int mileageType = pointInfo.getJSONObject(i).getInt("mileageType");

                                        //PointListItem aItem = new PointListItem(isSavingBool, mileage, referrer, availableDateApp, regDate, mileageTypeApp, mileageType);
                                        //pointData.add(aItem);
                                    }

                                    PointListViewAdapter pointAdapter = new PointListViewAdapter(getActivity(), R.layout.point_list_item, pointData);
                                    pointAdapter.notifyDataSetChanged();

                                    pointListView.setAdapter(pointAdapter);
                                } else {
                                    bq.id(R.id.screen_no_point).visible();
                                }
                            }

                        } else {

                        }
                    } catch (UnsupportedEncodingException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                }
            });
        }

        private void getCouponData() {
            RequestParams params = new RequestParams();
            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            params.put("applyType", "A");
            //params.put("applyValue", "A");
            params.put("page", 1);
            params.put("block", 100);
            params.put("k", 1);
            NetworkClient.post(Constants.COUPON_LIST, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                    // TODO Auto-generated method stub
                    DUtil.Log(Constants.COUPON_LIST, error.getMessage());
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                    String result;
                    try {
                        result = new String(data, "UTF-8");
                        DUtil.Log(Constants.COUPON_LIST, result);

                        JSONObject jsondata = new JSONObject(result);

                        if (dialog != null) {
                            dialog.dismiss();
                        }

                        if ("S0000".equals(jsondata.getString("resultCode"))) {
                            if (jsondata.isNull("data")) {
                                // 주문내역이 없습니다
                                bq.id(R.id.screen_no_coupon).visible();
                            } else {
                                JSONArray couponInfo = jsondata.getJSONArray("data");
                                String count = MZValidator.toNumFormat(couponInfo.length());
                                bq.id(R.id.text_available).text(count);

                                //bq.id(R.id.listView)
                                ListView couponListView = (ListView) rootView.findViewById(R.id.listView);
                                ArrayList<CouponListItem> couponData = new ArrayList<>();

                                if (couponInfo.length() > 0) {
                                    bq.id(R.id.screen_no_coupon).gone();
                                    for (int i = 0; i < couponInfo.length(); i++) {

                                        int couponId = couponInfo.getJSONObject(i).getInt("couponId");
                                        String couponTitle = couponInfo.getJSONObject(i).getString("couponTitle");
                                        String couponType = couponInfo.getJSONObject(i).getString("couponType");
                                        int couponValue = couponInfo.getJSONObject(i).getInt("couponValue");
                                        int couponBaseMoney = couponInfo.getJSONObject(i).getInt("couponBaseMoney");
                                        int couponMaxMoney = couponInfo.getJSONObject(i).getInt("couponMaxMoney");

                                        long useStartDateApp = couponInfo.getJSONObject(i).getLong("useStartDateApp");
                                        long useFinishDateApp = couponInfo.getJSONObject(i).getLong("useFinishDateApp");
                                        long registerDateApp = couponInfo.getJSONObject(i).getLong("registerDateApp");
                                        String isUse = couponInfo.getJSONObject(i).getString("isUse");

                                        CouponListItem aItem = new CouponListItem(couponId, couponTitle, couponType, couponValue, couponBaseMoney, couponMaxMoney, useStartDateApp, useFinishDateApp, registerDateApp, isUse);
                                        couponData.add(aItem);
                                    }

                                    CouponListViewAdapter pointAdapter = new CouponListViewAdapter(getActivity(), R.layout.coupon_list_item, couponData);
                                    pointAdapter.notifyDataSetChanged();

                                    couponListView.setAdapter(pointAdapter);
                                } else {
                                    bq.id(R.id.screen_no_coupon).visible();
                                }
                            }

                        } else {

                        }
                    } catch (UnsupportedEncodingException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                }
            });
        }
    }
}
