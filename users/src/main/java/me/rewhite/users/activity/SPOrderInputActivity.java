package me.rewhite.users.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class SPOrderInputActivity extends BaseActivity {

    private AQuery aq;
    private int count01;
    private int count02;
    private int count03;
    private int count04;
    private String addressSeq;

    int locationCount;
    private final Context mCtx = this;

    @Override
    protected void onResume(){
        super.onResume();
        initialize();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sporder_input);

        aq = new AQuery(this);

        companyString = "";

        initialize();

        aq.id(R.id.btn_selector).clicked(this, "selectCompany");
        aq.id(R.id.btn_add_01).clicked(this, "countAdd").tag(1);
        aq.id(R.id.btn_add_02).clicked(this, "countAdd").tag(2);
        aq.id(R.id.btn_add_03).clicked(this, "countAdd").tag(3);

        aq.id(R.id.btn_remove_01).clicked(this, "countRemove").tag(1);
        aq.id(R.id.btn_remove_02).clicked(this, "countRemove").tag(2);
        aq.id(R.id.btn_remove_03).clicked(this, "countRemove").tag(3);

        count01 = 0;
        count02 = 0;
        count03 = 0;
        count04 = 0;

        aq.id(R.id.btn_submit).clicked(this, "submitInput");
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
    }

    public void closeClicked(View button) {
        finish();
    }

    private void initialize(){
        try {

            JSONArray locations = SharedPreferencesUtility.getLocationData();
            if (locations != null) {
                if (locations.length() > 0) {
                    aq.id(R.id.btn_address_add).image(R.drawable.btn_sp_address_pick).clicked(this, "locationChangeClicked");

                    if (locations.length() >= 1) {
                        JSONObject myDefaultLocation = locations.getJSONObject(0);
                        addressSeq = myDefaultLocation.getString("addressSeq");
                        aq.id(R.id.address_label).text(myDefaultLocation.getString("address1") + "\n" + myDefaultLocation.getString("address2"));
                    }
                    locationCount = locations.length();
                }else{
                    locationCount = 0;
                    aq.id(R.id.btn_address_add).image(R.drawable.btn_sp_address_add).clicked(this, "locationAddClicked");
                }
            }else{
                locationCount = 0;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void countAdd(View button){
        int tag = (int)button.getTag();

        switch(tag){
            case 1:
                count01++;
                break;
            case 2:
                count02++;
                break;
            case 3:
                count03++;
                break;
        }

        displayCount();
    }

    public void countRemove(View button){
        int tag = (int)button.getTag();

        switch(tag){
            case 1:
                count01--;
                if(count01 < 0){
                    count01 = 0;
                }
                break;
            case 2:
                count02--;
                if(count02 < 0){
                    count02 = 0;
                }
                break;
            case 3:
                count03--;
                if(count03 < 0){
                    count03 = 0;
                }
                break;
        }

        displayCount();
    }

    private void displayCount(){
        count04 = count01 + count02 + count03;
        aq.id(R.id.count_01).text(count01 + "");
        aq.id(R.id.count_02).text(count02 + "");
        aq.id(R.id.count_03).text(count03 + "");
        aq.id(R.id.count_04).text(count04 + "");
    }

    final String items[] = {"우체국택배", "CJ대한통운", "KG로지스", "한진택배", "로젠택배", "현대로지스틱스", "경동택배", "동부택배"};
    int selectedIndex = 0;
    String companyString = "";

    public void selectCompany(View button){
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle("택배회사를 선택해주세요.");
        ab.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedIndex = which;
            }
        }).setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                companyString = items[selectedIndex];
                aq.id(R.id.company_label).text(companyString).typeface(CommonUtility.getNanumBarunTypeface());
            }
        }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        ab.show();
    }

    public static final int CHOICE_LOCATION = 8;

    public void locationChangeClicked(View button){

        Intent choiceIntent = new Intent(this, LocationChoiceScreen.class);
        choiceIntent.putExtra("content", SharedPreferencesUtility.getLocationData().toString());

        choiceIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(choiceIntent, CHOICE_LOCATION);
    }

    public void locationAddClicked(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, LocationActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
            }
        }, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CHOICE_LOCATION:
                    initialize();
                    break;

                default:
                    break;
            }
        }
    }

    public void submitInput(View button){

        if(locationCount == 0){
            DUtil.alertShow(this, "받으실 주소를 먼저 등록해주세요.");
            return;
        }

        if(count04 == 0){
            DUtil.alertShow(this, "수량을 입력해주세요.");
            return;
        }

        if(companyString.length() == 0){
            DUtil.alertShow(this, "택배회사를 선택해주세요.");
            return;
        }

        if(aq.id(R.id.input_parcel_number).getText().toString().length() == 0){
            DUtil.alertShow(this, "운송장번호를 입력해주세요.");
            return;
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                RequestParams params = new RequestParams();
                params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                params.put("addressSeq", addressSeq);
                params.put("pickupCompany", companyString);
                params.put("pickupNumber", aq.id(R.id.input_parcel_number).getText().toString());
                params.put("pickupItems", "상의^"+count01+"|하의^"+count02+"|한벌^"+count03);
                params.put("requestId", "1");
                params.put("pickupMessage", aq.id(R.id.input_request_message).getText().toString());
                params.put("k", 1);
                NetworkClient.post(Constants.ORDER_PARCEL, params, new AsyncHttpResponseHandler() {

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                        // TODO Auto-generated method stub
                        DUtil.Log(Constants.ORDER_PARCEL, error.getMessage());

                    }

                    @Override
                    public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                        String result;
                        try {
                            result = new String(data, "UTF-8");
                            DUtil.Log(Constants.ORDER_PARCEL, result);

                            JSONObject jsondata = new JSONObject(result);

                            if ("S0000".equals(jsondata.getString("resultCode"))) {
                                Tracker t = ((Bootstrap)getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                                t.send(new HitBuilders.EventBuilder()
                                        .setCategory("주문")
                                        .setAction("시즌세탁송장입력")
                                        //.setLabel("Order")
                                        .build());

                                android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(mCtx);
                                alertDialogBuilder.setCancelable(false).setMessage("성공적으로 접수되었습니다. 주문내역에서 진행상황을 확인할수 있습니다.")
                                        .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                finish();
                                            }
                                        });
                                android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }else{
                                android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(mCtx);
                                alertDialogBuilder.setCancelable(true).setMessage(jsondata.getString("message"))
                                        .setPositiveButton("확인", new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });
                                android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();

                            }
                        } catch (UnsupportedEncodingException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        } catch (JSONException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                    }
                });
            }
        }, 200);
    }
}
