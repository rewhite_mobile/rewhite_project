package me.rewhite.users.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.ToastUtility;

public class PasswordModActivity extends BaseActivity {

    AQuery aq;
    final Context ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_mod);

        aq = new AQuery(this);
        aq.id(R.id.btn_next).clicked(this, "modPasswordAction");
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        aq.id(R.id.text_old_alert).gone();
        aq.id(R.id.text_new_alert).gone();
    }

    public void closeClicked(View button) {
        finish();
    }

    public void modPasswordAction(View button) {

        String oldString = aq.id(R.id.tf_old).getText().toString();
        String newString = aq.id(R.id.tf_new).getText().toString();
        String newConfirmString = aq.id(R.id.tf_new_confirm).getText().toString();

        aq.id(R.id.text_old_alert).gone();
        aq.id(R.id.text_new_alert).gone();

        if (newString.equals(newConfirmString)) {
            if (newString.length() > 7) {
                if (oldString.length() > 7) {

                } else {
                    aq.id(R.id.text_old_alert).visible().text("비밀번호는 8자리 이상이여야 합니다.");
                    //DUtil.alertShow(this, "비밀번호는 6자리 이상입니다.");
                    return;
                }
            } else {
                aq.id(R.id.text_new_alert).visible().text("비밀번호는 8자리 이상이여야 합니다.");
                return;
            }
        } else {
            aq.id(R.id.text_new_alert).visible().text("입력하신 새로운 비밀번호가 일치하지 않습니다");
            //DUtil.alertShow(this, "입력하신 새로운 비밀번호가 일치하지 않습니다");
            return;
        }

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("secretKey1", aq.id(R.id.tf_old).getText().toString());
        params.put("secretKey2", aq.id(R.id.tf_new).getText().toString());
        params.put("k", 1);
        NetworkClient.post(Constants.MOD_PASSWORD, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.MOD_PASSWORD, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.MOD_PASSWORD, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        //JSONObject userInfo = jsondata.getJSONObject("data");
                        ToastUtility.show(ctx, "비밀번호가 성공적으로 변경되었습니다.", Toast.LENGTH_LONG);
                        finish();
                    } else if ("S9008".equals(jsondata.getString("resultCode"))) {
                        //JSONObject userInfo = jsondata.getJSONObject("data");
                        DUtil.alertShow(PasswordModActivity.this, "비밀번호가 다릅니다.");
                    } else {
                        DUtil.alertShow(PasswordModActivity.this, jsondata.getString("message"));
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }
}
