package me.rewhite.users.activity.theplace;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.androidquery.AQuery;
import com.balysv.materialripple.MaterialRippleLayout;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.util.DUtil;

public class ThePlaceStoreManualInputActivity extends BaseActivity {

    AQuery aq;
    String appendNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_the_place_store_manual_input);

        aq = new AQuery(this);
        aq.id(R.id.btn_del).clicked(this, "numberRemoveAction");
        aq.id(R.id.btn_del_1).clicked(this, "numberDelete1Action");
        aq.id(R.id.btn_popup_close).clicked(this, "closeAction");

        aq.id(R.id.btn_0).clicked(this, "numpadClickAction").tag(0);
        aq.id(R.id.btn_1).clicked(this, "numpadClickAction").tag(1);
        aq.id(R.id.btn_2).clicked(this, "numpadClickAction").tag(2);
        aq.id(R.id.btn_3).clicked(this, "numpadClickAction").tag(3);
        aq.id(R.id.btn_4).clicked(this, "numpadClickAction").tag(4);
        aq.id(R.id.btn_5).clicked(this, "numpadClickAction").tag(5);
        aq.id(R.id.btn_6).clicked(this, "numpadClickAction").tag(6);
        aq.id(R.id.btn_7).clicked(this, "numpadClickAction").tag(7);
        aq.id(R.id.btn_8).clicked(this, "numpadClickAction").tag(8);
        aq.id(R.id.btn_9).clicked(this, "numpadClickAction").tag(9);
    }

    private void setMaterialRippleLayout(View _target){
        MaterialRippleLayout.on(_target)
                .rippleColor(Color.parseColor("#FFffff"))
                .rippleAlpha(0.2f)
                .rippleHover(true)
                .rippleOverlay(true)
                .create();
    }

    public void numpadClickAction(View button){
        int tag = (int)button.getTag();

        if(appendNumber.length() < 6){
            appendNumber = appendNumber + tag;
            aq.id(R.id.num_text).text(appendNumber);
        }

        if(appendNumber.length() == 6){
            if(validCheckStoreId(appendNumber)){
                Intent resultData = new Intent();
                resultData.putExtra("code", appendNumber.substring(0,5));
                setResult(Activity.RESULT_OK, resultData);

                finish();
            }else{
                DUtil.alertShow(this, "입력한 정보가 유효하지않습니다.");
                appendNumber = "";
                aq.id(R.id.num_text).text(appendNumber);
            }
        }
    }

    private boolean validCheckStoreId(String _code){

        if(_code.length() != 6){
            return false;
        }

        Log.e("validCheckStoreId", _code);
        String preStoreId = _code.substring(0,1);
        String checkDigit = _code.substring(_code.length()-1,_code.length());

        if("7".equals(checkDigit)){
            if("1".equals(preStoreId) || "2".equals(preStoreId)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    public void closeAction(View button){
        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }

    public void numberDelete1Action(View button){
        if(appendNumber.length() > 0){
            appendNumber = appendNumber.substring(0, appendNumber.length()-1);

            aq.id(R.id.num_text).text(appendNumber);
        }else{
            appendNumber = "";
            aq.id(R.id.num_text).text("");
        }
    }

    public void numberRemoveAction(View button){
        appendNumber = "";
        aq.id(R.id.num_text).text("");
    }
}
