package me.rewhite.users.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.androidquery.AQuery;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;

public class SPOrderSendActivity extends BaseActivity {

    private AQuery aq;
    private final Context mCtx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sporder_send);

        aq = new AQuery(this);
        aq.id(R.id.btn_check).clicked(this, "checkClicked");

        aq.id(R.id.address_label).text("경기도 고양시 덕양구 주교동 579-14번지 1층\n월드드라이샵 (리화이트 주교점)\n050-7974-8013");

        aq.id(R.id.btn_type_01).clicked(this, "type01Clicked");
        aq.id(R.id.btn_type_02).clicked(this, "type02Clicked");

        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
    }

    public void closeClicked(View button) {
        finish();
    }

    public void checkClicked(View button){
        aq.id(R.id.sub02).visible();
        aq.id(R.id.sub03).visible();
    }

    public void type01Clicked(View button){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
        alertDialogBuilder.setCancelable(false).setMessage("고객님이 택배접수 후에 해당택배사의 운송장번호를 꼭 입력해야합니다. 우체국택배 사이트로 이동하시겠습니까?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(mCtx, HtmlActivity.class);
                        intent.putExtra("TERMS_URI", "https://m.epost.go.kr/parcel/mobile.ParcelRequest.parcel");
                        intent.putExtra("TITLE_NAME", "우체국택배");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        startActivity(intent);

                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void type02Clicked(View button){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
        alertDialogBuilder.setCancelable(false).setMessage("고객님이 택배접수 후에 해당택배사의 운송장번호를 꼭 입력해야합니다. CJ대한통운 사이트로 이동하시겠습니까?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(mCtx, HtmlActivity.class);
                        intent.putExtra("TERMS_URI", "https://www.doortodoor.co.kr/m/sub/dlv_main.jsp");
                        intent.putExtra("TITLE_NAME", "CJ대한통운");
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        startActivity(intent);

                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
