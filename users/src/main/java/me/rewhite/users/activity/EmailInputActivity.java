package me.rewhite.users.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.androidquery.AQuery;

import java.util.ArrayList;
import java.util.regex.Pattern;

import me.rewhite.users.R;
import me.rewhite.users.adapter.EmailListItem;
import me.rewhite.users.adapter.EmailListViewAdapter;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.StringUtil;
import me.rewhite.users.util.ValidateUtil;

public class EmailInputActivity extends BaseActivity {

    private final static String TAG = EmailInputActivity.class.getSimpleName();

    public AQuery aq;
    public final Context mCtx = this;
    private String content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_input);

        aq = new AQuery(this);

        Intent intent = getIntent();
        if (intent != null) {
            content = intent.getStringExtra("content");
        }


        ListView listView = (ListView) findViewById(R.id.listView);
        ArrayList<EmailListItem> data = new ArrayList<>();

        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        AccountManager mgr = AccountManager.get(this);
        Account[] accounts = mgr.getAccounts();

        for (Account account : accounts) {
            Log.i(TAG, "name = " + accounts.toString());
            if (emailPattern.matcher(account.name).matches()) {
                String name = account.name;
                String type = account.type;

                Log.i(TAG, "name = " + name +
                        "\ntype = " + type);

                if (ValidateUtil.checkEmail(name)) {
                    if (!checkDuplicated(data, name)) {
                        EmailListItem lion = new EmailListItem(name);
                        data.add(lion);

                        Log.i(TAG, "name = " + name +
                                "\ntype = " + type);
                    }
                }
            }
        }

        EmailListViewAdapter adapter = new EmailListViewAdapter(this, R.layout.email_list_item, data);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                AQuery bq = new AQuery(view);
                //EmailListItem entry = (EmailListItem) parent.getAdapter().getItem(position);
                String emailAddress = bq.id(R.id.text_email).getText().toString();
                aq.id(R.id.edit_email).text(emailAddress).typeface(CommonUtility.getNanumBarunTypeface());
                //view.findViewById(R.id.icon_check).setVisibility(View.VISIBLE);
            }
        });

        aq.id(R.id.btn_confirm).clicked(this, "selectConfirm");
    }

    public void selectConfirm(View button) {
        String email = aq.id(R.id.edit_email).getText().toString();
        if (StringUtil.isNullOrEmpty(email)) {
            DUtil.alertShow(this, "이메일을 입력하거나 선택해주세요");
        } else {
            Intent resultData = new Intent();
            resultData.putExtra("content", email);
            setResult(Activity.RESULT_OK, resultData);
            finish();
        }
    }

    private boolean checkDuplicated(ArrayList<EmailListItem> data, String name) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

}
