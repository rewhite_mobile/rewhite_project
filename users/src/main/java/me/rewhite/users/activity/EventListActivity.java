package me.rewhite.users.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.adapter.MyEventListItem;
import me.rewhite.users.adapter.MyEventListViewAdapter;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class EventListActivity extends AppCompatActivity {

    private AQuery aq;
    ProgressDialog dialog;
    JSONArray eventArray;

    Context mCtx = this;

    private ArrayList<MyEventListItem> myEventData;
    private MyEventListViewAdapter myEventAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);

        aq = new AQuery(this);
        aq.id(R.id.title_text).text("이벤트");
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked").image(R.mipmap.btn_menu_back_black);

        if(dialog == null){
            // Progress 처리 진행상황을 보기위해
            dialog = new ProgressDialog(this);
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.setInverseBackgroundForced(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setMessage(getString(R.string.web_loading_comment));
        }

        initialize();
    }

    public void closeClicked(View button) {
        finish();
    }

    private void initialize(){
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("page", 1);
        params.put("block", 100);
        params.put("k", 1);
        NetworkClient.post(Constants.EVENT_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.EVENT_LIST, error.getMessage());
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.EVENT_LIST, result);

                    JSONObject jsondata = new JSONObject(result);

                    if (dialog != null) {
                        dialog.dismiss();
                    }

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            eventArray = new JSONArray();
                        } else {
                            //eventArray = jsondata.getJSONArray("data");

                            // locationListView
                            ListView eventListView = (ListView) findViewById(R.id.listView);
                            myEventData = new ArrayList<>();

                            JSONArray eventItems = jsondata.getJSONArray("data");
                            if(eventItems.length() > 0){
                                for (int i = 0; i < eventItems.length(); i++) {
                                    int bannerId = eventItems.getJSONObject(i).getInt("bannerId");
                                    String title = eventItems.getJSONObject(i).getString("title");
                                    String eventType = eventItems.getJSONObject(i).getString("eventType");
                                    String url = eventItems.getJSONObject(i).getString("url");
                                    String image = eventItems.getJSONObject(i).getString("image");
                                    long startDateApp = eventItems.getJSONObject(i).getLong("startDateApp");
                                    long finishDateApp = eventItems.getJSONObject(i).getLong("finishDateApp");

                                    MyEventListItem aItem = new MyEventListItem(bannerId, title, eventType, url, image, startDateApp, finishDateApp);
                                    myEventData.add(aItem);
                                }

                                myEventAdapter = new MyEventListViewAdapter(mCtx, R.layout.myevent_list_item, myEventData);
                                myEventAdapter.notifyDataSetChanged();

                                eventListView.setAdapter(myEventAdapter);
                            }


                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    public void itemSelected(String url){
        Log.i("eventlist selected", url);

        Uri uri = Uri.parse(url);
        String queryHost = uri.getHost();

        if("http".equals(uri.getScheme()) || "https".equals(uri.getScheme())){
            Intent intent = new Intent(this, HtmlActivity.class);
            intent.putExtra("TERMS_URI", url);
            intent.putExtra("TITLE_NAME", "이벤트");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(intent);
        }else{
            //String url ="selphone://post_detail?post_id=10";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        }



    }
}
