package me.rewhite.users.activity.gsretail;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.PriceActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.PickupType;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class GS25OrderInputActivity extends AppCompatActivity implements NumberPicker.OnValueChangeListener{

    private static final String TAG = "GS25OrderInputActivity";
    private Context mContext;
    private AQuery aq;
    ImageButton button = null;

    JSONObject storeInfo;
    String partnerStoreId = "1002";
    String partnerStoreName = "";
    String priceGroupId = "";
    String storeId = "1002";
    int pickupMode = PickupType.PICKUPTYPE_GS25;

    int count_unit = 0;
    int count_pack = 0;

    private static final int REQUEST_CONTENT_INPUT = 1;
    private static final int REQUEST_ORDER_CONFIRM = 2;
    String request_content_string = "";

    static Dialog unitDialog;
    static Dialog packDialog;

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(true).setMessage("주문접수 중인데, 중단하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gs25_order_input);

        DUtil.alertShow(this, "편의점 점원에게 접수봉투를 받아서 수량을 입력해주세요.");

        aq = new AQuery(this);

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        mContext = this;

        Intent intent = getIntent();
        if(intent != null){
            try {
                storeInfo = new JSONObject(intent.getStringExtra("storeInfo"));
                storeId = storeInfo.getString("storeId");
                partnerStoreId = storeInfo.getString("partnerStoreId");
                partnerStoreName = storeInfo.getString("partnerStoreName");
                priceGroupId = storeInfo.getString("priceGroupId");
                getConvStoreInfo(partnerStoreId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        //convStoreId
        int washingDays = 0;
        try {
            washingDays = storeInfo.getJSONObject("storeInformation").getInt("partnerWashingDays");
            aq.id(R.id.text_complete_date).text("수거접수 후 " + washingDays + "일 이내");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        aq.id(R.id.input_desc_rcl).visible();

        aq.id(R.id.price_layout).clicked(this, "clickPriceTable");
        aq.id(R.id.unit_count_layout).clicked(this, "unitCountModClicked");
        aq.id(R.id.pack_count_layout).clicked(this, "packageCountModClicked");
        aq.id(R.id.request_content_layout).clicked(this, "requestContentClicked");
        aq.id(R.id.btn_submit).clicked(this, "submitClicked").enabled(false);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
    }
    public void closeClicked(View button) {
        finish();
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

        Log.i("value is",""+newVal);

    }

    private void unitNumberPickerShow(){
        final Dialog d = new Dialog(GS25OrderInputActivity.this);
        d.setTitle("맡기실 세탁물 수량을 입력해주세요.");
        d.setContentView(R.layout.dialog_unit_picker);
        Button b1 = (Button) d.findViewById(R.id.btn_unit_cancel);
        Button b2 = (Button) d.findViewById(R.id.btn_unit_ok);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.unitPicker);
        np.setMaxValue(100);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        b2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                //tv.setText(String.valueOf(np.getValue()));
                Log.e("unitPick", String.valueOf(np.getValue()));
                count_unit = np.getValue();
                if(count_unit > 0){
                    aq.id(R.id.text_unit_count).text(String.valueOf(count_unit) + " 개").textSize(18.f);
                }else{
                    aq.id(R.id.text_unit_count).text("수량을 정확하게 입력해주세요").textSize(13.f);
                }
                submitButtonInitialize();
                d.dismiss();
            }
        });
        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    private void packNumberPickerShow(){
        final Dialog d = new Dialog(GS25OrderInputActivity.this);
        d.setTitle("포장한 수거봉투 수량을 입력해주세요.");
        d.setContentView(R.layout.dialog_pack_picker);
        Button b1 = (Button) d.findViewById(R.id.btn_pack_cancel);
        Button b2 = (Button) d.findViewById(R.id.btn_pack_ok);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.packPicker);
        np.setMaxValue(100);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);
        np.setOnValueChangedListener(this);
        b2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                //tv.setText(String.valueOf(np.getValue()));
                Log.e("unitPick", String.valueOf(np.getValue()));
                count_pack = np.getValue();
                if(count_unit > 0){
                    aq.id(R.id.text_pack_count).text(String.valueOf(count_pack) + " 개").textSize(18.f);
                }else{
                    aq.id(R.id.text_pack_count).text("수량을 정확하게 입력해주세요").textSize(13.f);
                }
                submitButtonInitialize();
                d.dismiss();
            }
        });
        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

    private void getConvStoreInfo(String convStoreId){
        // 해당 편의점ID로 매칭된 세탁소정보 조회

        // 완료예정일
        aq.id(R.id.text_complete_date).text("3월 20일 오후 5시 이후");
        aq.id(R.id.conv_store_name).text(partnerStoreName);
    }

    public void clickPriceTable(View button){
        Intent intent = new Intent(this, PriceActivity.class);
        intent.putExtra("partnerId", "7");
        intent.putExtra("priceGroupId", priceGroupId);
        intent.putExtra("pickupMode", pickupMode);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    public void unitCountModClicked(View button){
        unitNumberPickerShow();
    }

    public void packageCountModClicked(View button){
        packNumberPickerShow();
    }

    public void requestContentClicked(View button){
        Intent intent = new Intent(this, GSOrderRequestContentActivity.class);
        intent.putExtra("content", request_content_string);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivityForResult(intent, REQUEST_CONTENT_INPUT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CONTENT_INPUT:
                    String result = data.getStringExtra("INPUT_CONTENT");
                    request_content_string = result;
                    if(result.length() > 15){
                        result = result.substring(0, 15) + "..";
                    }
                    if(result.length() > 0){
                        aq.id(R.id.input_desc_rcl).gone();
                    }else{
                        aq.id(R.id.input_desc_rcl).visible();
                    }
                    aq.id(R.id.text_req_content).text(result);
                    break;
                case REQUEST_ORDER_CONFIRM:
                    String orderResult = data.getStringExtra("REQ_ORDER_INFO");
                    Log.e("REQ_ORDER_INFO OK", orderResult);
                    try {
                        submitCompleteProcess(new JSONObject(orderResult));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
                default:
                    break;
            }
        }
    }

    private void submitCompleteProcess(JSONObject req){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        try {
            params.put("partnerId", req.getString("partnerId"));
            params.put("partnerStoreId", req.getString("partnerStoreId"));
            params.put("pickupQuantity", req.getString("pickupQuantity"));
            params.put("packQuantity", req.getString("packQuantity"));
            params.put("pickupRequestMessage", req.getString("pickupRequestMessage"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        params.put("k", 1);

        NetworkClient.post(Constants.GS_ORDER_CREATE, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GS_ORDER_CREATE, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GS_ORDER_CREATE, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        try{
                            Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
                            t.send(new HitBuilders.EventBuilder()
                                    .setCategory("GS주문")
                                    .setAction("주문완료")
                                    //.setLabel("Order")
                                    .build());
                        }catch (NullPointerException e){
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(GS25OrderInputActivity.this, GS25OrderCompleteActivity.class);
                        intent.putExtra("orderId", jsondata.getString("data"));
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(mContext, jsondata.getString("message"), Toast.LENGTH_LONG).show();
                        //DUtil.alertShow(GSOrderNoticeScreen.this, "이용가능한 GS25에서 QR코드를 촬영해주세요.");
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });

    }

    private void submitButtonInitialize(){
        if(count_pack > 0 && count_unit > 0){
            aq.id(R.id.btn_submit).image(R.mipmap.btn_gs_order_start).enabled(true);
        }else{
            aq.id(R.id.btn_submit).image(R.mipmap.btn_gs_order_input).enabled(false);
        }
    }

    public void submitClicked(View button){

        if(count_unit > 0  && count_pack > 0){
            if(count_unit < count_pack){
                DUtil.alertShow(this, "세탁물 수량보다 수거봉투 수량이 많습니다. 수량을 다시 확인해주세요.");
            }else{
                Intent intent = new Intent(this, GSOrderNoticeScreen.class);
                // DEBUG

                try {
                    JSONObject temp = new JSONObject();
                    temp.put("partnerId", "7");
                    temp.put("partnerStoreId", partnerStoreId);
                    temp.put("pickupQuantity", count_unit);
                    temp.put("packQuantity", count_pack);
                    temp.put("pickupRequestMessage", request_content_string);
                    intent.putExtra("orderInfo", temp.toString());

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | Intent.FLAG_ACTIVITY_SINGLE_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    startActivityForResult(intent, REQUEST_ORDER_CONFIRM);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }


    }

}
