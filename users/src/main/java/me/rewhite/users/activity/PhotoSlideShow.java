package me.rewhite.users.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ToxicBakery.viewpager.transforms.RotateUpTransformer;
import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;

public class PhotoSlideShow extends BaseActivity {

    private int mSelectedItem;
    private ViewPager mPager;
    private PageAdapter mAdapter;

    public JSONArray photos;
    AQuery aq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_slide_show);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                photos = new JSONArray(intent.getStringExtra("source"));
                mSelectedItem = intent.getIntExtra("position", 0);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        aq = new AQuery(this);

        mAdapter = new PageAdapter(getSupportFragmentManager());
        mAdapter.setItems(photos);
        // Reference (or instantiate) a ViewPager instance and apply a transformer
        mPager = (ViewPager) findViewById(R.id.container);
        mPager.setAdapter(mAdapter);
        mPager.setCurrentItem(mSelectedItem);
        mPager.setPageTransformer(true, new RotateUpTransformer());

        aq.id(R.id.btn_close).clicked(this, "closeAction");
    }

    public void closeAction(View button){
        finish();
    }

    private static final class PageAdapter extends FragmentStatePagerAdapter {
        JSONArray photos;

        public PageAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
            photos = new JSONArray();
        }

        public void setItems(JSONArray _items){
            photos = _items;
        }

        @Override
        public Fragment getItem(int position) {
            final Bundle bundle = new Bundle();
            bundle.putInt(PlaceholderFragment.EXTRA_POSITION, position + 1);
            try {
                bundle.putString("url", photos.getJSONObject(position).getString("fileUrl"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            final PlaceholderFragment fragment = new PlaceholderFragment();
            fragment.setArguments(bundle);

            return fragment;
        }

        @Override
        public int getCount() {
            return photos.length();
        }

    }

    public static class PlaceholderFragment extends Fragment {

        private static final String EXTRA_POSITION = "EXTRA_POSITION";
        AQuery aq;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final int position = getArguments().getInt(EXTRA_POSITION);
            final String url = getArguments().getString("url");

            View convertView = (View)inflater.inflate(R.layout.photo_slide_blank, container, false);
            aq = new AQuery(convertView);
            aq.id(R.id.imageContainer).image(url);

            return convertView;
        }

    }

    private static final class TransformerItem {

        final String title;
        final Class<? extends ViewPager.PageTransformer> clazz;

        public TransformerItem(Class<? extends ViewPager.PageTransformer> clazz) {
            this.clazz = clazz;
            title = clazz.getSimpleName();
        }

        @Override
        public String toString() {
            return title;
        }

    }
}
