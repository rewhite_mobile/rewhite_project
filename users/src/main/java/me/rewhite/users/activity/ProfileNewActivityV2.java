package me.rewhite.users.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.StringTokenizer;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.R;
import me.rewhite.users.adapter.ProfileNewAdapter;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.RewhiteLoginType;
import me.rewhite.users.layout.ProfileViewPager;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.session.RewhiteException;
import me.rewhite.users.session.RewhiteSession;
import me.rewhite.users.session.RewhiteSessionCallback;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.StringUtil;

public class ProfileNewActivityV2 extends BaseActivity {

    private final static String TAG = ProfileNewActivityV2.class.getSimpleName();
    public AQuery aq;
    public final Context mCtx = this;


    public String inputedNickname;
    public String inputedPhone;
    public String inputedEmail;
    public String inputedSharecode;

    boolean isEmailSaved = false;
    boolean isShareCodeSaved = false;

    String nickName;
    String imagePath;
    String imageThumbPath;
    String loginType;
    String emailAddress;
    String fNumber;

    private final RewhiteSessionCallback rewhiteSessionCallback = new ProfileNewActivityV2.RewhiteSessionStatusCallback();
    private RewhiteSession session;

    private ProfileNewAdapter mPagerAdapter;
    private ProfileViewPager mViewPager;


    private int mSelectedPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_new_v2);

        try{
            session = RewhiteSession.getCurrentSession();
        }catch(IllegalStateException e){
            e.printStackTrace();
            session = RewhiteSession.getInstance(mCtx);
        }
        session.addCallback(rewhiteSessionCallback);

        SharedPreferencesUtility.set(SharedPreferencesUtility.Command.IS_AUTO_LOGIN, "false");
        // Set screen name.
        mTracker.setScreenName(TAG);
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
        logger.logEvent("Profile input");


        aq = new AQuery(this);
        aq.id(R.id.text_title).text("이름 입력");

        try {
            Intent intent = getIntent();
            if (intent != null) {
                Bundle bundle = intent.getBundleExtra("bundle");
                Log.e("=== SIGN Bundle ===", bundle.toString());

                if (intent.hasExtra("loginType")) {
                    loginType = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_TYPE);
                    Log.e("loginType", loginType + "");
                } else {
                    Log.e("loginType", "loginType is NULL");
                }

                if (RewhiteLoginType.TYPE_EMAIL.equals(loginType)) {

                    if (!StringUtil.isNullOrEmpty(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL))) {
                        //aq.id(R.id.tf_email).text(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL));
                        inputedEmail = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL);
                        if (!StringUtil.isNullOrEmpty(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME))) {
                            DUtil.alertShow(this, "다른 계정에서 동일한 휴대전화번호를 인증하였습니다. 다시 인증해주세요.");
                            nickName = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME);
                            inputedNickname = nickName;
                        }else{
                            String emailaddr = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL);
                            StringTokenizer tokens = new StringTokenizer(emailaddr, "@");
                            String first = tokens.nextToken();
                            nickName = first;
                            Log.e("nickName", nickName);
                            inputedNickname = nickName;
                        }

                    } else {
                        Log.e("loginType", "THIS LOGINTYPE IS EMAIL");
                        String loginId = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_ID);
                        inputedEmail = loginId;
                        //aq.id(R.id.tf_email).text(loginId);
                        StringTokenizer tokens = new StringTokenizer(loginId, "@");
                        String first = tokens.nextToken();
                        nickName = first;
                        Log.e("nickName", nickName);
                        inputedNickname = nickName;

                        aq.id(R.id.email_area).visible().enabled(false);
                        aq.id(R.id.email_selector_icon).gone();
                        //aq.id(R.id.email_desc).gone();
                    }

                } else {
                    Log.e("loginType", "THIS LOGINTYPE IS SOCIAL");
                    try{
                        nickName = bundle.getString("nickName");
                        inputedNickname = nickName;
                        imagePath = bundle.getString("imagePath");
                        imageThumbPath = bundle.getString("imageThumbPath");
                        emailAddress = bundle.getString("email");

                        inputedNickname = nickName;
                        inputedEmail = emailAddress;
                    }catch(NullPointerException e){
                        e.printStackTrace();
                    }


//                    if (!StringUtil.isNullOrEmpty(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL))) {
//                        //aq.id(R.id.tf_email).text(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL));
//                        inputedEmail = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL);
//                        if (!StringUtil.isNullOrEmpty(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME))) {
//                            nickName = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME);
//                            inputedNickname = nickName;
//                        }
//                        DUtil.alertShow(this, "다른 계정에서 동일한 휴대전화번호를 인증하였습니다. 다시 인증해주세요.");
//                    }
                }



            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        if (RewhiteLoginType.TYPE_EMAIL.equals(loginType)) {
            isEmailSaved = true;
        }
        if (StringUtil.isNullOrEmpty(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL)) || StringUtil.isNullOrEmpty(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME))) {
            isShareCodeSaved = false;
            if("".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.REFER)) || SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.REFER) == null){
                isShareCodeSaved = false;
            }else{
                isShareCodeSaved = true;
            }
        } else {
            isShareCodeSaved = true;
        }

        aq.id(R.id.btn_back).clicked(this, "backAction");

        if (!"true".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Command.IS_TUTORIAL))) {
            Intent intent = new Intent(this, TutorialActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);

            startActivity(intent);
        }else{

        }

        initPager();
    }


    private void initPager(){
        mViewPager = (ProfileViewPager) findViewById(R.id.pager_layout);
        mPagerAdapter = new ProfileNewAdapter(getSupportFragmentManager());

        mViewPager.setPagingEnabled(false);

        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mSelectedPosition = position;

                switch (position) {
                    case 0: {
                        aq.id(R.id.text_title).text("이름 입력");
                        aq.id(R.id.indicator).image(R.mipmap.profile_indicator_01);
                        break;
                    }
                    case 1: {
                        aq.id(R.id.text_title).text("전화번호 입력");
                        aq.id(R.id.indicator).image(R.mipmap.profile_indicator_02);
                        break;
                    }
                    case 2: {
                        aq.id(R.id.text_title).text("이메일 입력");
                        aq.id(R.id.indicator).image(R.mipmap.profile_indicator_03);
                        break;
                    }
                    case 3: {
                        aq.id(R.id.text_title).text("추천코드 입력");
                        aq.id(R.id.indicator).image(R.mipmap.profile_indicator_04);
                        break;
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void finalSubmit(){
        if (StringUtil.isNullOrEmpty(inputedEmail)) {
            // 이메일주소 입력필요
            DUtil.alertShow(this, "이메일주소를 입력해주세요");
            return;
        } else if (StringUtil.isNullOrEmpty(inputedNickname)) {
            // 이름 입력필요
            DUtil.alertShow(this, "닉네임을 입력해주세요");
            return;
        }

        final ProgressDialog progressDialog = new ProgressDialog(ProfileNewActivityV2.this);
        progressDialog.setMessage("서버와 통신중입니다.");
        progressDialog.show();

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("userName", inputedNickname);
        params.put("registerShareCode", inputedSharecode);
        params.put("email", inputedEmail);
        params.put("phone", inputedPhone);
        params.put("imageBigPath", imagePath);
        params.put("imageThumbPath", imageThumbPath);
        if (StringUtil.isNullOrEmpty(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL)) || StringUtil.isNullOrEmpty(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME))) {
            params.put("isFirst", "Y");
        }
        params.put("k", 1);
        NetworkClient.post(Constants.USERINFO_MOD, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.USERINFO_MOD, error.getMessage());
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {


                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.USERINFO_MOD, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {

                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("가입및탈퇴")
                                .setAction("프로필입력완료")
                                //.setLabel(loginType)
                                .build());
                        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                        logger.logEvent("Profile inputed");

                        JSONObject userInfo = jsondata.getJSONObject("data");

                        String loginId = userInfo.getString("loginId");
                        String loginType = userInfo.getString("loginType");
                        String userId = userInfo.getInt("userId") + "";
                        String userName = userInfo.getString("userName");
                        String shareCode = userInfo.getString("shareCode");
                        String phoneno = userInfo.getString("phone");
                        String email = userInfo.getString("email");
                        String isEmailConfirm = userInfo.getString("isEmailConfirm");
                        String birth = userInfo.getString("birth");
                        String gender = userInfo.getString("gender");
                        String imageBigPath = userInfo.getString("imageBigPath");
                        String imageThumbPath = userInfo.getString("imageThumbPath");
                        String useAvailablePoint = userInfo.getString("useAvailablePoint");
                        String totalEarnedPoint = userInfo.getString("totalEarnedPoint");
                        String expectPoint = userInfo.getString("expectPoint");

                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_TYPE, loginType);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.LOGIN_ID, loginId);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.USER_ID, userId);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.USER_NAME, userName);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.SHARE_CODE, shareCode);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.EMAIL, email);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.EMAIL_AUTH_CHECK, isEmailConfirm);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.PHONE_NO, phoneno);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.BIRTH, birth);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.GENDER, gender);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.PROFILE_IMAGE, imageBigPath);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.PROFILE_THUMB, imageThumbPath);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.USE_AVAILABLE_POINT, useAvailablePoint);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.TOTAL_EARNED_POINT, totalEarnedPoint);
                        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.EXPECT_POINT, expectPoint);

                        SharedPreferencesUtility.set(SharedPreferencesUtility.Command.IS_AUTO_LOGIN, "true");

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {

                                Intent intent = new Intent();
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                intent.setClass(mCtx, MainActivity.class);
                                mCtx.startActivity(intent);
                                ((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                ((Activity) mCtx).finish();
                            }
                        }, 200);
                    } else {
                        DUtil.alertShow((Activity)mCtx, jsondata.getString("message"));
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        });
    }

    public void processNext(){
        if(mSelectedPosition < 3){
            if(mSelectedPosition == 1){
                if(isEmailSaved){
                    if(isShareCodeSaved){
                        // submit
                        finalSubmit();
                    }else{
                        mViewPager.setCurrentItem(3, false);
                    }
                }else{
                    mViewPager.setCurrentItem(2, true);
                }
            }else if(mSelectedPosition == 2){
                if(isShareCodeSaved){
                    // submit
                    finalSubmit();
                }else{
                    mViewPager.setCurrentItem(3, true);
                }
            }else{
                mViewPager.setCurrentItem(mSelectedPosition+1, true);
            }

        }else{
            // submit 처리
            finalSubmit();
        }
    }
    public void nextAction(View button){
        processNext();
    }

    public void backAction(View button) {

        if(mSelectedPosition == 0){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
            alertDialogBuilder.setCancelable(false).setMessage("로그아웃 하시겠습니까?")
                    .setPositiveButton("네", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            new Handler().postDelayed(new Runnable() {

                                @Override
                                public void run() {
                                    session.close();
                                }
                            }, 200);
                        }
                    }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else{
            if(mSelectedPosition == 3){
                if(isEmailSaved){
                    mViewPager.setCurrentItem(1, false);
                }else{
                    mViewPager.setCurrentItem(mSelectedPosition-1, true);
                }
            }else{
                mViewPager.setCurrentItem(mSelectedPosition-1, true);
            }

        }

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mCtx);
        alertDialogBuilder.setCancelable(false).setMessage("로그아웃 하시겠습니까?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                session.close();
                            }
                        }, 200);
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void saveAction(View button) {

    }


    @Override
    protected void onUserLeaveHint() {
        Log.e("outside", "onUserLeaveHint");
        // super.onUserLeaveHint();
        // impriortyPackage();
        aq.ajaxCancel();
        AjaxCallback.cancel();

        super.onUserLeaveHint();

    }

    private class RewhiteSessionStatusCallback implements RewhiteSessionCallback {

        /**
         * 세션이 오픈되었으면 가입페이지로 이동 한다.
         */
        @Override
        public void onSessionOpened() {
            // 뺑글이 종료

            // 프로그레스바를 보이고 있었다면 중지하고 세션 오픈후 보일 페이지로 이동
            ProfileNewActivityV2.this.onSessionOpened();
            DUtil.Log(TAG, "onSessionOpened");

        }

        @Override
        public void onSessionOpening() {
            // 뺑글이 시작
            DUtil.Log(TAG, "onSessionOpening");
        }

        /**
         * 세션이 삭제되었으니 로그인 화면이 보여야 한다.
         *
         * @param exception 에러가 발생하여 close가 된 경우 해당 exception
         */
        @Override
        public void onSessionClosed(final RewhiteException exception) {
            // TODO Auto-generated method stub
            ProfileNewActivityV2.this.onSessionClosed();
            DUtil.Log(TAG, "onSessionClosed = " + exception.getLocalizedMessage());

        }
    }

    protected void onSessionClosed() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, SignActivity.class);
                mCtx.startActivity(intent);
                ((Activity) mCtx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                ((Activity) mCtx).finish();
            }
        }, 200);
    }

    protected void onSessionOpened() {

    }
}
