package me.rewhite.users.activity.gsretail;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.balysv.materialripple.MaterialRippleLayout;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.R;
import me.rewhite.users.adapter.GSOrderCancelListItem;
import me.rewhite.users.adapter.GSOrderCancelRecyclerViewAdapter;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.zxing.BarcodeFormat;
import me.rewhite.users.zxing.MultiFormatWriter;
import me.rewhite.users.zxing.WriterException;
import me.rewhite.users.zxing.common.BitMatrix;

public class GSOrderReceiveCompletePopup extends BaseActivity {

    AQuery aq;
    JSONObject orderInfo;
    String orderStatus;
    boolean isAgreed = false;
    boolean isOrderCompleted = false;
    String partnerOrderNo;
    String isNewPOS;
    int boxCount;
    int orderSubType;

    RecyclerView recyclerView;
    GSOrderCancelRecyclerViewAdapter Adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gsorder_receive_popup);

        aq = new AQuery(this);

        Intent intent = getIntent();
        if(intent != null){
            try {
                orderInfo = new JSONObject(intent.getStringExtra("orderInfo"));
                orderStatus = intent.getStringExtra("orderStatus");
                orderSubType = intent.getIntExtra("orderSubType", -1);
                isNewPOS = intent.getStringExtra("isNewPOS");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        boxCount = 0;
        try {
            boxCount = orderInfo.getInt("boxQuantity");
            aq.id(R.id.text_deco_02).text(boxCount + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            partnerOrderNo = orderInfo.getString("partnerOrderNo");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        aq.id(R.id.text_deco_01).text("총 수량 ");
        aq.id(R.id.boxcount_text).text("배송박스 " + boxCount + "개");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        ArrayList<GSOrderCancelListItem> items = new ArrayList<GSOrderCancelListItem>();
        for(int i = 0; i < boxCount; i++){
            items.add(new GSOrderCancelListItem("배송박스"+(i+1), partnerOrderNo + "-" + (i+1)));
        }
        LinearLayoutManager llm = new LinearLayoutManager(GSOrderReceiveCompletePopup.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        Adapter = new GSOrderCancelRecyclerViewAdapter(items, this);
        recyclerView.setAdapter(Adapter);

        aq.id(R.id.btn_back).clicked(this, "closeAction");
        aq.id(R.id.btn_popup_close).clicked(this, "closeAction");
        aq.id(R.id.btn_close).clicked(this, "closeAction");


        setMaterialRippleLayout((View)findViewById(R.id.btn_submit));


        aq.id(R.id.btn_submit).clicked(this, "submitAction");

        if(orderSubType == 151){
            if("Y".equals(isNewPOS)){
                aq.id(R.id.btn_start).gone();
                try {
                    String orderId = orderInfo.getString("orderId");
                    setBarcodeData(orderId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                aq.id(R.id.barcode_container).gone();
                setMaterialRippleLayout((View)findViewById(R.id.btn_start));
                aq.id(R.id.btn_start).clicked(this, "startAction");
            }
        }else{
            aq.id(R.id.barcode_container).gone();
            setMaterialRippleLayout((View)findViewById(R.id.btn_start));
            aq.id(R.id.btn_start).clicked(this, "startAction");
        }

    }

    private void setBarcodeData(String orderId){
        MultiFormatWriter writer =new MultiFormatWriter();

        int blankV = 10 - orderId.length();
        String blankStr = "";
        for(int i = 0; i < blankV; i++){
            blankStr += "0";
        }

        String finaldata = Uri.encode("1" + blankStr + orderId, "utf-8");
        String outputFinalText = Uri.encode("1-" + blankStr + orderId, "utf-8");

        int barWidth = 300;
        int barHeight = 60;


        BitMatrix bm = null;
        try {
            bm = writer.encode(finaldata, BarcodeFormat.CODE_128,barWidth, barHeight);
        } catch (WriterException e) {
            e.printStackTrace();
        }


        Bitmap ImageBitmap = Bitmap.createBitmap(barWidth, barHeight, Bitmap.Config.ARGB_8888);

        for (int i = 0; i < barWidth; i++) {//width
            for (int j = 0; j < barHeight; j++) {//height
                ImageBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK: Color.WHITE);
            }
        }

        aq.id(R.id.barcode_text).text(outputFinalText);

        if (ImageBitmap != null) {
            aq.id(R.id.barcode_area).image(ImageBitmap);
            //qrcode.setImageBitmap(ImageBitmap);
        } else {
            Toast.makeText(getApplicationContext(), "바코드 생성에 실패했습니다.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void setMaterialRippleLayout(View _target){
        MaterialRippleLayout.on(_target)
                .rippleColor(Color.parseColor("#FFffff"))
                .rippleAlpha(0.2f)
                .rippleHover(true)
                .rippleOverlay(true)
                .create();
    }

    public void startAction(View button){
        aq.id(R.id.step01).gone();
        aq.id(R.id.step02).visible();
    }

    public void submitAction(View button){

        if("22".equals(orderStatus)){
            try {
                orderModAction("23");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void orderModAction(String _reqOrderStatus) throws JSONException{
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderInfo.getString("orderId"));
        params.put("orderStatus", _reqOrderStatus);
        params.put("k", 1);
        NetworkClient.post(Constants.GS_ORDER_MOD, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GS_ORDER_MOD, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GS_ORDER_MOD, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            //String orderId = jsondata.getString("data");
                            Intent resultData = new Intent();
                            //resultData.putExtra("RESULT", orderId);
                            setResult(Activity.RESULT_OK, resultData);
                            finish();
                        }
                    } else {
                        DUtil.alertShow(GSOrderReceiveCompletePopup.this, jsondata.getString("message"));
                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    public void closeAction(View button){
        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }

    private Handler handler = new Handler(){

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            // 주문취소여부 반복확인
            try {
                if(orderInfo != null){
                    getDataRefresh(orderInfo.getString("orderId"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    };

    private void getDataRefresh(String _orderId) throws JSONException {

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", _orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    //DUtil.Log(Constants.ORDER_DETAIL, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            orderInfo = jsondata.getJSONObject("data");
                            orderStatus = orderInfo.getString("orderStatus");

                            int statusCode = Integer.parseInt(orderStatus);
                            if(statusCode == 23 || statusCode == 24){
                                DUtil.Log(Constants.ORDER_DETAIL, orderStatus);
                                isOrderCompleted = true;

                                Intent resultData = new Intent();
                                //resultData.putExtra("RESULT", orderId);
                                setResult(Activity.RESULT_OK, resultData);
                                finish();
                            }

                        }
                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }


    protected void onDestroy(){
        if(thread.isAlive()){
            handler = null;
            thread.interrupt();
        }
        super.onDestroy();
    }

    Thread thread;
    protected void onResume() {

        super.onResume();

            thread = new Thread() {
                public void run() {
                    while (!isOrderCompleted) {
                        if(!thread.isInterrupted()){
                            try {
                                sleep(3000);
                            } catch (Exception e) {
                                // TODO: handle exception
                                e.printStackTrace();
                            }
                            if(handler != null){
                                handler.sendEmptyMessage(0);
                            }

                        }
                    }
                }
            };
            thread.start();



    }
}
