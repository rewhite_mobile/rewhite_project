package me.rewhite.users.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.session.AccessTokenRequest;
import me.rewhite.users.session.RewhiteException;
import me.rewhite.users.session.RewhiteSession;
import me.rewhite.users.session.RewhiteSessionCallback;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.StringUtil;

public class TermsAgreeActivity extends BaseActivity{

    private final static String TAG = SignActivity.class.getSimpleName();
    private AQuery aq;
    public SharedPreferences preferences;
    public Context ctx = this;
    private final RewhiteSessionCallback rewhiteSessionCallback = new RewhiteSessionStatusCallback();
    private RewhiteSession session;
    public boolean isLoginProcessing = false;
    ProgressDialog mProgressDialog;


    boolean isGoogleConnected = false;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (session.handleActivityResult(requestCode,
                resultCode, data)) {
            Log.d(TAG, "onActivityResult : RewhiteSession");
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void closeClicked(View button) {
        finish();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_agree);

        AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
        logger.logEvent("Try join a member");

        session = RewhiteSession.getInstance(ctx);
        session.addCallback(rewhiteSessionCallback);

        // Set screen name.
        mTracker.setScreenName(TAG);
        // Send a screen view.
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        preferences = getSharedPreferences(Constants.PREF_NAME,
                Context.MODE_PRIVATE);
        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        aq.id(R.id.btn_all_agree).clicked(this, "termsAgreeClicked");
        aq.id(R.id.btn_next).clicked(this, "nextClicked");

        aq.id(R.id.checkbox_01).clicked(this, "checkboxClick").tag("check01");
        aq.id(R.id.checkbox_02).clicked(this, "checkboxClick").tag("check02");
        aq.id(R.id.checkbox_03).clicked(this, "checkboxClick").tag("check03");
        aq.id(R.id.checkbox_04).clicked(this, "checkboxClick").tag("check04");
        aq.id(R.id.checkbox_05).clicked(this, "checkboxClick").tag("check05");

        aq.id(R.id.terms_detail_1).clicked(this, "detailClick").tag("check01");
        aq.id(R.id.terms_detail_2).clicked(this, "detailClick").tag("check02");
        aq.id(R.id.terms_detail_3).clicked(this, "detailClick").tag("check03");
        aq.id(R.id.terms_detail_4).clicked(this, "detailClick").tag("check04");
        aq.id(R.id.terms_detail_5).clicked(this, "detailClick").tag("check05");
    }

    public void detailClick(View Button) {

        Log.i("checkbox", Button.getTag().toString());
        Intent intent;
        if ("check01".equals(Button.getTag().toString())) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("가입및탈퇴")
                    .setAction("약관조회")
                    .setLabel("이용약관")
                    .build());

            intent = new Intent(this, TermsDetailView.class);
            intent.putExtra("TERMS_TITLE", "이용약관");
            intent.putExtra("TERMS_URI", Constants.TERMS_USE);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if ("check02".equals(Button.getTag().toString())) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("가입및탈퇴")
                    .setAction("약관조회")
                    .setLabel("개인정보취급방침")
                    .build());

            intent = new Intent(this, TermsDetailView.class);
            intent.putExtra("TERMS_TITLE", "개인정보 취급방침");
            intent.putExtra("TERMS_URI", Constants.TERMS_PRIVACY);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if ("check03".equals(Button.getTag().toString())) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("가입및탈퇴")
                    .setAction("약관조회")
                    .setLabel("개인정보 제3자 제공동의")
                    .build());

            intent = new Intent(this, TermsDetailView.class);
            intent.putExtra("TERMS_TITLE", "개인정보 제3자 제공동의");
            intent.putExtra("TERMS_URI", Constants.TERMS_PRIVATE_SUPPORT);

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if ("check04".equals(Button.getTag().toString())) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("가입및탈퇴")
                    .setAction("약관조회")
                    .setLabel("위치기반 서비스 이용약관")
                    .build());
            intent = new Intent(this, TermsDetailView.class);
            intent.putExtra("TERMS_TITLE", "위치기반 서비스 이용약관");
            intent.putExtra("TERMS_URI", Constants.TERMS_LOCATION);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } else if ("check05".equals(Button.getTag().toString())) {
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("가입및탈퇴")
                    .setAction("약관조회")
                    .setLabel("개인정보 제3자 제공동의 (선택)")
                    .build());
            intent = new Intent(this, TermsDetailView.class);
            intent.putExtra("TERMS_TITLE", "개인정보 제3자 제공동의 (선택)");
            intent.putExtra("TERMS_URI", Constants.TERMS_PRIVATE_THIRDS_SUPPORT);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    private boolean isChecked01 = false;
    private boolean isChecked02 = false;
    private boolean isChecked03 = false;
    private boolean isChecked04 = false;
    private boolean isChecked05 = false;

    public void checkboxClick(View Button) {

        Log.i("checkbox", Button.getTag().toString());

        if ("check01".equals(Button.getTag().toString())) {

            if (isChecked01) {
                isChecked01 = false;
                aq.id(R.id.checkbox_01).image(R.drawable.checkbox_off_term);
            } else {
                isChecked01 = true;
                aq.id(R.id.checkbox_01).image(R.drawable.checkbox_term);
            }

        } else if ("check02".equals(Button.getTag().toString())) {
            if (isChecked02) {
                isChecked02 = false;
                aq.id(R.id.checkbox_02).image(R.drawable.checkbox_off_term);
            } else {
                isChecked02 = true;
                aq.id(R.id.checkbox_02).image(R.drawable.checkbox_term);
            }
        } else if ("check03".equals(Button.getTag().toString())) {
            if (isChecked03) {
                isChecked03 = false;
                aq.id(R.id.checkbox_03).image(R.drawable.checkbox_off_term);
            } else {
                isChecked03 = true;
                aq.id(R.id.checkbox_03).image(R.drawable.checkbox_term);
            }
        } else if ("check04".equals(Button.getTag().toString())) {
            if (isChecked04) {
                isChecked04 = false;
                aq.id(R.id.checkbox_04).image(R.drawable.checkbox_off_term);
            } else {
                isChecked04 = true;
                aq.id(R.id.checkbox_04).image(R.drawable.checkbox_term);
            }
        }else if ("check05".equals(Button.getTag().toString())) {
            if (isChecked05) {
                isChecked05 = false;
                aq.id(R.id.checkbox_05).image(R.drawable.checkbox_off_term);
            } else {
                isChecked05 = true;
                aq.id(R.id.checkbox_05).image(R.drawable.checkbox_term);
            }
        }
/*
        if (checkAllAgreed()) {
            allAgreed();
        }
*/
    }

    public boolean checkAllAgreed() {
        if (isChecked01 && isChecked02 && isChecked03 && isChecked04) {
            return true;
        } else {
            //MZError.alertWithErrorMessage(this, "약관에 동의해주세요");
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    ctx);
            alertDialogBuilder.setMessage(
                    "약관에 동의해주세요.")
                    .setCancelable(true)
                    .setNegativeButton("확인", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            return false;
        }
    }

    Bundle bundle;

    public void allAgreed() {

        Intent intent = getIntent();
        bundle = intent.getBundleExtra("bundle");
        final String loginType = intent.getStringExtra("loginType");
        final String loginId = bundle.getString("loginId");
        final String secretKey = bundle.getString("secretKey");
        String nickName = bundle.getString("nickName");
        String imagePath = bundle.getString("imagePath");

        RequestParams params = new RequestParams();
        params.put("loginId", loginId);
        params.put("loginType", loginType);
        params.put("secretKey", secretKey);

        params.put("nickName", nickName);
        params.put("imagePath", imagePath);

        if(isChecked05){
            params.put("isAgreeThirdOptinal", "Y");
        }else{
            params.put("isAgreeThirdOptinal", "N");
        }

        params.put("deviceToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));

        params.put("k", 1);
        NetworkClient.post(Constants.MEMBER_REGISTER, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    JSONObject jsondata = new JSONObject(result);
                    //JSONObject json = jsondata.getJSONObject("data");

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("가입및탈퇴")
                                .setAction("가입완료")
                                .setLabel(loginType)
                                .build());
                        AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
                        logger.logEvent("Join completed");

                        requestSessionOpen(ctx, loginId, secretKey, loginType);
                    } else {
                        Log.i(TAG, "가입실패 : " + result);
                    }

                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();

                }
            }
        });

    }

    public void nextClicked(View Button) {
        if (checkAllAgreed()) {
            allAgreed();
        }

    }

    public void termsAgreeClicked(View Button) {

        isChecked01 = true;
        aq.id(R.id.checkbox_01).image(R.drawable.checkbox_term);
        isChecked02 = true;
        aq.id(R.id.checkbox_02).image(R.drawable.checkbox_term);
        isChecked03 = true;
        aq.id(R.id.checkbox_03).image(R.drawable.checkbox_term);
        isChecked04 = true;
        aq.id(R.id.checkbox_04).image(R.drawable.checkbox_term);
        isChecked05 = true;
        aq.id(R.id.checkbox_05).image(R.drawable.checkbox_term);

    }

    private class RewhiteSessionStatusCallback implements RewhiteSessionCallback {

        /**
         * 세션이 오픈되었으면 가입페이지로 이동 한다.
         */
        @Override
        public void onSessionOpened() {
            // 뺑글이 종료

            // 프로그레스바를 보이고 있었다면 중지하고 세션 오픈후 보일 페이지로 이동
            TermsAgreeActivity.this.onSessionOpened();
            DUtil.Log(TAG, "onSessionOpened");

        }

        @Override
        public void onSessionOpening() {
            // 뺑글이 시작
            isLoginProcessing = true;
            DUtil.Log(TAG, "onSessionOpening");
        }

        /**
         * 세션이 삭제되었으니 로그인 화면이 보여야 한다.
         *
         * @param exception 에러가 발생하여 close가 된 경우 해당 exception
         */
        @Override
        public void onSessionClosed(final RewhiteException exception) {
            // TODO Auto-generated method stub
            TermsAgreeActivity.this.onSessionClosed();

            DUtil.Log(TAG, "onSessionClosed = " + exception.getLocalizedMessage());

            isLoginProcessing = false;
            if (mProgressDialog != null) mProgressDialog.dismiss();


        }
    }

    public void requestSessionOpen(Context ctx, String loginId, String secretKey, String loginType) {
        DUtil.Log(TAG, "requestSessionOpen Start :::: " + loginId + " / " + secretKey + " / " + loginType);
        /*
        if(mProgressDialog == null){
            mProgressDialog = new ProgressDialog(Bootstrap.getCurrentActivity());
            mProgressDialog.setMessage("회원가입 처리중...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }*/

        isLoginProcessing = true;

        AccessTokenRequest aRequest = AccessTokenRequest.createRequestWithDeviceToken(ctx, loginId, secretKey, loginType, SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.GCM_ID));
        session.open(aRequest);
    }

    protected void onSessionClosed() {
    }

    protected void onSessionOpened() {

        if("S0100".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_RESULT_CODE))){
            // 휴면예정
        }else if("S0101".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.LOGIN_RESULT_CODE))){
            // 휴면처리 -> 정상 전환
        }else{
            onSessionOpenedAfter();
        }

    }
    private void onSessionOpenedAfter(){
        String userName = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.USER_NAME);
        String email = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.EMAIL);
        String mobile = SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.PHONE_NO);
        if (StringUtil.isNullOrEmpty(userName) || StringUtil.isNullOrEmpty(mobile) || StringUtil.isNullOrEmpty(email)) {
            // 필수정보 미입력시
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    isLoginProcessing = false;
                    if (mProgressDialog != null) mProgressDialog.dismiss();

                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setClass(ctx, ProfileNewActivityV2.class);
                    intent.putExtra("bundle", bundle);
                    ctx.startActivity(intent);
                    ((Activity) ctx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    ((Activity) ctx).finish();
                }
            }, 500);
        } else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    isLoginProcessing = false;
                    if (mProgressDialog != null) mProgressDialog.dismiss();

                    Intent intent = new Intent();
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setClass(ctx, MainActivity.class);
                    ctx.startActivity(intent);
                    ((Activity) ctx).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    ((Activity) ctx).finish();
                }
            }, 500);
        }
    }
}
