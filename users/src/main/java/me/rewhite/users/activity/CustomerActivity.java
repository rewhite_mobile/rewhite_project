package me.rewhite.users.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

import com.androidquery.AQuery;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;

public class CustomerActivity extends BaseActivity {

    AQuery aq;
    private Context ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        aq = new AQuery(this);
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
        aq.id(R.id.notice_layout).clicked(this, "htmlClicked");
        aq.id(R.id.faq_layout).clicked(this, "faqClicked");

        aq.id(R.id.help_layout).clicked(this, "helpClicked");
        aq.id(R.id.btn_yellow).clicked(this, "yellowClicked");
        aq.id(R.id.qna_layout).clicked(this, "qnaClicked");
        aq.id(R.id.about_layout).clicked(this, "aboutClicked");
        aq.id(R.id.alarm_layout).clicked(this, "alarmClicked");

        //aq.id(R.id.btn_terms).clicked(this, "termsClicked").tag(0);
        //aq.id(R.id.btn_privacy).clicked(this, "termsClicked").tag(1);

        PackageManager m = getPackageManager();
        // 설치된 패키지의 버전이름 추출 : 1.0.0 스타일
        String app_ver = "";
        try {
            app_ver = m.getPackageInfo(getPackageName(), 0).versionName;
            aq.id(R.id.text_version).text(getString(R.string.app_name)+" v" + app_ver);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void closeClicked(View button) {
        finish();
    }

    public void yellowClicked(View button) {

        Intent intent = new Intent(this, HtmlActivity.class);
        intent.putExtra("TERMS_URI", Constants.HTML_YELLOW);
        intent.putExtra("TITLE_NAME", "플러스친구");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    public void helpClicked(View button) {
        Intent intent = new Intent(this, TutorialActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    public void aboutClicked(View button) {
        Intent intent = new Intent(this, AboutActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    public void qnaClicked(View button) {
        Intent intent = new Intent(this, QNAActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    public void faqClicked(View button) {
        Intent intent = new Intent(this, FAQActivity.class);
        intent.putExtra("section", 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    public void eventClicked(View button) {
        Intent intent = new Intent(this, EventListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    public void htmlClicked(View button) {
        Intent intent = new Intent(this, HtmlActivity.class);
        intent.putExtra("TERMS_URI", Constants.HTML_NOTICE);
        intent.putExtra("TITLE_NAME", getString(R.string.title_notice));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    public void alarmClicked(View button){
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setClass(this, AlarmSettingActivity.class);
        startActivity(intent);
        //ra.overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
    }
}
