package me.rewhite.users.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.PickupType;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class QuickOrderActivity extends BaseActivity {

    Context mCtx = this;
    AQuery aq;
    JSONArray totalStoreInfo;
    JSONObject storeInfo;
    String addressSeq;
    String addressName;
    String addressDetailName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_order);

        Intent intent = getIntent();
        if (intent != null) {
            try {
                totalStoreInfo = new JSONArray(intent.getStringExtra("storeInfo"));

                addressSeq = intent.getStringExtra("addressSeq");
                addressDetailName = intent.getStringExtra("addressDetailName");
                addressName = intent.getStringExtra("addressName");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        try {
            for(int i =0; i < totalStoreInfo.length(); i++){
                if(totalStoreInfo.getJSONObject(i).getInt("storeId") == 1015){
                    storeInfo = totalStoreInfo.getJSONObject(i);
                    Log.e("storeInfo", storeInfo.toString());
                }else{
                    getStoreInfo("1015");
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        aq = new AQuery(this);
        aq.id(R.id.btn_price_today).clicked(this, "priceTodayClicked");
        aq.id(R.id.btn_price_tomorrow).clicked(this, "priceTomorrowClicked");

        aq.id(R.id.btn_order_today).clicked(this, "orderQuickToday");
        aq.id(R.id.btn_order_tomorrow).clicked(this, "orderQuickTomorrow");
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
    }

    public void closeClicked(View button) {
        finish();
    }


    public void orderQuickToday(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("주문")
                        .setAction("당일세탁주문")
                        //.setLabel("")
                        .build());
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, OrderActivity.class);
                intent.putExtra("storeInfo", storeInfo.toString());
                intent.putExtra("addressSeq", addressSeq);
                intent.putExtra("addressDetailName", addressDetailName);
                intent.putExtra("addressName", addressName);
                intent.putExtra("pickupMode", PickupType.PICKUPTYPE_TODAY);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
            }
        }, 100);
    }

    public void orderQuickTomorrow(View button){
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("주문")
                        .setAction("익일세탁주문")
                        //.setLabel("")
                        .build());
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, OrderActivity.class);
                intent.putExtra("storeInfo", storeInfo.toString());
                intent.putExtra("addressSeq", addressSeq);
                intent.putExtra("addressDetailName", addressDetailName);
                intent.putExtra("addressName", addressName);
                intent.putExtra("pickupMode", PickupType.PICKUPTYPE_TOMORROW);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
            }
        }, 100);
    }

    private void getStoreInfo(String storeId){
        //SHOW_STORE_INFO
        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("storeId", storeId);
        params.put("k", 1);
        NetworkClient.post(Constants.SHOW_STORE_INFO, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.SHOW_STORE_INFO, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.SHOW_STORE_INFO, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //

                        } else {
                            storeInfo = jsondata.getJSONObject("data");
                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void priceTodayClicked(View button){
        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("기타")
                .setAction("당일세탁요금표")
                //.setLabel("이메일")
                .build());

        Intent intent = new Intent(this, PriceActivity.class);
        try {
            intent.putExtra("storeId", storeInfo.getString("storeId"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        intent.putExtra("pickupMode", PickupType.PICKUPTYPE_TODAY);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    public void priceTomorrowClicked(View button){
        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("기타")
                .setAction("익일세탁요금표")
                //.setLabel("이메일")
                .build());

        Intent intent = new Intent(this, PriceActivity.class);
        try {
            intent.putExtra("storeId", storeInfo.getString("storeId"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        intent.putExtra("pickupMode", PickupType.PICKUPTYPE_TOMORROW);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }
}
