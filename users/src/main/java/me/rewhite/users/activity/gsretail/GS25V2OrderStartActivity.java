package me.rewhite.users.activity.gsretail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.Constants;
import me.rewhite.users.common.GSBaseActivity;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class GS25V2OrderStartActivity extends GSBaseActivity {

    private static final String TAG = "GS25V2OrderStartActivity";
    private Context mContext;
    private AQuery aq;
    int orderSubType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gs25_v2_order_start);

        Intent intent = getIntent();
        if(intent != null){
            orderSubType = intent.getIntExtra("orderSubType", 151);
            Log.e("orderSubType", "orderSubType : " + orderSubType);
        }

        aq = new AQuery(this);

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        mContext = this;
        aq.id(R.id.comp_layout).gone();
        aq.id(R.id.btn_close).clicked(this, "closeClicked");
        aq.id(R.id.btn_start).clicked(this, "gsStartOrderAction");
    }

    public void closeClicked(View button) {
        finish();
    }

    public void gsStartOrderAction(View button){

        // GS25 POS 미연동
//        IntentIntegrator integrator = new IntentIntegrator(this);
//        integrator.initiateScan();

        /* GS25 POS 연동버전 코드 */
        if(orderSubType == 151){
            // GS주문인경우
            // 바코드 출력 및 QR코드 Both 제공

            Intent inputIntent = new Intent();
            inputIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            inputIntent.setClass(mContext, GS25V3OrderAgreeActivity.class);
            //inputIntent.setClass(mContext, GS25V3OrderBarcodeActivity.class);

            startActivity(inputIntent);
        }else{
            IntentIntegrator integrator = new IntentIntegrator(this);
            integrator.initiateScan();
        }

    }

    String internalStoreId = "";

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            // handle scan result
            if(scanResult.getContents() != null){

                Log.e("barcode scan result", scanResult.getFormatName() + " / " + scanResult.getContents());

                if("QR_CODE".equals(scanResult.getFormatName())){

                    try{
                        final String decodedString = new String(Base64.decode(scanResult.getContents(), Base64.DEFAULT), "UTF-8");
                        internalStoreId = decodedString;
                        RequestParams params = new RequestParams();
                        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                        params.put("internalStoreId", decodedString);
                        params.put("k", 1);

                        Log.e("GS_STORE_VALID REQUEST", params.toString());

                        NetworkClient.post(Constants.GS_STORE_VALID, params, new AsyncHttpResponseHandler() {

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                                // TODO Auto-generated method stub
                                DUtil.Log(Constants.GS_STORE_VALID, error.getMessage());
                            }

                            @Override
                            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                                String result;
                                try {
                                    result = new String(data, "UTF-8");
                                    DUtil.Log(Constants.GS_STORE_VALID, result);

                                    final JSONObject jsondata = new JSONObject(result);

                                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                                        aq.id(R.id.comp_layout).visible();

                                        if("1".equals(decodedString.substring(0,1))){
                                            aq.id(R.id.comp_image).image(R.mipmap.img_gs25_v2_scanned_completed);
                                        }else{
                                            aq.id(R.id.comp_image).image(R.mipmap.img_theplace_v2_scanned_completed);
                                        }

                                        String storeName = jsondata.getJSONObject("data").getString("partnerStoreName");
                                        String text = "<font color=#909090> 고객님이 맡기시는 곳은 </font><BR><font color=#ffffff><b> "+storeName+" </b></font><font color=#909090> 입니다.</font><BR><BR><font color=#909090><small>'2초후에 화면이 자동이동합니다.'</small></font>";
                                        //textView.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
                                        TextView tv = (TextView)findViewById(R.id.store_info_text);
                                        tv.setText(Html.fromHtml(text));

                                        new Handler().postDelayed(new Runnable() {

                                            @Override
                                            public void run() {

                                                try {
                                                    Intent inputIntent = new Intent();
                                                    inputIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                    inputIntent.setClass(mContext, GS25V2OrderInputActivity.class);
                                                    inputIntent.putExtra("storeInfo", jsondata.getJSONObject("data").toString());
                                                    inputIntent.putExtra("internalStoreId", internalStoreId );
                                                    inputIntent.putExtra("partnerId", jsondata.getJSONObject("data").getInt("partnerId") + "");
                                                    startActivity(inputIntent);
                                                    finish();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        }, 1500);

                                    } else {
                                        if(orderSubType == 151){
                                            DUtil.alertShow(GS25V2OrderStartActivity.this, "이용가능한 GS25지점에서 QR코드를 촬영해주세요.");
                                        }else{
                                            DUtil.alertShow(GS25V2OrderStartActivity.this, "이용가능한 플레이스에서 QR코드를 촬영해주세요.");
                                        }

                                    }
                                } catch (UnsupportedEncodingException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                } catch (JSONException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }

                            }
                        });
                    }catch(UnsupportedEncodingException e){
                        e.printStackTrace();
                    }

                }

            }

        }
    }
}
