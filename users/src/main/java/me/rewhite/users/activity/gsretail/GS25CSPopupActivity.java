package me.rewhite.users.activity.gsretail;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.activity.HtmlActivity;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;

public class GS25CSPopupActivity extends BaseActivity {

    AQuery aq;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gs25_cspopup);

        aq = new AQuery(this);
        mContext = this;

        aq.id(R.id.btn_close).clicked(this, "closeAction");
        aq.id(R.id.btn_call).clicked(this, "callAction");
        aq.id(R.id.btn_kakao).clicked(this, "kakaoAction");
    }

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.CALL_PHONE
    };

    private static final int INITIAL_REQUEST = 1337;


    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }
    private boolean canAccessCall() {
        return(hasPermission(Manifest.permission.CALL_PHONE));
    }
    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case INITIAL_REQUEST:
                if (canAccessCall()) {

                }
                else {
                    Toast.makeText(this, "전화걸기 권한을 거절하셔서 통화연결이 불가합니다.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void closeAction(View button){
        finish();
    }

    public void callAction(View button){
        if (Build.VERSION.SDK_INT >= 23) {
            if (!canAccessCall()) {
                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
            } else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setCancelable(true)
                        .setMessage("리화이트 고객센터와 전화통화를 원하십니까?")
                        .setPositiveButton("전화걸기", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                //callIntent.setAction("android.intent.action.DIAL");

                                callIntent.setData(Uri.parse("tel:15442951"));
                                if(callIntent.resolveActivity(getPackageManager()) != null){
                                    startActivity(callIntent);
                                }
                            }})
                        .setNegativeButton("아니오", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        } else {
            call();
        }
    }

    public void call() {
        Tracker t = ((Bootstrap)getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("GS주문")
                .setAction("고객센터 전화")
                //.setLabel("Order")
                .build());
        AppEventsLogger logger = AppEventsLogger.newLogger(mContext);
        logger.logEvent("Call CSCenter");

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:15442951"));
        if(callIntent.resolveActivity(getPackageManager()) != null){
            startActivity(callIntent);
        }
    }

    public void kakaoAction(View button){
        Intent intent = new Intent(this, HtmlActivity.class);
        intent.putExtra("TERMS_URI", Constants.HTML_YELLOW);
        intent.putExtra("TITLE_NAME", "플러스친구");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }
}
