package me.rewhite.users.activity.gsretail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.androidquery.AQuery;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.ImageUtil;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.TimeUtil;
import me.rewhite.users.zxing.WriterException;

public class GS25OrderCompleteActivity extends AppCompatActivity{

    private static final String TAG = "GS25OrderCompleteActivity";
    private Context mContext;
    private AQuery aq;
    ImageButton button = null;

    JSONObject orderInfo;
    String partnerId = "7";
    String partnerStoreId = "1002";
    String partnerStoreName = "가재울아이파크점";
    String storeId = "1002";
    String pickupMode = "GS";

    String storeName = "";
    long pickupDateTimeStamp;
    String pickupDate = "";
    long completeDateTimestamp;
    String completeDate = "";


    String orderId = "GS-10301-4";
    String partnerOrderNo = "";

    int count_unit = 0;
    int count_pack = 0;
    String request_content_string = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gs25_order_completed);

        aq = new AQuery(this);

        Tracker t = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        // Set screen name.
        t.setScreenName(TAG);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        mContext = this;

        Intent intent = getIntent();
        if(intent != null){
            orderId = intent.getStringExtra("orderId");
            getOrderInfo(orderId);
        }

        aq.id(R.id.btn_drawer).clicked(this, "submitClicked");
        aq.id(R.id.btn_close).clicked(this, "closeClicked");
    }

    public void closeClicked(View button) {
        finish();
    }

    private void getOrderInfo(String orderId){

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("orderId", orderId);
        params.put("k", 1);
        NetworkClient.post(Constants.ORDER_DETAIL, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.ORDER_DETAIL, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.ORDER_DETAIL, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            // 주문내역이 없습니다
                        } else {
                            orderInfo = jsondata.getJSONObject("data");
                            // 해당 편의점ID로 매칭된 세탁소정보 조회
                            storeId = orderInfo.getString("storeId");

                            try {
                                partnerOrderNo = orderInfo.getString("partnerOrderNo");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                Log.i("barcode pixels", aq.id(R.id.barcode_container).getView().getWidth() + " / " + aq.id(R.id.barcode_container).getView().getHeight());
                                aq.id(R.id.barcode_text).text(partnerOrderNo);
                                aq.id(R.id.barcode_image).image(ImageUtil.createBarcodeBitmap(partnerOrderNo, 1400, 200));
                            } catch (WriterException e) {
                                e.printStackTrace();
                            }

                            partnerStoreName = orderInfo.getJSONObject("partnerStoreV3").getString("partnerStoreName");
                            if(orderInfo.isNull("pickupRequestMessage")){
                                request_content_string = "주문시 요청사항 없음";
                            }else{
                                if("".equals(orderInfo.getString("pickupRequestMessage"))){
                                    request_content_string = "주문시 요청사항 없음";
                                }else{
                                    request_content_string = orderInfo.getString("pickupRequestMessage");
                                }
                            }

                            pickupDateTimeStamp = orderInfo.getLong("pickupRequestTimeApp");
                            pickupDate = TimeUtil.convertTimestampToGSString(pickupDateTimeStamp);
                            completeDateTimestamp = orderInfo.getLong("deliveryRequestTimeApp");
                            completeDate = TimeUtil.convertTimestampToGSString(completeDateTimestamp);
                            count_unit = orderInfo.getInt("pickupQuantity");
                            count_pack = orderInfo.getInt("packQuantity");

                            aq.id(R.id.conv_store_name).text(partnerStoreName);
                            aq.id(R.id.text_unit_count).text(count_unit + "");
                            aq.id(R.id.text_pack_count).text(count_pack + "");

                            String tempRCS = request_content_string;
                            if(request_content_string.length() > 15){
                                tempRCS = request_content_string.substring(0, 15) + "...";
                            }
                            aq.id(R.id.text_request_message).text(tempRCS);
                            aq.id(R.id.text_complete_date).text(completeDate);


                        }

                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });


    }

    private void submitCompleteProcess(JSONObject req){
        Intent intent = new Intent(this, GSOrderNoticeScreen.class);

    }

    public void submitClicked(View button){
        finish();
    }

}
