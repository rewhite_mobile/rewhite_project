package me.rewhite.users.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.MZValidator;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.ValidateUtil;

public class PhoneAuthActivity extends BaseActivity {

    ProgressDialog dialog;
    private final static String TAG = PhoneAuthActivity.class.getSimpleName();
    public AQuery aq;
    public final Context mCtx = this;

    public String VARS_MOBILE_NUMBER = "";
    public String VARS_AUTH_NUMBER = "";

    private TelephonyManager telephonyManager;
    int authProcessStatus = 0; // Ready
    private boolean isReceiveMode = false;
    final public int countdownSec = 180;
    private static final int SEND_THREAD_INFOMATION = 0;
    private static final int SEND_THREAD_STOP_MESSAGE = 1;

    private SendMassgeHandler mMainHandler = null;
    private CountThread mCountThread = null;

    protected static final String LOG_TAG = "SMSReceiver";
    private static final String ACTION = "android.provider.Telephony.SMS_RECEIVED";
    SmsMessage currentMessage;
    String message = "", sms_message = "";

    BroadcastReceiver mReceiver;
    IntentFilter filter;
    private Intent smsIntent;
    String fNumber;
    String number_1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_auth);

        aq = new AQuery(this);
        setLayout();
        mMainHandler = new SendMassgeHandler();

        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.loading_comment));

        aq.id(R.id.btn_auth).clicked(this, "phoneAuthRequest");
        aq.id(R.id.tf_phone_no).typeface(CommonUtility.getNanumBarunTypeface());
        aq.id(R.id.btn_next).clicked(this, "phoneAuthSubmit");
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        try
        {
            if ( Build.VERSION.SDK_INT >= 23){
                if (!canAccessPermission()) {
                    requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
                }else{
                    setReceiver();
                    // 메인 핸들러 생성

                    number_1 = telephonyManager.getLine1Number();
                    String phoneNumber = number_1.replace("+82", "0");
                    Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                    fNumber = MZValidator.validTelNumber(phoneNumber);
                    aq.id(R.id.tf_phone_no).text(fNumber).typeface(CommonUtility.getNanumBarunTypeface());
                }
            }else{
                setReceiver();
                // 메인 핸들러 생성

                number_1 = telephonyManager.getLine1Number();
                String phoneNumber = number_1.replace("+82", "0");
                Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                fNumber = MZValidator.validTelNumber(phoneNumber);
                aq.id(R.id.tf_phone_no).text(fNumber).typeface(CommonUtility.getNanumBarunTypeface());
            }

        }
        catch(NullPointerException ex)
        {
            setReceiver();
            // 메인 핸들러 생성

            fNumber = "";
            aq.id(R.id.tf_phone_no).text(fNumber).typeface(CommonUtility.getNanumBarunTypeface());
        }

    }

    public void closeClicked(View button) {
        finish();
    }

    private static final String[] INITIAL_PERMS={
            Manifest.permission.READ_SMS,
            Manifest.permission.READ_PHONE_STATE
    };
    private static final String[] SMS_PERMS={
            Manifest.permission.READ_SMS,
            Manifest.permission.READ_PHONE_STATE
    };
    private static final int INITIAL_REQUEST=1337;
    private static final int SMS_REQUEST=INITIAL_REQUEST+3;

    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch(requestCode) {

            case SMS_REQUEST:
                if (canAccessPermission()) {
                    setReceiver();
                    // 메인 핸들러 생성

                    number_1 = telephonyManager.getLine1Number();
                    String phoneNumber = number_1.replace("+82", "0");
                    Log.i("phoneNumber", phoneNumber + "/naccessToken : " + SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
                    fNumber = MZValidator.validTelNumber(phoneNumber);
                    aq.id(R.id.tf_phone_no).text(fNumber).typeface(CommonUtility.getNanumBarunTypeface());
                }
                else {
                    Toast.makeText(this, "핸드폰번호를 알수 없습니다.", Toast.LENGTH_LONG).show();
                    finish();
                }
                break;
        }
    }
    private boolean canAccessPermission() {
        return(hasPermission(Manifest.permission.READ_SMS));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
    }

    // Handler 클래스
    class SendMassgeHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case SEND_THREAD_INFOMATION:
                    tv_Count.setText("" + msg.obj);
                    // aq.id(R.id.validtime).text(""+msg.obj);
                    break;

                case SEND_THREAD_STOP_MESSAGE:
                    mCountThread.stopThread();
                    tv_Count.setText("입력시간초과");
                    //Toast.makeText(mCtx, "입력시간이 초과되었습니다", Toast.LENGTH_SHORT).show();

                    break;

                default:
                    break;
            }
        }

    }

    // Thread 클래스
    class CountThread extends Thread implements Runnable {

        private boolean isPlay = false;
        private int i = countdownSec;

        public CountThread() {
            this.isPlay = true;
        }

        public void isThreadState(boolean isPlay) {
            this.isPlay = isPlay;
        }

        public void resetThread() {
            i = countdownSec;
            this.isPlay = true;
        }

        public void stopThread() {
            this.isPlay = false;
        }

        @Override
        public void run() {
            super.run();

            while (isPlay) {

                String outputSec = (i % 60 < 10) ? "0" + i % 60 : "" + i % 60;
                int outputMin = i / 60;
                String output = outputMin + ":" + outputSec;
                // Log.i("CountThread RUN" , i + "");

                // 메시지 얻어오기
                Message msg = mMainHandler.obtainMessage();
                // 메시지 ID 설정
                msg.what = SEND_THREAD_INFOMATION;

                // 메시지 정보 설정3 (Object 형식)
                msg.obj = output;
                mMainHandler.sendMessage(msg);

                i--;
                if (i <= 0) {
                    mMainHandler.sendEmptyMessage(SEND_THREAD_STOP_MESSAGE);
                    isPlay = false;
                    i = countdownSec;
                }

                // 1초 딜레이
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    //Mint.logException(e);
                }
            }
        }
    }

    private void setReceiver() {
        if (filter == null) {
            filter = new IntentFilter();
            filter.addAction(ACTION);
            mReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    //do something based on the intent's action
                    DUtil.Log("Smsintent recieved: ", intent.getAction());

                    //preferences = context.getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);

                    Log.i(LOG_TAG, "[inside onReceive]");
                    String fromCS = "";

                    if (intent.getAction().equals(ACTION)) {
                        // 받은 SMS의 내용을 가져오기 위해 StringBuilder생성
                        // 수신받은 Intent의 data를 가져오기 위해 getExtra로 bundle로 집어넣음
                        Bundle bundle = intent.getExtras();

                        if (bundle != null) {
                            // bundle에서 'pdus'를 key로 하는 값들을 가져옴
                            Object[] pdusObj = (Object[]) bundle.get("pdus");

                            // 가져온 object의 갯수만큼 class 생성
                            SmsMessage[] messages = new SmsMessage[pdusObj.length];

                            // object의 내용을 생성된 smsmessage에 할당
                            for (int i = 0; i < pdusObj.length; i++) {
                                messages[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                            }

                            // 추출된 smsmessage들을 toast로 보여주기 위해 조립
                            for (SmsMessage currentMessage : messages) {
                                fromCS = currentMessage.getDisplayOriginatingAddress();
                                message = currentMessage.getDisplayMessageBody();
                                Log.i(LOG_TAG, "" + message);
                            }
                        }

                        DUtil.Log("fromCS", fromCS);
                        DUtil.Log("message", message);

                        message = message.replace("[Web발신]", "");

						/*
                         * 핸드폰인증번호 전송 1544-2951, 02-707-2429 등록됨
						 */
                        if (Constants.SMS_SENDER_NO.equals(fromCS) || "027072429".equals(fromCS)) {

                            if (message.contains(context.getResources().getString(R.string.app_sms_pattern))) {
                                if (message.contains("[")) {
                                    // 모바일앱에서의 가입요청시 () 이 아닌 <> 으로
                                    // 숫자가 둘러쌓임.

                                    //StringTokenizer tokens = new StringTokenizer(message, " : ");
                                    //tokens.nextToken();
                                    String temp1 = message.split("\\[")[1];//tokens.nextToken();
                                    String temp2 = temp1.split("\\]")[0];//tokens.nextToken();

                                    sms_message = temp2;
                                    //
                                    Toast.makeText(context, context.getResources().getString(R.string.sep_sms_received), Toast.LENGTH_SHORT).show();

                                    Log.i(LOG_TAG, "(sms)" + sms_message);
                                    setAuthCodeArea(sms_message);
                                    // 다른 App에서 사용하지 못하도록 함
                                    // Android 4.4+ 부터 해당기능 작동불가와
                                    // 기기별 문제발생여지를 고려해 abort처리는 안함.
                                    // abortBroadcast();
                                }

                            } else {

                            }
                        }

                    }
                }
            };
            registerReceiver(mReceiver, filter);

            Log.d("onCreate()", "브로드캐스트리시버 등록됨");
        }
    }



    private void requestAuthCode(String _ctn) {
        if (_ctn != null && _ctn.length() > 9) {
            isReceiveMode = true;
            //aq.id(R.id.input_ctn_text).enabled(false);
            aq.id(R.id.btn_auth).text("인증번호 확인").typeface(CommonUtility.getNanumBarunTypeface());
            requestPhoneAuth(_ctn);
            aq.id(R.id.btn_clear_01).gone();
            aq.id(R.id.tf_phone_no).enabled(false);

            Toast.makeText(mCtx, "인증코드가 발송되었습니다.", Toast.LENGTH_SHORT).show();

        } else {
            DUtil.alertShow(this, "정상적인 번호가 아닙니다.");
            //DUtil.Log(TAG, "정상적인 번호가 아닙니다.");
            aq.id(R.id.btn_clear_01).visible();

        }
    }

    public void phoneAuthRequest(View button) {
        authProcessStatus = 1; // Requested
        if (!isReceiveMode) {
            // 인증문자 요청
            Log.d(TAG, "[phoneAuthRequest] ");
            String ctn = aq.id(R.id.tf_phone_no).getText().toString();
            String validCtn = ValidateUtil.validTelNumber(ctn);
            aq.id(R.id.btn_req).text("재전송").background(R.drawable.btn_reauth_back).typeface(CommonUtility.getNanumBarunTypeface());

            requestAuthCode(validCtn);
        } else {
            // 인증코드 검증
            String ctn = aq.id(R.id.tf_phone_no).getText().toString();
            String validCtn = ValidateUtil.validTelNumber(ctn);
            String authCode = aq.id(R.id.input_code_text).getText().toString();
            if (authCode.length() == 6) {
                authorizePhoneAuth(validCtn, authCode);
            } else {
                DUtil.alertShow(this, "인증번호는 6자리입니다.");
                DUtil.Log(TAG, "인증번호는 6자리입니다.");
                phoneInputEnable();
            }
        }

    }

    public void setAuthCodeArea(String _code) {
        aq.id(R.id.input_code_text).text(_code);

        String ctn = aq.id(R.id.tf_phone_no).getText().toString();
        String validCtn = ValidateUtil.validTelNumber(ctn);
        String authCode = aq.id(R.id.input_code_text).getText().toString();

        if (_code.length() == 6) {
            authorizePhoneAuth(validCtn, authCode);
        } else {
            DUtil.alertShow(this, "인증번호는 6자리입니다.");
            DUtil.Log(TAG, "인증번호는 6자리입니다.");
            phoneInputEnable();
        }
    }

    public void phoneAuthSubmit(View button){
        String ctn = aq.id(R.id.tf_phone_no).getText().toString();
        String validCtn = ValidateUtil.validTelNumber(ctn);
        String authCode = aq.id(R.id.input_code_text).getText().toString();
        if (authCode.length() == 6) {
            authorizePhoneAuth(validCtn, authCode);
        } else {
            DUtil.alertShow(this, "인증번호는 6자리입니다.");
            DUtil.Log(TAG, "인증번호는 6자리입니다.");
            phoneInputEnable();
        }
    }

    private void authorizePhoneAuth(final String _ctn, final String _authCode) {
        Map<String, Object> params = new HashMap<String, Object>();
        //params.put("phone", _ctn);
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("verifyCode", _authCode);
        VARS_MOBILE_NUMBER = _ctn;
        VARS_AUTH_NUMBER = _authCode;

        AjaxCallback.setNetworkLimit(1);
        aq.progress(dialog).ajax(CommonUtility.SERVER_URL + Constants.CONFIRM_CTN_AUTHCODE, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                if (status.getCode() >= 400) {
                    // IF ERROR
                    Log.i("authorizePhoneAuth StatusCode", status.getCode() + "");
                    Log.i("authorizePhoneAuth StatusError", status.getError());
                } else {
                    // IF NORMAL
                    if (json == null) {
                        Log.i("RETURN", "" + status.getMessage());
                        // 네트워크 오류페이지로 이동
                        //goNetworkErrorPage();
                        phoneInputEnable();
                        return;
                    }

                    Log.i("authorizePhoneAuth RETURN", "" + json.toString());

                    try {
                        if ("S0000".equals(json.getString("resultCode"))) {
                            Toast.makeText(mCtx, json.getString("message"), Toast.LENGTH_SHORT);
                            //aq.id(R.id.layout_codeauth).visible();
                            //aq.id(R.id.btn_auth).text("인증완료").enabled(false);
                            aq.id(R.id.auth_area).gone();
                            aq.id(R.id.auth_desc).visible();
                            aq.id(R.id.tf_phone_no).clickable(false).enabled(false);
                            aq.id(R.id.btn_req).gone();
                            mCountThread.stopThread();

                            authProcessStatus = 2; // authorized
                            //mActivity.showFragment(mActivity.FRAGMENT_EMAIL);

                            Intent resultData = new Intent();
                            resultData.putExtra("content", _ctn);
                            setResult(Activity.RESULT_OK, resultData);
                            finish();

                        } else {
                            //aq.id(R.id.layout_codeauth).gone();
                            //mActivity.VARS_MOBILE_NUMBER = "";
                            VARS_AUTH_NUMBER = "";
                            aq.id(R.id.input_code_text).text("");
                            phoneInputEnable();
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        phoneInputEnable();
                        e.printStackTrace();
                    }
                    Log.i("authorizePhoneAuth RESULT", json.toString());
                }
            }
        });
    }

    private void requestPhoneAuth(String _ctn) {
        Map<String, Object> params = new HashMap<String, Object>();
        _ctn = _ctn.replace("-", "");
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("phone", _ctn);

        Log.i("requestPhoneAuth PARAM", params.toString());

        AjaxCallback.setNetworkLimit(1);
        aq.progress(dialog).ajax(CommonUtility.SERVER_URL + Constants.REQUEST_CTN_AUTH, params, JSONObject.class, new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                if (status.getCode() >= 400) {
                    // IF ERROR
                    Log.i("requestPhoneAuth StatusCode", status.getCode() + "");
                    Log.i("requestPhoneAuth StatusError", status.getError());
                } else {
                    // IF NORMAL
                    if (json == null) {
                        Log.i("RETURN", "" + status.getMessage());
                        // 네트워크 오류페이지로 이동
                        //goNetworkErrorPage();
                        phoneInputEnable();
                        return;
                    }

                    Log.i("requestPhoneAuth RETURN", "" + json.toString());

                    try {
                        if ("S0000".equals(json.getString("resultCode"))) {
                            Toast.makeText(mCtx, json.getString("message"), Toast.LENGTH_SHORT).show();
                            aq.id(R.id.btn_auth).background(R.drawable.btn_login_register).enabled(true);
                            authValidTimerStart();
                            //aq.id(R.id.input_code_text).getEditText().requestFocus();
                        } else {
                            //aq.id(R.id.layout_codeauth).gone();
                            //aq.id(R.id.input_code_text).getEditText().requestFocus();
                            phoneInputEnable();
                            aq.id(R.id.btn_auth).background(R.color.disabled_color).enabled(false);
                            //DUtil.alertShow(mActivity, json.getString("result_msg"));
                            // Error Exception
                            //alert(json.getString("result_msg"), DURATION_SHORT);
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        phoneInputEnable();
                        e.printStackTrace();
                    }
                    //Log.i("requestPhoneAuth RESULT", json.toString());
                }
            }
        });
    }

    private void phoneInputEnable() {
        isReceiveMode = false;
        aq.id(R.id.tf_phone_no).enabled(true);
    }

    private void authValidTimerStart() {

        aq.id(R.id.validtime).visible();

        if (mCountThread == null) {
            mCountThread = new CountThread();
            mCountThread.start();
        } else {
            Log.i("CountThread ALIVE", mCountThread.isAlive() + "");

            mCountThread.interrupt();
            tv_Count.setText("");
            // mCountThread = new CountThread();
            if (mCountThread.isAlive()) {
                mCountThread.resetThread();
            } else {
                mCountThread = new CountThread();
                mCountThread.start();
            }
        }
    }

    private TextView tv_Count;

    public void setLayout() {
        tv_Count = aq.id(R.id.validtime).getTextView();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "[onResume]");
        VARS_MOBILE_NUMBER = "";
        VARS_AUTH_NUMBER = "";

        if ( Build.VERSION.SDK_INT >= 23){
            if (canAccessPermission()) {
                tv_Count.setText("");
            }
        }else{
            tv_Count.setText("");
        }

        super.onResume();
    }

    @Override
    public void onDestroy() {
        if ( Build.VERSION.SDK_INT >= 23){
            if (canAccessPermission()) {
                if(mReceiver != null){
                    unregisterReceiver(mReceiver);
                }

            }
        }else{
            if(mReceiver != null){
                unregisterReceiver(mReceiver);
            }
        }

        super.onDestroy();
    }

    @Override
    protected void onUserLeaveHint() {
        Log.e("outside", "onUserLeaveHint");
        // super.onUserLeaveHint();
        // impriortyPackage();
        aq.ajaxCancel();
        AjaxCallback.cancel();

        if ( Build.VERSION.SDK_INT >= 23){
            if (canAccessPermission()) {
                if(mReceiver != null){
                    unregisterReceiver(mReceiver);
                }
            }
        }else{
            if(mReceiver != null){
                unregisterReceiver(mReceiver);
            }
        }
        super.onUserLeaveHint();

    }
}
