package me.rewhite.users.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import me.rewhite.users.R;
import me.rewhite.users.adapter.AddressListItemV2;
import me.rewhite.users.adapter.AddressListViewAdapter;
import me.rewhite.users.adapter.AddressListViewAdapterV2;
import me.rewhite.users.adapter.ListTitleAdapter;
import me.rewhite.users.adapter.MergeAdapter;
import me.rewhite.users.adapter.MyLocationListItem;
import me.rewhite.users.adapter.MyLocationListViewAdapter;
import me.rewhite.users.adapter.PlaceListItem;
import me.rewhite.users.adapter.PlaceListViewAdapter;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.thread.GpsInfo;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class LocationActivity extends BaseActivity {

    private final static String TAG = LocationActivity.class.getSimpleName();
    AQuery aq;
    EditText inputSearchText;
    ProgressDialog dialog;
    public final Context mCtx = this;

    private static final int LOCATION_ADD = 1;
    private InputMethodManager imm;

    private ArrayList<PlaceListItem> placeData;
    private ArrayList<AddressListItemV2> addressData;
    private ArrayList<MyLocationListItem> myLocationData;
    private AddressListViewAdapterV2 addressAdapter;
    private PlaceListViewAdapter placeAdapter;
    private MyLocationListViewAdapter myLocationAdapter;

    private boolean locationTag = true;
    private String currentLocationAddress = "";
    // GPSTracker class
    private GpsInfo gps;

    double lat;
    double lng;

    private boolean hasLocation;

    private static final String HEADER_NAME_X_APPID = "x-appid";
    private static final String HEADER_NAME_X_PLATFORM = "x-platform";
    private static final String HEADER_VALUE_X_PLATFORM_ANDROID = "android";

    private static final String[] INITIAL_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final String[] LOCATION_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int INITIAL_REQUEST=1337;
    private static final int LOCATION_REQUEST=INITIAL_REQUEST+3;
    boolean isGpsGet = false;

    @Override
    public void onDestroy() {
        super.onDestroy();
        DUtil.Log("LocationActivity onDestroy", "==========onDestroy============");
        if (gps != null) {
            gps.stopUsingGPS();
        }
    }
    /**
     * 다른 화면으로 넘어갈 때, 일시정지 처리
     */
    @Override
    public void onPause() {
        //Activity LifrCycle 관련 메서드는 무조건 상위 메서드 호출 필요
        super.onPause();
        DUtil.Log("LocationActivity onPause", "==========onPause============");
        if (gps != null) {
            gps.stopUsingGPS();
        }
    }

    private synchronized void getLocationList(){
        if(dialog != null){
            dialog.show();
        }


        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.GET_LOCATION_LIST, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.GET_LOCATION_LIST, error.getMessage());
                dialog.dismiss();
                aq.id(R.id.no_have_location).visible();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                dialog.dismiss();

                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.GET_LOCATION_LIST, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //
                            hasLocation = false;
                        } else {
                            JSONArray locationInfo = jsondata.getJSONArray("data");
                            hasLocation = locationInfo.length() > 0;
                        }

                        if (hasLocation) {
                            aq.id(R.id.no_have_location).gone();
                        } else {
                            aq.id(R.id.no_have_location).visible();
                        }

                        // locationListView
                        ListView locationListView = (ListView) findViewById(R.id.locationListView);
                        myLocationData = new ArrayList<>();

                        JSONArray locationItems = jsondata.getJSONArray("data");
                        SharedPreferencesUtility.setLocationData(locationItems);

                        for (int i = 0; i < locationItems.length(); i++) {
                            String addressSeq = locationItems.getJSONObject(i).getString("addressSeq");
                            String label = locationItems.getJSONObject(i).getString("label");
                            String address = locationItems.getJSONObject(i).getString("address1");
                            String addressDetail = locationItems.getJSONObject(i).getString("address2");
                            String longitude = locationItems.getJSONObject(i).getString("longitude");
                            String latitude = locationItems.getJSONObject(i).getString("latitude");
                            String isDefault = locationItems.getJSONObject(i).getString("isDefault");
                            boolean defaultStatus = ("Y".equals(isDefault)) ? true : false;

                            MyLocationListItem aItem = new MyLocationListItem(addressSeq, label, address, addressDetail, longitude, latitude, defaultStatus);
                            myLocationData.add(aItem);
                        }

                        myLocationAdapter = new MyLocationListViewAdapter(mCtx, R.layout.mylocation_list_item, myLocationData);
                        myLocationAdapter.notifyDataSetChanged();

                        locationListView.setAdapter(myLocationAdapter);
                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        DUtil.Log("onResume", "==========onResume============");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.e("onActivityResult", "LOCATION_ADD");
        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case LOCATION_ADD:
                    Log.i("onActivityResult", "LOCATION_ADD");

                    JSONArray locationInfo = SharedPreferencesUtility.getLocationData();
                    if (locationInfo != null) {
                        if (locationInfo.length() > 0) {
                            //showToast("주소를 등록했습니다.");
                            Toast.makeText(LocationActivity.this, "주소를 등록했습니다.", Toast.LENGTH_SHORT).show();
                            if (locationInfo.length() == 1) {
                                finish();
                            }else{
                                getLocationInfo();

                                aq.id(R.id.result_area).gone();
                                aq.id(R.id.empty_area).visible();

                                getLocationList();
                            }
                        }
                    }

                    break;
                default:
                    break;
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.loading_comment));

        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
        logger.logEvent("Try address input");

        aq = new AQuery(this);
        inputSearchText = (EditText) findViewById(R.id.input_search_string);
        if (inputSearchText != null) {
            inputSearchText.setTypeface(CommonUtility.getNanumBarunTypeface());
        }
        inputSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchFullAddress();
                    //searchFencing();
                    return true;
                }
                return false;
            }
        });

        if ( Build.VERSION.SDK_INT >= 23){
            if (!canAccessLocation()) {
                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
            }else{
                isGpsGet = true;
                getLocationInfo();
            }
        }else{
            isGpsGet = true;
            getLocationInfo();
        }


        aq.id(R.id.current_pick).clicked(this, "currentPick");

        hideKeyboard(inputSearchText);

        aq.id(R.id.result_area).gone();
        aq.id(R.id.empty_area).visible();

        getLocationList();
    }

    public void hideKeyboard(EditText editText) {
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    // API 23 above Permission Protect statement
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch(requestCode) {

            case LOCATION_REQUEST:
                if (canAccessLocation()) {
                    isGpsGet = true;
                    getLocationInfo();
                }
                else {
                    Toast.makeText(this, "위치정보설정이 사용자에 의해 비활성화되어있습니다.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
    private boolean canAccessLocation() {
        return(hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    @TargetApi(23)
    private boolean hasPermission(String perm) {
        return(PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
    }

    public void getLocationInfo() {
        gps = new GpsInfo(LocationActivity.this);
        // GPS 사용유무 가져오기
        if (gps.isGetLocation()) {

            lat = gps.getLatitude();
            lng = gps.getLongitude();

//            Toast.makeText(
//                    getApplicationContext(),
//                    "당신의 위치 - \n위도: " + lat + "\n경도: " + lng,
//                    Toast.LENGTH_LONG).show();
        } else {
            // GPS 를 사용할수 없으므로
            gps.showSettingsAlert();
        }
    }

    public void itemRemoveSelected(String addressSeq) {

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("addressSeq", addressSeq);
        params.put("k", 1);
        NetworkClient.post(Constants.REMOVE_LOCATION, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.REMOVE_LOCATION, error.getMessage());
                aq.id(R.id.no_have_location).visible();
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                dialog.dismiss();

                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.REMOVE_LOCATION, result);

                    JSONObject jsondata = new JSONObject(result);

                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {
                            //
                            hasLocation = false;
                        } else {
                            JSONArray locationInfo = jsondata.getJSONArray("data");
                            hasLocation = locationInfo.length() > 0;
                        }

                        if (hasLocation) {
                            aq.id(R.id.no_have_location).gone();
                        } else {
                            aq.id(R.id.no_have_location).visible();
                        }

                        // locationListView
                        ListView locationListView = (ListView) findViewById(R.id.locationListView);
                        myLocationData = new ArrayList<>();

                        JSONArray locationItems = jsondata.getJSONArray("data");
                        SharedPreferencesUtility.setLocationData(locationItems);

                        for (int i = 0; i < locationItems.length(); i++) {
                            String addressSeq = locationItems.getJSONObject(i).getString("addressSeq");
                            String label = locationItems.getJSONObject(i).getString("label");
                            String address = locationItems.getJSONObject(i).getString("address1");
                            String addressDetail = locationItems.getJSONObject(i).getString("address2");
                            String longitude = locationItems.getJSONObject(i).getString("longitude");
                            String latitude = locationItems.getJSONObject(i).getString("latitude");
                            String isDefault = locationItems.getJSONObject(i).getString("isDefault");
                            boolean defaultStatus = ("Y".equals(isDefault)) ? true : false;

                            MyLocationListItem aItem = new MyLocationListItem(addressSeq, label, address, addressDetail, longitude, latitude, defaultStatus);
                            myLocationData.add(aItem);
                        }

                        myLocationAdapter = new MyLocationListViewAdapter(mCtx, R.layout.mylocation_list_item, myLocationData);
                        myLocationAdapter.notifyDataSetChanged();

                        locationListView.setAdapter(myLocationAdapter);
                    } else {

                    }
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            }
        });
    }

    public void currentPick(View button) {
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("가입및탈퇴")
                .setAction("현재주소사용")
                //.setLabel("")
                .build());
        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
        logger.logEvent("Use current address");

        if(!isGpsGet){
            //DUtil.alertShow(this, "현재 위치를 알기위해서는 위치정보 사용 작업 수행을 허용해야합니다.");
            if ( Build.VERSION.SDK_INT >= 23){
                if (!canAccessLocation()) {
                    requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
                }else{
                    isGpsGet = true;
                    getLocationInfo();
                }
            }else{
                isGpsGet = true;
                getLocationInfo();
            }
            return;
        }
        if (gps.isGetLocation()) {
            if (lat == 0 && lng == 0) {
                DUtil.alertShow(this, "잠시후 다시 시도해주세요");
            } else {
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, LocationDetailTMapActivity.class);
                intent.putExtra("sourceType", "current");
                intent.putExtra("longitude", lng + "");
                intent.putExtra("latitude", lat + "");
                intent.putExtra("addressName", findAddress(lat, lng));
                startActivityForResult(intent, LOCATION_ADD);
                //mCtx.startActivity(intent);
                //overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }

        } else {
            getLocationInfo();
        }
    }

    private void searchFullAddress(){
        final String _searchObjectString = aq.id(R.id.input_search_string).getText().toString();
        String Host = Constants.SKT_FULLTEXT_GEOCODING_SEARCH;
        String query = Host + "?count=20&page=&coordType=WGS84GEO&format=json&callback=&addressFlag=F00&version=1&fullAddr=" + _searchObjectString;

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {

                Log.i("SKT_FULLTEXT_GEOCODING_SEARCH RETURN", status.getCode() + " / " + status.getMessage() + " / " + status.getError());

                if (status.getCode() >= 400) {
                    // IF ERROR
                    Log.i("StatusCode", status.getCode() + "");
                    Log.i("StatusError", status.getError());

                    addressData = new ArrayList<>();
                    addressAdapter = new AddressListViewAdapterV2(mCtx, R.layout.address_list_item, addressData);
                    addressAdapter.notifyDataSetChanged();
                    performSearch();
                } else {

                    addressData = new ArrayList<>();

                    if (status.getCode() == 204) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("가입및탈퇴")
                                .setAction("주소검색결과없음")
                                //.setLabel("")
                                .build());
                        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                        logger.logEvent("no search address result");

                        Bundle parameters = new Bundle();
                        parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "search full location");
                        parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, _searchObjectString);
                        parameters.putInt(AppEventsConstants.EVENT_PARAM_SUCCESS, 1);
                        parameters.putString(AppEventsConstants.EVENT_PARAM_DESCRIPTION, "검색결과 없음");
                        logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED, parameters);

                        Log.i("FULLTEXT_GEOCODING_SEARCH RETURN", "" + status.getMessage());
                    } else if (status.getCode() == 200) {
                        // IF NORMAL
                        if (json == null) {
                            mTracker.send(new HitBuilders.EventBuilder()
                                    .setCategory("가입및탈퇴")
                                    .setAction("주소검색결과없음")
                                    //.setLabel("")
                                    .build());
                            AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                            logger.logEvent("no search address result");
                            Bundle parameters = new Bundle();
                            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "search full location");
                            parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, _searchObjectString);
                            parameters.putInt(AppEventsConstants.EVENT_PARAM_SUCCESS, 1);
                            parameters.putString(AppEventsConstants.EVENT_PARAM_DESCRIPTION, "검색결과 없음");
                            logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED, parameters);

                            Log.i("FULLTEXT_GEOCODING_SEARCH RETURN", "" + status.getMessage());
                            // 네트워크 오류페이지로 이동
                            return;
                        } else {
                            Log.i("FULLTEXT_GEOCODING_SEARCH RESULT", json.toString());
                            // 검색결과 있음

                            try {
                                JSONArray legalItems = json.getJSONObject("coordinateInfo").getJSONArray("coordinate");

                                for (int i = 0; i < legalItems.length(); i++) {
                                    JSONObject obj = legalItems.getJSONObject(i);
//                                    String description = obj.getString("description");
//                                    String dos = obj.getJSONObject("properties").getString("doName");
//                                    String gus = obj.getJSONObject("properties").getString("guName");
//                                    String dongs = obj.getString("regionName");
//
//                                    AddressListItem aItem = new AddressListItem(description, dos, gus, dongs);
//                                    addressData.add(aItem);

                                    AddressListItemV2 aItem = new AddressListItemV2(obj);
                                    addressData.add(aItem);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    addressAdapter = new AddressListViewAdapterV2(mCtx, R.layout.address_list_item_v2, addressData);
                    addressAdapter.notifyDataSetChanged();
                    performSearch();
                }
            }
        };
        cb.header("appKey", Constants.SK_APP_KEY);
        cb.header("Accept", "application/json");
        cb.header("Content-Type", "application/json; charset=utf-8");

        aq.ajax(query, JSONObject.class, cb);
    }

//    private void searchFencing() {
//        mTracker.send(new HitBuilders.EventBuilder()
//                .setCategory("가입및탈퇴")
//                .setAction("주소검색")
//                //.setLabel("")
//                .build());
//        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
//        logger.logEvent("Search address");
//
//        final String _searchObjectString = aq.id(R.id.input_search_string).getText().toString();
//        String Host = Constants.GEO_FENCING_SEARCH;
//        String query = Host + "?count=20&page=&reqCoordType=WGS84GEO&callback=&categories=legalDong&bizAppId=&searchType=KEYWORD&resCoordType=&reqLon=&reqLat=&version=1&searchKeyword=" + _searchObjectString;
//
//        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {
//
//            @Override
//            public void callback(String url, JSONObject json, AjaxStatus status) {
//
//                Log.i("GEO_FENCING_SEARCH RETURN", status.getCode() + " / " + status.getMessage() + " / " + status.getError());
//
//                if (status.getCode() >= 400) {
//                    // IF ERROR
//                    Log.i("StatusCode", status.getCode() + "");
//                    Log.i("StatusError", status.getError());
//                } else {
//
//                    addressData = new ArrayList<>();
//
//                    if (status.getCode() == 204) {
//                        mTracker.send(new HitBuilders.EventBuilder()
//                                .setCategory("가입및탈퇴")
//                                .setAction("주소검색결과없음")
//                                //.setLabel("")
//                                .build());
//                        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
//                        logger.logEvent("no search address result");
//
//                        Bundle parameters = new Bundle();
//                        parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "location");
//                        parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, _searchObjectString);
//                        parameters.putInt(AppEventsConstants.EVENT_PARAM_SUCCESS, 1);
//                        parameters.putString(AppEventsConstants.EVENT_PARAM_DESCRIPTION, "검색결과 없음");
//                        logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED, parameters);
//
//                        Log.i("GEO_FENCING_SEARCH RETURN", "" + status.getMessage());
//                    } else if (status.getCode() == 200) {
//                        // IF NORMAL
//                        if (json == null) {
//                            mTracker.send(new HitBuilders.EventBuilder()
//                                    .setCategory("가입및탈퇴")
//                                    .setAction("주소검색결과없음")
//                                    //.setLabel("")
//                                    .build());
//                            AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
//                            logger.logEvent("no search address result");
//                            Bundle parameters = new Bundle();
//                            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "location");
//                            parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, _searchObjectString);
//                            parameters.putInt(AppEventsConstants.EVENT_PARAM_SUCCESS, 1);
//                            parameters.putString(AppEventsConstants.EVENT_PARAM_DESCRIPTION, "검색결과 없음");
//                            logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED, parameters);
//
//                            Log.i("GEO_FENCING_SEARCH RETURN", "" + status.getMessage());
//                            // 네트워크 오류페이지로 이동
//                            return;
//                        } else {
//
//                            try {
//                                Log.i("GEO_FENCING_SEARCH RESULT", json.toString());
//                                JSONArray legalItems = json.getJSONArray("searchRegionsInfo");
//
//                                for (int i = 0; i < legalItems.length(); i++) {
//                                    JSONObject obj = legalItems.getJSONObject(i).getJSONObject("regionInfo");
//                                    String description = obj.getString("description");
//                                    String dos = obj.getJSONObject("properties").getString("doName");
//                                    String gus = obj.getJSONObject("properties").getString("guName");
//                                    String dongs = obj.getString("regionName");
//
//                                    AddressListItem aItem = new AddressListItem(description, dos, gus, dongs);
//                                    addressData.add(aItem);
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//
//                    addressAdapter = new AddressListViewAdapter(mCtx, R.layout.address_list_item, addressData);
//                    addressAdapter.notifyDataSetChanged();
//                    performSearch();
//                }
//            }
//        };
//        cb.header("appKey", Constants.SK_APP_KEY);
//        cb.header("Accept", "application/json");
//        cb.header("Content-Type", "application/json; charset=utf-8");
//
//        aq.ajax(query, JSONObject.class, cb);
//    }

    public void itemSelected(int section, final int position) {
        Log.i("ItemSelected", section + ":" + position);

        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
        logger.logEvent("Select address");

        switch (section) {

            case 1:

                final String addressName = addressData.get(position).getAddressString();

                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("가입및탈퇴")
                        .setAction("주소검색결과선택")
                        //.setLabel("")
                        .build());


                String lat = addressData.get(position).getLatitude() + ""; //json.getJSONObject("coordinateInfo").getString("lat");
                String lon = addressData.get(position).getLongitude() + ""; //json.getJSONObject("coordinateInfo").getString("lon");
                Log.i("lon RESULT", lat + " / " + lon);

                Intent intent1 = new Intent();
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent1.setClass(mCtx, LocationDetailTMapActivity.class);
                intent1.putExtra("title", addressData.get(position).getBuildingName());
                intent1.putExtra("longitude", lon);
                intent1.putExtra("latitude", lat);
                intent1.putExtra("addressName", addressName);
                startActivityForResult(intent1, LOCATION_ADD);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case 2:
                // 장소선택시
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.setClass(mCtx, LocationDetailTMapActivity.class);
                intent.putExtra("title", placeData.get(position).getTitle());
                intent.putExtra("longitude", placeData.get(position).getLongitude());
                intent.putExtra("latitude", placeData.get(position).getLatitude());
                intent.putExtra("addressName", placeData.get(position).getAddressName());
                intent.putExtra("newAddressName", placeData.get(position).getNewAddressName());
                startActivityForResult(intent, LOCATION_ADD);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                break;
            default:
                break;
        }
    }


    private void performSearch() {
        final String _searchObjectString = aq.id(R.id.input_search_string).getText().toString();
        String Host = Constants.GEO_SEARCH;
        String query = Host + "?apikey=" + Constants.DAUM_API_KEY + "&query=" + _searchObjectString;

        AjaxCallback<JSONObject> cb = new AjaxCallback<JSONObject>() {

            @Override
            public void callback(String url, JSONObject json, AjaxStatus status) {
                if (status.getCode() >= 400) {
                    // IF ERROR
                    Log.i("StatusCode", status.getCode() + "");
                    Log.i("StatusError", status.getError());
                    AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                    Bundle parameters = new Bundle();
                    parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "location");
                    parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, _searchObjectString);
                    parameters.putInt(AppEventsConstants.EVENT_PARAM_SUCCESS, 1);
                    parameters.putString(AppEventsConstants.EVENT_PARAM_DESCRIPTION, "네트워크 오류발생 >400");
                    logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED, parameters);
                } else {
                    // IF NORMAL
                    if (json == null) {
                        Log.i("RETURN", "" + status.getMessage());
                        // 네트워크 오류페이지로 이동
                        AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                        Bundle parameters = new Bundle();
                        parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "location");
                        parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, _searchObjectString);
                        parameters.putInt(AppEventsConstants.EVENT_PARAM_SUCCESS, 1);
                        parameters.putString(AppEventsConstants.EVENT_PARAM_DESCRIPTION, "네트워크 오류발생");
                        logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED, parameters);
                        return;
                    }

                    try {
                        Log.i("performSearch RESULT", json.toString() + " / " + addressData.size());
                        JSONArray arr = json.getJSONObject("channel").getJSONArray("item");

                        if (arr.length() == 0 && addressData.size() == 0) {
                            aq.id(R.id.empty_area).gone();
                            aq.id(R.id.noresult_area).visible();
                            aq.id(R.id.result_area).visible();

                            AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                            Bundle parameters = new Bundle();
                            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "location");
                            parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, _searchObjectString);
                            parameters.putInt(AppEventsConstants.EVENT_PARAM_SUCCESS, 1);
                            parameters.putString(AppEventsConstants.EVENT_PARAM_DESCRIPTION, "검색결과 없음");

                            logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED, parameters);
                        } else {
                            aq.id(R.id.noresult_area).gone();
                            aq.id(R.id.empty_area).gone();
                            aq.id(R.id.result_area).visible();

                            AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
                            Bundle parameters = new Bundle();
                            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "location");
                            parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, _searchObjectString);
                            parameters.putInt(AppEventsConstants.EVENT_PARAM_SUCCESS, 0);
                            logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED, parameters);
                        }

                        // TEST
                        ListView listView = (ListView) findViewById(R.id.listView);
                        placeData = new ArrayList<>();

                        for (int i = 0; i < arr.length(); i++) {
                            JSONObject regionInfo = arr.getJSONObject(i);
                            String titleName = regionInfo.getString("title");
                            String addressName = regionInfo.getString("address");
                            String newAddressName = regionInfo.getString("newAddress");

                            String longitude = regionInfo.getString("longitude");
                            String latitude = regionInfo.getString("latitude");
                            String id = regionInfo.getString("id");

                            PlaceListItem aItem = new PlaceListItem(titleName, addressName, newAddressName, longitude, latitude, id);
                            placeData.add(aItem);
                        }

                        placeAdapter = new PlaceListViewAdapter(mCtx, R.layout.place_list_item, placeData);
                        placeAdapter.notifyDataSetChanged();

                        MergeAdapter mergeAdapter = new MergeAdapter();

                        if (addressData.size() > 0) {
                            mergeAdapter.addAdapter(new ListTitleAdapter(mCtx, "주소검색 결과", addressAdapter));
                            mergeAdapter.addAdapter(addressAdapter);
                        }

                        if (placeData.size() > 0) {
                            mergeAdapter.addAdapter(new ListTitleAdapter(mCtx, "키워드검색 결과", placeAdapter));
                            mergeAdapter.addAdapter(placeAdapter);
                        }

                        //mergeAdapter.setNoItemsText("Nothing to display. This list is empty.");
                        listView.setAdapter(mergeAdapter);

                        imm.hideSoftInputFromWindow(aq.id(R.id.input_search_string).getView().getWindowToken(), 0);

                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position,
                                                    long id) {
                                AQuery bq = new AQuery(view);
                                Log.i("position", position + "/" + parent.getAdapter().getCount());

                                if (parent.getAdapter() instanceof PlaceListViewAdapter) {

                                } else if (parent.getAdapter() instanceof AddressListViewAdapter) {

                                }


                                //EmailListItem entry = (EmailListItem) parent.getAdapter().getItem(position);
                                //String title = bq.id(R.id.text_email).getText().toString();
                                //aq.id(R.id.edit_email).text(emailAddress);
                                //view.findViewById(R.id.icon_check).setVisibility(View.VISIBLE);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
        };

        cb.header("Accept", "application/json");
        cb.header("Content-Type", "application/json; charset=utf-8");
        cb.header(HEADER_NAME_X_APPID, "33199");
        cb.header(HEADER_NAME_X_PLATFORM, HEADER_VALUE_X_PLATFORM_ANDROID);

        aq.ajax(query, JSONObject.class, cb);
    }

    private String findAddress(double lat, double lng) {
        String geoString = null;

        //위치정보를 활용하기 위한 구글 API 객체
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        //주소 목록을 담기 위한 HashMap
        List<Address> list = null;

        try {
            list = geocoder.getFromLocation(lat, lng, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (list == null) {
            Log.e("getAddress", "주소 데이터 얻기 실패");
            AppEventsLogger logger = AppEventsLogger.newLogger(mCtx);
            Bundle parameters = new Bundle();
            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, "location");
            parameters.putString(AppEventsConstants.EVENT_PARAM_SEARCH_STRING, lat + "," + lng);
            parameters.putInt(AppEventsConstants.EVENT_PARAM_SUCCESS, 1);
            parameters.putString(AppEventsConstants.EVENT_PARAM_DESCRIPTION, "주소데이터 얻기 실패");
            logger.logEvent(AppEventsConstants.EVENT_NAME_SEARCHED, parameters);
            return null;
        }

        if (list.size() > 0) {
            Address addressElem = list.get(0);

            Log.e("Address", addressElem.toString());
            currentLocationAddress = addressElem.getAddressLine(0).toString().replace("대한민국 ", "");
        }

        return currentLocationAddress;

    }

}
