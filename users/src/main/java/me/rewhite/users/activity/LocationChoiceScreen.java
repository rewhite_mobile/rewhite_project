package me.rewhite.users.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.R;
import me.rewhite.users.adapter.LocationChoiceListViewAdapter;
import me.rewhite.users.adapter.MyLocationListItem;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

public class LocationChoiceScreen extends BaseActivity {

    private AQuery aq;
    String content;
    JSONArray data;
    private ArrayList<MyLocationListItem> myLocationData;
    private LocationChoiceListViewAdapter myLocationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        setContentView(R.layout.fragment_location_choice);

        aq = new AQuery(this);

        Intent intent = getIntent();
        if (intent != null) {
            content = intent.getStringExtra("content");
            Log.e("Location Data : ", content);
            try {
                data = new JSONArray(content);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        aq.id(R.id.btn_close).clicked(this, "noClicked");

        try {
            initialize();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initialize() throws JSONException {
        // locationListView
        ListView locationListView = (ListView) findViewById(R.id.listView);
        myLocationData = new ArrayList<>();

        //JSONArray locationItems = jsondata.getJSONArray("data");
        //SharedPreferencesUtility.setLocationData(data);

        for (int i = 0; i < data.length(); i++) {
            String addressSeq = data.getJSONObject(i).getString("addressSeq");
            String label = data.getJSONObject(i).getString("label");
            String address = data.getJSONObject(i).getString("address1");
            String addressDetail = data.getJSONObject(i).getString("address2");
            String longitude = data.getJSONObject(i).getString("longitude");
            String latitude = data.getJSONObject(i).getString("latitude");
            String isDefault = data.getJSONObject(i).getString("isDefault");
            boolean defaultStatus = ("Y".equals(isDefault)) ? true : false;

            MyLocationListItem aItem = new MyLocationListItem(addressSeq, label, address, addressDetail, longitude, latitude, defaultStatus);
            myLocationData.add(aItem);
        }

        myLocationAdapter = new LocationChoiceListViewAdapter(this, R.layout.location_choice_list_item, myLocationData);
        myLocationAdapter.notifyDataSetChanged();

        locationListView.setAdapter(myLocationAdapter);
    }

    public void noClicked(View button) {
        Log.e("okCilcked", "NO");
        setResult(Activity.RESULT_CANCELED);
        finish();
    }


    public void changeDefaultAddress(int position) {
        try {

            String addressSeq = data.getJSONObject(position).getString("addressSeq");
            String label = data.getJSONObject(position).getString("label");
            String address = data.getJSONObject(position).getString("address1");
            String addressDetail = data.getJSONObject(position).getString("address2");
            String longitude = data.getJSONObject(position).getString("longitude");
            String latitude = data.getJSONObject(position).getString("latitude");

            RequestParams params = new RequestParams();
            params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
            params.put("addressSeq", addressSeq);
            params.put("label", label);
            params.put("address1", address);
            params.put("address2", addressDetail);
            params.put("longitude", longitude);
            params.put("latitude", latitude);
            params.put("isDefault", "Y");
            params.put("k", 1);
            NetworkClient.post(Constants.MOD_LOCATION, params, new AsyncHttpResponseHandler() {

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                    // TODO Auto-generated method stub
                    DUtil.Log(Constants.MOD_LOCATION, error.getMessage());
                }

                @Override
                public void onSuccess(int arg0, Header[] arg1, byte[] data) {

                    String result;
                    try {
                        result = new String(data, "UTF-8");
                        DUtil.Log(Constants.MOD_LOCATION, result);

                        JSONObject jsondata = new JSONObject(result);

                        if ("S0000".equals(jsondata.getString("resultCode"))) {
                            if (!jsondata.isNull("data")) {
                                if (jsondata.getJSONArray("data").length() > 0) {
                                    SharedPreferencesUtility.setLocationData(jsondata.getJSONArray("data"));
                                }
                            }

                            Intent resultData = new Intent();
                            resultData.putExtra("result", true);
                            setResult(Activity.RESULT_OK, resultData);
                            finish();
                        } else {

                        }
                    } catch (UnsupportedEncodingException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
