package me.rewhite.users.activity.gsretail;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.androidquery.AQuery;

import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.users.R;

public class GSOrderNoticeScreen extends AppCompatActivity {

    AQuery aq;
    JSONObject reqOrderInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gsorder_notice_screen);

        aq = new AQuery(this);

        Intent intent = getIntent();
        if(intent != null){
            try {
                reqOrderInfo = new JSONObject(intent.getStringExtra("orderInfo"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        aq.id(R.id.btn_close).clicked(this, "closeAction");
        aq.id(R.id.btn_submit).clicked(this, "submitAction");
    }

    public void submitAction(View button){

        Intent resultData = new Intent();
        resultData.putExtra("REQ_ORDER_INFO", reqOrderInfo.toString());
        setResult(Activity.RESULT_OK, resultData);
        finish();
    }

    public void closeAction(View button){
        Intent resultData = new Intent();
        setResult(Activity.RESULT_CANCELED, resultData);
        finish();
    }
}
