package me.rewhite.users.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.androidquery.AQuery;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.util.EncodingUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;
import me.rewhite.users.common.BaseActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.network.NetworkClient;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;


@SuppressLint({"NewApi", "JavascriptInterface", "SetJavaScriptEnabled"})
public class ReceiptActivity extends BaseActivity {

    private AQuery aq;
    WebView webView;
    ProgressDialog dialog;
    public Context ctx = this;

    @Override
    public void onResume() {

        Log.d("ReceiptActivity", "[onResume]");

        //getBannerInfo();
        /*
        if(second != null){
            second.run();
        }*/

        super.onResume();
    }

    @Override
    public void onPause() {

        Log.d("ReceiptActivity", "[onPause]");

        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);

        Tracker t = ((Bootstrap)getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory("결제")
                .setAction("인수증보기")
                //.setLabel("Order")
                .build());
        AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
        logger.logEvent("Show Receipt");

        aq = new AQuery(this);
        aq.id(R.id.title_text).text("인수증");
        aq.id(R.id.btn_drawer).clicked(this, "closeClicked");
        aq.id(R.id.btn_banner).clicked(this, "bannerClicked");

        webView = (WebView) findViewById(R.id.webview_area);
        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setWebViewClient(new MyWebViewClient());
        webView.setWebChromeClient(new MyWebChromeClient());
        webView.clearCache(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        Intent myIntent = getIntent();
        orderId = myIntent.getExtras().getString("ORDER_ID");

        String postData = String.format("accessToken=%s&orderId=%s", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN), orderId);
        webView.postUrl(Constants.ORDER_RECEIPT, EncodingUtils.getBytes(postData, "BASE64"));

        // Progress 처리 진행상황을 보기위해
        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        dialog.setInverseBackgroundForced(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(getString(R.string.web_loading_comment));

        getBannerInfo();
    }

    String orderId;

    boolean loadingFinished = true;
    boolean redirect = false;

    public void closeClicked(View button) {
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
            if (!loadingFinished) {
                redirect = true;
            }

            if( urlNewString.startsWith("http:") || urlNewString.startsWith("https:") ) {
                return false;
            }else if (urlNewString.startsWith("tel:")) {
                Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse(urlNewString));
                startActivity(tel);
                return true;
            }
            loadingFinished = false;
            view.loadUrl(urlNewString);

            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap facIcon) {
            loadingFinished = false;
            // SHOW LOADING IF IT ISNT ALREADY VISIBLE
            dialog.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (!redirect) {
                loadingFinished = true;
            }

            if (loadingFinished && !redirect) {
                // HIDE LOADING IT HAS FINISHED
                dialog.dismiss();
            } else {
                redirect = false;
            }
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            //Toast.makeText(ctx, "Loading Error" + description, Toast.LENGTH_SHORT).show();
        }

    }

    private class MyWebChromeClient extends WebChromeClient {
        //javascript의 alert과 confirm을 인식하기 위해서는 onJsAlert과 onJsConfirm 메서드가 있어야 합니다.
        @Override
        public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result) {
            new AlertDialog.Builder(view.getContext())
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok,
                            new AlertDialog.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    result.confirm();
                                }
                            })
                    .setCancelable(true)
                    .create()
                    .show();

            return true;
        }

        @Override
        public boolean onJsConfirm(WebView view, String url, String message, final android.webkit.JsResult result) {
            new AlertDialog.Builder(view.getContext())
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    result.confirm();
                                }
                            })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    result.cancel();
                                }
                            })
                    .create()
                    .show();

            return true;
        }


    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }
    }

    private synchronized void getBannerInfo() {
        if(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.SHARE_CODE) == null || "".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.SHARE_CODE))){
            return;
        }

        RequestParams params = new RequestParams();
        params.put("accessToken", SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN));
        params.put("k", 1);
        NetworkClient.post(Constants.EVENT_CHECK, params, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] data, Throwable error) {
                // TODO Auto-generated method stub
                DUtil.Log(Constants.EVENT_CHECK, error.getMessage());
            }

            @Override
            public void onSuccess(int arg0, Header[] arg1, byte[] data) {
                String result;
                try {
                    result = new String(data, "UTF-8");
                    DUtil.Log(Constants.EVENT_CHECK, result);

                    JSONObject jsondata = new JSONObject(result);


                    if ("S0000".equals(jsondata.getString("resultCode"))) {
                        if (jsondata.isNull("data")) {

                        } else {
                            bannerInfo = jsondata.getJSONObject("data").getJSONArray("banner");

                            if(bannerInfo.length() > 0){
                                aq.id(R.id.btn_banner).image(bannerInfo.getJSONObject(0).getString("image"));
                            }

                        }
                    }

                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (JSONException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
    }

    JSONArray bannerInfo;
    JSONArray popupInfo;
//    Timer timer;
//    private TimerTask second;
//    private final Handler handler = new Handler();
//    int timer_sec = 0;
//
//    private void activateBanner(){
//        timer_sec = 0;
//
//        second = new TimerTask() {
//
//            @Override
//            public void run() {
//                Log.i("Test", "Timer start");
//                UpdateBanner();
//                timer_sec++;
//            }
//        };
//        timer = new Timer();
//        timer.schedule(second, 0, 4500);
//
//    }
//
//    protected void UpdateBanner() {
//        Runnable updater = new Runnable() {
//            public void run() {
//                try {
//                    aq.id(R.id.btn_banner).image(bannerInfo.getJSONObject(timer_sec%bannerInfo.length()).getString("image"));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        };
//        handler.post(updater);
//    }

    public void bannerClicked(View button){
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("이벤트")
                .setAction("인수증배너터치")
                //.setLabel("")
                .build());
        AppEventsLogger logger = AppEventsLogger.newLogger(ctx);
        logger.logEvent("Touch Receipt Banner");

        String url = null;
        try {
            url = bannerInfo.getJSONObject(0).getString("url");
            Uri uri = Uri.parse(url);
            String queryHost = uri.getHost();

            if("http".equals(uri.getScheme()) || "https".equals(uri.getScheme())){
                Intent intent = new Intent(ctx, HtmlActivity.class);
                intent.putExtra("TERMS_URI", url);
                intent.putExtra("TITLE_NAME", "이벤트");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
            }else{
                //String url ="selphone://post_detail?post_id=10";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//
//                Intent intent = new Intent();
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                intent.setClass(ctx, ShareActivity.class);
//                startActivity(intent);
//                getActivity().overridePendingTransition(R.anim.slide_in_anticipate, R.anim.slide_out_antipate);
//
//            }
//        }, 100);
    }
}
