package me.rewhite.users;

/**
 * Created by marines on 2015. 10. 14..
 */

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.rewhite.users.activity.IntroActivity;
import me.rewhite.users.activity.MainActivity;
import me.rewhite.users.common.PushWakeLock;
import me.rewhite.users.util.SharedPreferencesUtility;

/**
 * IntentService responsible for handling GCM messages.
 */
public class MyFcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    private static int mNotiId;
//
//    public MyFcmListenerService() {
//        super(SENDER_ID);
//        mNotiId = -1;
//    }
//
//    @Override
//    protected void onRegistered(Context context, String registrationId) {
//        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.GCM_ID, registrationId);
//        Log.i("regId onRegistered", registrationId);
//    }
//
//    @Override
//    protected void onUnregistered(Context context, String registrationId) {
//        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.GCM_ID, "");
//        if (GCMRegistrar.isRegisteredOnServer(context)) {
//            // ServerUtilities.unregister(context, registrationId);
//        } else {
//            // This callback results from the call to unregister made on
//            // ServerUtilities when the registration to the server failed.
//            Log.i(TAG, "Ignoring unregister callback");
//        }
//    }

    public Context mCtx = this;

    public static Map<String, List<String>> getUrlParameters(String url)
            throws UnsupportedEncodingException {
        Map<String, List<String>> params = new HashMap<String, List<String>>();
        String[] urlParts = url.split("\\?");
        if (urlParts.length > 1) {
            String query = urlParts[1];
            for (String param : query.split("&")) {
                String pair[] = param.split("=");
                String key = URLDecoder.decode(pair[0], "UTF-8");
                String value = "";
                if (pair.length > 1) {
                    value = URLDecoder.decode(pair[1], "UTF-8");
                }
                List<String> values = params.get(key);
                if (values == null) {
                    values = new ArrayList<String>();
                    params.put(key, values);
                }
                values.add(value);
            }
        }
        return params;
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String msg = data.getString("message");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + msg);

        Bundle extras = data;
        //Log.d(TAG, "extras: " + extras.toString());

        if (!extras.isEmpty()) {
            String c2dm_msg = (String)extras.get("message");
            //c2dm_msg = extras.get("message");
            String c2dm_parameter = null;
            c2dm_parameter = (String)extras.get("parameter");

            if (c2dm_parameter == null) return;

            // query = synchronize?memo_sync=Y
            Uri u = Uri.parse(c2dm_parameter);

            String queryHost = c2dm_parameter.split("\\?")[0];

            if (queryHost.equals("order")) {
                // silent push : for payment
                // 결제요청에 의한 PUSH처리
                Uri uri = Uri.parse(c2dm_parameter);
                String orderId = uri.getQueryParameter("orderId");
                int orderSubType = Integer.parseInt(uri.getQueryParameter("orderSubType"));
                String pushId = uri.getQueryParameter("pushId");

                Log.i(TAG, "new message= " + c2dm_msg);
                Log.i(TAG, "new type = " + queryHost);
                Log.i(TAG, "new parameter= " + uri.toString());
                Log.i(TAG, "new orderId= " + orderId);

                Intent resultIntent = new Intent(this, IntroActivity.class);
                resultIntent.putExtra("orderId", orderId);
                resultIntent.putExtra("orderSubType", orderSubType);
                resultIntent.putExtra("pushId", pushId);
                generateNotification(c2dm_msg, resultIntent);
            }else{
                String destinationType = "";
                String destParam = "";
                String viewType = "";
                String title = "";
                String comment = "";
                String image = "";
                int orderSubType = -1;

                String pre = c2dm_parameter.split("\\?")[0];

                try {
                    JSONObject rData = new JSONObject(c2dm_parameter.substring(pre.length()+1));
                    destinationType = rData.getString("destinationType");
                    destParam = rData.getString("destParam");
                    orderSubType = rData.getInt("orderSubType");
                    viewType = rData.getString("viewType");
                    title = rData.getString("title");
                    comment = rData.getString("comment");
                    image = rData.getString("image");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.i(TAG, "new message= " + c2dm_msg);
                Log.i(TAG, "new type = " + queryHost);
                Log.i(TAG, "new params = " + c2dm_parameter.split("\\?")[1]);

                Log.e("FCMService param", queryHost + " / " + destinationType + " / " + destParam + " / " + viewType + " / " + title + " / " + comment + " / " + image);

                Intent resultIntent = new Intent(this, IntroActivity.class);

                if(queryHost.equals("system")){

                    resultIntent.putExtra("queryHost", queryHost);
                    resultIntent.putExtra("destinationType", destinationType);
                    resultIntent.putExtra("destParam", destParam);
                    resultIntent.putExtra("orderSubType", orderSubType);
                    resultIntent.putExtra("viewType", viewType);

                }else if(queryHost.equals("web")){
                    resultIntent.putExtra("queryHost", queryHost);
                    resultIntent.putExtra("destinationType", destinationType);
                    resultIntent.putExtra("destParam", destParam);
                    resultIntent.putExtra("orderSubType", orderSubType);
                    resultIntent.putExtra("viewType", viewType);
                    resultIntent.putExtra("title", title);

                }else if (queryHost.equals("event")) {

                    Log.i(TAG, "new c2dm_parameter= " + c2dm_parameter);
                    Log.i(TAG, "imagePath = " + image);
                    resultIntent.putExtra("queryHost", queryHost);
                    resultIntent.putExtra("destinationType", destinationType);
                    resultIntent.putExtra("destParam", destParam);
                    resultIntent.putExtra("viewType", viewType);
                    resultIntent.putExtra("title", title);

                }else if(queryHost.equals("default")){

                    resultIntent.putExtra("queryHost", queryHost);
                    resultIntent.putExtra("destinationType", destinationType);
                    resultIntent.putExtra("destParam", destParam);
                    resultIntent.putExtra("viewType", viewType);

                }else{

                    Log.i(TAG, "new c2dm_parameter= " + c2dm_parameter);
                    resultIntent.putExtra("queryHost", queryHost);
                    resultIntent.putExtra("destinationType", destinationType);
                    resultIntent.putExtra("destParam", destParam);
                    resultIntent.putExtra("viewType", viewType);
                }

                if("".equals(image) || image == null){

                }else{
                    try {
                        URL url = new URL(image.toString());
                        URLConnection conn = url.openConnection();
                        conn.connect();
                        BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
                        Bitmap imgBitmap = BitmapFactory.decodeStream(bis);
                        bis.close();
                        loadedImage = imgBitmap;
                    }catch (MalformedURLException e) {
                        System.out.println("The URL is not valid.");
                        System.out.println(e.getMessage());
                    }catch (Exception e) {
                        Log.e("DEBUGTAG", "Remote Image Exception", e);
                    }
                }

                if("1".equals(viewType)){
                    generateNotification(comment, resultIntent);
                }else if("2".equals(viewType)){
                    generateBigTextNotification(title, comment, resultIntent);
                }else if("3".equals(viewType)){
                    generateImageNotification(title, comment, loadedImage, resultIntent);
                }else if("4".equals(viewType)) {
                    // 투명 Popup Activity 실행
                }else{
                    generateNotification(comment, resultIntent);
                }

            }

        }
    }


    protected Bitmap loadedImage;

    private void generateBigTextNotification(final String title, final String message, final Intent intent){

        NotificationCompat.Builder mBuilder;
        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        if(Build.VERSION.SDK_INT >= 26){
            NotificationChannel mChannel = new NotificationChannel("rewhiteusers","rewhiteusers",NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(mChannel);
            mBuilder = new NotificationCompat.Builder(this, mChannel.getId());
        }else{
            mBuilder = new NotificationCompat.Builder(this);
        }

        mBuilder.setAutoCancel(true)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.icon_line)
                .setContentText(message);

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle(mBuilder);
        bigTextStyle.setSummaryText("자세히 보기 +");
        bigTextStyle.setBigContentTitle(title);
        bigTextStyle.bigText(message);

        mBuilder.setStyle(bigTextStyle);
        mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        Intent resultIntent = intent;
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(IntroActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);


        mNotificationManager.notify(100, mBuilder.build());

    }

    private void generateImageNotification(final String title, final String message, final Bitmap _banner, final Intent intent){

        NotificationCompat.Builder mBuilder;
        NotificationManager mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        if(Build.VERSION.SDK_INT >= 26){
            NotificationChannel mChannel = new NotificationChannel("rewhiteusers","rewhiteusers",NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(mChannel);
            mBuilder = new NotificationCompat.Builder(this, mChannel.getId());
        }else{
            mBuilder = new NotificationCompat.Builder(this);
        }

        mBuilder.setAutoCancel(true)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.icon_line)
                .setLargeIcon(_banner)
                .setContentText(message);

        NotificationCompat.BigPictureStyle bigPicStyle = new NotificationCompat.BigPictureStyle(mBuilder);
        bigPicStyle.bigPicture(_banner);
        bigPicStyle.setBigContentTitle(title);
        bigPicStyle.setSummaryText(message);
        mBuilder.setStyle(bigPicStyle);
        mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        Intent resultIntent = intent;//new Intent(this, IntroActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        stackBuilder.addParentStack(IntroActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(100, mBuilder.build());

    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private void generateNotification(String message, final Intent intent) {
        if (!"true".equals(SharedPreferencesUtility.get(SharedPreferencesUtility.Command.IS_NOTIFICATION))) {
            return;
        }

        Intent notificationIntent = intent;//new Intent(this, IntroActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder;
        NotificationManager nm = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        if(Build.VERSION.SDK_INT >= 26){
            NotificationChannel mChannel = new NotificationChannel("rewhiteusers","rewhiteusers",NotificationManager.IMPORTANCE_DEFAULT);
            nm.createNotificationChannel(mChannel);
            builder = new NotificationCompat.Builder(this, mChannel.getId());
        }else{
            builder = new NotificationCompat.Builder(this);
        }

        // 작은 아이콘 이미지.
        builder.setSmallIcon(R.mipmap.icon_line);
        // 알림이 출력될 때 상단에 나오는 문구.
        builder.setTicker("리화이트 - 우리동네배달세탁소");
        // 알림 출력 시간.
        builder.setWhen(System.currentTimeMillis());
        // 알림 제목.
        builder.setContentTitle("리:화이트");
        // 프로그래스 바.
        //builder.setProgress(100, 50, false);
        // 알림 내용.
        builder.setContentText(message);
        // 알림시 사운드, 진동, 불빛을 설정 가능.
        builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
        // 알림 터치시 반응.
        builder.setContentIntent(pendingIntent);
        // 알림 터치시 반응 후 알림 삭제 여부.
        builder.setAutoCancel(true);
        // 우선순위.
        builder.setPriority(Notification.PRIORITY_MAX);

        // 고유ID로 알림을 생성.
        nm.notify(123456, builder.build());

        PushWakeLock.acquireCpuWakeLock(this);
    }


    private int checkActivity() {
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> infos = am.getRunningTasks(50);
        for (int i = 0; i < infos.size(); i++) {
            ActivityManager.RunningTaskInfo info = infos.get(i);
            String classname = info.topActivity.getClassName().toString();

            if (classname.equals(MainActivity.class.getName())) {
                return i;
            }
        }
        return -1;
    }
}
