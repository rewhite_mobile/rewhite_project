package me.rewhite.users;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by marines on 16. 5. 19..
 */
public class MyInstanceIDListenerService extends InstanceIDListenerService {

    private static final String TAG = "MyInstanceIDLS";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        //Intent intent = new Intent(this, RegistrationIntentService.class);
        //(intent);

        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);

//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//        Log.d(TAG, "Refreshed token: " + refreshedToken);
//        // TODO: Implement this method to send any registration to your app's servers.
//        //sendRegistrationToServer(refreshedToken);
//        SharedPreferencesUtility.set(SharedPreferencesUtility.UserInfo.GCM_ID, refreshedToken);
//        Log.e("regId onRegistered", refreshedToken);
    }

    // [END refresh_token]
}