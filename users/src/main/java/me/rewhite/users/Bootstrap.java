package me.rewhite.users;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.util.LruCache;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Logger;
import com.google.android.gms.analytics.Tracker;
import com.kakao.auth.ApprovalType;
import com.kakao.auth.AuthType;
import com.kakao.auth.IApplicationConfig;
import com.kakao.auth.ISessionConfig;
import com.kakao.auth.KakaoAdapter;
import com.kakao.auth.KakaoSDK;
import com.splunk.mint.Mint;
import com.toast.android.paycologin.EnvType;
import com.toast.android.paycologin.LangType;
import com.toast.android.paycologin.PaycoLoginManager;
import com.toast.android.paycologin.PaycoLoginManagerConfiguration;

import java.util.HashMap;

import me.rewhite.users.session.AccessToken;
import me.rewhite.users.util.SharedPreferencesUtility;

public class Bootstrap extends MultiDexApplication {

    private static volatile Bootstrap instance = null;
    private static volatile Activity currentActivity = null;
    private ImageLoader imageLoader;
    HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();
    public static GoogleAnalytics analytics;

    private static final String TAG = Bootstrap.class.getSimpleName();

    public static Context context;

    public static final int USER_INITIATED = 0x1000;
    public static final int APP_INITIATED = 0x2000;
    public static final int SERVER_INITIATED =0x3000;

    public static final String CLIENT_ID = "3RDt1VitfEAShHruWS7J";
    private static final String CLIENT_SECRET = "3aP2GHGNKvyN6hp1WLU2MxQN";
    private static final String SERVICE_PROVIDER_CODE = "FRIENDS";

    private static final EnvType envType = EnvType.REAL;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    /**
     * Google API project id registered to use GCM.
     */
    public static final String SENDER_ID = "526634387754";

    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     * <p/>
     * A single tracker is usually enough for most purposes. In case you do need
     * multiple trackers, storing them all in Application object helps ensure
     * that they are created only once per application instance.
     */
    public enum TrackerName {
        APP_TRACKER // Tracker used only in this app.

    }

    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            analytics = GoogleAnalytics.getInstance(this);
            analytics.setLocalDispatchPeriod(1800);
            GoogleAnalytics.getInstance(this).getLogger().setLogLevel(Logger.LogLevel.VERBOSE);

            Tracker t = analytics.newTracker("UA-65272889-1");
            t.enableExceptionReporting(true);
            t.enableAdvertisingIdCollection(false);
            t.enableAutoActivityTracking(true);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }


    /**
     * singleton 애플리케이션 객체를 얻는다.
     *
     * @return singleton 애플리케이션 객체
     */
    public static Bootstrap getGlobalApplicationContext() {
        if (instance == null)
            throw new IllegalStateException(
                    "this application does not inherit me.rewhite.users.Bootstrap");
        return instance;
    }

    /**
     * 이미지 로더, 이미지 캐시, 요청 큐를 초기화한다.
     */
    @Override
    public void onCreate() {
        super.onCreate();

        // register to be informed of activities starting up
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {

            @Override
            public void onActivityCreated(Activity activity,
                                          Bundle savedInstanceState) {

                // new activity created; force its orientation to portrait
                //activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }

        });

        instance = this;
        Log.e("rewhite users", "Application onCreate");

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        SharedPreferencesUtility.initialize(this);

        KakaoSDK.init(new KakaoSDKAdapter());

        Mint.setApplicationEnvironment(Mint.appEnvironmentRelease);
        Mint.initAndStartSession(instance, "027f4333");
        Mint.enableLogging(true);
        Mint.setUserIdentifier(AccessToken.createFromCache().getUserKey());
        Mint.setLogging(10);
        // Bugsense(Mint Express) Initialize


        //Mint.initAndStartSession(getGlobalApplicationContext(), "1586add0");

        final RequestQueue requestQueue = Volley.newRequestQueue(this);

        ImageLoader.ImageCache imageCache = new ImageLoader.ImageCache() {
            final LruCache<String, Bitmap> imageCache = new LruCache<String, Bitmap>(3);

            @Override
            public void putBitmap(String key, Bitmap value) {
                imageCache.put(key, value);
            }

            @Override
            public Bitmap getBitmap(String key) {
                return imageCache.get(key);
            }
        };

        imageLoader = new ImageLoader(requestQueue, imageCache);

        context = this.getApplicationContext();

        paycoInit();
    }

    private void paycoInit() {
        PaycoLoginManagerConfiguration configuration = new PaycoLoginManagerConfiguration.Builder()
                .setServiceProviderCode(SERVICE_PROVIDER_CODE)
                .setClientId(CLIENT_ID)
                .setClientSecret(CLIENT_SECRET)
                .setAppName(context.getResources().getString(R.string.app_name))
                .setEnvType(envType)
                .setLangType(LangType.KOREAN)
                .setDebug(true)
                .build();

        PaycoLoginManager.getInstance().init(this, configuration);
    }

    /**
     * 이미지 로더를 반환한다.
     *
     * @return 이미지 로더
     */
    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    private static class KakaoSDKAdapter extends KakaoAdapter {
        /**
         * Session Config에 대해서는 default값들이 존재한다.
         * 필요한 상황에서만 override해서 사용하면 됨.
         * @return Session의 설정값.
         */
        @Override
        public ISessionConfig getSessionConfig() {
            return new ISessionConfig() {
                @Override
                public AuthType[] getAuthTypes() {
                    return new AuthType[] {AuthType.KAKAO_LOGIN_ALL};
                }

                @Override
                public boolean isSecureMode() {
                    return false;
                }

                @Override
                public boolean isUsingWebviewTimer() {
                    return false;
                }

                @Override
                public ApprovalType getApprovalType() {
                    return ApprovalType.INDIVIDUAL;
                }

                @Override
                public boolean isSaveFormData() {
                    return true;
                }
            };
        }

        @Override
        public IApplicationConfig getApplicationConfig() {
            return new IApplicationConfig() {
                @Override
                public Context getApplicationContext() {
                    return Bootstrap.getGlobalApplicationContext();
                }
            };
        }
    }

    public static Activity getCurrentActivity() {
        Log.d("Bootstrap", "++ currentActivity : " + (currentActivity != null ? currentActivity.getClass().getSimpleName() : ""));
        return currentActivity;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        Bootstrap.currentActivity = currentActivity;
    }

    /**
     * 애플리케이션 종료시 singleton 어플리케이션 객체 초기화한다.
     */
    @Override
    public void onTerminate() {
        Log.e("Application", "Terminated");
        super.onTerminate();
        instance = null;
    }
}
