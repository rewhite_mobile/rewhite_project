package me.rewhite.users.layout;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Browser;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.net.URISyntaxException;

import me.rewhite.users.activity.PaymentBridgeActivity;

/**
 * Created by marines on 2015. 11. 6..
 */
public class UPlusWebView extends WebViewClient{

    public static final String INTENT_PROTOCOL_START = "intent:";
    public static final String KAKAOTALK_CUSTOM_URL = "kakaotalk:";

    private PaymentBridgeActivity host;
    private WebView webView;

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {

        host = (PaymentBridgeActivity) view.getContext();
        webView = view;
        Log.e("WebViewClient", url);

        if (url.startsWith("rewhite://payment?result=Y")) {
            //view.loadUrl(urlNewString);
            Intent resultData = new Intent();
            resultData.putExtra("RESULT", "Y");
            host.setResult(Activity.RESULT_OK, resultData);
            host.finish();
            return true;
        }else if (url.startsWith("rewhite://payment?result=N")) {
            //view.loadUrl(urlNewString);
            Intent resultData = new Intent();
            resultData.putExtra("RESULT", "N");
            host.setResult(Activity.RESULT_OK, resultData);
            host.finish();
            return true;
        } else if ((url.startsWith("http://") || url.startsWith("https://")) && url.endsWith(".apk")) {
            host.downloadFile(url);
            return super.shouldOverrideUrlLoading(view, url);
        } else if ((url.startsWith("http://") || url.startsWith("https://")) && (url.contains("market.android.com") || url.contains("m.ahnlab.com/kr/site/download"))) {
            Uri uri = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            try {
                host.startActivity(intent);
                return true;
            } catch (ActivityNotFoundException e) {
                return false;
            }
        } else if (url.startsWith("http://") || url.startsWith("https://")) {
            view.loadUrl(url);
            return true;
        }  else if (url != null
                && (url.contains("vguard") || url.contains("droidxantivirus") || url.contains("smhyundaiansimclick://")
                || url.contains("smshinhanansimclick://") || url.contains("smshinhancardusim://") || url.contains("smartwall://") || url.contains("appfree://")
                || url.contains("v3mobile") || url.endsWith(".apk") || url.contains("market://") || url.contains("ansimclick")
                || url.contains("market://details?id=com.shcard.smartpay") || url.contains("shinhan-sr-ansimclick://"))) {
            return host.callApp(url);
        } else if (url.startsWith("smartxpay-transfer://")) {
            boolean isatallFlag = host.isPackageInstalled(host.getApplicationContext(), "kr.co.uplus.ecredit");
            if (isatallFlag) {
                boolean override = false;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, host.getPackageName());

                try {
                    host.startActivity(intent);
                    override = true;
                } catch (ActivityNotFoundException ex) {
                }
                return override;
            } else {
                host.showAlert("확인버튼을 누르시면 구글플레이로 이동합니다.", "확인", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(("market://details?id=kr.co.uplus.ecredit")));
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.putExtra(Browser.EXTRA_APPLICATION_ID, host.getPackageName());
                        host.startActivity(intent);
                        host.overridePendingTransition(0, 0);
                    }
                }, "취소", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                return true;
            }
        } else if (url.startsWith("ispmobile://")) {
            boolean isatallFlag = host.isPackageInstalled(host.getApplicationContext(), "kvp.jjy.MispAndroid320");
            if (isatallFlag) {
                boolean override = false;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, host.getPackageName());

                try {
                    host.startActivity(intent);
                    override = true;
                } catch (ActivityNotFoundException ex) {
                }
                return override;
            } else {
                host.showAlert("확인버튼을 누르시면 구글플레이로 이동합니다.", "확인", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        webView.loadUrl("http://mobile.vpay.co.kr/jsp/MISP/andown.jsp");
                    }
                }, "취소", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                return true;
            }
        } else if (url.startsWith("paypin://")) {
            boolean isatallFlag = host.isPackageInstalled(host.getApplicationContext(), "com.skp.android.paypin");
            if (isatallFlag) {
                boolean override = false;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, host.getPackageName());

                try {
                    host.startActivity(intent);
                    override = true;
                } catch (ActivityNotFoundException ex) {
                }
                return override;
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(("market://details?id=com.skp.android.paypin&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS5za3AuYW5kcm9pZC5wYXlwaW4iXQ..")));
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, host.getPackageName());
                host.startActivity(intent);
                host.overridePendingTransition(0, 0);
                return true;
            }
        } else if (url.startsWith("lguthepay://")) {
            boolean isatallFlag = host.isPackageInstalled(host.getApplicationContext(), "com.lguplus.paynow");
            if (isatallFlag) {
                boolean override = false;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, host.getPackageName());

                try {
                    host.startActivity(intent);
                    override = true;
                } catch (ActivityNotFoundException ex) {
                }
                return override;
            } else {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(("market://details?id=com.lguplus.paynow")));
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID, host.getPackageName());
                host.startActivity(intent);
                host.overridePendingTransition(0, 0);
                return true;
            }
        }else  if(url.startsWith(KAKAOTALK_CUSTOM_URL)){ //custom scheme url
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            try {
                host.startActivity(intent);
            } catch(ActivityNotFoundException e) {
                host.goToPlayStore(url);
            }
            return true;
        } else if (url.startsWith(INTENT_PROTOCOL_START)) { //intent based url
            Intent intent = null;
            try {
                intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
            } catch (URISyntaxException e) {
                e.printStackTrace();
                return false;
            }
            if (intent != null) {
                try {
                    host.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    host.goToPlayStore(url);
                }
            }
        }
        /*else if (Uri.parse(url).getScheme().equals("market") || Uri.parse(url).getScheme().equals("lguthepay")) {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));

                host.startActivity(intent);
                return true;
            } catch (ActivityNotFoundException e) {
                // Google Play app is not installed, you may want to open the app store link
                Uri uri = Uri.parse(url);
                view.loadUrl("http://play.google.com/store/apps/" + uri.getHost() + "?" + uri.getQuery());
                return false;
            }

        }*/ else {
            //view.loadUrl(url);
            return host.callApp(url);
        }

        return true;
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        view.loadData("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>" +
                "</head><body>"+"요청실패 : ("+errorCode+")" + description+"</body></html>", "text/html", "utf-8");
    }
}
