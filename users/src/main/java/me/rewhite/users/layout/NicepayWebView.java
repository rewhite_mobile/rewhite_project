package me.rewhite.users.layout;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;

import me.rewhite.users.activity.PaymentBridgeActivity;
import me.rewhite.users.payments.ClientType;
import me.rewhite.users.util.Utility;

/**
 * Created by marines on 2015. 11. 6..
 */
public class NicepayWebView extends WebViewClient {

    private static final String TAG = NicepayWebView.class.getSimpleName();

    public static final String INTENT_PROTOCOL_START = "intent:";
    public static final String KAKAOTALK_CUSTOM_URL = "kakaotalk:";

    private static final int DIALOG_PROGRESS_WEBVIEW = 0;
    private static final int DIALOG_PROGRESS_MESSAGE = 1;
    private static final int DIALOG_ISP = 2;
    private static final int DIALOG_CARDAPP = 3;

    private static String DIALOG_CARDNM = "";
    // ISP앱에서 결제후 리턴받을 스키마 이름을 설정합니다.
    // AndroidManaifest.xml에 명시된 값과 동일한 값을 설정하십시요.
    // 스키마 뒤에 ://를 붙여주십시요.
    private String WAP_URL = "RewhitePay://";

    private PaymentBridgeActivity host;
    private WebView webView;

    private void startCallPhoneIntent(String url) {
        Intent callPhoneIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        host.startActivity(callPhoneIntent);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {

        host = (PaymentBridgeActivity) view.getContext();
        webView = view;
        Log.e("WebViewClient", url);
        Log.e("NICE","NicePay OverrideUrlLoading : "+url);

        if (url.startsWith("rewhite://payment?result=Y")) {
            //view.loadUrl(urlNewString);
            Intent resultData = new Intent();
            resultData.putExtra("RESULT", "Y");
            host.setResult(Activity.RESULT_OK, resultData);
            host.finish();
            return true;
        }else if (url.startsWith("rewhite://payment?result=N")) {
            //view.loadUrl(urlNewString);
            Intent resultData = new Intent();
            resultData.putExtra("RESULT", "N");
            host.setResult(Activity.RESULT_OK, resultData);
            host.finish();
            return true;
        }else if(url.startsWith(KAKAOTALK_CUSTOM_URL)){ //custom scheme url
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            try {
                host.startActivity(intent);
            } catch(ActivityNotFoundException e) {
                host.goToPlayStore(url);
            }
            return true;
        } else if (url.startsWith(INTENT_PROTOCOL_START)) { //intent based url
            Intent intent = null;
            try {
                intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
            } catch (URISyntaxException e) {
                e.printStackTrace();
                return false;
            }
            if (intent != null) {
                try {
                    host.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    host.goToPlayStore(url);
                }
            }
        }else if( !url.startsWith("http://") && !url.startsWith("https://") && !url.startsWith("javascript:") && !url.startsWith("rewhite:")) {
            Intent intent;

            try{
                Log.d("<INIPAYMOBILE>", "intent url : " + url);
                intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);

                Log.d("<INIPAYMOBILE>", "intent getDataString : " + intent.getDataString());
                Log.d("<INIPAYMOBILE>", "intent getPackage : " + intent.getPackage() );

            } catch (URISyntaxException ex) {
                Log.e("<INIPAYMOBILE>", "URI syntax error : " + url + ":" + ex.getMessage());
                return false;
            }

            Uri uri = Uri.parse(intent.getDataString());
            intent = new Intent(Intent.ACTION_VIEW, uri);

            try {
                host.startActivity(intent);

	    			/*가맹점의 사정에 따라 현재 화면을 종료하지 않아도 됩니다.
	    			    삼성카드 기타 안심클릭에서는 종료되면 안되기 때문에
	    			    조건을 걸어 종료하도록 하였습니다.*/
                if( url.startsWith("ispmobile://"))
                {
                    //host.finish();
                }

            }catch(ActivityNotFoundException e) {
                Log.e("INIPAYMOBILE", "INIPAYMOBILE, ActivityNotFoundException INPUT >> " + url);
                Log.e("INIPAYMOBILE", "INIPAYMOBILE, uri.getScheme()" + intent.getDataString());

                //ISP
                if (url.startsWith("ispmobile://")) {
                    view.loadData("<html><body></body></html>", "text/html", "euc-kr");
                    host.showDialog(DIALOG_ISP);
                    return false;
                }

                //현대앱카드
                else if (intent.getDataString().startsWith("hdcardappcardansimclick://")) {
                    DIALOG_CARDNM = "HYUNDAE";
                    Log.e("INIPAYMOBILE", "INIPAYMOBILE, 현대앱카드설치 ");
                    view.loadData("<html><body></body></html>", "text/html", "euc-kr");
                    host.showDialog(DIALOG_CARDAPP);
                    return false;
                }

                //신한앱카드
                else if (intent.getDataString().startsWith("shinhan-sr-ansimclick://")) {
                    DIALOG_CARDNM = "SHINHAN";
                    Log.e("INIPAYMOBILE", "INIPAYMOBILE, 신한카드앱설치 ");
                    view.loadData("<html><body></body></html>", "text/html", "euc-kr");
                    host.showDialog(DIALOG_CARDAPP);
                    return false;
                }

                //삼성앱카드
                else if (intent.getDataString().startsWith("mpocket.online.ansimclick://")) {
                    DIALOG_CARDNM = "SAMSUNG";
                    Log.e("INIPAYMOBILE", "INIPAYMOBILE, 삼성카드앱설치 ");
                    view.loadData("<html><body></body></html>", "text/html", "euc-kr");
                    host.showDialog(DIALOG_CARDAPP);
                    return false;
                }

                //롯데 모바일결제
                else if (intent.getDataString().startsWith("lottesmartpay://")) {
                    DIALOG_CARDNM = "LOTTE";
                    Log.e("INIPAYMOBILE", "INIPAYMOBILE, 롯데모바일결제 설치 ");
                    view.loadData("<html><body></body></html>", "text/html", "euc-kr");
                    host.showDialog(DIALOG_CARDAPP);
                    return false;
                }
                //롯데앱카드(간편결제)
                else if (intent.getDataString().startsWith("lotteappcard://")) {
                    DIALOG_CARDNM = "LOTTEAPPCARD";
                    Log.e("INIPAYMOBILE", "INIPAYMOBILE, 롯데앱카드 설치 ");
                    view.loadData("<html><body></body></html>", "text/html", "euc-kr");
                    host.showDialog(DIALOG_CARDAPP);
                    return false;
                }

                //KB앱카드
                else if (intent.getDataString().startsWith("kb-acp://")) {
                    DIALOG_CARDNM = "KB";
                    Log.e("INIPAYMOBILE", "INIPAYMOBILE, KB카드앱설치 ");
                    view.loadData("<html><body></body></html>", "text/html", "euc-kr");
                    host.showDialog(DIALOG_CARDAPP);
                    return false;
                }

                //하나SK카드 통합안심클릭앱
                else if (intent.getDataString().startsWith("hanaansim://")) {
                    DIALOG_CARDNM = "HANASK";
                    Log.e("INIPAYMOBILE", "INIPAYMOBILE, 하나카드앱설치 ");
                    view.loadData("<html><body></body></html>", "text/html", "euc-kr");
                    host.showDialog(DIALOG_CARDAPP);
                    return false;
                }

                //신한카드 SMART신한 앱
                else if (intent.getDataString().startsWith("smshinhanansimclick://")) {
                    DIALOG_CARDNM = "SHINHAN_SMART";
                    Log.e("INIPAYMOBILE", "INIPAYMOBILE, Smart신한앱설치");
                    view.loadData("<html><body></body></html>", "text/html", "euc-kr");
                    host.showDialog(DIALOG_CARDAPP);
                    return false;
                }


                /**
                 > 현대카드 안심클릭 droidxantivirusweb://
                 - 백신앱 : Droid-x 안드로이이드백신 - NSHC
                 - package name : net.nshc.droidxantivirus
                 - 특이사항 : 백신 설치 유무는 체크를 하고, 없을때 구글마켓으로 이동한다는 이벤트는 있지만, 구글마켓으로 이동되지는 않음
                 - 처리로직 : intent.getDataString()로 하여 droidxantivirusweb 값이 오면 현대카드 백신앱으로 인식하여
                 하드코딩된 마켓 URL로 이동하도록 한다.
                 */

                //현대카드 백신앱
                else if (intent.getDataString().startsWith("droidxantivirusweb")) {
                    /*************************************************************************************/
                    Log.d("<INIPAYMOBILE>", "ActivityNotFoundException, droidxantivirusweb 문자열로 인입될시 마켓으로 이동되는 예외 처리: ");
                    /*************************************************************************************/

                    Intent hydVIntent = new Intent(Intent.ACTION_VIEW);
                    hydVIntent.setData(Uri.parse("market://search?q=net.nshc.droidxantivirus"));
                    host.startActivity(hydVIntent);

                }


                //INTENT:// 인입될시 예외 처리
                else if (url.startsWith("intent://")) {

                    /**

                     > 삼성카드 안심클릭
                     - 백신앱 : 웹백신 - 인프라웨어 테크놀러지
                     - package name : kr.co.shiftworks.vguardweb
                     - 특이사항 : INTENT:// 인입될시 정상적 호출

                     > 신한카드 안심클릭
                     - 백신앱 : TouchEn mVaccine for Web - 라온시큐어(주)
                     - package name : com.TouchEn.mVaccine.webs
                     - 특이사항 : INTENT:// 인입될시 정상적 호출

                     > 농협카드 안심클릭
                     - 백신앱 : V3 Mobile Plus 2.0
                     - package name : com.ahnlab.v3mobileplus
                     - 특이사항 : 백신 설치 버튼이 있으며, 백신 설치 버튼 클릭시 정상적으로 마켓으로 이동하며, 백신이 없어도 결제가 진행이 됨

                     > 외환카드 안심클릭
                     - 백신앱 : TouchEn mVaccine for Web - 라온시큐어(주)
                     - package name : com.TouchEn.mVaccine.webs
                     - 특이사항 : INTENT:// 인입될시 정상적 호출

                     > 씨티카드 안심클릭
                     - 백신앱 : TouchEn mVaccine for Web - 라온시큐어(주)
                     - package name : com.TouchEn.mVaccine.webs
                     - 특이사항 : INTENT:// 인입될시 정상적 호출

                     > 하나SK카드 안심클릭
                     - 백신앱 : V3 Mobile Plus 2.0
                     - package name : com.ahnlab.v3mobileplus
                     - 특이사항 : 백신 설치 버튼이 있으며, 백신 설치 버튼 클릭시 정상적으로 마켓으로 이동하며, 백신이 없어도 결제가 진행이 됨

                     > 하나카드 안심클릭
                     - 백신앱 : V3 Mobile Plus 2.0
                     - package name : com.ahnlab.v3mobileplus
                     - 특이사항 : 백신 설치 버튼이 있으며, 백신 설치 버튼 클릭시 정상적으로 마켓으로 이동하며, 백신이 없어도 결제가 진행이 됨

                     > 롯데카드
                     - 백신이 설치되어 있지 않아도, 결제페이지로 이동

                     */

                    /*************************************************************************************/
                    Log.d("<INIPAYMOBILE>", "Custom URL (intent://) 로 인입될시 마켓으로 이동되는 예외 처리: ");
                    /*************************************************************************************/

                    try {

                        Intent excepIntent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                        String packageNm = excepIntent.getPackage();

                        Log.d("<INIPAYMOBILE>", "excepIntent getPackage : " + packageNm);

                        excepIntent = new Intent(Intent.ACTION_VIEW);
                        excepIntent.setData(Uri.parse("market://search?q=" + packageNm));

                        host.startActivity(excepIntent);

                    } catch (URISyntaxException e1) {
                        Log.e("<INIPAYMOBILE>", "INTENT:// 인입될시 예외 처리  오류 : " + e1);
                    }

                }
            }
        } else {
            ClientType type = ClientType.getClientType(url);
            if (type.equals(ClientType.BLANK) || type.equals(ClientType.JAVASCRIPT)) {
                return true;
            }

            if (type.equals(ClientType.WEB)) {
                return false;
            }

            // ARS 인증을 위한 전화 연결
            if (url.startsWith("tel:")) {
                startCallPhoneIntent(url);
                return true;
            }

            return handleAppUrl(url);
        }

        return true;
    }

    /********************************************************************
     * Inner methods
     * ******************************************************************/
    private boolean handleAppUrl(String url) {
        Log.e(TAG, " called__test - url=[" + url + "]");
        Intent intent = null;

        if (url.startsWith("intent")) {

            try {
                intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                Log.i("NICE","intent getDataString +++===>"+intent.getDataString());
            } catch (URISyntaxException ex) {
                Log.d(TAG, " URISyntaxException=[" + ex.getMessage() + "]");
                return false;
            }
            // 앱설치 체크를 합니다.
            if (host != null) {
                if (host.getPackageManager().resolveActivity(intent, 0) == null) {
                    String packagename = intent.getPackage();
                    Log.i("intent packagename", packagename);
                    if (packagename != null) {
                        Uri uri = Uri.parse("market://search?q=pname:"+packagename);
                        host.startActivity(new Intent(Intent.ACTION_VIEW, uri));
                        return true;
                    }
                }

                Uri uri = Uri.parse(intent.getDataString());
                intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    host.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Log.i(TAG, " ActivityNotFoundException=[" + e.getMessage() + "]");
                    host.goToPlayStore(url);
                    return false;
                }
            }else{
                Log.e("intent", "host null");
            }


            return true;

        } else {

            // 웹뷰에서 ispmobile  실행한 경우...
            if(url.startsWith("ispmobile")){
                if(Utility.isPackageInstalled(host.getApplicationContext(), "kvp.jjy.MispAndroid320")){
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    host.startActivity(intent);
                    return true;
                }else{
                    host.installISP();
                    return true;
                }

                // 웹뷰에서 계좌이체를 실행한 경우...
            }else if(url.startsWith("kftc-bankpay")){
                if(Utility.isPackageInstalled(host.getApplicationContext(), "com.kftc.bankpay.android")){
                    String sub_str_param = "kftc-bankpay://eftpay?";
                    String reqParam = url.substring(sub_str_param.length());
                    try {
                        reqParam = URLDecoder.decode(reqParam,"utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    reqParam = host.makeBankPayData(reqParam);

                    intent = new Intent(Intent.ACTION_MAIN);
                    intent.setComponent(new ComponentName("com.kftc.bankpay.android","com.kftc.bankpay.android.activity.MainActivity"));
                    intent.putExtra("requestInfo",reqParam);
                    host.startActivityForResult(intent,1);

                    return true;
                }else{
                    host.installKFTC();
                    return true;
                }

                // 웹뷰에서 안심클릭을 실행한 경우...
            }else if (url != null	&& (url.contains("vguard")
                    || url.contains("droidxantivirus")
                    || url.contains("lottesmartpay")
                    || url.contains("smshinhancardusim://")
                    || url.contains("shinhan-sr-ansimclick")
                    || url.contains("v3mobile")
                    || url.endsWith(".apk")
                    || url.contains("smartwall://")
                    || url.contains("appfree://")
                    || url.contains("market://")
                    || url.contains("ansimclick://")
                    || url.contains("ansimclickscard")
                    || url.contains("ansim://")
                    || url.contains("mpocket")
                    || url.contains("mvaccine")
                    || url.contains("market.android.com")
                    || url.startsWith("intent://")
                    || url.contains("samsungpay")
                    || url.contains("droidx3web://")
                    || url.contains("kakaopay")
                    || url.contains("rewhite")
                    || url.contains("rewhitepay")
                    || url.contains("http://m.ahnlab.com/kr/site/download"))) {

                try{
                    try {
                        intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                        Log.i("NICE","intent getDataString +++===>"+intent.getDataString());

                    } catch (URISyntaxException ex) {
                        Log.e("Browser","Bad URI " + url + ":" + ex.getMessage());
                        return false;
                    }


                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    host.startActivity(intent);
                    //return true;

                }catch (Exception e) {
                    Log.i("NICE exception", e.getMessage());
                    return false;
                }
            }else if (url.startsWith(WAP_URL)) {
                // ispmobile에서 결제 완료후 스마트주문 앱을 호출하여 결제결과를 전달하는 경우
                String thisurl = url.substring(WAP_URL.length());
                host.loadURL(thisurl);
                return true;
            }else{
                host.loadURL(url);
                return false; }
        }

        return true;
    }


    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
//        if(host != null){
//            host.showDialog(0);
//        }

    }

    @Override
    public void onLoadResource(WebView view, String url) {
        // TODO Auto-generated method stub
        super.onLoadResource(view, url);
    }

    @Override
    public void onPageFinished(WebView view, String url) {

//        if(host != null) {
//            host.dismissDialog(0);
//        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        view.loadData("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>" +
                "</head><body>"+"요청실패 : (errCode : "+errorCode+") " + description + " | " + failingUrl + "</body></html>", "text/html", "utf-8");
        //host.processCancel();
    }

    @TargetApi(android.os.Build.VERSION_CODES.M)
    @Override
    public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
        // Redirect to deprecated method, so you can use it in all SDK versions
        super.onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
    }
}
