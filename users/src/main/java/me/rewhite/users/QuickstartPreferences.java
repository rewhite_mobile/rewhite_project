package me.rewhite.users;

/**
 * Created by marines on 16. 5. 19..
 */
public class QuickstartPreferences {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
}
