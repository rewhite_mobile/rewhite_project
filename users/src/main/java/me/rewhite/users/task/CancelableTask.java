package me.rewhite.users.task;

import java.util.Collection;

/**
 * @author Hubert He
 * @version V1.0
 * @Description 可撤销任务接口
 * @Createdate 14-9-3 15:53
 */
public interface CancelableTask {

    void addListener(Collection<CancelableTask> cancelableTaskCollection);

    boolean cancel(boolean mayInterruptIfRunning);

    void remove();
}
