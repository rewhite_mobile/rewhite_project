package me.rewhite.users.task;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import com.androidquery.AQuery;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;

import me.rewhite.users.activity.ProfileActivity;
import me.rewhite.users.common.Constants;
import me.rewhite.users.util.DUtil;
import me.rewhite.users.util.SharedPreferencesUtility;

/**
 * Created by marines on 2015. 10. 5..
 */
public class ImageUploadModTask extends MZAsyncTask<HashMap<String, Object>, Void, Boolean, ProfileActivity> {
    final ProfileActivity context;
    private AQuery aq;
    SharedPreferences preferences;
    boolean uploadSuccess;
    private static final String TEMP_PHOTO_FILE = "upload.jpg"; // 임시 저장파일

    String rData;
    ProgressDialog pDialog;
    String token;
    String secret;

    public ImageUploadModTask(ProfileActivity target, SharedPreferences _preferences) {
        super(target);
        context = target;
        aq = new AQuery(context);
        // Enable hardware acceleration if the device has API 11 or above
        aq.hardwareAccelerated11();
        preferences = _preferences;
    }

    public HttpParams getParams() {
        // Tweak further as needed for your app
        HttpParams params = new BasicHttpParams();
        // set this to false, or else you'll get an Expectation Failed: error
        HttpProtocolParams.setUseExpectContinue(params, false);
        return params;
    }

    @Override
    protected Boolean doInBackground(ProfileActivity target, HashMap<String, Object>... params) {
        try {

            File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File f = new File(path, TEMP_PHOTO_FILE);

            f.getAbsolutePath();

            f = new File(path, TEMP_PHOTO_FILE);

            // create an HTTP request to a protected resource
            HttpPost uploadPost = new HttpPost(Constants.API_HOST + Constants.UPLOAD_PROFILE);
            //uploadPost.addHeader("User-Agent", "android");

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            FileBody imgData = new FileBody(f);
            entity.addPart("accessToken", new StringBody(SharedPreferencesUtility.get(SharedPreferencesUtility.UserInfo.ACCESS_TOKEN)));
            entity.addPart("k", new StringBody("1"));
            entity.addPart("file", imgData);

            uploadPost.setEntity(entity);

            DefaultHttpClient httpClient = new DefaultHttpClient(getParams());

            final org.apache.http.HttpResponse response = httpClient.execute(uploadPost);
            final HttpEntity responseEntity = response.getEntity();

            InputStream is = responseEntity.getContent();

            final String data = IOUtils.toString(is);
            rData = data;
            StatusLine st = response.getStatusLine();
            int c = st.getStatusCode();

            Log.i("statusCode", c + "");
            Log.i("Response Data", rData + "");

            uploadSuccess = c < 400;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    protected void onCancelled() {
        uploadSuccess = false;
        Log.i("onCancelled", "Image Upload Cancelled");
    }

    @Override
    protected void onPostExecute(ProfileActivity target, Boolean loginSuccess) {
        Log.i("onPostExecute", "Image Upload  Completed");

        if (uploadSuccess) {
            // 메인메뉴 화면 진입
            Log.i("onPostExecute", "Image Upload Success");
            target.uploadCompleted(rData);
        } else {
            Log.i("onPostExecute", "Image Upload Failed");
            // 로그인 실패 메시지 보여주고 로그인 화면 유지
            DUtil.alertShow(context, "이미지 전송이 실패했습니다.");
        }

    }
}
