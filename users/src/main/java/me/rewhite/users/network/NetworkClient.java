package me.rewhite.users.network;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import me.rewhite.users.session.AccessToken;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.SharedPreferencesUtility;
import me.rewhite.users.util.SharedPreferencesUtility.UserInfo;
import me.rewhite.users.util.StringUtil;


public class NetworkClient {

    private static final String BASE_URL = CommonUtility.SERVER_URL;

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        String gcmId = SharedPreferencesUtility.get(UserInfo.GCM_ID);
        String accessToken = AccessToken.createFromCache().getAccessTokenString();
        if (!StringUtil.isNullOrEmpty(gcmId) && !StringUtil.isNullOrEmpty(accessToken)) {
            client.addHeader("Authorization", "device_token='" + gcmId + "',access_token='" + accessToken + "'");
        }
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

}
