package me.rewhite.users.adapter;

/**
 * Created by marines on 2015. 10. 5..
 */
public class EmailListItem {
    private String name;
    private boolean isSelected = false;

    public String getName() {
        return name;
    }

    public EmailListItem(String name) {
        this.name = name;
    }

}
