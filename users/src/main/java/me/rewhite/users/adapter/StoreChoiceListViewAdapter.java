package me.rewhite.users.adapter;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.activity.StoreChoiceScreen;
import me.rewhite.users.util.CommonUtility;

/**
 * Created by marines on 2015. 10. 5..
 */
public class StoreChoiceListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<StoreListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;

    public StoreChoiceListViewAdapter(Context context, int layout, ArrayList<StoreListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public StoreListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        StoreListItem listviewitem = data.get(position);
        TextView titleText = (TextView) convertView.findViewById(R.id.store_name);
        titleText.setTypeface(CommonUtility.getNanumBarunTypeface());

        ImageView iv = (ImageView)convertView.findViewById(R.id.store_image);
        int storeId = listviewitem.getStoreId();

        //String imageUrl = "http://besuccess.com/wp-content/uploads/2016/03/re1.jpg";
        //aq.id(iv).image(listviewitem.getStoreImage1());
        int resid = ctx.getResources().getIdentifier("store_thumb_0"+(storeId%5+1), "mipmap", "me.rewhite.users");
        aq.id(iv).image(resid);

        TextView addrText = (TextView) convertView.findViewById(R.id.text_address);
        addrText.setText(listviewitem.getStoreAddress1());
        addrText.setTypeface(CommonUtility.getNanumBarunTypeface());

        double distanceMeter = (double)listviewitem.getDistance();
        if(distanceMeter < 1){
            aq.id(R.id.text_distance).text("거리 " + (int)(distanceMeter*1000) + "m" );
        }else{
            String str = String.format("%.1f", distanceMeter);
            aq.id(R.id.text_distance).text("거리 " + str + "km" );
        }


        Log.e("getView : " + position, "=========================");
        Log.e("getView Label : " + position, listviewitem.getStoreName() + "");

        String label = listviewitem.getStoreName();
        String addressDetailName = listviewitem.getStoreAddress2();
        String addressName = listviewitem.getStoreAddress1();

        titleText.setText(label);

        TextView historyText = (TextView) convertView.findViewById(R.id.text_history);
        historyText.setText(listviewitem.getOrderCountForUser()+"");

        aq.id(R.id.content).clicked(this, "selectLocation").tag(position);

        String[] storeAvailableServiceItem = listviewitem.getStoreAvailableService().split(",");
        aq.id(R.id.repair_icon).gone();
        for(int i = 0; i < storeAvailableServiceItem.length; i++){
            // code값 확인 수선:199
            if("202".equals(storeAvailableServiceItem[i])){
                aq.id(R.id.repair_icon).visible();
            }
        }

        return convertView;
    }

    public void selectLocation(View button) {

        final int position = (int) button.getTag();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ((StoreChoiceScreen) ctx).changeStore(position);
            }
        }, 100);
        /*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setCancelable(false).setMessage("선택하신 주소로 주문하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();*/
    }

}
