package me.rewhite.users.adapter;

/**
 * Created by marines on 2015. 10. 5..
 */
public class StoreListItem {

    private String storeName;
    private String storeImage1;
    private String storeAddress1;
    private String storeAddress2;
    private String storeClosingDay;
    private String storeBusinessHours;
    private double distance;
    private int storeRateScore;
    private int storeId;
    private String storeAvailableService;

    public int getOrderCountForUser() {
        return orderCountForUser;
    }

    private int orderCountForUser;

    public String getStoreName() {
        return storeName;
    }

    public String getStoreImage1() {
        return storeImage1;
    }

    public String getStoreAddress1() {
        return storeAddress1;
    }

    public String getStoreAddress2() {
        return storeAddress2;
    }

    public String getStoreClosingDay() {
        return storeClosingDay;
    }

    public String getStoreBusinessHours() {
        return storeBusinessHours;
    }

    public double getDistance() {
        return distance;
    }

    public int getStoreRateScore() {
        return storeRateScore;
    }

    public int getStoreId(){
        return storeId;
    }

    public String getStoreAvailableService(){
        return storeAvailableService;
    }

    public StoreListItem(int storeId, String storeName, String storeImage1, String storeAddress1, String storeAddress2, String storeClosingDay, String storeBusinessHours, double distance,  int storeRateScore, int orderCountForUser, String storeAvailableService) {
        this.storeId = storeId;
        this.storeName = storeName;
        this.storeImage1 = storeImage1;
        this.storeAddress1 = storeAddress1;
        this.storeAddress2 = storeAddress2;
        this.storeClosingDay = storeClosingDay;
        this.storeBusinessHours = storeBusinessHours;
        this.distance = distance;
        this.storeRateScore = storeRateScore;
        this.orderCountForUser = orderCountForUser;
        this.storeAvailableService = storeAvailableService;
    }

}
