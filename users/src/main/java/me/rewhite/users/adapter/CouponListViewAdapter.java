package me.rewhite.users.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.MZValidator;
import me.rewhite.users.util.TimeUtil;

/**
 * Created by marines on 2015. 10. 5..
 */
public class CouponListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<CouponListItem> data;
    private int layout;

    public CouponListViewAdapter(Context context, int layout, ArrayList<CouponListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public String getItem(int position) {
        return data.get(position).getCouponTitle();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }
        CouponListItem listviewitem = data.get(position);
        TextView name = (TextView) convertView.findViewById(R.id.text_title);
        TextView type = (TextView) convertView.findViewById(R.id.text_type);
        TextView point = (TextView) convertView.findViewById(R.id.text_point);
        TextView availableDate = (TextView) convertView.findViewById(R.id.text_available);

        name.setText(listviewitem.getCouponTitle());
        name.setTypeface(CommonUtility.getNanumBarunTypeface());

        long finishDateValue = listviewitem.getUseFinishDateApp();
        long leftTime = TimeUtil.getMinuteComparePastTimeFromNow(finishDateValue);
        Log.e("lefttime", leftTime + " / " + finishDateValue);
        if("Y".equals(listviewitem.getCouponUsed())){
            type.setText("사용완료");
        }else{
            // 173035
            if(leftTime > 0 && leftTime < 1*15*24*60){
                type.setText("마감임박");
            }else if(leftTime <= 0){
                type.setText("기간만료");
            }else{
                type.setText("");
            }
        }
        type.setTypeface(CommonUtility.getNanumBarunLightTypeface());

        String pointString;
        if ("F".equals(data.get(position).getCouponType())) {
            pointString = MZValidator.toNumFormat(listviewitem.getCouponValue());
        } else {
            pointString = listviewitem.getCouponValue() + "%"; //String.format("<font color=\"#5eaeff\"><B>%d", listviewitem.getCouponValue()) + "%</B></font> 할인";
        }
        point.setText(pointString);
        point.setTypeface(CommonUtility.getNanumBarunLightTypeface());

        long availableDateValue = listviewitem.getUseFinishDateApp();
        if (availableDateValue == 0) {
            availableDate.setVisibility(View.GONE);
        } else {
            availableDate.setVisibility(View.VISIBLE);
            String availableString = TimeUtil.getSimpleDisplayDateFormat(TimeUtil.convertTimestampToDate(availableDateValue));
            availableDate.setText("유효기간 " +  availableString);
            //availableDate.setTypeface(CommonUtility.getNanumBarunLightTypeface());
        }



        return convertView;
    }
}
