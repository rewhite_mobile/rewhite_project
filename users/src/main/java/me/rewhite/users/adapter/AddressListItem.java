package me.rewhite.users.adapter;

/**
 * Created by marines on 2015. 10. 5..
 */
public class AddressListItem {

    private String addressName;
    private String doName;
    private String guName;
    private String regionName;

    private boolean isSelected = false;

    public String getAddressName() {
        return addressName;
    }

    public String getDoName() {
        return doName;
    }

    public String getGuName() {
        return guName;
    }

    public String getRegionName() {
        return regionName;
    }

    public AddressListItem(String addressName, String doName, String guName, String regionName) {
        this.addressName = addressName;
        this.doName = doName;
        this.guName = guName;
        this.regionName = regionName;
    }

}
