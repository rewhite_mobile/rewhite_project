package me.rewhite.users.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;
import java.util.Date;

import me.rewhite.users.R;
import me.rewhite.users.activity.EventListActivity;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.TimeUtil;

/**
 * Created by marines on 2015. 10. 5..
 */
public class MyEventListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<MyEventListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;

    public MyEventListViewAdapter(Context context, int layout, ArrayList<MyEventListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public MyEventListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        MyEventListItem listviewitem = data.get(position);
        TextView titleText = (TextView) convertView.findViewById(R.id.event_title);
        titleText.setTypeface(CommonUtility.getNanumBarunTypeface());

        aq.id(R.id.banner_image).getView().setAlpha(1.f);
        aq.id(R.id.event_title).text(listviewitem.getTitle());
        aq.id(R.id.banner_image).image(listviewitem.getImage());

        if("A".equals(listviewitem.getEventType())){
            aq.id(R.id.tag_image).image(R.mipmap.tag_event_type_a);
        }else if("B".equals(listviewitem.getEventType())){
            aq.id(R.id.tag_image).image(R.mipmap.tag_event_type_b);
        }else if("C".equals(listviewitem.getEventType())){
            aq.id(R.id.tag_image).image(R.mipmap.tag_event_type_c);
        }else if("D".equals(listviewitem.getEventType())){
            aq.id(R.id.tag_image).image(R.mipmap.tag_event_type_d);
        }
        String startDate = TimeUtil.convertTimestampToString2(listviewitem.getStartDateApp());
        String finishDate = TimeUtil.convertTimestampToString2(listviewitem.getFinishDateApp());
        Date now = new Date();

        if(listviewitem.getFinishDateApp() - now.getTime() < 0){
            aq.id(R.id.tag_image).image(R.mipmap.tag_event_end);
            aq.id(R.id.banner_image).getView().setAlpha(0.5f);
        }
        aq.id(R.id.event_date).text(startDate + " ~ " + finishDate);

        aq.id(R.id.content).clicked(this, "itemSelected").tag(position);

        return convertView;
    }

    public void itemSelected(View button) {
        final int position = (int) button.getTag();

        MyEventListItem listviewitem = data.get(position);
        ((EventListActivity) ctx).itemSelected(listviewitem.getUrl());
    }
}
