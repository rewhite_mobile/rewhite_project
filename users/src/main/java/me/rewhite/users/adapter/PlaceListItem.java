package me.rewhite.users.adapter;

/**
 * Created by marines on 2015. 10. 5..
 */
public class PlaceListItem {
    private String title;
    private String addressName;
    private String newAddressName;
    private String longitude;
    private String latitude;
    private String addressId;

    private boolean isSelected = false;

    public String getTitle() {
        return title;
    }

    public String getAddressName() {
        return addressName;
    }

    public String getNewAddressName() {
        return newAddressName;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getAddressId() {
        return addressId;
    }

    public PlaceListItem(String title, String addressName, String newAddressName, String longitude, String latitude, String addressId) {
        this.title = title;
        this.addressName = addressName;
        this.newAddressName = newAddressName;
        this.longitude = longitude;
        this.latitude = latitude;
        this.addressId = addressId;
    }

}
