package me.rewhite.users.adapter;

/**
 * Created by marines on 2015. 10. 5..
 */
public class PointListItem {

    private boolean isSaving;
    private int value;
    private String referrer;

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    private String itemType;

    public boolean isSaving() {
        return isSaving;
    }

    public void setIsSaving(boolean isSaving) {
        this.isSaving = isSaving;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public long getAvailableDateApp() {
        return availableDateApp;
    }

    public long getRegDate() {
        return regDate;
    }

    public void setAvailableDateApp(long availableDateApp) {
        this.availableDateApp = availableDateApp;
    }

    public String getMileageTypeApp() {
        return mileageTypeApp;
    }

    public void setMileageTypeApp(String mileageTypeApp) {
        this.mileageTypeApp = mileageTypeApp;
    }

    public int getMileageType() {
        return mileageType;
    }

    public void setMileageType(int mileageType) {
        this.mileageType = mileageType;
    }

    private long availableDateApp;
    private long regDate;
    private String mileageTypeApp;
    private int mileageType;

    public PointListItem(String itemType, boolean isSaving, int value, String referrer, long availableDateApp, long regDate, String mileageTypeApp, int mileageType) {
        this.itemType = itemType;
        this.isSaving = isSaving;
        this.value = value;
        this.referrer = referrer;
        this.availableDateApp = availableDateApp;
        this.regDate = regDate;
        this.mileageTypeApp = mileageTypeApp;
        this.mileageType = mileageType;
    }

}
