package me.rewhite.users.adapter;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;

import me.rewhite.users.fragment.ProfileNewBaseFragment;
import me.rewhite.users.fragment.ProfileNewEmailFragment;
import me.rewhite.users.fragment.ProfileNewNicknameFragment;
import me.rewhite.users.fragment.ProfileNewPhoneFragment;
import me.rewhite.users.fragment.ProfileNewSharecodeFragment;

/**
 * Created by marines on 2017. 5. 3..
 */

public class ProfileNewAdapter extends FragmentPagerAdapter {

    private final SparseArray<ProfileNewBaseFragment> mFragments;


    public ProfileNewAdapter(FragmentManager fm) {
        super(fm);

        mFragments = new SparseArray<>();

        mFragments.put(0, new ProfileNewNicknameFragment());
        mFragments.put(1, new ProfileNewPhoneFragment());
        mFragments.put(2, new ProfileNewEmailFragment());
        mFragments.put(3, new ProfileNewSharecodeFragment());
    }

    @Override
    public ProfileNewBaseFragment getItem(int position) {
        if(position == 1){
            //mFragments.get(position).initialize();
            return mFragments.get(position);
        }else{
            return mFragments.get(position);
        }

    }

    @Override
    public int getCount() {
        return mFragments.size();
    }
}
