package me.rewhite.users.adapter;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.balysv.materialripple.MaterialRippleLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.users.R;
import me.rewhite.users.activity.gsretail.GS25InfoActivity;

/**
 * Created by marines on 2017. 5. 3..
 */

public class GSStoreRecyclerViewAdapter extends RecyclerView.Adapter<GSStoreRecyclerViewAdapter.MyViewHolder>{

    public JSONArray myValues;
    GS25InfoActivity mActivity;
    AQuery aq;

    public GSStoreRecyclerViewAdapter (JSONArray myValues, GS25InfoActivity _activity){
        this.myValues= myValues;
        mActivity = _activity;
    }

    private void setMaterialRippleLayout(View _target){
        MaterialRippleLayout.on(_target)
                .rippleColor(Color.parseColor("#FFffff"))
                .rippleAlpha(0.2f)
                .rippleHover(true)
                .rippleOverlay(true)
                .create();
    }

    public void priceClicked(View button){
        int pos = (int)button.getTag();
        try {
            mActivity.showPriceTable(myValues.getJSONObject(pos).getString("priceGroupId"), myValues.getJSONObject(pos).getInt("partnerId"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void mapClicked(View button){
        int pos = (int)button.getTag();
        mActivity.showMap(pos);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.gs_store_card_view, parent, false);
        aq = new AQuery(listItem);
        return new MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        try {
            String storeName = myValues.getJSONObject(position).getString("partnerStoreName");
            String storeAddress = myValues.getJSONObject(position).getString("partnerStoreAddress1") + ", "+ myValues.getJSONObject(position).getString("partnerStoreAddress2");
            //String storeDistance = myValues.getJSONObject(position).getInt("distance") + "m";
            holder.storeNameTextView.setText(storeName);
            holder.addressTextView.setText(storeAddress);
            //holder.distanceTextView.setText(storeDistance);

            double lat = myValues.getJSONObject(position).getDouble("latitude");
            holder.lat = lat;
            double lng = myValues.getJSONObject(position).getDouble("longitude");
            holder.lng = lng;

            String urlString = "http://maps.google.com/maps/api/staticmap?center="+lat+"%2C"+lng+"&zoom=16&format=png&maptype=roadmap&mobile=true&markers=|color:%23128DD9|label:Marker|"+lat+"%2C"+lng+"&size=1000x300&key=&sensor=false";
            //setMaterialRippleLayout(holder.mapView);
            aq.id(holder.mapView).image(urlString).clicked(this, "mapClicked").tag(position);


            //setMaterialRippleLayout(holder.priceView);
            aq.id(holder.priceView).clicked(this, "priceClicked").tag(position);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public JSONObject getItemValue(int position) throws JSONException {
        return myValues.getJSONObject(position);
    }

    @Override
    public int getItemCount() {
        return myValues.length();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView storeNameTextView;
        private TextView addressTextView;
        //private TextView distanceTextView;
        private ImageView mapView;
        private ImageView priceView;
        public double lat;
        public double lng;

        @SuppressLint({"SetJavaScriptEnabled"})
        public MyViewHolder(View itemView) {
            super(itemView);
            storeNameTextView = (TextView)itemView.findViewById(R.id.text_storename);
            addressTextView = (TextView)itemView.findViewById(R.id.text_address);
            priceView = (ImageView)itemView.findViewById(R.id.btn_price_view);
            //distanceTextView = (TextView)itemView.findViewById(R.id.text_distance);

            mapView = (ImageView)itemView.findViewById(R.id.map_view);
        }

    }
}
