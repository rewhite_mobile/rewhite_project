package me.rewhite.users.adapter;

/**
 * Created by marines on 2015. 10. 5..
 */
public class GSOrderCancelListItem {

    private String titleName;
    private String orderNoLabel;


    public String getTitleName() {
        return titleName;
    }

    public String getOrderNoLabel() {
        return orderNoLabel;
    }


    public GSOrderCancelListItem(String titleName, String orderNoLabel) {
        this.titleName = titleName;
        this.orderNoLabel = orderNoLabel;

    }

}
