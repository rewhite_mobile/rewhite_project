package me.rewhite.users.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.activity.LocationActivity;
import me.rewhite.users.util.CommonUtility;

/**
 * Created by marines on 2015. 10. 5..
 */
public class AddressListViewAdapterV2 extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<AddressListItemV2> data;
    private int layout;
    private Context ctx;
    AQuery aq;

    public AddressListViewAdapterV2(Context context, int layout, ArrayList<AddressListItemV2> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public AddressListItemV2 getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        AddressListItemV2 listviewitem = data.get(position);
        TextView addrText = (TextView) convertView.findViewById(R.id.text_address);
        TextView addrTextSub = (TextView) convertView.findViewById(R.id.text_address_sub);
        addrText.setText(listviewitem.getAddressMainString());
        addrText.setTypeface(CommonUtility.getNanumBarunTypeface());

        addrTextSub.setText(listviewitem.getAddressSubString());
        addrTextSub.setTypeface(CommonUtility.getNanumBarunTypeface());

        aq.id(R.id.btn_select).clicked(this, "listItemSelected").tag(position);

        return convertView;
    }

    public void listItemSelected(View button) {
        int position = (int) button.getTag();

        ((LocationActivity) ctx).itemSelected(1, position);
    }
}
