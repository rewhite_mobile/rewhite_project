package me.rewhite.users.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.androidquery.AQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.rewhite.users.R;
import me.rewhite.users.util.MZValidator;

/**
 * Created by marines on 2015. 10. 16..
 */
public class PickupItemAdapter extends RecyclerView.Adapter<PickupItemAdapter.ViewHolder> {

    private JSONArray mDataset;
    private AQuery aq;
    private Context ctx;
    private int section;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View mView;
        protected int position;
        protected ImageButton btn_add;
        protected ImageButton btn_remove;
        protected TextView text_count;
        protected TextView text_title;
        protected int count;
        protected JSONObject data;

        public ViewHolder(View v) {
            super(v);

            text_count = (TextView) v.findViewById(R.id.text_count);
            text_title = (TextView) v.findViewById(R.id.text_title);

            mView = v;
        }

        public void initFromData(JSONObject data, int position) {
            this.position = position;
            this.data = data;
            try {
                text_title.setText(data.getString("itemTitle"));
                //aq.id(R.id.text_title).text(mDataset.getJSONObject(position).getString("itemTitle"));
                if (!data.isNull("storePrice1")) {
                    text_count.setText(MZValidator.toNumFormat(data.getInt("storePrice1")) + " 원~");
                }

                //aq.id(R.id.btn_add).clicked(this, "addAction").tag(position);
                //aq.id(R.id.btn_remove).clicked(this, "removeAction").tag(position);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PickupItemAdapter(Context ctx, int section, JSONArray myDataset) {
        mDataset = myDataset;
        this.section = section;
        this.ctx = ctx;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PickupItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pickup_item_view, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        aq = new AQuery(v);

        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        //holder.mView.setText(mDataset[position]);

        try {
            holder.initFromData(mDataset.getJSONObject(position), position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.length();
    }


}
