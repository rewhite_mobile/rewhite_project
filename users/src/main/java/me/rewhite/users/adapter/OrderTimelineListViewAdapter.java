package me.rewhite.users.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;

import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.activity.MainActivity;
import me.rewhite.users.util.TimeUtil;

/**
 * Created by marines on 2015. 10. 5..
 */
public class OrderTimelineListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<OrderListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;

    /*
    private String orderId;
    private String orderStatus;
    private String addressSeq;
    private String storeId;
    private String deliveryPrice;
    private String pickupRequestTimeApp;
    private String deliveryRequestTimeApp;
    private String registerDateApp;
    private String orderRequest;
     */

    public OrderTimelineListViewAdapter(Context context, int layout, ArrayList<OrderListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public OrderListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        OrderListItem item = data.get(position);
        //String storeName = item.getStoreName();
        String orderPickupItemMessage = item.getOrderPickupItemMessage();
        //String orderRequest = item.getOrderRequest();

        aq.id(R.id.content).visible();
        aq.id(R.id.review).gone();
        aq.id(R.id.back).image(R.mipmap.timeline_back_v2);
        //Log.e("Long.parseLong(item.getStatusTimeApp())", item.getStatusTimeApp() + "");
        aq.id(R.id.text_date).text(TimeUtil.getTimelineDateString(Long.parseLong(item.getStatusTimeApp())));

        /*
            TODO
            ORDER TYPE별 아이콘표시
         */
        String typeString = "n";
        aq.id(R.id.image_gs).gone();
        aq.id(R.id.image_place).gone();

        if(item.getOrderSubType() == 101){
            aq.id(R.id.text_type).gone();
            typeString = "n";
            aq.id(R.id.image_gs).gone();
        }else if(item.getOrderSubType() == 201){
            //aq.id(R.id.icon_order_type).image(R.mipmap.icon_special_order).visible();
            aq.id(R.id.text_type).text("택배세탁").visible();
            typeString = "d";
            aq.id(R.id.image_gs).gone();
        }else if(item.getOrderSubType() == 105 || item.getOrderSubType() == 104){
            //aq.id(R.id.icon_order_type).image(R.mipmap.icon_quick_order).visible();
            aq.id(R.id.text_type).text("빠른세탁").visible();
            typeString = "t";
            aq.id(R.id.image_gs).gone();
        }else if(item.getOrderSubType() == 151){
            aq.id(R.id.image_gs).visible();
            aq.id(R.id.image_place).gone();
            aq.id(R.id.text_type).gone();
            typeString = "t";
        }else if(item.getOrderSubType() == 152){
            aq.id(R.id.image_gs).gone();
            aq.id(R.id.image_place).visible();
            aq.id(R.id.text_type).gone();
            typeString = "t";
        }

        String resource = "";

        if(item.getOrderSubType() == 151 || item.getOrderSubType() == 152){
            String placeName = "";
            if(item.getOrderSubType() == 151){
                placeName = "편의점";
            }else if(item.getOrderSubType() == 152){
                placeName = "플레이스";
            }

            if ("01".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("접수완료");
                resource = "@mipmap/icon_proc_" + typeString + "_01";
                aq.id(R.id.text_element).text("세탁소 입고 전 상태입니다.");
            } else if ("02".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("접수완료");
                resource = "@mipmap/icon_proc_" + typeString + "_01";
                aq.id(R.id.text_element).text("세탁소 입고 전 상태입니다.");
            } else if ("03".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("세탁소로 이동중");
                resource = "@mipmap/icon_proc_" + typeString + "_02";
                aq.id(R.id.text_element).text("세탁소 입고 후 인수증 발급예정입니다.");
            } else if ("11".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("세탁 준비중");
                resource = "@mipmap/icon_proc_" + typeString + "_03";

                if("N".equals(item.getIsPayment())){
                    aq.id(R.id.text_element).text("세탁물 인수증 확인! 결제는 리화이트 앱에서만 가능해요!");
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2_pay);
                }else{
                    aq.id(R.id.text_element).text("깨끗한 세탁을 위해 준비중입니다.");
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2);
                }
            } else if ("12".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("세탁중");
                resource = "@mipmap/icon_proc_" + typeString + "_04";

                if("N".equals(item.getIsPayment())){
                    aq.id(R.id.text_element).text("세탁물 인수증 확인! 결제는 리화이트 앱에서만 가능해요!");
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2_pay);
                }else{
                    aq.id(R.id.text_element).text("세탁완료 후 "+placeName+"에 입고되면 문자 보내드릴게요~");
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2);
                }
            } else if ("13".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("세탁완료");
                resource = "@mipmap/icon_proc_" + typeString + "_05";

                if("N".equals(item.getIsPayment())){
                    aq.id(R.id.text_element).text("세탁물 인수증 확인! 결제는 리화이트 앱에서만 가능해요!");
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2_pay);
                }else{
                    aq.id(R.id.text_element).text("세탁완료 후 "+placeName+"에 입고되면 문자 보내드릴게요~");
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2);
                }
            } else if ("21".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text(placeName+" 이동준비");
                resource = "@mipmap/icon_proc_" + typeString + "_06";

                if("N".equals(item.getIsPayment())){
                    aq.id(R.id.text_element).text(TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), item.getStoreDeliveryTime().split("\\|")[0]) +
                            placeName+" 입고예정.\n지금 결제해주세요~");
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2_pay);
                }else{
                    aq.id(R.id.text_element).text("세탁완료 후 "+placeName+"에 입고되면 문자 보내드릴게요~");
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2);
                }
            } else if ("22".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("수령대기중");
                resource = "@mipmap/icon_proc_" + typeString + "_07";

                if("N".equals(item.getIsPayment())){
                    aq.id(R.id.text_element).text(placeName+" 입고완료!\n지금 결제해주세요~");
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2_pay);
                }else{
                    aq.id(R.id.text_element).text(placeName+" 입고완료!\n지금 "+placeName+"에 방문해주세요.");
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2);
                }
            } else if ("23".equals(item.getOrderStatus())) {
                aq.id(R.id.content).gone();
                aq.id(R.id.review).visible();
            } else if ("24".equals(item.getOrderStatus())) {
                aq.id(R.id.content).gone();
                aq.id(R.id.review).visible();
            } else if ("92".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("취소");
                aq.id(R.id.text_element).text("재세탁을 위한 수거예정입니다.");
            } else {
                aq.id(R.id.progress_iv).image(R.mipmap.timeline_back_99);
                aq.id(R.id.text_element).text(orderPickupItemMessage);
            }
        }else{
            if ("01".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("수거방문예정");
                resource = "@mipmap/icon_proc_" + typeString + "_01";
                //aq.id(R.id.text_price).text("수거후 표시예정");
                aq.id(R.id.text_element).text(TimeUtil.getDateByMDSH(Long.parseLong(item.getPickupRequestTimeApp()), item.getStoreDeliveryTime().split("\\|")[0]) +
                        "에 세탁물수거를 위해 방문예정입니다.");
            } else if ("02".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("수거방문예정");
                resource = "@mipmap/icon_proc_" + typeString + "_01";
                //aq.id(R.id.progress_iv).image(R.mipmap.timeline_back_01);
                //aq.id(R.id.text_price).text("수거후 표시예정");
                aq.id(R.id.text_element).text("지금 수거를 하러 출발했습니다. 잠시후에 뵙겠습니다.");
            } else if ("03".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("세탁물 입고중");
                resource = "@mipmap/icon_proc_" + typeString + "_02";
                //aq.id(R.id.progress_iv).image(R.mipmap.timeline_back_02);
                //aq.id(R.id.text_price).text("수거후 표시예정");
                aq.id(R.id.text_element).text("세탁물을 수거해서 세탁소로 이동중입니다.");
            } else if ("11".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("세탁 준비중");
                resource = "@mipmap/icon_proc_" + typeString + "_03";
                //aq.id(R.id.progress_iv).image(R.mipmap.timeline_back_03);
                //aq.id(R.id.text_price).text(item.getDeliveryPrice() + " 원");
                aq.id(R.id.text_element).text("지금은 세탁 준비중입니다.\n" + TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), item.getStoreDeliveryTime().split("\\|")[0]) +
                        "에 배송예정입니다.");

                if("N".equals(item.getIsPayment())){
                    //aq.id(R.id.icon_pay_baloon).visible();
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2_pay);
                }else{
                    //aq.id(R.id.icon_pay_baloon).gone();
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2);
                }
            } else if ("12".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("세탁중");
                resource = "@mipmap/icon_proc_" + typeString + "_04";
                //aq.id(R.id.progress_iv).image(R.mipmap.timeline_back_04);
                //aq.id(R.id.text_price).text(item.getDeliveryPrice() + " 원");
                aq.id(R.id.text_element).text("지금은 세탁중입니다.\n" + TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), item.getStoreDeliveryTime().split("\\|")[0]) +
                        "에 배송예정입니다.");

                if("N".equals(item.getIsPayment())){
                    //aq.id(R.id.icon_pay_baloon).visible();
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2_pay);
                }else{
                    //aq.id(R.id.icon_pay_baloon).gone();
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2);
                }
            } else if ("13".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("세탁완료");
                resource = "@mipmap/icon_proc_" + typeString + "_05";
                //aq.id(R.id.progress_iv).image(R.mipmap.timeline_back_05);
                //aq.id(R.id.text_price).text(item.getDeliveryPrice() + " 원");
                aq.id(R.id.text_element).text("세탁이 완료되었습니다.\n" + TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), item.getStoreDeliveryTime().split("\\|")[0]) +
                        "에 배송예정입니다.");

                if("N".equals(item.getIsPayment())){
                    //aq.id(R.id.icon_pay_baloon).visible();
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2_pay);
                }else{
                    //aq.id(R.id.icon_pay_baloon).gone();
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2);
                }
            } else if ("21".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("배송준비중");
                resource = "@mipmap/icon_proc_" + typeString + "_06";
                //.id(R.id.progress_iv).image(R.mipmap.timeline_back_06);
                //aq.id(R.id.text_price).text(item.getDeliveryPrice() + " 원");
                aq.id(R.id.text_element).text("배송을 위해 준비중입니다.\n" + TimeUtil.getDateByMDSH(Long.parseLong(item.getDeliveryRequestTimeApp()), item.getStoreDeliveryTime().split("\\|")[0]) +
                        "에 배송예정입니다.");

                if("N".equals(item.getIsPayment())){
                    //aq.id(R.id.icon_pay_baloon).visible();
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2_pay);
                }else{
                    //aq.id(R.id.icon_pay_baloon).gone();
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2);
                }
            } else if ("22".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("배송중");
                resource = "@mipmap/icon_proc_" + typeString + "_07";
                //aq.id(R.id.progress_iv).image(R.mipmap.timeline_back_07);
                //aq.id(R.id.text_price).text(item.getDeliveryPrice() + " 원");
                aq.id(R.id.text_element).text("지금 세탁물을 돌려드리러 출발했습니다. 잠시후에 뵙겠습니다.");

                if("N".equals(item.getIsPayment())){
                    //aq.id(R.id.icon_pay_baloon).visible();
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2_pay);
                }else{
                    //aq.id(R.id.icon_pay_baloon).gone();
                    aq.id(R.id.back).image(R.mipmap.timeline_back_v2);
                }
            } else if ("23".equals(item.getOrderStatus())) {
            /*
            aq.id(R.id.text_title).text("배송완료");
            aq.id(R.id.text_element).text("배송이 완료되었습니다. 자주 애용해주세요.");
            aq.id(R.id.text_date).text("배송완료");
            */
                aq.id(R.id.content).gone();
                aq.id(R.id.review).visible();
            } else if ("24".equals(item.getOrderStatus())) {
            /*
            aq.id(R.id.text_title).text("배송완료");
            aq.id(R.id.text_element).text("직접 수령해가셨네요. 자주 애용해주세요.");
            aq.id(R.id.text_date).text("배송완료");
            */
                aq.id(R.id.content).gone();
                aq.id(R.id.review).visible();
            } else if ("92".equals(item.getOrderStatus())) {
                aq.id(R.id.text_title).text("취소");
                //aq.id(R.id.progress_iv).image(R.mipmap.timeline_back_99);
                //aq.id(R.id.price_area).gone();
                aq.id(R.id.text_element).text("재세탁을 위한 수거예정입니다.");
            } else {
                //aq.id(R.id.text_status).text("기타");
                aq.id(R.id.progress_iv).image(R.mipmap.timeline_back_99);
                //aq.id(R.id.price_area).gone();
                aq.id(R.id.text_element).text(orderPickupItemMessage);
            }
        }



        int id = ctx.getResources().getIdentifier(resource, "drawable", "me.rewhite.users");
        aq.id(R.id.progress_iv).getImageView().setImageResource(id);
        aq.id(R.id.content).clicked(this, "listItemSelected").tag(position);
        aq.id(R.id.review).clicked(this, "listItemSelected").tag(position);

        return convertView;
    }

    public void listItemSelected(View button) {
        int position = (int) button.getTag();

        ((MainActivity) ctx).itemSelected(data.get(position).getOrderId(), data.get(position).getOrderStatus(), data.get(position).getStoreId(), data.get(position).getOrderSubType());
    }
}
