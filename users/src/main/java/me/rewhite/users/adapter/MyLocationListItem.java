package me.rewhite.users.adapter;

/**
 * Created by marines on 2015. 10. 5..
 */
public class MyLocationListItem {
    private String addressSeq;
    private String addressName;
    private String addressDetailName;
    private String longitude;
    private String latitude;
    private String label;
    private boolean isDefault;

    private boolean isSelected = false;

    public String getAddressSeq() {
        return addressSeq;
    }

    public String getAddressName() {
        return addressName;
    }

    public String getAddressDetailName() {
        return addressDetailName;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public boolean getDefailtStatus() {
        return isDefault;
    }

    public String getLabel() {
        return label;
    }

    public MyLocationListItem(String addressSeq, String label, String addressName, String addressDetailName, String longitude, String latitude, boolean isDefault) {
        this.addressSeq = addressSeq;
        this.label = label;
        this.addressName = addressName;
        this.addressDetailName = addressDetailName;
        this.longitude = longitude;
        this.latitude = latitude;
        this.isDefault = isDefault;
    }

}
