package me.rewhite.users.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.util.CommonUtility;

/**
 * Created by marines on 2015. 10. 5..
 */
public class EmailListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<EmailListItem> data;
    private int layout;

    public EmailListViewAdapter(Context context, int layout, ArrayList<EmailListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public String getItem(int position) {
        return data.get(position).getName();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }
        EmailListItem listviewitem = data.get(position);
        TextView name = (TextView) convertView.findViewById(R.id.text_email);
        name.setText(listviewitem.getName());
        name.setTypeface(CommonUtility.getNanumBarunTypeface());

        return convertView;
    }
}
