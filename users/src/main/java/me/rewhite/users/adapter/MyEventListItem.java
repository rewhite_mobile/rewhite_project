package me.rewhite.users.adapter;

/**
 * Created by marines on 2015. 10. 5..
 */
public class MyEventListItem {

    private int bannerId;
    private String title;
    private String eventType;
    private String url;
    private String image;
    private long startDateApp;

    public int getBannerId() {
        return bannerId;
    }

    public String getTitle() {
        return title;
    }

    public String getEventType() {
        return eventType;
    }

    public String getUrl() {
        return url;
    }

    public String getImage() {
        return image;
    }

    public long getStartDateApp() {
        return startDateApp;
    }

    public long getFinishDateApp() {
        return finishDateApp;
    }

    private long finishDateApp;

    private boolean isSelected = false;


    public MyEventListItem(int bannerId, String title, String eventType, String url, String image, long startDateApp, long finishDateApp) {
        this.bannerId = bannerId;
        this.title = title;
        this.eventType = eventType;
        this.url = url;
        this.image = image;
        this.startDateApp = startDateApp;
        this.finishDateApp = finishDateApp;
    }

}
