package me.rewhite.users.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import me.rewhite.users.R;
import me.rewhite.users.util.CommonUtility;

/**
 * Created by marines on 2015. 10. 8..
 */
public class ListTitleAdapter extends BaseAdapter {

    Context context;
    private LayoutInflater inflater;
    String text;
    BaseAdapter parentAdapter;

    public ListTitleAdapter(Context c, String textToShow) {
        this(c, textToShow, null);
    }

    public ListTitleAdapter(Context c, String textToShow, BaseAdapter dependentAdapter) {
        super();
        this.inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        context = c;
        text = textToShow;

        if (dependentAdapter != null) {
            parentAdapter = dependentAdapter;
        }
    }

    public int getCount() {
        if (parentAdapter != null) {
            if (parentAdapter.getCount() == 0) {
                return 0;
            }
        }
        return 1;
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_section_item, parent, false);
        }

        TextView ttitle = (TextView) convertView.findViewById(R.id.title_text);
        ttitle.setText(text);
        ttitle.setTypeface(CommonUtility.getNanumBarunTypeface());

        return convertView;
    }
}
