package me.rewhite.users.adapter;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by marines on 2015. 10. 5..
 */
public class AddressListItemV2 {

    private String matchFlag;
    private double latitude;
    private double longitude;
    private String city_do;
    private String gu_gun;
    private String eup_myun;
    private String dong;
    private String ri;
    private String bunji;
    private String buildingName;
    private String buildingDong;
    private String newRoadName;
    private String newBuildingIndex;
    private String newBuildingName;
    private String newBuildingCateName;
    private String newBuildingDong;
    private String zipcode;

    public AddressListItemV2(JSONObject data){
        try {
            if("".equals(data.getString("matchFlag"))){
                // 도로명주소
                this.matchFlag = data.getString("newMatchFlag");
                this.latitude = data.getDouble("newLat");
                this.longitude = data.getDouble("newLon");
                this.buildingName = data.getString("buildingName");
                this.buildingDong = data.getString("buildingDong");
                this.newRoadName = data.getString("newRoadName");
                this.newBuildingIndex = data.getString("newBuildingIndex");
                this.newBuildingName = data.getString("newBuildingName");
                this.newBuildingCateName = data.getString("newBuildingCateName");
                this.newBuildingDong = data.getString("newBuildingDong");
            }else{
                // 지번주소
                this.matchFlag = data.getString("matchFlag");
                this.latitude = data.getDouble("lat");
                this.longitude = data.getDouble("lon");
                this.eup_myun = data.getString("eup_myun");
                if("".equals(data.getString("legalDong"))){
                    this.dong = data.getString("adminDong");
                }else{
                    this.dong = data.getString("legalDong");
                }

                this.ri = data.getString("ri");
                this.bunji = data.getString("bunji");
                this.buildingName = data.getString("buildingName");
                this.buildingDong = data.getString("buildingDong");
            }
            this.city_do = data.getString("city_do");
            this.gu_gun = data.getString("gu_gun");
            this.zipcode = data.getString("zipcode");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public AddressListItemV2(String matchFlag, double lat, double lon, String city_do, String gu_gun, String eup_myun, String dong, String ri, String bunji, String buildingName, String buildingDong, String newMatchFlag,
                             double newLat, double newLon, String newRoadName, String newBuildingIndex, String newBuildingName, String newBuildingCateName, String newBuildingDong, String zipcode) {

        if("".equals(matchFlag)){
            // 도로명주소
            this.matchFlag = newMatchFlag;
            this.latitude = newLat;
            this.longitude = newLon;
            this.buildingName = buildingName;
            this.buildingDong = buildingDong;
            this.newRoadName = newRoadName;
            this.newBuildingIndex = newBuildingIndex;
            this.newBuildingName = newBuildingName;
            this.newBuildingCateName = newBuildingCateName;
            this.newBuildingDong = newBuildingDong;
        }else{
            // 지번주소
            this.matchFlag = matchFlag;
            this.latitude = lat;
            this.longitude = lon;
            this.eup_myun = eup_myun;
            this.dong = dong;
            this.ri = ri;
            this.bunji = bunji;
            this.buildingName = buildingName;
            this.buildingDong = buildingDong;
        }

        this.city_do = city_do;
        this.gu_gun = gu_gun;
        this.zipcode = zipcode;
    }

    public double getLongitude(){
        return this.longitude;
    }
    public double getLatitude(){
        return this.latitude;
    }
    public String getBuildingName(){
        return this.buildingName;
    }
    public String getAddressMainString(){
        String addressString = this.city_do + " " + this.gu_gun;
        return addressString;
    }
    public String getAddressSubString(){
        String addressString = "";
        if(this.matchFlag.startsWith("N")){
            // 도로명주소
            if(!"".equals(this.newRoadName)){
                addressString += this.newRoadName + " ";
            }
            if(!"".equals(this.newBuildingIndex)){
                addressString += this.newBuildingIndex + " ";
            }
            if(!"".equals(this.newBuildingName)){
                addressString +=  this.newBuildingName + " ";
            }
            if(!"".equals(this.newBuildingCateName)){
                addressString +=  this.newBuildingCateName + " ";
            }
            if(!"".equals(this.newBuildingDong)){
                addressString +=  this.newBuildingDong + " ";
            }

        }else{
            // 지번주소
            if(!"".equals(this.eup_myun)){
                addressString +=  this.eup_myun + " ";
            }
            if(!"".equals(this.dong)){
                addressString +=  this.dong + " ";
            }
            if(!"".equals(this.ri)){
                addressString +=  this.ri + " ";
            }
            if(!"".equals(this.bunji)){
                addressString +=  this.bunji + " ";
            }
            if(!"".equals(this.buildingName)){
                addressString +=  this.buildingName + " ";
            }
            if(!"".equals(this.buildingDong)){
                addressString +=  this.buildingDong + " ";
            }
        }
        return addressString;
    }
    public String getAddressString(){
        String addressString = this.city_do + " " + this.gu_gun;

        if(this.matchFlag.startsWith("N")){
            // 도로명주소
            if(!"".equals(this.newRoadName)){
                addressString += " " + this.newRoadName;
            }
            if(!"".equals(this.newBuildingIndex)){
                addressString += " " + this.newBuildingIndex;
            }
            if(!"".equals(this.newBuildingName)){
                addressString += " " + this.newBuildingName;
            }
            if(!"".equals(this.newBuildingCateName)){
                addressString += " " + this.newBuildingCateName;
            }
            if(!"".equals(this.newBuildingDong)){
                addressString += " " + this.newBuildingDong;
            }

        }else{
            // 지번주소
            if(!"".equals(this.eup_myun)){
                addressString += " " + this.eup_myun;
            }
            if(!"".equals(this.dong)){
                addressString += " " + this.dong;
            }
            if(!"".equals(this.ri)){
                addressString += " " + this.ri;
            }
            if(!"".equals(this.bunji)){
                addressString += " " + this.bunji;
            }
            if(!"".equals(this.buildingName)){
                addressString += " " + this.buildingName;
            }
            if(!"".equals(this.buildingDong)){
                addressString += " " + this.buildingDong;
            }
        }
        return addressString;
    }

}
