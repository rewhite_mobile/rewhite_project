package me.rewhite.users.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;

import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.activity.OrderHistoryActivity;
import me.rewhite.users.util.TimeUtil;

/**
 * Created by marines on 2015. 10. 5..
 */
public class OrderListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<OrderListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;

    /*
    private String orderId;
    private String orderStatus;
    private String addressSeq;
    private String storeId;
    private String deliveryPrice;
    private String pickupRequestTimeApp;
    private String deliveryRequestTimeApp;
    private String registerDateApp;
    private String orderRequest;
     */

    public OrderListViewAdapter(Context context, int layout, ArrayList<OrderListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public OrderListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        OrderListItem item = data.get(position);
        String storeName = item.getStoreName();
        String orderPickupItemMessage = item.getOrderPickupItemMessage();
        String orderRequest = item.getOrderRequest();

        aq.id(R.id.text_date).text(TimeUtil.convertTimestampToString(Long.parseLong(item.getStatusTimeApp())));
        aq.id(R.id.text_status).text("주문번호 : " + item.getOrderId()).gone();

        int deliveryPrice = Integer.parseInt(item.getDeliveryPrice());
        if(deliveryPrice < 20000){
            deliveryPrice = deliveryPrice + 2000;
        }


        if ("01".equals(item.getOrderStatus())) {
            //aq.id(R.id.text_status).text("수거방문예정");
            aq.id(R.id.back).image(R.mipmap.back_order_history_01);
            aq.id(R.id.text_price).text("수거후 표시예정");
            aq.id(R.id.text_element).text("수거후 표시예정");
        } else if ("02".equals(item.getOrderStatus())) {
            //aq.id(R.id.text_status).text("수거방문예정");
            aq.id(R.id.back).image(R.mipmap.back_order_history_01);
            aq.id(R.id.text_price).text("수거후 표시예정");
            aq.id(R.id.text_element).text("수거후 표시예정");
        } else if ("03".equals(item.getOrderStatus())) {
            //aq.id(R.id.text_status).text("세탁물 입고중");
            aq.id(R.id.back).image(R.mipmap.back_order_history_02);
            aq.id(R.id.text_price).text("수거후 표시예정");
            aq.id(R.id.text_element).text("수거후 표시예정");
        } else if ("11".equals(item.getOrderStatus())) {
            //aq.id(R.id.text_status).text("세탁 준비중");
            aq.id(R.id.back).image(R.mipmap.back_order_history_03);
            aq.id(R.id.text_price).text(deliveryPrice + " 원");
            aq.id(R.id.text_element).text(orderPickupItemMessage);
        } else if ("12".equals(item.getOrderStatus())) {
            //aq.id(R.id.text_status).text("세탁중");
            aq.id(R.id.back).image(R.mipmap.back_order_history_04);
            aq.id(R.id.text_price).text(deliveryPrice + " 원");
            aq.id(R.id.text_element).text(orderPickupItemMessage);
        } else if ("13".equals(item.getOrderStatus())) {
            //aq.id(R.id.text_status).text("세탁완료");
            aq.id(R.id.back).image(R.mipmap.back_order_history_05);
            aq.id(R.id.text_price).text(deliveryPrice + " 원");
            aq.id(R.id.text_element).text(orderPickupItemMessage);
        } else if ("21".equals(item.getOrderStatus())) {
            //aq.id(R.id.text_status).text("배송준비중");
            aq.id(R.id.back).image(R.mipmap.back_order_history_06);
            aq.id(R.id.text_price).text(deliveryPrice + " 원");
            aq.id(R.id.text_element).text(orderPickupItemMessage);
        } else if ("22".equals(item.getOrderStatus())) {
            //aq.id(R.id.text_status).text("배송중");
            aq.id(R.id.back).image(R.mipmap.back_order_history_07);
            aq.id(R.id.text_price).text(deliveryPrice + " 원");
            aq.id(R.id.text_element).text(orderPickupItemMessage);
        } else if ("23".equals(item.getOrderStatus())) {
            //aq.id(R.id.text_status).text("배송완료");
            aq.id(R.id.back).image(R.mipmap.back_order_history_08);
            aq.id(R.id.text_price).text(deliveryPrice + " 원");
            aq.id(R.id.text_element).text(orderPickupItemMessage);
        } else if ("24".equals(item.getOrderStatus())) {
            //aq.id(R.id.text_status).text("방문수령완료");
            aq.id(R.id.back).image(R.mipmap.back_order_history_24);
            aq.id(R.id.text_price).text(deliveryPrice + " 원");
            aq.id(R.id.text_element).text(orderPickupItemMessage);
        } else if ("91".equals(item.getOrderStatus())) {
            //aq.id(R.id.text_status).text("주문취소");
            aq.id(R.id.back).image(R.mipmap.back_order_history_91);
            aq.id(R.id.price_area).gone();
            aq.id(R.id.text_element).text("취소된 내역입니다.");
        } else if ("95".equals(item.getOrderStatus())) {
            //aq.id(R.id.text_status).text("취소");
            aq.id(R.id.back).image(R.mipmap.back_order_history_91);
            aq.id(R.id.price_area).gone();
            aq.id(R.id.text_element).text("취소된 내역입니다.");
        } else if ("96".equals(item.getOrderStatus())) {
            //aq.id(R.id.text_status).text("취소");
            aq.id(R.id.back).image(R.mipmap.back_order_history_91);
            aq.id(R.id.price_area).gone();
            aq.id(R.id.text_element).text("취소된 내역입니다.");
        } else {
            //aq.id(R.id.text_status).text("기타");
            aq.id(R.id.back).image(R.mipmap.back_order_history_99);
            aq.id(R.id.price_area).gone();
            aq.id(R.id.text_element).text("");
        }

        for (int i = 0; i < orderRequest.length(); i++) {
            if ("1".equals(String.valueOf(orderRequest.charAt(i)))) {
                switch (i) {
                    case 0:
                        aq.id(R.id.icon_01).visible();
                        break;
                    case 1:
                        aq.id(R.id.icon_02).visible();
                        break;
                    case 2:
                        aq.id(R.id.icon_03).visible();
                        break;
                    default:
                        break;
                }
            }
        }

        if(item.getOrderSubType() == 151){
            aq.id(R.id.gs_icon).image(R.mipmap.icon_list_gs).visible();
        }else if(item.getOrderSubType() == 152){
            aq.id(R.id.gs_icon).image(R.mipmap.icon_list_place).visible();
        }else{
            aq.id(R.id.gs_icon).gone();
        }


        aq.id(R.id.text_store).text(storeName);


        aq.id(R.id.content).clicked(this, "listItemSelected").tag(position);

        return convertView;
    }

    public void listItemSelected(View button) {
        int position = (int) button.getTag();

        ((OrderHistoryActivity) ctx).itemSelected(position);
    }
}
