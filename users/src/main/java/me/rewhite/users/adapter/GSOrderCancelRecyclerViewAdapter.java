package me.rewhite.users.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import me.rewhite.users.R;

/**
 * Created by marines on 2017. 5. 3..
 */

public class GSOrderCancelRecyclerViewAdapter extends RecyclerView.Adapter<GSOrderCancelRecyclerViewAdapter.MyViewHolder>{

    private Context context;
    private ArrayList<GSOrderCancelListItem> mItems;
    // Allows to remember the last item shown on screen
    private int lastPosition = -1;

    public GSOrderCancelRecyclerViewAdapter(ArrayList<GSOrderCancelListItem> items, Context mContext) {
        mItems = items;
        context = mContext;
    }

    // 필수로 Generate 되어야 하는 메소드 1 : 새로운 뷰 생성
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // 새로운 뷰를 만든다
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gs_order_list_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.titleView.setText(mItems.get(position).getTitleName());
        holder.orderNoView.setText(mItems.get(position).getOrderNoLabel());
    }


    // 필수로 Generate 되어야 하는 메소드 3
    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView titleView;
        private TextView orderNoView;

        @SuppressLint({"SetJavaScriptEnabled"})
        public MyViewHolder(View itemView) {
            super(itemView);
            titleView = (TextView) itemView.findViewById(R.id.title_text);
            orderNoView = (TextView) itemView.findViewById(R.id.orderno_text);


        }

    }
}
