package me.rewhite.users.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.activity.LocationActivity;
import me.rewhite.users.util.CommonUtility;

/**
 * Created by marines on 2015. 10. 5..
 */
public class PlaceListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<PlaceListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;

    public PlaceListViewAdapter(Context context, int layout, ArrayList<PlaceListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public PlaceListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        PlaceListItem listviewitem = data.get(position);
        TextView titleText = (TextView) convertView.findViewById(R.id.text_title);
        titleText.setText(listviewitem.getTitle());
        titleText.setTypeface(CommonUtility.getNanumBarunTypeface());

        TextView addrText = (TextView) convertView.findViewById(R.id.text_address);
        addrText.setText(listviewitem.getAddressName());
        addrText.setTypeface(CommonUtility.getNanumBarunTypeface());

        aq.id(R.id.btn_select).clicked(this, "listItemSelected").tag(position);

        return convertView;
    }

    public void listItemSelected(View button) {
        int position = (int) button.getTag();

        ((LocationActivity) ctx).itemSelected(2, position);
    }
}
