package me.rewhite.users.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import me.rewhite.users.fragment.HorizontalBannerView;

/**
 * Created by marines on 16. 10. 4..
 */
public class AdViewPagerAdapter extends FragmentStatePagerAdapter {
    int MAX_PAGE=5;
    Context context;
    JSONArray data;

    public AdViewPagerAdapter(FragmentManager fm, JSONArray _data) {
        super(fm);
        data = _data;
    }

    @Override
    public Fragment getItem(int position) {
        if(position<0 || MAX_PAGE<=position)
            return null;

        try {
            return HorizontalBannerView.newInstance(position, data.getJSONObject(position).getString(""));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int getCount() {
        return data.length();
    }

}
