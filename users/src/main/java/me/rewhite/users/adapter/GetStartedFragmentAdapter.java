package me.rewhite.users.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;

import java.util.ArrayList;

import me.rewhite.users.R;

public class GetStartedFragmentAdapter extends PagerAdapter {

    private Activity _activity;
    private ArrayList<Integer> _imagePaths;
    private LayoutInflater inflater;
    private AQuery aq;

    // constructor
    public GetStartedFragmentAdapter(Activity activity,
                                     ArrayList<Integer> imagePaths) {
        this._activity = activity;
        this._imagePaths = imagePaths;
    }

    @Override
    public int getCount() {
        return this._imagePaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.tutorial_fragment, container,
                false);
        aq = new AQuery(viewLayout);
        aq.id(R.id.iv).image(_imagePaths.get(position));

        container.addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);

    }
}