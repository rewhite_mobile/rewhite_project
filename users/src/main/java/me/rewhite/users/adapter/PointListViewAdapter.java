package me.rewhite.users.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.MZValidator;
import me.rewhite.users.util.TimeUtil;

/**
 * Created by marines on 2015. 10. 5..
 */
public class PointListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<PointListItem> data;
    private int layout;
    private AQuery aq;

    public PointListViewAdapter(Context context, int layout, ArrayList<PointListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public String getItem(int position) {
        return data.get(position).getMileageTypeApp();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }
        aq = new AQuery(convertView);

        PointListItem listviewitem = data.get(position);
        TextView name = (TextView) convertView.findViewById(R.id.text_title);
        TextView regdate = (TextView) convertView.findViewById(R.id.text_reg_date);
        TextView point = (TextView) convertView.findViewById(R.id.text_point);
        TextView unit = (TextView) convertView.findViewById(R.id.text_unit);
        //TextView availableDate = (TextView) convertView.findViewById(R.id.text_available);
        ImageView backImage = (ImageView) convertView.findViewById(R.id.back_image);

        long regdateValue = listviewitem.getRegDate();
        String regDateString = TimeUtil.getSimpleDisplayDateFormat(TimeUtil.convertTimestampToDate(regdateValue));
        regdate.setText(regDateString);
        regdate.setTypeface(CommonUtility.getNanumBarunTypeface());

        String pointString;

        if("M".equals(data.get(position).getItemType())){
            name.setText(listviewitem.getMileageTypeApp());
            unit.setText("P");
            unit.setTypeface(CommonUtility.getNanumBarunTypeface());
            if (data.get(position).isSaving()) {
                aq.id(R.id.back_image).image(R.mipmap.cell_back_add_nor);
                //pointString = String.format("<font color=\"%s\">+<B>%d </B>P</font>", "#5eaeff", listviewitem.getMileage());
            } else {
                aq.id(R.id.back_image).image(R.mipmap.cell_back_use_nor);
                //pointString = String.format("<font color=\"%s\">-<B>%d </B>P</font>", "#ff5e9e", listviewitem.getMileage());
            }
            point.setText(MZValidator.toNumFormat(listviewitem.getValue()));
        }else{
            unit.setText("R");
            unit.setTypeface(CommonUtility.getNanumBarunTypeface());
            if (data.get(position).isSaving()) {
                aq.id(R.id.back_image).image(R.mipmap.cell_back_coin_nor);
                //pointString = String.format("<font color=\"%s\">+<B>%d </B>P</font>", "#5eaeff", listviewitem.getMileage());
                name.setText("이용권 코인충전");
                point.setText(MZValidator.toNumFormat(listviewitem.getValue()));
            } else {
                aq.id(R.id.back_image).image(R.mipmap.cell_back_coin_nor);
                name.setText("R코인 사용");
                point.setText(MZValidator.toNumFormat(-1*listviewitem.getValue()));
                //pointString = String.format("<font color=\"%s\">-<B>%d </B>P</font>", "#ff5e9e", listviewitem.getMileage());
            }
        }
        name.setTypeface(CommonUtility.getNanumBarunTypeface());

//        long availableDateValue = listviewitem.getAvailableDateApp();
//        if (availableDateValue == 0) {
//            availableDate.setVisibility(View.GONE);
//        } else {
//            availableDate.setVisibility(View.VISIBLE);
//            String availableString = TimeUtil.getSimpleDisplayDateFormat(TimeUtil.convertTimestampToDate(availableDateValue));
//            availableDate.setText(Html.fromHtml(String.format("<font color=\"#b0b0b0\">유효기간 %s</font>", availableString)));
//            availableDate.setTypeface(CommonUtility.getNanumBarunLightTypeface());
//        }


        //point.setTypeface(CommonUtility.getNanumBarunTypeface());

        return convertView;
    }
}
