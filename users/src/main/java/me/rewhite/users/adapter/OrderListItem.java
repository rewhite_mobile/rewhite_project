package me.rewhite.users.adapter;

/**
 * Created by marines on 2015. 10. 12..
 */
public class OrderListItem {

    public String getOrderId() {
        return orderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getAddressSeq() {
        return addressSeq;
    }

    public String getStoreId() {
        return storeId;
    }

    public String getDeliveryPrice() {
        return deliveryPrice;
    }

    public String getPickupRequestTimeApp() {
        return pickupRequestTimeApp;
    }

    public String getDeliveryRequestTimeApp() {
        return deliveryRequestTimeApp;
    }

    public String getRegisterDateApp() {
        return registerDateApp;
    }

    public String getOrderRequest() {
        return orderRequest;
    }

    private String orderId;
    private String orderStatus;
    private String addressSeq;
    private String storeId;
    private String deliveryPrice;
    private String pickupRequestTimeApp;
    private String deliveryRequestTimeApp;
    private String registerDateApp;
    private String orderRequest;
    private String isPayment;
    private int orderSubType;

    public int getOrderSubType(){
        return orderSubType;
    }

    public String getIsPayment(){
        return isPayment;
    }

    public String getStoreDeliveryTime() {
        return storeDeliveryTime;
    }

    private String storeDeliveryTime;

    public String getStoreName() {
        return storeName;
    }

    private String storeName;

    public String getOrderPickupItemMessage() {
        return orderPickupItemMessage;
    }

    private String orderPickupItemMessage;

    public String getStatusTimeApp() {
        return statusTimeApp;
    }

    private String statusTimeApp;

    public OrderListItem(String orderId, String orderStatus, String addressSeq, String storeId, String storeName, String deliveryPrice, String pickupRequestTimeApp,
                         String deliveryRequestTimeApp, String registerDateApp, String orderRequest, String statusTimeApp, String orderPickupItemMessage, String storeDeliveryTime, String isPayment, int orderSubType) {
        this.orderId = orderId;
        this.orderStatus = orderStatus;
        this.addressSeq = addressSeq;
        this.storeId = storeId;
        this.storeName = storeName;
        this.deliveryPrice = deliveryPrice;
        this.pickupRequestTimeApp = pickupRequestTimeApp;
        this.deliveryRequestTimeApp = deliveryRequestTimeApp;
        this.registerDateApp = registerDateApp;
        this.orderRequest = orderRequest;
        this.statusTimeApp = statusTimeApp;
        this.orderPickupItemMessage = orderPickupItemMessage;
        this.storeDeliveryTime = storeDeliveryTime;
        this.isPayment = isPayment;
        this.orderSubType = orderSubType;
    }
}
