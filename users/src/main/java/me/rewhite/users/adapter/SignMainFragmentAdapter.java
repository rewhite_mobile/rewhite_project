package me.rewhite.users.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import me.rewhite.users.activity.SignActivity;
import me.rewhite.users.fragment.SignMainJoinFragment;
import me.rewhite.users.fragment.SignMainLoginFragment;


public class SignMainFragmentAdapter extends FragmentPagerAdapter {

    protected static final String[] CONTENT = new String[]{"LOGIN", "JOIN"};
    private List<Fragment> fragments;
    private SignActivity mSignActivity;

    public SignMainFragmentAdapter(SignActivity mSignActivity) {
        super(mSignActivity.getSupportFragmentManager());
        this.fragments = getFragments();
        this.mSignActivity = mSignActivity;
    }

    private List<Fragment> getFragments() {

        List<Fragment> fList = new ArrayList<Fragment>();

        fList.add(SignMainLoginFragment.newInstance(CONTENT[0]));
        fList.add(SignMainJoinFragment.newInstance(CONTENT[1]));

        return fList;
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return this.fragments.size();
    }

}
