package me.rewhite.users.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.activity.LocationChoiceScreen;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.StringUtil;

/**
 * Created by marines on 2015. 10. 5..
 */
public class LocationChoiceListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<MyLocationListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;

    public LocationChoiceListViewAdapter(Context context, int layout, ArrayList<MyLocationListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public MyLocationListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        MyLocationListItem listviewitem = data.get(position);
        TextView titleText = (TextView) convertView.findViewById(R.id.text_title);
        titleText.setTypeface(CommonUtility.getNanumBarunTypeface());

        TextView addrText = (TextView) convertView.findViewById(R.id.text_address);
        addrText.setText(listviewitem.getAddressName());
        addrText.setTypeface(CommonUtility.getNanumBarunTypeface());

        Log.e("getView : " + position, "=========================");
        Log.e("getView Label : " + position, listviewitem.getLabel() + "");
        Log.e("getView getAddressDetailName : " + position, listviewitem.getAddressDetailName() + "");
        Log.e("getView getAddressName : " + position, listviewitem.getAddressName() + "");

        String label = listviewitem.getLabel();
        String addressDetailName = listviewitem.getAddressDetailName();
        String addressName = listviewitem.getAddressName();

        if (StringUtil.isNullOrEmpty(label) && StringUtil.isNullOrEmpty(addressDetailName)) {
            titleText.setText(addressName);
        } else {
            if (StringUtil.isNullOrEmpty(label) || StringUtil.isEmpty(label)) {
                if(StringUtil.isNullOrEmpty(addressDetailName) || StringUtil.isEmpty(addressDetailName) ){
                    aq.id(titleText).text(addressName);
                    aq.id(addrText).gone();
                    //titleText.setText("입력내용이 없습니다.");
                }else{
                    titleText.setText(addressDetailName);
                    aq.id(addrText).visible();
                }

            } else {
                if (listviewitem.getAddressDetailName().contains(label)) {
                    titleText.setText(addressDetailName);
                } else {
                    if(StringUtil.isNullOrEmpty(addressDetailName) || StringUtil.isEmpty(addressDetailName) ){
                        titleText.setText(label);
                    }else{
                        titleText.setText(addressDetailName + " (" + label + ")");
                    }

                }
            }
        }

        aq.id(R.id.content).clicked(this, "selectLocation").tag(position);



        return convertView;
    }

    public void selectLocation(View button) {

        final int position = (int) button.getTag();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setCancelable(false).setMessage("선택하신 주소로 주문하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ((LocationChoiceScreen) ctx).changeDefaultAddress(position);
                            }
                        }, 100);
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
