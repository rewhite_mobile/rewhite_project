package me.rewhite.users.adapter;

/**
 * Created by marines on 2015. 10. 5..
 */
public class CouponListItem {

    private int couponId;
    private String couponTitle;
    private String couponType;
    private int couponValue;
    private int couponBaseMoney;
    private int couponMaxMoney;
    private long useStartDateApp;
    private long useFinishDateApp;
    private long registerDateApp;

    public String getCouponUsed() {
        return couponUsed;
    }

    public void setCouponUsed(String couponUsed) {
        this.couponUsed = couponUsed;
    }

    private String couponUsed;

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }

    public String getCouponTitle() {
        return couponTitle;
    }

    public void setCouponTitle(String couponTitle) {
        this.couponTitle = couponTitle;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    public int getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(int couponValue) {
        this.couponValue = couponValue;
    }

    public int getCouponBaseMoney() {
        return couponBaseMoney;
    }

    public void setCouponBaseMoney(int couponBaseMoney) {
        this.couponBaseMoney = couponBaseMoney;
    }

    public int getCouponMaxMoney() {
        return couponMaxMoney;
    }

    public void setCouponMaxMoney(int couponMaxMoney) {
        this.couponMaxMoney = couponMaxMoney;
    }

    public long getUseStartDateApp() {
        return useStartDateApp;
    }

    public void setUseStartDateApp(long useStartDateApp) {
        this.useStartDateApp = useStartDateApp;
    }

    public long getUseFinishDateApp() {
        return useFinishDateApp;
    }

    public void setUseFinishDateApp(long useFinishDateApp) {
        this.useFinishDateApp = useFinishDateApp;
    }

    public long getRegisterDateApp() {
        return registerDateApp;
    }

    public void setRegisterDateApp(long registerDateApp) {
        this.registerDateApp = registerDateApp;
    }


    public CouponListItem(int couponId, String couponTitle, String couponType, int couponValue, int couponBaseMoney, int couponMaxMoney, long useStartDateApp, long useFinishDateApp, long registerDateApp, String couponUsed) {
        this.couponId = couponId;
        this.couponTitle = couponTitle;
        this.couponType = couponType;
        this.couponValue = couponValue;
        this.couponBaseMoney = couponBaseMoney;
        this.couponMaxMoney = couponMaxMoney;
        this.useStartDateApp = useStartDateApp;
        this.useFinishDateApp = useFinishDateApp;
        this.registerDateApp = registerDateApp;
        this.couponUsed = couponUsed;
    }

}
