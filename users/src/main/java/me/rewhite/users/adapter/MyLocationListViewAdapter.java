package me.rewhite.users.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import me.rewhite.users.R;
import me.rewhite.users.activity.LocationActivity;
import me.rewhite.users.util.CommonUtility;
import me.rewhite.users.util.StringUtil;

/**
 * Created by marines on 2015. 10. 5..
 */
public class MyLocationListViewAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<MyLocationListItem> data;
    private int layout;
    private Context ctx;
    AQuery aq;

    public MyLocationListViewAdapter(Context context, int layout, ArrayList<MyLocationListItem> data) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.layout = layout;
        this.ctx = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public MyLocationListItem getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(layout, parent, false);
        }

        aq = new AQuery(convertView);

        MyLocationListItem listviewitem = data.get(position);
        TextView titleText = (TextView) convertView.findViewById(R.id.text_title);
        titleText.setTypeface(CommonUtility.getNanumBarunTypeface());

        String currentLocationAddress = "";
        if (StringUtil.isNullOrEmpty(listviewitem.getLabel()) && StringUtil.isNullOrEmpty(listviewitem.getAddressDetailName())) {
            currentLocationAddress = listviewitem.getAddressName();
        } else {
            if (StringUtil.isNullOrEmpty(listviewitem.getLabel()) || "null".equals(listviewitem.getLabel())) {

                if (StringUtil.isNullOrEmpty(listviewitem.getAddressDetailName())) {
                    currentLocationAddress = listviewitem.getAddressName();
                } else {
                    currentLocationAddress = listviewitem.getAddressDetailName();
                }
            } else {
                if (listviewitem.getAddressDetailName().contains(listviewitem.getLabel())) {
                    currentLocationAddress = listviewitem.getAddressDetailName();
                } else {
                    if (StringUtil.isNullOrEmpty(listviewitem.getAddressDetailName())) {
                        currentLocationAddress = listviewitem.getAddressName();
                    } else {
                        currentLocationAddress = listviewitem.getAddressDetailName() + " (" + listviewitem.getLabel() + ")";
                    }

                }
            }
        }

        currentLocationAddress = currentLocationAddress.replace("대한민국 ", "");
        currentLocationAddress = currentLocationAddress.replace("서울특별시", "서울시");
        titleText.setText(currentLocationAddress);

        TextView addrText = (TextView) convertView.findViewById(R.id.text_address);
        addrText.setText(listviewitem.getAddressName().replace("대한민국 ", ""));
        addrText.setTypeface(CommonUtility.getNanumBarunTypeface());

        aq.id(R.id.btn_remove).clicked(this, "removeItemSelected").tag(position);

        return convertView;
    }

    public void removeItemSelected(View button) {
        final int position = (int) button.getTag();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setCancelable(false).setMessage("선택하신 주소를 삭제하시겠어요?")
                .setPositiveButton("네", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ((LocationActivity) ctx).itemRemoveSelected(data.get(position).getAddressSeq());
                            }
                        }, 100);
                    }
                }).setNegativeButton("아니오", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }
}
