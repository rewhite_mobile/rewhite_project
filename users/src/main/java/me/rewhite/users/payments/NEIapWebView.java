/*
 * @(#)NEIapWebView.java $version 2015. 12. 18.
 *
 * Copyright 2015 NHN Entertainment Corp. All rights Reserved. 
 * NHN Entertainment PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package me.rewhite.users.payments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.net.URISyntaxException;

public class NEIapWebView extends WebView {
	private static final String TAG = NEIapWebView.class.getSimpleName();
	
	private Activity activity;
	
	/********************************************************************
	 * NEIapWebView Initializer
	 * ******************************************************************/
	public NEIapWebView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	@SuppressLint({ "SetJavaScriptEnabled", "NewApi"})
	private void init() {
		clearCache(true);
		setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		
		getSettings().setAppCacheEnabled(true);
		getSettings().setDomStorageEnabled(true);
		getSettings().setJavaScriptEnabled(true);
		getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		
		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			// https -> http 호출 허용.
			getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
	
			// 서드파티 쿠키 허용.
			CookieManager cookieManager = CookieManager.getInstance();
			cookieManager.setAcceptCookie(true);
			cookieManager.setAcceptThirdPartyCookies(this, true);
		}

		setWebChromeClient(new WebChromeClient());
		setWebViewClient(new IapWebViewClient());
	}
	
	public void loadUrl(Activity activity, String url) {
		if (activity == null || url == null) {
			new Throwable("NEIapWebView::loadIapUrl - activity, url, scheme values must valid!");
			return;
		}
		
		this.activity = activity;
		loadUrl(url);
	}

	/********************************************************************
	 * Custom WebViewClient
	 * ******************************************************************/
	private class IapWebViewClient extends WebViewClient {
		
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			ClientType type = ClientType.getClientType(url);
			if (type.equals(ClientType.BLANK) || type.equals(ClientType.JAVASCRIPT)) {
				return true;
			}
			
			if (type.equals(ClientType.WEB)) {
				return false;
			} 
			
			// ARS 인증을 위한 전화 연결
			if (url.startsWith("tel:")) {
				startCallPhoneIntent(url);
				return true;
			}
			
			// 앱 실행
			return handleAppUrl(url);
		}
	}
	
	/********************************************************************
	 * Inner methods
	 * ******************************************************************/
	private boolean handleAppUrl(String url) {
		Log.d(TAG, " called__test - url=[" + url + "]");

		if (url.startsWith("intent")) {
			Intent intent = null;

			try {
				intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
			} catch (URISyntaxException ex) {
				Log.d(TAG, " URISyntaxException=[" + ex.getMessage() + "]");
				return false;
			}

			// 앱설치 체크를 합니다.
			if (activity != null) {
				if (activity.getPackageManager().resolveActivity(intent, 0) == null) {
					String packagename = intent.getPackage();
					if (packagename != null) {
						activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pname:" + packagename)));
						return true;
					}
				}

				intent = new Intent(Intent.ACTION_VIEW, Uri.parse(intent.getDataString()));
				try {
					activity.startActivity(intent);
				} catch (ActivityNotFoundException e) {
					Log.d(TAG, " ActivityNotFoundException=[" + e.getMessage() + "]");
					return false;
				}
			}
		} else {
			try {
				if (activity != null) {
					activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
				}
			} catch (Exception e) {
				// 어플이 설치 안되어 있을경우 오류 발생. 해당 부분은 업체에 맞게 구현
				if (activity != null) {
					Toast.makeText(activity, "어플을 설치해 주세요.", Toast.LENGTH_LONG).show();
				}
			}
		}

		return true;
	}

	private void startCallPhoneIntent(String url) {
		Intent callPhoneIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		if (activity != null) {
			activity.startActivity(callPhoneIntent);
		} else {
			getContext().startActivity(callPhoneIntent);
		}	
	}
	
}
