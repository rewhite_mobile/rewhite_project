/*
 * @(#)ClientType.java $version 2015. 12. 18.
 *
 * Copyright 2015 NHN Entertainment Corp. All rights Reserved. 
 * NHN Entertainment PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package me.rewhite.users.payments;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum ClientType {
	WEB, JAVASCRIPT, BLANK, APP;

	public static ClientType getClientType(String url) {
		Matcher httpMatcher = Pattern.compile(
				"^(https?):\\/\\/([^:\\/\\s]+)(/?)").matcher(url);
		if (httpMatcher.find()) {
			return ClientType.WEB;
			
		} else if (url.startsWith("javascript:")) {
			return ClientType.JAVASCRIPT;

		} else if (url.startsWith("about:blank")) {
			return ClientType.BLANK;

		} else {
			return ClientType.APP;
		}
	}
}