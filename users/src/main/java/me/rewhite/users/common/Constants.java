package me.rewhite.users.common;

public class Constants {

    public static String PREF_NAME = "REWHITE_USERS";
    public static String SMS_SENDER_NO = "15442951";
    public static String SK_APP_KEY = "71b2bcdb-dd2c-4ef9-8e78-c3021e96e5f1";
    public static String DAUM_API_KEY = "92b6aebead05fc512067121f10357729";//"cb3a127554215958184db75e3e0ee7e2";

    public static String PACKAGE_NAME = "me.rewhite.users";
    public static String DB_NAME = "";

    public static String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAo2O4D/XuIHG8jcLhFMugq51r9l22kuOH/lnHVXv0y2yTtkVdLdU10Vq2XyDKeMedPyA4KF82GQgqwaMKEHv2Mo+Nf2wWGBfsRWsVfm4p8PzXKBaRAS61FQ6R6N1OPHp5Dk/+qzfvEXNzXWKOs9MlG22NMo/Led96zD9Y6K0TFghVWqDarfxK1YArBvnZmm6YkAocxI3fsebvLKStG6+qqkIXdWJeIFDDlYYEYBZtIAv308kYuzhAHSj5IKrHHSoQNKgiRIjcHlC5SWpa5k8cquOmS8+tvYov9+JIAgqZS6uWayrvVzxJhO3DL/NFb0zPHstyxTyVt1EGleCB4hyY8QIDAQAB";

    public static String DEBUG = "debug";
    public static String REAL = "real";
    public static String DEBUG_MODE = Constants.REAL;

    public static String RWWW_HOST = "https://www.rewhite.me";
    public static String TWWW_HOST = "http://twww.rewhite.me";
    public static String RAPI_HOST = "https://api.rewhite.me";
    public static String TAPI_HOST = "https://tapi.rewhite.me:443";
    public static String STORE_HOST = "http://www.rewhite.me/shop/";

    public static String WWW_HOST = (Constants.REAL.equals(DEBUG_MODE))?Constants.RWWW_HOST:Constants.TWWW_HOST;
    public static String API_HOST = (Constants.REAL.equals(DEBUG_MODE))?Constants.RAPI_HOST:Constants.TAPI_HOST;

    public static String VERSION_CHECK = "/Management/GetAppInfo";
    public static String EVENT_CHECK   = "/Management/AppBanner";
    public static String BANNER_LOAD   = "/Management/EventBannerList";


    // ACCOUNT
    public static String MEMBER_REGISTER = "/User/Join";
    public static String LOGIN = "/User/Login";
    public static String AUTH_ACCESS_TOKEN = "/User/VerifyAccessToken";

    /*
    PAYCO 회원정보 조회
     */
    public static String PAYCO_GET_PROFILE = "https://apis3.krp.toastoven.net/payco/friends/getMemberProfileByFriendsToken.json"; // "https://apis3.krp.toastoven.net/payco/friends/getIdNoByFriendsToken.json";

    /*
      TODO 신한카드Token 인증
    */
    public static String SHINHAN_AUTH_ACCESSTOKEN = "/v2/Partner/ShinhanCardTokenVerification";
    public static String USER_INFO = "/User/UserInfo";
    public static String USERINFO_MOD = "/User/ModUserInfo";
    public static String UPLOAD_PROFILE = "/User/ProfileImage";
    public static String MOD_PASSWORD = "/User/PasswordChange";
    public static String WITHDRAW = "/User/SetUserSession";
    public static String PASSWORD_RESET = "/User/PasswordReset";
    public static String SET_THIRDS_AGREE = "/v2/User/SetUserAgreeThird";


    public static String CONFIRM_CTN_AUTHCODE = "/User/VerifyPhone";
    public static String REQUEST_CTN_AUTH = "/User/GetPhoneVerifyCode";

    public static String REMOVE_ACCOUNT = "";
    public static String CHANGE_NICKNAME = "";
    public static String CHANGE_PASSWORD = "";
    public static String CHANGE_PHONENO = "";

    // GS Order
    public static String GS_STORE_LIST = "/v3/User/PartnerStoreDistance";
    public static String GS_STORE_VALID = "/v3/User/PartnerStoreView";
    public static String GS_ORDER_CREATE = "/v3/User/PartnerOrder";
    public static String GS_ORDER_MOD = "/v3/User/PartnerOrderComplete";

    // ORDER
    public static String ORDER_TIMETABLE = "/v2/User/OrderAvailableTime";
    public static String ORDER_PRICE = "/User/StorePrice";
    public static String ORDER_PRICE_USER = "/User/StorePriceForUser";
    public static String ORDER_PRICE_USER_V2 = "/v2/User/StorePriceForUser";
    public static String ORDER_PRICE_USER_V3 = "/v3/User/PartnerPriceForUser";
    public static String FIND_NEAR_STORE = "/v2/User/Distance";
    public static String SHOW_STORE_INFO = "/User/StoreInformation";
    public static String ADD_ALARM_NEW = "/User/StoreOpenNotification";
    public static String ORDER_LIST = "/User/OrderList";
    public static String ADD_ORDER = "/User/Order";
    public static String ORDER_DETAIL = "/User/OrderView";
    public static String GET_PUSH_SETTING = "/User/GetPushSettings";
    public static String SET_PUSH_SETTING = "/User/SetPushSettings";
    public static String ORDER_MOD = "/User/OrderUpdate";
    public static String ORDER_CANCEL = "/User/OrderStatus";
    public static String ORDER_RECEIPT = WWW_HOST + "/payment/receipt";
    public static String ORDER_AVAILABLE_CHECK = "/User/OrderAvailableTime";
    public static String ORDER_PARCEL = "/User/OrderDelivery";
    public static String ORDER_REVIEW = "/User/StoreRateScore";
    public static String ORDER_ADD_REQ_MESSAGE = "/v3/User/OrderUpdatePickupRequestMessage";



    // Location

    /* Deprecated */
    //public static String GEO_FENCING_SEARCH = "https://apis.skplanetx.com/tmap/geofencing/regions";
    //public static String FULLTEXT_GEOCODING_SEARCH = "https://apis.skplanetx.com/tmap/geo/fullAddrGeo";
    //public static String GEOCODING = "https://apis.skplanetx.com/tmap/geo/geocoding";
    public static String GEO_SEARCH = "https://apis.daum.net/local/v1/search/keyword.json";
    public static String TRANS_GEO_ADDRESS = "https://apis.daum.net/local/geo/coord2detailaddr";

    // SKT API
    // http://tmapapi.sktelecom.com/main.html#webservice/guide/webserviceGuide.guide3
    public static String SKT_FULLTEXT_GEOCODING_SEARCH = "https://api2.sktelecom.com/tmap/geo/fullAddrGeo";
    public static String SKT_GEO_FENCING_SEARCH = "https://api2.sktelecom.com/tmap/geofencing/regions";
    public static String SKT_GEOCODING = "https://api2.sktelecom.com/tmap/geo/geocoding";


    // Location V2 (Naver)
    // Overview : https://developers.naver.com/docs/map/overview/
    public static String GEO_FENCING_SEARCH_V2 = "https://openapi.naver.com/v1/map/geocode"; // json (주소 -> 좌표)
    public static String GEOCODING_V2 = "https://openapi.naver.com/v1/map/reversegeocode"; // json (좌표 -> 주소)
    public static String STATICMAP_V2 = "https://openapi.naver.com/v1/map/staticmap.bin"; // JPG,PNG (해당좌표 이미지 출력)

    public static String GET_LOCATION_LIST = "/User/GetLocationList";
    public static String ADD_LOCATION = "/User/AddLocation";
    public static String MOD_LOCATION = "/User/ModLocation";
    public static String REMOVE_LOCATION = "/User/RemoveLocation";

    public static String HTML_YELLOW = "http://pf.kakao.com/_Nxjvkxd";
    public static String HTML_NOTICE = "http://www.rewhite.me/customer/notice";
    public static String HTML_FAQ = "http://www.rewhite.me/customer/faq";
    public static String TERMS_USE = WWW_HOST + "/customer/tos";
    public static String TERMS_PRIVACY = WWW_HOST + "/customer/privacy";
    public static String TERMS_PRIVATE_SUPPORT = WWW_HOST + "/customer/agreementthird";
    public static String TERMS_PRIVATE_THIRDS_SUPPORT = WWW_HOST + "/customer/agreementthird#AddItem";
    public static String TERMS_LOCATION = WWW_HOST + "/customer/location";
    public static String TERMS_OSS = WWW_HOST + "/customer/opensource";

    // POINT
    public static String MILEAGE_LIST = "/User/GetUserMileageList";
    public static String COIN_LIST = "/v2/User/GetUserPrepaymentV2List";
    public static String COUPON_LIST = "/User/GetCouponIssueList";
    public static String ADD_COUPON  = "/User/SetCouponNumber";
    public static String ADD_COIN  = "/v2/User/SetPartnerRequestV2Use";

    // Event
    public static String EVENT_LIST = "/Management/EventBannerList";

    // Payment
    public static String PAYMENT_PRECODE_REQ = "/User/SetPaymentAppMessage";
    public static String PAYMENT_FACE = "/User/SetOrderPaymentDirect";
    public static String CHECK_ORDER = WWW_HOST + "/payment/checkorder";


    public static String QNA_SUBMIT = "/User/SetQNA";
    public static String SEND_TPHONE = "/v2/Delivery/SendTPhone";
}
