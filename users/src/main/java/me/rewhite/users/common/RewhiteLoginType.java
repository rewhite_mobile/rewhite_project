package me.rewhite.users.common;

/**
 * Created by marines on 2015. 10. 23..
 */
public class RewhiteLoginType {
    public static String TYPE_EMAIL = "EM";
    public static String TYPE_FACEBOOK = "FB";
    public static String TYPE_KAKAO = "KK";
    public static String TYPE_GOOGLE = "GG";
    public static String TYPE_PAYCO = "PC";
}
