package me.rewhite.users.common;

public class RewhiteCMD {
    public static final String EVENT_NEW_NOTE = "new_note";

    public static final String LOAD = "load";
    public static final String START_SYNC_PROCESS = "start_synchronize";
    public static final String INITIALIZE_LOAD = "initialize_load";
    public static final String TIMESTAMP_SETUP = "timestamp_initialize";
    public static final String UPDATE_LIST = "update_list";
    public static final String UPDATE_MEMO = "update_memo";
    public static final String UPDATE_FOLDER = "update_folder";
    public static final String INITIALIZE_MERGE = "initialize_merge";
    public static final String REQUEST_SYNC_OTHER = "request_sync_other";
    public static final String PASSWORD_CHECK = "password_check";

    public static final String TERMS = "terms";
    public static final String TERMS_USE = "use";
    public static final String TERMS_PERSONAL = "personal";

    public static final String VERSION = "current_version";
    public static final String TYPE_BOARD = "type_info";
    public static final String NOTICE = "notice";
    public static final String FAQ = "faq";
    public static final String INQUIRE = "inquire";

    public static final String JSON_RESULT = "result";

    public static final String JSON_VERSION = "version";

    public static final String JSON_PAGE = "page";

    public static final String JSON_BOARD = "board";
    public static final String JSON_TYPE_LIST = "type_list";
    public static final String JSON_BOARD_INDEX = "idx";
    public static final String JSON_BOARD_TYPE = "type";
    public static final String JSON_BOARD_TITLE = "title";
    public static final String JSON_BOARD_CONTENT = "content";
    public static final String JSON_BOARD_IMAGE_PATH = "image_path";
    public static final String JSON_BOARD_REGDT = "regdt";

    public static final String JSON_INQUIRE_TYPE = "type";
    public static final String JSON_INQUIRE_NAME = "name";
    public static final String JSON_INQUIRE_EMAIL = "email";
    public static final String JSON_INQUIRE_TITLE = "title";
    public static final String JSON_INQUIRE_CONTENT = "content";

    public static final String MEMBER_MOD_PHONENO = "mod_phoneno";
    public static final String MEMBER_MOD_NICKNAME = "mod_nickname";
    public static final String MEMBER_MOD_PASSWORD = "mod_password";
    public static final String MEMBER_REMOVE_ACCOUNT = "remove_account";

    public static final String MEMBER_AUTH_MOBILE = "auth_mobile";

    public static final String JSON_PASSWORD = "password";
    public static final String JSON_TERMS_CONTENT = "content";

    public static final String JSON_KEY = "key";
    public static final String JSON_VALUE = "value";

    public static final String USER_INFO = "user_info";
    public static final String USER_EXPIRE_DATE = "expire_date";
    public static final String USER_NICKNAME = "nickname";
    public static final String USER_MOBILE = "mobile";
    public static final String PRODUCT_INFO = "product_info";
    public static final String PRODUCT_INFO_PERIOD = "period";
}
