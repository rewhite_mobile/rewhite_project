package me.rewhite.users.common;

import android.app.Activity;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;

/**
 * Created by marines on 2015. 10. 22..
 */
public class BaseFragmentActivity extends FragmentActivity {
    protected static Activity self;
    View decorView;

    @Override
    protected void onResume() {
        super.onResume();
        Bootstrap.setCurrentActivity(this);
        self = BaseFragmentActivity.this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR);//.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }

        decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(View.SYSTEM_UI_LAYOUT_FLAGS);
    }

    @Override
    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    private void clearReferences() {
        Activity currActivity = Bootstrap.getCurrentActivity();
        if (currActivity != null && currActivity.equals(this)) {
            Bootstrap.setCurrentActivity(null);
        }
    }

    protected static void showWaitingDialog() {
        //WaitingDialog.showWaitingDialog(self);
    }

    protected static void cancelWaitingDialog() {
        //WaitingDialog.cancelWaitingDialog();
    }

    protected void redirectLoginActivity() {
//        final Intent intent = new Intent(this, SampleLoginActivity.class);
//        startActivity(intent);
//        finish();
    }

    protected void redirectSignupActivity() {
//        final Intent intent = new Intent(this, SampleSignupActivity.class);
//        startActivity(intent);
//        finish();
    }
}
