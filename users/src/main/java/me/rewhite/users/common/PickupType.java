package me.rewhite.users.common;

/**
 * Created by kimhyunwoo on 15. 8. 10..
 */
public class PickupType {
    public static int PICKUPTYPE_CLEAN = 0;
    public static int PICKUPTYPE_REPAIR = 1;
    public static int PICKUPTYPE_PARCEL = 2;
    public static int PICKUPTYPE_TODAY = 3;
    public static int PICKUPTYPE_TOMORROW = 4;
    public static int PICKUPTYPE_GS25 = 5;
}
