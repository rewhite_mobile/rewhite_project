package me.rewhite.users.common;

/**
 * Created by marines on 2015. 10. 23..
 */
public class RewhitePaymentType {
    public static String FACE_CARD = "P91";
    public static String FACE_CASH = "P92";
}
