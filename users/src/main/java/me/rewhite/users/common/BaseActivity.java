package me.rewhite.users.common;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import com.google.android.gms.analytics.Tracker;

import me.rewhite.users.Bootstrap;
import me.rewhite.users.R;

/**
 * Created by marines on 2015. 10. 22..
 */
public class BaseActivity extends AppCompatActivity {
    protected static Activity self;
    View decorView;
    public Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (android.os.Build.VERSION.SDK_INT >= 21){
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            //window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.primary_dark));
        }

        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mTracker = ((Bootstrap) getApplication()).getTracker(Bootstrap.TrackerName.APP_TRACKER);

        //decorView = getWindow().getDecorView();
        //decorView.setSystemUiVisibility(View.SYSTEM_UI_LAYOUT_FLAGS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Bootstrap.setCurrentActivity(this);
        self = BaseActivity.this;
    }

    @Override
    protected void onPause() {
        clearReferences();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    private void clearReferences() {
        Activity currActivity = Bootstrap.getCurrentActivity();
        if (currActivity != null && currActivity.equals(this)) {
            Bootstrap.setCurrentActivity(null);
        }
    }

    protected static void showWaitingDialog() {
        //WaitingDialog.showWaitingDialog(self);
    }

    protected static void cancelWaitingDialog() {
        //WaitingDialog.cancelWaitingDialog();
    }

    protected void redirectLoginActivity() {
//        final Intent intent = new Intent(this, SampleLoginActivity.class);
//        startActivity(intent);
//        finish();
    }

    protected void redirectSignupActivity() {
//        final Intent intent = new Intent(this, SampleSignupActivity.class);
//        startActivity(intent);
//        finish();
    }
}
